drop procedure `recruit`.`SP_UpdateSubmissionDate`;

delimiter $$
CREATE PROCEDURE `SP_UpdateSubmissionDate`(id int,date varchar(350))
BLOCK1:BEGIN
DECLARE strLen,candidateId INT DEFAULT 0;
DECLARE new_date varchar(50);
DECLARE submissionDate datetime;

delete from temp_requirements where requirementId=id;
do_this: LOOP

    SET strLen =   LENGTH(date);
    
    SET new_date = subString(date,1,INSTR(date, ',')-1);

    IF new_date = NULL or new_date = '' THEN
      SET new_date = date;
    END IF;    

    SET date = TRIM(MID(date, length(new_date)+2,strLen));

select subString(new_date,1,INSTR(new_date, '-')-1) ,
if(new_date like '%null%',null,str_to_date(subString(new_date,INSTR(new_date, '-')+1,length(new_date)),'%m/%d/%Y'))
into candidateId,submissionDate;

insert into temp_requirements(requirementId,candidateId,submittedDate) values(id,candidateId,submissionDate);

    IF date = NULL or date = '' THEN
      LEAVE do_this;
    END IF;    

END LOOP do_this;

END Block1$$