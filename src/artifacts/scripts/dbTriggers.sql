DELIMITER $$
CREATE TRIGGER `recruit`.`candidateInsertionTrigger` AFTER INSERT ON `recruit`.`candidate` FOR EACH ROW
BEGIN
insert into quickaccess(name,type,referenceId) 
  values(concat(NEW.firstname,' ',NEW.lastname),'Candidate',NEW.id);
END$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER `recruit`.`candidateDeletionTrigger` BEFORE DELETE ON `recruit`.`candidate` FOR EACH ROW
BEGIN
delete from quickaccess where referenceId=old.id and type= 'Candidate'
and name = (select concat(firstname,' ',lastname) from candidate where id=old.id);
END$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER `recruit`.`vendorInsertionTrigger` AFTER INSERT ON `recruit`.`client`
FOR EACH ROW
BEGIN
if NEW.type = 'Vendor' then 
insert into quickaccess(name,type,referenceId) 
  values(NEW.name,'Vendor',NEW.id);
end if;
END$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER `recruit`.`vendorDeletionTrigger` BEFORE DELETE ON `recruit`.`client` FOR EACH ROW
BEGIN
if (select type from client where id=old.id) = 'Vendor' then
delete from quickaccess where referenceId=old.id and type= 'Vendor'
and name = (select name from client where id=old.id);
end if;
END$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER `recruit`.`JobOpeningStatusUpdateTrigger` BEFORE UPDATE ON `recruit`.`requirements` FOR EACH ROW
IF NOT (NEW.jobOpeningStatus <=> OLD.jobOpeningStatus ) THEN
  insert into eventlogging(oldValue,newValue,tableName,columnName,referenceId,loggedOn,loggedBy) 
  values(OLD.jobOpeningStatus,NEW.jobOpeningStatus,'Requirements','jobOpeningStatus',NEW.id,NEW.lastUpdated,NEW.lastUpdatedUser);
END IF$$
DELIMITER ;