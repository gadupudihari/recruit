package com.recruit.util;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

/**
 * Converts a java.sql.Timestamp to text.
 *
 * @author Joe Walnes
 */
public class SqlTimestampConverter extends AbstractSingleValueConverter {

    public boolean canConvert(Class type) {
        return type.equals(Timestamp.class);
    }

    public Object fromString(String str) {
    	if(str!= null ){
    		str = str.replace('T', ' ');
    	}
    	if(str.indexOf("/") >0 ){
    		
    	}
    	if("".equals(str) || str == null){
    		return null;
    	}
        return Timestamp.valueOf(str);
    }

    public Object fromStringToTime(String str) {
    	if(str!= null ){
    		str = str.replace('T', ' ');
    	}
    	if(str.indexOf("/") >0 ){
    		
    	}
    	if("".equals(str) || str == null){
    		return null;
    	}
        return Time.valueOf(str);
    }

    public Object fromStringToDate(String str) {
    	if(str!= null ){
    		str = str.replace('T', ' ');
    	}
    	if(str.indexOf("/") >0 ){
    		
    	}
    	if("".equals(str) || str == null){
    		return null;
    	}
        return Date.valueOf(str.substring(0,10));
    }

}