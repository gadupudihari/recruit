package com.recruit.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.w3c.dom.Element;

public class SendPendingTasks implements Job {

	@SuppressWarnings("deprecation")
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			Connection conn = null;

			//Mail willbe send on Sunday (index = 0), Saturday (index = 6) 
			if(UtilityHelper.getDate().getDay() ==0 || UtilityHelper.getDate().getDay() == 6){
				System.out.println("Mail will be send on weekends");
				return;
			}
			
			String url = "";
			String driver = "";
			String userName = "";
			String password = "";
			
			//Retrieving database url, username, password from jdbc.properties
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("jdbc.properties"));
			url = properties.getProperty("jdbc.databaseurl");
			driver = properties.getProperty("jdbc.driverClassName");
			userName = properties.getProperty("jdbc.username");
			password = properties.getProperty("jdbc.password");
			if (url.contains("autoReconnect")) 				
				url = url.substring(0,url.indexOf("autoReconnect")-1);
			
			Class.forName(driver).newInstance();
			conn = DriverManager
					.getConnection(url, userName, password);
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("appSettings.properties"));
			final String mailusername = properties.getProperty("email.id");
			final String mailpassword = properties.getProperty("email.password");

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");//this is smtp server address
			props.put("mail.smtp.port", "587");//this is port of the smtp server 
	 
			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailusername, mailpassword);
				}
			});

			//pending research tasks email
			pendingTasksEmail(session, conn, mailusername);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    //Mail to send abount research tasks
	@SuppressWarnings("deprecation")
	private void pendingTasksEmail(Session session, Connection conn, String mailusername){
		try {
			System.out.println("----------Pending research tasks-----------"+UtilityHelper.getDate());
			String mailtoRole="",mailtoignore="";
			Element eElement = UtilityHelper.readXmlFile("Pending research tasks");
        	mailtoRole = eElement.getElementsByTagName("mailtoRole").item(0).getChildNodes().item(0).getNodeValue();
        	mailtoignore = eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue();
        	int priority = Integer.parseInt(eElement.getElementsByTagName("priority").item(0).getChildNodes().item(0).getNodeValue());
		     if (mailtoRole.contains(",")) 
		    	 mailtoRole = mailtoRole.replaceAll(",", "','");
		     if (UtilityHelper.getDate().getHours() == 13)
		    	 mailtoRole = "RECRUITER";
		     mailtoRole = "'"+mailtoRole+"'";
		     String[] ignoreEmails = mailtoignore.split(",");


			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT r.id,r.hotness,r.cityAndState,r.postingTitle,date_format(r.postingdate,'%m/%d') postingdate," +
					"r.module,r.addInfo_Todos alert,t.task,t.status,t.comments,t.serialNo," +
					"t.lastUpdatedUser,date_format(t.lastUpdated,'%m/%d/%Y') lastUpdated,(select name from recruit_clients where id=r.clientId) client," +
					"(select group_concat(name) from recruit_clients c,recruit_sub_vendors s where c.id=s.vendorId and s.requirementId =r.id) vendor," +
					"(select count(candidateId) from recruit_sub_requirements where requirementId=r.id and submissionStatus='Shortlisted'and deleted = false ) shortlisted," +
					"(select group_concat(name) from recruit_clients where id in (select distinct s1.vendorId from recruit_clients c1,recruit_requirements r1," +
					"recruit_sub_vendors s1 where r1.id=s1.requirementId and r1.clientId=c1.id and r1.clientId= r.clientId and c1.name!='n/a')) suggestedVendors" +
					" FROM timesheetz.recruit_requirements r,recruit_researchtasks t " +
					" where r.id=t.requirementId and r.hotness in ('00','01') and r.jobOpeningStatus = '01.In-progress' and datediff(now(),r.postingDate) <=30 " +
					" and r.id in (select requirementId from recruit_researchtasks where status in('In Progress','Next in Line','Yet to Start'))  " +
					" order by r.postingdate desc,r.hotness,r.id,field(t.status,'Yet to Start','In Progress','Next in Line','Completed'),t.lastUpdated desc");
			
			String table ="";
			
			String headings = "<col width='5%'> <col width='10%'> <col width='35%'> <col width='35%'> <col width='10%'> <col width='10%'>"+
					"<tr bgcolor='#EFEFFF' style='font-style:italic;text-align:center;'>"+
					"<td>S.No</td>"+
					"<td>Status</td>"+
					"<td>Task</td>"+
					"<td>Comments</td>"+
					"<td>Updated by</td>"+
					"<td>Updated on</td></tr>";
			String preId ="";
			while (rs.next()) {
				String id = rs.getString("id");
				String client = rs.getString("client");
				String vendor = rs.getString("vendor");
				String suggested = rs.getString("suggestedVendors");
				
				if (client == null || client.equalsIgnoreCase("null")) 
					client = "N/A";
				if (vendor == null || vendor.equalsIgnoreCase("null")) 
					vendor = "N/A";
				if (suggested == null || suggested.equalsIgnoreCase("null")) 
					suggested = "N/A";

				String requirement = "<tr bgcolor='#DAEAFC'><td colspan='6'>Id : <b>"+rs.getString("id")+
						"</b>, Date : <b>"+rs.getString("postingdate")+"</b>, Title : "+rs.getString("postingTitle")+", Module : <b>"+rs.getString("module")+
						"</b>, Client : <b>"+client+",</b> Vendor : <b>"+vendor+"</b>, Alert : <span style='color:red;'>"+rs.getString("alert")+"</span>"+
						", City & State : "+rs.getString("cityAndState")+", Shortlisted : "+rs.getString("shortlisted")+", Suggested Vendors : "+suggested
						+"</td></tr>";
				
				if (preId.equals("")) {
					preId = id;
					table += "<table cellpadding='5' width='100%' bgcolor='#575252'>"+requirement+headings;
				}
				if (!preId.equals(id)) {
					table += "</table><br><table cellpadding='5' width='100%' bgcolor='#575252'>"+requirement+headings;
					preId = id;
				}
				String textColor = "#000000", bgColor="#FFFFFF";
				if (rs.getString("status").equalsIgnoreCase("Completed")){
					textColor = "gray";
					bgColor = "#efefef";
				}
				
				table += "<tr bgcolor='"+bgColor+"' style='color: "+textColor+"; '>" +
						"<td style='text-align:center;'>"+rs.getString("serialNo")+"</td>"+
						"<td>"+rs.getString("status")+"</td>"+
						"<td>"+rs.getString("task")+"</td>"+
						"<td>"+rs.getString("comments")+"</td>"+
						"<td>"+rs.getString("lastUpdatedUser")+"</td>"+
						"<td>"+rs.getString("lastUpdated")+"</td></tr>";
			}
			if (table.length() > 0) {
				table =  table+ "</table>" ;
				
				//This alert body cannot be saved in data base hence alert will be sent now and stored in email table without body
				 String sql = "insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,alertInDays,refId,application,priority) values(?,?,?,?,?,?,?,?,?,?,?,?)";
				 PreparedStatement ps=conn.prepareStatement(sql);
			     ps.setObject(2, UtilityHelper.getDate());
			     ps.setObject(3, UtilityHelper.getDate());
			     ps.setObject(4, "Pending research tasks");
			     ps.setObject(5, "Please verify your email to know the details");
			     ps.setObject(6, true);
			     ps.setObject(7, "Pending research tasks");
			     ps.setObject(8, "Detailed");
			     ps.setObject(9, 0);
			     ps.setObject(10, 0);
			     ps.setObject(11, "recruit");
			     ps.setObject(12, priority);

				
				String allEmailIds ="";
				ResultSet adminUsers = stmt.executeQuery("SELECT u.* FROM recruit_user u, recruit_authority a where u.loginId=a.loginId and a.authority in ("+mailtoRole+") " );
				while (adminUsers.next()) {
					String emailId = adminUsers.getString("emailId");

					//checking emailid given in ignore list
					if (! Arrays.asList(ignoreEmails).contains(emailId)) {
						allEmailIds += emailId+",";
						
						//Store in db
						ps.setObject(1, emailId);
					    ps.executeUpdate();
					}
				}
				
				if(allEmailIds.length() > 0){
					allEmailIds = allEmailIds.substring(0,allEmailIds.length()-1);
					Message msg = new MimeMessage(session);
					msg.setFrom(new InternetAddress(mailusername));//from email address
					msg.setSubject("Pending research tasks");//sets subject of email
					//sets the content body of the email
					msg.setContent(table+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");//sets the content body of the email
					msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
					Transport.send(msg);//sends the entire message object
				}
				
				createHtmlFile("PendingTasks", "Pending research tasks", table);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createHtmlFile(String newFileName,String htmlTitle, String htmlString) {
		try {
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("appSettings.properties"));
			String path = properties.getProperty("submissionReportPath");
			System.out.println("----------Delete older files-------------");
	        File destFolder = new File(path);
			if(!destFolder.exists()) destFolder.mkdirs();
			//delete old files 
			File[] filesList = destFolder.listFiles();  
			for (int n = 0; n < filesList.length; n++) {
				if (filesList[n].isFile()){
					Date lastmodified = new Date(filesList[n].lastModified());
					if ((new Date().getTime() - lastmodified.getTime())/(1000*60*60*24) > 30) {
						filesList[n].delete();
					}
				}
			}
			System.out.println("----------Deleted older files-------------");
			
			
			System.out.println("----------Creating report file-------------");
			//create text file
		    File file = new File(path+"test.txt");
		    if (!file.exists()) {
		    	file.createNewFile();
			}
		    BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		    bw.write(htmlString);
		    bw.close();
		    Date date = new Date();
		    String currentDate= new SimpleDateFormat("MM-dd-yyyy").format(date);
		    //tranfer text file into html file
		    BufferedReader txtfile = new BufferedReader(new FileReader(path+"test.txt"));
            OutputStream htmlfile= new FileOutputStream(new File(path+newFileName+"-"+currentDate+".html"));
            PrintStream printhtml = new PrintStream(htmlfile);

            String[] txtbyLine = new String[500];
            String temp = "";
            String txtfiledata = "";

            String htmlheader="<html><head>";
            htmlheader+="<title>"+htmlTitle+"</title>";
            htmlheader+="</head><body>";
            String htmlfooter="</body></html>";
            int linenum = 0 ;

            while ((txtfiledata = txtfile.readLine())!= null)
               {
                    txtbyLine[linenum] = txtfiledata;
                    linenum++;
                } 
            for(int i=0;i<linenum;i++)
                {
                    if(i == 0)
                    {
                        temp = htmlheader + txtbyLine[0];
                        txtbyLine[0] = temp;
                    }
                    if(linenum == i+1)
                    {
                        temp = txtbyLine[i] + htmlfooter;
                        txtbyLine[i] = temp;
                    }
                    printhtml.println(txtbyLine[i]);
                }
            System.out.println("--------------html file created ");
        printhtml.close();
        htmlfile.close();
        txtfile.close();
        file.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
