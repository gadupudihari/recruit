package com.recruit.util;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
 
public class Encriptor {
    private Cipher ecipher;
    private Cipher dcipher;
    private final static String md5HashValue = "76da72fcd443f752";
 
    Encriptor(SecretKey key) {
        try {
            ecipher = Cipher.getInstance("AES");
            dcipher = Cipher.getInstance("AES");
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            dcipher.init(Cipher.DECRYPT_MODE, key);
        } catch (Exception e) {
            System.out.println("Failed in initialization");
        }
    }
 
    public String encrypt(String str) {
        try {
            byte[] utf8 = str.getBytes("UTF-8");
            byte[] enc = ecipher.doFinal(utf8);
             
            return new sun.misc.BASE64Encoder().encode(enc);
        } catch (Exception e) {
            System.out.println("Failed in Encryption");
        }
        return null;
    }
 
    public String decrypt(String str) {
        try {
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
 
            byte[] utf8 = dcipher.doFinal(dec);
 
            return new String(utf8, "UTF-8");
        } catch (Exception e) {
            System.out.println("Failed in Decryption");
        }
        return null;
    }
 
    public static void main(String[] args) {
        try {
        	//Write a method to get hash value of the logged in use from Database
        	String md5HashValue = "76da72fcd443f752b80816871b92a750";
        	//The key should only be 16 chars in length so cut it
            String mykey =md5HashValue.substring(0, 16);
            System.out.println(mykey);
            SecretKey key = new SecretKeySpec(mykey.getBytes(), "AES");
            //Pass the key for encryption / decryption
            Encriptor encrypter = new Encriptor(key);
            String original = "C@ndrops123";         
            //Save this encryted in DB as password
            String encrypted = encrypter.encrypt(original);
            System.out.println("After Encryption   : " + encrypted);
            
            
            //read the email password from DB and pass this to decryption 
            String decrypted = encrypter.decrypt("7og3DYEwUkecMh1ep1HrhQ==");
            System.out.println("After Decryption   : " + decrypted);
             
        } catch (Exception e) {
        }
    }
    
    public static String encryptString(String originalString){
    	//The key should only be 16 chars in length 
    	SecretKey key = new SecretKeySpec(md5HashValue.getBytes(), "AES");
    	Encriptor encrypter = new Encriptor(key);
        //Pass the key for encryption / decryption
    	String encrypted = encrypter.encrypt(originalString);
    	return encrypted;
    }

    public static String decryptString(String originalString){
    	//The key should only be 16 chars in length 
    	SecretKey key = new SecretKeySpec(md5HashValue.getBytes(), "AES");
    	Encriptor decrypter = new Encriptor(key);
        //Pass the key for encryption / decryption
    	String decrypted = decrypter.decrypt(originalString);
    	return decrypted;
    }

}