package com.recruit.util;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class HelloSchedule extends HttpServlet{
	
	 @Override
	    protected void service(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
		 
	 }
	 
	 /** Handles the HTTP <code>GET</code> method.
	     * @param request servlet request
	     * @param response servlet response
	     */
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	        service(request, response);
	    }
	    
	    /** Handles the HTTP <code>POST</code> method.
	     * @param request servlet request
	     * @param response servlet response
	     */
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	        service(request, response);
	    }
	    
	    /** Returns a short description of the servlet.
	     */
	    public String getServletInfo() {
	        return "Short description";
	    }
	    
	    public void init(ServletConfig config) throws ServletException {
	        super.init(config);
	        System.out.println("Entered HelloSchedule Servlet init..........................");

	    	QuartzScheduler b1=new QuartzScheduler();
	    	try{
	    		System.out.println("HelloSchedule Servelet init() invoked.... SendEmail Table Triggers are set.");
	    			b1.run();
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	 
	    }
	
}