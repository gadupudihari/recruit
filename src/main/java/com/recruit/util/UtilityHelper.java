package com.recruit.util;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.dataengine.security.SecurityContext;
import com.recruit.base.presentation.TSSecurityContext;

public class UtilityHelper {

	//returns login id of user who is logged in
	public static String getLoginId(HttpServletRequest request){
		Object obj1= request.getAttribute(SecurityContext.DATENGINE_CONTEXT);
		TSSecurityContext user = (TSSecurityContext)obj1;
		String loginId =user.getUserId();
		return loginId;
	}
	
	//returns current date
	public static Timestamp getDate(){
		Calendar cal = Calendar.getInstance();
		Date currentDate = cal.getTime();
		java.sql.Timestamp createdDate = new Timestamp(currentDate.getTime());
		return createdDate;
	}
	
	//returns property value of specified in specified file 
	public static String getPropery(String fileName,String property){
		Properties properties = new Properties();
		String url = "";
		try {
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName));
			url = properties.getProperty(property);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return url;
	}

	//Venkat -- converts string date to time stamp date
    public static Timestamp stringDateToDBFormat(String stringDate){
    	Timestamp dbDate = null;
    	 
    	try
		{	
    		System.out.println("Trying to convert MM/DD/YYYY format to DB format");
			SimpleDateFormat fromCSVFile = new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat toDBFormat = new SimpleDateFormat("yyyy-MM-dd");
			String reformattedStr = toDBFormat.format(fromCSVFile.parse(stringDate));
			//String reformattedStr = toDBFormat.format(stringDate);
			Date parsedDate = toDBFormat.parse(reformattedStr);
			dbDate = new java.sql.Timestamp(parsedDate.getTime());
			return dbDate;
		}
		catch(Exception e)
		{			
			System.out.println("Error ---> Input Date is not in MM/DD/YYYY format");
			
		}
    	try
		{
    		System.out.println("Trying to convert MM-DD-YYYY format to DB format");
			SimpleDateFormat fromCSVFile = new SimpleDateFormat("MM-dd-yyyy");
			SimpleDateFormat toDBFormat = new SimpleDateFormat("yyyy-MM-dd");
			String reformattedStr = toDBFormat.format(fromCSVFile.parse(stringDate));
			//String reformattedStr = toDBFormat.format(stringDate);
			Date parsedDate = toDBFormat.parse(reformattedStr);
			dbDate = new java.sql.Timestamp(parsedDate.getTime());
			return dbDate;
		}
		catch(Exception e)
		{			
			System.out.println("Error ---> Input Date is not in MM-DD-YYYY format");
		}
    	return null;
    	
    }    
       
    //venkat - Converts yyyy-mm-dd format to timestamp
    public static Timestamp stringDateToDBFormat1(String dt){
    	Timestamp dbDate = null;
    	try
		{
			//TO DO -- Move this logic to some Utility.java file and use it from there.
			SimpleDateFormat fromCSVFile = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat toDBFormat = new SimpleDateFormat("yyyy-MM-dd");
			String reformattedStr = toDBFormat.format(fromCSVFile.parse(dt));
			//String reformattedStr = toDBFormat.format(stringDate);
			Date parsedDate = toDBFormat.parse(reformattedStr);
			dbDate = new java.sql.Timestamp(parsedDate.getTime());
			return dbDate;
		}
		catch(Exception e)
		{			
			e.printStackTrace();			
		}
    	return null;
    }
    
    //Returns the value of given tag in a from the xml file
	public static String getTagValue(String sTag, Element eElement) {
	    NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
	    Node nValue = (Node) nlList.item(0);
	    if (nValue != null) 
	    	return nValue.getNodeValue();	
		return null;
	    
	}

	//Formats big decimal value in currency format $1,500.00 
	public static String toCurrency(BigDecimal amount) {
		java.text.NumberFormat money = NumberFormat.getCurrencyInstance();
		money = NumberFormat.getCurrencyInstance(Locale.CANADA);
	    money.setRoundingMode(RoundingMode.HALF_EVEN);
		return money.format(amount.setScale(2, RoundingMode.HALF_EVEN));
	}
	
	//Formats big decimal value in currency format 1,500.00 
	public static String toCurrencyValue(BigDecimal amount) {
		DecimalFormat df = new DecimalFormat("#,###,##0.00");
        return df.format(amount);
	}
	
	//Venkat
	//convert Timestamp to Month-Year format for example 2012-01-30 is converted to Jan-2012 
	public static String toMonthYear(Timestamp ts) {		
		return(String.format("%1$Tb-%1$TY", ts));
		
	}

	//Delete all .xls files in specified path
	public static void deleteExcelFiles(String path, String lastWord){
	      File file = new File(path);
	      File[] filesList = file.listFiles();  
	      for (int n = 0; n < filesList.length; n++) {
	    	  String fname = "",ext = "";
	    	  int mid = 0;
	    	  if (filesList[n].isFile())
	    		  if(filesList[n].getName().contains(".")){
					mid= filesList[n].getName().lastIndexOf(".");
					fname=filesList[n].getName().substring(0,mid);
					ext=filesList[n].getName().substring(mid+1,filesList[n].getName().length());
					if(ext.equals("xls") && fname.contains(lastWord)){
		              filesList[n].delete();
					}
				}
	      }

	}

	public static void deletePdfFiles(String contextPath) {
		  //delete all .pdf files in server
	      File file = new File(contextPath);
	      File[] filesList = file.listFiles();  
	      for (int n = 0; n < filesList.length; n++) {
	    	  String ext = "";
	    	  int mid = 0;
	    	  if (filesList[n].isFile())
	    		  if(filesList[n].getName().contains(".")){
					mid= filesList[n].getName().lastIndexOf(".");
					ext=filesList[n].getName().substring(mid+1,filesList[n].getName().length());
					if(ext.equals("pdf") ){
		              filesList[n].delete();
					}
				}
	      }
		
	}

	public static Element readXmlFile(String alertName){
		try {
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("appSettings.properties"));
			String contextPath = properties.getProperty("contextPath");
			File fXmlFile = new File(contextPath+"\\Xml files\\alerts.xml");
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile); 
			doc.getDocumentElement().normalize();
	    	NodeList nList = doc.getElementsByTagName("alert");

			for (int temp = 0; temp < nList.getLength(); temp++) {
		        Node nNode = nList.item(temp);
		        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		            Element eElement = (Element) nNode;
		            
                    NodeList nodeList = eElement.getElementsByTagName("alertname");
                    if (nodeList.item(0).getChildNodes().item(0).getNodeValue().equalsIgnoreCase(alertName)) {
                    	return eElement;
					}
		        }
		    }
		} catch (SAXException e) {
			e.printStackTrace();
		} 
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
