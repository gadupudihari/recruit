package com.recruit.util;

import java.util.Calendar;
import java.util.Date;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzScheduler {
	//Author - Naveen 
	//Method to set Triggers. We are setting 2 triggers 
	//1--SendEmail - which sends email on a given time regarding interviews to all admin users 
	public void run() throws Exception{
		SchedulerFactory schedulerFactory =new StdSchedulerFactory();
		System.out.println("Entered QuartzScheduler run method...Interview Trigger");
		Scheduler sched=schedulerFactory.getScheduler();
		java.util.Calendar cal = new java.util.GregorianCalendar();
		cal.set(Calendar.AM_PM, 0);
		cal.set(Calendar.HOUR, 6);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		//scheduling at a particular time daily in the morning at 10:01 A.M.
		Date startTime = cal.getTime();
		// start the scheduler
		sched.start();
		// Initiate JobDetail with job name, job group, and executable job class to send Email Alert
		JobDetail jd=new JobDetail("Interviewjob","InterviewGroup",SendInterviewEmail.class);
		// Initiate SimpleTrigger with its name and group name runs every day(86400 seconds) at start time
		SimpleTrigger st=new SimpleTrigger("Interviewtrigger","Interviewgroup",startTime,null,SimpleTrigger.REPEAT_INDEFINITELY,86400L*1000L);
		sched.scheduleJob(jd, st);
		
		System.out.println("Entered QuartzScheduler run method...reminder Trigger");
		
		//Naveen 02/12: interview alert before 1 hour, 2 hours is removed 
	    Scheduler sched2=schedulerFactory.getScheduler();
	    //Initiate JobDetail with job name, job group, and executable job class to send Email Alert
	    JobDetail jd2=new JobDetail("ReminderJob","ReminderGroup",CheckReminder.class);
	    // Initiate SimpleTrigger with its name and group name runs every 60 minutes from start time
	    CronTrigger ct=new CronTrigger("cronTrigger","ReminderGroup","0 0/60 * * * ?");
	    sched2.scheduleJob(jd2,ct);
	    sched2.start();

		System.out.println("Entered QuartzScheduler run method...submission Trigger");
		Scheduler sched3=schedulerFactory.getScheduler();
		java.util.Calendar cal3 = new java.util.GregorianCalendar();
		cal3.set(Calendar.AM_PM, 0);
		cal3.set(Calendar.HOUR, 18);
		cal3.set(Calendar.MINUTE, 0);
		cal3.set(Calendar.SECOND, 0);
		cal3.set(Calendar.MILLISECOND, 0);
		System.out.println("-------------Submission Trigger-----------"+cal3.getTime());
		//scheduling at a particular time daily in the morning at 10:01 A.M.
		Date startTime3 = cal3.getTime();
		// start the scheduler
		sched3.start();
		// Initiate JobDetail with job name, job group, and executable job class to send Email Alert
		JobDetail jd3=new JobDetail("myjob3","Group3",SendSubmissionReport.class);
		// Initiate SimpleTrigger with its name and group name runs every day(86400 seconds) at start time
		SimpleTrigger st3=new SimpleTrigger("mytrigger3","scheduledgroup3",startTime3,null,SimpleTrigger.REPEAT_INDEFINITELY,86400L*1000L);
		sched3.scheduleJob(jd3, st3);

		System.out.println("Entered QuartzScheduler run method...Completed tasks Trigger");
		Scheduler sched4=schedulerFactory.getScheduler();
		java.util.Calendar cal4 = new java.util.GregorianCalendar();
		cal4.set(Calendar.AM_PM, 0);
		cal4.set(Calendar.HOUR, 18);
		cal4.set(Calendar.MINUTE, 05);
		cal4.set(Calendar.SECOND, 0);
		cal4.set(Calendar.MILLISECOND, 0);
		//scheduling at a particular time daily in the morning at 10:01 A.M.
		Date startTime4 = cal4.getTime();
		// start the scheduler
		sched4.start();
		// Initiate JobDetail with job name, job group, and executable job class to send Email Alert
		JobDetail jd4=new JobDetail("myjob4","Group4",SendCompletedTasks.class);
		// Initiate SimpleTrigger with its name and group name runs every day(86400 seconds) at start time
		SimpleTrigger st4=new SimpleTrigger("mytrigger4","scheduledgroup4",startTime4,null,SimpleTrigger.REPEAT_INDEFINITELY,86400L*1000L);
		sched4.scheduleJob(jd4, st4);

		System.out.println("Entered QuartzScheduler run method...Pending tasks Trigger");
		Scheduler sched5=schedulerFactory.getScheduler();
		java.util.Calendar cal5 = new java.util.GregorianCalendar();
		cal5.set(Calendar.AM_PM, 0);
		cal5.set(Calendar.HOUR, 18);
		cal5.set(Calendar.MINUTE, 0);
		cal5.set(Calendar.SECOND, 0);
		cal5.set(Calendar.MILLISECOND, 0);
		System.out.println("-------------Pending Tasks Trigger-----------"+cal5.getTime());
		//scheduling at a particular time daily in the morning at 10:01 A.M.
		Date startTime5 = cal5.getTime();
		// start the scheduler
		sched5.start();
		// Initiate JobDetail with job name, job group, and executable job class to send Email Alert
		JobDetail jd5=new JobDetail("myjob5","Group5",SendPendingTasks.class);
		// Initiate SimpleTrigger with its name and group name runs every day(86400 seconds) at start time
		SimpleTrigger st5=new SimpleTrigger("mytrigger5","scheduledgroup5",startTime5,null,SimpleTrigger.REPEAT_INDEFINITELY,86400L*1000L);
		sched4.scheduleJob(jd5, st5);

		System.out.println("Entered QuartzScheduler run method...Run Events Trigger");
		Scheduler sched6=schedulerFactory.getScheduler();
		java.util.Calendar cal6 = new java.util.GregorianCalendar();
		cal6.set(Calendar.AM_PM, 0);
		cal6.set(Calendar.HOUR, 1);
		cal6.set(Calendar.MINUTE, 0);
		cal6.set(Calendar.SECOND, 0);
		cal6.set(Calendar.MILLISECOND, 0);
		//scheduling at a particular time daily in the morning at 10:01 A.M.
		Date startTime6 = cal6.getTime();
		// start the scheduler
		sched6.start();
		// Initiate JobDetail with job name, job group, and executable job class to send Email Alert
		JobDetail jd6=new JobDetail("myjob6","Group6",RunDatabaseEvents.class);
		// Initiate SimpleTrigger with its name and group name runs every day(86400 seconds) at start time
		SimpleTrigger st6=new SimpleTrigger("mytrigger6","scheduledgroup6",startTime6,null,SimpleTrigger.REPEAT_INDEFINITELY,86400L*1000L);
		sched4.scheduleJob(jd6, st6);

		System.out.println("Entered QuartzScheduler run method...Weekly Submissions Trigger");
		Scheduler sched7=schedulerFactory.getScheduler();
		java.util.Calendar cal7 = new java.util.GregorianCalendar();
		cal7.set(Calendar.AM_PM, 0);
		cal7.set(Calendar.HOUR, 6);
		cal7.set(Calendar.MINUTE, 0);
		cal7.set(Calendar.SECOND, 0);
		cal7.set(Calendar.MILLISECOND, 0);
		//scheduling at a particular time daily in the morning at 10:01 A.M.
		Date startTime7 = cal7.getTime();
		// start the scheduler
		sched7.start();
		// Initiate JobDetail with job name, job group, and executable job class to send Email Alert
		JobDetail jd7=new JobDetail("myjob7","Group7",SendEmailAlerts.class);
		// Initiate SimpleTrigger with its name and group name runs every day(86400 seconds) at start time
		SimpleTrigger st7=new SimpleTrigger("mytrigger7","scheduledgroup7",startTime7,null,SimpleTrigger.REPEAT_INDEFINITELY,86400L*1000L);
		sched4.scheduleJob(jd7, st7);

	}
}
