package com.recruit.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.w3c.dom.Element;

public class SendEmailAlerts implements Job {

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			Connection conn = null;

			String url = "";
			String driver = "";
			String userName = "";
			String password = "";
			
			//Retrieving database url, username, password from jdbc.properties
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("jdbc.properties"));
			url = properties.getProperty("jdbc.databaseurl");
			driver = properties.getProperty("jdbc.driverClassName");
			userName = properties.getProperty("jdbc.username");
			password = properties.getProperty("jdbc.password");
			if (url.contains("autoReconnect")) 				
				url = url.substring(0,url.indexOf("autoReconnect")-1);
			
			Class.forName(driver).newInstance();
			conn = DriverManager
					.getConnection(url, userName, password);
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("appSettings.properties"));
			final String mailusername = properties.getProperty("email.id");
			final String mailpassword = properties.getProperty("email.password");

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");//this is smtp server address
			props.put("mail.smtp.port", "587");//this is port of the smtp server 
	 
			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailusername, mailpassword);
				}
			});

			//submissions email
			submissionsEmail(session, conn, mailusername);
			
			//weekly candidate submissions email
			p0candidateSubmissions(session, conn, mailusername);
			p1candidateSubmissions(session, conn, mailusername);
			
			//Candidate checks 
			candidateChecksEmail(session, conn, mailusername);
			
			//Candidates submitted today
			SubmissionsEmail(session, conn, mailusername);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//Mail to send about weekly submissions 
	@SuppressWarnings("deprecation")
	private void submissionsEmail(Session session, Connection conn, String mailusername){
		try {
			//Mail willbe send on Monday (index = 1) 
			if(UtilityHelper.getDate().getDay() !=1 ){
				System.out.println("Mail will be send only on monday");
				return;
			}

			System.out.println("----------Weekly Submissions Email-----------"+UtilityHelper.getDate());
			String mailto="",mailtoignore="";
			Element eElement = UtilityHelper.readXmlFile("Weekly Submissions");
			mailto = eElement.getElementsByTagName("mailto").item(0).getChildNodes().item(0).getNodeValue();
        	mailtoignore = eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue();
        	int priority = Integer.parseInt(eElement.getElementsByTagName("priority").item(0).getChildNodes().item(0).getNodeValue());
        	String[] ignoreEmails = mailtoignore.split(",");
        	String[] emailIds = mailto.split(",");

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select distinct s.id,s.vendorId,date_format(s.submittedDate,'%m/%d/%Y') submittedDate,s.submittedRate1," +
					"s.submittedRate2,date_format(r.postingDate,'%m/%d/%Y') postingDate,r.cityAndState,r.postingTitle,r.module," +
					"concat(c.firstName,' ',c.lastName) candidate,s.submissionStatus," +
					"(select name from recruit_clients where id=s.vendorId) vendor,(select name from recruit_clients where id=r.clientId) client " +
					"from recruit_sub_requirements s,recruit_requirements r,recruit_candidate c where r.id = s.requirementId and c.id=s.candidateId " +
					"and s.deleted = false and date(s.submittedDate) >= date_sub(curdate(),interval 7 day) order by s.vendorId,s.submittedDate desc");
			
			String table ="";
			String headings = "<col width='15%'> <col width='8%'> <col width='8%'> <col width='10%'> <col width='8%'> " +
					"<col width='12%'> <col width='15%'> <col width='8%'> <col width='15%'> <col width='10%'>"+
					"<tr bgcolor='#F2F2F2' style='font-weight:bold;'>" +
					"<th >Comments</th>"+
					"<th >Status</th>"+
					"<th >Submitted Date</th>"+
					"<th >Candidate</th>"+
					"<th >City & State</th>"+
					"<th >Client</th>"+
					"<th >Posting Title</th>"+
					"<th >Posting Date</th>"+
					"<th >Module</th>"+
					"<th >Submitted Rate</th>" +
					"</tr>";
			String preId ="",preVendor = "";
			String vendor ="";
			int i =0;
			while (rs.next()) {
				String vendorId = rs.getString("vendorId");
				String client = rs.getString("client");
				vendor = rs.getString("vendor");
				
				if (client == null || client.equalsIgnoreCase("null")) 
					client = "N/A";

				if (preId.equals("")) {
					preId = vendorId;
					preVendor = vendor;
					table += "<table cellpadding='5' bgcolor='#575252'>"+headings;
				}
				if (!preId.equals(vendorId)) {
					table += "</table>" ;
					//This alert body cannot be saved in data base hence alert will be sent now and stored in email table without body
					 String sql = "insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,alertInDays,refId,application,priority) values(?,?,?,?,?,?,?,?,?,?,?,?)";
					 PreparedStatement ps=conn.prepareStatement(sql);
				     ps.setObject(2, UtilityHelper.getDate());
				     ps.setObject(3, UtilityHelper.getDate());
				     ps.setObject(4, preVendor+" Last week submissions");
				     ps.setObject(5, "Please verify your email to know the details");
				     ps.setObject(6, true);
				     ps.setObject(7, "Weekly Submissions");
				     ps.setObject(8, "Detailed");
				     ps.setObject(9, 0);
				     ps.setObject(10, 0);
				     ps.setObject(11, "recruit");
				     ps.setObject(12, priority);
					
				     String allEmailIds ="";
					for (String emailId : emailIds) {
						//checking emailid given in ignore list
						if (! Arrays.asList(ignoreEmails).contains(emailId)) {
							allEmailIds += emailId+",";

							//Store in db
							ps.setObject(1, emailId);
						    ps.executeUpdate();
						}
					}
					
					if(allEmailIds.length() > 0){
						allEmailIds = allEmailIds.substring(0,allEmailIds.length()-1);
						Message msg = new MimeMessage(session);
						msg.setFrom(new InternetAddress(mailusername));//from email address
						msg.setSubject(preVendor+" Last week submissions");//sets subject of email
						//sets the content body of the email
						msg.setContent(table+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");//sets the content body of the email
						msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
						Transport.send(msg);//sends the entire message object
					}
					
					createHtmlFile(preVendor+" Last week submissions", "Submissions", table);
					table = "<table cellpadding='5' bgcolor='#575252'>"+headings;
					preId = vendorId;
					preVendor = vendor;
					i =0;
				}
				String backGround = "FFFFFF";
				if (i%2 == 0)
					backGround = "E3FFE1";
				i++;
				
				table += "<tr bgcolor='#"+backGround+"'>" +
						"<td></td>"+
						"<td>"+rs.getString("submissionStatus")+"</td>"+
						"<td>"+rs.getString("submittedDate")+"</td>"+
						"<td>"+rs.getString("candidate")+"</td>"+
						"<td>"+rs.getString("cityAndState")+"</td>"+
						"<td>"+client+"</td>"+
						"<td>"+rs.getString("postingTitle")+"</td>" +
						"<td>"+rs.getString("postingDate")+"</td>" +
						"<td>"+rs.getString("module")+"</td>" +
						"<td>"+rs.getString("submittedRate1")+"<br>"+rs.getString("submittedRate2")+"</td></tr>";
			}
			table += "</table>" ;
			
			//This alert body cannot be saved in data base hence alert will be sent now and stored in email table without body
			 String sql = "insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,alertInDays,refId,application,priority) values(?,?,?,?,?,?,?,?,?,?,?,?)";
			 PreparedStatement ps=conn.prepareStatement(sql);
		     ps.setObject(2, UtilityHelper.getDate());
		     ps.setObject(3, UtilityHelper.getDate());
		     ps.setObject(4, vendor+" Last week submissions");
		     ps.setObject(5, "Please verify your email to know the details");
		     ps.setObject(6, true);
		     ps.setObject(7, "Weekly Submissions");
		     ps.setObject(8, "Detailed");
		     ps.setObject(9, 0);
		     ps.setObject(10, 0);
		     ps.setObject(11, "recruit");
		     ps.setObject(12, priority);
			
		     String allEmailIds ="";
		     for (String emailId : emailIds) {
		    	 //checking emailid given in ignore list
		    	 if (! Arrays.asList(ignoreEmails).contains(emailId)) {
		    		 allEmailIds += emailId+",";
		    		 
		    		 //Store in db
		    		 ps.setObject(1, emailId);
		    		 ps.executeUpdate();
		    	 }
		     }
		     
		     if(allEmailIds.length() > 0){
		    	 allEmailIds = allEmailIds.substring(0,allEmailIds.length()-1);
		    	 Message msg = new MimeMessage(session);
		    	 msg.setFrom(new InternetAddress(mailusername));//from email address
	    		 msg.setSubject(preVendor+" Last week submissions");//sets subject of email
	    		 //sets the content body of the email
	    		 msg.setContent(table+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");//sets the content body of the email
		    	 msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
		    	 Transport.send(msg);//sends the entire message object
		     }
		     
		     createHtmlFile(vendor+" Last week submissions", "Submissions", table);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    @SuppressWarnings("deprecation")
	private void p0candidateSubmissions(Session session, Connection conn,	String mailusername) {
		try {
			//Mail willbe send on Monday (index = 1) 
			if(UtilityHelper.getDate().getDay() !=1 ){
				System.out.println("Mail will be send only on monday");
				return;
			}

			System.out.println("----------3 months Candidate Submissions Email-----------"+UtilityHelper.getDate());
			String mailtoRole="",mailtoignore="";
			Element eElement = UtilityHelper.readXmlFile("Weekly Candidate Submissions");
        	mailtoRole = eElement.getElementsByTagName("mailtoRole").item(0).getChildNodes().item(0).getNodeValue();
        	mailtoignore = eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue();
		     if (mailtoRole.contains(",")) 
		    	 mailtoRole = mailtoRole.replaceAll(",", "','");
		     mailtoRole = "'"+mailtoRole+"'";
		     String[] ignoreEmails = mailtoignore.split(",");

        	int priority = Integer.parseInt(eElement.getElementsByTagName("priority").item(0).getChildNodes().item(0).getNodeValue());

		     String allEmailIds ="";
		     Statement stmt2 = conn.createStatement();
		     ResultSet adminUsers = stmt2.executeQuery("SELECT u.* FROM recruit_user u, recruit_authority a where u.loginId=a.loginId and a.authority in ("+mailtoRole+") " );
		     while (adminUsers.next()) {
		    	 String emailId = adminUsers.getString("emailId");
		    	 //checking emailid given in ignore list
		    	 if (! Arrays.asList(ignoreEmails).contains(emailId)) {
		    		 allEmailIds += emailId+",";
		    	 }
		     }
		     if(allEmailIds.length() > 0){
		    	 allEmailIds = allEmailIds.substring(0,allEmailIds.length()-1);
		     }

        	
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select * from (" +
					"select distinct s.id,s.candidateId,date_format(s.submittedDate,'%m/%d/%Y') submittedDate, s.submittedDate canSubmittedDate," +
					"s.submissionStatus,r.cityAndState, r.postingTitle,concat(c.firstName,' ',c.lastName) candidate,c.priority," +
					"(select distinct max(p.endDate) from project p,employeebillingrate eb,billingrate b,employee e where p.id=b.projectId " +
					"and eb.billingRateId=b.id and eb.employeeId=e.id and e.candidateId=s.candidateId ) projectEndDate " +
					"from recruit_sub_requirements s,recruit_requirements r,recruit_candidate c where r.id = s.requirementId " +
					"and c.id=s.candidateId and s.deleted = false and s.submissionStatus != 'Shortlisted' " +
					"and c.marketingStatus in ('1. Active','2. Yet to Start') and c.priority in (0) " +
					") tab where if(projectEndDate is null,1=1,canSubmittedDate > date_sub(projectEndDate, interval 2 month)) " +
					"order by priority,candidateId,canSubmittedDate desc");
			
			String table ="", emailBody = "", subject="";
			String headings = "<col width='40%'> <col width='20%'> <col width='20%'> <col width='20%'> <tr bgcolor='#F2F2F2' style='font-weight:bold;'>" +
					"<th>Posting Title</th>"+
					"<th>City & State</th>"+
					"<th>Submitted Date</th>"+
					"<th>Submission Status</th>"+
					"</tr>";
			String preId ="",preCandidate = "", candidate ="";
			int submissionsCount = 0, interviewsCount = 0;
			while (rs.next()) {
				String candidateId = rs.getString("candidateId");
				candidate = rs.getString("candidate");
				
				if (preId.equals("")) {
					preId = candidateId;
					preCandidate = candidate;
					table += "<table cellpadding='5' width='60%' bgcolor='#575252'>"+headings;
				}
				if (!preId.equals(candidateId)) {
					table += "</table><br>" ;

					table = "<b>"+submissionsCount+" Subs, "+interviewsCount+" Interviews for "+preCandidate+"</b>"+table;
					subject = submissionsCount+" Subs, "+interviewsCount+" Interviews for "+preCandidate;
					//p0 candidate send email seperately
					String[] emailIds = allEmailIds.split(",");
					for (String emailId : emailIds) {
						//This alert body cannot be saved in data base hence alert will be sent now and stored in email table without body
						if (! Arrays.asList(ignoreEmails).contains(emailId)) {
							 String sql = "insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,alertInDays,refId,application,priority) values(?,?,?,?,?,?,?,?,?,?,?,?)";
							 PreparedStatement ps=conn.prepareStatement(sql);
						     ps.setObject(1, emailId);
						     ps.setObject(2, UtilityHelper.getDate());
						     ps.setObject(3, UtilityHelper.getDate());
						     ps.setObject(4, subject);
						     ps.setObject(5, "Please verify your email to know the details");
						     ps.setObject(6, true);
						     ps.setObject(7, "Last 3 Months Submissions");
						     ps.setObject(8, "Detailed");
						     ps.setObject(9, 0);
						     ps.setObject(10, 0);
						     ps.setObject(11, "recruit");
						     ps.setObject(12, priority);
				    		 ps.executeUpdate();

						     createHtmlFile(subject, "Submissions", subject+candidate+"<br><br>"+table);
						}
					}
					Message msg = new MimeMessage(session);
					msg.setFrom(new InternetAddress(mailusername));//from email address
					msg.setSubject(subject);//sets subject of email
					//sets the content body of the email
					msg.setContent(table+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");//sets the content body of the email
					msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
					Transport.send(msg);//sends the entire message object
									table = "<table cellpadding='5' width='60%' bgcolor='#575252'>"+headings;
					preId = candidateId;
					preCandidate = candidate;
					submissionsCount = 0;
					interviewsCount = 0;
				}
				submissionsCount++;
				Date submittedDate = new SimpleDateFormat("MM/dd/yyyy").parse(rs.getString("submittedDate"));
				Date today = new Date();
				String backGround = "FFFFFF";
				String[] interviews = {"I-Scheduled","I-Succeeded","I-Withdrawn","I-Failed","C-Declined"};
				if (Arrays.asList(interviews).contains(rs.getString("submissionStatus"))){
					backGround = "FFD3A7";
					interviewsCount++;
				}else if (rs.getString("submissionStatus").equalsIgnoreCase("C-Accepted")){
					backGround = "A7FDA1";
					interviewsCount++;
				}else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) == 0) 
					backGround = "C3D5F6";
				else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) == 1) 
					backGround = "CFDCF5";
				else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) <= 7) 
					backGround = "D9E3F5";
				else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) <= 30) 
					backGround = "E5EBF5";

				table += "<tr bgcolor='#"+backGround+"'>" +
						"<td>"+rs.getString("postingTitle")+"</td>" +
						"<td>"+rs.getString("cityAndState")+"</td>"+
						"<td>"+rs.getString("submittedDate")+"</td>"+
						"<td>"+rs.getString("submissionStatus")+"</td>"+
						"</tr>";
			}
			table += "</table>" ;
			table = "<b>"+submissionsCount+" Subs, "+interviewsCount+" Interviews for "+candidate+"</b>"+table;
			subject = submissionsCount+" Subs, "+interviewsCount+" Interviews for "+candidate;
			
			//This alert body cannot be saved in data base hence alert will be sent now and stored in email table without body
			 String sql = "insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,alertInDays,refId,application,priority) values(?,?,?,?,?,?,?,?,?,?,?,?)";
			 PreparedStatement ps=conn.prepareStatement(sql);
		     ps.setObject(2, UtilityHelper.getDate());
		     ps.setObject(3, UtilityHelper.getDate());
		     ps.setObject(4, subject);
		     ps.setObject(5, "Please verify your email to know the details");
		     ps.setObject(6, true);
		     ps.setObject(7, "Weekly Submissions");
		     ps.setObject(8, "Detailed");
		     ps.setObject(9, 0);
		     ps.setObject(10, 0);
		     ps.setObject(11, "recruit");
		     ps.setObject(12, priority);
			
		     String[] emailIds = allEmailIds.split(",");
		     for (String emailId : emailIds) {
		    	 if (! Arrays.asList(ignoreEmails).contains(emailId)) {
		    		 //Store in db
		    		 ps.setObject(1, emailId);
		    		 ps.executeUpdate();
		    	 }
		     }

		     if(allEmailIds.length() > 0){
		    	 Message msg = new MimeMessage(session);
		    	 msg.setFrom(new InternetAddress(mailusername));//from email address
	    		 msg.setSubject(subject);//sets subject of email
	    		 //sets the content body of the email
	    		 msg.setContent(table+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");//sets the content body of the email
		    	 msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
		    	 Transport.send(msg);//sends the entire message object
		     }
		     
		     createHtmlFile(subject, "Submissions", subject+candidate+"<br><br>"+emailBody);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
    @SuppressWarnings("deprecation")
	private void p1candidateSubmissions(Session session, Connection conn,	String mailusername) {
		try {
			//Mail willbe send on Monday (index = 1) 
			if(UtilityHelper.getDate().getDay() !=1 ){
				System.out.println("Mail will be send only on monday");
				return;
			}

			System.out.println("----------3 months Candidate Submissions Email-----------"+UtilityHelper.getDate());
			String mailtoRole="",mailtoignore="";
			Element eElement = UtilityHelper.readXmlFile("Weekly Candidate Submissions");
        	mailtoRole = eElement.getElementsByTagName("mailtoRole").item(0).getChildNodes().item(0).getNodeValue();
        	mailtoignore = eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue();
		     if (mailtoRole.contains(",")) 
		    	 mailtoRole = mailtoRole.replaceAll(",", "','");
		     mailtoRole = "'"+mailtoRole+"'";
		     String[] ignoreEmails = mailtoignore.split(",");

        	int priority = Integer.parseInt(eElement.getElementsByTagName("priority").item(0).getChildNodes().item(0).getNodeValue());

		     String allEmailIds ="";
		     Statement stmt2 = conn.createStatement();
		     ResultSet adminUsers = stmt2.executeQuery("SELECT u.* FROM recruit_user u, recruit_authority a where u.loginId=a.loginId and a.authority in ("+mailtoRole+") " );
		     while (adminUsers.next()) {
		    	 String emailId = adminUsers.getString("emailId");
		    	 
		    	 //checking emailid given in ignore list
		    	 if (! Arrays.asList(ignoreEmails).contains(emailId)) {
		    		 allEmailIds += emailId+",";
		    	 }
		     }
		     if(allEmailIds.length() > 0){
		    	 allEmailIds = allEmailIds.substring(0,allEmailIds.length()-1);
		     }
		     
		     //delete data from temp tables
		     String sql = "delete FROM submissions";
		     PreparedStatement ps=conn.prepareStatement(sql);
			 ps.executeUpdate();

			 sql = "delete FROM summary";
			 ps=conn.prepareStatement(sql);
			 ps.executeUpdate();
		     
			 //insert submissions
			 sql = "insert into submissions " +
				 		"select id,candidateId,candidate,canSubmittedDate,submissionStatus,cityAndState,postingTitle from ( " +
				 		"select distinct s.id,s.candidateId,date_format(s.submittedDate,'%m/%d/%Y') submittedDate, s.submittedDate canSubmittedDate, " +
				 		"s.submissionStatus,r.cityAndState, r.postingTitle,concat(c.firstName,' ',c.lastName) candidate,c.priority, " +
				 		"(select distinct max(p.endDate) from project p,employeebillingrate eb,billingrate b,employee e where p.id=b.projectId " +
				 		"and eb.billingRateId=b.id and eb.employeeId=e.id and e.candidateId=s.candidateId ) projectEndDate " +
				 		"from recruit_sub_requirements s,recruit_requirements r,recruit_candidate c where r.id = s.requirementId " +
				 		"and c.id=s.candidateId and s.deleted = false and s.submissionStatus != 'Shortlisted' " +
				 		"and c.marketingStatus in ('1. Active','2. Yet to Start') and c.priority in (1) " +
				 		") tab where if(projectEndDate is null,1=1,canSubmittedDate > date_sub(projectEndDate, interval 2 month)) " +
				 		"order by priority,candidateId,canSubmittedDate desc ";
			 ps=conn.prepareStatement(sql);
			 ps.executeUpdate();
		     
		     
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM submissions order by candidateId desc");

			String preId ="",preCandidate = "", candidate ="", candidateId ="";
			int submissionsCount = 0, interviewsCount = 0;

			//Insert summary 
			 sql = "insert into summary (candidateId,candidate,subs,interviews) values(?,?,?,?)";
			 ps=conn.prepareStatement(sql);
			
			while (rs.next()) {
				candidateId = rs.getString("candidateId");
				candidate = rs.getString("candidate");
				if (preId.equals("")) {
					preId = candidateId;
					preCandidate = candidate;
				}
				if (!preId.equals(candidateId)) {
					
					 ps.setObject(1, preId);
					 ps.setObject(2, preCandidate);
				     ps.setObject(3, submissionsCount);
				     ps.setObject(4, interviewsCount);
				     ps.executeUpdate();
					
					preId = candidateId;
					preCandidate = candidate;
					submissionsCount = 0;
					interviewsCount = 0;
				}
				submissionsCount++;
				String[] interviews = {"I-Scheduled","I-Succeeded","I-Withdrawn","I-Failed","C-Declined"};
				if (Arrays.asList(interviews).contains(rs.getString("submissionStatus")))
					interviewsCount++;
			}

			 ps.setObject(1, candidateId);
			 ps.setObject(2, candidate);
		     ps.setObject(3, submissionsCount);
		     ps.setObject(4, interviewsCount);
		     ps.executeUpdate();
		     
		     String emailBody = "";
		     String headings = "<col width='40%'> <col width='20%'> <col width='20%'> <col width='20%'> <tr bgcolor='#F2F2F2' style='font-weight:bold;'>" +
		    		 "<th>Posting Title</th>"+
		    		 "<th>City & State</th>"+
		    		 "<th>Submitted Date</th>"+
		    		 "<th>Submission Status</th>"+
		    		 "</tr>";
		     String summaryTable = "<b>Summary of  for P1 Candidates</b><br>" +
		     		"<table cellpadding='5' width='60%' bgcolor='#575252'><tr bgcolor='#F2F2F2' style='font-weight:bold;'>" +
		     		"<th>Name</th>"+
		     		"<th>Subs</th>"+
		     		"<th>Interviews</th>"+
		     		"</tr>";

		     stmt = conn.createStatement();
		     rs = stmt.executeQuery("SELECT * FROM summary order by subs desc");
		     while (rs.next()) {
		    	 candidateId = rs.getString("candidateId");
		    	 candidate = rs.getString("candidate");
		    	 submissionsCount = rs.getInt("subs");
		    	 interviewsCount = rs.getInt("interviews");
		    	 
			     summaryTable += "<tr bgcolor='#FFFFFF'>" +"<td>"+candidate+"</td>"
			    		 		+"<td style='text-align:center;'>"+submissionsCount+"</td>"
			    		 		+"<td style='text-align:center;'>"+interviewsCount+"</td>"+"</tr>";
			     emailBody += "<b>"+submissionsCount+" Subs, "+interviewsCount+" Interviews for "+candidate+"</b>";
			     emailBody += "<table cellpadding='5' width='60%' bgcolor='#575252'>"+headings;
			     
			     Statement innerstmt = conn.createStatement();
			     ResultSet submissions = innerstmt.executeQuery("SELECT * FROM submissions where candidateId = "+candidateId+" order by submittedDate desc");
			     while (submissions.next()) {
			    	 Date submittedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(submissions.getString("submittedDate"));
			    	 Date today = new Date();
			    	 String backGround = "FFFFFF";
			    	 String[] interviews = {"I-Scheduled","I-Succeeded","I-Withdrawn","I-Failed","C-Declined"};
			    	 if (Arrays.asList(interviews).contains(submissions.getString("submissionStatus"))){
			    		 backGround = "FFD3A7";
			    	 }else if (submissions.getString("submissionStatus").equalsIgnoreCase("C-Accepted")){
			    		 backGround = "A7FDA1";
			    	 }else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) == 0) 
			    		 backGround = "C3D5F6";
			    	 else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) == 1) 
			    		 backGround = "CFDCF5";
			    	 else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) <= 7) 
			    		 backGround = "D9E3F5";
			    	 else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) <= 30) 
			    		 backGround = "E5EBF5";

			    	 emailBody += "<tr bgcolor='#"+backGround+"'>" +
								"<td>"+submissions.getString("postingTitle")+"</td>" +
								"<td>"+submissions.getString("cityAndState")+"</td>"+
								"<td>"+new SimpleDateFormat("MM/dd/yyyy").format(submittedDate)+"</td>"+
								"<td>"+submissions.getString("submissionStatus")+"</td>"+
								"</tr>";
	
			     }
			     emailBody += "</table><br>" ;
		     }
		     summaryTable += "</table><br>";
		    		 
		     String subject = "Submissions for P1 Candidates.";
		     
				//This alert body cannot be saved in data base hence alert will be sent now and stored in email table without body
			 sql = "insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,alertInDays,refId,application,priority) values(?,?,?,?,?,?,?,?,?,?,?,?)";
			 ps=conn.prepareStatement(sql);
		     ps.setObject(2, UtilityHelper.getDate());
		     ps.setObject(3, UtilityHelper.getDate());
		     ps.setObject(4, subject);
		     ps.setObject(5, "Please verify your email to know the details");
		     ps.setObject(6, true);
		     ps.setObject(7, "Weekly Submissions");
		     ps.setObject(8, "Detailed");
		     ps.setObject(9, 0);
		     ps.setObject(10, 0);
		     ps.setObject(11, "recruit");
		     ps.setObject(12, priority);
			
		     String[] emailIds = allEmailIds.split(",");
		     for (String emailId : emailIds) {
		    	 if (! Arrays.asList(ignoreEmails).contains(emailId)) {
		    		 //Store in db
		    		 ps.setObject(1, emailId);
		    		 ps.executeUpdate();
		    	 }
		     }

		     if(allEmailIds.length() > 0){
		    	 Message msg = new MimeMessage(session);
		    	 msg.setFrom(new InternetAddress(mailusername));//from email address
	    		 msg.setSubject(subject);//sets subject of email
	    		 //sets the content body of the email
	    		 msg.setContent(summaryTable+emailBody+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");//sets the content body of the email
		    	 msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
		    	 Transport.send(msg);//sends the entire message object
		     }
		     
		     createHtmlFile(subject, "Submissions", subject+candidate+"<br><br>"+emailBody);
		     
		     //delete data from temp tables
		     sql = "delete FROM submissions";
		     ps=conn.prepareStatement(sql);
			 ps.executeUpdate();

			 sql = "delete FROM summary";
			 ps=conn.prepareStatement(sql);
			 ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private void candidateChecksEmail(Session session, Connection conn,String mailusername) {
		try {
			System.out.println("----------Candidate checks Email-----------"+UtilityHelper.getDate());
			String mailtoRole="",mailtoignore="";
			Element eElement = UtilityHelper.readXmlFile("Candidate Vetting  status");
        	mailtoRole = eElement.getElementsByTagName("mailtoRole").item(0).getChildNodes().item(0).getNodeValue();
        	mailtoignore = eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue();
		     if (mailtoRole.contains(",")) 
		    	 mailtoRole = mailtoRole.replaceAll(",", "','");
		     mailtoRole = "'"+mailtoRole+"'";
		     String[] ignoreEmails = mailtoignore.split(",");
        	int priority = Integer.parseInt(eElement.getElementsByTagName("priority").item(0).getChildNodes().item(0).getNodeValue());

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT *,date_format(created,'%m/%d/%Y') createDate," +
					"(select date_format(startDate,'%m/%d/%Y') from employee where id=c.employeeId) empStartDate," +
					"(select group_concat(s.name) from recruit_candidate_skillset cs,recruit_settings s where s.id=cs.skillSetId and cs.candidateId=c.id) allskillset" +
					"(select group_concat(s.name) from recruit_candidate_skillsetgroup cs,recruit_skillsetgroup s where s.id=cs.skillSetGroupId and cs.candidateId=c.id) skillsetGroup" +
					" FROM recruit_candidate c where priority in (0,1) and marketingStatus in ('1. Active','2. Yet to Start') and (" +
					" (communication is null or communication = '')" +
					" or (personality is null or personality = '')" +
					" or (jobHelp='100-Yes' and (targetRoles is null or targetRoles = ''))" +
					" or (jobHelp='0-No' and (select count(id) from recruit_candidate_skillset where candidateId=c.id) = 0) " +
					" or (jobHelp is null or jobHelp = '')" +
					" or (jobHelp='0-No' and (select count(id) from recruit_candidate_skillsetgroup where candidateId=c.id) = 0)  " +
					") order by priority,marketingStatus,firstname");
			
			String table ="";
			String headings = "<tr bgcolor='#F2F2F2' style='font-weight:bold;'>" +
					"<th >Candidate</th>"+
					"<th >Marketing Status</th>"+
					"<th >Referred By</th>"+
					"<th >Communication</th>"+
					"<th >Personality</th>"+
					"<th >Job Help</th>"+
					"<th >Skill Set</th>"+
					"<th >Target Roles</th>"+
					"<th >Availability</th>"+
					"<th >Start date</th>"+
					"<th >Initial Contact Date</th>"+
					"<th >Experience</th>"+
					"<th >Technical Score</th>"+
					"<th >Skill Set Group</th>"+
					"</tr>";
			while (rs.next()) {
				String availability = "",contactDate="",technicalScore="";
				if (rs.getDate("availability") != null)
					availability = new SimpleDateFormat("MM/dd/yyyy").format(rs.getDate("availability"));
				if (rs.getDate("contactDate") != null)
					contactDate = new SimpleDateFormat("MM/dd/yyyy").format(rs.getDate("contactDate"));
				if (rs.getString("technicalScore") != null && rs.getString("technicalScore").length() > 0)
					technicalScore = rs.getString("technicalScore");
				
				table += "<tr bgcolor='#FFFFFF'>" ;
				table += "<td>"+rs.getString("firstName")+' '+rs.getString("lastName")+"</td>" ;
				table += "<td>"+rs.getString("marketingStatus")+"</td>" ;
				if (rs.getString("referral").equals(null) || rs.getString("referral").equals(""))
					table += "<td style='background: #FDE9D9;'></td>";
				else
					table += "<td>"+rs.getString("referral")+"</td>";
				
				if (rs.getString("communication").equals(null) || rs.getString("communication").equals(""))
					table += "<td style='background: #FDE9D9;'></td>";
				else
					table += "<td>"+rs.getString("communication")+"</td>";
				
				if (rs.getString("personality").equals(null) || rs.getString("personality").equals(""))
					table += "<td style='background: #FDE9D9;'></td>";
				else
					table += "<td>"+rs.getString("personality")+"</td>";
				
				if (rs.getString("jobHelp").equals(null) || rs.getString("jobHelp").equals(""))
					table += "<td style='background: #FDE9D9;'></td>";
				else
					table += "<td>"+rs.getString("jobHelp")+"</td>";
				
				if (rs.getString("allskillset") == null || rs.getString("allskillset").equals(""))
					table += "<td style='background: #FDE9D9;'></td>";
				else
					table += "<td>"+rs.getString("allskillset")+"</td>";
				
				if (rs.getString("targetRoles").equals(null) || rs.getString("targetRoles").equals(""))
					table += "<td style='background: #FDE9D9;'></td>";
				else
					table += "<td>"+rs.getString("targetRoles")+"</td>";
				
				table += "<td>"+availability+"</td>" ;
				
				if (rs.getString("empStartDate") == null || rs.getString("empStartDate").equals(""))
					table += "<td>"+rs.getString("createDate")+"</td>";
				else
					table += "<td>"+rs.getString("empStartDate")+"</td>";
				
				table += "<td>"+contactDate+"</td>";
				
				if (rs.getString("experience").equals(null) || rs.getString("experience").equals(""))
					table += "<td style='background: #FDE9D9;text-align:center;'></td>";
				else
					table += "<td style='text-align:center;'>"+rs.getString("experience")+"</td>";
				
				if (technicalScore == null || technicalScore =="")
					table += "<td style='background: #FDE9D9;'></td>";
				else
					table += "<td style='text-align:center;'>"+technicalScore+"</td>";

				if (rs.getString("skillsetGroup") == null || rs.getString("skillsetGroup").equals(""))
					table += "<td style='background: #FDE9D9;'></td>";
				else
					table += "<td>"+rs.getString("skillsetGroup")+"</td>";
				
				table += "</tr>";
			}
			if (!table.equals("")) {
				table = "<table cellpadding='5' width='100%' bgcolor='#575252'>"+headings+table + "</table>" ;
				table += "<br><br>Alert Id: 960" +
						"<br>Frequency : Daily" +
						"<br><dl><dt>Description:</dt>" +
						"<dd>For P0 and P1s candidates with Marketing status Active and Yet to Start" +
						"<br>1.Communication and Personality are not filled" +
						"<br>2.Job-Help - Yes and Target Role is not filled" +
						"<br>3.Job-help - n/a alert" +
						"<br>4.Job-help - no, no Skill set" +
						"<br>4.Job-help - no, no Skill set group</dd></dl>" ;
				
				
				//This alert body cannot be saved in data base hence alert will be sent now and stored in email table without body
				 String sql = "insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,alertInDays,refId,application,priority) values(?,?,?,?,?,?,?,?,?,?,?,?)";
				 PreparedStatement ps=conn.prepareStatement(sql);
			     ps.setObject(2, UtilityHelper.getDate());
			     ps.setObject(3, UtilityHelper.getDate());
			     ps.setObject(4, "Candidate Vetting  status");
			     ps.setObject(5, "Please verify your email to know the details");
			     ps.setObject(6, true);
			     ps.setObject(7, "Candidate Vetting  status");
			     ps.setObject(8, "Detailed");
			     ps.setObject(9, 0);
			     ps.setObject(10, 0);
			     ps.setObject(11, "recruit");
			     ps.setObject(12, priority);
				
			     String allEmailIds ="";
			     Statement stmt2 = conn.createStatement();
			     ResultSet adminUsers = stmt2.executeQuery("SELECT u.* FROM recruit_user u, recruit_authority a where u.loginId=a.loginId and a.authority in ("+mailtoRole+") " );
			     while (adminUsers.next()) {
			    	 String emailId = adminUsers.getString("emailId");
			    	 //checking emailid given in ignore list
			    	 if (! Arrays.asList(ignoreEmails).contains(emailId)) {
			    		 allEmailIds += emailId+",";
			    		 
			    		 //Store in db
			    		 ps.setObject(1, emailId);
			    		 ps.executeUpdate();
			    	 }
			     }
			     if(allEmailIds.length() > 0){
			    	 allEmailIds = allEmailIds.substring(0,allEmailIds.length()-1);
			    	 Message msg = new MimeMessage(session);
			    	 msg.setFrom(new InternetAddress(mailusername));//from email address
		    		 msg.setSubject("Candidate Vetting  status");//sets subject of email
		    		 //sets the content body of the email
		    		 msg.setContent(table+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");//sets the content body of the email
			    	 msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
			    	 Transport.send(msg);//sends the entire message object
			     }
			     createHtmlFile("Candidate Checks", "Candidate Checks", table);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void SubmissionsEmail(Session session, Connection conn,String mailusername) {
		try {
			System.out.println("----------Candidate Submission-----------"+UtilityHelper.getDate());
			String mailtoRole="",mailtoignore="";
			Element eElement = UtilityHelper.readXmlFile("Candidate Submission");
        	mailtoRole = eElement.getElementsByTagName("mailtoRole").item(0).getChildNodes().item(0).getNodeValue();
        	mailtoignore = eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue();
		     if (mailtoRole.contains(",")) 
		    	 mailtoRole = mailtoRole.replaceAll(",", "','");
		     mailtoRole = "'"+mailtoRole+"'";
		     String[] ignoreEmails = mailtoignore.split(",");
        	int priority = Integer.parseInt(eElement.getElementsByTagName("priority").item(0).getChildNodes().item(0).getNodeValue());

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select r.id,concat(c.firstName,' ',c.lastName) candidate,date_format(s.submittedDate,'%m/%d/%Y') submittedDate," +
					"r.location,r.postingTitle from recruit_requirements r,recruit_sub_requirements s,recruit_candidate c where s.deleted = false and " +
					"s.requirementId=r.id and s.submissionStatus='Submitted' and c.id=s.candidateId and date(s.statusUpdated)=date_sub(curdate(),interval 1 day)");
			
			while (rs.next()) {
				String subject = rs.getString("candidate")+" submitted for "+rs.getString("postingTitle");
				String body = rs.getString("candidate")+" submitted for "+rs.getString("postingTitle")+" at "+rs.getString("location")+
								" on "+rs.getString("submittedDate");

				//This alert body cannot be saved in data base hence alert will be sent now and stored in email table without body
				 String sql = "insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,alertInDays,refId,application,priority) values(?,?,?,?,?,?,?,?,?,?,?,?)";
				 PreparedStatement ps=conn.prepareStatement(sql);
			     ps.setObject(2, UtilityHelper.getDate());
			     ps.setObject(3, UtilityHelper.getDate());
			     ps.setObject(4, subject);
			     ps.setObject(5, body);
			     ps.setObject(6, false);
			     ps.setObject(7, "Candidate Submission");
			     ps.setObject(8, "Other");
			     ps.setObject(9, 0);
			     ps.setObject(10, 0);
			     ps.setObject(11, "recruit");
			     ps.setObject(12, priority);

			     String allEmailIds ="";
			     Statement stmt2 = conn.createStatement();
			     ResultSet adminUsers = stmt2.executeQuery("SELECT u.* FROM recruit_user u, recruit_authority a where u.loginId=a.loginId and a.authority in ("+mailtoRole+") " );
			     while (adminUsers.next()) {
			    	 String emailId = adminUsers.getString("emailId");
			    	 //checking emailid given in ignore list
			    	 if (! Arrays.asList(ignoreEmails).contains(emailId)) {
			    		 allEmailIds += emailId+",";
			    		 
			    		 //Store in db
			    		 ps.setObject(1, emailId);
			    		 ps.executeUpdate();
			    	 }
			     }
			     
			     if(allEmailIds.length() > 0){
			    	 allEmailIds = allEmailIds.substring(0,allEmailIds.length()-1);
			    	 Message msg = new MimeMessage(session);
			    	 msg.setFrom(new InternetAddress(mailusername));//from email address
		    		 msg.setSubject(subject);//sets subject of email
		    		 //sets the content body of the email
		    		 msg.setContent(body+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");//sets the content body of the email
			    	 msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
			    	 Transport.send(msg);//sends the entire message object
			     }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createHtmlFile(String newFileName,String htmlTitle, String htmlString) {
		try {
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("appSettings.properties"));
			String path = properties.getProperty("submissionReportPath");
			System.out.println("----------Delete older files-------------");
	        File destFolder = new File(path);
			if(!destFolder.exists()) destFolder.mkdirs();
			//delete old files 
			File[] filesList = destFolder.listFiles();  
			for (int n = 0; n < filesList.length; n++) {
				if (filesList[n].isFile()){
					Date lastmodified = new Date(filesList[n].lastModified());
					if ((new Date().getTime() - lastmodified.getTime())/(1000*60*60*24) > 30) {
						filesList[n].delete();
					}
				}
			}
			System.out.println("----------Deleted older files-------------");
			
			System.out.println("----------Creating report file-------------");
			//create text file
		    File file = new File(path+"test.txt");
		    if (!file.exists()) {
		    	file.createNewFile();
			}
		    BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		    bw.write(htmlString);
		    bw.close();
		    Date date = new Date();
		    String currentDate= new SimpleDateFormat("MM-dd-yyyy").format(date);
		    //tranfer text file into html file
		    BufferedReader txtfile = new BufferedReader(new FileReader(path+"test.txt"));
            OutputStream htmlfile= new FileOutputStream(new File(path+newFileName+"-"+currentDate+".html"));
            PrintStream printhtml = new PrintStream(htmlfile);

            String[] txtbyLine = new String[500];
            String temp = "";
            String txtfiledata = "";

            String htmlheader="<html><head>";
            htmlheader+="<title>"+htmlTitle+"</title>";
            htmlheader+="</head><body>";
            String htmlfooter="</body></html>";
            int linenum = 0 ;

            while ((txtfiledata = txtfile.readLine())!= null)
               {
                    txtbyLine[linenum] = txtfiledata;
                    linenum++;
                } 
            for(int i=0;i<linenum;i++)
                {
                    if(i == 0)
                    {
                        temp = htmlheader + txtbyLine[0];
                        txtbyLine[0] = temp;
                    }
                    if(linenum == i+1)
                    {
                        temp = txtbyLine[i] + htmlfooter;
                        txtbyLine[i] = temp;
                    }
                    printhtml.println(txtbyLine[i]);
                }
            System.out.println("--------------html file created ");
        printhtml.close();
        htmlfile.close();
        txtfile.close();
        file.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
