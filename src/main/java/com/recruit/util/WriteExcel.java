package com.recruit.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.recruit.candidate.service.Candidate;
import com.recruit.candidate.service.CandidateService;
import com.recruit.client.service.Client;
import com.recruit.client.service.ClientService;
import com.recruit.client.service.Contact;
import com.recruit.interview.service.Interview;
import com.recruit.placementLeads.service.PlacementLeads;
import com.recruit.requirement.service.Requirement;
import com.recruit.requirement.service.Sub_Requirement;
import com.recruit.requirement.service.Sub_Vendor;

@Service
public class WriteExcel {

  private WritableCellFormat timesBoldUnderline;
  private WritableCellFormat timesBoldColor;
  private WritableCellFormat times;
  private String inputFile;
  @Autowired
	CandidateService candidateService;

	@Autowired
	ClientService clientService;

	public void setOutputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	private void createLabel(WritableSheet sheet, Boolean color) throws WriteException {
	    // Lets create a times font
	    WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
	    // Define the cell format
	    times = new WritableCellFormat(times10pt);
	    // Lets automatically wrap the cells
	    times.setWrap(true);
	
	    // Create create a bold font with underlines
	    WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
	    if(color)
	    	times10ptBoldUnderline.setColour(Colour.LIGHT_BLUE);
	    
	    timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
	    // Lets automatically wrap the cells
	    timesBoldUnderline.setWrap(true);
	    
	    CellView cv = new CellView();
	    cv.setFormat(times);
	    cv.setFormat(timesBoldUnderline);
	    cv.setAutosize(true);
	    cv.setSize(30);
	}

	private void addColorCaption(WritableSheet sheet, int column, int row, Colour colour, String s)throws RowsExceededException, WriteException {
		WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
		times10ptBoldUnderline.setColour(Colour.BLACK);
		timesBoldColor = new WritableCellFormat(times10ptBoldUnderline);
		timesBoldColor.setBackground(colour);
		Label label;
		label = new Label(column, row, s, timesBoldColor);
		sheet.addCell(label);
	}

	private void addLabel(WritableSheet sheet, int column, int row, String s)throws WriteException, RowsExceededException {
		Label label;
		label = new Label(column, row, s, times);
		sheet.addCell(label);
	}

  	public void writeRequirments(List<Requirement> requirements, String[] columns) throws IOException, WriteException {
	    File file = new File(inputFile);
	    WorkbookSettings wbSettings = new WorkbookSettings();
	    wbSettings.setLocale(new Locale("en", "EN"));
	    WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	    workbook.createSheet("Salary", 0);
	    WritableSheet excelSheet = workbook.getSheet(0);
	    createLabel(excelSheet,false);
	    createRequirments(excelSheet,requirements, columns);
	    workbook.write();
	    workbook.close();
	  }

	private void createRequirments(WritableSheet sheet,List<Requirement> requirements, String[] column_width) throws WriteException, RowsExceededException {
		String[] columns = new String[column_width.length];
		String[] columnWidths = new String[column_width.length];
		for (int i = 0; i < column_width.length; i++) {
			columns[i] = column_width[i].split(":")[0];
			columnWidths[i] = column_width[i].split(":")[1];
		}

		int row = 0,column=0;
		for (int i = 0; i < columns.length; i++,column++) {
			addColorCaption(sheet, column, row,Colour.VERY_LIGHT_YELLOW, columns[i]);
		}
		
		for (Requirement requirement : requirements) {
			column = 0;
			String vendorName = "", candidateName = "", contactName = "",submittedDate="";
			if(requirement.getSub_Requirements() != null && requirement.getSub_Requirements().size() > 0){
				for (Sub_Requirement sub_Requirement : requirement.getSub_Requirements()) {
					if (sub_Requirement.getCandidateId()!= null){						
						Candidate candidate  = candidateService.findCandidateName(sub_Requirement.getCandidateId());
						candidateName += candidate.getFirstName()+' '+candidate.getLastName()+",";
						if (sub_Requirement.getSubmittedDate()!= null)
							submittedDate += candidate.getFirstName()+' '+candidate.getLastName() +"-"+ new SimpleDateFormat("MM/dd/yyyy").format(sub_Requirement.getSubmittedDate())+",";
					}
				}
			}
			if(requirement.getSub_Vendors() != null && requirement.getSub_Vendors().size() > 0){
				for (Sub_Vendor sub_Vendor : requirement.getSub_Vendors()) {
					if (sub_Vendor.getVendorId()!= null){
						Client vendor = clientService.findVendorName(sub_Vendor.getVendorId());
						vendorName += vendor.getName()+",";
					}
					if (sub_Vendor.getContactId()!= null){
						Contact contact = clientService.findContactName(sub_Vendor.getContactId());
						contactName += contact.getFirstName()+' '+contact.getLastName()+",";
					}
				}
			}
			
			if (Arrays.asList(columns).contains("Hotness"))
				addLabel(sheet, column, ++row, requirement.getHotness());
			else{
				--column;
				++row;
			}
			if (Arrays.asList(columns).contains("Unique Id"))
				addLabel(sheet, ++column, row, requirement.getId().toString());
			if (Arrays.asList(columns).contains("Job Opening Status"))
				addLabel(sheet, ++column, row, requirement.getJobOpeningStatus());
			if (Arrays.asList(columns).contains("Posting Date")){
				if (requirement.getPostingDate() != null) 
					addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(requirement.getPostingDate()));
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Posting Title"))
				addLabel(sheet, ++column, row, requirement.getPostingTitle());
			if (Arrays.asList(columns).contains("Location"))
				addLabel(sheet, ++column, row, requirement.getLocation());
			if (Arrays.asList(columns).contains("Alert"))
				addLabel(sheet, ++column, row, requirement.getAddInfo_Todos());
			if (Arrays.asList(columns).contains("Client")){
				if (requirement.getClient() != null)
					addLabel(sheet, ++column, row, requirement.getClient().getName());
				else
					addLabel(sheet, ++column, row, "");
			}
			if (Arrays.asList(columns).contains("Vendor"))
				addLabel(sheet, ++column, row, vendorName);
			if (Arrays.asList(columns).contains("Contact Person"))
				addLabel(sheet, ++column, row, contactName);
			if (Arrays.asList(columns).contains("Roles and Responsibilities"))
				addLabel(sheet, ++column, row, requirement.getRequirement());
			if (Arrays.asList(columns).contains("Candidate"))
				addLabel(sheet, ++column, row, candidateName);
			if (Arrays.asList(columns).contains("Submitted Date"))
				addLabel(sheet, ++column, row, submittedDate);
			if (Arrays.asList(columns).contains("Skill Set"))
				addLabel(sheet, ++column, row, requirement.getSkillSet());
			if (Arrays.asList(columns).contains("Resume")){
				if (requirement.getResume() != null) 
					addLabel(sheet, ++column, row, requirement.getResume().toString());
				else
					addLabel(sheet, ++column, row, "");
			}
			if (Arrays.asList(columns).contains("Module"))
				addLabel(sheet, ++column, row, requirement.getModule());
			if (Arrays.asList(columns).contains("Employer"))
				addLabel(sheet, ++column, row, requirement.getEmployer());
			if (Arrays.asList(columns).contains("Comments"))
				addLabel(sheet, ++column, row, requirement.getComments());
			if (Arrays.asList(columns).contains("Public Link"))
				addLabel(sheet, ++column, row, requirement.getPublicLink());
			if (Arrays.asList(columns).contains("Source"))
				addLabel(sheet, ++column, row, requirement.getSource());
			if (Arrays.asList(columns).contains("Max Bill Rate"))
				addLabel(sheet, ++column, row, requirement.getRateSubmitted());
			if (Arrays.asList(columns).contains("Communication"))
				addLabel(sheet, ++column, row, requirement.getCommunication());
			if (Arrays.asList(columns).contains("Last Updated User"))
				addLabel(sheet, ++column, row, requirement.getLastUpdatedUser());
			if (Arrays.asList(columns).contains("Created"))
				addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(requirement.getCreated()));
			if (Arrays.asList(columns).contains("Last Updated"))
				addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(requirement.getLastUpdated()));
			
			for (int i = 0; i < columnWidths.length; i++) {
				sheet.setColumnView(i, Integer.parseInt(columnWidths[i])/4);	
			}
		}
	}

	public void writeCandidates(List<Candidate> candidates, String[] columns) throws IOException, WriteException {
	    File file = new File(inputFile);
	    WorkbookSettings wbSettings = new WorkbookSettings();
	    wbSettings.setLocale(new Locale("en", "EN"));
	    WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	    workbook.createSheet("Salary", 0);
	    WritableSheet excelSheet = workbook.getSheet(0);
	    createLabel(excelSheet,false);
	    createCandidates(excelSheet,candidates, columns);
	    workbook.write();
	    workbook.close();
	}

	private void createCandidates(WritableSheet sheet, List<Candidate> candidates, String[] column_width) throws RowsExceededException, WriteException {
		int row = 0,column=0;
		String[] columns = new String[column_width.length];
		String[] columnWidths = new String[column_width.length];
		for (int i = 0; i < column_width.length; i++) {
			columns[i] = column_width[i].split(":")[0];
			columnWidths[i] = column_width[i].split(":")[1];
		}
		
		for (int i = 0; i < columns.length; i++,column++) {
			addColorCaption(sheet, column, row,Colour.VERY_LIGHT_YELLOW, columns[i]);
		}
		
		for (Candidate candidate : candidates) {
			column = 0;
			++row;
			if (candidate.getPriority() != null && Arrays.asList(columns).contains("Priority")) 
				addLabel(sheet, column, row, candidate.getPriority().toString());
			else
				--column;
			
			if (Arrays.asList(columns).contains("Marketing Status"))
				addLabel(sheet, ++column, row, candidate.getMarketingStatus());
			if (Arrays.asList(columns).contains("Unique Id"))
				addLabel(sheet, ++column, row, candidate.getId().toString());
			if (Arrays.asList(columns).contains("First Name"))
				addLabel(sheet, ++column, row, candidate.getFirstName());
			if (Arrays.asList(columns).contains("Last Name"))
				addLabel(sheet, ++column, row, candidate.getLastName());
			if (Arrays.asList(columns).contains("Certifications"))
				addLabel(sheet, ++column, row, candidate.getAllCertifications());
			if (Arrays.asList(columns).contains("Skill Set"))
				addLabel(sheet, ++column, row, candidate.getSkillSet());
			if (Arrays.asList(columns).contains("Alert"))
				addLabel(sheet, ++column, row, candidate.getAlert());
			if (Arrays.asList(columns).contains("Email Id"))
				addLabel(sheet, ++column, row, candidate.getEmailId());
			if (Arrays.asList(columns).contains("Contact No"))
				addLabel(sheet, ++column, row, candidate.getContactNumber());
			if (Arrays.asList(columns).contains("Availability")) {
				if (candidate.getAvailability() != null) 
					addLabel(sheet, ++column, row, new SimpleDateFormat("MM/dd/yyyy").format(candidate.getAvailability()));	
				else
					++column;
			}
			if (Arrays.asList(columns).contains("P1 Comments"))
				addLabel(sheet, ++column, row, candidate.getP1comments());
			if (Arrays.asList(columns).contains("P2 Comments"))
				addLabel(sheet, ++column, row, candidate.getP2comments());
			if (Arrays.asList(columns).contains("Source"))
				addLabel(sheet, ++column, row, candidate.getSource());
			if (Arrays.asList(columns).contains("Referral"))
				addLabel(sheet, ++column, row, candidate.getReferral());
			if (Arrays.asList(columns).contains("Expected Rate"))
				addLabel(sheet, ++column, row, candidate.getExpectedRate());
			if (Arrays.asList(columns).contains("Rate From")) {
				if (candidate.getRateFrom() != null && Arrays.asList(columns).contains("Rate From"))
					addLabel(sheet, ++column, row, candidate.getRateFrom().toString());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Rate To")) {
				if (candidate.getRateTo()  != null && Arrays.asList(columns).contains("Rate To"))
					addLabel(sheet, ++column, row, candidate.getRateTo().toString());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Reloc?")){
				if (candidate.getRelocate())
					addLabel(sheet, ++column, row, "Yes");
				else
					addLabel(sheet, ++column, row, "No");
			}
			if (Arrays.asList(columns).contains("Trvl?")){
				if (candidate.getTravel())
					addLabel(sheet, ++column, row, "Yes");
				else
					addLabel(sheet, ++column, row, "No");
			}
			if (Arrays.asList(columns).contains("Open To CTH"))
				addLabel(sheet, ++column, row, candidate.getOpenToCTH());
			if (Arrays.asList(columns).contains("City & State"))
				addLabel(sheet, ++column, row, candidate.getCityAndState());
			if (Arrays.asList(columns).contains("Follow Up"))
				addLabel(sheet, ++column, row, candidate.getFollowUp());
			if (Arrays.asList(columns).contains("Employment Type"))
				addLabel(sheet, ++column, row, candidate.getEmploymentType());
			if (Arrays.asList(columns).contains("About Partner"))
				addLabel(sheet, ++column, row, candidate.getAboutPartner());
			if (Arrays.asList(columns).contains("Personality"))
				addLabel(sheet, ++column, row, candidate.getPersonality());
			if (Arrays.asList(columns).contains("Contact Address"))
				addLabel(sheet, ++column, row, candidate.getContactAddress());
			if (Arrays.asList(columns).contains("Start Date")) {
				if (candidate.getStartDate() != null ) 
					addLabel(sheet, ++column, row, new SimpleDateFormat("MM/dd/yyyy").format(candidate.getStartDate()));	
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Submitted Date")) {
				if (candidate.getSubmittedDate() != null ) 
					addLabel(sheet, ++column, row, new SimpleDateFormat("MM/dd/yyyy").format(candidate.getSubmittedDate()));	
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Education"))
				addLabel(sheet, ++column, row, candidate.getEducation());
			if (Arrays.asList(columns).contains("Immigration Status"))
				addLabel(sheet, ++column, row, candidate.getImmigrationStatus());
			if (Arrays.asList(columns).contains("Type"))
				addLabel(sheet, ++column, row, candidate.getType());
			if (Arrays.asList(columns).contains("Current Job Title"))
				addLabel(sheet, ++column, row, candidate.getCurrentJobTitle());
			if (Arrays.asList(columns).contains("Employer"))
				addLabel(sheet, ++column, row, candidate.getEmployer());
			if (Arrays.asList(columns).contains("Highest Qualification Held"))
				addLabel(sheet, ++column, row, candidate.getHighestQualification());
			if (Arrays.asList(columns).contains("Last Updated User"))
				addLabel(sheet, ++column, row, candidate.getLastUpdatedUser());
			if (Arrays.asList(columns).contains("Created")) {
				if (candidate.getCreated() != null ) 
					addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(candidate.getCreated()));	
				else
					++column;
			}
			if (candidate.getLastUpdated() != null && Arrays.asList(columns).contains("Last Updated")) 
				addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(candidate.getLastUpdated()));
		}
		
		for (int i = 0; i < columnWidths.length; i++) {
			sheet.setColumnView(i, Integer.parseInt(columnWidths[i])/4);	
		}
	}

  	public void writeContacts(List<Contact> contacts, String[] columns) throws IOException, WriteException {
	    File file = new File(inputFile);
	    WorkbookSettings wbSettings = new WorkbookSettings();
	    wbSettings.setLocale(new Locale("en", "EN"));
	    WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	    workbook.createSheet("Salary", 0);
	    WritableSheet excelSheet = workbook.getSheet(0);
	    createLabel(excelSheet,false);
	    createContacts(excelSheet,contacts, columns);
	    workbook.write();
	    workbook.close();
	  }

	private void createContacts(WritableSheet sheet,List<Contact> contacts, String[] column_width) throws RowsExceededException, WriteException {
		String[] columns = new String[column_width.length];
		String[] columnWidths = new String[column_width.length];
		for (int i = 0; i < column_width.length; i++) {
			columns[i] = column_width[i].split(":")[0];
			columnWidths[i] = column_width[i].split(":")[1];
		}

		int row = 0,column=0;
		for (int i = 0; i < columns.length; i++,column++) {
			addColorCaption(sheet, column, row,Colour.VERY_LIGHT_YELLOW, columns[i]);
		}
		
		for (Contact contact : contacts) {
			column = 0;
			if (Arrays.asList(columns).contains("Unique Id"))
				addLabel(sheet, column, ++row, contact.getId().toString());
			else{
				--column;
				++row;
			}
			if (Arrays.asList(columns).contains("First Name"))
				addLabel(sheet, ++column, row, contact.getFirstName());
			if (Arrays.asList(columns).contains("Last Name"))
				addLabel(sheet, ++column, row, contact.getLastName());
			if (Arrays.asList(columns).contains("Client/Vendor")){
				if (contact.getClient() != null) 
					addLabel(sheet, ++column, row, contact.getClient().getName());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Type")){
				if (contact.getClient() != null) 
					addLabel(sheet, ++column, row, contact.getClient().getType());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("City")){
				if (contact.getClient() != null) 
					addLabel(sheet, ++column, row, contact.getClient().getCity());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("State")){
				if (contact.getClient() != null) 
					addLabel(sheet, ++column, row, contact.getClient().getState());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Score")){
				if (contact.getScore() != null)
					addLabel(sheet, ++column, row, contact.getScore().toString());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Email"))
				addLabel(sheet, ++column, row, contact.getEmail());
			if (Arrays.asList(columns).contains("Job Title"))
				addLabel(sheet, ++column, row, contact.getJobTitle());
			if (Arrays.asList(columns).contains("Internet Link"))
				addLabel(sheet, ++column, row, contact.getInternetLink());
			if (Arrays.asList(columns).contains("CellPhone"))
				addLabel(sheet, ++column, row, contact.getCellPhone());
			if (Arrays.asList(columns).contains("Extension"))
				addLabel(sheet, ++column, row, contact.getExtension());
			if (Arrays.asList(columns).contains("Comments"))
				addLabel(sheet, ++column, row, contact.getComments());
			if (Arrays.asList(columns).contains("Interviewer")){
				if (contact.getInterviews().size() > 0)
					addLabel(sheet, ++column, row, "Yes");
				else
					addLabel(sheet, ++column, row, "No");
			}
			if (Arrays.asList(columns).contains("Last Updated User"))
				addLabel(sheet, ++column, row, contact.getLastUpdatedUser());
			if (Arrays.asList(columns).contains("Created")){
				if (contact.getCreated() != null) 
					addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(contact.getCreated()));	
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Last Updated"))
				addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(contact.getLastUpdated()));
		}
		
		for (int i = 0; i < columnWidths.length; i++) {
			sheet.setColumnView(i, Integer.parseInt(columnWidths[i])/5);	
		}
	}

	public void writeClients(List<Client> clients, String[] columns) throws IOException, WriteException {
	    File file = new File(inputFile);
	    WorkbookSettings wbSettings = new WorkbookSettings();
	    wbSettings.setLocale(new Locale("en", "EN"));
	    WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	    workbook.createSheet("Salary", 0);
	    WritableSheet excelSheet = workbook.getSheet(0);
	    createLabel(excelSheet,false);
	    createClients(excelSheet,clients,columns);
	    workbook.write();
	    workbook.close();
	}

	private void createClients(WritableSheet sheet, List<Client> clients, String[] column_width) throws RowsExceededException, WriteException {
		String[] columns = new String[column_width.length];
		String[] columnWidths = new String[column_width.length];
		for (int i = 0; i < column_width.length; i++) {
			columns[i] = column_width[i].split(":")[0];
			columnWidths[i] = column_width[i].split(":")[1];
		}

		int row = 0,column=0;
		for (int i = 0; i < columns.length; i++,column++) {
			addColorCaption(sheet, column, row,Colour.VERY_LIGHT_YELLOW, columns[i]);
		}
		
		for (Client client : clients) {
			String contractorName = "", fullTimeName = "";
			column = 0;
			if (Arrays.asList(columns).contains("Unique Id"))
				addLabel(sheet, column, ++row, client.getId().toString());
			else{
				--column;
				++row;
			}
			if (Arrays.asList(columns).contains("Name"))
				addLabel(sheet, ++column, row, client.getName());
			if (Arrays.asList(columns).contains("Type"))
				addLabel(sheet, ++column, row, client.getType());
			if (Arrays.asList(columns).contains("Score")){
				if (client.getScore() != null)
					addLabel(sheet, ++column, row, client.getScore().toString());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("City"))
				addLabel(sheet, ++column, row, client.getCity());
			if (Arrays.asList(columns).contains("State"))
				addLabel(sheet, ++column, row, client.getState());
			if (Arrays.asList(columns).contains("Address"))
				addLabel(sheet, ++column, row, client.getAddress());
			if (Arrays.asList(columns).contains("Comments"))
				addLabel(sheet, ++column, row, client.getComments());
			if (Arrays.asList(columns).contains("Contractors")){
				if (client.getContractorId() != null && !client.getContractorId().equalsIgnoreCase("")) {
					List<Candidate> contractors = candidateService.findByIds(client.getContractorId());
					for (Candidate contractor : contractors) {
						contractorName += contractor.getFirstName()+' '+contractor.getLastName()+',';
					}
				}
				addLabel(sheet, ++column, row, contractorName);
			}
			if (Arrays.asList(columns).contains("Full Time")){
				if (client.getFullTimeId() != null && !client.getFullTimeId().equalsIgnoreCase("")) {
					List<Candidate> fulltimes = candidateService.findByIds(client.getFullTimeId());
					for (Candidate contractor : fulltimes) {
						fullTimeName += contractor.getFirstName()+' '+contractor.getLastName()+',';
					}
				}
				addLabel(sheet, ++column, row, fullTimeName);
			}
			if (Arrays.asList(columns).contains("Contact Number"))
				addLabel(sheet, ++column, row, client.getContactNumber());
			if (Arrays.asList(columns).contains("Fax"))
				addLabel(sheet, ++column, row, client.getFax());
			if (Arrays.asList(columns).contains("Email"))
				addLabel(sheet, ++column, row, client.getEmail());
			if (Arrays.asList(columns).contains("Website"))
				addLabel(sheet, ++column, row, client.getWebsite());
			if (Arrays.asList(columns).contains("Industry"))
				addLabel(sheet, ++column, row, client.getIndustry());
			if (Arrays.asList(columns).contains("About"))
				addLabel(sheet, ++column, row, client.getAbout());
			if (Arrays.asList(columns).contains("Last Updated User"))
				addLabel(sheet, ++column, row, client.getLastUpdatedUser());
			if (Arrays.asList(columns).contains("Created")){
				if (client.getCreated() != null) 
					addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(client.getCreated()));	
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Last Updated")){
				if (client.getLastUpdated() != null)
					addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(client.getLastUpdated()));
			}
		}

		for (int i = 0; i < columnWidths.length; i++) {
			sheet.setColumnView(i, Integer.parseInt(columnWidths[i])/5);	
		}
	}

	public void writePlacements(List<PlacementLeads> leads, String[] columns) throws WriteException, IOException {
	    File file = new File(inputFile);
	    WorkbookSettings wbSettings = new WorkbookSettings();
	    wbSettings.setLocale(new Locale("en", "EN"));
	    WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	    workbook.createSheet("Salary", 0);
	    WritableSheet excelSheet = workbook.getSheet(0);
	    createLabel(excelSheet,false);
	    createPlacements(excelSheet,leads,columns);
	    workbook.write();
	    workbook.close();
	}

	private void createPlacements(WritableSheet sheet, List<PlacementLeads> leads, String[] column_width) throws RowsExceededException, WriteException {
		int row = 0,column=0;
		String[] columns = new String[column_width.length];
		String[] columnWidths = new String[column_width.length];
		for (int i = 0; i < column_width.length; i++) {
			columns[i] = column_width[i].split(":")[0];
			columnWidths[i] = column_width[i].split(":")[1];
		}
		for (int i = 0; i < columns.length; i++,column++) {
			addColorCaption(sheet, column, row,Colour.VERY_LIGHT_YELLOW, columns[i]);
		}
		
		for (PlacementLeads placement : leads) {
			column = 0;
			if (Arrays.asList(columns).contains("Date")){
				if (placement.getDate() != null )
					addLabel(sheet, column, ++row,  new SimpleDateFormat("MM/dd/yyyy").format(placement.getDate()));
				else
					++row;
			}else{
				--column;
				++row;
			}
			if (Arrays.asList(columns).contains("Source of Information"))
				addLabel(sheet, ++column, row, placement.getSourceOfInformation());
			if (Arrays.asList(columns).contains("Source Sublevel")){
				if (placement.getSource_Sublevel() != null) 
					addLabel(sheet, ++column, row, new SimpleDateFormat("MM/dd/yyyy").format(placement.getSource_Sublevel()));
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Priority")){
				if (placement.getPriority() != null) 
					addLabel(sheet, ++column, row, placement.getPriority().toString());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Type of Information"))
				addLabel(sheet, ++column, row, placement.getTypeOfInformation());
			if (Arrays.asList(columns).contains("Information"))
				addLabel(sheet, ++column, row, placement.getInformation());
			if (Arrays.asList(columns).contains("Hospital / Entity"))
				addLabel(sheet, ++column, row, placement.getHospital_Entity());
			if (Arrays.asList(columns).contains("Location"))
				addLabel(sheet, ++column, row, placement.getLocation());
			if (Arrays.asList(columns).contains("Go Live Date"))
				addLabel(sheet, ++column, row, placement.getLiveDate());
			if (Arrays.asList(columns).contains("Link 1"))
				addLabel(sheet, ++column, row, placement.getLink1());
			if (Arrays.asList(columns).contains("Link 2"))
				addLabel(sheet, ++column, row, placement.getLink2());
			if (Arrays.asList(columns).contains("Outreach"))
				addLabel(sheet, ++column, row, placement.getOutreach());
			if (Arrays.asList(columns).contains("Comments"))
				addLabel(sheet, ++column, row, placement.getComments());
			if (Arrays.asList(columns).contains("Person"))
				addLabel(sheet, ++column, row, placement.getPerson());
			if (Arrays.asList(columns).contains("Last Updated User"))
				addLabel(sheet, ++column, row, placement.getLastUpdatedUser());
			if (Arrays.asList(columns).contains("Created")){
				if (placement.getCreated() != null) 
					addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(placement.getCreated()));	
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Last Updated")){
				if (placement.getLastUpdated() != null)
					addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(placement.getLastUpdated()));
			}
		}
		
		for (int i = 0; i < columnWidths.length; i++) {
			sheet.setColumnView(i, Integer.parseInt(columnWidths[i])/4);	
		}
	}

	public void writeInterviews(List<Interview> interviews, String[] columns) throws IOException, WriteException {
	    File file = new File(inputFile);
	    WorkbookSettings wbSettings = new WorkbookSettings();
	    wbSettings.setLocale(new Locale("en", "EN"));
	    WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	    workbook.createSheet("Salary", 0);
	    WritableSheet excelSheet = workbook.getSheet(0);
	    createLabel(excelSheet,false);
	    createInterviews(excelSheet,interviews,columns);
	    workbook.write();
	    workbook.close();
	}

	private void createInterviews(WritableSheet sheet,List<Interview> interviews, String[] column_width) throws RowsExceededException, WriteException {
		int row = 0,column=0;
		String[] columns = new String[column_width.length];
		String[] columnWidths = new String[column_width.length];
		for (int i = 0; i < column_width.length; i++) {
			columns[i] = column_width[i].split(":")[0];
			columnWidths[i] = column_width[i].split(":")[1];
		}

		for (int i = 0; i < columns.length; i++,column++) {
			addColorCaption(sheet, column, row,Colour.VERY_LIGHT_YELLOW, columns[i]);
		}
		for (Interview interview: interviews) {
			column = 0;
			if (Arrays.asList(columns).contains("Client")) {
				if (interview.getRequirement() != null)
					addLabel(sheet, column, ++row, interview.getRequirement().getId().toString());
				else{
					++row;
				}
			}else {
				--column;
				++row;
			}
			if (Arrays.asList(columns).contains("Candidate")){
				if (interview.getCandidate() != null) 
					addLabel(sheet, ++column, row, interview.getCandidate().getFirstName()+' '+interview.getCandidate().getLastName());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Client")){
				if (interview.getClient() != null)
					addLabel(sheet, ++column, row, interview.getClient().getName());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Vendor")){
				if (interview.getVendor() != null)
					addLabel(sheet, ++column, row, interview.getVendor().getName());
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Interviewer")){
				addLabel(sheet, ++column, row, interview.getContactId());
			}
			
			if (Arrays.asList(columns).contains("Interview Date")){
				if (interview.getInterviewDate() != null) 
					addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(interview.getInterviewDate()));	
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Time")){
				if (interview.getTime() != null) 
					addLabel(sheet, ++column, row, interview.getTime().toString());	
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Comments"))
				addLabel(sheet, ++column, row, interview.getComments());
			if (Arrays.asList(columns).contains("Last Updated User"))
				addLabel(sheet, ++column, row, interview.getLastUpdatedUser());
			if (Arrays.asList(columns).contains("Created")){
				if (interview.getCreated() != null) 
					addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(interview.getCreated()));	
				else
					++column;
			}
			if (Arrays.asList(columns).contains("Last Updated")){
				if (interview.getLastUpdated() != null)
					addLabel(sheet, ++column, row,  new SimpleDateFormat("MM/dd/yyyy").format(interview.getLastUpdated()));
			}
		}
		for (int i = 0; i < columnWidths.length; i++) {
			sheet.setColumnView(i, Integer.parseInt(columnWidths[i])/5);	
		}		
	}

	@SuppressWarnings("rawtypes")
	public void writeAvailCandidates(List list) throws IOException, WriteException {
	    File file = new File(inputFile);
	    WorkbookSettings wbSettings = new WorkbookSettings();
	    wbSettings.setLocale(new Locale("en", "EN"));
	    WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	    workbook.createSheet("Salary", 0);
	    WritableSheet excelSheet = workbook.getSheet(0);
	    createLabel(excelSheet,false);
	    createReport(excelSheet,list);
	    workbook.write();
	    workbook.close();
	}

	@SuppressWarnings("rawtypes")
	private void createReport(WritableSheet sheet, List list) throws RowsExceededException, WriteException {
		int row = 0,column=0;
		
		addColorCaption(sheet, column, row,Colour.VERY_LIGHT_YELLOW, "Candidate Id");
		addColorCaption(sheet, ++column, row,Colour.VERY_LIGHT_YELLOW, "Certifications");
		addColorCaption(sheet, ++column, row,Colour.VERY_LIGHT_YELLOW, "Skillset");
		addColorCaption(sheet, ++column, row,Colour.VERY_LIGHT_YELLOW, "Availability");
		addColorCaption(sheet, ++column, row,Colour.VERY_LIGHT_YELLOW, "Rate");
		addColorCaption(sheet, ++column, row,Colour.VERY_LIGHT_YELLOW, "Reloc?");
		addColorCaption(sheet, ++column, row,Colour.VERY_LIGHT_YELLOW, "Trvl?");
		addColorCaption(sheet, ++column, row,Colour.VERY_LIGHT_YELLOW, "City & State");
		addColorCaption(sheet, ++column, row,Colour.VERY_LIGHT_YELLOW, "Highest Qualification");
		
		
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			column = 0;
			row++;
			Object[] objects = (Object[]) iterator.next();
			for (int i = 0; i < objects.length; i++ , column++) {
				if(objects[i] != null)
					addLabel(sheet, column, row,  objects[i].toString());
			}
		}

		for (int i = 0; i <= 10; i++) {
			sheet.setColumnView(i, 15);	
		}
		sheet.setColumnView(1, 35);
		sheet.setColumnView(2, 35);
	}

}