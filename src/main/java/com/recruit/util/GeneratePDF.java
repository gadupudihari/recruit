package com.recruit.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class GeneratePDF extends PdfPageEventHelper {
	
	
  private static Font timesBold18 = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
  private static Font times12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);
  
  private static Font timesBold16 = new Font(Font.FontFamily.TIMES_ROMAN, 16,Font.BOLD);
  private static Font times10 = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.NORMAL);
  
  protected static com.itextpdf.text.Font arial9,arial10,arial20,ar;

  public Phrase leftHeader;
  public Phrase rightHeader;
  public Phrase footer;
  int pagenumber;
  
  //ads empty line to the document
  public static void addEmptyLine(Paragraph paragraph, int number) {
    for (int i = 0; i < number; i++) {
      paragraph.add(new Paragraph(" "));
    }
  }

  //Creates a pdf file 
  public static void createFile(String inputFile) {
	  try {
		  Document document = new Document();
		  PdfWriter.getInstance(document, new FileOutputStream(inputFile));
	  } catch (FileNotFoundException e) {
			e.printStackTrace();
	  } catch (DocumentException e) {
			e.printStackTrace();
	  }
  }

  public Font createFont(String contextPath,String imagePath,int fontSize){
	  Font font = new Font();
	  try {
		  font = new Font(BaseFont.createFont(contextPath+imagePath,BaseFont.WINANSI, BaseFont.EMBEDDED),fontSize);
	  } catch (DocumentException e) {
		  e.printStackTrace();
	  } catch (IOException e) {
		  e.printStackTrace();
	  }
	  return font;
  }
  
  public void onStartPage(PdfWriter writer, Document document) {
      pagenumber++;
  }
  
  public GeneratePDF(String leftHeaderText,String rightHeaderText){
	  leftHeader = new Phrase(leftHeaderText);
	  rightHeader = new Phrase(rightHeaderText);
	  
  }

  public GeneratePDF(){}
  
  
  public void onEndPage(PdfWriter writer, Document document) {
	  PdfContentByte cb = writer.getDirectContent();
	  ColumnText.showTextAligned(cb, Element.ALIGN_LEFT, leftHeader,document.left(), document.top() + 10, 0);
	  ColumnText.showTextAligned(cb, Element.ALIGN_LEFT, rightHeader,document.right()-180, document.top() + 10, 0);
	  ColumnText.showTextAligned(writer.getDirectContent(),Element.ALIGN_CENTER, new Phrase(String.format("Page %d", pagenumber)),
              (document.left() + document.right()) / 2, document.bottom() - 18, 0);
  }

  //prints monthly totals of bank data 
  public static void printTotals(PdfPTable table,String accType,String total,BigDecimal total1,BigDecimal total2, String contextPath,BaseColor backGroundColor ){
	  
	  Font arial9Bold = FontFactory.getFont("Arial", 10, Font.BOLD);
	  PdfPCell cell = new PdfPCell(new Phrase(total, arial9Bold));
	  cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	  cell.setBackgroundColor(backGroundColor);
	  if (accType.equalsIgnoreCase("Checking")) {
		  cell.setColspan(3);
		  table.addCell(cell);
		  cell = new PdfPCell(new Phrase(UtilityHelper.toCurrency(total1), arial9Bold));
		  cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  cell.setBackgroundColor(backGroundColor);
		  table.addCell(cell);
		  cell = new PdfPCell(new Phrase(UtilityHelper.toCurrency(total2), arial9Bold));
		  cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  cell.setBackgroundColor(backGroundColor);
		  table.addCell(cell);
	  }else{
		  cell.setColspan(5);
		  table.addCell(cell);
		  cell = new PdfPCell(new Phrase(UtilityHelper.toCurrency(total1), arial9Bold));
		  cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  cell.setBackgroundColor(backGroundColor);
		  table.addCell(cell);
	  }
	  
  }
  
  //adds table heading with left align, gray background and arial 10 font
  public static PdfPCell tableLeftHeading(String heading,String contextPath){
	    try {
			arial10 = new Font (BaseFont.createFont(contextPath+"/images/arial.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED), 10);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	    PdfPCell c1 = new PdfPCell(new Phrase(10,heading,arial10));
	    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
	    c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    c1.setFixedHeight(20);
	    return c1;
}
  
  
  //adds table heading with left align, gray background and arial font and given font size
  public static PdfPCell tableHeading(String heading,String contextPath,int fontSize){
	    try {
			arial10 = new Font (BaseFont.createFont(contextPath+"/images/arial.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED), fontSize);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	    PdfPCell c1 = new PdfPCell(new Phrase(10,heading,arial10));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    c1.setFixedHeight(20);
	    return c1;
  }
  
  //add a cell to table with fixed height 20 ,align center and times 10 font
  public static PdfPCell tableCell(String value) {
	    PdfPCell c1 = new PdfPCell(new Phrase(10,value,times10));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setFixedHeight(20);
	    return c1;
  }
  
  //add a cell to table with fixed height 20 ,align right and times 10 font
  public static PdfPCell tableRightCell(String value,Font font) {
	    PdfPCell c1 = new PdfPCell(new Phrase(10,value,font));
	    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    return c1;
  }

  //add a cell to table with fixed height 20 ,align left and times 10 font
  public static PdfPCell tableLeftCell(String value,Font font) {
	    PdfPCell c1 = new PdfPCell(new Phrase(10,value,font));
	    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
	    return c1;
  }

  //add a cell to table with align right and given font and increases row height
  public static PdfPCell tableRightCell(String value,Font font, float rowHeight) {
	    PdfPCell c1 = new PdfPCell(new Phrase(10,value,font));
	    c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    c1.setFixedHeight(rowHeight+25);
	    return c1;
  }

  //add a cell to table with align left and given font and increases row height
  public static PdfPCell tableLeftCell(String value,Font font, float rowHeight) {
	    PdfPCell c1 = new PdfPCell(new Phrase(10,value,font));
	    c1.setHorizontalAlignment(Element.ALIGN_LEFT);
	    c1.setFixedHeight(rowHeight+25);
	    return c1;
  }
  
  public void tableLayout(PdfPTable table, float[][] width, float[] height,
          int rowStart, PdfContentByte[] canvas) {
      float widths[] = width[0];
      float x1 = widths[0];
      float x2 = widths[widths.length - 1];
      float y1 = height[0];
      float y2 = height[height.length - 1];
      PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
      cb.rectangle(x1, y1, x2 - x1, y2 - y1);
      cb.stroke();
      cb.resetRGBColorStroke();
  }

  public static void addEmailData(String json, String contextPath, String fileName) {
	  
	    Paragraph preface = new Paragraph();
	    try {
			arial10 = new Font (BaseFont.createFont(contextPath+"/images/arial.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED), 10);
			arial20 = new Font (BaseFont.createFont(contextPath+"/images/arial.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED), 20);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	    System.out.println(fileName);
		try {
		    
			Document document = new Document(PageSize.LETTER);
			
			PdfWriter.getInstance(document,new FileOutputStream(fileName));
			document.open();
			document.addAuthor("Aj Recruit");
			document.addCreator("Aj Recruit");
			document.addSubject("Thanks for your support");
			document.addCreationDate();
			document.addTitle("Please read this");
			HTMLWorker htmlWorker = new HTMLWorker(document);
			htmlWorker.parse(new StringReader(json));
			htmlWorker.close();
			document.close();
			System.out.println("Done");
			
		} catch (Exception e) {
			e.printStackTrace();
		}

}

  
} 