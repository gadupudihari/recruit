package com.recruit.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.w3c.dom.Element;

import com.recruit.base.presentation.BaseController;
import com.recruit.base.service.Param;
import com.recruit.base.service.ParamParser;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.service.Candidate;
import com.recruit.candidate.service.CandidateService;
import com.recruit.client.service.Client;
import com.recruit.client.service.ClientService;
import com.recruit.client.service.Contact;
import com.recruit.interview.service.Interview;
import com.recruit.interview.service.InterviewService;
import com.recruit.reports.service.ReportsService;
import com.recruit.requirement.service.Requirement;
import com.recruit.requirement.service.Sub_Requirement;
import com.recruit.user.service.User;
import com.recruit.user.service.UserService;

@Controller
@RequestMapping("/reminder")
@PreAuthorize("hasAnyRole('ADMIN','ADMIN1')")
public class ReminderController extends BaseController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	ReportsService reportsService;
	
	@Autowired
	InterviewService interviewService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	CandidateService candidateService;
	
	@Autowired
	ClientService clientService;
	
	// alert id - 950 
	//send email when interview is I-Succeeded
	@RequestMapping(value = "/interviewSucceedEmail", method = {RequestMethod.POST,RequestMethod.GET })
	public String interviewSucceedEmail(@RequestParam("json") String json,  Map<String, Object> map,HttpSession session,HttpServletRequest request) throws JSONException {
		String loginId = UtilityHelper.getLoginId(request);
		Timestamp currentDate = UtilityHelper.getDate();

		String mailto="",mailtoRole="",mailtoignore="";
		Element eElement = UtilityHelper.readXmlFile("Interview Succeeded");
    	mailto = eElement.getElementsByTagName("mailto").item(0).getChildNodes().item(0).getNodeValue();
    	mailtoRole = eElement.getElementsByTagName("mailtoRole").item(0).getChildNodes().item(0).getNodeValue();
    	mailtoignore = eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue();

    	if (mailtoRole.contains(",")) 
    		mailtoRole = mailtoRole.replaceAll(",", "','");
    	mailtoRole = "'"+mailtoRole+"'";

		String[] submissionIds = json.split(",");
		for (String submissionId : submissionIds) {
			Sub_Requirement sub_Requirement = interviewService.findSub_RequirementById(Integer.parseInt(submissionId));
			Requirement requirement = sub_Requirement.getRequirement();
			Client client = sub_Requirement.getRequirement().getClient();
			Interview interview = interviewService.findBySubmissionId(Integer.parseInt(submissionId));

			Candidate candidate = candidateService.findById(sub_Requirement.getCandidateId());
			Client vendor = clientService.findById(sub_Requirement.getVendorId());
			Contact contact = clientService.findContactByJobVendor(sub_Requirement.getRequirement().getId(),sub_Requirement.getVendorId());

			//Delete emails exists for the same interview
			String query = "Delete from email where alertName ='Interview Succeeded' and date(alertDate) = date(now()) and refId = "+candidate.getId();
			reportsService.updateEmailRecords(query);

			String subject = candidate.getFirstName() +" " + candidate.getLastName()+ " Interview Succeeded";
			String body = candidate.getFirstName()+' '+candidate.getLastName()+ " interview is successful. The following are the details" ;
			if(vendor != null)
				body += "<br>Vendor : " +vendor.getName();
			if(client != null)
				body += "<br>Client : " +client.getName();
			if(requirement != null)
				body += "<br>Job ID : " +requirement.getId();
			if(contact != null)
				body += "<br>Interviewer : " +contact.getFirstName()+' '+contact.getLastName();
			if(interview != null && interview.getApproxStartDate() != null)
				body += "<br>Tentative Start date : " +new SimpleDateFormat("MM/dd/yyyy").format(interview.getApproxStartDate());
					
			if (mailtoRole != " ") {
				List<User> admins = userService.getEmailAdmin();
				for (User user : admins) {
					if (! mailtoignore.contains(user.getEmailId())){
						//here user id holds interview id and refid holds candidate id
						String emailQuery="insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,loginId,alertInDays,refId,application) " +
								"values('"+user.getEmailId()+"','" +currentDate.toString()+"','"+currentDate.toString()+"','"+subject+"','"+body+"',"+true+",'"+"Interview Succeeded"+"','"+
								"AD HOC"+"','"+loginId+"',"+0+",'"+candidate.getId()+"','recruit')";
					
						reportsService.updateEmailRecords(emailQuery);
					}
				}
			}else if (mailto != " ") {
				String emails[] = mailto.split(",");
				for (String emailId : emails) {
					if (! mailtoignore.contains(emailId)) {
						//here user id holds interview id and refid holds candidate id
						String emailQuery="insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,loginId,alertInDays,refId,application) " +
								"values('"+emailId+"','" +currentDate.toString()+"','"+currentDate.toString()+"','"+subject+"','"+body+"',"+true+",'"+"Interview Succeeded"+"','"+
								"AD HOC"+"','"+loginId+"',"+0+",'"+candidate.getId()+"','recruit')";
					
						reportsService.updateEmailRecords(emailQuery);
					}
				}
			}

		}

		
		return returnJson(map, response);
	}

	@RequestMapping(value = "/candidateUpdateRequest", method = {RequestMethod.POST,RequestMethod.GET })
	public String candidateUpdateRequest(@RequestParam("json") String json,  Map<String, Object> map,HttpSession session,HttpServletRequest request) {
		String loginId = UtilityHelper.getLoginId(request);
		Timestamp currentDate = UtilityHelper.getDate();
		
		SearchFilter filter = ParamParser.parse(json);
		Map<String, Param> filterMap = filter.getParamMap();
		String candidateId = filterMap.get("candidateId").getStrValue();
		String message = filterMap.get("message").getStrValue();

		Candidate candidate = candidateService.findById(Integer.parseInt(candidateId));
		
		
		String subject = "Requesting for the changes for the candidate "+candidate.getFirstName()+' '+candidate.getLastName() ;
		message = loginId+" has requested changes for the following fields, Please verify these and make changes" +
				"<br><br>New Data entered by "+loginId+message;
		
		List<User> admins = userService.getAJAdmins();
		
		String allEmailIds ="";
		for (User user : admins) {
			allEmailIds += user.getEmailId() +",";

			String emailQuery="insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,loginId,alertInDays,refId,application) " +
					"values('"+user.getEmailId()+"','" +currentDate.toString()+"','"+currentDate.toString()+"','"+subject+"','"+message+"',"+false+",'"
					+"Candidate Updation"+"','"+"AD HOC"+"','"+loginId+"',"+0+",'"+candidate.getEmployeeId()+"','timesheetz')";
		
			reportsService.updateEmailRecords(emailQuery);

		}
		
		SendEmails.sendHtmlEMail(subject, message, allEmailIds);
		return returnJson(map, response);

	}

		
	
	
}
