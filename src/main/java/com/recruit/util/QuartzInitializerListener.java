package com.recruit.util;


import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
public class QuartzInitializerListener implements ServletContextListener {
 public static final String QUARTZ_FACTORY_KEY = "org.quartz.impl.StdSchedulerFactory.KEY";
 
    private boolean performShutdown = true;
 
      private Scheduler scheduler = null;


       public void contextInitialized(ServletContextEvent sce) {
	  
          System.out.println("Quartz Initializer Servlet loaded, initializing Scheduler...");
 
	             ServletContext servletContext = sce.getServletContext();
	             StdSchedulerFactory factory;
	             try {
	     
	                 String configFile = servletContext.getInitParameter("config-file");
                 String shutdownPref = servletContext.getInitParameter("shutdown-on-unload");
     
                 if (shutdownPref != null) {
                     performShutdown = Boolean.valueOf(shutdownPref).booleanValue();
                 }
     
                 // get Properties
                 if (configFile != null) {
                     factory = new StdSchedulerFactory(configFile);
                 } else {
                     factory = new StdSchedulerFactory();
                 }
     
                 // Always want to get the scheduler, even if it isn't starting, 
                 // to make sure it is both initialized and registered.
                 scheduler = factory.getScheduler();
     
                 // Should the Scheduler being started now or later
                 String startOnLoad = servletContext
                         .getInitParameter("start-scheduler-on-load");

                 if (startOnLoad == null || (Boolean.valueOf(startOnLoad).booleanValue())) {
                     // Start now
                     scheduler.start();
                     System.out.println("Scheduler has been started...");
                 } else {
                     System.out.println("Scheduler has not been started. Use scheduler.start()");
                 }
     
                 String factoryKey = 
                     servletContext.getInitParameter("servlet-context-factory-key");
                 if (factoryKey == null) {
                     factoryKey = QUARTZ_FACTORY_KEY;
                 }
     
                 System.out.println("Storing the Quartz Scheduler Factory in the servlet context at key: "
                         + factoryKey);
                 servletContext.setAttribute(factoryKey, factory);
     
             } catch (Exception e) {
                 System.out.println("Quartz Scheduler failed to initialize: " + e.toString());
                 e.printStackTrace();
             }
         }
     
         public void contextDestroyed(ServletContextEvent sce) {
     
             if (!performShutdown) {
                 return;
             }
     
             try {
                 if (scheduler != null) {
                     scheduler.shutdown();
                 }
             } catch (Exception e) {
                 System.out.println("Quartz Scheduler failed to shutdown cleanly: " + e.toString());
                 e.printStackTrace();
             }
     
             System.out.println("Quartz Scheduler successful shutdown.");
         }
     
     
     }

