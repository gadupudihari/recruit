package com.recruit.util;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

public class IPLogger extends SavedRequestAwareAuthenticationSuccessHandler {
	
	
	@Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
   // use authentication object to fetch the user object and update its timestamp
		//getting the application path to place the log file
        ServletContext servletContext = request.getSession().getServletContext();
	    String contextPath = servletContext.getRealPath(File.separator);
	    logIpAddressLocation(contextPath,request.getRemoteAddr());
	    System.out.println("----->> Entered IPLogger onAuthenticationSuccess method........................");
	    try {
			response.sendRedirect(request.getContextPath() + "/home.jsp");
		} catch (IOException e) {
			e.printStackTrace();
		}
   }
	    
	    public void logIpAddressLocation(String contextPath,String remoteIp){
		    User user = (User) (SecurityContextHolder.getContext()).getAuthentication().getPrincipal();
		    System.out.println("---------Real Context Path---------------------"+contextPath);
			System.out.println("user name is-->"+user.getUsername());
			try
			{
		    	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date date = new Date();
				Writer output = null;  
				String location = getIPLocation(remoteIp); 
				  File file = new File(contextPath+"iplogging.log");
				  if(!file.exists()){
					  //Creating new file if the file doesn't exist
					  System.out.println("file doesnot exists");
					  file.createNewFile();
					  output = new BufferedWriter(new FileWriter(file));
					  output.write("user:"+user.getUsername()+",");
					  output.write("date:"+dateFormat.format(date)+",");
					  output.write("IP:"+remoteIp+',');
					  output.write(location);
					  output.write("\n");
					  output.close();
					  System.out.println("Your file has been written");
				  }
				  else
				  {
					//Appending to existing file at the beginning of the file.
					  RandomAccessFile rfile = new RandomAccessFile(file, "rws");
					    byte[] text = new byte[(int) rfile.length()];
					    rfile.readFully(text);
					    rfile.seek(0);
					    rfile.writeBytes("user:"+user.getUsername()+",");
					    rfile.writeBytes("date:"+dateFormat.format(date)+",");
					    rfile.writeBytes("IP:"+remoteIp+',');
					    rfile.writeBytes(location);
					    rfile.writeBytes(" \n");
					    rfile.write(text);
					    rfile.close();
					  
				  System.out.println("appended");  
				  }
			}
			catch(Exception e){
				e.printStackTrace();
			}  
	    }
	    
	    public String getExternalIP(HttpServletRequest request)
		{
			try {

				java.net.URL whatismyip = new java.net.URL("http://automation.whatismyip.com/n09230945.asp");
				java.net.URLConnection connection = whatismyip.openConnection();
			    connection.addRequestProperty("Protocol", "Http/1.1");
			    connection.addRequestProperty("Connection", "keep-alive");
			    connection.addRequestProperty("Keep-Alive", "1000");
			    connection.addRequestProperty("User-Agent", "Web-Agent");

			    BufferedReader in = 
			        new BufferedReader(new InputStreamReader(connection.getInputStream()));
			    String ip = in.readLine(); //you get the IP as a String
			    System.out.println(ip);
			    String location=getIPLocation(request.getRemoteAddr());
	            return ip+','+location;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return "Failed";
	        }
			
		}
		
		public String getIPLocation(String IP){
			String  city="",country="",state="";
			
			try {
				java.net.URL link = new java.net.URL("http://www.infosniper.net/index.php?ip_address="+IP);
		
				BufferedReader in = new BufferedReader(new InputStreamReader(link.openStream()));
				String inputLine;
				
				/*System.out.println("IP Details -----------|||||");
				System.out.println("Ip address"+IP);*/
				int count=100;
				while ((inputLine = in.readLine()) != null){
					//System.out.println(inputLine);
					
					if (inputLine.contains("City</td>")) 
						count=0;
					
					if(count==9){
						city = inputLine.substring(inputLine.indexOf('>')+1,inputLine.indexOf("<a href"));
					}
					if(inputLine.contains("State (Code)</td>"))
						count=10;
					
					if(count==19){
						state = inputLine.substring(inputLine.indexOf('>')+1,inputLine.indexOf("<a href"));
					}
					if(inputLine.contains("Country</td>"))
						count=20;
					
					if(count==29){
						country = inputLine.substring(inputLine.indexOf('>')+1,inputLine.indexOf("<a href"));
						count=100;
					}
					count++;
					
				}
			}
			catch (StringIndexOutOfBoundsException  e) {
				city=" ";
				state=" ";
				country=" ";
			} 
			catch(Exception e){
				 e.printStackTrace();
		         //return "Failed";
			}
			return "city:"+city+",state:"+state+",country:"+country;
		}
	    
}