package com.recruit.util;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.recruit.authority.service.Authority;
import com.recruit.candidate.service.Candidate;
import com.recruit.candidate.service.Candidate_RoleSet;
import com.recruit.candidate.service.Certification;
import com.recruit.client.service.Client;
import com.recruit.client.service.Contact;
import com.recruit.client.service.Contact_RoleSet;
import com.recruit.client.service.Contact_SkillSetGroup;
import com.recruit.interview.service.Interview;
import com.recruit.placementLeads.service.PlacementLeads;
import com.recruit.requirement.service.Requirement;
import com.recruit.requirement.service.Sub_Certification;
import com.recruit.requirement.service.Sub_Requirement;
import com.recruit.requirement.service.Sub_SkillSet;
import com.recruit.requirement.service.Sub_Vendor;
import com.recruit.requirement.service.WorkList;
import com.recruit.research.service.Research;
import com.recruit.settings.service.Module;
import com.recruit.settings.service.Settings;
import com.recruit.user.service.User;


public class JsonParserUtility {

	public static Object toRequirementJSON(List<Requirement> requirements) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < requirements.size(); i++) {
				JSONObject obj = toJSON(requirements.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", requirements.size());
			mainObj.put("success", Boolean.TRUE);
			
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static JSONObject toJSON(Requirement requirement) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id", requirement.getId());
			mainObj.put("addInfo_Todos",	requirement.getAddInfo_Todos());
			mainObj.put("researchComments",	requirement.getResearchComments());
			mainObj.put("created",	requirement.getCreated());
			mainObj.put("postingDate",	requirement.getPostingDate());
			mainObj.put("hotness",	requirement.getHotness());
			mainObj.put("lastUpdated",	requirement.getLastUpdated());
			mainObj.put("lastUpdatedUser",	requirement.getLastUpdatedUser());
			mainObj.put("location",	requirement.getLocation());
			mainObj.put("module",	requirement.getModule());
			mainObj.put("communication",	requirement.getCommunication());
			mainObj.put("requirement",	requirement.getRequirement());
			mainObj.put("source",	requirement.getSource());
			mainObj.put("comments", requirement.getComments());
			mainObj.put("postingTitle", requirement.getPostingTitle());
			mainObj.put("jobOpeningStatus", requirement.getJobOpeningStatus());
			mainObj.put("rateSubmitted", requirement.getRateSubmitted());
			mainObj.put("employer", requirement.getEmployer());
			mainObj.put("publicLink", requirement.getPublicLink());
			mainObj.put("skillSet", requirement.getSkillSet());
			mainObj.put("resume", requirement.getResume());
			mainObj.put("experience", requirement.getExperience());
			mainObj.put("helpCandidate", requirement.getHelpCandidate());
			mainObj.put("helpCandidateComments", requirement.getHelpCandidateComments());
			mainObj.put("secondaryModule", requirement.getSecondaryModule());
			mainObj.put("targetRoles", requirement.getTargetRoles());
			mainObj.put("certifications", requirement.getCertifications());
			mainObj.put("cityAndState", requirement.getCityAndState());
			mainObj.put("remote", requirement.getRemote());
			mainObj.put("relocate", requirement.getRelocate());
			mainObj.put("travel", requirement.getTravel());
			mainObj.put("tAndEPaid", requirement.gettAndEPaid());
			mainObj.put("tAndENotPaid", requirement.gettAndENotPaid());
			mainObj.put("version", requirement.getVersion());
			mainObj.put("requirementNo", requirement.getRequirementNo());
			
			String vendorId = "", candidateId = "", contactId = "",submittedDate="", certificationIds = "", skillsetIds = "";
			String shortlisted = "";
			int shortlistedCount = 0;
			JSONArray tneArray = new JSONArray();
			if(requirement.getSub_Requirements() != null && requirement.getSub_Requirements().size() > 0){
				for (Sub_Requirement sub_Requirement : requirement.getSub_Requirements()) {
					if (sub_Requirement.getDeleted() == false) {
						if (sub_Requirement.getSubmissionStatus().equalsIgnoreCase("Shortlisted")) {
							shortlisted += sub_Requirement.getCandidateId() +" :: "+sub_Requirement.getComments()+"####";
							shortlistedCount++;
						}else{
							if (sub_Requirement.getCandidateId()!= null)
								candidateId += sub_Requirement.getCandidateId()+",";
							if (sub_Requirement.getSubmittedDate()!= null)
								submittedDate += sub_Requirement.getCandidateId().toString() +"-"+ new SimpleDateFormat("MM/dd/yy HH:mm:ss").format(sub_Requirement.getSubmittedDate())+",";
							
							JSONObject obj = new JSONObject();
							obj.put("id",sub_Requirement.getId());
							obj.put("candidateId",sub_Requirement.getCandidateId());
							obj.put("vendorId",sub_Requirement.getVendorId());
							obj.put("submittedDate",sub_Requirement.getSubmittedDate());
							obj.put("submissionStatus",sub_Requirement.getSubmissionStatus());
							obj.put("submittedRate1",sub_Requirement.getSubmittedRate1());
							obj.put("submittedRate2",sub_Requirement.getSubmittedRate2());
							obj.put("projectInserted",sub_Requirement.getProjectInserted());
							obj.put("resumeLink",sub_Requirement.getResumeLink());
							tneArray.put(obj);
						}//else
					}//if
				}//for
			}//if
			if(requirement.getSub_Vendors() != null && requirement.getSub_Vendors().size() > 0){
				for (Sub_Vendor sub_Vendor : requirement.getSub_Vendors()) {
					if (sub_Vendor.getVendorId()!= null)
						vendorId += sub_Vendor.getVendorId()+",";
					if (sub_Vendor.getContactId()!= null)
						contactId += sub_Vendor.getContactId()+",";
				}
			}
			if (requirement.getClient() != null ) {
				mainObj.put("clientId", requirement.getClient().getId());
			}
			if (requirement.getRecruiterObj() != null ) {
				mainObj.put("recruiter", requirement.getRecruiterObj().getId());
			}
			if (requirement.getSub_Certifications() != null && requirement.getSub_Certifications().size() > 0) {
				for (Sub_Certification certification : requirement.getSub_Certifications()) {
					certificationIds += certification.getSettingsId()+",";
				}
			}
			if (requirement.getSub_Skillsets() != null && requirement.getSub_Skillsets().size() > 0) {
				for (Sub_SkillSet skillSet : requirement.getSub_Skillsets()) {
					skillsetIds += skillSet.getSkillSetId()+","; 
				}
			}
			
			if (vendorId.length() > 0) 
				vendorId = vendorId.substring(0,vendorId.length()-1);
			if (contactId.length() > 0) 
				contactId = contactId.substring(0,contactId.length()-1);
			if (candidateId.length() > 0) 
				candidateId = candidateId.substring(0,candidateId.length()-1);
			if (submittedDate.length() > 0) 
				submittedDate = submittedDate.substring(0,submittedDate.length()-1);
			if (shortlisted.length() > 0) 
				shortlisted = shortlisted.substring(0,shortlisted.length()-4);
			if (certificationIds.length() > 0) 
				certificationIds = certificationIds.substring(0,certificationIds.length()-1);
			if (skillsetIds.length() > 0) 
				skillsetIds = skillsetIds.substring(0,skillsetIds.length()-1);

			mainObj.put("shortlistedCandidates", shortlisted.toString());
			mainObj.put("shortlistedCount", shortlistedCount);
			mainObj.put("certificationIds", certificationIds);
			mainObj.put("skillSetIds", skillsetIds);

			int pendingTasksCount = 0;
			String pendingTasks = "";
			if (requirement.getResearchs() != null) {
				for (Research research : requirement.getResearchs()) {
					if (research.getStatus() != null && (research.getStatus().equals("In Progress") || research.getStatus().equals("Yet to Start"))){
						pendingTasksCount++;
						pendingTasks += pendingTasksCount + ". "+research.getStatus() +" - "+research.getTask()+"<br>";
					}
				}
			}
			mainObj.put("pendingTasksCount", pendingTasksCount);
			mainObj.put("pendingTasks", pendingTasks);
			
			mainObj.put("vendorId", vendorId);
			mainObj.put("contactId", contactId);
			mainObj.put("candidateId", candidateId);
			mainObj.put("submittedDate", submittedDate);
			mainObj.put("sub_Requirements", tneArray);
			
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static Object toPlacementJSON(List<PlacementLeads> leads) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < leads.size(); i++) {
				JSONObject obj = toJSON(leads.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", leads.size());
			mainObj.put("success", Boolean.TRUE);
			
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	private static JSONObject toJSON(PlacementLeads placement) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id", placement.getId());
			mainObj.put("priority",	placement.getPriority());
			mainObj.put("date",	placement.getDate());
			mainObj.put("person",	placement.getPerson());
			mainObj.put("sourceOfInformation",	placement.getSourceOfInformation());
			mainObj.put("source_Sublevel",	placement.getSource_Sublevel());
			mainObj.put("typeOfInformation",	placement.getTypeOfInformation());
			mainObj.put("information",	placement.getInformation());
			mainObj.put("hospital_Entity",	placement.getHospital_Entity());
			mainObj.put("location",	placement.getLocation());
			mainObj.put("liveDate",	placement.getLiveDate());
			mainObj.put("link1",	placement.getLink1());
			mainObj.put("link2",	placement.getLink2());
			mainObj.put("outreach",	placement.getOutreach());
			mainObj.put("created",	placement.getCreated());
			mainObj.put("lastUpdated",	placement.getLastUpdated());
			mainObj.put("lastUpdatedUser",	placement.getLastUpdatedUser());
			mainObj.put("comments", placement.getComments());
			mainObj.put("state", placement.getState());
			
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}
	
	public static Object toClientJSON(List<Client> clients) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < clients.size(); i++) {
				JSONObject obj = toJSON(clients.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", clients.size());
			mainObj.put("success", Boolean.TRUE);
			
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	private static JSONObject toJSON(Client client) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id", client.getId());
			mainObj.put("comments", client.getComments());
			mainObj.put("created", client.getCreated());
			mainObj.put("lastUpdated", client.getLastUpdated());
			mainObj.put("lastUpdatedUser", client.getLastUpdatedUser());
			mainObj.put("name", client.getName());
			mainObj.put("contactNumber", client.getContactNumber());
			mainObj.put("fax", client.getFax());
			mainObj.put("website", client.getWebsite());
			mainObj.put("industry", client.getIndustry());
			mainObj.put("about", client.getAbout());
			mainObj.put("type", client.getType());
			mainObj.put("address", client.getAddress());
			mainObj.put("email", client.getEmail());
			mainObj.put("score", client.getScore());
			mainObj.put("city", client.getCity());
			mainObj.put("state", client.getState());
			mainObj.put("contractorId", client.getContractorId());
			mainObj.put("fullTimeId", client.getFullTimeId());
			mainObj.put("confidentiality", client.getConfidentiality());
			mainObj.put("referenceId", client.getReferenceId());
			mainObj.put("helpCandidate", client.getHelpCandidate());
			mainObj.put("helpCandidateComments", client.getHelpCandidateComments());
			mainObj.put("recruiter", client.getRecruiter());
			mainObj.put("skillSetGroupIds", client.getSkillSetGroupIds());
			
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}
	
	public static Object toCandidateJSON(List<Candidate> candidates) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < candidates.size(); i++) {
				JSONObject obj = toJSON(candidates.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", candidates.size());
			mainObj.put("success", Boolean.TRUE);
			
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	private static JSONObject toJSON(Candidate candidate) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id", candidate.getId());
			mainObj.put("firstName", candidate.getFirstName());
			mainObj.put("lastName", candidate.getLastName());
			mainObj.put("emailId", candidate.getEmailId());
			mainObj.put("contactNumber", candidate.getContactNumber());
			mainObj.put("contactAddress", candidate.getContactAddress());
			mainObj.put("priority", candidate.getPriority());
			mainObj.put("currentJobTitle", candidate.getCurrentJobTitle());
			mainObj.put("type", candidate.getType());
			mainObj.put("p1comments", candidate.getP1comments());
			mainObj.put("p2comments", candidate.getP2comments());
			mainObj.put("created", candidate.getCreated());
			mainObj.put("lastUpdated", candidate.getLastUpdated());
			mainObj.put("lastUpdatedUser", candidate.getLastUpdatedUser());
			mainObj.put("employer", candidate.getEmployer());
			mainObj.put("skillSet", candidate.getSkillSet());
			mainObj.put("highestQualification", candidate.getHighestQualification());
			mainObj.put("referral", candidate.getReferral());
			mainObj.put("marketingStatus", candidate.getMarketingStatus());
			mainObj.put("availability", candidate.getAvailability());
			mainObj.put("source", candidate.getSource());
			mainObj.put("relocate", candidate.getRelocate());
			mainObj.put("travel", candidate.getTravel());
			mainObj.put("cityAndState", candidate.getCityAndState());
			mainObj.put("followUp", candidate.getFollowUp());
			mainObj.put("education", candidate.getEducation());
			mainObj.put("immigrationStatus", candidate.getImmigrationStatus());
			mainObj.put("openToCTH", candidate.getOpenToCTH());
			mainObj.put("startDate", candidate.getStartDate());
			mainObj.put("employmentType", candidate.getEmploymentType());
			mainObj.put("expectedRate", candidate.getExpectedRate());
			mainObj.put("aboutPartner", candidate.getAboutPartner());
			mainObj.put("personality", candidate.getPersonality());
			mainObj.put("submittedDate", candidate.getSubmittedDate());
			
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static JSONObject toJSONAuthority(List<Authority> authority) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < authority.size(); i++) {
				JSONObject obj = toJSON(authority.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", authority.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static JSONObject toJSON(Authority authority) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("loginId", authority.getLoginId());
			mainObj.put("authority", authority.getAuthority());
			
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static JSONObject toContactJSON(List<Contact> contacts) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < contacts.size(); i++) {
				JSONObject obj = toJSON(contacts.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", contacts.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static JSONObject toJSON(Contact contact) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id", contact.getId());
			mainObj.put("loginId", contact.getId());
			mainObj.put("firstName", contact.getFirstName());
			mainObj.put("lastName", contact.getLastName());
			mainObj.put("contactName", contact.getFirstName()+' '+contact.getLastName());
			mainObj.put("cellPhone", contact.getCellPhone());
			mainObj.put("comments", contact.getComments());
			mainObj.put("created", contact.getCreated());
			mainObj.put("email", contact.getEmail());
			mainObj.put("internetLink", contact.getInternetLink());
			mainObj.put("jobTitle", contact.getJobTitle());
			mainObj.put("lastUpdated", contact.getLastUpdated());
			mainObj.put("lastUpdatedUser", contact.getLastUpdatedUser());
			mainObj.put("workPhone", contact.getWorkPhone());
			mainObj.put("score", contact.getScore());
			mainObj.put("extension", contact.getExtension());
			mainObj.put("confidentiality", contact.getConfidentiality());
			mainObj.put("type", contact.getType());
			mainObj.put("role", contact.getRole());
			mainObj.put("gender", contact.getGender());
			mainObj.put("priority", contact.getPriority());
			mainObj.put("module", contact.getModule());
			mainObj.put("SMEComments", contact.getSMEComments());
			mainObj.put("placementComments", contact.getPlacementComments());
			mainObj.put("candidateDeleted", contact.getCandidateDeleted());
			mainObj.put("smeType", contact.getSmeType());
			mainObj.put("source", contact.getSource());
			mainObj.put("referral", contact.getReferral());
			mainObj.put("resumeLink", contact.getResumeLink());
			
			if (contact.getClient() !=  null){ 
				mainObj.put("clientId", contact.getClient().getId());
				mainObj.put("clientName", contact.getClient().getName());
				mainObj.put("clientType", contact.getClient().getType());
				mainObj.put("city", contact.getClient().getCity());
				mainObj.put("state", contact.getClient().getState());
			}
			if (contact.getCandidate() !=  null){
				mainObj.put("candidateId", contact.getCandidate().getId());
				mainObj.put("candidate", contact.getCandidate().getFirstName()+' '+contact.getCandidate().getLastName());
				if (! contact.getCandidateDeleted()) {
					mainObj.put("skillSet", contact.getCandidate().getSkillSet());
					mainObj.put("targetSkillSet", contact.getCandidate().getTargetSkillSet());
					mainObj.put("targetRoles", contact.getCandidate().getTargetRoles());
					mainObj.put("currentRoles", contact.getCandidate().getCurrentRoles());
					String certifications ="";
					for (Certification certification : contact.getCandidate().getCertificationsList()) {
						certifications += certification.getName() + ", ";
					}
					if (certifications.length() > 0) 
						certifications = certifications.substring(0,certifications.length()-1);

					String roleSetIds ="";
					for (Candidate_RoleSet roleSet: contact.getCandidate().getCandidate_RoleSets()) {
						roleSetIds += roleSet.getRoleSetId() + ",";
					}
					if (roleSetIds.length() > 0) 
						roleSetIds = roleSetIds.substring(0,roleSetIds.length()-1);

					mainObj.put("certifications", certifications);
					mainObj.put("canRoleSetIds", roleSetIds);
				}
			}
			if (contact.getContact_RoleSets() != null) {
				String roleSetIds ="";
				for (Contact_RoleSet roleSet : contact.getContact_RoleSets()) {
					roleSetIds += roleSet.getRoleSetId() + ",";
				}
				if (roleSetIds.length() > 0) 
					roleSetIds = roleSetIds.substring(0,roleSetIds.length()-1);

				mainObj.put("roleSetIds", roleSetIds);
			}
			if (contact.getContact_SkillSetGroups() != null) {
				String skillSetIds="";
				for (Contact_SkillSetGroup skillSet : contact.getContact_SkillSetGroups()) {
					skillSetIds += skillSet.getSkillSetGroupId()+",";
				}
				if (skillSetIds.length() > 0) 
					skillSetIds = skillSetIds.substring(0,skillSetIds.length()-1);
				mainObj.put("skillSetGroupIds", skillSetIds);
			}
			mainObj.put("interviews", contact.getInterviews().size());
			
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static Object toInterviewJSON(List<Interview> interviews) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < interviews.size(); i++) {
				JSONObject obj = toJSON(interviews.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", interviews.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	private static JSONObject toJSON(Interview interview) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id", interview.getId());
			mainObj.put("comments", interview.getComments());
			mainObj.put("created", interview.getCreated());
			mainObj.put("lastUpdated", interview.getLastUpdated());
			mainObj.put("lastUpdatedUser", interview.getLastUpdatedUser());
			mainObj.put("interviewDate", interview.getInterviewDate());
			mainObj.put("time", interview.getTime());
			//mainObj.put("status", interview.getStatus());
			mainObj.put("projectInserted", interview.getProjectInserted());
			mainObj.put("interview", interview.getInterview());
			mainObj.put("currentTime", interview.getCurrentTime());
			mainObj.put("timeZone", interview.getTimeZone());
			mainObj.put("approxStartDate", interview.getApproxStartDate());
			mainObj.put("contactId", interview.getContactId());
			mainObj.put("employeeFeedback", interview.getEmployeeFeedback());
			mainObj.put("vendorFeedback", interview.getVendorFeedback());
			
			if (interview.getCandidate() != null) {
				mainObj.put("candidateId", interview.getCandidate().getId());
				mainObj.put("candidate", interview.getCandidate().getFirstName()+' '+interview.getCandidate().getLastName());
			}
			if (interview.getClient() !=  null){ 
				mainObj.put("clientId", interview.getClient().getId());
				mainObj.put("client", interview.getClient().getName());
			}
			if (interview.getVendor() != null) {
				mainObj.put("vendorId", interview.getVendor().getId());
				mainObj.put("vendor", interview.getVendor().getName());
			}
			if (interview.getRequirement() != null){
				mainObj.put("requirementId", interview.getRequirement().getId());
				mainObj.put("skillSet", interview.getRequirement().getSkillSet());
				String skillsetIds ="";
				if (interview.getRequirement().getSub_Skillsets() != null && interview.getRequirement().getSub_Skillsets().size() > 0) {
					for (Sub_SkillSet skillSet : interview.getRequirement().getSub_Skillsets()) {
						skillsetIds += skillSet.getSkillSetId()+","; 
					}
				}
				if (skillsetIds.length() > 0) 
					skillsetIds = skillsetIds.substring(0,skillsetIds.length()-1);
				mainObj.put("skillSetIds", skillsetIds);
				
				if (interview.getCandidate() != null && interview.getRequirement().getSub_Requirements() != null && interview.getRequirement().getSub_Requirements().size() > 0) {
					for (Sub_Requirement requirement : interview.getRequirement().getSub_Requirements()) {
						if (requirement.getCandidateId().equals(interview.getCandidate().getId())){
							mainObj.put("status", requirement.getSubmissionStatus());
						}
					}
				}
			}
			
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static Object toUserJSON(List<User> users) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < users.size(); i++) {
				JSONObject obj = toUserJSON(users.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", users.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	private static JSONObject toUserJSON(User user) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id", user.getId());
			mainObj.put("firstName", user.getFirstName());
			mainObj.put("lastName", user.getLastName());
			mainObj.put("loginId", user.getLoginId());
			mainObj.put("emailId", user.getEmailId());
			mainObj.put("password", user.getPassword());
			mainObj.put("alternateEmailId", user.getAlternateEmailId());
			mainObj.put("active", user.getActive());
			mainObj.put("authority", user.getAuthority());
			mainObj.put("excludeConfidential", user.getExcludeConfidential());
			
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static Object toResearchJson(List<Research> list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < list.size(); i++) {
				JSONObject obj = toResearchJson(list.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	private static JSONObject toResearchJson(Research research) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id", research.getId());
			mainObj.put("task", research.getTask());
			mainObj.put("status", research.getStatus());
			mainObj.put("date", research.getDate());
			mainObj.put("created", research.getCreated());
			mainObj.put("lastUpdated", research.getLastUpdated());
			mainObj.put("lastUpdatedUser", research.getLastUpdatedUser());
			mainObj.put("potentialCandidates", research.getPotentialCandidates());
			mainObj.put("comments", research.getComments());
			mainObj.put("serialNo", research.getSerialNo());
			if (research.getRequirement() != null)
				mainObj.put("requirementId", research.getRequirement().getId());
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toSub_RequirementJson(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("id",objects[0]);
				obj.put("requirementId",objects[1]);
				obj.put("candidateId",objects[2]);
				obj.put("vendorId",objects[3]);
				obj.put("submittedDate",objects[4]);
				obj.put("created",objects[5]);
				obj.put("lastUpdated",objects[6]);
				obj.put("lastUpdatedUser",objects[7]);
				obj.put("submittedRate1",objects[8]);
				obj.put("submittedRate2",objects[9]);
				obj.put("submissionStatus",objects[10]);
				obj.put("projectInserted", objects[11]);
				obj.put("comments",objects[12]);
				obj.put("postingDate",objects[13]);
				obj.put("reqCityAndState",objects[14]);
				obj.put("location",objects[15]);
				obj.put("postingTitle",objects[16]);
				obj.put("candidate",objects[17]);
				obj.put("candidateExpectedRate",objects[18]);
				obj.put("cityAndState",objects[19]);
				obj.put("relocate",objects[20]);
				obj.put("travel",objects[21]);
				obj.put("availability",objects[22]);
				obj.put("immigrationVerified",objects[23]);
				obj.put("vendor",objects[24]);
				obj.put("client",objects[25]);
				obj.put("addInfo_Todos",objects[26]);
				obj.put("module", objects[27]);
				obj.put("hotness", objects[28]);
				obj.put("resumeHelp", objects[29]);
				obj.put("interviewHelp", objects[30]);
				obj.put("jobHelp", objects[31]);
				obj.put("vendorId", objects[32]);
				obj.put("clientId", objects[33]);
				
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toSub_VendorJson(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("id",objects[0]);
				obj.put("requirementId",objects[1]);
				obj.put("vendorId",objects[2]);
				obj.put("contactId",objects[3]);
				obj.put("email",objects[4]);
				obj.put("cellPhone",objects[5]);
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static Object toCertifications(List<Certification> certifications) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < certifications.size(); i++) {
				JSONObject obj = toJSON(certifications.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", certifications.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
    }
	
	public static JSONObject toJSON(Certification certification) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id",certification.getId());
			mainObj.put("dateAchieved",certification.getDateAchieved());
			mainObj.put("name",certification.getName());
			mainObj.put("reimbursementStatus",certification.getReimbursementStatus());
			mainObj.put("sponsoredBy",certification.getSponsoredBy());
			mainObj.put("status",certification.getStatus());
			mainObj.put("version",certification.getVersion());
			mainObj.put("certificationComments",certification.getCertificationComments());
			mainObj.put("epicInvoiceNo",certification.getEpicInvoiceNo());
			mainObj.put("epicUserWeb",certification.getEpicUserWeb());
			mainObj.put("expenseComments",certification.getExpenseComments());
			mainObj.put("expensesStatus",certification.getExpensesStatus());
			mainObj.put("latestNVT",certification.getLatestNVT());
			mainObj.put("paymentDetails",certification.getPaymentDetails());
			mainObj.put("verified",certification.getVerified());
			mainObj.put("created", certification.getCreated());
			mainObj.put("createdUser", certification.getCreatedUser());
			mainObj.put("lastUpdated", certification.getLastUpdated());
			mainObj.put("lastUpdatedUser", certification.getLastUpdatedUser());
			mainObj.put("epicInvoiceComments",certification.getEpicInvoiceComments());
			mainObj.put("epicInvoiceAmount",certification.getEpicInvoiceAmount());
			mainObj.put("cpsPaymentToEpic",certification.getCpsPaymentToEpic());
			mainObj.put("expensesIncurred",certification.getExpensesIncurred());
			mainObj.put("expensesPaid",certification.getExpensesPaid());
			mainObj.put("settingsId", certification.getSettingsId());

			if (certification.getCandidate() != null) {
				mainObj.put("candidateId", certification.getCandidate().getId());
				mainObj.put("candidate", certification.getCandidate().getFirstName()+' '+certification.getCandidate().getLastName());
			}
			JSONArray tneArray = new JSONArray();
			mainObj.put("tneDetail", tneArray);
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toCandidateListJSON(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("id",objects[0]);
				obj.put("fullName",objects[1]);
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toContactListJSON(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("id",objects[0]);
				obj.put("fullName",objects[1]);
				obj.put("clientId",objects[2]);
				obj.put("email",objects[3]);
				obj.put("cellPhone",objects[4]);
				obj.put("internetLink",objects[5]);
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toMessageJosn(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("id",objects[0]);
				obj.put("eMailId",objects[1]);
				obj.put("createdDate",objects[2]);
				obj.put("alertDate",objects[3]);
				obj.put("subject",objects[4]);
				obj.put("message",objects[5]);
				obj.put("alertInDays",objects[6]);
				obj.put("refId",objects[7]);
				obj.put("completedDate",objects[8]);
				obj.put("deletedDate",objects[9]);
				obj.put("priority",objects[10]);
				obj.put("pinned", objects[11]);
				obj.put("alertName", objects[12]);
				obj.put("status",objects[13]);
				obj.put("employeeName",objects[14]);
				
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
			
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toSubmission(List requirements) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = requirements.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("id",objects[0]);
				obj.put("postingDate",objects[1]);
				obj.put("submittedDate",objects[2]);
				obj.put("jobOpeningStatus",objects[3]);
				obj.put("location",objects[4]);
				obj.put("module",objects[5]);
				obj.put("hotness",objects[6]);
				obj.put("submittedRate1",objects[7]);
				obj.put("client",objects[8]);
				obj.put("vendor",objects[9]);
				obj.put("candidate",objects[10]);
				obj.put("day", objects[11]);
				obj.put("count",objects[12]);
				obj.put("time",objects[13]);
				obj.put("requirement",objects[14]);
				obj.put("postingTitle",objects[15]);
				obj.put("submittedRate2",objects[16]);
				
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", requirements.size());
			mainObj.put("success", Boolean.TRUE);
			
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toProjectJson(List projects) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = projects.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("projectCode",objects[0]);
				obj.put("projectName",objects[1]);
				obj.put("vendorName",objects[2]);
				obj.put("projectStatus",objects[3]);
				obj.put("employer",objects[4]);
				obj.put("employeeRate",objects[5]);
				obj.put("vendorRate",objects[6]);
				obj.put("projectStartDate",objects[7]);
				obj.put("projectEndDate",objects[8]);
				obj.put("approxEndDate",objects[9]);
				obj.put("invoicingTerms",objects[10]);
				obj.put("paymentTerms",objects[11]);
				obj.put("projectComments",objects[12]);

				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", projects.size());
			mainObj.put("success", Boolean.TRUE);
			
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toShortlistedJson(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("id",objects[0]);
				obj.put("requirementId",objects[1]);
				obj.put("candidateId",objects[2]);
				obj.put("created",objects[3]);
				obj.put("lastUpdated",objects[4]);
				obj.put("lastUpdatedUser",objects[5]);
				obj.put("comments",objects[6]);
				obj.put("candidate",objects[7]);
				obj.put("fullName",objects[7]);
				obj.put("candidateExpectedRate",objects[8]);
				obj.put("cityAndState",objects[9]);
				obj.put("relocate",objects[10]);
				obj.put("travel",objects[11]);
				obj.put("availability",objects[12]);
				obj.put("immigrationVerified",objects[13]);
				obj.put("client",objects[14]);
				obj.put("postingDate",objects[15]);
				obj.put("location",objects[16]);
				obj.put("postingTitle",objects[17]);
				obj.put("resumeHelp", objects[18]);
				obj.put("interviewHelp", objects[19]);
				obj.put("jobHelp", objects[20]);
				obj.put("emailId", objects[21]);
				obj.put("addInfoTodos", objects[22]);
				obj.put("module", objects[23]);
				obj.put("hotness", objects[24]);
				obj.put("vendor", objects[25]);

				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toSuggestedCriteria(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("type",objects[0]);
				obj.put("value",objects[1]);
				if (objects.length > 2)
					obj.put("ids",objects[2]);
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toPotentialCriteria(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("type",objects[0]);
				obj.put("value",objects[1]);
				obj.put("count",objects[2]);
				obj.put("clientId",objects[3]);
				obj.put("module",objects[4]);
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toStatsJson(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("reqs",objects[0]);
				obj.put("subs",objects[1]);
				obj.put("subReqs",objects[2]);
				obj.put("interviews",objects[3]);
				obj.put("placements",objects[4]);
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toCurrentReport(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				obj.put("id",objects[0]);
				obj.put("hotness",objects[1]);
				obj.put("cityAndState",objects[2]);
				obj.put("postingTitle",objects[3]);
				obj.put("postingDate",objects[4]);
				obj.put("module",objects[5]);
				obj.put("alert",objects[6]);
				obj.put("task",objects[7]);
				obj.put("status",objects[8]);
				obj.put("comments",objects[9]);
				obj.put("lastUpdatedUser",objects[10]);
				obj.put("client",objects[11]);
				obj.put("vendor",objects[12]);
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static Object toWorkListJSON(List<WorkList> list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < list.size(); i++) {
				JSONObject obj = toJSON(list.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
    }

	private static JSONObject toJSON(WorkList workList) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id",workList.getId());
			mainObj.put("userId",workList.getUserId());
			mainObj.put("assignedUserId",workList.getAssignedUserId());
			mainObj.put("comments",workList.getComments());
			if (workList.getRequirement() != null) {
				mainObj.put("requirementId", workList.getRequirement().getId());
				mainObj.put("postingDate",workList.getRequirement().getPostingDate());
				mainObj.put("postingTitle",workList.getRequirement().getPostingTitle());
				mainObj.put("cityAndState",workList.getRequirement().getCityAndState());
				mainObj.put("jobOpeningStatus",workList.getRequirement().getJobOpeningStatus());
				mainObj.put("location",workList.getRequirement().getLocation());
				mainObj.put("addInfoTodos",workList.getRequirement().getAddInfo_Todos());
				mainObj.put("module",workList.getRequirement().getModule());
				mainObj.put("hotness",workList.getRequirement().getHotness());
				mainObj.put("clientId",workList.getRequirement().getClient().getId());
			
				String vendorId = "", candidateId = "", contactId = "",submittedDate="" , shortlisted = "";
				int shortlistedCount = 0;

				if(workList.getRequirement().getSub_Vendors() != null && workList.getRequirement().getSub_Vendors().size() > 0){
					for (Sub_Vendor sub_Vendor : workList.getRequirement().getSub_Vendors()) {
						if (sub_Vendor.getVendorId()!= null)
							vendorId += sub_Vendor.getVendorId()+",";
						if (sub_Vendor.getContactId()!= null)
							contactId += sub_Vendor.getContactId()+",";
					}
				}
				if(workList.getRequirement().getSub_Requirements() != null && workList.getRequirement().getSub_Requirements().size() > 0){
					for (Sub_Requirement sub_Requirement : workList.getRequirement().getSub_Requirements()) {
						if (sub_Requirement.getSubmissionStatus().equalsIgnoreCase("Shortlisted")) {
							shortlisted += sub_Requirement.getCandidateId() +" :: "+sub_Requirement.getComments()+"####";
							shortlistedCount++;
						}else{
							if (sub_Requirement.getCandidateId()!= null)
								candidateId += sub_Requirement.getCandidateId()+",";
							if (sub_Requirement.getSubmittedDate()!= null)
								submittedDate += sub_Requirement.getCandidateId().toString() +"-"+ new SimpleDateFormat("MM/dd/yy HH:mm:ss").format(sub_Requirement.getSubmittedDate())+",";
						}
					}
				}
				if (vendorId.length() > 0) 
					vendorId = vendorId.substring(0,vendorId.length()-1);
				if (contactId.length() > 0) 
					contactId = contactId.substring(0,contactId.length()-1);
				if (candidateId.length() > 0) 
					candidateId = candidateId.substring(0,candidateId.length()-1);
				if (submittedDate.length() > 0) 
					submittedDate = submittedDate.substring(0,submittedDate.length()-1);
				if (shortlisted.length() > 0) 
					shortlisted = shortlisted.substring(0,shortlisted.length()-4);
				mainObj.put("vendorId",vendorId);
				mainObj.put("contactId",contactId);
				mainObj.put("candidateId",candidateId);
				mainObj.put("submittedDate",submittedDate);
				mainObj.put("shortlistedCount",shortlistedCount);
				mainObj.put("shortlisted",shortlisted);
			}
			JSONArray tneArray = new JSONArray();
			mainObj.put("tneDetail", tneArray);
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static Object toModuleJSON(List<Module> list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < list.size(); i++) {
				JSONObject obj = toJSON(list.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
    }

	private static JSONObject toJSON(Module module) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id",module.getId());
			mainObj.put("inthelp1",module.getInthelp1());
			mainObj.put("inthelp2",module.getInthelp2());
			mainObj.put("trainer",module.getTrainer());
			mainObj.put("resumeHelp",module.getResumeHelp());
			if (module.getSettings() != null) {
				mainObj.put("settingsId",module.getSettings().getId());
				mainObj.put("name",module.getSettings().getName());
				mainObj.put("value",module.getSettings().getValue());
				mainObj.put("orderNo",module.getSettings().getOrderNo());
				mainObj.put("type",module.getSettings().getType());
				mainObj.put("account",module.getSettings().getAccount());
				mainObj.put("module",module.getSettings().getModule());
			}
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static Object toModuleSettingsJson(List<Module> list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < list.size(); i++) {
				JSONObject obj = toModuleJSON(list.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
    }

	private static JSONObject toModuleJSON(Module module) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id",module.getId());
			mainObj.put("inthelp1",module.getInthelp1());
			mainObj.put("inthelp2",module.getInthelp2());
			mainObj.put("trainer",module.getTrainer());
			mainObj.put("resumeHelp",module.getResumeHelp());
			mainObj.put("name",module.getName());
			mainObj.put("value",module.getValue());
			mainObj.put("type",module.getType());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	@SuppressWarnings("rawtypes")
	public static Object toReportStatstics(List list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object[] objects = (Object[]) iterator.next();
				JSONObject obj = new JSONObject();
				for (int i = 0; i < objects.length; i++) {
					obj.put("value"+i,objects[i]);	
				}
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}

	public static Object toSettingsJson(List<Settings> list) {
		JSONObject mainObj = new JSONObject();
		try {
			JSONArray array = new JSONArray();
			for (int i = 0; i < list.size(); i++) {
				JSONObject obj = toJSON(list.get(i));
				array.put(obj);
			}
			mainObj.put("rows", array);
			mainObj.put("size", list.size());
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
    }

	public static JSONObject toJSON(Settings settings) {
		JSONObject mainObj = new JSONObject();
		try {
			mainObj.put("id",settings.getId());
			mainObj.put("name",settings.getName());
			mainObj.put("value",settings.getValue());
			mainObj.put("type",settings.getType());
			mainObj.put("notes",settings.getNotes());
			mainObj.put("account",settings.getAccount());
			mainObj.put("module",settings.getModule());
			mainObj.put("orderNo",settings.getOrderNo());
			mainObj.put("defaultSelection",settings.getDefaultSelection());
			mainObj.put("dependency",settings.getDependency());
			mainObj.put("tableName",settings.getTableName());
			mainObj.put("acronym",settings.getAcronym());
			if (settings.getSkillsetGroup() != null) {
				mainObj.put("skillsetGroupId",settings.getSkillsetGroup().getId());
				mainObj.put("skillsetGroupName",settings.getSkillsetGroup().getName());
			}
			mainObj.put("success", Boolean.TRUE);
		} catch (org.json.JSONException e) {
			e.printStackTrace();
		}
		return mainObj;
	}
	
}
