package com.recruit.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Properties;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class RunDatabaseEvents implements Job {

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			Connection conn = null;
			String url = "";
			String driver = "";
			String userName = "";
			String password = "";
			
			//Retrieving database url, username, password from jdbc.properties
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("jdbc.properties"));
			url = properties.getProperty("jdbc.databaseurl");
			driver = properties.getProperty("jdbc.driverClassName");
			userName = properties.getProperty("jdbc.username");
			password = properties.getProperty("jdbc.password");
			if (url.contains("autoReconnect")) 				
				url = url.substring(0,url.indexOf("autoReconnect")-1);
			
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, userName, password);

			System.out.println("--------Run Database Events -------");
			String query = "update recruit_candidate set marketingStatus='1. Active', lastUpdated=now(),lastUpdatedUser='AJ' " +
					"where date(availability)<curdate() and date(availability) > date_add(curdate(),interval -2 month) and marketingStatus!='1. Active'";
			//updating candidate marketingStatus to '1. Active' if availability is today as well as lastUpdated,lastUpdatedUser
			PreparedStatement ps = conn.prepareStatement(query);
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
