package com.recruit.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class SendReminder implements Job{

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			Connection conn = null;

			String url = "";
			String driver = "";
			String userName = "";
			String password = "";
			
			//Retrieving database url, username, password from jdbc.properties
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("jdbc.properties"));
			url = properties.getProperty("jdbc.databaseurl");
			driver = properties.getProperty("jdbc.driverClassName");
			userName = properties.getProperty("jdbc.username");
			password = properties.getProperty("jdbc.password");
			if (url.contains("autoReconnect")) 				
				url = url.substring(0,url.indexOf("autoReconnect")-1);
			
			Class.forName(driver).newInstance();
			conn = DriverManager
					.getConnection(url, userName, password);
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("appSettings.properties"));
			final String mailusername = properties.getProperty("email.id");
			final String mailpassword = properties.getProperty("email.password");

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");//this is smtp server address
			props.put("mail.smtp.port", "587");//this is port of the smtp server 
	 
			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailusername, mailpassword);
				}
			});
			System.out.println("------------ SendReminder -------------");
			System.out.println("------------ "+UtilityHelper.getDate()+" -------------");
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT distinct i.candidateId,(select concat(firstName,' ',lastName) from recruit_candidate where id=i.candidateId) candidate," +
							"(select group_concat(concat(firstName,' ',lastName)) from recruit_contacts where find_in_set(id,i.contactId)) interviewer," +
							"date_format(interviewDate,'%a %m/%d/%Y') interviewDate,time_format(time,'%r') time,time_format(currentTime,'%r') currentTime,timeZone, " +
							"(select name from recruit_clients where id=i.clientId) client,(select name from recruit_clients where id=i.vendorId) vendor," +
							"(select group_concat(concat(firstName,' ',lastName)) from recruit_contacts where find_in_set(id,s.guide)) guide, " +
							"r.postingtitle,r.module,r.cityandstate,date_format(r.postingDate,'%m/%d/%Y') postingDate,r.hotness,r.addInfo_Todos," +
							" r.location, r.source,r.targetRoles,r.certifications,r.helpCandidate,r.requirement," +
							"(select firstName from recruit_candidate where id=i.candidateId) firstName,"+
							"(select if(expectedRate is null or expectedRate ='',concat(rateFrom,'-',rateTo),expectedRate) from recruit_candidate where id=i.candidateId) expectedRate," +
							"(select submittedRate1 from recruit_sub_requirements where candidateId=i.candidateId and requirementId=i.requirementId) submittedRate," +
							"(select group_concat(s.name) from recruit_sub_skillset cs,recruit_settings s where s.id=cs.skillSetId and cs.requirementId=r.id) skillSet"+
							" FROM recruit_interviews i " +
							" left join recruit_requirements r on i.requirementId=r.id " +
							" left join recruit_sub_requirements s on s.requirementId=r.id and s.candidateId=i.candidateId " +
							" where s.submissionStatus in('I-Scheduled') and (i.interviewDate = curdate() and (i.time = ADDTIME(SEC_TO_TIME(FLOOR(TIME_TO_SEC(CURTIME())/60)*60), '1:00:00') " +
							" or i.time = ADDTIME(SEC_TO_TIME(FLOOR(TIME_TO_SEC(CURTIME())/60)*60), '2:00:00'))) ");

			while (rs.next()) {
				int candidateId = rs.getInt("candidateId");
				String candidate = rs.getString("candidate");
				String interviewDate = rs.getString("interviewDate");
				String currentTime = rs.getString("currentTime");
				String timeZone = rs.getString("timeZone");
				String time = rs.getString("time");
				String client = rs.getString("client");
				String vendor = rs.getString("vendor");
				String interviewer = rs.getString("interviewer");
				String guide = rs.getString("guide");

				String postingtitle = rs.getString("postingtitle");
				String module = rs.getString("module");
				String cityandstate = rs.getString("cityandstate");
				String postingDate = rs.getString("postingDate");
				String hotness = rs.getString("hotness");
				String alert = rs.getString("addInfo_Todos");
				String location = rs.getString("location");
				String source = rs.getString("source");
				String skillSet = rs.getString("skillSet");
				String targetRoles = rs.getString("targetRoles");
				String certifications = rs.getString("certifications");
				String requirement = rs.getString("requirement");
				String helpCandidate = rs.getString("helpCandidate");

				if (client == null || client.equalsIgnoreCase("null"))
					client = "";
				if (vendor == null || vendor.equalsIgnoreCase("null"))
					vendor = "";
				if (interviewer == null || interviewer.equalsIgnoreCase("null"))
					interviewer = "";
				if (guide == null || guide.equalsIgnoreCase("null"))
					guide = "";

				if (postingtitle == null || postingtitle.equalsIgnoreCase("null")) postingtitle = "";
				if (module == null || module.equalsIgnoreCase("null")) module = "";
				if (cityandstate == null || cityandstate.equalsIgnoreCase("null")) cityandstate = "";
				if (postingDate == null || postingDate.equalsIgnoreCase("null")) postingDate = "";
				if (hotness == null || hotness.equalsIgnoreCase("null")) hotness = "";
				if (alert == null || alert.equalsIgnoreCase("null")) alert = "";
				if (location == null || location.equalsIgnoreCase("null")) location = "";
				if (source == null || source.equalsIgnoreCase("null")) source = "";
				if (skillSet == null || skillSet.equalsIgnoreCase("null")) skillSet = "";
				if (targetRoles == null || targetRoles.equalsIgnoreCase("null")) targetRoles = "";
				if (certifications == null || certifications.equalsIgnoreCase("null")) certifications = "";
				if (requirement == null || requirement.equalsIgnoreCase("null")) requirement = "";
				if (helpCandidate == null || helpCandidate.equalsIgnoreCase("null")) helpCandidate = "";

				currentTime = currentTime.substring(0,5)+currentTime.substring(8,11);
				time = time.substring(0,5)+time.substring(8,11);
				
				System.out.println("------------"+time);

				String subject = "Reminder : Interview scheduled for "+candidate+" on "+interviewDate+" at "+time+" PST";
				String body = "Interview scheduled for "+candidate+" on <b>"+interviewDate+" at "+currentTime+" "+timeZone;
				if (!timeZone.equalsIgnoreCase("PST"))
					body += " ~ "+time+" PST";
				body += "</b>";

				String detailsTable = "<br><br><table cellpadding='5' bgcolor='#575252'>" +
						"<tr bgcolor='#ffffff'>" +
							"<th>Posting date</th>" +
							"<th>Posting Title</th>" +
							"<th>Hotness</th>" +
							"<th>City & State</th>" +
							"<th>Client</th>" +
							"<th>Vendor</th>" +
							"<th>Interviewers</th>" +
							"<th>Guide</th>" +
						"</tr>" +
						"<tr bgcolor='#ffffff'>" +
							"<td>"+postingDate+"</td>" +
							"<td>"+postingtitle+"</td>" +
							"<td>"+hotness+"</td>" +
							"<td>"+cityandstate+"</td>" +
							"<td>"+client+"</td>" +
							"<td>"+vendor+"</td>" +
							"<td>"+interviewer+"</td>" +
							"<td>"+guide+"</td>" +
						"</tr></table>";
				
				detailsTable += "<br><table cellpadding='5' bgcolor='#575252' width='90%'>" +
						"<tr bgcolor='#ffffff'><th>Module</th><td>"+module+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Skill Set</th><td>"+skillSet+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Mandatory Certifications</th><td>"+certifications+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Role Set</th><td>"+targetRoles+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Alert</th><td>"+alert+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Location</th><td> "+location+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Source</th><td>"+source+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Ok to Train Candidate</th><td>"+helpCandidate+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Roles & Responsibilities</th><td>"+requirement+"</td></tr>" +
						"</table>";

				//Rate alert
				String firstName = rs.getString("firstName");
				String expectedRate = rs.getString("expectedRate");
				String submittedRate = rs.getString("submittedRate");
				if (firstName == null || firstName.equalsIgnoreCase("null")) firstName = "";
				if (expectedRate == null || expectedRate.equalsIgnoreCase("null")) expectedRate = "";
				if (submittedRate == null || submittedRate.equalsIgnoreCase("null")) submittedRate = "";

				String subject2 = "Rate info for "+firstName+"-"+vendor+"-"+client+" Interview";
				String body2 = "Interview scheduled for "+candidate+" on <b>"+interviewDate+" at "+currentTime+" "+timeZone;
				if (!timeZone.equalsIgnoreCase("PST"))
					body2 += " ~ "+time+" PST";
				body2 += "</b>";
				
				String detailsTable2 = "<br><br><table cellpadding='5' bgcolor='#575252'>" +
						"<tr bgcolor='#ffffff'><th>Posting Title</th><td>"+postingtitle+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Vendor</th><td>"+vendor+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Client</th><td>"+client+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Submitted Rate</th><td>"+submittedRate+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Outgoing Rate</th><td> "+expectedRate+"</td></tr>" +
						"<tr bgcolor='#ffffff'><th>Guide</th><td>"+guide+"</td></tr>" +
						"</table>";

				
				String allEmailIds ="";
				Statement stmt2 = conn.createStatement();
				ResultSet adminUsers = stmt2
						.executeQuery("SELECT u.* FROM recruit_user u, recruit_authority a where u.loginId=a.loginId and a.authority in ('ADMIN','RECRUITER') and emailId not in ('pratima@candorps.com')");

				Calendar cal = Calendar.getInstance();
				Date currentDate = cal.getTime();
				java.sql.Timestamp createdDate = new Timestamp(currentDate.getTime());

				while (adminUsers.next()) {
					String emailId = adminUsers.getString("emailId");
					System.out.println("emailId : "+emailId);
					allEmailIds += emailId+",";

					String sql = "insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,alertInDays,refId,application,priority) values(?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement ps=conn.prepareStatement(sql);
					ps.setObject(1, emailId);
					ps.setObject(2, createdDate);
					ps.setObject(3, createdDate);
					ps.setObject(4, subject);
					ps.setObject(5, body);
					ps.setObject(6, false);
					ps.setObject(7, "Interview");
					ps.setObject(8, "Other");
					ps.setObject(9, 0);
					ps.setObject(10, candidateId);
					ps.setObject(11, "recruit");
					ps.setObject(12, 1);
					ps.executeUpdate();

					PreparedStatement ps2=conn.prepareStatement(sql);
					ps2.setObject(1, emailId);
					ps2.setObject(2, createdDate);
					ps2.setObject(3, createdDate);
					ps2.setObject(4, subject2);
				    ps2.setObject(5, body2);
				    ps2.setObject(6, false);
				    ps2.setObject(7, "Interview");
				    ps2.setObject(8, "Other");
				    ps2.setObject(9, 0);
				    ps2.setObject(10, candidateId);
				    ps2.setObject(11, "recruit");
				    ps2.setObject(12, 1);
				    ps2.executeUpdate();

				}
				
				body = body+detailsTable;
				body2 = body2+detailsTable2;

				if(allEmailIds.length() > 0){
					allEmailIds = allEmailIds.substring(0,allEmailIds.length()-1);
					Message msg = new MimeMessage(session);
					msg.setFrom(new InternetAddress(mailusername));//from email address
					msg.setSubject(subject);//sets subject of email
					//sets the content body of the email
					msg.setContent(body+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");//sets the content body of the email
					msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
					Transport.send(msg);//sends the entire message object 

					//Rate Alert
					Message msg2 = new MimeMessage(session);
					msg2.setFrom(new InternetAddress(mailusername));//from email address
					msg2.setSubject(subject2);//sets subject of email
					//sets the content body of the email
					msg2.setContent(body2+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");//sets the content body of the email
					msg2.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
					//Transport.send(msg2);//sends the entire message object
				}
			}
			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		
	}
	

}
