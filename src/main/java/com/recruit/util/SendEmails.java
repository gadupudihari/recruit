package com.recruit.util;

import java.security.Security;
import java.util.Date;
import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.recruit.base.presentation.Response;
import com.recruit.user.service.UserEmail;
import com.sun.mail.smtp.SMTPTransport;

public class SendEmails {

	//this method will sends an email even if the user in not logges in.
	public static void sendRecoveryMail(String title, String message,String recipientEmail) throws AddressException, MessagingException {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        String url= "appSettings.properties";
		final String username = UtilityHelper.getPropery(url, "email.id"); 		//retrives mailid,password,cc  
		final String password = UtilityHelper.getPropery(url, "email.password"); //from properties file
		final String cc = UtilityHelper.getPropery(url, "email.cc");
        
        // Get a Properties object
        Properties props = System.getProperties();
        props.setProperty("mail.smtps.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");
        props.setProperty("mail.smtps.auth", "true");

        props.put("mail.smtps.quitwait", "false");

        Session session = Session.getInstance(props, null);

        // -- Create a new message --
        final MimeMessage msg = new MimeMessage(session);

        // -- Set the FROM and TO fields --
        msg.setFrom(new InternetAddress(username ));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));

        if (cc != null && cc.length() > 0) {
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc, false));
        }

        msg.setSubject(title);
        msg.setText(message, "utf-8");
        msg.setSentDate(new Date());

        SMTPTransport t = (SMTPTransport)session.getTransport("smtps");

        t.connect("smtp.gmail.com", username, password);
        t.sendMessage(msg, msg.getAllRecipients());      
        t.close();
    }
	
	//this method will sends an email only if the user is logged in.
	public static void sendEMail(String subject, String body, String emailId) {
		try {
			String url= "appSettings.properties";
			final String mailusername = UtilityHelper.getPropery(url, "email.id"); 		//retrives mailid,password,cc  
			final String mailpassword = UtilityHelper.getPropery(url, "email.password"); //from properties file
			final String cc = UtilityHelper.getPropery(url, "email.cc");
			
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");//this is smtp server address
			props.put("mail.smtp.port", "587");//this is port of the smtp server 
			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailusername, mailpassword);
				}
			});
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(mailusername));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailId));
			if (cc != null && cc.length() > 0) {
				msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
			}
			msg.setSubject(subject);		//sets subject of email
			msg.setText(body);				//sets the content body of the email
			
			
			Transport.send(msg);			//sends the entire message object	
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	//this method will sends an email in html format 
	public static void sendHtmlEMail(String subject, String body, String emailId) {
		try {
			String url= "appSettings.properties";
			final String mailusername = UtilityHelper.getPropery(url, "email.id"); 		//retrives mailid,password,cc  
			final String mailpassword = UtilityHelper.getPropery(url, "email.password"); //from properties file
			final String cc = UtilityHelper.getPropery(url, "email.cc");
			
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");//this is smtp server address
			props.put("mail.smtp.port", "587");//this is port of the smtp server 
			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailusername, mailpassword);
				}
			});
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(mailusername));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailId));
			if (cc != null && cc.length() > 0) {
				msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
			}
			msg.setSubject(subject);		//sets subject of email
			msg.setContent(body, "text/html; charset=utf-8");				//sets the content body of the email
			
			Transport.send(msg);			//sends the entire message object	
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	public static Response sendSeperateEMail(Response response, UserEmail email, String subject,String message, String[] emailIds, String cc) {
		try {
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", email.getSmtp());//this is smtp server address
			props.put("mail.smtp.port", email.getPortNo());//this is port of the smtp server
			final String mailusername = email.getEmailId();
			final String password = Encriptor.decryptString(email.getPassword());
			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailusername, password);
				}
			});
			
			for (String emaiTo : emailIds) {
				Message msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(mailusername));
				msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emaiTo));
				msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
				msg.setSubject(subject);		//sets subject of email
				msg.setContent(message, "text/html; charset=utf-8");				//sets the content body of the email
				
				Transport.send(msg);			//sends the entire message object	
			}
		} catch (AuthenticationFailedException e) {
			response.setSuccess(false);
			response.setErrorMessage("Authentication failed, Invalid email credentials");
			return response;
		}catch (Exception e) {
			e.printStackTrace();
		}

		response.setSuccess(true);
		return response; 
	}

}
