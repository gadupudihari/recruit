package com.recruit.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.w3c.dom.Element;

public class SendSubmissionReport implements Job {

	@SuppressWarnings("deprecation")
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			Connection conn = null;

			//Mail willbe send on Sunday (index = 0), Saturday (index = 6) 
			if(UtilityHelper.getDate().getDay() ==0 || UtilityHelper.getDate().getDay() == 6){
				System.out.println("Mail will be send on weekends");
				return;
			}
			
			String url = "";
			String driver = "";
			String userName = "";
			String password = "";
			
			//Retrieving database url, username, password from jdbc.properties
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("jdbc.properties"));
			url = properties.getProperty("jdbc.databaseurl");
			driver = properties.getProperty("jdbc.driverClassName");
			userName = properties.getProperty("jdbc.username");
			password = properties.getProperty("jdbc.password");
			if (url.contains("autoReconnect")) 				
				url = url.substring(0,url.indexOf("autoReconnect")-1);
			
			Class.forName(driver).newInstance();
			conn = DriverManager
					.getConnection(url, userName, password);
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("appSettings.properties"));
			final String mailusername = properties.getProperty("email.id");
			final String mailpassword = properties.getProperty("email.password");

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");//this is smtp server address
			props.put("mail.smtp.port", "587");//this is port of the smtp server 
	 
			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailusername, mailpassword);
				}
			});

			System.out.println("------------------SendSubmissionReport----------------------");
			Statement stmt = conn.createStatement();
			//Today Statistics
			ResultSet rs = stmt.executeQuery("select if(round(name) >2,'Others',round(name)) hotness," +
					"(select count(*) from recruit_requirements where hotness = s.name and date(postingDate) = date(curdate())) reqs," +
					"(select count(distinct r1.id) from recruit_requirements r,recruit_sub_requirements r1 where r1.submissionStatus != 'Shortlisted' " +
					"and r1.deleted = false and r1.requirementId=r.id and r.hotness = s.name and date(postingDate) = date(curdate())) submissions," +
					"(select count(distinct r.id) from recruit_requirements r,recruit_sub_requirements r1 where r1.submissionStatus != 'Shortlisted' " +
					"and r1.deleted = false and r1.requirementId=r.id and r.hotness = s.name and date(postingDate) = date(curdate())) submittedReqs " +
					"from recruit_settings s where type='Hotness'");
			
			int totalReqs = 0, totalSubmissions = 0, totalSubmittedReqs =0;
			int totalInterviews = 0, totalPlacements = 0;

			String finalEmailString = "", todayStats ="", todaySubStats ="" , todayReqs ="", 
					yesterDayStats ="", yesterDaySubStats ="" , yesterDayReqs ="", monthlyStats ="" ;
			while (rs.next()) {
				int reqs = rs.getInt("reqs");
				int submissions = rs.getInt("submissions");
				int submittedReqs = rs.getInt("submittedReqs");
				todayStats += "<tr bgcolor='#FFFFFF' style='text-align:center;'>" +
						"<td style='background-color:#D8D8D8;'>"+rs.getString("hotness")+"</td>"+
						"<td >"+reqs+"</td>"+
						"<td >"+submittedReqs+"</td>";
				if (reqs == 0)
					todayStats += "<td >0%</td>";
				else
					todayStats += "<td >"+Math.round(submittedReqs/reqs*100)+"%</td>";
				todayStats += "<td >"+submissions+"</td></tr>"; 
				totalReqs += reqs;
				totalSubmissions += submissions;
				totalSubmittedReqs += submittedReqs;
			}
			if (todayStats != "") {
				todayStats = "<table cellpadding='5' bgcolor='#575252'><tr bgcolor='#F2F2F2' style='text-align:center;'>" +
						"<td width='70'>Priority</td>"+
						"<td width='90'>New Reqs</td>"+
						"<td width='140'>New Reqs Submitted To</td>"+
						"<td width='60' style='text-align:center;'>%</td>"+
						"<td width='140'>Total Subs for new reqs</td></tr>"+
						todayStats+
						"<tr bgcolor='#FFFFFF' style='font-weight:bold;text-align:center;'>" +
						"<td style='background-color:#D8D8D8;'>Total</td>"+
						"<td >"+totalReqs+"</td>"+
						"<td >"+totalSubmittedReqs+"</td>";
				if (totalReqs == 0)
					todayStats += "<td >0%</td>";
				else
					todayStats += "<td >"+Math.round(totalSubmittedReqs*100/totalReqs)+"%</td>"; 
				todayStats +=	"<td >"+totalSubmissions+"</td></tr></table>"; 
			}
			
			//Today submission stats
			rs = stmt.executeQuery("select if(round(name) >2,'Others',round(name)) hotness, " +
					"(select count(distinct r.id) from recruit_requirements r,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
					"and r.id=s.requirementId and hotness = s.name and (date(s.submittedDate) = curdate() or date(s.statusUpdated) = curdate())) reqs, " +
					"(select count(distinct s.id) from recruit_requirements r,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
					"and r.id=s.requirementId and hotness = s.name and date(s.submittedDate) = curdate()) subs, " +
					"(select count(distinct s.id) from recruit_requirements r,recruit_sub_requirements s " +
					"where r.id=s.requirementId and hotness = s.name and (date(s.submittedDate) = curdate() or date(s.statusUpdated) = curdate()) " +
					"and s.submissionStatus in('I-Scheduled','I-Succeeded','I-Withdrawn','I-Failed','C-Declined') and s.deleted = false) interviews, " +
					"(select count(distinct s.id) from recruit_requirements r,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
					"and r.id=s.requirementId and hotness = s.name and (date(s.submittedDate) = curdate() or date(s.statusUpdated) = curdate()) " +
					"and s.submissionStatus ='C-Accepted') placements from recruit_settings s where type='Hotness' ");
			
			totalReqs = 0; totalSubmissions = 0; 
			totalInterviews = 0; totalPlacements = 0;

			while (rs.next()) {
				int reqs = rs.getInt("reqs"); // submitted today or status updated today
				int subs = rs.getInt("subs"); // submitted today 
				int interviews = rs.getInt("interviews"); // submitted today or status updated today to I-Scheduled || I-Succeeded || I-Failed || C-Declined
				int placements = rs.getInt("placements"); // submitted today or status updated today to C-Accepted
				
				todaySubStats += "<tr bgcolor='#FFFFFF' style='text-align:center;'>" +
						"<td style='background-color:#D8D8D8;'>"+rs.getString("hotness")+"</td>"+
						"<td >"+reqs+"</td>"+
						"<td >"+subs+"</td>" +
						"<td >"+interviews+"</td>" +
						"<td >"+placements+"</td></tr>";
				totalReqs += reqs;
				totalSubmissions += subs;
				totalInterviews += interviews;
				totalPlacements += placements;
			}
			if (todaySubStats != "") {
				todaySubStats = "<table cellpadding='5' bgcolor='#575252'><tr bgcolor='#F2F2F2' style='text-align:center;'>" +
						"<td width='70'>Priority</td>"+
						"<td width='90'>Reqs</td>"+
						"<td width='130'>Total Submissions</td>" +
						"<td width='105'>Interview ? </td>" +
						"<td width='105'>Accepted ? </td></tr>"+
						todaySubStats+
						"<tr bgcolor='#FFFFFF' style='font-weight:bold;text-align:center;'>" +
						"<td style='background-color:#D8D8D8;'>Total</td>"+
						"<td >"+totalReqs+"</td>"+
						"<td >"+totalSubmissions+"</td>" +
						"<td >"+totalInterviews+"</td>" +
						"<td >"+totalPlacements+"</td></tr></table>"; 
			}
			
			String topHeadings = "<tr bgcolor='#F2F2F2' style='text-align:center;'><td >Sub Time</td>" +
					"<td>Priority</td>"+
					"<td>Posting Date</td>"+
					"<td>Turnaround Time</td>"+
					"<td>Submission Status</td>"+
					"<td>Candidate</td>"+
					"<td>Posting Title</td>"+
					"<td>Module</td>"+
					"<td>Vendor</td>"+
					"<td>Client</td>"+
					"<td>Location</td>"+
					"<td>Submitted Rate</td>"+
					"<td>Outgoing Expected Rate</td></tr>";
			
			//Today Requirments
			rs = stmt.executeQuery("select r.id,round(r.hotness) hotness,date_format(r.postingDate,'%m/%d/%Y') postingDate," +
					"date_format(s.submittedDate,'%m/%d/%Y') submittedDate,date_format(s.submittedDate,'%H:%i') submittedTime," +
					"s.submissionStatus,r.location,r.module,r.hotness,submittedRate1,submittedRate2,concat(c.firstName,' ',c.lastName) candidate,c.expectedRate," +
					"CAST(TIME_FORMAT(SEC_TO_TIME((TIME_TO_SEC(timediff(s.submittedDate,postingDate)))),'%H:%i') AS char) time, " +
					"(select name from recruit_clients where id =r.clientId) client,(select name from recruit_clients where id =s.vendorId) vendor,postingTitle " +
					"from recruit_requirements r,recruit_sub_requirements s,recruit_candidate c where s.submissionStatus != 'Shortlisted' and s.deleted = false and " +
					"s.requirementId=r.id and c.id=s.candidateId and date(s.statusUpdated) = curdate() ");
			
			while (rs.next()) {
				String client = rs.getString("client");
				String vendor = rs.getString("vendor");
				String candidate = rs.getString("candidate");
				String submittedTime = rs.getString("submittedTime");
				String time = rs.getString("time");
				
				if (client == null || client.equalsIgnoreCase("null"))
					client = "";
				if (vendor == null || vendor.equalsIgnoreCase("null"))
					vendor = "";
				if (candidate == null || candidate.equalsIgnoreCase("null"))
					candidate = "";
				if (time == null || time.equalsIgnoreCase("null"))
					time = "0";
				if (submittedTime == null || submittedTime.equalsIgnoreCase("null"))
					submittedTime = "";

				Date today = new Date();
				String backGround = "FFFFFF";

				if (rs.getString("submittedDate") == null || rs.getString("submittedDate").equalsIgnoreCase("null")) {
					backGround = "FFFFFF";
				}else{
					Date submittedDate = new SimpleDateFormat("MM/dd/yyyy").parse(rs.getString("submittedDate"));
					String[] interviews = {"I-Scheduled","I-Succeeded","I-Withdrawn","I-Failed","C-Declined"};
					if (Arrays.asList(interviews).contains(rs.getString("submissionStatus")))
						backGround = "FFD3A7";
					else if (rs.getString("submissionStatus").equalsIgnoreCase("C-Accepted"))
						backGround = "A7FDA1";
					else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) == 0) 
						backGround = "C3D5F6";
					else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) == 1) 
						backGround = "CFDCF5";
					else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) <= 7) 
						backGround = "D9E3F5";
					else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) <= 30) 
						backGround = "E5EBF5";
				}

				todayReqs += "<tr bgcolor='#"+backGround+"'><td>"+submittedTime+"</td>" +
						"<td>"+rs.getString("hotness")+"</td>"+
						"<td>"+rs.getString("postingDate")+"</td>"+
						"<td>"+time+" Hrs</td>"+
						"<td>"+rs.getString("submissionStatus")+"</td>"+
						"<td>"+candidate+"</td>"+
						"<td>"+rs.getString("postingTitle")+"</td>"+
						"<td>"+rs.getString("module")+"</td>"+
						"<td>"+vendor+"</td>"+
						"<td>"+client+"</td>"+
						"<td>"+rs.getString("location")+"</td>"+
						"<td>"+rs.getString("submittedRate1")+"<br>"+rs.getString("submittedRate2")+"</td>"+
						"<td>"+rs.getString("expectedRate")+"</td></tr>";
			}
			if (todayReqs != "") {
				todayReqs = "<table cellpadding='5' bgcolor='#575252'>"+topHeadings+todayReqs+"</table>" +
						"<br>Note : The above table is generated based on status updated date in submitted candidates<br>";
			}
			
			String todayData = "<FONT size='4' style='background-color: #F0F1FF'><b>"+new SimpleDateFormat("EEEE - MM/dd/yyyy").format(new Date())+" :</b></FONT>" +
					"<table cellpadding='5' width='1200' style='border:1px solid black;border-collapse:collapse;'><tr><td style='background:#F0F1FF'>" +
					"<FONT style='background-color: #FFFFFF'><b>New requirements stats : </b></FONT>"+todayStats+"<br>"+ 
					"<FONT style='background-color: #FFFFFF'><b>Submissions stats : </b></FONT>"+ todaySubStats +"<br>"+
					todayReqs + "</table>";
			
			//Yesterday statistics
			rs = stmt.executeQuery("select if(round(name) >2,'Others',round(name)) hotness," +
					"(select count(*) from recruit_requirements where hotness = s.name and " +
					" if(DAYOFWEEK(now()) = 2,date(postingDate) = date(date_sub(now(),interval 3 day)),date(postingDate) = date(date_sub(now(),interval 1 day)))) reqs," +
					"(select count(distinct r1.id) from recruit_requirements r,recruit_sub_requirements r1 where r1.submissionStatus != 'Shortlisted' and r1.deleted = false and r1.requirementId=r.id and r.hotness = s.name and " +
					" if(DAYOFWEEK(now()) = 2,date(postingDate) = date(date_sub(now(),interval 3 day)),date(postingDate) = date(date_sub(now(),interval 1 day)))) submissions," +
					"(select count(distinct r.id) from recruit_requirements r,recruit_sub_requirements r1 where r1.submissionStatus != 'Shortlisted' and r1.deleted = false and r1.requirementId=r.id and r.hotness = s.name and " +
					" if(DAYOFWEEK(now()) = 2,date(postingDate) = date(date_sub(now(),interval 3 day)),date(postingDate) = date(date_sub(now(),interval 1 day)))) submittedReqs," +
					"(select count(distinct r1.id) from recruit_requirements r,recruit_sub_requirements r1 where r1.submissionStatus != 'Shortlisted' and r1.deleted = false and r1.requirementId=r.id and r.hotness = s.name and " +
					"if(DAYOFWEEK(now()) = 2,date(postingDate) = date(date_sub(now(),interval 3 day)),date(postingDate) = date(date_sub(now(),interval 1 day))) " +
					"and r1.submissionStatus in(select name from recruit_settings where type ='Submission Status' and notes ='Interview')) interview, " +
					"(select count(distinct r1.id) from recruit_requirements r,recruit_sub_requirements r1 where r1.submissionStatus != 'Shortlisted' and r1.deleted = false and r1.requirementId=r.id and r.hotness = s.name and " +
					"if(DAYOFWEEK(now()) = 2,date(postingDate) = date(date_sub(now(),interval 3 day)),date(postingDate) = date(date_sub(now(),interval 1 day))) " +
					"and r1.submissionStatus = 'C-Accepted') placements from recruit_settings s where type='Hotness'");
			
			totalReqs = 0; totalSubmissions = 0; totalSubmittedReqs =0;
			totalInterviews = 0; totalPlacements = 0;
			while (rs.next()) {
				int reqs = rs.getInt("reqs");
				int submissions = rs.getInt("submissions");
				int submittedReqs = rs.getInt("submittedReqs");
				int interview = rs.getInt("interview");
				int placements = rs.getInt("placements");
				yesterDayStats += "<tr bgcolor='#FFFFFF' style='text-align:center;'>" +
						"<td style='background-color:#D8D8D8;'>"+rs.getString("hotness")+"</td>"+
						"<td >"+reqs+"</td>"+
						"<td >"+submittedReqs+"</td>";
				if (reqs == 0)
					yesterDayStats += "<td >0%</td>";
				else
					yesterDayStats += "<td >"+Math.round(submittedReqs*100/reqs)+"%</td>";
				yesterDayStats += "<td >"+interview+"</td>"+
						"<td >"+placements+"</td>" +
						"<td >"+submissions+"</td></tr>"; 
				totalReqs += reqs;
				totalSubmissions += submissions;
				totalSubmittedReqs += submittedReqs;
				totalInterviews += interview;
				totalPlacements += placements;
			}
			if (yesterDayStats != "") {
				yesterDayStats = "<table cellpadding='5' bgcolor='#575252'><tr bgcolor='#F2F2F2' style='text-align:center;'>" +
						"<td width='50'>Priority</td>"+
						"<td width='40'>Reqs</td>"+
						"<td width='120'>Reqs Submitted To</td>"+
						"<td width='40'>%</td>"+
						"<td width='70'>Interview ?</td>"+
						"<td width='70'>Accepted ?</td>"+
						"<td width='110'>Total Submissions</td></tr>"+
						yesterDayStats+
						"<tr bgcolor='#FFFFFF' style='font-weight:bold;text-align:center;'>" +
						"<td style='background-color:#D8D8D8;'>Total</td>"+
						"<td >"+totalReqs+"</td>"+
						"<td >"+totalSubmittedReqs+"</td>";
						if (totalReqs == 0)
							yesterDayStats += "<td >0%</td>";
						else
							yesterDayStats += "<td >"+Math.round(totalSubmittedReqs*100/totalReqs)+"%</td>"; 
				yesterDayStats +="<td >"+totalInterviews+"</td>"+
						"<td >"+totalPlacements+"</td>"+
						"<td >"+totalSubmissions+"</td></tr></table>"; 
			}
			
			//Yester day submission stats
			int daysToSub = 1; 
			if ((new Date()).getDay() == 1) {
				//for monday
				daysToSub = 3;
			}
			
			rs = stmt.executeQuery("select if(round(name) >2,'Others',round(name)) hotness," +
					"(select count(distinct r.id) from recruit_requirements r,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false and r.id=s.requirementId and hotness = s.name and " +
					"(date(s.submittedDate) = date_sub(curdate(),interval "+daysToSub+" day) or date(s.statusUpdated) = date_sub(curdate(),interval "+daysToSub+" day))) reqs," +
					" (select count(distinct s.id) from recruit_requirements r,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false and " +
					"r.id=s.requirementId and hotness = s.name and date(s.submittedDate) = date_sub(curdate(),interval "+daysToSub+" day)) subs," +
					" (select count(distinct s.id) from recruit_requirements r,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false and r.id=s.requirementId and hotness = s.name and " +
					"(date(s.submittedDate) = date_sub(curdate(),interval "+daysToSub+" day) or date(s.statusUpdated) = date_sub(curdate(),interval "+daysToSub+" day))" +
					" and s.submissionStatus in('I-Scheduled','I-Succeeded','I-Failed','I-Withdrawn','C-Declined')) interviews," +
					" (select count(distinct s.id) from recruit_requirements r,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false and r.id=s.requirementId and hotness = s.name and " +
					"(date(s.submittedDate) = date_sub(curdate(),interval "+daysToSub+" day) or date(s.statusUpdated) = date_sub(curdate(),interval "+daysToSub+" day))" +
					" and s.submissionStatus ='C-Accepted') placements from recruit_settings s where type='Hotness' ");

			totalReqs = 0; totalSubmissions = 0; totalSubmittedReqs =0;
			totalInterviews = 0; totalPlacements = 0;

			while (rs.next()) {
				int reqs = rs.getInt("reqs"); // submitted today or status updated today
				int subs = rs.getInt("subs"); // submitted today 
				int interviews = rs.getInt("interviews"); // submitted today or status updated today to I-Scheduled || I-Succeeded || I-Failed || C-Declined
				int placements = rs.getInt("placements"); // submitted today or status updated today to C-Accepted
				
				yesterDaySubStats += "<tr bgcolor='#FFFFFF' style='text-align:center;'>" +
						"<td style='background-color:#D8D8D8;'>"+rs.getString("hotness")+"</td>"+
						"<td >"+reqs+"</td>"+
						"<td >"+subs+"</td>" +
						"<td >"+interviews+"</td>" +
						"<td >"+placements+"</td></tr>";
				totalReqs += reqs;
				totalSubmissions += subs;
				totalInterviews += interviews;
				totalPlacements += placements;
			}
			if (yesterDaySubStats != "") {
				yesterDaySubStats = "<table cellpadding='5' bgcolor='#575252'><tr bgcolor='#F2F2F2' style='text-align:center;'>" +
						"<td width='60'>Priority</td>"+
						"<td width='105'>Reqs</td>"+
						"<td width='130'>Total Submissions</td>" +
						"<td width='115'>Interview ? </td>" +
						"<td width='110'>Accepted ? </td></tr>"+
						yesterDaySubStats+
						"<tr bgcolor='#FFFFFF' style='font-weight:bold;text-align:center;'>" +
						"<td style='background-color:#D8D8D8;'>Total</td>"+
						"<td >"+totalReqs+"</td>"+
						"<td >"+totalSubmissions+"</td>" +
						"<td >"+totalInterviews+"</td>" +
						"<td >"+totalPlacements+"</td></tr></table>"; 
			}

			//Yesterday Requirments
			rs = stmt.executeQuery("select r.id,round(r.hotness) hotness,date_format(r.postingDate,'%m/%d/%Y') postingDate," +
					"date_format(s.submittedDate,'%m/%d/%Y') submittedDate,date_format(s.submittedDate,'%H:%i') submittedTime," +
					"s.submissionStatus,r.location,r.module,r.hotness,submittedRate1,submittedRate2,concat(c.firstName,' ',c.lastName) candidate,c.expectedRate," +
					"CAST(TIME_FORMAT(SEC_TO_TIME((TIME_TO_SEC(timediff(s.submittedDate,postingDate)))),'%H:%i') AS char) time, " +
					"(select name from recruit_clients where id =r.clientId) client,(select name from recruit_clients where id =s.vendorId) vendor,postingTitle " +
					"from recruit_requirements r,recruit_sub_requirements s,recruit_candidate c where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
					"and s.requirementId=r.id and c.id=s.candidateId and date(s.statusUpdated) = date_sub(curdate(),interval "+daysToSub+" day) ");
			
			while (rs.next()) {
				String client = rs.getString("client");
				String vendor = rs.getString("vendor");
				String candidate = rs.getString("candidate");
				String submittedTime = rs.getString("submittedTime");
				String time = rs.getString("time");
				
				if (client == null || client.equalsIgnoreCase("null"))
					client = "";
				if (vendor == null || vendor.equalsIgnoreCase("null"))
					vendor = "";
				if (candidate == null || candidate.equalsIgnoreCase("null"))
					candidate = "";
				if (time == null || time.equalsIgnoreCase("null"))
					time = "0";
				if (submittedTime == null || submittedTime.equalsIgnoreCase("null"))
					submittedTime = "";

				Date today = new Date();
				String backGround = "CFDCF5";
				if (rs.getString("submittedDate") == null || rs.getString("submittedDate").equalsIgnoreCase("null")) {
					backGround = "FFFFFF";
				}else{
					Date submittedDate = new SimpleDateFormat("MM/dd/yyyy").parse(rs.getString("submittedDate"));
					String[] interviews = {"I-Scheduled","I-Succeeded","I-Failed","I-Withdrawn","C-Declined"};
					if (Arrays.asList(interviews).contains(rs.getString("submissionStatus")))
						backGround = "FFD3A7";
					else if (rs.getString("submissionStatus").equalsIgnoreCase("C-Accepted"))
						backGround = "A7FDA1";
					else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) == 0) 
						backGround = "C3D5F6";
					else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) == 1) 
						backGround = "CFDCF5";
					else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) <= 7) 
						backGround = "D9E3F5";
					else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) <= 30) 
						backGround = "E5EBF5";
				}

				yesterDayReqs += "<tr bgcolor='#"+backGround+"'><td>"+rs.getString("submittedTime")+"</td>" +
						"<td>"+rs.getString("hotness")+"</td>"+
						"<td>"+rs.getString("postingDate")+"</td>"+
						"<td>"+rs.getString("time")+" Hrs</td>"+
						"<td>"+rs.getString("submissionStatus")+"</td>"+
						"<td>"+candidate+"</td>"+
						"<td>"+rs.getString("postingTitle")+"</td>"+
						"<td>"+rs.getString("module")+"</td>"+
						"<td>"+vendor+"</td>"+
						"<td>"+client+"</td>"+
						"<td>"+rs.getString("location")+"</td>"+
						"<td>"+rs.getString("submittedRate1")+"<br>"+rs.getString("submittedRate2")+"</td>"+
						"<td>"+rs.getString("expectedRate")+"</td></tr>";
			}
			if (yesterDayReqs != "") {
				yesterDayReqs = "<table cellpadding='5' bgcolor='#575252'>"+topHeadings+yesterDayReqs+"</table>" +
						"<br>Note : The above table is generated based on status updated date in submitted candidates<br>";
			}

			String yesterday = ""; 
			if (new Date().getDay() == 1){
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.DATE, -3);
				yesterday = new SimpleDateFormat("EEEE - MM/dd/yyyy").format(calendar.getTime()); 
			}else{
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.DATE, -1);
				yesterday = new SimpleDateFormat("EEEE - MM/dd/yyyy").format(calendar.getTime()); 
			}
			
			String yesterdayData = "<FONT size='4' style='background-color: #FFF7EE'><b>"+yesterday+" :</b></FONT>" +
					"<table cellpadding='5' width='1200' style='border:1px solid black;border-collapse:collapse;'><tr><td style='background:#FFF7EE'>" +
					"<FONT style='background-color: #FFFFFF'><b>New requirements stats : </b></FONT>"+yesterDayStats+"<br>"+ 
					"<FONT style='background-color: #FFFFFF'><b>Submissions stats : </b></FONT>"+ yesterDaySubStats +"<br>"+
					yesterDayReqs + "</table>";

			finalEmailString = todayData +"<br>"+ yesterdayData+"<br>";
			
			//This month statistics
			rs = stmt.executeQuery("select if(round(name) >2,'Others',round(name)) hotness," +
					"(select count(*) from recruit_requirements where hotness = s.name and month(postingDate) = month(now()) and year(postingDate) = year(now())) reqs," +
					"(select count(distinct r1.id) from recruit_requirements r,recruit_sub_requirements r1 where r1.submissionStatus != 'Shortlisted' and r1.deleted = false " +
					"and r1.requirementId=r.id and r.hotness = s.name and month(postingDate) = month(now()) and year(postingDate) = year(now()) ) submissions," +
					"(select count(distinct r.id) from recruit_requirements r,recruit_sub_requirements r1 where r1.submissionStatus != 'Shortlisted' and r1.deleted = false " +
					"and r1.requirementId=r.id and r.hotness = s.name and month(postingDate) = month(now()) and year(postingDate) = year(now()) ) submittedReqs," +
					"(select count(distinct r1.id) from recruit_requirements r,recruit_sub_requirements r1 where r1.submissionStatus != 'Shortlisted' and r1.deleted = false " +
					"and r1.requirementId=r.id and r.hotness = s.name and month(postingDate) = month(now()) and year(postingDate) = year(now()) " +
					"and r1.submissionStatus in(select name from recruit_settings where type ='Submission Status' and notes ='Interview')) interview," +
					"(select count(distinct r1.id) from recruit_requirements r,recruit_sub_requirements r1 where r1.submissionStatus != 'Shortlisted' and r1.deleted = false " +
					"and r1.requirementId=r.id and r.hotness = s.name and month(postingDate) = month(now()) and year(postingDate) = year(now()) and r1.submissionStatus = 'C-Accepted') placements " +
					"from recruit_settings s where type='Hotness'");
			
			totalReqs = 0; totalSubmissions = 0; totalSubmittedReqs =0;
			totalInterviews = 0; totalPlacements = 0;
			while (rs.next()) {
				int reqs = rs.getInt("reqs");
				int submissions = rs.getInt("submissions");
				int submittedReqs = rs.getInt("submittedReqs");
				int interview = rs.getInt("interview");
				int placements = rs.getInt("placements");
				monthlyStats += "<tr bgcolor='#FFFFFF' style='text-align:center;'>" +
						"<td style='background-color:#D8D8D8;'>"+rs.getString("hotness")+"</td>"+
						"<td >"+reqs+"</td>"+
						"<td >"+submittedReqs+"</td>";
				if (reqs == 0)
					monthlyStats += "<td >0%</td>";
				else
					monthlyStats += "<td >"+Math.round(submittedReqs*100/reqs)+"%</td>";
				monthlyStats += "<td >"+interview+"</td>"+
						"<td >"+placements+"</td>" +
						"<td >"+submissions+"</td></tr>"; 
				totalReqs += reqs;
				totalSubmissions += submissions;
				totalSubmittedReqs += submittedReqs;
				totalInterviews += interview;
				totalPlacements += placements;
			}
			if (monthlyStats != "") {
				monthlyStats = "<table cellpadding='5' bgcolor='#575252'><tr bgcolor='#F2F2F2' style='text-align:center;'>" +
						"<td width='65'>Priority</td>"+
						"<td width='45'>Reqs</td>"+
						"<td width='115'>Reqs Submitted To</td>"+
						"<td width='45'>%</td>"+
						"<td width='65'>Interview ?</td>"+
						"<td width='65'>Accepted ?</td>"+
						"<td width='115'>Total Submissions</td></tr>"+
						monthlyStats+
						"<tr bgcolor='#FFFFFF' style='font-weight:bold;text-align:center;'>" +
						"<td style='background-color:#D8D8D8;'>Total</td>"+
						"<td >"+totalReqs+"</td>"+
						"<td >"+totalSubmittedReqs+"</td>";
						if (totalReqs == 0)
							monthlyStats += "<td >0%</td>";
						else
							monthlyStats += "<td >"+Math.round(totalSubmittedReqs*100/totalReqs)+"%</td>"; 
				monthlyStats +="<td >"+totalInterviews+"</td>"+
						"<td >"+totalPlacements+"</td>"+
						"<td >"+totalSubmissions+"</td></tr></table>"; 
			}

			//Monthly Calender
	    	Calendar firstDay = Calendar.getInstance();
	    	firstDay.set(Calendar.DATE, 1);
	    	if (firstDay.get(Calendar.DAY_OF_WEEK) != 0) {
	    		firstDay.add(Calendar.DATE, firstDay.get(Calendar.DAY_OF_WEEK)*-1);
	    		firstDay.add(Calendar.DATE, 1);
			}
	    	Calendar lastDate = Calendar.getInstance();
	    	lastDate.set(Calendar.DAY_OF_MONTH, lastDate.getActualMaximum(Calendar.DAY_OF_MONTH));
	    	if (lastDate.get(Calendar.DAY_OF_WEEK) != 7) {
				lastDate.add(Calendar.DATE, 7-lastDate.get(Calendar.DAY_OF_WEEK));
			}

			rs = stmt.executeQuery("select count(distinct id) reqs,count(distinct subId) subs,count(distinct subReqs) subReqs,postingDate,group_concat(hours) hours," +
					"count(if(submissionStatus in(select name from recruit_settings where type ='Submission Status' and notes ='Interview'),1,null)) interviews," +
					"count(if(submissionStatus='C-Accepted',1,null)) placements,count(distinct intReqs) intReqs from (" +
					"SELECT r.id,date_format(postingDate,'%m/%d/%Y') postingDate,s.id subId,if(s.id is null,null,r.id) subReqs," +
					"TIMESTAMPDIFF(HOUR, postingDate,submittedDate) hours,s.submissionStatus," +
					"if(s.submissionStatus in (select name from recruit_settings where type ='Submission Status' and notes ='Interview'),r.id,null) intReqs " +
					"FROM recruit_requirements r " +
					"LEFT JOIN recruit_sub_requirements s on s.submissionStatus != 'Shortlisted' and s.deleted = false and s.requirementId=r.id " +
					"where date(postingDate) >= '"+new SimpleDateFormat("yyyy-MM-dd").format(firstDay.getTime())+"' and date(postingDate) <= '" 
					+new SimpleDateFormat("yyyy-MM-dd").format(lastDate.getTime())+"' )tab group by postingdate ") ;
			
			List<String> dates  = new ArrayList<String>();
			List<Integer[]> hoursArray  = new ArrayList<Integer[]>();
			List<Integer> reqsArray  = new ArrayList<Integer>();
			List<Integer> subsArray  = new ArrayList<Integer>();
			List<Integer> subReqsArray  = new ArrayList<Integer>();
			List<Integer> interviewsArray  = new ArrayList<Integer>();
			List<Integer> placementsArray  = new ArrayList<Integer>();
			List<Integer> intReqsArray  = new ArrayList<Integer>();

			while (rs.next()) {
				int reqs = rs.getInt("reqs");
				int subs = rs.getInt("subs");
				int subReqs = rs.getInt("subReqs");
				String postingDate = rs.getString("postingDate");
				String hours = rs.getString("hours");
				int interviews = rs.getInt("interviews");
				int placements = rs.getInt("placements");
				int intReqs = rs.getInt("intReqs");
				
				reqsArray.add(reqs);
				subsArray.add(subs);
				subReqsArray.add(subReqs);
				dates.add(postingDate);
				interviewsArray.add(interviews);
				placementsArray.add(placements);
				intReqsArray.add(intReqs);
				
				if (hours != null && hours.length() > 0) {
					String[] innerHours = hours.split(",");
					Integer[] intarray=new Integer[innerHours.length];
				    int i=0;
				    for(String str:innerHours){
				        try {
				            intarray[i]=Integer.parseInt(str.trim());
				            i++;
				        } catch (NumberFormatException e) {
				            throw new IllegalArgumentException("Not a number: " + str + " at index " + i, e);
				        }
				    }
				    hoursArray.add(intarray);
				}else
					hoursArray.add(null);
			}

	    	Calendar calendar = new GregorianCalendar();
	    	int year = calendar.get(Calendar.YEAR);  // 2012
	    	int month = calendar.get(Calendar.MONTH)+1; 

		    String[] months = {"","January", "February", "March","April", "May", "June","July", "August", "September","October", "November", "December"};

		     // days[i] = number of days in month i
	        int[] days = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	        // check for leap year
	        if (month == 2 && isLeapYear(year)) days[month] = 29;

	        // print calendar header
	    	String table = "<table width='1190' cellpadding='5' bgcolor='#575252'>" +
	    			"<tr><th colspan='9' style='background-color:#dddddd;font-weight:bold;'>"+ months[month] + " " + year+"</th></tr>"+
	    			"<tr style='background-color:#dddddd;font-weight:bold;text-align:center;'><td width='100px' >Sunday</td>" +
	    			"<td width='100px' >Monday</td>" +
	    			"<td width='100px' >Tuesday</td>" +
	    			"<td width='100px' >Wednesday</td>" +
	    			"<td width='100px' >Thursday</td>" +
	    			"<td width='100px' >Friday</td>" +
	    			"<td width='100px' >Saturday</td>"+
	    			"<td width='120px' >Weekly average</td>"+
	    			"<td width='100px' >Placements Average</td>";;
	    	
	    	table +="<tr bgcolor='#ffffff' style='text-align:center;vertical-align:top;'>";

	        int weeklyReqs =0, weeklySubs =0, weeklySubReqs =0, weeklyInterviews =0, weeklyPlacements =0, weeklyIntReqs=0   ;
	        List<Integer> weeklyHours = new ArrayList<Integer>();

	    	for (int i=0; true ;i++) {
	    		//when current day exceeds last day of month leave from loop
	    		if (firstDay.after(lastDate)) {
	    			break;	
				}
	    		int dayOfMonth = firstDay.get(Calendar.DAY_OF_MONTH);
	    		int monthOfYear = firstDay.get(Calendar.MONTH);
	    		int percentage = 0;
	    		if (weeklyReqs == 0)
	    			percentage = 0;
	    		else
	    			percentage = weeklySubReqs*100/weeklyReqs;
	    		int weeklyMedian = median(weeklyHours);
	    		
	    		if((i%7)==0 && i > 0){
	    			table +="<td style='text-align:center;color: #0070C0;'>"+ weeklyReqs+" Reqs, "+weeklySubReqs+" Subs, "+weeklySubs+" T"
	    					+"<br>"+percentage+"%,"+weeklyMedian+" Hrs (Median)</td>";
	    			table +="<td style='text-align:center;color: #0070C0;'>"+ weeklyPlacements+" Pl, "+weeklyInterviews+" Int, "
	    					+"<br>"+weeklyIntReqs+" Reqs</td>";
	    			table += "</tr><tr bgcolor='#ffffff' style='text-align:center;vertical-align:top;'>";

	    			weeklyReqs = 0;
	    			weeklySubs = 0;
	    			weeklySubReqs = 0;
	    			weeklyInterviews = 0;
	    			weeklyPlacements = 0;
	    			weeklyIntReqs = 0;
	    			weeklyHours = new ArrayList<Integer>();
	    		}
    			calendar.clear();
    			calendar.set(Calendar.MONTH, monthOfYear);
    			calendar.set(Calendar.YEAR, year);
    			calendar.set(Calendar.DATE, dayOfMonth);
    			Date date = calendar.getTime();
    			String today = new SimpleDateFormat("MM/dd/yyyy").format(date);

    			String backGround = "FDE9D9";//orange
				if (UtilityHelper.getDate().getDate() == dayOfMonth && UtilityHelper.getDate().getMonth() == monthOfYear )
					backGround = "ffff99"; //today Yellow
				else if (dates.contains(today))
					backGround = "ffffff"; //submissions white
				else if (calendar.get(Calendar.DAY_OF_WEEK) == 1 || calendar.get(Calendar.DAY_OF_WEEK) == 7)
					backGround = "dddddd"; //Weekends gray
				else if (UtilityHelper.getDate().getMonth() < monthOfYear) 
					backGround = "ffffff"; //Next month always white
				else if (UtilityHelper.getDate().getDate() < dayOfMonth &&  UtilityHelper.getDate().getMonth() == monthOfYear)
					backGround = "ffffff"; //Future in current month submissions white
				
    			if (dates.contains(today)) {
					int medianHours = median(hoursArray.get(dates.indexOf(today)));
					int reqs = reqsArray.get(dates.indexOf(today));
					int subs = subsArray.get(dates.indexOf(today));
					int subReqs = subReqsArray.get(dates.indexOf(today));
					int percantage = subReqs*100/reqs;
					
					if (percantage > 99)
						backGround = "B0FF9F";
					else if (percantage > 74) 
						backGround = "DBFFC1";
					else if (percantage > 24) 
						backGround = "E7FFA8";
					else if (percantage > 1) 
						backGround = "ffffff";
					
					table +="<td style='text-align:center;background: #"+backGround+";'><b>"+ dayOfMonth +"</b>"+ "<br>"
							+reqs+" Reqs,"+subReqs+" Subs,"+subs+" T <br>"+percantage+"%, "+medianHours+" Hrs</td>";
	    			weeklyReqs += reqs;
	    			weeklySubs += subs;
	    			weeklySubReqs += subReqs;
	    			weeklyPlacements += placementsArray.get(dates.indexOf(today));
	    			weeklyInterviews += interviewsArray.get(dates.indexOf(today));
	    			weeklyIntReqs += intReqsArray.get(dates.indexOf(today));
	    			weeklyHours.add(medianHours);
    			}else
    				table +="<td style='text-align:center;background: #"+backGround+";'><b>"+ dayOfMonth +"</b></td>";
    			
	    		firstDay.add(Calendar.DATE, 1);
			}

    		int percentage = 0;
    		if (weeklyReqs == 0)
    			percentage = 0;
    		else
    			percentage = weeklySubs*100/weeklyReqs;
    		int weeklyMedian = median(weeklyHours);
			table +="<td style='text-align:center;color: #0070C0;'>"+ weeklyReqs+" Reqs, "+weeklySubReqs+" Subs, "+weeklySubs+" T"
					+"<br>"+percentage+"%,"+weeklyMedian+" Hrs (Median)</td>";
			table +="<td style='text-align:center;color: #0070C0;'>"+ weeklyPlacements+" Pl, "+weeklyInterviews+" Int, "
					+"<br>"+weeklyIntReqs+" Reqs</td>";
	    	table += "</tr></table>";
	    	
			String monthlyData = "<FONT size='4' style='background-color: #ECFFEC'><b>"+new SimpleDateFormat("MMMM-yyyy").format(new Date())+" :</b></FONT>" +
					"<table cellpadding='5' width='1200' style='border:1px solid black;border-collapse:collapse;'><tr><td style='background:#ECFFEC'>" +
					""+monthlyStats+"<br>"+table +"</table>";

			finalEmailString += monthlyData;
	    	
	    	//"Still Working On Req's" posted in the last 30 days: ( Priority 0/1)
			Statement stmt1 = conn.createStatement();
			ResultSet requirements = stmt1.executeQuery("select r.id,date_format(r.postingDate,'%m/%d/%Y') postingDate,postingTitle," +
					"r.jobOpeningStatus,r.location,r.module,r.hotness,requirement,(select name from recruit_clients where id =r.clientId) client," +
					"(select group_concat(c.name) from recruit_clients c,recruit_sub_vendors s where c.id=s.vendorId and s.requirementId =r.id) vendor," +
					"(select group_concat(concat(firstName,' ',lastName)) from recruit_candidate c,recruit_sub_requirements s where " +
					"s.submissionStatus != 'Shortlisted' and s.deleted = false and c.id=s.candidateId and s.requirementId =r.id) candidate " +
					"from recruit_requirements r where r.hotness in ('00','01') and jobOpeningStatus in ('01.In-progress') " +
					"and datediff(now(),r.postingDate) <=30 order by r.postingDate DESC,hotness desc");
	    	
			String openRecords ="",submittedRecords = ""  ;
			while (requirements.next()) {
				String requirement = requirements.getString("requirement");
				String client = requirements.getString("client");
				String vendor = requirements.getString("vendor");
				String candidate = requirements.getString("candidate");
				
				if (client == null || client.equalsIgnoreCase("null"))
					client = "";
				if (vendor == null || vendor.equalsIgnoreCase("null"))
					vendor = "";
				if (candidate == null || candidate.equalsIgnoreCase("null"))
					candidate = "";
				if (requirement.length() > 100)
					requirement = requirement.substring(0,99)+"....";
				
				Date postingDate = new SimpleDateFormat("MM/dd/yyyy").parse(requirements.getString("postingDate"));
				Date today = new Date();
				
				String backGround = "FFFFFF";
				if ((today.getTime() - postingDate.getTime())/(1000*60*60*24) == 0) 
					backGround = "FFFCBC";
				else if ((today.getTime() - postingDate.getTime())/(1000*60*60*24) == 1) 
					backGround = "FAFAD0";
				else if ((today.getTime() - postingDate.getTime())/(1000*60*60*24) <= 7) 
					backGround = "FFFFE0";
				else if ((today.getTime() - postingDate.getTime())/(1000*60*60*24) <= 30) 
					backGround = "FFFFEE";

				openRecords += "<tr bgcolor='#"+backGround+"'>" +
								"<td >"+requirements.getString("id")+"</td>" +
								"<td >"+requirements.getString("hotness")+"</td>" +
								"<td >"+requirements.getString("postingDate")+"</td>" +
								"<td >"+requirements.getString("postingTitle")+"</td>" +
								"<td >"+requirements.getString("jobOpeningStatus")+"</td>" +
								"<td >"+requirements.getString("location")+"</td>" +
								"<td >"+requirements.getString("module")+"</td>" +
								"<td >"+requirement+"</td>" +
								"<td >"+client+"</td>" +
								"<td >"+vendor+"</td>" +
								"<td >"+candidate+"</td></tr>" ;
			}
			
			if (openRecords.length() > 0) {
				openRecords = "<table width='1200' cellpadding='5' bgcolor='#575252'>" +
								"<tr style='background-color:#dddddd;text-align:center;'>" +
		    					"<td width='50px' >Unique Id </td>" +
				    			"<td width='50px' >Hotness</td>" +
				    			"<td width='100px' >Posting Date</td>" +
				    			"<td width='100px' >Posting Title</td>" +
				    			"<td width='100px' >Job Opening Status</td>" +
				    			"<td width='100px' >Location</td>" +
				    			"<td width='100px' >Module</td>"+
				    			"<td width='400px' >Roles and Responsibilities</td>"+
				    			"<td width='100px' >Client</td>"+
				    			"<td width='100px' >Vendor</td>"+
				    			"<td width='100px' >Candidate</td>"
				    			+ openRecords + "</table>";
			}
			
			//Submissions in Last 30 Days for Priority 0,1 Requirements
			rs = stmt.executeQuery("select r.id,s.id,round(r.hotness) hotness,date_format(r.postingDate,'%m/%d/%Y') postingDate,date_format(s.submittedDate,'%m/%d/%Y') submittedDate," +
					"s.submissionStatus,r.location,r.module,r.hotness,submittedRate1,submittedRate2,concat(c.firstName,' ',c.lastName) candidate,c.expectedRate," +
					"CAST(TIME_FORMAT(SEC_TO_TIME((TIME_TO_SEC(timediff(s.submittedDate,postingDate)))),'%H:%i') AS char) time," +
					"(select name from recruit_clients where id =r.clientId) client,(select name from recruit_clients where id =s.vendorId) vendor,postingTitle " +
					"from recruit_requirements r,recruit_sub_requirements s,recruit_candidate c where s.submissionStatus != 'Shortlisted' and s.deleted = false and s.requirementId=r.id and c.id=s.candidateId " +
					"and hotness in ('00','01') and jobOpeningStatus='03.Submitted' and datediff(now(),s.submittedDate) <=30 order by s.submittedDate desc");

			while (rs.next()) {
				String client = rs.getString("client");
				String vendor = rs.getString("vendor");
				String candidate = rs.getString("candidate");
				
				if (client == null || client.equalsIgnoreCase("null"))
					client = "";
				if (vendor == null || vendor.equalsIgnoreCase("null"))
					vendor = "";
				if (candidate == null || candidate.equalsIgnoreCase("null"))
					candidate = "";

				Date submittedDate = new SimpleDateFormat("MM/dd/yyyy").parse(rs.getString("submittedDate"));
				Date today = new Date();
				
				String backGround = "FFFFFF";
				String[] interviews = {"I-Scheduled","I-Succeeded","I-Failed","I-Withdrawn","C-Declined"};
				if (Arrays.asList(interviews).contains(rs.getString("submissionStatus")))
					backGround = "FFD3A7";
				else if (rs.getString("submissionStatus").equalsIgnoreCase("C-Accepted"))
					backGround = "A7FDA1";
				else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) == 0) 
					backGround = "C3D5F6";
				else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) == 1) 
					backGround = "CFDCF5";
				else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) <= 7) 
					backGround = "D9E3F5";
				else if ((today.getTime() - submittedDate.getTime())/(1000*60*60*24) <= 30) 
					backGround = "E5EBF5";
				
				submittedRecords += "<tr bgcolor='#"+backGround+"'><td>"+rs.getString("submittedDate")+"</td>" +
						"<td>"+rs.getString("hotness")+"</td>"+
						"<td>"+rs.getString("postingDate")+"</td>"+
						"<td>"+rs.getString("time")+" Hrs</td>"+
						"<td>"+rs.getString("submissionStatus")+"</td>"+
						"<td>"+candidate+"</td>"+
						"<td>"+rs.getString("postingTitle")+"</td>"+
						"<td>"+rs.getString("module")+"</td>"+
						"<td>"+vendor+"</td>"+
						"<td>"+client+"</td>"+
						"<td>"+rs.getString("location")+"</td>"+
						"<td>"+rs.getString("submittedRate1")+"<br>"+rs.getString("submittedRate2")+"</td>"+
						"<td>"+rs.getString("expectedRate")+"</td></tr>";
			}
			if (submittedRecords.length() > 0) {
				submittedRecords = "<FONT size='4' style='background-color:#FAFAD0'><b>Submissions in Last 30 Days for Priority 0,1 Requirements:</b></FONT><br>" +
						"<table cellpadding='5' width='1200' bgcolor='#575252'>" + topHeadings + submittedRecords + "</table>";
			}
			openRecords = "<FONT size='4' style='background-color:#FAFAD0'><b>\"Still Working On Req's\" posted in the last 30 days: ( Priority 0/1):</b></FONT><br>"
							+openRecords +"<br>"+ submittedRecords;
	    	
			String subject = "Daily requirement submissions summary";
			String mailtoRole="",mailtoignore="";
			Element eElement = UtilityHelper.readXmlFile("Submissions turnaround time");
        	mailtoRole = eElement.getElementsByTagName("mailtoRole").item(0).getChildNodes().item(0).getNodeValue();
        	mailtoignore = eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue();
        	int priority = Integer.parseInt(eElement.getElementsByTagName("priority").item(0).getChildNodes().item(0).getNodeValue());
        	
			//This alert body cannot be saved in data base hence alert will be sent now and stored in email table without body
			 String sql = "insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,alertInDays,refId,application,priority) values(?,?,?,?,?,?,?,?,?,?,?,?)";
			 PreparedStatement ps=conn.prepareStatement(sql);
		     ps.setObject(2, UtilityHelper.getDate());
		     ps.setObject(3, UtilityHelper.getDate());
		     ps.setObject(4, subject);
		     ps.setObject(5, "Please verify your email to know the details");
		     ps.setObject(6, true);
		     ps.setObject(7, "Submissions turnaround time");
		     ps.setObject(8, "Detailed");
		     ps.setObject(9, 0);
		     ps.setObject(10, 0);
		     ps.setObject(11, "recruit");
		     ps.setObject(12, priority);

		     if (mailtoRole.contains(",")) 
		    	 mailtoRole = mailtoRole.replaceAll(",", "','");
		     mailtoRole = "'"+mailtoRole+"'";
		     String[] ignoreEmails = mailtoignore.split(",");
		     
		     System.out.println("--------------Submission email formation completed ----------"+UtilityHelper.getDate());
		     
			String allEmailIds ="";
			Statement stmt2 = conn.createStatement();
			ResultSet adminUsers = stmt2.executeQuery("SELECT u.* FROM recruit_user u, recruit_authority a where u.loginId=a.loginId and a.authority in ("+mailtoRole+") " );
			while (adminUsers.next()) {
				String emailId = adminUsers.getString("emailId");

				//checking emailid given in ignore list
				if (! Arrays.asList(ignoreEmails).contains(emailId)) {
					allEmailIds += emailId+",";
					
					//Store in db
					ps.setObject(1, emailId);
				    ps.executeUpdate();
				}
			}
			
			if(allEmailIds.length() > 0){
				allEmailIds = allEmailIds.substring(0,allEmailIds.length()-1);
				Message msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(mailusername));//from email address
				msg.setSubject(subject);//sets subject of email
				//sets the content body of the email
				msg.setContent(finalEmailString+"<br>"+openRecords+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit", "text/html; charset=utf-8");				//sets the content body of the email
				msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(allEmailIds));//to email address
				Transport.send(msg);//sends the entire message object 
			}
			
			createHtmlFile("Submissions","Submissions turnaround time",finalEmailString+table+"<br>"+openRecords+"<br><br>"+ "Regards"+"<br>"+"AJ Recruit");

			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    private int median(Integer[] values) {
    	if (values == null)
    		return 0;
    	int middle = values.length/2;
        if (values.length%2 == 1) {
            return values[middle];
        } else {
            return (int) ((values[middle-1] + values[middle]) / 2.0);
        }
	}

    public int median (List<Integer> a){
    	if (a.size() == 0)
    		return 0;
    	else if (a.size() ==1)
    		return a.get(0);
        int middle = a.size()/2;
 
        if (a.size() % 2 == 1) {
            return a.get(middle);
        } else {
           return (int) ((a.get(middle-1) + a.get(middle)) / 2.0);
        }
    }
    
	private void createHtmlFile(String newFileName,String htmlTitle, String htmlString) {
		try {
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("appSettings.properties"));
			String path = properties.getProperty("submissionReportPath");
			System.out.println("----------Delete older files-------------");
	        File destFolder = new File(path);
			if(!destFolder.exists()) destFolder.mkdirs();
			//delete old files 
			File[] filesList = destFolder.listFiles();  
			for (int n = 0; n < filesList.length; n++) {
				if (filesList[n].isFile()){
					Date lastmodified = new Date(filesList[n].lastModified());
					if ((new Date().getTime() - lastmodified.getTime())/(1000*60*60*24) > 30) {
						filesList[n].delete();
					}
				}
			}
			System.out.println("----------Deleted older files-------------");
			
			
			System.out.println("----------Creating report file-------------");
			//create text file
		    File file = new File(path+"test.txt");
		    if (!file.exists()) {
		    	file.createNewFile();
			}
		    BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		    bw.write(htmlString);
		    bw.close();
		    Date date = new Date();
		    String currentDate= new SimpleDateFormat("MM-dd-yyyy").format(date);
		    //tranfer text file into html file
		    BufferedReader txtfile = new BufferedReader(new FileReader(path+"test.txt"));
            OutputStream htmlfile= new FileOutputStream(new File(path+newFileName+"-"+currentDate+".html"));
            PrintStream printhtml = new PrintStream(htmlfile);

            String[] txtbyLine = new String[500];
            String temp = "";
            String txtfiledata = "";

            String htmlheader="<html><head>";
            htmlheader+="<title>"+htmlTitle+"</title>";
            htmlheader+="</head><body>";
            String htmlfooter="</body></html>";
            int linenum = 0 ;

            while ((txtfiledata = txtfile.readLine())!= null)
               {
                    txtbyLine[linenum] = txtfiledata;
                    linenum++;
                } 
            for(int i=0;i<linenum;i++)
                {
                    if(i == 0)
                    {
                        temp = htmlheader + txtbyLine[0];
                        txtbyLine[0] = temp;
                    }
                    if(linenum == i+1)
                    {
                        temp = txtbyLine[i] + htmlfooter;
                        txtbyLine[i] = temp;
                    }
                    printhtml.println(txtbyLine[i]);
                }
            System.out.println("--------------html file created ");
        printhtml.close();
        htmlfile.close();
        txtfile.close();
        file.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static int day(int M, int D, int Y) {
        int y = Y - (14 - M) / 12;
        int x = y + y/4 - y/100 + y/400;
        int m = M + 12 * ((14 - M) / 12) - 2;
        int d = (D + x + (31*m)/12) % 7;
        return d;
    }

    // return true if the given year is a leap year
    public static boolean isLeapYear(int year) {
        if  ((year % 4 == 0) && (year % 100 != 0)) return true;
        if  (year % 400 == 0) return true;
        return false;
    }
    
    static int toNearestHour(String time) throws ParseException {
    	int hours = Integer.parseInt(time.split(":")[0]);
    	int minutes = Integer.parseInt(time.split(":")[1]);
        if (minutes >= 30)
        	hours++;
        return hours;
    }

}
