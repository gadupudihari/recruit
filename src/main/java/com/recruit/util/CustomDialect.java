package com.recruit.util;

import org.hibernate.dialect.MySQLDialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

public class CustomDialect extends MySQLDialect {

	public CustomDialect(){
	    registerFunction("group_concat", new StandardSQLFunction("group_concat", StringType.INSTANCE));	 
	    registerFunction("FIND_IN_SET", new StandardSQLFunction("FIND_IN_SET", IntegerType.INSTANCE));
	}
}