package com.recruit.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;

public class CheckReminder implements Job{

	@SuppressWarnings("deprecation")
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			Connection conn = null;

			String url = "";
			String driver = "";
			String userName = "";
			String password = "";
			
			//Retrieving database url, username, password from jdbc.properties
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("jdbc.properties"));
			url = properties.getProperty("jdbc.databaseurl");
			driver = properties.getProperty("jdbc.driverClassName");
			userName = properties.getProperty("jdbc.username");
			password = properties.getProperty("jdbc.password");
			if (url.contains("autoReconnect")) 				
				url = url.substring(0,url.indexOf("autoReconnect")-1);
			
			Class.forName(driver).newInstance();
			conn = DriverManager
					.getConnection(url, userName, password);
			System.out.println("------------ CheckReminder -------------");
			System.out.println("------------ "+UtilityHelper.getDate()+" -------------");
			// this will create the jobs that before 1 hour and 2 hours to the interview time 
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT * FROM recruit_interviews i where interviewDate = curdate() and time between ADDTIME(curtime(), '2:00:00') and ADDTIME(curtime(), '2:59:59') group by time");

			SchedulerFactory schedulerFactory =new StdSchedulerFactory();
			Scheduler sched=schedulerFactory.getScheduler();

			int i = 0;
			while (rs.next()) {
				Time time = rs.getTime("time");
				java.util.Calendar cal = new java.util.GregorianCalendar();
				cal.set(Calendar.AM_PM, 0);
				cal.set(Calendar.HOUR, time.getHours());
				cal.set(Calendar.MINUTE, time.getMinutes());
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);

				// set job before 1 hours to interview time
/*				cal.add(Calendar.HOUR, -1);
				Date startTime = cal.getTime();
				System.out.println("before 1 hour------------"+startTime);
				sched.deleteJob("myjob"+i,"Group"+i);
				SimpleTrigger st1=new SimpleTrigger(cal.getTime().toString(),"scheduledgroup"+i,startTime,null,SimpleTrigger.REPEAT_INDEFINITELY,86400L*1000L);
				JobDetail jd1=new JobDetail(cal.getTime().toString(),"Group"+i,SendReminder.class);
				sched.scheduleJob(jd1, st1);
				sched.start();
				i++;
				System.out.println(cal.getTime());
*/
				// set job before 2 hours to interview time
				cal.add(Calendar.HOUR, -1);
				Date startTime= cal.getTime();
				System.out.println("before 2 hours------------"+startTime);
				sched.deleteJob("myjob"+i,"Group"+i);
				SimpleTrigger st2=new SimpleTrigger(cal.getTime().toString(),"scheduledgroup"+i,startTime,null,SimpleTrigger.REPEAT_INDEFINITELY,86400L*1000L);
				JobDetail jd2=new JobDetail(cal.getTime().toString(),"Group"+i,SendReminder.class);
				sched.scheduleJob(jd2, st2);
				sched.start();
				i++;
				System.out.println(cal.getTime());
			}
				
			stmt.close();
			conn.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
}
