// default package
package com.recruit.authority.integration;

// Generated 30 Aug, 2012 5:31:31 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.authority.service.Authority;

/**
 * Home object for domain model class Authority.
 * @see .Authority
 * @author Hibernate Tools
 */
@Repository
public class AuthorityDAO {

	private static final Log log = LogFactory.getLog(AuthorityDAO.class);

	@Autowired
	private SessionFactory sessionFactory;// = getSessionFactory();

	public void persist(Authority transientInstance) {
		log.debug("persisting Authority instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Authority instance) {
		log.debug("attaching dirty Authority instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	@SuppressWarnings("deprecation")
	public void attachClean(Authority instance) {
		log.debug("attaching clean Authority instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Authority persistentInstance) {
		log.debug("deleting Authority instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Authority merge(Authority detachedInstance) {
		log.debug("merging Authority instance");
		try {
			Authority result = (Authority) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Authority findById(java.lang.String id) {
		log.debug("getting Authority instance with id: " + id);
		try {
			Authority instance = (Authority) sessionFactory.getCurrentSession()
					.get("com.recruit.authority.service.Authority", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findByExample(Authority instance) {
		log.debug("finding Authority instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.authority.service.Authority").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public void deleteAuthority(String loginId) {
		log.debug("deleting Authority instance with id: " + loginId);
		try {
			String query = "delete from recruit_authority where loginId = '"+loginId +"'";
			 sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Authority> getAuthorityDetails() {
		try {
			String sql = "from Authority where 1=1 group by authority";
			List<Authority> results = sessionFactory.getCurrentSession()
					.createQuery(sql).list();
			if (results.size()>0) {
				//System.out.println("authority list size is"+results.size());
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return results;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
