package com.recruit.requirement.presentation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.client.service.Client;
import com.recruit.requirement.service.Requirement;
import com.recruit.requirement.service.Sub_Vendor;
import com.recruit.user.service.User;
import com.recruit.util.SqlTimestampConverter;

public class RequirementsParser {

	public static List<Requirement> parseJSON(String json){
        List<Requirement> requirements = new ArrayList<Requirement>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	Requirement requirement = new Requirement();
            	if(rObj.has("id")){
            		requirement.setId(rObj.getInt("id"));     
            	}
            	if(rObj.has("postingDate") && !rObj.getString("postingDate").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		requirement.setPostingDate((Timestamp)c.fromString(rObj.getString("postingDate")));
            	}
            	if(rObj.has("created") && !rObj.getString("created").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		requirement.setCreated((Timestamp)c.fromString(rObj.getString("created")));
            	}
            	if(rObj.has("lastUpdated") && !rObj.getString("lastUpdated").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		requirement.setLastUpdated((Timestamp)c.fromString(rObj.getString("lastUpdated")));
            	}
            	if (rObj.has("lastUpdatedUser")) {
            		requirement.setLastUpdatedUser(rObj.getString("lastUpdatedUser"));
				}
            	if (rObj.has("researchComments")) {
					requirement.setResearchComments(rObj.getString("researchComments"));
				}
            	if (rObj.has("location")) {
					requirement.setLocation(rObj.getString("location"));
				}
            	if (rObj.has("module")) {
					requirement.setModule(rObj.getString("module"));
				}
            	if (rObj.has("communication")) {
					requirement.setCommunication(rObj.getString("communication"));
				}
            	if (rObj.has("requirement")) {
					requirement.setRequirement(rObj.getString("requirement"));
				}
            	if (rObj.has("source")) {
					requirement.setSource(rObj.getString("source"));
				}
            	if (rObj.has("addInfo_Todos")) {
					requirement.setAddInfo_Todos(rObj.getString("addInfo_Todos"));
				}
            	if (rObj.has("hotness")) {
					requirement.setHotness(rObj.getString("hotness"));
				}
            	if (rObj.has("comments")) {
					requirement.setComments(rObj.getString("comments"));
				}
            	if (rObj.has("rateSubmitted")) {
					requirement.setRateSubmitted(rObj.getString("rateSubmitted"));
				}
            	if (rObj.has("jobOpeningStatus")) {
					requirement.setJobOpeningStatus(rObj.getString("jobOpeningStatus"));
				}
            	if (rObj.has("postingTitle")) {
					requirement.setPostingTitle(rObj.getString("postingTitle"));
				}
            	if (rObj.has("employer")) {
					requirement.setEmployer(rObj.getString("employer"));
				}
            	if (rObj.has("publicLink")) {
					requirement.setPublicLink(rObj.getString("publicLink"));
				}
            	if (rObj.has("skillSet")) {
					requirement.setSkillSet(rObj.getString("skillSet"));
				}
            	if (rObj.has("resume")) {
					requirement.setResume(rObj.getInt("resume"));
				}
            	if (rObj.has("experience")) {
					requirement.setExperience(rObj.getString("experience"));
				}
            	if (rObj.has("helpCandidate")) {
					requirement.setHelpCandidate(rObj.getString("helpCandidate"));
				}
            	if (rObj.has("helpCandidateComments")) {
					requirement.setHelpCandidateComments(rObj.getString("helpCandidateComments"));
				}
            	if (rObj.has("targetRoles")) {
					requirement.setTargetRoles(rObj.getString("targetRoles"));
				}
            	if (rObj.has("secondaryModule")) {
					requirement.setSecondaryModule(rObj.getString("secondaryModule"));
				}
            	if (rObj.has("certifications")) {
					requirement.setCertifications(rObj.getString("certifications"));
				}
            	if (rObj.has("cityAndState")) {
					requirement.setCityAndState(rObj.getString("cityAndState"));
				}
            	if (rObj.has("remote")) {
					requirement.setRemote(rObj.getBoolean("remote"));
				}
            	if (rObj.has("relocate")) {
					requirement.setRelocate(rObj.getBoolean("relocate"));
				}
            	if (rObj.has("travel")) {
					requirement.setTravel(rObj.getBoolean("travel"));
				}
            	if (rObj.has("tAndEPaid")) {
					requirement.settAndEPaid(rObj.getBoolean("tAndEPaid"));
				}
            	if (rObj.has("tAndENotPaid")) {
					requirement.settAndENotPaid(rObj.getBoolean("tAndENotPaid"));
				}
            	if (rObj.has("version")) {
					requirement.setVersion(rObj.getInt("version"));
				}
            	if (rObj.has("requirementNo")) {
					requirement.setRequirementNo(rObj.getInt("requirementNo"));
				}
            	if (rObj.has("recruiter")) {
            		User recruiter = new User();
            		recruiter.setId(rObj.getInt("recruiter"));
					requirement.setRecruiterObj(recruiter);
				}
            	if (rObj.has("clientId")) {
            		Client client = new Client();
            		client.setId(rObj.getInt("clientId"));
            		requirement.setClient(client);
            	}
            	if (rObj.has("vendorId")) {
            		Set<Sub_Vendor> sub_Vendors = new HashSet<Sub_Vendor>(0);
					Sub_Vendor sub_Vendor = new Sub_Vendor();
					sub_Vendor.setVendorId(rObj.getInt("vendorId"));
					sub_Vendors.add(sub_Vendor);
					if (rObj.has("contactId"))
						sub_Vendor.setContactId(rObj.getInt("contactId"));
					requirement.setSub_Vendors(sub_Vendors);
				}
            	requirements.add(requirement);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return requirements;
	}

	
}
