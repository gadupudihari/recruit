package com.recruit.requirement.presentation;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.requirement.service.Requirement;
import com.recruit.requirement.service.Sub_Vendor;

public class Sub_VendorParser {

	public static List<Sub_Vendor> parseJSON(String json){
        List<Sub_Vendor> sub_Vendors = new ArrayList<Sub_Vendor>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	Sub_Vendor vendor = new Sub_Vendor();
            	if(rObj.has("id")){
            		vendor.setId(rObj.getInt("id"));     
            	}
            	if (rObj.has("vendorId")) {
            		vendor.setVendorId(rObj.getInt("vendorId"));
            	}
            	if (rObj.has("contactId")) {
            		vendor.setContactId(rObj.getInt("contactId"));
            	}
            	if (rObj.has("requirementId")) {
					Requirement requirement = new Requirement();
					requirement.setId(rObj.getInt("requirementId"));
					vendor.setRequirement(requirement);
				}
            	
            	sub_Vendors.add(vendor);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return sub_Vendors;
	}
	
}
