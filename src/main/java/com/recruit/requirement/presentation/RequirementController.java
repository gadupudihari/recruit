package com.recruit.requirement.presentation;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.recruit.base.presentation.BaseController;
import com.recruit.base.service.Param;
import com.recruit.base.service.ParamParser;
import com.recruit.base.service.SearchFilter;
import com.recruit.requirement.service.Requirement;
import com.recruit.requirement.service.RequirementService;
import com.recruit.requirement.service.Sub_Requirement;
import com.recruit.requirement.service.Sub_Vendor;
import com.recruit.requirement.service.WorkList;
import com.recruit.research.presentation.ResearchParser;
import com.recruit.research.service.Research;
import com.recruit.util.JSONException;
import com.recruit.util.JSONParser;
import com.recruit.util.JsonParserUtility;
import com.recruit.util.UtilityHelper;
import com.recruit.util.WriteExcel;

@Controller
@RequestMapping("/requirement")
@PreAuthorize("hasAnyRole('ADMIN','RECRUITER')")
public class RequirementController extends BaseController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	RequirementService requirementService;

	@Autowired
	WriteExcel writeExcel;
	
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	
	@RequestMapping(value = "/getRequirements", method = {RequestMethod.POST,RequestMethod.GET })
	public String getRequirements(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Requirement> requirements = requirementService.getRequirements(filter);
		return returnJson(map, JsonParserUtility.toRequirementJSON(requirements).toString());
	}

	@RequestMapping(value = "/getAllRequirements", method = {RequestMethod.POST,RequestMethod.GET })
	public String getAllRequirements(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Requirement> requirements = requirementService.getAllRequirements(filter);
		return returnJson(map, requirements);
	}

	@RequestMapping(value = "/saveRequirements",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveRequirements(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		List<Requirement> requirements = RequirementsParser.parseJSON(json);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		requirementService.saveRequirements(requirements,loginId,createdDate);
		response.setSuccess(Boolean.TRUE);
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/deleteRequirement/{id}", method = RequestMethod.GET)
	public String deleteRequirement(@PathVariable("id") Integer id, Map<String, Object> map){
		requirementService.deleteRequirement(response,id);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/saveRequirement",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveRequirement(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		Requirement requirement = (Requirement) JSONParser.parseJSON(json, Requirement.class);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		requirementService.saveRequirement(requirement,loginId,createdDate,response);
		
		response.setReturnVal(JsonParserUtility.toJSON(requirement).toString());
		return returnJson(map, response);
	}

	@RequestMapping(value = "/excelExport", method = {RequestMethod.POST,RequestMethod.GET })
	public String excelExport(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request,HttpServletResponse res) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Requirement> requirements = requirementService.getRequirements(filter);
		Map<String, Param> filterMap = filter.getParamMap();
		String columns[] = filterMap.get("columns").getStrValue().split(",");
		String contextPath="", fileName = "Job Openings";
		  try{
		      ServletContext servletContext = session.getServletContext();
		      contextPath = servletContext.getRealPath(File.separator);
		      System.out.println("File system context path (in TestFilter): " + contextPath);
		      System.out.println("---------------------"+requirements.size());
		      
		      //WriteExcel test = new WriteExcel();
		      UtilityHelper.deleteExcelFiles(contextPath, "Job Openings");
		      
		      writeExcel.setOutputFile(contextPath+fileName+".xls");
		      //Writing to Excel File
		      writeExcel.writeRequirments(requirements,columns);
			  System.out.println("Download Path ------ " + contextPath+fileName+".xls");
		  }
		  catch(Exception ex){
			  System.err.println("---------- **** Exception while generating Excel file **** ---------");
			  System.err.println(ex.getMessage());
			  ex.printStackTrace();
			  response.setSuccess(false);
		  }
		  response.setSuccess(true);
		  response.setReturnVal(fileName+".xls");
		  return returnJson(map, response);
	}

	@RequestMapping(value = "/removeCandidate", method = {RequestMethod.POST,RequestMethod.GET })
	public String removeCandidate(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		int id = Integer.parseInt(json);
		requirementService.removeCandidate(id);
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/removeVendor", method = {RequestMethod.POST,RequestMethod.GET })
	public String removeVendor(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		requirementService.removeSubVendor(response,Integer.parseInt(json));
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/getTasks", method = {RequestMethod.POST,RequestMethod.GET })
	public String getTasks(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Research> list = requirementService.getTasks(filter);
		response.setSuccess(true);
		return returnJson(map, JsonParserUtility.toResearchJson(list).toString());
	}
	
	@RequestMapping(value = "/saveTask",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveTask(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		List<Research> researchList = ResearchParser.parseJSON(json);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		requirementService.saveTask(researchList,loginId,createdDate);
		response.setSuccess(Boolean.TRUE);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/deleteTask/{id}", method = RequestMethod.GET)
	public String deleteTask(@PathVariable("id") Integer id, Map<String, Object> map){
		requirementService.deleteTask(id);
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/getSubRequirements", method = {RequestMethod.POST,RequestMethod.GET })
	public String getSubRequirements(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		String loginId =UtilityHelper.getLoginId(request);
		List<Sub_Requirement> list = requirementService.getSubRequirements(filter,loginId);
		response.setSuccess(true);
		return returnJson(map, list);
	}
	
	@RequestMapping(value = "/saveSubRequirements",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveSubRequirements(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		List<Sub_Requirement> requirements = Sub_RequirementParser.parseJSON(json);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		requirementService.saveSubRequirements(response,loginId,createdDate,requirements);
		return returnJson(map, response);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getSubVendors", method = {RequestMethod.POST,RequestMethod.GET })
	public String getSubVendors(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List list = requirementService.getSubVendors(filter);
		response.setSuccess(true);
		return returnJson(map, JsonParserUtility.toSub_VendorJson(list).toString());
	}

	@RequestMapping(value = "/saveSubVendors",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveSubVendors(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		List<Sub_Vendor> requirements = Sub_VendorParser.parseJSON(json);
		requirementService.saveSubVendors(requirements);
		response.setSuccess(Boolean.TRUE);
		response.setReturnVal(requirements);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/getPendingJobs", method = {RequestMethod.POST,RequestMethod.GET })
	public String getPendingJobs(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		List<Requirement> requirements = requirementService.getPendingJobs();
		response.setReturnVal(JsonParserUtility.toRequirementJSON(requirements).toString());
		return returnJson(map, response);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getShortlisted", method = {RequestMethod.POST,RequestMethod.GET })
	public String getShortlisted(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List list = requirementService.getShortlisted(filter);
		response.setSuccess(true);
		return returnJson(map, list);
	}

	@RequestMapping(value = "/saveShortlisted",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveShortlisted(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		List<Sub_Requirement> candidates = Sub_RequirementParser.parseJSON(json);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		requirementService.saveShortlisted(response,loginId,createdDate,candidates);
		response.setReturnVal(candidates);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/removeShortlist", method = {RequestMethod.POST,RequestMethod.GET })
	public String removeShortlist(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		int id = Integer.parseInt(json);
		requirementService.removeShortlist(id);
		response.setSuccess(true);
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/removeShortlistedJob", method = {RequestMethod.POST,RequestMethod.GET })
	public String removeShortlistedJob(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		requirementService.removeShortlistedJob(filter);
		response.setSuccess(true);
		return returnJson(map, response);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getSuggestedCriteria", method = {RequestMethod.POST,RequestMethod.GET })
	public String getSuggestedCriteria(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		int id = Integer.parseInt(json);
		List list = requirementService.getSuggestedCriteria(id);
		response.setReturnVal(JsonParserUtility.toSuggestedCriteria(list).toString());
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getPotentialCriteria", method = {RequestMethod.POST,RequestMethod.GET })
	public String getPotentialCriteria(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List list = requirementService.getPotentialCriteria(filter);
		response.setReturnVal(JsonParserUtility.toPotentialCriteria(list).toString());
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping( value ="/getWorkList", method = {RequestMethod.POST,RequestMethod.GET})
	public String getWorkList(@RequestParam("json") String json, Map<String, Object> map){
		SearchFilter filter = ParamParser.parse(json);
		List<WorkList> list = requirementService.getWorkList(filter);
		return returnJson(map, JsonParserUtility.toWorkListJSON(list).toString());
	}
	
	@RequestMapping(value = "/saveWorkList", method = { RequestMethod.POST,RequestMethod.GET})
	public String saveWorkList(@RequestParam("json") String json, Map<String, Object> map, HttpServletRequest request){
		List<WorkList> list = WorkListParser.parseJSON(json);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		requirementService.saveWorkList(response,list,loginId,createdDate);
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/getRequirementNo", method = { RequestMethod.POST,RequestMethod.GET})
	public String getRequirementNo(@RequestParam("json") String json, Map<String, Object> map, HttpServletRequest request){
		requirementService.getRequirementNo(response);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/getHighContacts", method = {RequestMethod.POST,RequestMethod.GET })
	public String getHighContacts(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Sub_Requirement> list = requirementService.getHighContacts(filter);
		response.setSuccess(true);
		return returnJson(map, list);
	}

	
}
