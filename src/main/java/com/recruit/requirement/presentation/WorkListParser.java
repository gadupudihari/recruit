package com.recruit.requirement.presentation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.requirement.service.Requirement;
import com.recruit.requirement.service.WorkList;
import com.recruit.util.SqlTimestampConverter;

public class WorkListParser {

	public static List<WorkList> parseJSON(String json){
        List<WorkList> workLists = new ArrayList<WorkList>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	WorkList list = new WorkList();
            	if(rObj.has("id")){
            		list.setId(rObj.getInt("id"));     
            	}
            	if (rObj.has("userId")) {
            		list.setUserId(rObj.getInt("userId"));
            	}
            	if (rObj.has("assignedUserId")) {
            		list.setAssignedUserId(rObj.getInt("assignedUserId"));
            	}
            	if (rObj.has("comments")) {
            		list.setComments(rObj.getString("comments"));
				}
            	if(rObj.has("created") && !rObj.getString("created").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		list.setCreated((Timestamp)c.fromString(rObj.getString("created")));
            	}
            	if(rObj.has("lastUpdated") && !rObj.getString("lastUpdated").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		list.setLastUpdated((Timestamp)c.fromString(rObj.getString("lastUpdated")));
            	}
            	if(rObj.has("deleted") && !rObj.getString("deleted").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		list.setDeleted((Timestamp)c.fromString(rObj.getString("deleted")));
            	}
            	if (rObj.has("createdUser")) {
            		list.setCreatedUser(rObj.getString("createdUser"));
				}
            	if (rObj.has("lastUpdatedUser")) {
            		list.setLastUpdatedUser(rObj.getString("lastUpdatedUser"));
				}
            	if (rObj.has("deletedUser")) {
            		list.setDeletedUser(rObj.getString("deletedUser"));
				}
            	if (rObj.has("requirementId")) {
					Requirement requirement = new Requirement();
					requirement.setId(rObj.getInt("requirementId"));
					list.setRequirement(requirement);
				}
            	
            	workLists.add(list);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return workLists;
	}
	
}
