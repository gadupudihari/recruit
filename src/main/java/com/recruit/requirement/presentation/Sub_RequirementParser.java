package com.recruit.requirement.presentation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.requirement.service.Requirement;
import com.recruit.requirement.service.Sub_Requirement;
import com.recruit.util.SqlTimestampConverter;

public class Sub_RequirementParser {

	public static List<Sub_Requirement> parseJSON(String json){
        List<Sub_Requirement> requirements = new ArrayList<Sub_Requirement>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	Sub_Requirement sub_Requirement = new Sub_Requirement();
            	if(rObj.has("id")){
            		sub_Requirement.setId(rObj.getInt("id"));     
            	}
            	if(rObj.has("submittedDate") && !rObj.getString("submittedDate").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		sub_Requirement.setSubmittedDate((Timestamp)c.fromString(rObj.getString("submittedDate")));
            	}
            	if (rObj.has("candidateId")) {
            		sub_Requirement.setCandidateId(rObj.getInt("candidateId"));
            	}
            	if (rObj.has("vendorId")) {
            		sub_Requirement.setVendorId(rObj.getInt("vendorId"));
            	}
            	if (rObj.has("lastUpdatedUser")) {
            		sub_Requirement.setLastUpdatedUser(rObj.getString("lastUpdatedUser"));
				}
            	if(rObj.has("created") && !rObj.getString("created").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		sub_Requirement.setCreated((Timestamp)c.fromString(rObj.getString("created")));
            	}
            	if(rObj.has("lastUpdated") && !rObj.getString("lastUpdated").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		sub_Requirement.setLastUpdated((Timestamp)c.fromString(rObj.getString("lastUpdated")));
            	}
            	if (rObj.has("statusUpdated") && !rObj.getString("statusUpdated").equals("null")) {
					SqlTimestampConverter c = new SqlTimestampConverter();
					sub_Requirement.setStatusUpdated((Timestamp) c.fromString(rObj.getString("statusUpdated")));
				}
            	if (rObj.has("submittedRate1")) {
					sub_Requirement.setSubmittedRate1(rObj.getString("submittedRate1"));
				}
            	if (rObj.has("submittedRate2")) {
					sub_Requirement.setSubmittedRate2(rObj.getString("submittedRate2"));
				}
            	if (rObj.has("submissionStatus")) {
					sub_Requirement.setSubmissionStatus(rObj.getString("submissionStatus"));
				}
            	if (rObj.has("projectInserted")) {
            		sub_Requirement.setProjectInserted(rObj.getBoolean("projectInserted"));
				}
            	if (rObj.has("comments")) {
					sub_Requirement.setComments(rObj.getString("comments"));
				}
            	if (rObj.has("resumeLink")) {
					sub_Requirement.setResumeLink(rObj.getString("resumeLink"));
				}
            	if (rObj.has("guide")) {
					sub_Requirement.setGuide(rObj.getString("guide"));
				}
            	if (rObj.has("reference")) {
					sub_Requirement.setReference(rObj.getString("reference"));
				}
            	if (rObj.has("deleted")) {
					sub_Requirement.setDeleted(rObj.getBoolean("deleted"));
				}
            	if (rObj.has("requirementId")) {
					Requirement requirement = new Requirement();
					requirement.setId(rObj.getInt("requirementId"));
					sub_Requirement.setRequirement(requirement);
				}
            	
            	requirements.add(sub_Requirement);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return requirements;
	}

	
}
