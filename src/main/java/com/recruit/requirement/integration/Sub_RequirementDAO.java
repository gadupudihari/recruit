package com.recruit.requirement.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.transform.Transformers;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.requirement.service.Sub_Requirement;


/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class Sub_RequirementDAO extends MySQL5Dialect{

	private static final Log log = LogFactory.getLog(Sub_RequirementDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Sub_Requirement transientInstance) {
		log.debug("persisting Sub_Requirements instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Sub_Requirement instance) {
		log.debug("attaching dirty Sub_Requirements instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Sub_Requirement persistentInstance) {
		log.debug("deleting Sub_Requirements instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Sub_Requirement merge(Sub_Requirement detachedInstance) {
		log.debug("merging Sub_Requirements instance");
		try {
			Sub_Requirement result = (Sub_Requirement) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Sub_Requirement findById(java.lang.Integer id) {
		log.debug("getting Sub_Requirements instance with id: " + id);
		try {
			Sub_Requirement instance = (Sub_Requirement) sessionFactory.getCurrentSession()
					.get("com.recruit.requirement.service.Sub_Requirement", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Sub_Requirement> findByExample(Sub_Requirement instance) {
		log.debug("finding Sub_Requirement instance by example");
		try {
			List<Sub_Requirement> results = (List<Sub_Requirement>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.requirement.service.Sub_Requirement").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings({"unchecked" })
	public List<Sub_Requirement> findByCriteria(SearchFilter filter) {
		try {
			String query = "select distinct s.*,r.postingDate,r.cityAndState reqCityAndState,r.location, r.postingTitle, concat(c.firstName,' ',c.lastName) candidate," +
					" c.expectedRate candidateExpectedRate,c.cityAndState,c.relocate,c.travel,c.availability,c.immigrationVerified," +
					"(select name from recruit_clients where id=s.vendorId) vendor,(select name from recruit_clients where id=r.clientId) client, " +
					"r.addInfo_Todos addInfoTodos,r.module,r.hotness,c.resumeHelp,c.interviewHelp,c.jobHelp,r.clientId,c.emailId,c.alert," +
					"(select CAST(contactId as char) from recruit_sub_vendors where requirementId=r.id and vendorId=s.vendorId) contactId," +
					"(select date_format(max(interviewDate),'%m/%d/%Y') from recruit_interviews i where candidateId=c.id and vendorId = s.vendorId " +
					"and requirementId=r.id and i.clientId=r.clientId and s.submissionstatus ='I-Scheduled') interviewDate," +
					"(select max(approxStartDate) from recruit_interviews i where candidateId=c.id and vendorId = s.vendorId " +
					"and requirementId=r.id and i.clientId=r.clientId and s.submissionstatus ='C-Accepted') projectStartDate " +
					"from recruit_sub_requirements s,recruit_requirements r,recruit_candidate c where 1=1 and r.id = s.requirementId and c.id=s.candidateId";
			
			Map<String, Param> map = filter.getParamMap();

			if (map.get("requirementId") != null) {
				query += " and s.requirementId like '"+map.get("requirementId").getStrValue()+"'";
			}
			if (map.get("vendorId") != null) {
				query += " and s.vendorId like '"+map.get("vendorId").getStrValue()+"'";
			}
			if (map.get("clientId") != null) {
				query += " and r.clientId = '"+map.get("clientId").getStrValue()+"'";
			}
			if (map.get("candidateId") != null) {
				query += " and s.candidateId in ("+map.get("candidateId").getStrValue()+")";
			}
			if (map.get("hotness") != null) {
				query += " and r.hotness in (" + map.get("hotness").getStrValue()+ ")";
			}
			if (map.get("submissionStatus") != null) {
				query += " and s.submissionStatus in ("+map.get("submissionStatus").getStrValue()+")";
			}
			if (map.get("submittedDateFrom") != null) {
				query += " and date(s.submittedDate) >= date('"+map.get("submittedDateFrom").getStrValue()+"')";
			}
			if (map.get("submittedDateTo") != null) {
				query += " and date(s.submittedDate) <= date('"+map.get("submittedDateTo").getStrValue()+"')";
			}
			if (map.get("deleted") != null && map.get("deleted").getStrValue().equals("false")) {
				query += " and s.deleted = false";
			}
			if (map.get("table") != null) {
				if (map.get("table").getStrValue().equalsIgnoreCase("submissions")) 
					query += " and s.submissionStatus != 'Shortlisted'";
				else if (map.get("table").getStrValue().equalsIgnoreCase("Shortlisted"))
					query += " and s.submissionStatus = 'Shortlisted'";
			}
			if (map.get("order") != null) {
				query += " order by "+map.get("order").getStrValue();
			}
			if (map.get("orderby") != null) {
				query += " "+map.get("orderby").getStrValue();
			}

			System.out.println("Sub Requirements : "+query);
			List<Sub_Requirement> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.setResultTransformer(Transformers.aliasToBean(Sub_Requirement.class))
					.setMaxResults(filter.getPageSize())
					.list();

			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Sub_Requirement> findByVendor(Integer vendorId, Integer requirementId) {
		try {
			String query = "from Sub_Requirement t where 1=1 and t.requirement.id like '"+requirementId+"' and vendorId like '"+vendorId+"'";

			System.out.println("Research : "+query);
			List<Sub_Requirement> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean checkExist(Sub_Requirement sub_Requirement) {
		try {
			String query = "from Sub_Requirement t where 1=1 and t.submissionStatus != 'Shortlisted' and t.deleted = false " +
					" and t.requirement.id like '"+sub_Requirement.getRequirement().getId()+
					"' and candidateId like '"+sub_Requirement.getCandidateId()+"'";
			if(sub_Requirement.getId() != null)
				query += " and id !="+sub_Requirement.getId();

			System.out.println("Research : "+query);
			List<Sub_Requirement> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			if(list.size() > 0)
				return true;
			return false;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Sub_Requirement findShortlisted(Sub_Requirement sub_Requirement) {
		try {
			String query = "from Sub_Requirement t where 1=1 and t.submissionStatus = 'Shortlisted' and t.deleted = false " +
					" and t.requirement.id like '"+sub_Requirement.getRequirement().getId()+
					"' and candidateId like '"+sub_Requirement.getCandidateId()+"'";
			if(sub_Requirement.getId() != null)
				query += " and id !="+sub_Requirement.getId();

			List<Sub_Requirement> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			if(list.size() > 0)
				return list.get(0);
			return null;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Sub_Requirement findExisting(Sub_Requirement sub_Requirement) {
		try {
			String query = "from Sub_Requirement t where 1=1 and t.deleted = false " +
					" and t.requirement.id like '"+sub_Requirement.getRequirement().getId()+
					"' and candidateId like '"+sub_Requirement.getCandidateId()+"'";
			if(sub_Requirement.getId() != null)
				query += " and id !="+sub_Requirement.getId();

			List<Sub_Requirement> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			if(list.size() > 0)
				return list.get(0);
			return null;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	public void removeDeletedShortList(Sub_Requirement candidate) {
		try {
			String query = "delete from recruit_sub_requirements " +
					"where requirementId = "+candidate.getRequirement().getId()+" and candidateId = "+candidate.getCandidateId()+
					" and submissionStatus = 'Shortlisted' and deleted = true ";
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
		} catch (RuntimeException re) {
			log.error("find by employee Id failed", re);
			throw re;
		}
	}
	
	public void removeDeletedCandidate(Sub_Requirement candidate) {
		try {
			String query = "delete from recruit_sub_requirements " +
					"where requirementId = "+candidate.getRequirement().getId()+" and candidateId = "+candidate.getCandidateId()+
					" and submissionStatus != 'Shortlisted' and deleted = true ";
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
		} catch (RuntimeException re) {
			log.error("find by employee Id failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Sub_Requirement> getHighContacts(SearchFilter filter) {
		try {
			String query = "select * from (select s.id,s.vendorId,r.clientId,s.candidateId,CAST(v.contactId as char) contactId,s.statusUpdated,co.score," +
					" s.submissionStatus,s.requirementId,r.module,concat(c.firstName,' ',c.lastName) candidate,concat(co.firstName,' ',co.lastName) contact," +
					" (select name from recruit_clients where id=s.vendorId) vendor,(select name from recruit_clients where id=r.clientId) client" +
					" from recruit_sub_requirements s,recruit_requirements r,recruit_candidate c,recruit_sub_vendors v,recruit_contacts co" +
					" where r.id = s.requirementId and c.id=s.candidateId and r.id=v.requirementId and v.vendorId=s.vendorId and v.contactId = co.id" +
					" and submissionStatus in ('I-Scheduled','I-Succeeded','I-Withdrawn','I-Failed','C-Accepted','C-Declined') and deleted != true" +
					" and contactId is not null order by contactId,statusUpdated desc) tab group by contactId";
			
			List<Sub_Requirement> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.setResultTransformer(Transformers.aliasToBean(Sub_Requirement.class))
					.setMaxResults(filter.getPageSize())
					.list();

			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}
}