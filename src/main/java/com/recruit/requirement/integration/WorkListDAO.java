package com.recruit.requirement.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.requirement.service.WorkList;


/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class WorkListDAO {

	private static final Log log = LogFactory.getLog(RequirementsDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(WorkList  transientInstance) {
		log.debug("persisting WorkList instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(WorkList instance) {
		log.debug("attaching dirty WorkList instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(WorkList persistentInstance) {
		log.debug("deleting WorkList instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public WorkList merge(WorkList detachedInstance) {
		log.debug("merging WorkList instance");
		try {
			WorkList result = (WorkList) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public WorkList findById(java.lang.Integer id) {
		log.debug("getting WorkList instance with id: " + id);
		try {
			WorkList instance = (WorkList) sessionFactory.getCurrentSession()
					.get("com.recruit.requirement.service.WorkList", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<WorkList> findByCriteria(SearchFilter filter) {
		try {
			String query = "select t from WorkList t where 1=1 ";
			
			Map<String, Param> map = filter.getParamMap();

			if (map.get("requirementId") != null) {
				query += " and s.requirement.id like '"+map.get("requirementId").getStrValue()+"'";
			}

			System.out.println("WorkList : "+query);
			List<WorkList> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.setMaxResults(filter.getPageSize())
					.list();

			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}


}