package com.recruit.requirement.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.requirement.service.Sub_Certification;


/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class Sub_CertificationDAO {

	private static final Log log = LogFactory.getLog(Sub_CertificationDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Sub_Certification  transientInstance) {
		log.debug("persisting Sub_Certification instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Sub_Certification instance) {
		log.debug("attaching dirty Sub_Certification instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Sub_Certification persistentInstance) {
		log.debug("deleting Sub_Certification instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Sub_Certification merge(Sub_Certification detachedInstance) {
		log.debug("merging Sub_Certification instance");
		try {
			Sub_Certification result = (Sub_Certification) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Sub_Certification findById(java.lang.Integer id) {
		log.debug("getting Sub_Certification instance with id: " + id);
		try {
			Sub_Certification instance = (Sub_Certification) sessionFactory.getCurrentSession()
					.get("com.recruit.requirement.service.Sub_Certification", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Sub_Certification> findByExample(Sub_Certification instance) {
		log.debug("finding Requirement instance by example");
		try {
			List<Sub_Certification> results = (List<Sub_Certification>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.requirement.service.Sub_Certification").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findByCriteria(SearchFilter filter) {
		try {
			String query = "select distinct s.* from recruit_Sub_Certifications s where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("requirementId") != null) {
				query += " and s.requirementId like '"+map.get("requirementId").getStrValue()+"'";
			}

			System.out.println("Research : "+query);
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	public void deleteByRequirement(String reqId) {
		try {
			String query = "delete from recruit_Sub_Certifications where requirementId = "+reqId;
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
		} catch (RuntimeException re) {
			log.error("find by employee Id failed", re);
			throw re;
		}
	}

}