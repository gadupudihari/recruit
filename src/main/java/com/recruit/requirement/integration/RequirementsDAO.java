package com.recruit.requirement.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.requirement.service.Requirement;


/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class RequirementsDAO extends MySQL5Dialect  {

	private static final Log log = LogFactory.getLog(RequirementsDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Requirement transientInstance) {
		log.debug("persisting Requirement instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Requirement instance) {
		log.debug("attaching dirty Requirement instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Requirement persistentInstance) {
		log.debug("deleting Requirement instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Requirement merge(Requirement detachedInstance) {
		log.debug("merging Requirement instance");
		try {
			Requirement result = (Requirement) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Requirement findById(java.lang.Integer id) {
		log.debug("getting Requirement instance with id: " + id);
		try {
			Requirement instance = (Requirement) sessionFactory.getCurrentSession()
					.get("com.recruit.requirement.service.Requirement", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Requirement> findByExample(Requirement instance) {
		log.debug("finding Requirement instance by example");
		try {
			List<Requirement> results = (List<Requirement>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.requirement.service.Requirement").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Requirement> findByCandidateId(int candidateId) {
		try {
			String query = " from Requirement t where 1=1 and id in (select distinct s.requirement.id from Sub_Requirement s where s.candidateId ="+ candidateId +")" ;
			
			
			System.out.println("Requirement : "+query);
			List<Requirement> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Requirement> findByVendorId(int vendorId) {
		try {
			String query = " from Requirement t where 1=1 and id in (select distinct s.requirement.id from Sub_Vendor s where s.vendorId ="+ vendorId +")" ;
			
			
			System.out.println("Requirement : "+query);
			List<Requirement> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Requirement> findByCriteria(SearchFilter filter) {

		//myConf.addSqlFunction("group_concat", new StandardSQLFunction("group_concat", new StringType()));		
	    registerFunction("group_concat", new StandardSQLFunction("group_concat", StringType.INSTANCE) );  
		try {
			String query = " from Requirement t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("id") != null) {
				if (map.get("id").getStrValue().contains(",")) 
					query += " and t.id in(" +map.get("id").getStrValue()+ ")";
				else
					query += " and t.id like '" +map.get("id").getStrValue()+ "'";
			}
			if (map.get("candidateId") != null) {
				query += " and id in (select distinct s.requirement.id from Sub_Requirement s where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
						"and s.candidateId ="+ map.get("candidateId").getStrValue() +")" ;
			}
			if (map.get("shortlistedCandidateId") != null) {
				query += " and id in (select distinct s.requirement.id from Sub_Requirement s where s.submissionStatus = 'Shortlisted' and s.deleted = false " +
						"and s.candidateId ="+ map.get("shortlistedCandidateId").getStrValue() +")" ;
			}
			if (map.get("vendor") != null) {
				query += " and id in (select distinct s.requirement.id from Sub_Vendor s where s.vendorId ="+ map.get("vendor").getStrValue() +")" ;
			}
			if (map.get("client") != null) {
				query += " and t.client.id like '" + map.get("client").getStrValue()+ "'";
			}
			if (map.get("hotness") != null) {
				query += " and t.hotness in (" + map.get("hotness").getStrValue()+ ")";
			}
			if (map.get("module") != null) {
				query += " and concat(t.module,',',t.secondaryModule) like '%" + map.get("module").getStrValue()+ "%'";
			}
			if (map.get("jobOpeningStatus") != null) {
				query += " and t.jobOpeningStatus in (" + map.get("jobOpeningStatus").getStrValue()+ ")";
			}
			if (map.get("submittedDate") != null) {
				query += " and t.submissionDate like '%"+ map.get("submittedDate").getStrValue()+"%' " ;
			}
			if (map.get("submittedDateFrom") != null) {
				query += " and t.id in(select s.requirement.id from Sub_Requirement s where date(s.submittedDate) >= date('"+map.get("submittedDateFrom").getStrValue()+"'))";
			}
			if (map.get("submittedDateTo") != null) {
				query += " and t.id in(select s.requirement.id from Sub_Requirement s where date(s.submittedDate) <= date('"+map.get("submittedDateTo").getStrValue()+"'))";
			}
			if (map.get("statusUpdatedFrom") != null) {
				query += " and t.id in(select s.requirement.id from Sub_Requirement s where date(s.statusUpdated) >= date('"+map.get("statusUpdatedFrom").getStrValue()+"'))";
			}
			if (map.get("statusUpdatedTo") != null) {
				query += " and t.id in(select s.requirement.id from Sub_Requirement s where date(s.statusUpdated) <= date('"+map.get("statusUpdatedTo").getStrValue()+"'))";
			}
			if (map.get("postingDateFrom") != null) {
				query += " and date(t.postingDate) >= date('"+map.get("postingDateFrom").getStrValue()+"')";
			}
			if (map.get("postingDateTo") != null) {
				query += " and date(t.postingDate) <= date('"+map.get("postingDateTo").getStrValue()+"')";
			}
			if (map.get("candidateLastUpdated") != null) {
				query += " and t.id in(select distinct s.requirement.id from Sub_Requirement s where date(s.lastUpdated) = '"+map.get("candidateLastUpdated").getStrValue()+"')";
			}
			if (map.get("exceptShortlist") != null) {
				query += " and t.id not in(select distinct s.requirement.id from Sub_Requirement s where s.submissionStatus = 'Shortlisted' and s.deleted = false " +
						"and s.candidateId = "+map.get("exceptShortlist").getStrValue()+")";
			}
			if (map.get("nextJob") != null) {
				query += " and t.id=(SELECT r.id FROM Requirement r where r.id > "+map.get("nextJob").getStrValue()+" order by id limit 1 )";
			}
			if (map.get("reference") != null ) {
				query += " and t.id in (select s.requirement.id from Sub_Requirement s where concat(',',s.reference,',') like '%,"+map.get("reference").getStrValue()+",%')";
			}
			if (map.get("cityAndState") != null) {
				query += " and t.cityAndState like '%"+map.get("cityAndState").getStrValue()+"%'";
			}
			if (map.get("search") != null) {
				String concatString = "concat_ws (hotness,module,postingTitle,jobOpeningStatus,lastUpdatedUser,comments,employer,location,requirement,communication," +
						"addInfo_Todos,source,rateSubmitted,postingTitle,publicLink,researchComments,secondaryModule, resume,experience,helpCandidate," +
						"helpCandidateComments,date_format(postingDate,'%m/%d/%Y'),date_format(created,'%m/%d/%Y'),date_format(lastUpdated,'%m/%d/%Y')," +
						"(select group_concat(concat(firstName,' ',lastName)) from Candidate c,Sub_Requirement s where s.candidateId = c.id and s.requirement.id = t.id)," +
						"(select group_concat(name) from Client c,Sub_Vendor s where s.vendorId = c.id and s.requirement.id = t.id)," +
						"(select group_concat(concat(firstName,' ',lastName)) from Contact c,Sub_Vendor s where s.contactId = c.id and s.requirement.id = t.id)," +
						"(select name from Client c where c.id = t.client.id)) ";

				String searchQuery = concatString +" like '%"+map.get("search").getStrValue()+"%'";
				String words[] = map.get("search").getStrValue().split(" ");
				if (words.length > 1) {
					for (int i = 0; i < words.length; i++) {
						searchQuery += " OR " + concatString + "like '%" + words[i] + "%' ";
					}
				}
				query += " and ("+searchQuery+")";
			}
			
			query += " order by postingDate desc,hotness"; 
			
			System.out.println("Requirement : "+query);
			List<Requirement> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.setMaxResults(filter.getPageSize())
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Requirement> getAllRequirements(SearchFilter filter) {
		try {
			String query = "select *,addInfo_Todos alert," +
					"(select group_concat(concat(c.firstName,' ',c.lastName,'::',c.expectedRate,'::',c.jobHelp,'::',c.cityAndState,'::',c.source) SEPARATOR '####') " +
					"from recruit_sub_requirements s,recruit_candidate c where c.id=s.candidateId and s.requirementId=t.id and s.submissionStatus='Shortlisted' and s.deleted=false  ) shortlistedCandidates," +
					"(select group_concat(candidateId) from recruit_sub_requirements where requirementId=t.id " +
					"and submissionStatus!='Shortlisted' and deleted=false ) candidateId," +
					"(select group_concat(concat(candidateId,'-',date_format(submittedDate,'%m/%d/%Y %H:%i:%s'))) from recruit_sub_requirements " +
					"where requirementId=t.id and submissionStatus!='Shortlisted' and deleted=false ) submittedDate," +
					"(select group_concat(vendorId) from recruit_sub_vendors where requirementId=t.id ) vendorId," +
					"(select group_concat(concat(status,' - ',task) SEPARATOR '####') from recruit_researchtasks where requirementId=t.id ) pendingTasks," +
					"(select group_concat(skillSetId) from recruit_sub_skillset where requirementId=t.id ) skillSetIds" +
					" from recruit_requirements t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("hotness") != null) {
				query += " and t.hotness in (" + map.get("hotness").getStrValue()+ ")";
			}
			if (map.get("module") != null) {
				query += " and t.module like '%" + map.get("module").getStrValue()+ "%'";
			}
			if (map.get("jobOpeningStatus") != null) {
				query += " and t.jobOpeningStatus in (" + map.get("jobOpeningStatus").getStrValue()+ ")";
			}
			if (map.get("postingDateFrom") != null) {
				query += " and date(t.postingDate) >= date('"+map.get("postingDateFrom").getStrValue()+"')";
			}
			if (map.get("postingDateTo") != null) {
				query += " and date(t.postingDate) <= date('"+map.get("postingDateTo").getStrValue()+"')";
			}
			
			query += " order by postingDate desc,hotness"; 
			
			System.out.println("Requirement : "+query);
			List<Requirement> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.setResultTransformer(Transformers.aliasToBean(Requirement.class))
					.setMaxResults(filter.getPageSize())
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Requirement> getPendingJobs() {

		//myConf.addSqlFunction("group_concat", new StandardSQLFunction("group_concat", new StringType()));		
		try {
			String query = " from Requirement t where 1=1 and t.id in(select distinct s.requirement.id from Sub_Requirement s " +
					"where (s.submittedDate is null or s.submittedDate ='') and date(s.lastUpdated)=curdate() )  " ;
			
			System.out.println("Requirement : "+query);
			List<Requirement> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public boolean hasInterviews(Integer requirementId) {
		try {
			String query = "from Interview t where 1=1 and t.requirement.id like '" + requirementId+ "'";
			List list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			if(list.size()>0)
				return true;		
			return false;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public List getSuggestedCriteria(int id) {
		try {
			String query = "select 'location' type,CAST(count(*) AS CHAR) value,group_concat(distinct id) candidates " +
					" from recruit_candidate where marketingStatus in('1. Active','2. Yet to Start')" +
					" and location !='' and location = (select location from recruit_requirements where id =" +id+") " +
					//location
					" union " +
					//Client and module (All marketingStatus)
					"select 'clientAll',count(*) client,group_concat(distinct id) candidates from recruit_candidate where id in " +
					"(select candidateId from recruit_sub_requirements where submissionStatus != 'Shortlisted' and deleted = false and requirementId in " +
					"(select id from recruit_requirements where clientId =" +
					"(select clientId from recruit_requirements t,recruit_clients c where c.id=t.clientId and c.name != 'n/a' and t.id ="+id+")" +
					"and matchingCount(concat(module,',',secondaryModule), (select concat(module,',',secondaryModule) from recruit_requirements where id ="+id+")) != 0 and id != "+id+"))"+
					" union " +
					
					//Client and module (1. Active,2. Yet to Start)
					"select 'client',count(*) client,group_concat(distinct id) candidates from recruit_candidate " +
					"where marketingStatus in('1. Active','2. Yet to Start') and " +
					"id in (select candidateId from recruit_sub_requirements where submissionStatus != 'Shortlisted' and deleted = false " +
					"and requirementId in (select id from recruit_requirements where clientId =" +
					"(select clientId from recruit_requirements t,recruit_clients c where c.id=t.clientId and c.name != 'n/a' and t.id ="+id+")" +
					"and matchingCount(concat(module,',',secondaryModule), (select concat(module,',',secondaryModule) from recruit_requirements where id ="+id+")) != 0 and id != "+id+")) "+
					" union " +

					//certifications
					"select 'certification',count(*) certifications,group_concat(distinct id) candidates from recruit_candidate where id in (select " +
					"candidateId from recruit_certification where matchingCount(name,(select certifications from recruit_requirements where id ="+id+")) != 0 ) "+
					" union " +
					
					//Same Vendor and module
					"select 'vendor',count(distinct c.id),group_concat(distinct c.id) candidates from recruit_candidate c, recruit_sub_requirements s," +
					"recruit_requirements r where s.submissionStatus != 'Shortlisted' and s.deleted = false and c.id=s.candidateId and r.id=s.requirementId " +
					"and s.vendorId in (select vendorId from recruit_sub_requirements where deleted = false and requirementId="+id+")" +
					"and matchingCount(concat(r.module,',',r.secondaryModule),(select concat(module,',',secondaryModule) from recruit_requirements where id ="+id+")) != 0 " +
					"and r.id != "+id+
					" union " +
					
					//Same client and module in Projects
					"select 'project',count(*) client,group_concat(distinct id) candidates from recruit_candidate where id in (select e.candidateId " +
					"from project p,billingrate b,employeebillingrate eb,employee e where p.id=b.projectId and b.id=eb.billingRateId and " +
					"e.id=eb.employeeId and e.candidateId is not null and p.clientId=(select clientId from recruit_requirements where id ="+id+") " +
					"and matchingCount(p.module,(select concat(module,',',secondaryModule) from recruit_requirements where id ="+id+")) != 0 )";
					
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public List getPotentialCriteria(SearchFilter filter) {
		try {
			Map<String, Param> map = filter.getParamMap();
			String requirementId = map.get("requirementId").getStrValue();
			String candidateIds = map.get("candidateIds").getStrValue();
			//same client and diffrent module
			String query = "select 'client',group_concat(concat(candidateId,' -- ',if(r.module='','',concat('P-',r.module)),if(r.secondaryModule='','',concat(', O-',r.secondaryModule))) SEPARATOR ' :: ')," +
					" count(candidateId),r.clientId,r.module " +
					" from recruit_sub_requirements s,recruit_requirements r where s.submissionStatus != 'Shortlisted' and s.deleted = false" +
					" and s.requirementId=r.id and candidateId in ("+candidateIds+")  and r.id in (select id from recruit_requirements where clientId = " +
					"(select clientId from recruit_requirements t,recruit_clients c where c.id=t.clientId and c.name != 'n/a' and t.id ="+requirementId+") " +
					"and matchingCount((select concat(module,',',secondaryModule) from recruit_requirements where id ="+requirementId+"),concat(module,',',secondaryModule)) = 0 " +
					") and concat(module,secondaryModule) != '' " +
					" union " +
					//same vendor and diffrent module
					"select 'vendor',group_concat(distinct concat(c.id,' -- ',s.vendorId,' -- ',if(module='','',concat('P-',module)),if(secondaryModule='','',concat(' O-',secondaryModule))) SEPARATOR ' :: ')," +
					"count(distinct c.id),s.vendorId,r.module from recruit_candidate c,recruit_sub_requirements s,recruit_requirements r " +
					" where s.submissionStatus != 'Shortlisted' and s.deleted = false and c.id=s.candidateId and r.id=s.requirementId " +
					" and s.vendorId in (select vendorId from recruit_sub_vendors where requirementId="+requirementId+") and concat(module,secondaryModule) != ''" +
					" and matchingCount((select concat(module,',',secondaryModule) from recruit_requirements where id ="+requirementId+"),concat(r.module,',',r.secondaryModule)) = 0 " +
					" and r.id != "+requirementId+" and c.id in ("+candidateIds+") " +
					" union " +
					//same vendor and same client
					"select 'Client_vendor',group_concat(distinct concat(c.id,' -- ',s.vendorId,' -- ',r.clientId) SEPARATOR ' :: '),count(distinct c.id),s.vendorId,r.module" +
					" from recruit_candidate c,recruit_sub_requirements s,recruit_requirements r where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
					" and c.id=s.candidateId and r.id=s.requirementId and r.clientId=" +
					" (select clientId from recruit_requirements t,recruit_clients c where c.id=t.clientId and c.name != 'n/a' and t.id ="+requirementId+") " +
					" and vendorId in (select vendorId from recruit_sub_vendors where requirementId="+requirementId+") " +
					" and c.id in ("+candidateIds+") and r.id != "+requirementId+			
					" union " +
					//same module, same vendor, same client
					"select 'MVC',group_concat(distinct c.id SEPARATOR ' :: '),count(distinct c.id),s.vendorId,concat(if(r.module='','',concat('P-',r.module)),if(r.secondaryModule='','',concat(' O-',r.secondaryModule)))" +
					" from recruit_candidate c,recruit_sub_requirements s,recruit_requirements r where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
					" and c.id=s.candidateId and r.id=s.requirementId and r.clientId=" +
					" (select clientId from recruit_requirements t,recruit_clients c where c.id=t.clientId and c.name != 'n/a' and t.id ="+requirementId+") " +
					" and vendorId in (select vendorId from recruit_sub_requirements where requirementId="+requirementId+") " +
					" and matchingCount((select concat(module,',',secondaryModule) from recruit_requirements where id ="+requirementId+"),concat(r.module,',',r.secondaryModule)) > 0 " +
					" and c.id in ("+candidateIds+") and r.id != "+requirementId + " and concat(module,secondaryModule) != '' "+ 
					" union " +
					//same vendor and same module
					"select 'MV',group_concat(distinct c.id SEPARATOR ' :: '),count(distinct c.id),s.vendorId,concat(if(r.module='','',concat('P-',r.module)),if(r.secondaryModule='','',concat(' O-',r.secondaryModule)))" +
					" from recruit_candidate c,recruit_sub_requirements s,recruit_requirements r where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
					" and c.id=s.candidateId and r.id=s.requirementId " +
					" and vendorId in (select vendorId from recruit_sub_requirements where requirementId="+requirementId+") " +
					" and matchingCount((select concat(module,',',secondaryModule) from recruit_requirements where id ="+requirementId+"),concat(r.module,',',r.secondaryModule)) > 0 " +
					" and c.id in ("+candidateIds+") and r.id != "+requirementId +" and concat(module,secondaryModule) != '' " +
					" union " +
					//Vendor Withdrawn candidates
					"select 'VendorWithdrawn',group_concat(distinct concat(s.candidateId,' -- ',s.vendorId)),count(candidateId),s.vendorId,s.submissionStatus" +
					" from recruit_requirements r,recruit_sub_requirements s where r.id=s.requirementId and s.submissionStatus='I-Withdrawn' and " +
					" s.vendorId in (select vendorId from recruit_sub_vendors where requirementId="+requirementId+") and s.candidateId in ("+candidateIds+")" +
					" union " +
					//Client Withdrawn candidates
					"select 'CLientWithdrawn',group_concat(distinct s.candidateId),count(candidateId),r.clientId,s.submissionStatus from " +
					"recruit_requirements r,recruit_sub_requirements s where r.id=s.requirementId and s.submissionStatus in ('I-Withdrawn','Withdrawn') " +
					"and r.clientId in (select clientId from recruit_requirements where id="+requirementId+") and s.candidateId in ("+candidateIds+")" +
					" union " +
					//Same module and diffrent vendor
					"select 'SameModuleDifferentVendor',group_concat(distinct c.id),count(distinct c.id),r.clientId,group_concat(distinct concat(c.id,'-',s.vendorId))" +
					" from recruit_candidate c,recruit_sub_requirements s,recruit_requirements r where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
					" and c.id=s.candidateId and r.id=s.requirementId " +
					" and vendorId not in (select vendorId from recruit_sub_requirements where requirementId="+requirementId+") " +
					" and matchingCount((select concat(module,',',secondaryModule) from recruit_requirements where id ="+requirementId+"),concat(r.module,',',r.secondaryModule)) > 0 " +
					" and c.id in ("+candidateIds+") and r.id != "+requirementId +" and concat(module,secondaryModule) != '' " ;
			
			
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	
}