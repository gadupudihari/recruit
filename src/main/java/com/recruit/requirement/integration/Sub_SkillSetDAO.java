package com.recruit.requirement.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.requirement.service.Sub_SkillSet;


/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class Sub_SkillSetDAO {

	private static final Log log = LogFactory.getLog(Sub_SkillSetDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Sub_SkillSet  transientInstance) {
		log.debug("persisting Sub_SkillSet instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Sub_SkillSet instance) {
		log.debug("attaching dirty Sub_SkillSet instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Sub_SkillSet persistentInstance) {
		log.debug("deleting Sub_SkillSet instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Sub_SkillSet merge(Sub_SkillSet detachedInstance) {
		log.debug("merging Sub_SkillSet instance");
		try {
			Sub_SkillSet result = (Sub_SkillSet) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Sub_SkillSet findById(java.lang.Integer id) {
		log.debug("getting Sub_SkillSet instance with id: " + id);
		try {
			Sub_SkillSet instance = (Sub_SkillSet) sessionFactory.getCurrentSession()
					.get("com.recruit.requirement.service.Sub_SkillSet", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Sub_SkillSet> findByExample(Sub_SkillSet instance) {
		log.debug("finding Requirement instance by example");
		try {
			List<Sub_SkillSet> results = (List<Sub_SkillSet>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.requirement.service.Sub_SkillSet").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findByCriteria(SearchFilter filter) {
		try {
			String query = "select distinct s.* from recruit_sub_skillset s where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("requirementId") != null) {
				query += " and s.requirementId like '"+map.get("requirementId").getStrValue()+"'";
			}

			System.out.println("Research : "+query);
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	public void deleteByRequirement(String reqId) {
		try {
			String query = "delete from recruit_sub_skillset where requirementId = "+reqId;
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
		} catch (RuntimeException re) {
			log.error("find by employee Id failed", re);
			throw re;
		}
	}

}