package com.recruit.requirement.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.requirement.service.Sub_Vendor;


/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class Sub_VendorDAO {

	private static final Log log = LogFactory.getLog(Sub_VendorDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Sub_Vendor  transientInstance) {
		log.debug("persisting Sub_Vendor instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Sub_Vendor instance) {
		log.debug("attaching dirty Sub_Vendor instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Sub_Vendor persistentInstance) {
		log.debug("deleting Sub_Vendor instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Sub_Vendor merge(Sub_Vendor detachedInstance) {
		log.debug("merging Sub_Vendor instance");
		try {
			Sub_Vendor result = (Sub_Vendor) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Sub_Vendor findById(java.lang.Integer id) {
		log.debug("getting Sub_Vendor instance with id: " + id);
		try {
			Sub_Vendor instance = (Sub_Vendor) sessionFactory.getCurrentSession()
					.get("com.recruit.requirement.service.Sub_Vendor", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Sub_Vendor> findByExample(Sub_Vendor instance) {
		log.debug("finding Requirement instance by example");
		try {
			List<Sub_Vendor> results = (List<Sub_Vendor>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.requirement.service.Sub_Vendor").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findByCriteria(SearchFilter filter) {
		try {
			String query = "select distinct s.*, " +
					"(select email from recruit_contacts where id=s.contactId) email,(select cellPhone from recruit_contacts where id=s.contactId) cellPhone" +
					" from recruit_sub_vendors s where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("requirementId") != null) {
				query += " and s.requirementId like '"+map.get("requirementId").getStrValue()+"'";
			}

			System.out.println("Research : "+query);
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	public void deleteByVendor(String reqId, String vendorId) {
		try {
			String query = "delete from recruit_sub_vendors where requirementId = "+reqId+" and vendorId = "+vendorId;
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
		} catch (RuntimeException re) {
			log.error("find by employee Id failed", re);
			throw re;
		}
	}

}