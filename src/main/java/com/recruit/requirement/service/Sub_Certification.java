package com.recruit.requirement.service;

public class Sub_Certification implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer settingsId;

	private Requirement requirement;

	public Sub_Certification() {}
	
	public Sub_Certification(Integer id, Integer settingsId , Requirement requirement) {
		this.id = id;
		this.requirement = requirement;
		this.settingsId = settingsId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSettingsId() {
		return settingsId;
	}

	public void setSettingsId(Integer settingsId) {
		this.settingsId = settingsId;
	}

	public Requirement getRequirement() {
		return requirement;
	}

	public void setRequirement(Requirement requirement) {
		this.requirement = requirement;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id  + ", settingsId=" + settingsId +"]";
	}


}