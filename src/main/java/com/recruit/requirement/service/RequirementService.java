package com.recruit.requirement.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.recruit.base.presentation.Response;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.integration.CandidateDAO;
import com.recruit.client.integration.ClientDAO;
import com.recruit.placementLeads.integration.PlacementLeadsDAO;
import com.recruit.requirement.integration.RequirementsDAO;
import com.recruit.requirement.integration.Sub_CertificationDAO;
import com.recruit.requirement.integration.Sub_RequirementDAO;
import com.recruit.requirement.integration.Sub_SkillSetDAO;
import com.recruit.requirement.integration.Sub_VendorDAO;
import com.recruit.requirement.integration.WorkListDAO;
import com.recruit.research.integration.ResearchDAO;
import com.recruit.research.service.Research;
import com.recruit.settings.integration.CounterDAO;
import com.recruit.settings.integration.SettingsDAO;
import com.recruit.settings.service.Counter;
import com.recruit.user.integration.UserDAO;
import com.recruit.util.UtilityHelper;

@Service
public class RequirementService {

	@Autowired
	CandidateDAO candidateDAO;
	
	@Autowired
	RequirementsDAO requirementsDAO;
	
	@Autowired
	PlacementLeadsDAO placementLeadsDAO;

	@Autowired
	Sub_RequirementDAO sub_RequirementDAO;
	
	@Autowired
	SettingsDAO settingsDAO;
	
	@Autowired
	ResearchDAO researchDAO;

	@Autowired
	Sub_VendorDAO sub_VendorDAO;
	
	@Autowired
	ClientDAO clientDAO;

	@Autowired
	WorkListDAO workListDAO;

	@Autowired
	UserDAO userDAO;

	@Autowired
	CounterDAO counterDAO;

	@Autowired
	Sub_CertificationDAO sub_CertificationDAO;
	
	@Autowired
	Sub_SkillSetDAO sub_SkillSetDAO;
	
	@Transactional
	public List<Requirement> getRequirements(SearchFilter filter) {
		return requirementsDAO.findByCriteria(filter);
	}

	@Transactional
	public List<Requirement> getAllRequirements(SearchFilter filter) {
		return requirementsDAO.getAllRequirements(filter);
	}

	@Transactional
	public void saveRequirements(List<Requirement> requirements,String loginId, Timestamp createdDate) {
		for (Requirement requirement : requirements) {
			requirement.setLastUpdated(createdDate);
			requirement.setLastUpdatedUser(loginId);
			if (requirement.getId() == null)
				requirement.setCreated(createdDate);
			if (requirement.getClient() != null) {
				requirement.setClient(clientDAO.findById(requirement.getClient().getId()));
			}
			requirementsDAO.merge(requirement);
			for (Sub_Vendor sub_Vendor : requirement.getSub_Vendors()) {
				sub_Vendor.setRequirement(requirement);
				sub_VendorDAO.persist(sub_Vendor);
			}
		}
	}

	@Transactional
	public void deleteRequirement(Response response, Integer id) {
		Requirement requirement = requirementsDAO.findById(id);
		if (requirement.getResearchs() != null && requirement.getResearchs().size() >0) {
			response.setSuccess(false);
			response.setErrorMessage("There are Research Tasks associated to this Job. Cannot delete.");
		}else if (requirement.getSub_Requirements() != null && requirement.getSub_Requirements().size() >0) {
			response.setSuccess(false);
			response.setErrorMessage("There are Candidates associated to this Job. Cannot delete.");
		}else if (requirement.getSub_Vendors() != null && requirement.getSub_Vendors().size() >0) {
			response.setSuccess(false);
			response.setErrorMessage("There are Vendors associated to this Job. Cannot delete.");
		}else if (requirementsDAO.hasInterviews(requirement.getId())) {
			response.setSuccess(false);
			response.setErrorMessage("There are Interviews associated to this Job. Cannot delete.");
		}else{
			//delete certifications before delete job opeing
			sub_CertificationDAO.deleteByRequirement(requirement.getId().toString());
			//delete Skillset before delete job opeing
			sub_SkillSetDAO.deleteByRequirement(requirement.getId().toString());
			requirementsDAO.delete(requirement);
			response.setSuccess(true);
		}
	}

	@Transactional
	public void saveRequirement(Requirement requirement, String loginId, Timestamp createdDate, Response response) {
		requirement.setLastUpdated(createdDate);
		requirement.setLastUpdatedUser(loginId);
		if (requirement.getId() == null){ 
			requirement.setCreated(createdDate);
			requirementsDAO.attachDirty(requirement);
			for (Research research : requirement.getResearchs()) {
				research.setLastUpdatedUser(loginId);
				research.setLastUpdated(createdDate);
				if (research.getId() != null)
					research.setCreated(createdDate);
				research.setRequirement(requirement);
				if(research.getId() == null)
					researchDAO.attachDirty(research);
				else
					researchDAO.merge(research);
			}
			for (Sub_Certification certification : requirement.getSub_Certifications()) {
				certification.setRequirement(requirement);
				sub_CertificationDAO.attachDirty(certification);
			}
			for (Sub_SkillSet skillSet : requirement.getSub_Skillsets()) {
				skillSet.setRequirement(requirement);
				sub_SkillSetDAO.attachDirty(skillSet);
			}
		}else{
			Requirement preRequirement = requirementsDAO.findById(requirement.getId());
			//check version of previous and present requirement
			if (requirement.getVersion() < preRequirement.getVersion()) {
				response.setSuccess(Boolean.FALSE);
				response.setErrorMessage("Version mismatch");
				return ;
			}else
				requirement.setVersion(preRequirement.getVersion()+1);
			
			requirement.setCreated(preRequirement.getCreated());
			for (Research research : requirement.getResearchs()) {
				research.setLastUpdatedUser(loginId);
				research.setLastUpdated(createdDate);
				if (research.getId() != null)
					research.setCreated(createdDate);
				research.setRequirement(requirement);
				if(research.getId() == null)
					researchDAO.attachDirty(research);
				else
					researchDAO.merge(research);
			}
			
			//delete and add certifications 
			sub_CertificationDAO.deleteByRequirement(requirement.getId().toString());
			for (Sub_Certification certification : requirement.getSub_Certifications()) {
				certification.setRequirement(requirement);
				sub_CertificationDAO.attachDirty(certification);
			}

			//delete and add skillset
			sub_SkillSetDAO.deleteByRequirement(requirement.getId().toString());
			for (Sub_SkillSet skillSet : requirement.getSub_Skillsets()) {
				skillSet.setRequirement(requirement);
				sub_SkillSetDAO.attachDirty(skillSet);
			}
			
			if (requirement.getRecruiterObj() != null && requirement.getRecruiterObj().getId() != null ){
				requirement.setRecruiterObj(userDAO.findById(requirement.getRecruiterObj().getId()));
			}
			requirementsDAO.merge(requirement);
		}
		response.setSuccess(true);
		response.setReturnVal(requirement);
	}

	@Transactional
	public void removeCandidate(int id) {
		Sub_Requirement candidate = sub_RequirementDAO.findById(id);
		candidate.setDeleted(true);
		sub_RequirementDAO.attachDirty(candidate);
	}

	@Transactional
	public List<Research> getTasks(SearchFilter filter) {
		return researchDAO.findByCriteria(filter);
	}

	@Transactional
	public void saveTask(List<Research> researchList, String loginId,Timestamp createdDate) {
		for (Research research : researchList) {
			research.setLastUpdatedUser(loginId);
			research.setLastUpdated(createdDate);
			if (research.getId() != null)
				research.setCreated(createdDate);
			researchDAO.attachDirty(research);
		}
	}

	@Transactional
	public void deleteTask(Integer id) {
		Research research = researchDAO.findById(id);
		researchDAO.delete(research);
	}

	@Transactional
	public List<Sub_Requirement> getSubRequirements(SearchFilter filter, String loginId) {
		return sub_RequirementDAO.findByCriteria(filter);
	}

	@Transactional
	public void saveSubRequirements(Response response, String loginId, Timestamp createdDate, List<Sub_Requirement> requirements) {
		for (Sub_Requirement sub_Requirement : requirements) {
			sub_RequirementDAO.removeDeletedCandidate(sub_Requirement);
			if(sub_RequirementDAO.checkExist(sub_Requirement)){
				response.setErrorMessage("Candidate already submitted.Please refresh.");
				response.setSuccess(false);
				return ;
			}
		}
		for (Sub_Requirement sub_Requirement : requirements) {
			sub_Requirement.setLastUpdatedUser(loginId);
			sub_Requirement.setLastUpdated(createdDate);
			if (sub_Requirement.getId() == null) 
				sub_Requirement.setCreated(createdDate);
			Sub_Requirement shortlistedCandidate = sub_RequirementDAO.findShortlisted(sub_Requirement);
			if (shortlistedCandidate != null) {
				sub_Requirement.setId(shortlistedCandidate.getId());
				sub_Requirement.setComments(shortlistedCandidate.getComments());
				sub_Requirement.setGuide(shortlistedCandidate.getGuide());
			}
			sub_RequirementDAO.merge(sub_Requirement);
		}
		response.setSuccess(true);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getSubVendors(SearchFilter filter) {
		return sub_VendorDAO.findByCriteria(filter);
	}

	@Transactional
	public void removeSubVendor(Response response, int vendorId) {
		Sub_Vendor sub_Vendor = sub_VendorDAO.findById(vendorId);
		List<Sub_Requirement> associatedCandidates = sub_RequirementDAO.findByVendor(sub_Vendor.getVendorId(),sub_Vendor.getRequirement().getId());
		if (associatedCandidates.size() > 0 ) {
			response.setErrorMessage("Candidates Associated to this vendor");
			response.setSuccess(false);
		}else{
			sub_VendorDAO.delete(sub_Vendor);
			response.setSuccess(true);
		}
	}

	@Transactional
	public void saveSubVendors(List<Sub_Vendor> requirements) {
		for (Sub_Vendor sub_Vendor : requirements) {
			sub_VendorDAO.attachDirty(sub_Vendor);
		}
	}

	@Transactional
	public List<Requirement> getPendingJobs() {
		return requirementsDAO.getPendingJobs();
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getShortlisted(SearchFilter filter) {
		return sub_RequirementDAO.findByCriteria(filter);
	}

	@Transactional
	public void saveShortlisted(Response response, String loginId, Timestamp createdDate,List<Sub_Requirement> candidates) {
		for (Sub_Requirement candidate : candidates) {
			sub_RequirementDAO.removeDeletedShortList(candidate);
			Sub_Requirement preCandidate = sub_RequirementDAO.findExisting(candidate);
			if(preCandidate != null ){
				if (preCandidate.getSubmissionStatus() != null && preCandidate.getSubmissionStatus().equalsIgnoreCase("Shortlisted"))
					response.setErrorMessage("Candidate already shortlisted.Please refresh.");
				else
					response.setErrorMessage("Candidate already submitted.Please refresh.");
				response.setSuccess(false);
				return ;
			}
			candidate.setLastUpdatedUser(loginId);
			candidate.setLastUpdated(createdDate);
			if (candidate.getId() == null) 
				candidate.setCreated(createdDate);
		}
		for (Sub_Requirement candidate : candidates) {
			sub_RequirementDAO.attachDirty(candidate);
		}
		response.setSuccess(true);
	}

	@Transactional
	public void removeShortlist(int id) {
		Sub_Requirement candidate = sub_RequirementDAO.findById(id);
		candidate.setDeleted(true);
		sub_RequirementDAO.attachDirty(candidate);
	}

	@Transactional
	public void removeShortlistedJob(SearchFilter filter) {
		List<Sub_Requirement> list = sub_RequirementDAO.findByCriteria(filter);
		for (Sub_Requirement sub_Requirement : list) {
			Sub_Requirement candidate = sub_RequirementDAO.findById(sub_Requirement.getId());
			candidate.setDeleted(true);
			sub_RequirementDAO.attachDirty(candidate);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Transactional
	public List getSuggestedCriteria(int id) {
		return requirementsDAO.getSuggestedCriteria(id);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getPotentialCriteria(SearchFilter filter) {
		return requirementsDAO.getPotentialCriteria(filter);
	}

	@Transactional
	public void saveWorkList(Response response, List<WorkList> list, String loginId, Timestamp createdDate) {
		for (WorkList workList : list) {
			workList.setLastUpdated(createdDate);
			workList.setLastUpdatedUser(loginId);
			if (workList.getId() == null || workList.getId() == 0) {
				workList.setCreated(createdDate);
				workList.setCreatedUser(loginId);
			}
			workListDAO.attachDirty(workList);
		}
	}

	@Transactional
	public List<WorkList> getWorkList(SearchFilter filter) {
		return workListDAO.findByCriteria(filter);
	}

	@Transactional
	public void getRequirementNo(Response response) {
		Counter counter = counterDAO.findByTableName("recruit_requirements");
		java.sql.Timestamp currentDate = UtilityHelper.getDate();
		if (counter == null) {
			counter = new Counter();
			counter.setTableName("recruit_requirements");
			counter.setDate(currentDate);
			counter.setCounter(1);
		}else if (new SimpleDateFormat("MM/dd/yyyy").format(counter.getDate()).equals(new SimpleDateFormat("MM/dd/yyyy").format(currentDate))) {
			counter.setCounter(counter.getCounter()+1);
		}else{
			counter.setDate(currentDate);
			counter.setCounter(1);
		}
		counterDAO.attachDirty(counter);
		response.setSuccess(true);
		response.setSuccessMessage(counter.getCounter().toString());
	}

	@Transactional
	public List<Sub_Requirement> getHighContacts(SearchFilter filter) {
		return sub_RequirementDAO.getHighContacts(filter);
	}
	
}
