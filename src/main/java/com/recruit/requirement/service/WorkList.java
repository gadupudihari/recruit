package com.recruit.requirement.service;

import java.sql.Timestamp;

public class WorkList implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer userId;
	private Integer assignedUserId;
	private String comments;
	private String status;
	private Timestamp created;
	private Timestamp lastUpdated;
	private Timestamp deleted;
	private String createdUser;
	private String lastUpdatedUser;
	private String deletedUser;

	private Requirement requirement;

	public WorkList() {}
	
	public WorkList(Integer id, Integer userId, Integer assignedUserId, String comments, 
			Timestamp created, Timestamp lastUpdated, Timestamp deleted, String createdUser, String lastUpdatedUser, String deletedUser, 
			Requirement requirement) {
		this.id = id;
		this.userId = userId;
		this.assignedUserId = assignedUserId;
		this.comments = comments;
		this.created = created;
		this.lastUpdated = lastUpdated;
		this.deleted = deleted;
		this.createdUser = createdUser;
		this.lastUpdatedUser = lastUpdatedUser;
		this.deletedUser = deletedUser;
		this.requirement = requirement;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getAssignedUserId() {
		return assignedUserId;
	}

	public void setAssignedUserId(Integer assignedUserId) {
		this.assignedUserId = assignedUserId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Timestamp getDeleted() {
		return deleted;
	}

	public void setDeleted(Timestamp deleted) {
		this.deleted = deleted;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getDeletedUser() {
		return deletedUser;
	}

	public void setDeletedUser(String deletedUser) {
		this.deletedUser = deletedUser;
	}

	public Requirement getRequirement() {
		return requirement;
	}

	public void setRequirement(Requirement requirement) {
		this.requirement = requirement;
	}

	@Override
	public String toString() {
		return "WorkList [id=" + id + ", userId=" + userId + ", assignedUserId=" + assignedUserId + ", comments=" + comments + 
				", created=" + created + ", lastUpdated=" + lastUpdated + ", deleted=" + deleted + ", createdUser=" + createdUser 
				+ ", lastUpdatedUser=" + lastUpdatedUser + ", deletedUser=" + deletedUser + ", requirement=" + requirement 
				+ "]";
	}


}