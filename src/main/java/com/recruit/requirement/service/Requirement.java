package com.recruit.requirement.service;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.recruit.client.service.Client;
import com.recruit.research.service.Research;
import com.recruit.user.service.User;

public class Requirement implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Timestamp postingDate;
	private String location;
	private String cityAndState;
	private String  requirement;
	private String hotness;
	private String module;
	private String secondaryModule;
	private String communication;
	private String researchComments;
	private String addInfo_Todos;
	private String source;
	private Timestamp created;
	private Timestamp lastUpdated;
	private String lastUpdatedUser;
	private String comments;
	private String rateSubmitted;
	private String postingTitle;
	private String jobOpeningStatus;
	private String employer;
	private String publicLink;
	private String skillSet;
	private Integer resume;
	private String experience;
	private String helpCandidate;
	private String helpCandidateComments;
	private String targetRoles;
	private String certifications;
	private Boolean remote;
	private Boolean relocate;
	private Boolean travel;
	private Boolean tAndEPaid;
	private Boolean tAndENotPaid;
	private Integer version;
	private Integer requirementNo;
	
	private User recruiterObj;
	private Client client;
	private Set<Research> researchs = new HashSet<Research>(0); 
	private Set<Sub_Requirement> sub_Requirements = new HashSet<Sub_Requirement>(0);
	private Set<Sub_Vendor> sub_Vendors = new HashSet<Sub_Vendor>(0);
	private Set<Sub_Certification> sub_Certifications = new HashSet<Sub_Certification>(0);
	private Set<Sub_SkillSet> sub_Skillsets = new HashSet<Sub_SkillSet>(0);
	
	private Integer clientId;
	private Integer recruiter;
	private String alert;
	private String shortlistedCandidates;
	private String candidateId;
	private String vendorId;
	private String pendingTasks;
	private String submittedDate;
	private String skillSetIds;
	
	public Requirement() {}
	
	public Requirement(Integer id, Timestamp postingDate, String location, String requirement, String hotness, String module,
			String communication, String researchComments, String addInfo_Todos, String source, Timestamp created,
			Timestamp lastUpdated, String lastUpdatedUser, String comments, String rateSubmitted, String postingTitle, String jobOpeningStatus,
			String employer, String publicLink,String skillSet,Integer resume,String experience,String helpCandidate,String helpCandidateComments,
			String targetRoles,String certifications,String cityAndState,String secondaryModule,Integer requirementNo,
			Boolean remote, Boolean relocate, Boolean travel, Boolean tAndEPaid, Boolean tAndENotPaid, Integer version, 
			User recruiterObj, Client client, 
			Set<Research> researchs, Set<Sub_Requirement> sub_Requirements, Set<Sub_Vendor> sub_Vendors,Set<Sub_Certification> sub_Certifications,
			Set<Sub_SkillSet> sub_Skillsets) {
		this.id = id;
		this.postingDate = postingDate;
		this.location = location;
		this.cityAndState = cityAndState;
		this.requirement = requirement;
		this.hotness = hotness;
		this.module = module;
		this.communication = communication;
		this.researchComments = researchComments;
		this.addInfo_Todos = addInfo_Todos;
		this.source = source;
		this.created = created;
		this.lastUpdated = lastUpdated;
		this.lastUpdatedUser = lastUpdatedUser;
		this.comments = comments;
		this.rateSubmitted = rateSubmitted;
		this.postingTitle = postingTitle;
		this.jobOpeningStatus = jobOpeningStatus;
		this.employer = employer;
		this.publicLink = publicLink;
		this.skillSet = skillSet;
		this.resume = resume;
		this.experience = experience;
		this.helpCandidate = helpCandidate;
		this.helpCandidateComments = helpCandidateComments;
		this.targetRoles = targetRoles;
		this.certifications = certifications;
		this.secondaryModule = secondaryModule;
		this.remote = remote;
		this.relocate = relocate;
		this.travel = travel;
		this.tAndEPaid = tAndEPaid;
		this.tAndENotPaid = tAndENotPaid;
		this.version = version;
		this.requirementNo = requirementNo;
		
		this.recruiterObj = recruiterObj;
		this.client = client;
		this.researchs = researchs;
		this.sub_Requirements = sub_Requirements;
		this.sub_Vendors = sub_Vendors;
		this.sub_Certifications = sub_Certifications;
		this.sub_Skillsets = sub_Skillsets;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Timestamp postingDate) {
		this.postingDate = postingDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public String getHotness() {
		return hotness;
	}

	public void setHotness(String hotness) {
		this.hotness = hotness;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getCommunication() {
		return communication;
	}

	public void setCommunication(String communication) {
		this.communication = communication;
	}

	public String getResearchComments() {
		return researchComments;
	}

	public void setResearchComments(String researchComments) {
		this.researchComments = researchComments;
	}

	public String getAddInfo_Todos() {
		return addInfo_Todos;
	}

	public void setAddInfo_Todos(String addInfo_Todos) {
		this.addInfo_Todos = addInfo_Todos;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getRateSubmitted() {
		return rateSubmitted;
	}

	public void setRateSubmitted(String rateSubmitted) {
		this.rateSubmitted = rateSubmitted;
	}

	public String getPostingTitle() {
		return postingTitle;
	}

	public void setPostingTitle(String postingTitle) {
		this.postingTitle = postingTitle;
	}

	public String getJobOpeningStatus() {
		return jobOpeningStatus;
	}

	public void setJobOpeningStatus(String jobOpeningStatus) {
		this.jobOpeningStatus = jobOpeningStatus;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getPublicLink() {
		return publicLink;
	}

	public void setPublicLink(String publicLink) {
		this.publicLink = publicLink;
	}

	public String getSkillSet() {
		return skillSet;
	}

	public void setSkillSet(String skillSet) {
		this.skillSet = skillSet;
	}

	public Integer getResume() {
		return resume;
	}

	public void setResume(Integer resume) {
		this.resume = resume;
	}

	public Set<Research> getResearchs() {
		return researchs;
	}

	public void setResearchs(Set<Research> researchs) {
		this.researchs = researchs;
	}

	public Set<Sub_Requirement> getSub_Requirements() {
		return sub_Requirements;
	}

	public void setSub_Requirements(Set<Sub_Requirement> sub_Requirements) {
		this.sub_Requirements = sub_Requirements;
	}

	public Set<Sub_Vendor> getSub_Vendors() {
		return sub_Vendors;
	}

	public void setSub_Vendors(Set<Sub_Vendor> sub_Vendors) {
		this.sub_Vendors = sub_Vendors;
	}

	public Set<Sub_Certification> getSub_Certifications() {
		return sub_Certifications;
	}

	public void setSub_Certifications(Set<Sub_Certification> sub_Certifications) {
		this.sub_Certifications = sub_Certifications;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getHelpCandidate() {
		return helpCandidate;
	}

	public void setHelpCandidate(String helpCandidate) {
		this.helpCandidate = helpCandidate;
	}

	public String getHelpCandidateComments() {
		return helpCandidateComments;
	}

	public void setHelpCandidateComments(String helpCandidateComments) {
		this.helpCandidateComments = helpCandidateComments;
	}
	
	public String getTargetRoles() {
		return targetRoles;
	}

	public void setTargetRoles(String targetRoles) {
		this.targetRoles = targetRoles;
	}

	public String getCertifications() {
		return certifications;
	}

	public void setCertifications(String certifications) {
		this.certifications = certifications;
	}

	public String getCityAndState() {
		return cityAndState;
	}

	public void setCityAndState(String cityAndState) {
		this.cityAndState = cityAndState;
	}

	public String getSecondaryModule() {
		return secondaryModule;
	}

	public void setSecondaryModule(String secondaryModule) {
		this.secondaryModule = secondaryModule;
	}

	public Boolean getRemote() {
		return remote;
	}

	public void setRemote(Boolean remote) {
		this.remote = remote;
	}

	public Boolean getRelocate() {
		return relocate;
	}

	public void setRelocate(Boolean relocate) {
		this.relocate = relocate;
	}

	public Boolean getTravel() {
		return travel;
	}

	public void setTravel(Boolean travel) {
		this.travel = travel;
	}

	public Boolean gettAndEPaid() {
		return tAndEPaid;
	}

	public void settAndEPaid(Boolean tAndEPaid) {
		this.tAndEPaid = tAndEPaid;
	}

	public Boolean gettAndENotPaid() {
		return tAndENotPaid;
	}

	public void settAndENotPaid(Boolean tAndENotPaid) {
		this.tAndENotPaid = tAndENotPaid;
	}

	public User getRecruiterObj() {
		return recruiterObj;
	}

	public void setRecruiterObj(User recruiterObj) {
		this.recruiterObj = recruiterObj;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Integer getRequirementNo() {
		return requirementNo;
	}

	public void setRequirementNo(Integer requirementNo) {
		this.requirementNo = requirementNo;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public Integer getRecruiter() {
		return recruiter;
	}

	public void setRecruiter(Integer recruiter) {
		this.recruiter = recruiter;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public String getShortlistedCandidates() {
		return shortlistedCandidates;
	}

	public void setShortlistedCandidates(String shortlistedCandidates) {
		this.shortlistedCandidates = shortlistedCandidates;
	}

	public String getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(String candidateId) {
		this.candidateId = candidateId;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getPendingTasks() {
		return pendingTasks;
	}

	public void setPendingTasks(String pendingTasks) {
		this.pendingTasks = pendingTasks;
	}

	public String getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(String submittedDate) {
		this.submittedDate = submittedDate;
	}

	public Set<Sub_SkillSet> getSub_Skillsets() {
		return sub_Skillsets;
	}

	public void setSub_Skillsets(Set<Sub_SkillSet> sub_Skillsets) {
		this.sub_Skillsets = sub_Skillsets;
	}

	public String getSkillSetIds() {
		return skillSetIds;
	}

	public void setSkillSetIds(String skillSetIds) {
		this.skillSetIds = skillSetIds;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id + ", location=" + location + ", requirement=" + requirement + ", hotness=" + hotness + ", module=" + module + 
				", communication=" + communication + ", postingDate=" + postingDate + ", researchComments=" + researchComments +", addInfo_Todos=" + addInfo_Todos + 
				", source=" + source + ", created=" + created + ", lastUpdated=" + lastUpdated + ", lastUpdatedUser=" + lastUpdatedUser +
				", comments=" + comments + ", rateSubmitted=" + rateSubmitted + ", postingTitle=" + postingTitle +", jobOpeningStatus=" + jobOpeningStatus +
				", employer="+ employer + ", publicLink="+ publicLink +", skillSet="+skillSet +", resume="+ resume +", experience="+ experience +
				", helpCandidate="+ helpCandidate + ", helpCandidateComments="+ helpCandidateComments + ", secondaryModule="+ secondaryModule 
				+ ", targetRoles="+ targetRoles + ", certifications="+ certifications + ", cityAndState="+ cityAndState 
				+ ", remote="+ remote + ", relocate="+ relocate + ", travel="+ travel + ", tAndEPaid="+ tAndEPaid + ", tAndENotPaid="+ tAndENotPaid 
				+ ", version="+ version + ", requirementNo="+ requirementNo 
				+"]";
	}
}