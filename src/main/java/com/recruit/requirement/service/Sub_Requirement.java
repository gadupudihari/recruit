 package com.recruit.requirement.service;

import java.sql.Timestamp;

public class Sub_Requirement implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer candidateId;
	private Integer vendorId;
	private Timestamp submittedDate;
	private Timestamp created;
	private Timestamp lastUpdated;
	private Timestamp statusUpdated;
	private String lastUpdatedUser;
	private String submittedRate1;
	private String submittedRate2;
	private String submissionStatus;
	private Boolean projectInserted;
	private String comments;
	private String resumeLink;
	private String guide;
	private String reference;
	private Boolean deleted; 
	private String alert;
	
	//Fileds not there in table
	private Integer clientId;
	private Integer requirementId;
	private String contactId;

	private String emailId;
	private String reqCityAndState;
	private String location;
	private String postingTitle;
	private String candidate;
	private String contact;
	private String candidateExpectedRate;
	private String cityAndState;
	private Boolean relocate;
	private Boolean travel;
	private String immigrationVerified;
	private String vendor;
	private String client;
	private String addInfoTodos;
	private String module;
	private String hotness;
	private String resumeHelp;
	private String interviewHelp;
	private String jobHelp;
	private Timestamp availability;
	private Timestamp postingDate;
	private String interviewDate;
	private Timestamp projectStartDate;
	private Integer score;
	
	private Requirement requirement;

	public Sub_Requirement() {}
	
	public Sub_Requirement(Integer id, Integer candidateId, Integer vendorId, Timestamp submittedDate,String submittedRate1,String submittedRate2, 
			String submissionStatus,Boolean projectInserted, Timestamp created,Timestamp lastUpdated, String lastUpdatedUser, String comments,
			Timestamp statusUpdated, String resumeLink , String guide, String reference, Boolean deleted, Requirement requirement) {
		this.id = id;
		this.requirement = requirement;
		this.candidateId = candidateId;
		this.vendorId = vendorId;
		this.submittedDate = submittedDate;
		this.created = created;
		this.lastUpdated = lastUpdated;
		this.lastUpdatedUser = lastUpdatedUser;
		this.submittedRate1 = submittedRate1;
		this.submittedRate2 = submittedRate2;
		this.submissionStatus = submissionStatus;
		this.projectInserted = projectInserted;
		this.comments = comments;
		this.statusUpdated = statusUpdated;
		this.resumeLink = resumeLink;
		this.guide = guide;
		this.reference = reference;
		this.deleted = deleted;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(Integer candidateId) {
		this.candidateId = candidateId;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public Timestamp getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(Timestamp submittedDate) {
		this.submittedDate = submittedDate;
	}

	public Requirement getRequirement() {
		return requirement;
	}

	public void setRequirement(Requirement requirement) {
		this.requirement = requirement;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getSubmittedRate1() {
		return submittedRate1;
	}

	public void setSubmittedRate1(String submittedRate1) {
		this.submittedRate1 = submittedRate1;
	}

	public String getSubmittedRate2() {
		return submittedRate2;
	}

	public void setSubmittedRate2(String submittedRate2) {
		this.submittedRate2 = submittedRate2;
	}

	public String getSubmissionStatus() {
		return submissionStatus;
	}

	public void setSubmissionStatus(String submissionStatus) {
		this.submissionStatus = submissionStatus;
	}

	public Boolean getProjectInserted() {
		return projectInserted;
	}

	public void setProjectInserted(Boolean projectInserted) {
		this.projectInserted = projectInserted;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Timestamp getStatusUpdated() {
		return statusUpdated;
	}

	public void setStatusUpdated(Timestamp statusUpdated) {
		this.statusUpdated = statusUpdated;
	}

	public String getReqCityAndState() {
		return reqCityAndState;
	}

	public void setReqCityAndState(String reqCityAndState) {
		this.reqCityAndState = reqCityAndState;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPostingTitle() {
		return postingTitle;
	}

	public void setPostingTitle(String postingTitle) {
		this.postingTitle = postingTitle;
	}

	public String getCandidate() {
		return candidate;
	}

	public void setCandidate(String candidate) {
		this.candidate = candidate;
	}

	public String getCandidateExpectedRate() {
		return candidateExpectedRate;
	}

	public void setCandidateExpectedRate(String candidateExpectedRate) {
		this.candidateExpectedRate = candidateExpectedRate;
	}

	public String getCityAndState() {
		return cityAndState;
	}

	public void setCityAndState(String cityAndState) {
		this.cityAndState = cityAndState;
	}

	public Boolean getRelocate() {
		return relocate;
	}

	public void setRelocate(Boolean relocate) {
		this.relocate = relocate;
	}

	public Boolean getTravel() {
		return travel;
	}

	public void setTravel(Boolean travel) {
		this.travel = travel;
	}

	public String getImmigrationVerified() {
		return immigrationVerified;
	}

	public void setImmigrationVerified(String immigrationVerified) {
		this.immigrationVerified = immigrationVerified;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getAddInfoTodos() {
		return addInfoTodos;
	}

	public void setAddInfoTodos(String addInfoTodos) {
		this.addInfoTodos = addInfoTodos;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getHotness() {
		return hotness;
	}

	public void setHotness(String hotness) {
		this.hotness = hotness;
	}

	public String getResumeHelp() {
		return resumeHelp;
	}

	public void setResumeHelp(String resumeHelp) {
		this.resumeHelp = resumeHelp;
	}

	public String getInterviewHelp() {
		return interviewHelp;
	}

	public void setInterviewHelp(String interviewHelp) {
		this.interviewHelp = interviewHelp;
	}

	public String getJobHelp() {
		return jobHelp;
	}

	public void setJobHelp(String jobHelp) {
		this.jobHelp = jobHelp;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public Timestamp getAvailability() {
		return availability;
	}

	public void setAvailability(Timestamp availability) {
		this.availability = availability;
	}

	public Timestamp getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Timestamp postingDate) {
		this.postingDate = postingDate;
	}

	public Integer getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(Integer requirementId) {
		this.requirementId = requirementId;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getResumeLink() {
		return resumeLink;
	}

	public void setResumeLink(String resumeLink) {
		this.resumeLink = resumeLink;
	}

	public String getGuide() {
		return guide;
	}

	public void setGuide(String guide) {
		this.guide = guide;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getInterviewDate() {
		return interviewDate;
	}

	public void setInterviewDate(String interviewDate) {
		this.interviewDate = interviewDate;
	}

	public Timestamp getProjectStartDate() {
		return projectStartDate;
	}

	public void setProjectStartDate(Timestamp projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id + ", candidateId=" + candidateId + ", vendorId=" + vendorId + ", submittedDate=" + submittedDate 
				+ ", submittedRate1=" + submittedRate1+ ", submittedRate2=" + submittedRate2 + ", submissionStatus=" + submissionStatus
				+ ", projectInserted=" + projectInserted + ", created=" + created + ", lastUpdated=" + lastUpdated + ", lastUpdatedUser=" + lastUpdatedUser 
				+ ", comments=" + comments + ", statusUpdated=" + statusUpdated + ", resumeLink=" + resumeLink + ", guide=" + guide
				+ ", reference=" + reference + ", deleted=" + deleted
				+"]";
	}


}