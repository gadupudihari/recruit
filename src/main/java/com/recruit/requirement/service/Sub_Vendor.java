package com.recruit.requirement.service;

public class Sub_Vendor implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer vendorId;
	private Integer contactId;

	private Requirement requirement;

	public Sub_Vendor() {}
	
	public Sub_Vendor(Integer id, Integer vendorId, Integer contactId , Requirement requirement) {
		this.id = id;
		this.requirement = requirement;
		this.vendorId = vendorId;
		this.contactId = contactId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public Integer getContactId() {
		return contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public Requirement getRequirement() {
		return requirement;
	}

	public void setRequirement(Requirement requirement) {
		this.requirement = requirement;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id  + ", vendorId=" + vendorId + ", contactId=" + contactId  
				+"]";
	}


}