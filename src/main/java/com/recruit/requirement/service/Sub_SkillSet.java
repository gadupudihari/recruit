package com.recruit.requirement.service;

public class Sub_SkillSet implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer skillSetId;

	private Requirement requirement;

	public Sub_SkillSet() {}
	
	public Sub_SkillSet(Integer id, Integer skillSetId , Requirement requirement) {
		this.id = id;
		this.requirement = requirement;
		this.skillSetId = skillSetId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSkillSetId() {
		return skillSetId;
	}

	public void setSkillSetId(Integer skillSetId) {
		this.skillSetId = skillSetId;
	}

	public Requirement getRequirement() {
		return requirement;
	}

	public void setRequirement(Requirement requirement) {
		this.requirement = requirement;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id  + ", skillSetId=" + skillSetId +"]";
	}


}