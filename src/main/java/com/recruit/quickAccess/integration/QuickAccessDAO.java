package com.recruit.quickAccess.integration;

// Generated Sep 11, 2011 8:31:09 AM by Hibernate Tools 3.3.0.GA

import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.quickAccess.service.QuickAccess;


/**
 * Home object for domain model class BillingRate.
 * @see com.timesheetz.project.service.BillingRate
 * @author Hibernate Tools
 */
@Repository
public class QuickAccessDAO {

	private static final Log log = LogFactory.getLog(QuickAccessDAO.class);


	@Autowired
	private  SessionFactory sessionFactory;

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	@SuppressWarnings("unchecked")
	public List<QuickAccess> findByCriteria(SearchFilter filter) {
		try {
			String query = "from QuickAccess t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();
			
			if (map.get("search") != null) {
				query += " and t.name like '%" + map.get("search").getStrValue()+ "%'";
			}

			List<QuickAccess> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<QuickAccess> getAllEmployees() {
		try {
			String query = "from QuickAccess t where 1=1 order by type,name";
			
			List<QuickAccess> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public List getStatistics(SearchFilter filter) {
		try {
			Map<String, Param> map = filter.getParamMap();
			String query = "";
			if (map.get("type").getStrValue().equalsIgnoreCase("client")) {
				query = "select count(distinct req) reqs,count(distinct subs) subs,count(distinct subReqs),count(distinct interviews) interviews," +
						"count(distinct placements) placements from ( select r.id req,s.id subs,if(s.id is null,null,r.id) subReqs," +
						"if(s.submissionStatus in('I-Scheduled','I-Succeeded','I-Failed','I-Withdrawn','C-Accepted','C-Declined'),s.id,null) interviews," +
						"if(s.submissionStatus ='C-Accepted',s.id,null) placements " +
						"from recruit_requirements r " +
						"left join recruit_sub_requirements s on s.requirementId=r.id and s.submissionStatus != 'Shortlisted' and s.deleted = false " +
						"where r.clientId="+map.get("clientId").getStrValue()+" ) tab";
			}else{
				query = "select count(distinct req) reqs,count(distinct subs) subs,count(distinct subReqs),count(distinct interviews) interviews," +
						"count(distinct placements) placements from ( select r.id req,s.id subs,if(s.id is null,null,r.id) subReqs," +
						"if(s.submissionStatus in('I-Scheduled','I-Succeeded','I-Failed','I-Withdrawn','C-Accepted','C-Declined'),s.id,null) interviews," +
						"if(s.submissionStatus ='C-Accepted',s.id,null) placements " +
						"from recruit_requirements r join recruit_sub_vendors v on v.requirementId=r.id " +
						"left join recruit_sub_requirements s on s.requirementId=r.id and v.vendorId = s.vendorId and s.submissionStatus != 'Shortlisted' and s.deleted = false " +
						"where v.vendorId="+map.get("vendorId").getStrValue()+" ) tab";
			}
			
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

}
