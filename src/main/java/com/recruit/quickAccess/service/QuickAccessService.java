package com.recruit.quickAccess.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.recruit.base.service.SearchFilter;
import com.recruit.quickAccess.integration.QuickAccessDAO;


@Service
public class QuickAccessService{

	@Autowired
	private QuickAccessDAO quickAccessDAO;

	@Transactional
	public List<QuickAccess> getEmployee(SearchFilter filter) {
		return quickAccessDAO.findByCriteria(filter);
	}

	@Transactional
	public List<QuickAccess> getAllEmployees() {
		return quickAccessDAO.getAllEmployees();
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getStatistics(SearchFilter filter) {
		return quickAccessDAO.getStatistics(filter);
	}
}
