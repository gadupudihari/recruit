package com.recruit.quickAccess.presentation;


import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.recruit.base.presentation.BaseController;
import com.recruit.base.service.ParamParser;
import com.recruit.base.service.SearchFilter;
import com.recruit.quickAccess.service.QuickAccess;
import com.recruit.quickAccess.service.QuickAccessService;
import com.recruit.util.JsonParserUtility;

@Controller
@RequestMapping("/quickAccess")
@PreAuthorize("hasAnyRole('ADMIN','RECRUITER')")
public class QuickAccessController extends BaseController {

	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private QuickAccessService quickAccessService;
	
	@RequestMapping(value = "/getAllEmployees", method = {RequestMethod.POST,RequestMethod.GET })
	public String getAllEmployees(@RequestParam("json") String json, Map<String, Object> map){
		List<QuickAccess> list = quickAccessService.getAllEmployees();
		return returnJson(map, list);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getStatistics", method = {RequestMethod.POST,RequestMethod.GET })
	public String getStatistics(@RequestParam("json") String json, Map<String, Object> map){
		SearchFilter filter = ParamParser.parse(json);
		List list = quickAccessService.getStatistics(filter);
		return returnJson(map, JsonParserUtility.toStatsJson(list).toString());
	}
	 
	
}
