package com.recruit.reports.presentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.recruit.base.presentation.BaseController;
import com.recruit.base.service.Param;
import com.recruit.base.service.ParamParser;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.service.Candidate;
import com.recruit.candidate.service.Certification;
import com.recruit.reports.service.ReportsService;
import com.recruit.user.service.UserEmail;
import com.recruit.util.GeneratePDF;
import com.recruit.util.JSONException;
import com.recruit.util.JsonParserUtility;
import com.recruit.util.SendEmails;
import com.recruit.util.UtilityHelper;
import com.recruit.util.WriteExcel;

@Controller
@RequestMapping("/reports")
@PreAuthorize("hasAnyRole('ADMIN','RECRUITER')")
public class ReportsController extends BaseController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	ReportsService reportsService;

	@Autowired
	WriteExcel writeExcel;

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	
	@RequestMapping(value = "/getResult", method = {RequestMethod.POST,RequestMethod.GET })
	public String getResult(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		response = reportsService.getResult(response,json);
		return returnJson(map, response);	
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getStatistics", method = {RequestMethod.POST,RequestMethod.GET })
	public String getStatistics(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List list = reportsService.getStatistics(response,filter);
		response.setSuccess(true);
		response.setReturnVal(JsonParserUtility.toReportStatstics(list).toString());
		return returnJson(map, response);	
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getComboValues", method = {RequestMethod.POST,RequestMethod.GET })
	public String getComboValues(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		List list = reportsService.getComboValues(json);
		response.setReturnVal(list);
		response.setSuccess(Boolean.TRUE);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/loadDataReport", method = {RequestMethod.POST,RequestMethod.GET })
	public String loadDataReport(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		List<Candidate> candidates = reportsService.loadDataReport(json);
		response.setReturnVal(JsonParserUtility.toCandidateJSON(candidates).toString());
		response.setSuccess(true);
		return returnJson(map, response);	
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/loadCurrentReport", method = {RequestMethod.POST,RequestMethod.GET })
	public String loadCurrentReport(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		List requirements = reportsService.loadCurrentReport(json);
		return returnJson(map, JsonParserUtility.toCurrentReport(requirements).toString());	
	}
	
	@RequestMapping(value = "/loadCertifications", method = {RequestMethod.POST,RequestMethod.GET })
	public String loadCertifications(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		List<Certification> certifications = reportsService.loadCertifications(json);
		response.setReturnVal(certifications);
		response.setSuccess(true);
		return returnJson(map, response);	
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/loadSubmissionReport", method = {RequestMethod.POST,RequestMethod.GET })
	public String loadSubmissionReport(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		List requirements = reportsService.loadSubmissionReport(json);
		response.setReturnVal(JsonParserUtility.toSubmission(requirements).toString());
		response.setSuccess(true);
		return returnJson(map, response);	
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/exportAvailCandidates", method = {RequestMethod.POST,RequestMethod.GET })
	public String exportAvailCandidates(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request,HttpServletResponse res) throws JSONException {
		
		List list = reportsService.getAvailCandidates(json);
		String contextPath="", fileName = "Available Candidates";
		  try{
		      ServletContext servletContext = session.getServletContext();
		      contextPath = servletContext.getRealPath(File.separator);
		      System.out.println("File system context path (in TestFilter): " + contextPath);
		      System.out.println("---------------------"+list.size());
		      
		      //WriteExcel test = new WriteExcel();
		      UtilityHelper.deleteExcelFiles(contextPath, "Available Candidates");
		      
		      writeExcel.setOutputFile(contextPath+fileName+".xls");
		      //Writing to Excel File
		      writeExcel.writeAvailCandidates(list);
			  System.out.println("Download Path ------ " + contextPath+fileName+".xls");
		  }
		  catch(Exception ex){
			  System.err.println("---------- **** Exception while generating Excel file **** ---------");
			  System.err.println(ex.getMessage());
			  ex.printStackTrace();
			  response.setSuccess(false);
		  }
		  response.setSuccess(true);
		  response.setReturnVal(fileName+".xls");
		  return returnJson(map, response);
	}
	
	@RequestMapping(value = "/completeMessage", method = {RequestMethod.POST,RequestMethod.GET })
	public String completeMessage(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		reportsService.completeMessage(json);
		response.setReturnVal(" \"No data\"");
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getMessages", method = {RequestMethod.POST,RequestMethod.GET })
	public String getMessages(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		String loginId =UtilityHelper.getLoginId(request);
		List emails = reportsService.getMessages(filter,loginId);
		return returnJson(map, JsonParserUtility.toMessageJosn(emails).toString());
	}
	
	@RequestMapping(value = "/deleteMessage", method = {RequestMethod.POST,RequestMethod.GET })
	public String deleteMessage(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		reportsService.deleteMessage(json);
		response.setReturnVal(" \"No data\"");
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/activeMessage", method = {RequestMethod.POST,RequestMethod.GET })
	public String activeMessage(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		reportsService.activeMessage(json);
		response.setReturnVal(" \"No data\"");
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/pinMessage", method = {RequestMethod.POST,RequestMethod.GET })
	public String pinMessage(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		reportsService.updatePinMessage(json,true);
		response.setReturnVal(" \"No data\"");
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/unPinMessage", method = {RequestMethod.POST,RequestMethod.GET })
	public String unPinMessage(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		reportsService.updatePinMessage(json,false);
		response.setReturnVal(" \"No data\"");
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/sweepAllEmail", method = {RequestMethod.POST,RequestMethod.GET })
	public String sweepAllEmail(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		String loginId =UtilityHelper.getLoginId(request);
		reportsService.sweepAllEmail(json,loginId);
		response.setReturnVal(" \"No data\"");
		response.setSuccess(true);
		return returnJson(map, response);
	}

	//Send email from home page in messages panel
	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
	public String sendEmail(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request){
		SearchFilter filter = ParamParser.parse(json);
		Map<String, Param> filterMap = filter.getParamMap();
		String emailIds = "", subject = "", message = "";
		emailIds = filterMap.get("emailId").getStrValue();
		subject = filterMap.get("subject").getStrValue();
		message = filterMap.get("message").getStrValue();
		SendEmails.sendHtmlEMail(subject, message, emailIds);
		return returnJson(map, response);
	}

	//Send email from home page in messages panel
	@RequestMapping(value = "/sendHomeEmail", method = RequestMethod.POST)
	public String sendHomeEmail(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request){
		SearchFilter filter = ParamParser.parse(json);
		Map<String, Param> filterMap = filter.getParamMap();
		String emailIds = "", subject = "", message = "";
		String fromEmailId = filterMap.get("from").getStrValue();
		emailIds = filterMap.get("to").getStrValue();
		subject = filterMap.get("subject").getStrValue();
		message = filterMap.get("message").getStrValue();
		String cc = "";
		if (filterMap.get("cc") != null)
			cc = filterMap.get("cc").getStrValue();
		String emails[] = emailIds.split(",");
		UserEmail email = reportsService.findEmailByEmailId(fromEmailId);
		
		response = SendEmails.sendSeperateEMail(response, email, subject, message, emails,cc);
		
		return returnJson(map, response);
	}

	@RequestMapping(value = "/getSubmissionReport", method = {RequestMethod.POST,RequestMethod.GET })
	public String getSubmissionReport(@RequestParam("json") String json, Map<String, Object> map,HttpSession session) throws JSONException {
		String fileName = json;
		String path = UtilityHelper.getPropery("appSettings.properties", "submissionReportPath");
		File file = new File(path + fileName);
		ServletContext servletContext = session.getServletContext();
		String contextPath = servletContext.getRealPath(File.separator);
        System.out.println("-------------------------------------------");

        File destFolder = new File(contextPath + File.separator + "Submission Reports");
		if(!destFolder.exists()) destFolder.mkdirs();
		//delete all files in server employeeDocuments folder
		File[] filesList = destFolder.listFiles();  
		for (int n = 0; n < filesList.length; n++) {
			if (filesList[n].isFile())
				filesList[n].delete();
		}
		System.out.println("-------"+destFolder);
		
		File source = new File(path + fileName);
		File dest = new File(destFolder + File.separator + file.getName());
		try {
		    FileUtils.copyFile(source, dest);
		}catch (FileNotFoundException e) {
			response.setSuccessMessage("");
			response.setErrorMessage("Report does not exists");
			response.setSuccess(false);
			return returnJson(map, response);
		}catch (IOException e) {
		    e.printStackTrace();
		}
		response.setSuccessMessage("");
		response.setErrorMessage("");
		response.setReturnVal('"'+dest.getName()+'"');
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/sendSummaryEmail", method = RequestMethod.POST)
	public String sendSummaryEmail(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request){
		SearchFilter filter = ParamParser.parse(json);
		Map<String, Param> filterMap = filter.getParamMap();
		String fromEmailId = filterMap.get("from").getStrValue();
		String subject = filterMap.get("subject").getStrValue();
		String message = filterMap.get("message").getStrValue();
		String candidates = filterMap.get("candidates").getStrValue();
		String[] candidateNames = candidates.split(",");
		message += "<br><br>This email is sent to the following employees.";
		for (String candidateName : candidateNames) {
			message += "<br>"+candidateName;
		}
		SendEmails.sendHtmlEMail(subject, message, fromEmailId);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/getMissingData", method = {RequestMethod.POST,RequestMethod.GET })
	public String getMissingData(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		response = reportsService.getMissingData(response,json);
		return returnJson(map, response);	
	}

	@RequestMapping(value = "/pdfExport", method = {RequestMethod.POST,RequestMethod.GET })
	public String pdfExport(@RequestParam("json") String json, HttpSession session, Map<String, Object> map){
		  try{
			  System.out.println("---------- **** In PDF Export Method **** ---------");
					
			  ServletContext servletContext = session.getServletContext();
			  String contextPath = servletContext.getRealPath(File.separator);
				
			  //delete all .pdf files in server
			  File file = new File(contextPath);
			  File[] filesList = file.listFiles();  
			  for (int n = 0; n < filesList.length; n++) {
				  String ext = "";
				  int mid = 0;
				  if (filesList[n].isFile())
					  if(filesList[n].getName().contains(".")){
						  mid= filesList[n].getName().lastIndexOf(".");
						  ext=filesList[n].getName().substring(mid+1,filesList[n].getName().length());
						  if(ext.equals("pdf") ){
							  filesList[n].delete();
						  }
					  }
			  }
			  
			  SearchFilter filter = ParamParser.parse(json);
			  Map<String, Param> filterMap = filter.getParamMap();
			  String fileName = filterMap.get("fileName").getStrValue();
			  String pdfValue = filterMap.get("pdfValue").getStrValue();
			  fileName = fileName+".pdf";
			  System.out.println("Download Path ------ " + contextPath+fileName);
			  GeneratePDF.addEmailData(pdfValue,contextPath,contextPath+fileName);

			  response.setSuccessMessage("");
			  response.setErrorMessage("");
			  response.setReturnVal('"'+fileName+'"');
			  response.setSuccess(true);
		  }
		  catch(Exception ex){
			  System.err.println("---------- **** Exception while generating PDF file **** ---------");
			  System.err.println(ex.getMessage());
				response.setSuccessMessage("");
				response.setErrorMessage(" ");
				response.setSuccess(false);
				return returnJson(map, response);
		  }		
		 //returning filename to js
		return returnJson(map, response);
	
	}


}