package com.recruit.reports.integration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.presentation.Response;
import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.service.Candidate;
import com.recruit.candidate.service.Certification;

@Repository
public class ReportDAO {

	private static final Log log = LogFactory.getLog(ReportDAO.class);
	
	@Autowired
	private  SessionFactory sessionFactory;
	
	public Response getRecords(Response response,String query){
		try {
			System.err.println(query);
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("jdbc.properties"));
			String url = properties.getProperty("jdbc.databaseurl");
			String driver = properties.getProperty("jdbc.driverClassName");
			String userName = properties.getProperty("jdbc.username");
			String password = properties.getProperty("jdbc.password");
			
			if (url.contains("autoReconnect")) 				
				url = url.substring(0,url.indexOf("autoReconnect")-1);
			
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url, userName, password);
			Statement stmt = conn.createStatement();
			ResultSet rs=stmt.executeQuery(query);
			ResultSetMetaData md = rs.getMetaData();
			List<String> columnNames = new ArrayList<String>();
			
			String json ="<table width='100%' style='border:1px solid black;border-collapse:collapse;' ><tr>";
			
			for (int i = 1; i <= md.getColumnCount(); i++){
				columnNames.add(md.getColumnLabel(i));
				if (!md.getColumnLabel(i).equalsIgnoreCase(""))
					json += "<th height='25' style='background-color:#FFFF99;font-weight:bold;border:1px solid black;'>"+md.getColumnLabel(i)+"</th>";
			}
			
			json += "</tr>";
			
			while (rs.next()) {
				json +="<tr>";
				for (int i = 0; i < columnNames.size(); i++) {
					if (rs.getString(i+1) != null)
						json += "<td style='border:1px solid black;'>"+rs.getString(i+1)+"</td>";
					else
						json += "<td style='border:1px solid black;'></td>";
				}
				json +="</tr>";
			}
			json += "</table>";
			System.out.println("json-----------"+json);
			response.setReturnVal(json);
			response.setSuccess(true);
			response.setErrorMessage("");
			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMessage(e.getMessage());
			response.setReturnVal(" \"No data\"");
			System.err.println(e.getMessage());
			return response;
		}
	}

	@SuppressWarnings("rawtypes")
	public List getStatistics(SearchFilter filter){
		try {
			Map<String, Param> map = filter.getParamMap();
			String query = map.get("query").getStrValue();
			System.out.println(query);
			List list = sessionFactory.getCurrentSession().createSQLQuery(query).list();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	public List getComboValues(String query) {
		try {
			List list = sessionFactory.getCurrentSession().createSQLQuery(query).list();
			return list;
		} catch (RuntimeException e) {
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Candidate> loadDataReport(String query) {
		try {
			List<Candidate> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.setResultTransformer(Transformers.aliasToBean(Candidate.class))
					.list();
			return list;
		} catch (RuntimeException e) {
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public List loadCurrentReport(String query) {
		try {
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Certification> loadCertifications(String query) {
		try {
			System.out.println(query);
			List<Certification> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public List getAvailCandidates(String query) {
		try {
			List list = sessionFactory.getCurrentSession().createSQLQuery(query).list();
			return list;
		} catch (RuntimeException e) {
			throw e;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public List getMessages(SearchFilter filter, String loginId) {
		try {
			String query = "select id,eMailId,createdDate,alertDate,subject,message,alertInDays,refId,completedDate,deletedDate,priority,pinned,alertName," +
					"case" +
					" when date(alertdate) = date(now()) then 1" +
					" when date(alertdate) = date(date_sub(now(),interval 1 day)) then 2 " +
					" when date(alertdate) > date(date_sub(now(),interval 7 day)) then 3 " +
					" when month(alertdate) = month(now()) then 4 " +
					" when month(alertdate) = month(date_sub(now(),interval 1 month)) then 5 " +
					" when month(alertdate) = month(date_sub(now(),interval 2 month)) then 6" +
					" when month(alertdate) = month(date_sub(now(),interval 3 month)) then 7" +
					" when month(alertdate) = month(date_sub(now(),interval 4 month)) then 8 " +
					" when month(alertdate) = month(date_sub(now(),interval 5 month)) then 9 " +
					" when month(alertdate) = month(date_sub(now(),interval 6 month)) then 10 " +
					" when 1=1 then 11 " +
					"end as status,(select concat(firstName,' ',lastName) from recruit_candidate where id = refId) " +
					"from Email where emailId = (select emailId from recruit_user where loginId='"+loginId+"')  and application = 'recruit' " ;
			
			Map<String, Param> map = filter.getParamMap();
			if (map.get("report") != null ) {
				if (map.get("report").getStrValue().equalsIgnoreCase("Inbox"))
					query += " and completedDate is NULL ";
				else if (map.get("report").getStrValue().equalsIgnoreCase("Pinned")) 
					query += " and completedDate is NULL and pinned = true ";
				else if (map.get("report").getStrValue().equalsIgnoreCase("Completed")) 
					query += " and completedDate is not NULL ";
				else if (map.get("report").getStrValue().equalsIgnoreCase("Deleted")) 
					query += " and deletedDate is not NULL ";
			}else
				query += " and completedDate is NULL ";
			
			query += " order by status,priority,alertDate DESC";
			
			System.out.println(query);
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	public void completeMessage(String id) {
		try {
			String sql = "update email set completedDate = now() where id="+id;
			sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			System.out.println(sql);
			log.debug("complete successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}
	
	public void deleteMessage(String id) {
		try {
			String sql = "update email set deletedDate = now() where id="+id;
			sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			System.out.println(sql);
			log.info("delete successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void activeMessage(String id) {
		try {
			String sql = "update email set completedDate = null where id="+id;
			sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			System.out.println(sql);
			log.debug("complete successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void updatePinMessage(String id, boolean pinned) {
		try {
			String sql = "update email set pinned = "+pinned+" where id="+id;
			sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			System.out.println(sql);
			log.debug("pinned successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void sweepAllEmail(String group, String loginId) {
		try {
			String sql = "update email set completedDate=now() where (pinned = false or pinned is null) and application = 'recruit' and eMailId=(select emailId from user where loginId='"+loginId+"')";
			
			if (group.equalsIgnoreCase("Today"))
				sql +=" and date(alertdate) = date(now())";
			else if (group.equalsIgnoreCase("Yesterday"))
				sql +=" and date(alertdate) = date(date_sub(now(),interval 1 day))";
			else if (group.equalsIgnoreCase("This Week"))
				sql +=" and date(alertdate) > date(date_sub(now(),interval 7 day)) and date(alertdate) < date(date_sub(now(),interval 1 day))";
			else if (group.equalsIgnoreCase("This Month"))
				sql +=" and month(alertdate) = month(now()) and date(alertdate) <= date(date_sub(now(),interval 7 day)) ";
			else if (group.equalsIgnoreCase("Earlier"))
				sql +=" and month(alertdate) < month(date_sub(now(),interval 6 month)) and date(alertdate) < date(date_sub(now(),interval 6 month))";
			else 
				sql +=" and date_format(alertDate,'%M %Y') = '"+ group+"'";
			
			sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			System.out.println(sql);
			log.debug("pinned successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}


	@SuppressWarnings("rawtypes")
	public List loadSubmissionReport(String query) {
		try {
			System.out.println(query);
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			throw e;
		}
	}
	
	// insert and deletes from the email table
	public void updateEmailRecords(String query) {
		try {
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
			System.err.println(query);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public Response getMissingData(Response response, String query) {
		try {
			System.err.println(query);
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("jdbc.properties"));
			String url = properties.getProperty("jdbc.databaseurl");
			String driver = properties.getProperty("jdbc.driverClassName");
			String userName = properties.getProperty("jdbc.username");
			String password = properties.getProperty("jdbc.password");
			
			if (url.contains("autoReconnect")) 				
				url = url.substring(0,url.indexOf("autoReconnect")-1);
			
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url, userName, password);
			Statement stmt = conn.createStatement();
			ResultSet rs=stmt.executeQuery(query);
			
			String json ="<table cellpadding='5' bgcolor='#575252'>" +
					"<tr height='25' style='background-color:#FFFF99;'>"+
					"<th>Field</th><th>Missing</th><th>%</th></tr>";
			double totalCount =0;
			String totalCountString ="";
			while (rs.next()) {
				if (rs.getString("field").equalsIgnoreCase("All")){
					totalCount = rs.getDouble("count");
					totalCountString = rs.getString("count");
				}else{
					json +="<tr bgcolor='#FFFFFF' style='text-align:center;'>";
					json += "<td>"+rs.getString("field")+"</td>";
					json += "<td>"+rs.getString("count")+'/'+totalCountString+"</td>";
					json += "<td>"+Math.round(rs.getDouble("count")*100/totalCount * 100.0) / 100.0+"%</td>";
					json +="</tr>";
				}
			}
			json += "</table>";
			System.out.println("json-----------"+json);
			response.setReturnVal(json);
			response.setSuccess(true);
			response.setErrorMessage("");
			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			response.setErrorMessage(e.getMessage());
			response.setReturnVal(" \"No data\"");
			System.err.println(e.getMessage());
			return response;
		}
	}

}
