package com.recruit.reports.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.recruit.base.presentation.Response;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.service.Candidate;
import com.recruit.candidate.service.Certification;
import com.recruit.reports.integration.ReportDAO;
import com.recruit.user.integration.UserEmailDAO;
import com.recruit.user.service.UserEmail;

@Service
public class ReportsService {

	@Autowired
	ReportDAO reportDAO;

	@Autowired
	UserEmailDAO userEmailDAO; 
	
	@Transactional
	public Response getResult(Response response, String query) {
		return reportDAO.getRecords(response, query);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getStatistics(Response response, SearchFilter filter) {
		return reportDAO.getStatistics(filter);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getComboValues(String query) {
		return reportDAO.getComboValues(query);
	}

	@Transactional
	public List<Candidate> loadDataReport(String query) {
		return reportDAO.loadDataReport(query);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List loadCurrentReport(String query) {
		return reportDAO.loadCurrentReport(query);
	}

	@Transactional
	public List<Certification> loadCertifications(String query) {
		return reportDAO.loadCertifications(query);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getAvailCandidates(String query) {
		return reportDAO.getAvailCandidates(query);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List loadSubmissionReport(String query) {
		return reportDAO.loadSubmissionReport(query);
	}
	
	@SuppressWarnings("rawtypes")
	@Transactional
	public List getMessages(SearchFilter filter, String loginId) {
		return reportDAO.getMessages(filter,loginId);
	}

	@Transactional
	public void completeMessage(String id) {
		reportDAO.completeMessage(id);
	}
	
	@Transactional
	public void deleteMessage(String id) {
		reportDAO.deleteMessage(id);
	}

	@Transactional
	public void activeMessage(String id) {
		reportDAO.activeMessage(id);
	}

	@Transactional
	public void updatePinMessage(String id,boolean pinned) {
		reportDAO.updatePinMessage(id,pinned);
	}

	@Transactional
	public void sweepAllEmail(String group, String loginId) {
		reportDAO.sweepAllEmail(group, loginId);
	}

	@Transactional
	public void updateEmailRecords(String query) {
		reportDAO.updateEmailRecords(query);
	}

	@Transactional
	public UserEmail findEmailByEmailId(String fromEmailId) {
		return userEmailDAO.findByEmailId(fromEmailId);
	}

	public Response getMissingData(Response response, String query) {
		return reportDAO.getMissingData(response, query);
	}

}
