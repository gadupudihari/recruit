package com.recruit.user.presentation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.recruit.base.presentation.BaseController;
import com.recruit.user.service.User;
import com.recruit.user.service.UserService;
import com.recruit.util.SendEmails;

import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

@Controller
@RequestMapping("/recoveryPassword")
public class RecoveryPasswordController extends BaseController {
	
    @Autowired
    private UserService userService;
    
    //To Dos
    //Add logic to check if the corresponding emailID/Login Id is present in DB
    //Generate a Random password
    //update user table with new password.
    //send email to the user based on the retrieved emailId from DB    
	@RequestMapping(value = "/pass", method =RequestMethod.POST)
	public String recoverPassword(@RequestParam String email, ModelMap model){
        String loginId = email.substring(0,email.indexOf(','));
        String emailId = email.substring(email.indexOf(',')+1);
        
        List<User> users = userService.getUsers(loginId,emailId);
        if(users.size() == 0){
        	model.addAttribute("failMessage", "Password Recovery Failed ! Invalid loginId or EmailId");
        }else if (users.get(0).getLoginId() == null || users.get(0).getLoginId() == "" || users.get(0).getPassword() == null || users.get(0).getPassword() == "" ) {
        	model.addAttribute("failMessage", "You don't have access to AJ. Please contact to Administrator.");
		}else {
    		String pass = userService.getRandom(8);
    		String md5 = userService.md5(pass);
    		User user = users.get(0);
    		user.setPassword(md5);
    		userService.saveUser(user);
    		String subject = "Recruit : Password Recovery";
    		String body = "Dear User,"+"\n\n"+"Your New Password for Recruit is "+pass+"\n"+"Your LoginId is "+user.getLoginId()+"."+"\n\n"+"Thanks"+"\n"+"AJ Team.";
    		try {
				SendEmails.sendRecoveryMail(subject, body, user.getEmailId());
			} catch (AddressException e) {
				e.printStackTrace();
			} catch (MessagingException e) {
				e.printStackTrace();
			}
    		//sendEMail(subject, body, emailId);
    		model.addAttribute("successMessage", "A New Password is Sent to your emailId "+ user.getEmailId());
		}
        return "/emailPass";
    }
	
	
}