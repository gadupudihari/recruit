package com.recruit.user.presentation;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.recruit.authority.service.Authority;
import com.recruit.base.presentation.BaseController;
import com.recruit.base.presentation.Response;
import com.recruit.base.service.ParamParser;
import com.recruit.base.service.SearchFilter;
import com.recruit.user.service.User;
import com.recruit.user.service.UserService;
import com.recruit.util.JSONException;
import com.recruit.util.JSONParser;
import com.recruit.util.JsonParserUtility;
import com.recruit.util.UtilityHelper;

@Controller
@RequestMapping("/user")
@PreAuthorize("hasAnyRole('ADMIN','RECRUITER')")
public class UserController extends BaseController {

	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map) throws JSONException {
		return returnJson(map, userService.list());
	}

	@RequestMapping(value = "/getUsers", method = {RequestMethod.POST,RequestMethod.GET })
	public String getUsers(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<User> users = userService.getUsers(filter);
		return returnJson(map, JsonParserUtility.toUserJSON(users).toString());
	}

	@RequestMapping(value = "/get/{id}", method = {RequestMethod.POST,RequestMethod.GET })
	public String get(@PathVariable("id") Integer userId, HttpSession session, Map<String, Object> map)
			throws JSONException {
		User user = userService.get(userId);
		return returnJson(map, user);
		
	}

	@RequestMapping(value = "/loadRoles", method = { RequestMethod.POST,RequestMethod.GET })
	public String loadRoles(Map<String, Object> map) throws JSONException {
		List<Authority> authority = userService.getAuthorityDetails();
		return returnJson(map, JsonParserUtility.toJSONAuthority(authority).toString());
	}

	@RequestMapping(value = "/savePassword", method = RequestMethod.POST)
	public String savePassword(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		response = new Response();
		//Getting loginId from request object
		
		String loginId =UtilityHelper.getLoginId(request);	
		int index = json.indexOf(",");
		String jsonVal = json.substring(index+1,json.length()-2);
		String oldPass[] = jsonVal.split(":");
		String oldPassword = oldPass[1].substring(1);
		json =	json.substring(0, index)+'}';
		//String newJson = json.substring(0, json.length()-2)+'"'+loginId+'"'+'}';
		if(!userService.checkPassword(loginId,oldPassword)){
			response.setSuccess(Boolean.FALSE);
			return returnJson(map, response);
		}
		User user = (User) JSONParser.parseJSON(json, User.class);	
		user.setLoginId(loginId);
		//userService.checkValid(user,oldPassword);
		userService.savePassword(user);
		response.setReturnVal(json);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public String saveUser(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		User user = (User) JSONParser.parseJSON(json, User.class);	
		userService.save(user);
		response.setReturnVal(user);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/delete/{id}", method = {RequestMethod.POST,RequestMethod.GET })
	public String delete(@PathVariable("id") Integer userId, HttpSession session, Map<String, Object> map)throws JSONException {
		userService.deleteUser(userId);
		response.setSuccess(true);
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/resetPassword/{id}", method = {RequestMethod.POST,RequestMethod.GET })
	public String resetPassword(@PathVariable("id") Integer userId, HttpSession session, Map<String, Object> map)throws JSONException {
		String password = userService.resetPassword(userId);
		response.setSuccessMessage(password);
		response.setSuccess(true);
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/givingAcess/{id}", method = {RequestMethod.POST,RequestMethod.GET })
	public String givingAcess(@PathVariable("id") Integer userId, HttpSession session, Map<String, Object> map)throws JSONException {
		String password = userService.givingAcess(response,userId);
		response.setSuccessMessage(password);
		response.setSuccess(true);
		return returnJson(map, response);
	}
	@RequestMapping(value = "/revokeAcess/{id}", method = {RequestMethod.POST,RequestMethod.GET })
	public String revokeAcess(@PathVariable("id") Integer userId, HttpSession session, Map<String, Object> map)throws JSONException {
		userService.revokeAcess(response,userId);
		response.setSuccess(true);
		return returnJson(map, response);
	}

}
