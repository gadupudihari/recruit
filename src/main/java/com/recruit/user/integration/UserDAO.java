package com.recruit.user.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.user.service.User;



/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class UserDAO {

	private static final Log log = LogFactory.getLog(UserDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(User transientInstance) {
		log.debug("persisting User instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(User instance) {
		log.debug("attaching dirty User instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(User persistentInstance) {
		log.debug("deleting User instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public User merge(User detachedInstance) {
		log.debug("merging User instance");
		try {
			User result = (User) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public User findById(java.lang.Integer id) {
		log.debug("getting User instance with id: " + id);
		try {
			User instance = (User) sessionFactory.getCurrentSession()
					.get("com.recruit.user.service.User", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> findByLoginId(String loginId) {
		try {
			String query = "from User as u where 1=1 ";
						
			if (loginId != null) {
				query += " and u.loginId = '" + loginId+"'";
			}
			
			List<User> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<User> findByExample(User instance) {
		log.debug("finding User instance by example");
		try {
			List<User> results = (List<User>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.user.service.User").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	//Get all Email IDs
	//@ Author - Venkat
	@SuppressWarnings("unchecked")
	@Transactional	
	public List<String> getEmails() {
		log.debug("getting all Email Ids");
		try {
			String query = "select emailId from User as u where 1=1 ";									
			return sessionFactory.getCurrentSession().createQuery(query).list();
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> getUser() {
		log.debug("getting all users");
		try {
			String query = "from User as u where 1=1 ";									
			List<User> list = sessionFactory.getCurrentSession().createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> FindByEmail(String emailId) {
		try {
			String query = "from User as u where 1=1 and u.emailId = '"+ emailId+"'";
						
			List<User> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> findByCriteria(SearchFilter filter) {
		try {
			String query = "select t.*,(select authority from recruit_authority where loginId=t.loginId) authority," +
					"(select excludeConfidential from recruit_authority where loginId=t.loginId) excludeConfidential from recruit_user t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("userId") != null) {
				query += " and t.id like '" + map.get("userId").getStrValue()+ "'";
			}
			if (map.get("active") != null) {
				if (map.get("active").getStrValue().equalsIgnoreCase("YES")) {
					query += " and t.active = true" ;
				}else if (map.get("active").getStrValue().equalsIgnoreCase("NO")) {
					query += " and t.active = false" ;	
				}
			}
			if (map.get("authority") != null) {
				query = "select * from("+query+")tab where authority = '"+map.get("authority").getStrValue()+"'";
			}
			System.out.println("User  : "+query);
			
			List<User> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.setResultTransformer(Transformers.aliasToBean(User.class))
					.list();

			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> getAdminUsers() {
		try {
			String query = "SELECT u.* FROM recruit_user u, recruit_authority a where u.loginId=a.loginId and a.authority='ADMIN'";
			
			List<User> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.setResultTransformer(Transformers.aliasToBean(User.class))
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}
	
	// to send email alerts
	@SuppressWarnings("unchecked")
	public List<User> getEmailAdmin() {
		try {
			String query = "SELECT u.* FROM recruit_user u, recruit_authority a where u.loginId=a.loginId and a.authority in ('ADMIN','RECRUITER') and emailId not in ('pratima@candorps.com')";
			
			List<User> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.setResultTransformer(Transformers.aliasToBean(User.class))
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> getAJAdmins() {
		try {
			String query = "SELECT u.id,u.loginId,u.password,u.firstName,u.lastName,u.emailId,u.active FROM user u, authority a " +
					"where u.loginId=a.loginId and a.authority like 'ADMIN%' and emailId not in ('pratima@candorps.com')";
			
			List<User> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.setResultTransformer(Transformers.aliasToBean(User.class))
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

}
