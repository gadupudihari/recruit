package com.recruit.user.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.user.service.UserEmail;

/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class UserEmailDAO {

	private static final Log log = LogFactory.getLog(UserEmailDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(UserEmail transientInstance) {
		log.debug("persisting UserEmail instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(UserEmail instance) {
		log.debug("attaching dirty UserEmail instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(UserEmail persistentInstance) {
		log.debug("deleting UserEmail instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UserEmail merge(UserEmail detachedInstance) {
		log.debug("merging UserEmail instance");
		try {
			UserEmail result = (UserEmail) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public UserEmail findById(java.lang.Integer id) {
		log.debug("getting UserEmail instance with id: " + id);
		try {
			UserEmail instance = (UserEmail) sessionFactory.getCurrentSession()
					.get("com.recruit.UserEmail.service.UserEmail", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<UserEmail> findByLoginId(String loginId) {
		try {
			String query = "from UserEmail as u where 1=1 ";
						
			if (loginId != null) {
				query += " and u.user.loginId = '" + loginId+"'";
			}
			
			List<UserEmail> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean checkEmailExists(String emailId) {
		try {
			String query = "from UserEmail as u where 1=1 and u.emailId = '" + emailId+"'";
			
			List<UserEmail> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			if (list.size() > 0)
				return true;
			return false;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public UserEmail findByEmailId(String emailId) {
		try {
			String query = "from UserEmail as u where 1=1 and u.emailId = '" + emailId+"'";
			
			List<UserEmail> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			if (list.size() > 0)
				return list.get(0);
			return null;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public void deleteByEmailId(String emailId) {
		try {
			String query = "Delete from UserEmail where 1=1 and emailId = '" + emailId+"'";
			
			sessionFactory.getCurrentSession().createQuery(query).executeUpdate();
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
}
