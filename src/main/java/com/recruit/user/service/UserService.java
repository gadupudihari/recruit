package com.recruit.user.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dataengine.security.SecurityContext;
import com.recruit.authority.integration.AuthorityDAO;
import com.recruit.authority.service.Authority;
import com.recruit.base.presentation.Response;
import com.recruit.base.presentation.TSSecurityContext;
import com.recruit.base.service.SearchFilter;
import com.recruit.user.integration.UserDAO;


@Service
public class UserService{

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private AuthorityDAO authorityDAO;

	@Transactional
	public void save(User user) {
		if (user.getLoginId() != null && !user.getLoginId().equals("")) {
			Authority authority = authorityDAO.findById(user.getLoginId());
			authority.setAuthority(user.getAuthority());
			authority.setExcludeConfidential(user.getExcludeConfidential());
			authorityDAO.persist(authority);
		}
		userDAO.persist(user);
	}
	
	@Transactional
	public void save(Response response, User user) {
		User userInst = userDAO.merge(user);
		userDAO.persist(userInst);
		response.setSuccess(Boolean.TRUE);
	}
	
	@Transactional
	public void savePassword(User user) {
		//User userInst = userDAO.merge(user);
		List<User> users = userDAO.findByLoginId(user.getLoginId());
		if(users != null){		
			User user1 =(User)users.get(0);
			String md5value= this.md5(user.getPassword());
			user1.setPassword(md5value);
			userDAO.persist(user1);
		}		
	}
	
	@Transactional
	public boolean checkPassword(String loginId, String oldPassword) {
		List<User> users = userDAO.findByLoginId(loginId);
		String md5value= this.md5(oldPassword);
		int count =0;
		for (User user : users) {
			if (md5value.equals(user.getPassword())) {
				count++;
			}	
		}
		if (count>0) 
			return true;
		else
			return false;
	} 
	
	@Transactional
	public void saveLoginId(User user,String empPass) {
		User user1 = userDAO.findById(user.getId().intValue());
		if(user1 != null){
			String loginId=user.getEmailId();
			user1.setLoginId(loginId);
			//String password = "12345678";
			user1.setPassword(empPass);
			this.savePassword(user1);
			userDAO.persist(user1);
		}		
	}
	
	@Transactional
	public void deleteLogiId(User user) {
		if(user != null){
			user.setLoginId("");			
			user.setPassword("");
			userDAO.persist(user);
		}		
	}
	
	@Transactional
	public String resetPassword(int id) {
		User user = userDAO.findById(id);
		String password = getRandom(8);
		String md5=md5(password);
		user.setPassword(md5);
		userDAO.persist(user);
		return password;
	}
	
		
	public String getRandom(int size ) {
		String keylist="abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRTUVWXYZ123456789@*&$/?+%#!";
		String temp= "";
		Random random=new Random();
		for (int i=0;i<size;i++)
			temp+=keylist.charAt(random.nextInt(68));
		return temp;
	}
		
	public  String hex(byte[] array) {
	  StringBuffer sb = new StringBuffer();
	  for (int i = 0; i < array.length; ++i) {
	    sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
	  }
	  return sb.toString();
	}
	 
	public  String md5(String message) { 
	  try { 
	    MessageDigest md = MessageDigest.getInstance("MD5"); 
	    return hex (md.digest(message.getBytes("CP1252"))); 
	  } catch (NoSuchAlgorithmException e) { } catch (UnsupportedEncodingException e) { } 
	  return null;
	}
	
	
	@Transactional
	public List<User> list() {
		return userDAO.findByExample(new User());
	}

	@Transactional
	public User get(Integer userId) {
		return userDAO.findById(userId);
	}
	
	@Transactional
	public void saveUser(User user) {
		userDAO.persist(user);
	}

	@Transactional
	public List<User> findByLoginId(String loginId) {
		return userDAO.findByLoginId(loginId);
	}

	@Transactional
	public List<User> getUsers(String loginId, String emailId) {
		List<User> users = new ArrayList<User>();
		if (loginId.length() >0) {
			users = userDAO.findByLoginId(loginId);
		}
		if(users.size() == 0 && emailId.length()>0){
			users = userDAO.FindByEmail(emailId);
		}
		return users;
	}

	@Transactional
	public String givingAcess(Response response, int id) {
		User user = userDAO.findById(id);
		user.setLoginId(user.getEmailId());
		String password = getRandom(8);
		String md5 = md5(password);
		user.setPassword(md5);
		userDAO.persist(user);
		Authority authority = new Authority();
		authority.setLoginId(user.getLoginId());
		authority.setAuthority("ADMIN");
		authorityDAO.persist(authority);
		System.out.println("------------------"+password);
		user.setAuthority(authority.getAuthority());
		response.setReturnVal(user);
		return password;
	}

	@Transactional
	public List<Authority> getAuthorityDetails() {
		return authorityDAO.getAuthorityDetails();
	}

	@Transactional
	public List<User> getUsers(SearchFilter filter) {
		return userDAO.findByCriteria(filter);
	}

	@Transactional
	public void deleteUser(Integer userId) {
		User user = userDAO.findById(userId);
		if (user.getLoginId() != null && !user.getLoginId().equals("")) {
			Authority authority = authorityDAO.findById(user.getLoginId());
			authorityDAO.delete(authority);
		}
		userDAO.delete(user);
	}

	@Transactional
	public void revokeAcess(Response response, Integer userId) {
		User user = userDAO.findById(userId);
		Authority authority = authorityDAO.findById(user.getLoginId());
		user.setLoginId(null);
		user.setPassword(null);
		userDAO.attachDirty(user);
		authorityDAO.delete(authority);
		response.setReturnVal(user);
	}

	@Transactional
	public List<User> getAdminUsers() {
		return userDAO.getAdminUsers();
	}

	@Transactional
	public User loggedUser(HttpServletRequest request) {
		Object obj= request.getAttribute(SecurityContext.DATENGINE_CONTEXT);
		TSSecurityContext user = (TSSecurityContext)obj;
		String loginId =user.getUserId();
		List<User> users = userDAO.findByLoginId(loginId);
		return users.get(0);
	}

	@Transactional
	public List<User> getEmailAdmin() {
		return userDAO.getEmailAdmin();
	}

	@Transactional
	public List<User> getAJAdmins() {
		return userDAO.getAJAdmins();
	}
	
}
