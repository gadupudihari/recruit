package com.recruit.client.presentation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.candidate.service.Candidate;
import com.recruit.client.service.Client;
import com.recruit.client.service.Contact;
import com.recruit.util.SqlTimestampConverter;

public class ContactParser {

	public static List<Contact> parseJSON(String json){
        List<Contact> contacts = new ArrayList<Contact>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	Contact contact = new Contact();
            	if(rObj.has("id")){
            		contact.setId(rObj.getInt("id"));     
            	}
            	if(rObj.has("created") && !rObj.getString("created").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		contact.setCreated((Timestamp)c.fromString(rObj.getString("created")));
            	}
            	if(rObj.has("lastUpdated") && !rObj.getString("lastUpdated").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		contact.setLastUpdated((Timestamp)c.fromString(rObj.getString("lastUpdated")));
            	}
            	if (rObj.has("comments")) {
            		contact.setComments(rObj.getString("comments"));
				}
            	if (rObj.has("lastUpdatedUser")) {
            		contact.setLastUpdatedUser(rObj.getString("lastUpdatedUser"));
				}
            	if (rObj.has("email")) {
            		contact.setEmail(rObj.getString("email"));
				}
            	if (rObj.has("firstName")) {
            		contact.setFirstName(rObj.getString("firstName"));
				}
            	if (rObj.has("lastName")) {
            		contact.setLastName(rObj.getString("lastName"));
				}
            	if (rObj.has("internetLink")) {
            		contact.setInternetLink(rObj.getString("internetLink"));
				}
            	if (rObj.has("jobTitle")) {
            		contact.setJobTitle(rObj.getString("jobTitle"));
				}
            	if (rObj.has("workPhone")) {
            		contact.setWorkPhone(rObj.getString("workPhone"));
				}
            	if (rObj.has("cellPhone")) {
            		contact.setCellPhone(rObj.getString("cellPhone"));
				}
            	if (rObj.has("score")) {
					contact.setScore(rObj.getInt("score"));
				}
            	if (rObj.has("extension")) {
					contact.setExtension(rObj.getString("extension"));
				}
            	if (rObj.has("confidentiality")) {
            		contact.setConfidentiality(rObj.getInt("confidentiality"));
				}
            	if (rObj.has("type")) {
					contact.setType(rObj.getString("type"));
				}
            	if (rObj.has("role")) {
            		contact.setRole(rObj.getString("role"));
				}
            	if (rObj.has("gender")) {
					contact.setGender(rObj.getString("gender"));
				}
            	if (rObj.has("module")) {
					contact.setModule(rObj.getString("module"));
				}
            	if (rObj.has("priority")) {
					contact.setPriority(rObj.getInt("priority"));
				}
            	if (rObj.has("placementComments")) {
					contact.setPlacementComments(rObj.getString("placementComments"));
				}
            	if (rObj.has("SMEComments")) {
					contact.setSMEComments(rObj.getString("SMEComments"));
				}
            	if (rObj.has("candidateDeleted")) {
					contact.setCandidateDeleted(rObj.getBoolean("candidateDeleted"));
				}
            	if (rObj.has("clientId")) {
					Client client = new Client();
					client.setId(rObj.getInt("clientId"));
					contact.setClient(client);
				}
            	if (rObj.has("candidateId")) {
					Candidate candidate = new Candidate();
					candidate.setId(rObj.getInt("candidateId"));
					contact.setCandidate(candidate);
				}
            	
            	contacts.add(contact);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return contacts;
	}

}
