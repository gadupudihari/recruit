package com.recruit.client.presentation;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.recruit.base.presentation.BaseController;
import com.recruit.base.service.Param;
import com.recruit.base.service.ParamParser;
import com.recruit.base.service.SearchFilter;
import com.recruit.client.service.Client;
import com.recruit.client.service.ClientService;
import com.recruit.client.service.Contact;
import com.recruit.user.service.User;
import com.recruit.user.service.UserService;
import com.recruit.util.JSONException;
import com.recruit.util.JSONParser;
import com.recruit.util.JsonParserUtility;
import com.recruit.util.UtilityHelper;
import com.recruit.util.WriteExcel;

@Controller
@RequestMapping("/client")
@PreAuthorize("hasAnyRole('ADMIN','RECRUITER')")
public class ClientController extends BaseController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	ClientService clientService;

	@Autowired
	UserService userService;

	@Autowired
	WriteExcel writeExcel;

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	
	@RequestMapping(value = "/getClients", method = {RequestMethod.POST,RequestMethod.GET })
	public String getClients(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		String loginId =UtilityHelper.getLoginId(request);
		List<Client> clients = clientService.getClients(filter,loginId);
		return returnJson(map, JsonParserUtility.toClientJSON(clients).toString());
	}

	@RequestMapping(value = "/getAllClients", method = {RequestMethod.POST,RequestMethod.GET })
	public String getAllClients(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Client> clients = clientService.getAllClients(filter);
		return returnJson(map, JsonParserUtility.toClientJSON(clients).toString());
	}

	@RequestMapping(value = "/saveClients",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveClients(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		List<Client> clients = ClientParser.parseJSON(json);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		clientService.saveClients(clients, loginId, createdDate,response);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/saveclient",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveclient(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		Client client = (Client) JSONParser.parseJSON(json, Client.class);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		client.setLastUpdated(createdDate);
		client.setLastUpdatedUser(loginId);
		clientService.saveClient(client,response);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/deleteClient/{id}", method = RequestMethod.GET)
	public String deleteClient(@PathVariable("id") Integer id, Map<String, Object> map){
		clientService.deleteClient(response,id);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/getContacts", method = {RequestMethod.POST,RequestMethod.GET })
	public String getContacts(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		String loginId =UtilityHelper.getLoginId(request);
		List<Contact> contacts = clientService.getContacts(filter,loginId);
		return returnJson(map, JsonParserUtility.toContactJSON(contacts).toString());
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getAllContacts", method = {RequestMethod.POST,RequestMethod.GET })
	public String getAllContacts(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		List contacts = clientService.getAllContacts();
		return returnJson(map, JsonParserUtility.toContactListJSON(contacts).toString());
	}

	@RequestMapping(value = "/saveContacts",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveContacts(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		System.out.println("-------------------"+json);
		List<Contact> contacts = ContactParser.parseJSON(json);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		clientService.saveContacts(response,contacts, loginId, createdDate);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/deleteContact/{id}", method = RequestMethod.GET)
	public String deleteContact(@PathVariable("id") Integer id, Map<String, Object> map){
		clientService.deleteContact(response,id);
		response.setReturnVal(" \"No data\"");
		return returnJson(map, response);
	}

	@RequestMapping(value = "/contactExcelExport", method = {RequestMethod.POST,RequestMethod.GET })
	public String contactExcelExport(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request,HttpServletResponse res) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		String loginId =UtilityHelper.getLoginId(request);
		List<Contact> contacts = clientService.getContacts(filter,loginId);
		Map<String, Param> filterMap = filter.getParamMap();
		String columns[] = filterMap.get("columns").getStrValue().split(",");
		String contextPath="", fileName = "Contacts";
		  try{
		      ServletContext servletContext = session.getServletContext();
		      contextPath = servletContext.getRealPath(File.separator);
		      System.out.println("File system context path (in TestFilter): " + contextPath);
		      
		      UtilityHelper.deleteExcelFiles(contextPath, "Contacts");
		      
		      writeExcel.setOutputFile(contextPath+fileName+".xls");
		      //Writing to Excel File
			  writeExcel.writeContacts(contacts,columns);
			  System.out.println("Download Path ------ " + contextPath+fileName+".xls");
		  }
		  catch(Exception ex){
			  System.err.println("---------- **** Exception while generating Excel file **** ---------");
			  System.err.println(ex.getMessage());
			  ex.printStackTrace();
			  response.setSuccess(false);
		  }
		  response.setSuccess(true);
		  response.setReturnVal(fileName+".xls");
		  return returnJson(map, response);
	}

	@RequestMapping(value = "/clientExcelExport", method = {RequestMethod.POST,RequestMethod.GET })
	public String clientExcelExport(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request,HttpServletResponse res) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		String loginId =UtilityHelper.getLoginId(request);
		List<Client> clients = clientService.getClients(filter,loginId);
		Map<String, Param> filterMap = filter.getParamMap();
		String columns[] = filterMap.get("columns").getStrValue().split(",");
		String contextPath="", fileName = "Clients";
		  try{
		      ServletContext servletContext = session.getServletContext();
		      contextPath = servletContext.getRealPath(File.separator);
		      System.out.println("File system context path (in TestFilter): " + contextPath);
		      
		      UtilityHelper.deleteExcelFiles(contextPath, "Clients");
		      
		      writeExcel.setOutputFile(contextPath+fileName+".xls");
		      //Writing to Excel File
			  writeExcel.writeClients(clients,columns);
			  System.out.println("Download Path ------ " + contextPath+fileName+".xls");
		  }
		  catch(Exception ex){
			  System.err.println("---------- **** Exception while generating Excel file **** ---------");
			  System.err.println(ex.getMessage());
			  ex.printStackTrace();
			  response.setSuccess(false);
		  }
		  response.setSuccess(true);
		  response.setReturnVal(fileName+".xls");
		  return returnJson(map, response);
	}

	@RequestMapping(value = "/saveContact",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveContact(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		Contact contacts = (Contact) JSONParser.parseJSON(json,Contact.class);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		User user = userService.loggedUser(request);
		String name = user.getFirstName().substring(0,1)+user.getLastName().substring(0,1);
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		clientService.saveContact(response,contacts, loginId, createdDate,name);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/linkCandidate",  method = {RequestMethod.POST,RequestMethod.GET })
	public String linkCandidate(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		SearchFilter filter = ParamParser.parse(json);
		Map<String, Param> filterMap = filter.getParamMap();
		String contactId = filterMap.get("contactId").getStrValue();
		String candidateId = filterMap.get("candidateId").getStrValue();
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		Contact contact = clientService.linkCandidate(response,contactId,candidateId,loginId);
		response.setReturnVal(JsonParserUtility.toJSON(contact).toString());
		response.setSuccess(true);
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/unlinkCandidate/{id}", method = RequestMethod.GET)
	public String unlinkCandidate(@PathVariable("id") Integer id, Map<String, Object> map){
		Contact contact = clientService.unlinkCandidate(id);
		response.setReturnVal(JsonParserUtility.toJSON(contact).toString());
		response.setSuccess(true);
		return returnJson(map, response);
	}


}