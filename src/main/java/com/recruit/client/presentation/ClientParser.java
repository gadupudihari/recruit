package com.recruit.client.presentation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.client.service.Client;
import com.recruit.util.SqlTimestampConverter;

public class ClientParser {

	public static List<Client> parseJSON(String json){
        List<Client> clients = new ArrayList<Client>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	Client client = new Client();
            	if(rObj.has("id")){
            		client.setId(rObj.getInt("id"));     
            	}
            	if(rObj.has("created") && !rObj.getString("created").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		client.setCreated((Timestamp)c.fromString(rObj.getString("created")));
            	}
            	if(rObj.has("lastUpdated") && !rObj.getString("lastUpdated").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		client.setLastUpdated((Timestamp)c.fromString(rObj.getString("lastUpdated")));
            	}
            	if (rObj.has("comments")) {
            		client.setComments(rObj.getString("comments"));
				}
            	if (rObj.has("lastUpdatedUser")) {
            		client.setLastUpdatedUser(rObj.getString("lastUpdatedUser"));
				}
            	if (rObj.has("contactNumber")) {
            		client.setContactNumber(rObj.getString("contactNumber"));
				}
            	if (rObj.has("about")) {
            		client.setAbout(rObj.getString("about"));
				}
            	if (rObj.has("address")) {
            		client.setAddress(rObj.getString("address"));
				}
            	if (rObj.has("fax")) {
            		client.setFax(rObj.getString("fax"));
				}
            	if (rObj.has("industry")) {
            		client.setIndustry(rObj.getString("industry"));
				}
            	if (rObj.has("type")) {
            		client.setType(rObj.getString("type"));
				}
            	if (rObj.has("name")) {
            		client.setName(rObj.getString("name"));
				}
            	if (rObj.has("website")) {
            		client.setWebsite(rObj.getString("website"));
				}
            	if (rObj.has("email")) {
					client.setEmail(rObj.getString("email"));
				}
            	if (rObj.has("score")) {
					client.setScore(rObj.getInt("score"));
				}
            	if (rObj.has("city")) {
					client.setCity(rObj.getString("city"));
				}
            	if (rObj.has("state")) {
					client.setState(rObj.getString("state"));
				}
            	if (rObj.has("contractorId")) {
					client.setContractorId(rObj.getString("contractorId"));
				}
            	if (rObj.has("fullTimeId")) {
					client.setFullTimeId(rObj.getString("fullTimeId"));
				}
            	if (rObj.has("confidentiality")) {
            		client.setConfidentiality(rObj.getInt("confidentiality"));
				}
            	if (rObj.has("referenceId")) {
					client.setReferenceId(rObj.getString("referenceId"));
				}
            	if (rObj.has("helpCandidate")) {
            		client.setHelpCandidate(rObj.getString("helpCandidate"));
				}
            	if (rObj.has("helpCandidateComments")) {
            		client.setHelpCandidateComments(rObj.getString("helpCandidateComments"));
				}
            	if (rObj.has("recruiter")) {
            		client.setRecruiter(rObj.getInt("recruiter"));
				}
            	clients.add(client);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return clients;
	}

}
