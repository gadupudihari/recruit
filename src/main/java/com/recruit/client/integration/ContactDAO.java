package com.recruit.client.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.client.service.Contact;


/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class ContactDAO {

	private static final Log log = LogFactory.getLog(ContactDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Contact transientInstance) {
		log.debug("persisting Contact instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Contact instance) {
		log.debug("attaching dirty Contact instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Contact persistentInstance) {
		log.debug("deleting Contact instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Contact merge(Contact detachedInstance) {
		log.debug("merging Contact instance");
		try {
			Contact result = (Contact) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Contact findById(java.lang.Integer id) {
		log.debug("getting Contact instance with id: " + id);
		try {
			Contact instance = (Contact) sessionFactory.getCurrentSession()
					.get("com.recruit.client.service.Contact", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Contact> findByExample(Contact instance) {
		log.debug("finding Contact instance by example");
		try {
			List<Contact> results = (List<Contact>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.client.service.Contact").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Contact> findByCriteria(SearchFilter filter, String loginId) {
		try {
			String query = "from Contact t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("id") != null) {
				if (map.get("id").getStrValue().contains(",")) 
					query += " and t.id in(" +map.get("id").getStrValue()+ ")";
				else
					query += " and t.id like '" +map.get("id").getStrValue()+ "'";
			}
			if (map.get("client") != null) {
				query += " and t.client.id like '"+map.get("client").getStrValue()+"'";
			}
			if (map.get("type") != null) {
				query += " and t.client.type like '"+map.get("type").getStrValue()+"'";
			}
			if (map.get("candidateId") != null) {
				query += " and t.candidate.id like '"+map.get("candidateId").getStrValue()+"' and candidateDeleted = false";
			}
			if (map.get("contactType") != null) {
				query += " and t.type like '%"+map.get("contactType").getStrValue()+"%'";
			}
			if (map.get("score") != null) {
				query += " and score like '"+map.get("score").getStrValue()+"'";
			}
			if (map.get("gender") != null) {
				query += " and gender like '"+map.get("gender").getStrValue()+"'";
			}
			if (map.get("priority") != null) {
				query += " and priority like '"+map.get("priority").getStrValue()+"'";
			}
			if (map.get("role") != null) {
				query += " and role like '%"+map.get("role").getStrValue()+"%'";
			}
			if (map.get("firstName") != null) {
				query += " and firstName like '"+map.get("firstName").getStrValue()+"'";
			}
			if (map.get("lastName") != null) {
				query += " and lastName like '"+map.get("lastName").getStrValue()+"'";
			}
			if (map.get("module") != null) {
				String[] modules = map.get("module").getStrValue().split(",");
				String moduleQuery = "";
				for (int i = 0; i < modules.length; i++) {
					moduleQuery += "module like '%"+modules[i]+"%'";
					if (i+1 < modules.length)
						moduleQuery += " or ";
				}
				query += " and ("+moduleQuery+")";	
			}
			if (map.get("search") != null) {
				query +=" and Concat( CASE WHEN firstName is null THEN '' ELSE firstName END,' ', " +
							"CASE WHEN lastName is null THEN '' ELSE lastName END,' ', " +
							"CASE WHEN email is null THEN '' ELSE email END,' '," +
							"CASE WHEN jobTitle is null THEN '' ELSE jobTitle END,' '," +
							"CASE WHEN workPhone is null THEN '' ELSE workPhone END,' '," +
							"CASE WHEN internetLink is null THEN '' ELSE internetLink END,' '," +
							"CASE WHEN cellPhone is null THEN '' ELSE cellPhone END,' '," +
							"CASE WHEN module is null THEN '' ELSE module END,' '," +
							"CASE WHEN extension is null THEN '' ELSE extension END,' '," +
							"CASE WHEN extension is null THEN '' ELSE extension END,' '," +
							"CASE WHEN role is null THEN '' ELSE role END,' '," +
							"CASE WHEN gender is null THEN '' ELSE gender END,' '," +
							"CASE WHEN SMEComments is null THEN '' ELSE SMEComments END,' '," +
							"CASE WHEN placementComments is null THEN '' ELSE placementComments END,' '," +
							"CASE WHEN type is null THEN '' ELSE type END,' '," +
							"CASE WHEN smeType is null THEN '' ELSE smeType END,' '," +
							"CASE WHEN comments is null THEN '' ELSE comments END" +
						") like '%"+map.get("search").getStrValue()+"%'";
			}
			if (loginId != null) {
				query += " and ((select CASE WHEN excludeConfidential  is null THEN '' ELSE excludeConfidential  END from Authority where loginId='"+loginId+"') " +
						"not like concat('%',confidentiality,'%') or confidentiality is null)";
			}

			query += " order by FIELD(priority,priority,null) ASC";
			if (map.get("order") != null ) {
				query += ","+map.get("order").getStrValue();
			}

			System.out.println("--------------Contact : "+query);
			List<Contact> list = sessionFactory.getCurrentSession()
					.createQuery(query).setMaxResults(filter.getPageSize())
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public List getAllContacts() {
		try {
			String query = "select id,concat(firstName,' ',lastName),clientId,email,cellPhone,internetLink from recruit_contacts t order by firstName ";
			
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public boolean hasRequirements(Contact contact) {
		try {
			String query = " from Requirement t where 1=1 and id in (select distinct s.requirement.id from Sub_Vendor s where s.contactId ="+ contact.getId() +")" ;
			
			List list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			
			if (list.size() > 0)
				return true;
			return false;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Contact findContactName(Integer contactId) {
		try {
			String query = "select id,firstName,lastName from recruit_contacts t where 1=1 and id in ("+contactId+")";

			List<Contact> list = sessionFactory.getCurrentSession().createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Contact.class)).list(); 
			
			return list.get(0);
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Contact findByJobVendor(Integer requirementId ,  Integer vendorId) {
		try {
			String query = "from Contact t where 1=1 " +
					"and id in (select distinct contactId from Sub_Vendor s where s.requirement.id ="+ requirementId+" and vendorId="+vendorId+")";
			
			List<Contact> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			if (list.size() > 0) 
				return list.get(0);
			return null;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Contact> findByIds(String contactIds) {
		try {
			String query = "from Contact t where id in ("+ contactIds+")";
			
			List<Contact> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean checkCandidateLinked(Contact contact) {
		try {
			String query = "from Contact t where t.candidate.id like '"+contact.getCandidate().getId()+"' and candidateDeleted = false";
			if (contact.getId() != null)
				query += " and t.id !="+contact.getId();
			List<Contact> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			if (list.size() > 0)
				return true;
			return false;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}
	
}