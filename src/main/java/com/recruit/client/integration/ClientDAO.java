package com.recruit.client.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.client.service.Client;



/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class ClientDAO {

	private static final Log log = LogFactory.getLog(ClientDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Client transientInstance) {
		log.debug("persisting Client instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Client instance) {
		log.debug("attaching dirty Client instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Client persistentInstance) {
		log.debug("deleting Client instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Client merge(Client detachedInstance) {
		log.debug("merging Client instance");
		try {
			Client result = (Client) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Client findById(java.lang.Integer id) {
		log.debug("getting Client instance with id: " + id);
		try {
			Client instance = (Client) sessionFactory.getCurrentSession()
					.get("com.recruit.client.service.Client", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Client> findByExample(Client instance) {
		log.debug("finding Client instance by example");
		try {
			List<Client> results = (List<Client>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.client.service.Client").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Client> findByEmployeeId(int employeeId) {
		try {
			List<Client> results = (List<Client>) sessionFactory					
					.getCurrentSession()
					.createQuery(" from Client where employee.id = :_eId")
					.setParameter("_eId",employeeId)
					.list();
			return results;
		} catch (RuntimeException re) {
			log.error("find by employee Id failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Client> findByCriteria(SearchFilter filter, String loginId) {
		try {
			String query = "SELECT *,(select group_concat(contractorId) from recruit_client_candidate where clientId=t.id) contractorId, " +
					"(select group_concat(fullTimeId) from recruit_client_candidate where clientId=t.id) fullTimeId," +
					"(select group_concat(skillsetgroupId) from recruit_vendor_skillsetgroup where vendorId=t.id ) skillSetGroupIds  " +
					"FROM recruit_clients t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("id") != null) {
				if (map.get("id").getStrValue().contains(",")) 
					query += " and t.id in(" +map.get("id").getStrValue()+ ")";
				else
					query += " and t.id like '" +map.get("id").getStrValue()+ "'";
			}
			if (map.get("type") != null) {
				query += " and type like '"+map.get("type").getStrValue()+"'";
			}
			if (map.get("score") != null) {
				query += " and score like '"+map.get("score").getStrValue()+"'";
			}
			if (map.get("city") != null) {
				query += " and city like '%"+map.get("city").getStrValue()+"%'";
			}
			if (map.get("state") != null) {
				query += " and state like '%"+map.get("state").getStrValue()+"%'";
			}
			if (map.get("contractor") != null) {
				query += " and (FIND_IN_SET("+map.get("contractor").getStrValue()+", (select group_concat(contractorId) from recruit_client_candidate where clientId=t.id) )) ";
			}
			if (map.get("fullTime") != null) {
				query += " and (FIND_IN_SET("+map.get("fullTime").getStrValue()+", (select group_concat(fullTimeId) from recruit_client_candidate where clientId=t.id) )) ";
			}
			if (loginId != null) {
				query += " and (NOT FIND_IN_SET(confidentiality,(select if(excludeConfidential is null,'',excludeConfidential) from recruit_authority where loginId='"+loginId+"')) or confidentiality is null)";
			}
			if (map.get("vendor_candidateId") != null) {
				query += " and id in (select vendorId from recruit_sub_requirements where submissionStatus != 'Shortlisted' and deleted = false and candidateId = "+map.get("vendor_candidateId").getStrValue()+")";
			}
			if (map.get("client_candidateId") != null) {
				query += " and id in (select r.clientId FROM recruit_requirements r,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false and r.id=s.requirementId and s.candidateId= "+map.get("client_candidateId").getStrValue()+")";
			}

			if (map.get("relatedVendors") != null) {
				query += " and id in (select s.vendorId FROM recruit_requirements r,recruit_sub_vendors s,recruit_clients c where r.id=s.requirementId " +
						" and r.clientid = c.id and c.name!='n/a' and r.clientId= "+map.get("relatedVendors").getStrValue()+")";
			}
			if (map.get("relatedClients") != null) {
				query += " and id in (select r.clientId FROM recruit_requirements r,recruit_sub_vendors s,recruit_clients c where r.id=s.requirementId" +
						" and r.clientid = c.id and c.name!='n/a' and s.vendorId= "+map.get("relatedClients").getStrValue()+")";
			}

			if (map.get("search") != null) {
				query +=" and Concat( CASE WHEN name is null THEN '' ELSE name END,' ', " +
							"CASE WHEN contactNumber is null THEN '' ELSE contactNumber END,' ', " +
							"CASE WHEN fax is null THEN '' ELSE fax END,' '," +
							"CASE WHEN website is null THEN '' ELSE website END,' '," +
							"CASE WHEN email is null THEN '' ELSE email END,' '," +
							"CASE WHEN type is null THEN '' ELSE type END,' '," +
							"CASE WHEN industry is null THEN '' ELSE industry END,' '," +
							"CASE WHEN about is null THEN '' ELSE about END,' '," +
							"CASE WHEN address is null THEN '' ELSE address END,' '," +
							"CASE WHEN score is null THEN '' ELSE score END,' '," +
							"CASE WHEN city is null THEN '' ELSE city END,' '," +
							"CASE WHEN state is null THEN '' ELSE state END,' '," +
							"CASE WHEN created is null THEN '' ELSE created END,' '," +
							"CASE WHEN lastUpdated is null THEN '' ELSE lastUpdated END,' '," +
							"CASE WHEN lastUpdatedUser is null THEN '' ELSE lastUpdatedUser END,' '," +
							"CASE WHEN comments is null THEN '' ELSE comments END" +
						") like '%"+map.get("search").getStrValue()+"%'";
			}
			query += " order by score";
			
			List<Client> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Client.class))
					.setMaxResults(filter.getPageSize())
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	//delete from sub tables contractors full time and skill set group sub tables
	public void delete(Integer id) {
		String query = "delete from recruit_client_candidate where clientId = "+id;
		sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();

		query = "delete from recruit_vendor_skillsetgroup where vendorId = "+id;
		sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();

		query = "delete from recruit_clients where id = "+id;
		sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
		
	}

	@SuppressWarnings("rawtypes")
	public boolean hasInterviews(Client client) {
		try {
			String query = "from Interview t where 1=1 ";
			
			if (client.getType().equalsIgnoreCase("vendor")) 
				query += " and t.vendor.id like '"+client.getId()+"'";
			else if (client.getType().equalsIgnoreCase("Client")) 
				query += " and t.client.id like '"+client.getId()+"'";
			
			System.out.println("Interview : "+query);
			List list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			
			if (list.size() > 0)
				return true;
			return false;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public boolean hasRequirements(Client client) {
		try {
			String query = "";
			if (client.getType().equalsIgnoreCase("Client")) {
				query = "from Requirement t where 1=1 and t.client.id like '"+client.getId()+"'";	
			}else if (client.getType().equalsIgnoreCase("vendor")) {
				query = " from Requirement t where 1=1 and id in (select distinct s.requirement.id from Sub_Vendor s where s.vendorId ="+ client.getId() +")" ;
			}
			
			List list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			
			if (list.size() > 0)
				return true;
			return false;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Client> getAllClients(SearchFilter filter) {
		try {
			String query = "SELECT *,(select group_concat(contractorId) from recruit_client_candidate where clientId=t.id) contractorId, " +
					"(select group_concat(fullTimeId) from recruit_client_candidate where clientId=t.id) fullTimeId FROM recruit_clients t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("id") != null) {
				if (map.get("id").getStrValue().contains(",")) 
					query += " and t.id in(" +map.get("id").getStrValue()+ ")";
				else
					query += " and t.id like '" +map.get("id").getStrValue()+ "'";
			}
			if (map.get("type") != null) {
				query += " and type like '"+map.get("type").getStrValue()+"'";
			}
			if (map.get("score") != null) {
				query += " and score like '"+map.get("score").getStrValue()+"'";
			}

			if (map.get("search") != null) {
				query +=" and Concat( CASE WHEN name is null THEN '' ELSE name END,' ', " +
							"CASE WHEN contactNumber is null THEN '' ELSE contactNumber END,' ', " +
							"CASE WHEN fax is null THEN '' ELSE fax END,' '," +
							"CASE WHEN website is null THEN '' ELSE website END,' '," +
							"CASE WHEN email is null THEN '' ELSE email END,' '," +
							"CASE WHEN industry is null THEN '' ELSE industry END,' '," +
							"CASE WHEN about is null THEN '' ELSE about END,' '," +
							"CASE WHEN address is null THEN '' ELSE address END,' '," +
							"CASE WHEN score is null THEN '' ELSE score END,' '," +
							"CASE WHEN city is null THEN '' ELSE city END,' '," +
							"CASE WHEN state is null THEN '' ELSE state END,' '," +
							"CASE WHEN created is null THEN '' ELSE created END,' '," +
							"CASE WHEN lastUpdated is null THEN '' ELSE lastUpdated END,' '," +
							"CASE WHEN lastUpdatedUser is null THEN '' ELSE lastUpdatedUser END,' '," +
							"CASE WHEN comments is null THEN '' ELSE comments END" +
						") like '%"+map.get("search").getStrValue()+"%'";
			}
			query += " order by score";
			
			List<Client> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Client.class))
					.setMaxResults(filter.getPageSize())
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Client findVendorName(Integer vendorId) {
		try {
			String query = "select id,name from recruit_clients t where 1=1 and id in ("+vendorId+")";

			List<Client> list = sessionFactory.getCurrentSession().createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Client.class)).list(); 
			
			return list.get(0);
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Client findByJobId(Integer requirementId) {
		try {
			String query = "SELECT * FROM recruit_clients t where 1=1 and id = (select clientId from recruit_requirements where id = "+requirementId+")";
			
			List<Client> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Client.class))
					.list();
			if (list.size() > 0) {
				return list.get(0);
			}
			return null;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean hasProjects(Client client) {
		try {
			String query = "SELECT * FROM project t where 1=1 ";
			if (client.getType().equalsIgnoreCase("vendor")) 
				query += " and t.r_vendorId like '"+client.getId()+"'";
			else if (client.getType().equalsIgnoreCase("Client")) 
				query += " and t.clientId like '"+client.getId()+"'";

			List<Client> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			if (list.size() > 0)
				return true;
			return false;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}
	
	
}