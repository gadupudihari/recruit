package com.recruit.client.service;

import com.recruit.client.service.Contact;

public class Contact_RoleSet implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer roleSetId;

	private Contact contact;

	public Contact_RoleSet() {}
	
	public Contact_RoleSet(Integer id, Integer roleSetId , Contact contact) {
		this.id = id;
		this.contact = contact;
		this.roleSetId = roleSetId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoleSetId() {
		return roleSetId;
	}

	public void setRoleSetId(Integer roleSetId) {
		this.roleSetId = roleSetId;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id  + ", roleSetId=" + roleSetId +"]";
	}


}