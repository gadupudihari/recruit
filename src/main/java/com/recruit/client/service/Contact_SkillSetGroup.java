package com.recruit.client.service;

public class Contact_SkillSetGroup implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer skillSetGroupId;

	private Contact contact;

	public Contact_SkillSetGroup() {}
	
	public Contact_SkillSetGroup(Integer id, Integer skillSetGroupId , Contact contact) {
		this.id = id;
		this.contact = contact;
		this.skillSetGroupId = skillSetGroupId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getSkillSetGroupId() {
		return skillSetGroupId;
	}

	public void setSkillSetGroupId(Integer skillSetGroupId) {
		this.skillSetGroupId = skillSetGroupId;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id  + ", skillSetGroupId=" + skillSetGroupId +"]";
	}


}