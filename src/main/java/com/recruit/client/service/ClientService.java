package com.recruit.client.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.recruit.base.presentation.Response;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.integration.CandidateDAO;
import com.recruit.candidate.integration.MarketingCandidatesDAO;
import com.recruit.candidate.service.Candidate;
import com.recruit.candidate.service.Candidate_SkillSetGroup;
import com.recruit.client.integration.ClientDAO;
import com.recruit.client.integration.ContactDAO;
import com.recruit.client.integration.Contact_RoleSetDAO;
import com.recruit.client.integration.Contact_SkillSetGroupDAO;
import com.recruit.client.integration.Vendor_SkillSetGroupDAO;
import com.recruit.requirement.integration.RequirementsDAO;
import com.recruit.requirement.service.Requirement;
import com.recruit.util.UtilityHelper;

@SuppressWarnings("unused")
@Service
public class ClientService {

	@Autowired
	ClientDAO clientDAO;
	
	@Autowired
	ContactDAO contactDAO;
	
	@Autowired
	CandidateDAO candidateDAO;

	@Autowired 
	RequirementsDAO requirementsDAO;

	@Autowired
	Contact_RoleSetDAO contact_RoleSetDAO;

	@Autowired
	Vendor_SkillSetGroupDAO vendor_SkillSetGroupDAO;

	@Autowired
	Contact_SkillSetGroupDAO contact_SkillSetGroupDAO;

	@Autowired
	MarketingCandidatesDAO marketingCandidatesDAO;

	@Transactional
	public List<Client> getClients(SearchFilter filter, String loginId) {
		return clientDAO.findByCriteria(filter,loginId);
	}

	@Transactional
	public Response saveClients(List<Client> clients, String loginId,Timestamp createdDate, Response response) {
		for (Client client  : clients) {
			client.setLastUpdated(createdDate);
			client.setLastUpdatedUser(loginId);
			if ( client.getContractorId() != null && !client.getContractorId().equals("") ) {
				String contractorIds[] = client.getContractorId().split(",");
				Set<Candidate> contractors = new HashSet<Candidate>(0);
				for (int i = 0; i < contractorIds.length; i++) {
					Candidate contractor = candidateDAO.findById(Integer.parseInt(contractorIds[i]));
					contractors.add(contractor);
				}
				client.setContractors(contractors);
			}
			if ( client.getFullTimeId() != null && !client.getFullTimeId().equals("") ) {
				String fullTimeIds[] = client.getFullTimeId().split(",");
				Set<Candidate> fullTimes = new HashSet<Candidate>(0);
				for (int i = 0; i < fullTimeIds.length; i++) {
					Candidate fullTime = candidateDAO.findById(Integer.parseInt(fullTimeIds[i]));
					fullTimes.add(fullTime);
				}
				client.setFullTime(fullTimes);
			}
			if (client.getId() != null) {
				Client oldClient = clientDAO.findById(client.getId());
				if (oldClient.getType() != null && client.getType() != null && !oldClient.getType().equalsIgnoreCase(client.getType())) {
					//if old client type and new client type are not equal 
					if (clientDAO.hasRequirements(oldClient)) {
						response.setSuccess(Boolean.FALSE);
						response.setErrorMessage(client.getName()+": "+oldClient.getType()+" has Job openings cannot modify type.");
						return response;
					}
					if (clientDAO.hasInterviews(oldClient)) {
						response.setSuccess(Boolean.FALSE);
						response.setErrorMessage("'"+client.getName()+"' "+oldClient.getType()+" has Interviews cannot modify type.");
						return response;
					}
					
				}
			}

			clientDAO.merge(client);
		}
		response.setSuccess(true);
		return response;
	}

	@Transactional
	public void deleteClient(Response response,Integer id) {
		Client client = clientDAO.findById(id);
		if(client == null){
			response.setSuccess(Boolean.FALSE);
			response.setErrorMessage("Specified Client is not present.");
		}else if (client.getContacts() != null && client.getContacts().size() > 0) {
			response.setSuccess(false);
			response.setErrorMessage("There are Contacts associated to this "+client.getType()+".");
		}else if (clientDAO.hasInterviews(client)) {
			response.setSuccess(false);
			response.setErrorMessage("There are Interviews associated to this "+client.getType()+".");
		}else if (clientDAO.hasProjects(client)) {
			response.setSuccess(false);
			response.setErrorMessage("There are Projects associated to this "+client.getType()+".");
		}else if (client.getType() != null && client.getType().equalsIgnoreCase("vendor")) {
			List<Requirement> requirements = requirementsDAO.findByVendorId(id);
			if (requirements != null && requirements.size() > 0) {
				response.setSuccess(false);
				response.setErrorMessage("There are Requirements associated to this Vendor.");
			}else if (marketingCandidatesDAO.hasMarketted(client)) {
				response.setSuccess(false);
				response.setErrorMessage("This vendor is marketted, Cannot delete.");
			}else{
				// this will deletes contrctors and full time candidates from client_candidate table and also deletes client  
				clientDAO.delete(id);
				response.setSuccess(Boolean.TRUE);
			}
		}else{
			if (clientDAO.hasRequirements(client)) {
				response.setSuccess(false);
				response.setErrorMessage("There are Requirements associated to this Client.");
			}else {
				// this will deletes contrctors and full time candidates from client_candidate table and also deletes client  
				clientDAO.delete(id);
				response.setSuccess(Boolean.TRUE);
			}
		}
	}

	@Transactional
	public Response saveContacts(Response response, List<Contact> contacts, String loginId, Timestamp createdDate) {
		for (Contact contact : contacts) {
			contact.setLastUpdated(createdDate);
			contact.setLastUpdatedUser(loginId);
			
			if (contact.getId() != null) {
				Contact oldContact = contactDAO.findById(contact.getId());
				if (oldContact.getClient() != null && contact.getClient() != null && !oldContact.getClient().getId().equals(contact.getClient().getId()) ) {
					//if old contact-client  and new contact-client ids are not same 
					if (contactDAO.hasRequirements(oldContact)) {
						response.setSuccess(Boolean.FALSE);
						response.setErrorMessage(contact.getFirstName()+" "+contact.getLastName()+ " has Job openings cannot modify Client/Vendor.");
						return response;
					}
				}
			}
			contactDAO.merge(contact);
			response.setSuccess(true);
		}
		return response;
	}

	@Transactional
	public void deleteContact(Response response, Integer id) {
		Contact contact = contactDAO.findById(id);
		if (contact.getInterviews() != null && contact.getInterviews().size() > 0) {
			response.setSuccess(false);
			response.setErrorMessage("There are Interviews associated to this Contact.");
		}else if (contactDAO.hasRequirements(contact)) {
			response.setSuccess(false);
			response.setErrorMessage("There are Requirements associated to this Contact.");
		}else{
			contact_RoleSetDAO.deleteByContact(contact.getId());
			contact_SkillSetGroupDAO.deleteByContact(contact.getId());
			contactDAO.delete(contact);	
			response.setSuccess(true);
		}
		
	}

	@Transactional
	public List<Contact> getContacts(SearchFilter filter, String loginId) {
		return contactDAO.findByCriteria(filter,loginId);
	}

	@Transactional
	public Response saveClient(Client client, Response response) {
		if ( client.getContractorId() != null && !client.getContractorId().equals("") ) {
			String contractorIds[] = client.getContractorId().split(",");
			Set<Candidate> contractors = new HashSet<Candidate>(0);
			for (int i = 0; i < contractorIds.length; i++) {
				Candidate contractor = candidateDAO.findById(Integer.parseInt(contractorIds[i]));
				contractors.add(contractor);
			}
			client.setContractors(contractors);
		}
		if ( client.getFullTimeId() != null && !client.getFullTimeId().equals("") ) {
			String fullTimeIds[] = client.getFullTimeId().split(",");
			Set<Candidate> fullTimes = new HashSet<Candidate>(0);
			for (int i = 0; i < fullTimeIds.length; i++) {
				Candidate fullTime = candidateDAO.findById(Integer.parseInt(fullTimeIds[i]));
				fullTimes.add(fullTime);
			}
			client.setFullTime(fullTimes);
		}

		if (client.getId() == null){
			clientDAO.attachDirty(client);

			//delete and insert Skill set group
			if (client.getVendor_SkillSetGroups() != null && client.getVendor_SkillSetGroups().size() > 0) {
				for (Vendor_SkillSetGroup skillSet : client.getVendor_SkillSetGroups()) {
					skillSet.setVendor(client);
					vendor_SkillSetGroupDAO.attachDirty(skillSet);
				}
			}
		}else{
			Client oldClient = clientDAO.findById(client.getId());
			if (oldClient.getType() != null && client.getType() != null && !oldClient.getType().equalsIgnoreCase(client.getType())) {
				//if old client type and new client type are not equal 
				if (clientDAO.hasRequirements(oldClient)) {
					response.setSuccess(Boolean.FALSE);
					response.setErrorMessage("Job openings associated to this "+oldClient.getType()+". Cannot modify type.");
					return response;
				}
				if (clientDAO.hasInterviews(oldClient)) {
					response.setSuccess(Boolean.FALSE);
					response.setErrorMessage("Interviews associated to this "+oldClient.getType()+". Cannot modify type.");
					return response;
				}
			}
			client.setCreated(oldClient.getCreated());
			
			//delete and insert Skill set group
			vendor_SkillSetGroupDAO.deleteByVendor(client.getId());
			if (client.getVendor_SkillSetGroups() != null && client.getVendor_SkillSetGroups().size() > 0) {
				for (Vendor_SkillSetGroup skillSet : client.getVendor_SkillSetGroups()) {
					skillSet.setVendor(client);
					vendor_SkillSetGroupDAO.attachDirty(skillSet);
				}
			}
			
			clientDAO.merge(client);
		}
		response.setReturnVal(client);
		response.setSuccess(true);
		return response;
	}


	@Transactional
	public Client findById(Integer vendorId) {
		return clientDAO.findById(vendorId);
	}
	
	@Transactional
	public Contact findContactById(Integer contactId) {
		return contactDAO.findById(contactId);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getAllContacts() {
		return contactDAO.getAllContacts();
	}

	@Transactional
	public List<Client> getAllClients(SearchFilter filter) {
		return clientDAO.getAllClients(filter);
	}

	@Transactional
	public Client findVendorName(Integer vendorId) {
		return clientDAO.findVendorName(vendorId);
	}

	@Transactional
	public Contact findContactName(Integer contactId) {
		return contactDAO.findContactName(contactId);
	}

	@Transactional
	public Response saveContact(Response response, Contact contact, String loginId, Timestamp createdDate, String name) {
		contact.setLastUpdated(createdDate);
		contact.setLastUpdatedUser(loginId);
		
		if (contact.getCandidate() != null && contact.getCandidate().getId() != null ) {
			if (contactDAO.checkCandidateLinked(contact)) {
				response.setSuccess(Boolean.FALSE);
				response.setErrorMessage("Candidate already linked.");
				return response;
			}
		}
		
		if (contact.getId() == null){
			contact.setCreated(createdDate);
			contactDAO.attachDirty(contact);
			
			//insert role set into roleset table
			if (contact.getContact_RoleSets() != null && contact.getContact_RoleSets().size() > 0) {
				for (Contact_RoleSet roleSet : contact.getContact_RoleSets()) {
					roleSet.setContact(contact);
					contact_RoleSetDAO.attachDirty(roleSet);
				}
			}
			
			//insert skill set into skill set group table
			if (contact.getContact_SkillSetGroups() != null && contact.getContact_SkillSetGroups().size() > 0) {
				for (Contact_SkillSetGroup skillSet : contact.getContact_SkillSetGroups()) {
					skillSet.setContact(contact);
					contact_SkillSetGroupDAO.attachDirty(skillSet);
				}
			}
		}else{
			Contact oldContact = contactDAO.findById(contact.getId());
			if (oldContact.getClient() != null && contact.getClient() != null && !oldContact.getClient().getId().equals(contact.getClient().getId()) ) {
				//if old contact-client  and new contact-client ids are not same 
				if (contactDAO.hasRequirements(oldContact)) {
					response.setSuccess(Boolean.FALSE);
					response.setErrorMessage(contact.getFirstName()+" "+contact.getLastName()+ " has Job openings cannot modify Client/Vendor.");
					return response;
				}
			}
			
			//delete and insert role set into roleset table
			contact_RoleSetDAO.deleteByContact(contact.getId());
			if (contact.getContact_RoleSets() != null && contact.getContact_RoleSets().size() > 0) {
				for (Contact_RoleSet roleSet : contact.getContact_RoleSets()) {
					roleSet.setContact(contact);
					contact_RoleSetDAO.attachDirty(roleSet);
				}
			}
			//delete and insert skill set into skill set group table
			contact_SkillSetGroupDAO.deleteByContact(contact.getId());
			if (contact.getContact_SkillSetGroups() != null && contact.getContact_SkillSetGroups().size() > 0) {
				for (Contact_SkillSetGroup skillSet : contact.getContact_SkillSetGroups()) {
					skillSet.setContact(contact);
					contact_SkillSetGroupDAO.attachDirty(skillSet);
				}
			}
			
			contact.setCreated(oldContact.getCreated());
			contactDAO.merge(contact);
		}
		response.setReturnVal(contact);
		response.setSuccess(true);
		return response;
		
	}

	@Transactional
	public Contact findContactByJobVendor(Integer requirementId ,  Integer vendorId) {
		return contactDAO.findByJobVendor(requirementId, vendorId);
	}

	@Transactional
	public Contact linkCandidate(Response response, String contactId,String candidateId, String loginId) {
		Contact contact = contactDAO.findById(Integer.parseInt(contactId));
		Candidate candidate = candidateDAO.findById(Integer.parseInt(candidateId));
		contact.setCandidate(candidate);
		contact.setCandidateDeleted(false);
		contact.setLastUpdated(UtilityHelper.getDate());
		contact.setLastUpdatedUser(loginId);
		contactDAO.attachDirty(contact);
		return contact;
	}

	@Transactional
	public Contact unlinkCandidate(Integer id) {
		Contact contact = contactDAO.findById(id);
		contact.setCandidateDeleted(true);
		if (contact.getType() != null && contact.getType().length() > 0) {
			String[] type = contact.getType().split(",");
			if (Arrays.asList(type).contains("SME")) 
				type = (String[]) ArrayUtils.removeElement(type, "SME");
			if (Arrays.asList(type).contains("Contact")) 
				type = (String[]) ArrayUtils.removeElement(type, "Contact");
			
			String newType = "";
			for (int i = 0; i < type.length; i++) {
				newType += type[i]+',';
			}
			if (newType.length() > 0)
				newType = newType.substring(0,newType.length()-1);
			contact.setType(newType);
		}
		
		contactDAO.attachDirty(contact);
		return contact;
	}

}
