package com.recruit.client.service;

public class Vendor_SkillSetGroup implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer skillSetGroupId;

	private Client vendor;

	public Vendor_SkillSetGroup() {}
	
	public Vendor_SkillSetGroup(Integer id, Integer skillSetGroupId , Client vendor) {
		this.id = id;
		this.vendor = vendor;
		this.skillSetGroupId = skillSetGroupId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getSkillSetGroupId() {
		return skillSetGroupId;
	}

	public void setSkillSetGroupId(Integer skillSetGroupId) {
		this.skillSetGroupId = skillSetGroupId;
	}

	public Client getVendor() {
		return vendor;
	}

	public void setVendor(Client vendor) {
		this.vendor = vendor;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id  + ", skillSetGroupId=" + skillSetGroupId +"]";
	}


}