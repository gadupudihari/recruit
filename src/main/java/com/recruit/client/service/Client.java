package com.recruit.client.service;


import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.recruit.candidate.service.Candidate;

public class Client implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;

	private Integer id;
	private String name;
	private String contactNumber;
	private String fax;
	private String website;
	private String email;
	private String industry;
	private String about;
	private String type;
	private String address;
	private String comments;
	private String lastUpdatedUser;
	private Timestamp created;
	private Timestamp lastUpdated;
	private Integer score;
	private String city;
	private String state;
	private Integer confidentiality;
	private String referenceId;
	private String helpCandidate;
	private String helpCandidateComments;
	
	private Integer recruiter;
	private Set<Candidate> contractors = new HashSet<Candidate>(0);
	private Set<Candidate> fullTime = new HashSet<Candidate>(0);
	private Set<Contact> contacts = new HashSet<Contact>(0);
	private Set<Vendor_SkillSetGroup> vendor_SkillSetGroups = new HashSet<Vendor_SkillSetGroup>(0);

	private String contractorId;
	private String fullTimeId;
	private String skillSetGroupIds;
	
	public Client() {}
	
	public Client(int id) {
		this.id = id;
	}

	public Client(Integer id, String name, String contactNumber, String fax, String website, String email, String industry, String about, Integer score, String type, 
			String address, String comments, String lastUpdatedUser, Timestamp created, Timestamp lastUpdated, String city, String state, Integer confidentiality,
			String referenceId,String helpCandidate, String helpCandidateComments , Integer recruiter, String contractorId,String fullTimeId,String skillSetGroupIds,
			Set<Contact> contacts, Set<Candidate> contractors, Set<Candidate> fullTime, Set<Vendor_SkillSetGroup> vendor_SkillSetGroups) {
		this.id = id;
		this.name = name;
		this.contactNumber = contactNumber;
		this.fax = fax;
		this.website = website;
		this.email = email;
		this.industry = industry;
		this.about = about;
		this.type = type;
		this.address = address;
		this.comments = comments;
		this.lastUpdatedUser = lastUpdatedUser;
		this.created = created;
		this.lastUpdated = lastUpdated;
		this.contacts = contacts;
		this.score = score;
		this.city = city;
		this.state = state;
		this.confidentiality = confidentiality;
		this.referenceId = referenceId;
		this.helpCandidate = helpCandidate;
		this.helpCandidateComments = helpCandidateComments;
		
		this.recruiter = recruiter;
		this.contractors = contractors;
		this.fullTime = fullTime;
		this.vendor_SkillSetGroups = vendor_SkillSetGroups;
		this.contractorId = contractorId;
		this.fullTimeId = fullTimeId;
		this.skillSetGroupIds = skillSetGroupIds;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Set<Candidate> getContractors() {
		return contractors;
	}

	public void setContractors(Set<Candidate> contractors) {
		this.contractors = contractors;
	}

	public Set<Candidate> getFullTime() {
		return fullTime;
	}

	public void setFullTime(Set<Candidate> fullTime) {
		this.fullTime = fullTime;
	}

	public String getContractorId() {
		return contractorId;
	}

	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}

	public String getFullTimeId() {
		return fullTimeId;
	}

	public void setFullTimeId(String fullTimeId) {
		this.fullTimeId = fullTimeId;
	}

	public Integer getConfidentiality() {
		return confidentiality;
	}

	public void setConfidentiality(Integer confidentiality) {
		this.confidentiality = confidentiality;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getHelpCandidate() {
		return helpCandidate;
	}

	public void setHelpCandidate(String helpCandidate) {
		this.helpCandidate = helpCandidate;
	}

	public String getHelpCandidateComments() {
		return helpCandidateComments;
	}

	public void setHelpCandidateComments(String helpCandidateComments) {
		this.helpCandidateComments = helpCandidateComments;
	}

	public Integer getRecruiter() {
		return recruiter;
	}

	public void setRecruiter(Integer recruiter) {
		this.recruiter = recruiter;
	}

	public Set<Vendor_SkillSetGroup> getVendor_SkillSetGroups() {
		return vendor_SkillSetGroups;
	}

	public void setVendor_SkillSetGroups(
			Set<Vendor_SkillSetGroup> vendor_SkillSetGroups) {
		this.vendor_SkillSetGroups = vendor_SkillSetGroups;
	}

	public String getSkillSetGroupIds() {
		return skillSetGroupIds;
	}

	public void setSkillSetGroupIds(String skillSetGroupIds) {
		this.skillSetGroupIds = skillSetGroupIds;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", contactNumber=" + contactNumber + ", fax=" + fax + ", website=" + website
				+ ", email=" + email + ", industry=" + industry + ", about=" + about + ", type=" + type + ", address=" + address
				+ ", comments=" + comments + ", lastUpdatedUser=" + lastUpdatedUser + ", created=" + created + ", lastUpdated=" + lastUpdated 
				+ ", score=" + score + ", city=" + city + ", state=" + state + ", confidentiality=" + confidentiality + ", referenceId=" + referenceId 
				+ ", helpCandidate=" + helpCandidate + ", helpCandidateComments=" + helpCandidateComments + ", contractors=" + contractors
				+ ", fullTime=" + fullTime + ", contacts=" + contacts + ", contractorId=" + contractorId + ", fullTimeId=" + fullTimeId
				+ ", skillSetGroupIds=" + skillSetGroupIds 
				+ "]";
	}

	
}