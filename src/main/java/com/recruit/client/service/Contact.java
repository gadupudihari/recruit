package com.recruit.client.service;


import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.recruit.candidate.service.Candidate;
import com.recruit.interview.service.Interview;

public class Contact implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;

	private Integer id;
	private String firstName;
	private String lastName;
	private String email;
	private String jobTitle;
	private String workPhone;
	private String internetLink;
	private String cellPhone;
	private String comments;
	private String lastUpdatedUser;
	private Timestamp created;
	private Timestamp lastUpdated;
	private Integer score;
	private String extension;
	private Integer confidentiality;
	private String type;
	private String role;
	private String gender;
	private String module;
	private Integer priority;
	private String SMEComments;
	private String placementComments;
	private Boolean candidateDeleted;
	private String smeType;
	private String source;
	private String referral;
	private String resumeLink;
	
	private Client client;
	private Candidate candidate;
	private Set<Interview> interviews = new HashSet<Interview>(0);
	private Set<Contact_RoleSet> contact_RoleSets = new HashSet<Contact_RoleSet>(0);
	private Set<Contact_SkillSetGroup> contact_SkillSetGroups = new HashSet<Contact_SkillSetGroup>(0);
	
	public Contact() {}
	
	public Contact(int id) {
		this.id = id;
	}

	public Contact(Integer id, String firstName, String lastName, String email, String jobTitle, String workPhone, String internetLink,Integer score,String extension,
			String cellPhone, String comments, String lastUpdatedUser, Timestamp created, Timestamp lastUpdated, Client client,Integer confidentiality,String type,
			String role, String gender,String module, Integer priority, String SMEComments, String placementComments, Boolean candidateDeleted, String smeType,
			String source, String referral, String resumeLink,
			Set<Contact_RoleSet> contact_RoleSets, Set<Interview> interviews,Set<Contact_SkillSetGroup> contact_SkillSetGroups, Candidate candidate) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.jobTitle = jobTitle;
		this.workPhone = workPhone;
		this.internetLink = internetLink;
		this.cellPhone = cellPhone;
		this.comments = comments;
		this.lastUpdatedUser = lastUpdatedUser;
		this.created = created;
		this.lastUpdated = lastUpdated;
		this.client = client;
		this.score = score;
		this.extension = extension;
		this.confidentiality = confidentiality;
		this.type = type;
		this.role = role;
		this.gender = gender;
		this.module = module;
		this.priority = priority;
		this.SMEComments = SMEComments;
		this.placementComments = placementComments;
		this.candidateDeleted = candidateDeleted;
		this.smeType = smeType;
		this.source = source;
		this.referral = referral;
		this.resumeLink = resumeLink;
		
		this.candidate = candidate;
		this.interviews = interviews;
		this.contact_RoleSets = contact_RoleSets;
		this.contact_SkillSetGroups = contact_SkillSetGroups;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	public String getInternetLink() {
		return internetLink;
	}

	public void setInternetLink(String internetLink) {
		this.internetLink = internetLink;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Set<Interview> getInterviews() {
		return interviews;
	}

	public void setInterviews(Set<Interview> interviews) {
		this.interviews = interviews;
	}

	public Integer getConfidentiality() {
		return confidentiality;
	}

	public void setConfidentiality(Integer confidentiality) {
		this.confidentiality = confidentiality;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public String getSMEComments() {
		return SMEComments;
	}

	public void setSMEComments(String sMEComments) {
		SMEComments = sMEComments;
	}

	public String getPlacementComments() {
		return placementComments;
	}

	public void setPlacementComments(String placementComments) {
		this.placementComments = placementComments;
	}

	public Boolean getCandidateDeleted() {
		return candidateDeleted;
	}

	public void setCandidateDeleted(Boolean candidateDeleted) {
		this.candidateDeleted = candidateDeleted;
	}

	public Set<Contact_RoleSet> getContact_RoleSets() {
		return contact_RoleSets;
	}

	public void setContact_RoleSets(Set<Contact_RoleSet> contact_RoleSets) {
		this.contact_RoleSets = contact_RoleSets;
	}

	public String getSmeType() {
		return smeType;
	}

	public void setSmeType(String smeType) {
		this.smeType = smeType;
	}

	public Set<Contact_SkillSetGroup> getContact_SkillSetGroups() {
		return contact_SkillSetGroups;
	}

	public void setContact_SkillSetGroups(Set<Contact_SkillSetGroup> contact_SkillSetGroups) {
		this.contact_SkillSetGroups = contact_SkillSetGroups;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

	public String getResumeLink() {
		return resumeLink;
	}

	public void setResumeLink(String resumeLink) {
		this.resumeLink = resumeLink;
	}

	@Override
	public String toString() {
		return "Candidate [id=" + id + ", firstName="+firstName+ ", lastName="+lastName+ ", email="+email + ", jobTitle="+jobTitle 
				+ ", workPhone="+workPhone+ ", internetLink="+internetLink+ ", cellPhone="+cellPhone
				+ ", comments="+comments+", created="+created+ ", lastUpdated="+lastUpdated+", lastUpdatedUser="+lastUpdatedUser+" score="+ score 
				+ ", extension="+extension+ ", confidentiality="+confidentiality + ", type="+ type + ", role="+ role + ", gender="+ gender 
				+ ", module="+ module + ", priority="+ priority + ", SMEComments="+ SMEComments + ", placementComments="+ placementComments
				+ ", candidateDeleted="+ candidateDeleted + ", smeType="+ smeType + ", source="+ source + ", referral="+ referral + ", resumeLink="+ resumeLink 
				+ "]";
	}
}