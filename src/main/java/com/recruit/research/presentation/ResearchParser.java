package com.recruit.research.presentation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.requirement.service.Requirement;
import com.recruit.research.service.Research;
import com.recruit.util.SqlTimestampConverter;

public class ResearchParser {

	public static List<Research> parseJSON(String json){
        List<Research> researchList = new ArrayList<Research>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	Research research = new Research();
            	if(rObj.has("id")){
            		research.setId(rObj.getInt("id"));     
            	}
            	if(rObj.has("created") && !rObj.getString("created").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		research.setCreated((Timestamp)c.fromString(rObj.getString("created")));
            	}
            	if(rObj.has("lastUpdated") && !rObj.getString("lastUpdated").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		research.setLastUpdated((Timestamp)c.fromString(rObj.getString("lastUpdated")));
            	}
            	if(rObj.has("date") && !rObj.getString("date").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		research.setDate((Timestamp)c.fromString(rObj.getString("date")));
            	}
            	if (rObj.has("task")) {
            		research.setTask(rObj.getString("task"));
				}
            	if (rObj.has("status")) {
            		research.setStatus(rObj.getString("status"));
				}
            	if (rObj.has("lastUpdatedUser")) {
            		research.setLastUpdatedUser(rObj.getString("lastUpdatedUser"));
				}
            	if (rObj.has("comments")) {
					research.setComments(rObj.getString("comments"));
				}
            	if (rObj.has("potentialCandidates")) {
					research.setPotentialCandidates(rObj.getString("potentialCandidates"));
				}
            	if (rObj.has("serialNo")) {
					research.setSerialNo(rObj.getInt("serialNo"));
				}
            	if (rObj.has("requirementId")) {
					Requirement requirement = new Requirement();
					requirement.setId(rObj.getInt("requirementId"));
					research.setRequirement(requirement);
				}else
					research.setRequirement(null);

            	researchList.add(research);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return researchList;
	}

}
