package com.recruit.research.service;


import java.sql.Timestamp;

import com.recruit.requirement.service.Requirement;

public class Research implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;

	private Integer id;
	private String task;
	private String status;
	private String lastUpdatedUser;
	private String comments;
	private String potentialCandidates;
	private Timestamp date;
	private Timestamp created;
	private Timestamp lastUpdated;
	private Integer serialNo;

	private Requirement requirement;
	
	public Research() {}
	
	public Research(int id) {
		this.id = id;
	}


	public Research(Integer id, String task, String status, String lastUpdatedUser, String comments, String potentialCandidates,
			Timestamp date, Timestamp created, Timestamp lastUpdated,Integer serialNo, Requirement requirement) {
		this.id = id;
		this.task = task;
		this.status = status;
		this.lastUpdatedUser = lastUpdatedUser;
		this.comments = comments;
		this.date = date;
		this.created = created;
		this.lastUpdated = lastUpdated;
		this.serialNo = serialNo;
		this.requirement = requirement;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Requirement getRequirement() {
		return requirement;
	}

	public void setRequirement(Requirement requirement) {
		this.requirement = requirement;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPotentialCandidates() {
		return potentialCandidates;
	}

	public void setPotentialCandidates(String potentialCandidates) {
		this.potentialCandidates = potentialCandidates;
	}

	public Integer getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}

	@Override
	public String toString() {
		return "Interview [id=" + id + ", date="+date+ ", task="+task + ", status="+status + ", comments="+comments +", potentialCandidates="+potentialCandidates
				+", created="+created+ ", lastUpdated="+lastUpdated+", lastUpdatedUser="+lastUpdatedUser+ ", serialNo="+serialNo+ 
				"]";
	}
}