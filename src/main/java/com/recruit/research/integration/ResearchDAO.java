package com.recruit.research.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.client.service.Client;
import com.recruit.research.service.Research;



/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class ResearchDAO {

	private static final Log log = LogFactory.getLog(ResearchDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Research transientInstance) {
		log.debug("persisting Research instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Research instance) {
		log.debug("attaching dirty Research instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Research persistentInstance) {
		log.debug("deleting Research instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Research merge(Research detachedInstance) {
		log.debug("merging Research instance");
		try {
			Research result = (Research) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Research findById(java.lang.Integer id) {
		log.debug("getting Research instance with id: " + id);
		try {
			Research instance = (Research) sessionFactory.getCurrentSession()
					.get("com.recruit.research.service.Research", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Research> findByExample(Client instance) {
		log.debug("finding Client instance by example");
		try {
			List<Research> results = (List<Research>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.research.service.Research").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Research> findByCriteria(SearchFilter filter) {
		try {
			String query = "from Research t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("requirementId") != null) {
				query += " and t.requirement.id like '"+map.get("requirementId").getStrValue()+"'";
			}

			System.out.println("Research : "+query);
			List<Research> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

}