package com.recruit.candidate.service;


public class Candidate_RoleSet implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer roleSetId;

	private Candidate candidate;

	public Candidate_RoleSet() {}
	
	public Candidate_RoleSet(Integer id, Integer roleSetId , Candidate candidate) {
		this.id = id;
		this.candidate = candidate;
		this.roleSetId = roleSetId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoleSetId() {
		return roleSetId;
	}

	public void setRoleSetId(Integer roleSetId) {
		this.roleSetId = roleSetId;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id  + ", roleSetId=" + roleSetId +"]";
	}


}