package com.recruit.candidate.service;


import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.recruit.client.service.Contact;
import com.recruit.requirement.service.Requirement;

public class Candidate implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer priority;
	private String firstName;
	private String lastName;
	private String emailId;
	private String contactNumber;
	private String contactAddress;
	private String currentJobTitle;
	private String source;
	private String type;
	private String p1comments;
	private String p2comments;
	private String lastUpdatedUser;
	private String employer;
	private String skillSet;
	private String highestQualification;
	private String referral;
	private String marketingStatus;
	private Timestamp availability;
	private Boolean relocate;
	private Boolean travel;
	private Boolean goodCandidateForRemote;
	private String cityAndState;
	private String followUp;
	private String education;
	private String immigrationStatus;
	private Timestamp created;
	private Timestamp lastUpdated;
	private String openToCTH;
	private String employmentType;
	private String expectedRate;
	private String aboutPartner;
	private String personality;
	private Timestamp startDate;
	private Timestamp submittedDate;
	private String alert;
	private Integer confidentiality; 
	private Integer employeeId;
	private BigDecimal rateFrom;
	private BigDecimal rateTo;
	private String location;
	private String immigrationVerified;
	private String communication;
	private String experience;
	private String targetRoles;
	private String targetSkillSet;
	private String currentRoles;
	private String resumeHelp;
	private String interviewHelp;
	private String jobHelp;
	private String resumeHelpComments;
	private String interviewHelpComments;
	private String jobHelpComments;
	private String oldSkillSet;
	private String otherSkillSet;
	private Integer version;
	private String gender;
	private String contactId;
	private String resumeLink;
	private Timestamp contactDate;
	private Integer technicalScore;
	private String billingRange;
	
	private Set<Requirement> contractors = new HashSet<Requirement>(0);
	private Set<Requirement> fullTime = new HashSet<Requirement>(0);
	private Set<Certification> certificationsList = new HashSet<Certification>(0);
	private Set<Contact> contacts = new HashSet<Contact>(0);
	private Set<Candidate_SkillSet> candidate_SkillSets = new HashSet<Candidate_SkillSet>(0);
	private Set<Candidate_TargetSkillSet> candidate_TargetSkillSets = new HashSet<Candidate_TargetSkillSet>(0);
	private Set<Candidate_RoleSet> candidate_RoleSets = new HashSet<Candidate_RoleSet>(0);
	private Set<Candidate_SkillSetGroup> candidate_SkillSetGroups = new HashSet<Candidate_SkillSetGroup>(0);
	
	//this is virtual field
	private String allCertifications;
	private String skillSetIds;
	private String targetSkillSetIds;
	private String roleSetIds;
	private String skillSetGroupIds;
	
	public Candidate() {}
	
	public Candidate(int id) {
		this.id = id;
	}

	public Candidate(Integer id, Integer priority, String firstName, String lastName, String emailId, String contactNumber, String contactAddress, String currentJobTitle,
			String source, String p1comments, String p2comments, String lastUpdatedUser, Timestamp created, Timestamp lastUpdated,String type, String employer, String skillSet,
			String highestQualification, String referral, String marketingStatus, Timestamp availability, Boolean relocate, Boolean travel,String cityAndState, String followUp,
			String education, String immigrationStatus,Timestamp submittedDate, String openToCTH, Timestamp startDate, String employmentType, String expectedRate, String aboutPartner,
			String personality,String alert, Integer confidentiality, Integer employeeId, BigDecimal rateFrom, BigDecimal rateTo, String location,
			String communication, String experience, String targetRoles, String targetSkillSet, String currentRoles, String resumeHelp, String interviewHelp, String jobHelp,
			String resumeHelpComments,String interviewHelpComments,String jobHelpComments,String oldSkillSet, String allCertifications,String otherSkillSet,Boolean goodCandidateForRemote,
			String immigrationVerified, Integer version,String gender,String contactId, String resumeLink,Timestamp contactDate, Integer technicalScore,String billingRange,
			Set<Requirement> contractors, Set<Requirement> fullTime,Set<Certification> certificationsList, Set<Contact> contacts,
			Set<Candidate_SkillSet> candidate_SkillSets, Set<Candidate_TargetSkillSet> candidate_TargetSkillSets, Set<Candidate_SkillSetGroup> candidate_SkillSetGroups) {
		this.id = id;
		this.priority = priority;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.contactNumber = contactNumber;
		this.contactAddress = contactAddress;
		this.currentJobTitle = currentJobTitle;
		this.source = source;
		this.p1comments = p1comments;
		this.p2comments = p2comments;
		this.lastUpdatedUser = lastUpdatedUser;
		this.employer = employer;
		this.skillSet = skillSet;
		this.highestQualification = highestQualification;
		this.referral = referral;
		this.marketingStatus = marketingStatus;
		this.availability = availability;
		this.created = created;
		this.lastUpdated = lastUpdated;
		this.type = type;
		this.relocate = relocate;
		this.travel = travel;
		this.goodCandidateForRemote = goodCandidateForRemote;
		this.cityAndState = cityAndState;
		this.followUp = followUp;
		this.education = education;
		this.immigrationStatus = immigrationStatus;
		this.openToCTH = openToCTH;
		this.startDate = startDate;
		this.employmentType = employmentType;
		this.expectedRate =expectedRate;
		this.aboutPartner = aboutPartner;
		this.personality = personality;
		this.submittedDate = submittedDate;
		this.alert = alert;
		this.confidentiality = confidentiality;
		this.employeeId = employeeId;
		this.rateFrom = rateFrom;
		this.rateTo = rateTo;
		this.location = location;
		this.immigrationVerified = immigrationVerified;
		this.contractors = contractors;
		this.fullTime = fullTime;
		this.certificationsList = certificationsList;
		this.communication = communication;
		this.experience = experience;
		this.targetRoles = targetRoles;
		this.targetSkillSet = targetSkillSet;
		this.currentRoles = currentRoles;
		this.resumeHelp = resumeHelp;
		this.interviewHelp = interviewHelp;
		this.jobHelp = jobHelp;
		this.resumeHelpComments = resumeHelpComments;
		this.interviewHelpComments = interviewHelpComments;
		this.jobHelpComments = jobHelpComments;
		this.oldSkillSet = oldSkillSet;
		this.allCertifications = allCertifications;
		this.otherSkillSet = otherSkillSet;
		this.version = version;
		this.gender = gender;
		this.contacts = contacts;
		this.contactId = contactId;
		this.resumeLink = resumeLink;
		this.contactDate = contactDate;
		this.technicalScore = technicalScore;
		this.billingRange = billingRange;
		
		this.candidate_SkillSets = candidate_SkillSets;
		this.candidate_TargetSkillSets = candidate_TargetSkillSets;
		this.candidate_SkillSetGroups = candidate_SkillSetGroups;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactAddress() {
		return contactAddress;
	}

	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}

	public String getCurrentJobTitle() {
		return currentJobTitle;
	}

	public void setCurrentJobTitle(String currentJobTitle) {
		this.currentJobTitle = currentJobTitle;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getP1comments() {
		return p1comments;
	}

	public void setP1comments(String p1comments) {
		this.p1comments = p1comments;
	}

	public String getP2comments() {
		return p2comments;
	}

	public void setP2comments(String p2comments) {
		this.p2comments = p2comments;
	}

	public String getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getSkillSet() {
		return skillSet;
	}

	public void setSkillSet(String skillSet) {
		this.skillSet = skillSet;
	}

	public String getHighestQualification() {
		return highestQualification;
	}

	public void setHighestQualification(String highestQualification) {
		this.highestQualification = highestQualification;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

	public String getMarketingStatus() {
		return marketingStatus;
	}

	public void setMarketingStatus(String marketingStatus) {
		this.marketingStatus = marketingStatus;
	}

	public Timestamp getAvailability() {
		return availability;
	}

	public void setAvailability(Timestamp availability) {
		this.availability = availability;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getRelocate() {
		return relocate;
	}

	public void setRelocate(Boolean relocate) {
		this.relocate = relocate;
	}

	public Boolean getTravel() {
		return travel;
	}

	public void setTravel(Boolean travel) {
		this.travel = travel;
	}

	public Boolean getGoodCandidateForRemote() {
		return goodCandidateForRemote;
	}

	public void setGoodCandidateForRemote(Boolean goodCandidateForRemote) {
		this.goodCandidateForRemote = goodCandidateForRemote;
	}

	public String getCityAndState() {
		return cityAndState;
	}

	public void setCityAndState(String cityAndState) {
		this.cityAndState = cityAndState;
	}

	public String getFollowUp() {
		return followUp;
	}

	public void setFollowUp(String followUp) {
		this.followUp = followUp;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getImmigrationStatus() {
		return immigrationStatus;
	}

	public void setImmigrationStatus(String immigrationStatus) {
		this.immigrationStatus = immigrationStatus;
	}

	public Set<Requirement> getContractors() {
		return contractors;
	}

	public void setContractors(Set<Requirement> contractors) {
		this.contractors = contractors;
	}

	public Set<Requirement> getFullTime() {
		return fullTime;
	}

	public void setFullTime(Set<Requirement> fullTime) {
		this.fullTime = fullTime;
	}

	public String getOpenToCTH() {
		return openToCTH;
	}

	public void setOpenToCTH(String openToCTH) {
		this.openToCTH = openToCTH;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public String getExpectedRate() {
		return expectedRate;
	}

	public void setExpectedRate(String expectedRate) {
		this.expectedRate = expectedRate;
	}

	public String getAboutPartner() {
		return aboutPartner;
	}

	public void setAboutPartner(String aboutPartner) {
		this.aboutPartner = aboutPartner;
	}

	public String getPersonality() {
		return personality;
	}

	public void setPersonality(String personality) {
		this.personality = personality;
	}

	public Timestamp getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(Timestamp submittedDate) {
		this.submittedDate = submittedDate;
	}

	public Set<Certification> getCertificationsList() {
		return certificationsList;
	}

	public void setCertificationsList(Set<Certification> certificationsList) {
		this.certificationsList = certificationsList;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public Integer getConfidentiality() {
		return confidentiality;
	}

	public void setConfidentiality(Integer confidentiality) {
		this.confidentiality = confidentiality;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public BigDecimal getRateFrom() {
		return rateFrom;
	}

	public void setRateFrom(BigDecimal rateFrom) {
		this.rateFrom = rateFrom;
	}

	public BigDecimal getRateTo() {
		return rateTo;
	}

	public void setRateTo(BigDecimal rateTo) {
		this.rateTo = rateTo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getImmigrationVerified() {
		return immigrationVerified;
	}

	public void setImmigrationVerified(String immigrationVerified) {
		this.immigrationVerified = immigrationVerified;
	}

	public String getCommunication() {
		return communication;
	}

	public void setCommunication(String communication) {
		this.communication = communication;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getTargetRoles() {
		return targetRoles;
	}

	public void setTargetRoles(String targetRoles) {
		this.targetRoles = targetRoles;
	}

	public String getTargetSkillSet() {
		return targetSkillSet;
	}

	public void setTargetSkillSet(String targetSkillSet) {
		this.targetSkillSet = targetSkillSet;
	}

	public String getCurrentRoles() {
		return currentRoles;
	}

	public void setCurrentRoles(String currentRoles) {
		this.currentRoles = currentRoles;
	}

	public String getResumeHelp() {
		return resumeHelp;
	}

	public void setResumeHelp(String resumeHelp) {
		this.resumeHelp = resumeHelp;
	}

	public String getInterviewHelp() {
		return interviewHelp;
	}

	public void setInterviewHelp(String interviewHelp) {
		this.interviewHelp = interviewHelp;
	}

	public String getJobHelp() {
		return jobHelp;
	}

	public void setJobHelp(String jobHelp) {
		this.jobHelp = jobHelp;
	}

	public String getResumeHelpComments() {
		return resumeHelpComments;
	}

	public void setResumeHelpComments(String resumeHelpComments) {
		this.resumeHelpComments = resumeHelpComments;
	}

	public String getInterviewHelpComments() {
		return interviewHelpComments;
	}

	public void setInterviewHelpComments(String interviewHelpComments) {
		this.interviewHelpComments = interviewHelpComments;
	}

	public String getJobHelpComments() {
		return jobHelpComments;
	}

	public void setJobHelpComments(String jobHelpComments) {
		this.jobHelpComments = jobHelpComments;
	}

	public String getOldSkillSet() {
		return oldSkillSet;
	}

	public void setOldSkillSet(String oldSkillSet) {
		this.oldSkillSet = oldSkillSet;
	}

	public String getAllCertifications() {
		return allCertifications;
	}

	public void setAllCertifications(String allCertifications) {
		this.allCertifications = allCertifications;
	}

	public String getOtherSkillSet() {
		return otherSkillSet;
	}

	public void setOtherSkillSet(String otherSkillSet) {
		this.otherSkillSet = otherSkillSet;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getResumeLink() {
		return resumeLink;
	}

	public void setResumeLink(String resumeLink) {
		this.resumeLink = resumeLink;
	}

	public Timestamp getContactDate() {
		return contactDate;
	}

	public void setContactDate(Timestamp contactDate) {
		this.contactDate = contactDate;
	}

	public Integer getTechnicalScore() {
		return technicalScore;
	}

	public void setTechnicalScore(Integer technicalScore) {
		this.technicalScore = technicalScore;
	}

	public Set<Candidate_SkillSet> getCandidate_SkillSets() {
		return candidate_SkillSets;
	}

	public void setCandidate_SkillSets(Set<Candidate_SkillSet> candidate_SkillSets) {
		this.candidate_SkillSets = candidate_SkillSets;
	}

	public Set<Candidate_TargetSkillSet> getCandidate_TargetSkillSets() {
		return candidate_TargetSkillSets;
	}

	public void setCandidate_TargetSkillSets(
			Set<Candidate_TargetSkillSet> candidate_TargetSkillSets) {
		this.candidate_TargetSkillSets = candidate_TargetSkillSets;
	}

	public String getSkillSetIds() {
		return skillSetIds;
	}

	public void setSkillSetIds(String skillSetIds) {
		this.skillSetIds = skillSetIds;
	}

	public String getTargetSkillSetIds() {
		return targetSkillSetIds;
	}

	public void setTargetSkillSetIds(String targetSkillSetIds) {
		this.targetSkillSetIds = targetSkillSetIds;
	}

	public String getRoleSetIds() {
		return roleSetIds;
	}

	public void setRoleSetIds(String roleSetIds) {
		this.roleSetIds = roleSetIds;
	}

	public Set<Candidate_RoleSet> getCandidate_RoleSets() {
		return candidate_RoleSets;
	}

	public void setCandidate_RoleSets(Set<Candidate_RoleSet> candidate_RoleSets) {
		this.candidate_RoleSets = candidate_RoleSets;
	}

	public Set<Candidate_SkillSetGroup> getCandidate_SkillSetGroups() {
		return candidate_SkillSetGroups;
	}

	public void setCandidate_SkillSetGroups(Set<Candidate_SkillSetGroup> candidate_SkillSetGroups) {
		this.candidate_SkillSetGroups = candidate_SkillSetGroups;
	}

	public String getSkillSetGroupIds() {
		return skillSetGroupIds;
	}

	public void setSkillSetGroupIds(String skillSetGroupIds) {
		this.skillSetGroupIds = skillSetGroupIds;
	}

	public String getBillingRange() {
		return billingRange;
	}

	public void setBillingRange(String billingRange) {
		this.billingRange = billingRange;
	}

	@Override
	public String toString() {
		return "Candidate [id=" + id + ", priority=" + priority + ", firstName=" + firstName + ", lastName=" + lastName + ", emailId=" + emailId + ", contactNumber=" + contactNumber
				+ ", contactAddress=" + contactAddress + ", currentJobTitle=" + currentJobTitle + ", source=" + source + ", type=" + type + ", p1comments=" + p1comments 
				+ ", p2comments=" + p2comments + ", lastUpdatedUser=" + lastUpdatedUser + ", employer=" + employer + ", skillSet=" + skillSet 
				+ ", highestQualification=" + highestQualification + ", referral=" + referral + ", marketingStatus=" + marketingStatus + ", availability=" + availability 
				+ ", relocate=" + relocate + ", travel=" + travel + ", cityAndState=" + cityAndState + ", followUp=" + followUp + ", education=" + education 
				+ ", immigrationStatus=" + immigrationStatus + ", created=" + created + ", lastUpdated=" + lastUpdated + ", openToCTH=" + openToCTH 
				+ ", employmentType=" + employmentType + ", expectedRate=" + expectedRate + ", aboutPartner=" + aboutPartner + ", personality=" + personality 
				+ ", startDate=" + startDate + ", submittedDate=" + submittedDate + ", alert=" + alert + ", confidentiality=" + confidentiality 
				+ ", employeeId=" + employeeId + ", rateFrom=" + rateFrom + ", rateTo=" + rateTo + ", location=" + location + ", immigrationVerified=" + immigrationVerified 
				+ ", contractors=" + contractors + ", fullTime=" + fullTime + ", communication=" + communication + ", experience=" + experience + ", targetRoles=" + targetRoles 
				+ ", targetSkillSet=" + targetSkillSet + ", currentRoles=" + currentRoles + ", resumeHelp=" + resumeHelp + ", interviewHelp=" + interviewHelp + ", jobHelp=" + jobHelp
				+ ", resumeHelpComments=" + resumeHelpComments + ", interviewHelpComments=" + interviewHelpComments + ", jobHelpComments=" + jobHelpComments
				+ ", certificationsList=" + certificationsList + ", oldSkillSet=" + oldSkillSet + ", allCertifications=" + allCertifications + ", otherSkillSet=" + otherSkillSet
				+ ", goodCandidateForRemote=" + goodCandidateForRemote + ", version=" + version + ", gender=" + gender + ", contacts=" + contacts + ", contactId=" + contactId
				+ ", resumeLink=" + resumeLink + ", contactDate=" + contactDate + ", technicalScore=" + technicalScore + ", billingRange=" + billingRange
				+ "]";
	}
	
}