package com.recruit.candidate.service;

public class Candidate_SkillSetGroup implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer skillSetGroupId;

	private Candidate candidate;

	public Candidate_SkillSetGroup() {}
	
	public Candidate_SkillSetGroup(Integer id, Integer skillSetGroupId , Candidate candidate) {
		this.id = id;
		this.candidate = candidate;
		this.skillSetGroupId = skillSetGroupId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getSkillSetGroupId() {
		return skillSetGroupId;
	}

	public void setSkillSetGroupId(Integer skillSetGroupId) {
		this.skillSetGroupId = skillSetGroupId;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id  + ", skillSetGroupId=" + skillSetGroupId +"]";
	}


}