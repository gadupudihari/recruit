package com.recruit.candidate.service;

import java.sql.Timestamp;

import com.recruit.client.service.Client;


public class MarketingCandidates implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private String emailFrom;
	private String emailTo;
	private Timestamp marketedDate;
	
	private Candidate candidate;
	private Client vendor;

	public MarketingCandidates() {}

	public MarketingCandidates(Integer id, String emailFrom, String emailTo, Timestamp marketedDate, Candidate candidate, Client vendor) {
		this.id = id;
		this.emailFrom = emailFrom;
		this.emailTo = emailTo;
		this.marketedDate = marketedDate;
		this.candidate = candidate;
		this.vendor = vendor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public Timestamp getMarketedDate() {
		return marketedDate;
	}

	public void setMarketedDate(Timestamp marketedDate) {
		this.marketedDate = marketedDate;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public Client getVendor() {
		return vendor;
	}

	public void setVendor(Client vendor) {
		this.vendor = vendor;
	}

	@Override
	public String toString() {
		return "MarketingCandidates [id=" + id + ", emailFrom=" + emailFrom + ", emailTo=" + emailTo + ", marketedDate=" + marketedDate
				+ "]";
	}
	
	

}