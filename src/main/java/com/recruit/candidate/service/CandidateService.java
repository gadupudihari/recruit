package com.recruit.candidate.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.recruit.base.presentation.Response;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.integration.CandidateDAO;
import com.recruit.candidate.integration.Candidate_RoleSetDAO;
import com.recruit.candidate.integration.Candidate_SkillSetDAO;
import com.recruit.candidate.integration.Candidate_SkillSetGroupDAO;
import com.recruit.candidate.integration.Candidate_TargetSkillSetDAO;
import com.recruit.candidate.integration.CertificationDAO;
import com.recruit.candidate.integration.MarketingCandidatesDAO;
import com.recruit.client.service.Client;
import com.recruit.interview.integration.InterviewDAO;
import com.recruit.reports.integration.ReportDAO;
import com.recruit.requirement.integration.RequirementsDAO;
import com.recruit.requirement.service.Requirement;
import com.recruit.user.integration.UserDAO;
import com.recruit.user.service.User;
import com.recruit.util.UtilityHelper;

@Service
public class CandidateService {

	@Autowired
	CandidateDAO candidateDAO;
	
	@Autowired 
	RequirementsDAO requirementsDAO;
	
	@Autowired
	CertificationDAO certificationDAO;

	@Autowired
	InterviewDAO interviewDAO;

	@Autowired
	UserDAO userDAO;
	
	@Autowired
	ReportDAO reportDAO;
	
	@Autowired
	Candidate_SkillSetDAO candidate_SkillSetDAO;
	
	@Autowired
	Candidate_TargetSkillSetDAO candidate_TargetSkillSetDAO;

	@Autowired
	Candidate_RoleSetDAO candidate_RoleSetDAO;

	@Autowired
	Candidate_SkillSetGroupDAO candidate_SkillSetGroupDAO;

	@Autowired
	MarketingCandidatesDAO marketingCandidatesDAO;
	
	@Transactional
	public List<Candidate> getCandidates(SearchFilter filter, String loginId) {
		return candidateDAO.findByCriteria(filter,loginId);
	}

	//This methos is for saving candidates only from suggested candidates grid
	@Transactional
	public void saveCandidates(List<Candidate> candidates, String loginId,Timestamp createdDate, String name) {
		for (Candidate candidate : candidates) {
			candidate.setLastUpdated(createdDate);
			candidate.setLastUpdatedUser(loginId);
			if (candidate.getId() != null ) {
				Candidate preCandidate = candidateDAO.findById(candidate.getId());
				
				preCandidate.setPriority(candidate.getPriority());
				preCandidate.setExperience(candidate.getExperience());
				preCandidate.setMarketingStatus(candidate.getMarketingStatus());
				preCandidate.setAlert(candidate.getAlert());
				preCandidate.setExpectedRate(candidate.getExpectedRate());
				preCandidate.setAvailability(candidate.getAvailability());
				preCandidate.setRelocate(candidate.getRelocate());
				preCandidate.setTravel(candidate.getTravel());
				preCandidate.setOpenToCTH(candidate.getOpenToCTH());
				preCandidate.setCityAndState(candidate.getCityAndState());
				preCandidate.setLocation(candidate.getLocation());
				preCandidate.setFollowUp(candidate.getFollowUp());
				preCandidate.setLastUpdated(createdDate);
				preCandidate.setLastUpdatedUser(loginId);

				candidateDAO.merge(preCandidate);
			}
		}
	}

	@Transactional
	public void deleteCandidate(Response response,Integer id) {
		Candidate candidate = candidateDAO.findById(id);
		List<Requirement> requirements = requirementsDAO.findByCandidateId(id);
		if (requirements != null && requirements.size() > 0) {
			response.setSuccess(false);
			response.setErrorMessage("There are Requirements associated to this Candidate.");
		}else if (candidate.getContractors() != null && candidate.getContractors().size() >0) {
			response.setSuccess(false);
			response.setErrorMessage("This Candidate is associated to the Client as Contractor.");
		}else if (candidate.getFullTime() != null && candidate.getFullTime().size() >0) {
			response.setSuccess(false);
			response.setErrorMessage("This Candidate is associated to the Client as FullTime.");
		}else if (candidate.getCertificationsList() != null && candidate.getCertificationsList().size() >0) {
			response.setSuccess(false);
			response.setErrorMessage("There are Certifications associated to this Candidate.");
		}else if (interviewDAO.findbyCandidateId(candidate.getId()) != null && (interviewDAO.findbyCandidateId(candidate.getId())).size() > 0) {
			response.setSuccess(false);
			response.setErrorMessage("There are Interviews associated to this Candidate.");
		}else if (candidate.getEmployeeId() != null && candidate.getEmployeeId() != 0) {
			response.setSuccess(false);
			response.setErrorMessage("This Candidate is an employee in AJ, Cannot Delete.");
		}else if (marketingCandidatesDAO.hasMarketted(candidate)) {
			response.setSuccess(false);
			response.setErrorMessage("This Candidate is marketted, Cannot Delete.");
		}else{
			//delete skill set and target skill set, roll set and skill set group from sub tables
			candidate_SkillSetDAO.deleteByCandidate(candidate.getId());
			candidate_TargetSkillSetDAO.deleteByCandidate(candidate.getId());
			candidate_RoleSetDAO.deleteByCandidate(candidate.getId());
			candidate_SkillSetGroupDAO.deleteByCandidate(candidate.getId());
			candidateDAO.delete(candidate);
			response.setSuccess(true);
		}
	}

	@Transactional
	public void saveCandidate(Candidate candidate, String name, Timestamp createdDate, Response response) {
		if (candidate.getId() != null ) {
			Candidate preCandidate = candidateDAO.findById(candidate.getId());
			
			//check version of previous and present Candidates
			if (candidate.getVersion() < preCandidate.getVersion()) {
				response.setSuccess(Boolean.FALSE);
				response.setErrorMessage("Version mismatch");
				return ;
			}else
				candidate.setVersion(preCandidate.getVersion()+1);
			
			if (candidate.getP1comments() != null && candidate.getP1comments().length() > 0){
				// New candidate comeents in not null
				if ((preCandidate.getP1comments() == null || preCandidate.getP1comments().length() == 0) && candidate.getP1comments().length() < 989 ) {
					// old comments is null the set set date before new comeents , and size should not ecxeed database size
					candidate.setP1comments(new SimpleDateFormat("MM.dd.yy").format(createdDate) +" "+ name +". "+ candidate.getP1comments());
				}else if (!preCandidate.getP1comments().equals(candidate.getP1comments()) && candidate.getP1comments().length() < 989 ) {
					//Old comments is bot null && new comments is not null, the compare both if not equal set date , and size should not ecxeed database size
					candidate.setP1comments(new SimpleDateFormat("MM.dd.yy").format(createdDate) +" "+ name +". "+ candidate.getP1comments());
				}
			}
			if (candidate.getP2comments() != null && candidate.getP2comments().length() > 0){
				// New candidate comeents in not null
				if ((preCandidate.getP2comments() == null || preCandidate.getP2comments().length() == 0) && candidate.getP2comments().length() < 989 ) {
					// old comments is null the set set date before new comeents, and size should not ecxeed database size
					candidate.setP2comments(new SimpleDateFormat("MM.dd.yy").format(createdDate) +" "+ name +". "+ candidate.getP2comments());
				}else if (!preCandidate.getP2comments().equals(candidate.getP2comments()) && candidate.getP2comments().length() < 989 ) {
					//Old comments is bot null && new comments is not null, the compare both if not equal set date , and size should not ecxeed database size
					candidate.setP2comments(new SimpleDateFormat("MM.dd.yy").format(createdDate) +" "+ name +". "+ candidate.getP2comments());
				}
			}
			
			//if candidate exists and added into aj don't change email, cellphone, immigration
			if (candidate.getEmployeeId() != null) {
				candidate.setEmailId(preCandidate.getEmailId());
				candidate.setContactNumber(preCandidate.getContactNumber());
				candidate.setImmigrationStatus(preCandidate.getImmigrationStatus());
			}
	   		if(candidate.getP1comments().length()  > 1000 || candidate.getP2comments().length()  > 1000 ){
	   			response.setErrorMessage("Max length(1000) exceeded for comments.");
	   			response.setSuccess(Boolean.FALSE);
	   			return ;
	   		}

			candidate.setCreated(preCandidate.getCreated());
			
			//delete and insert skillset 
			candidate_SkillSetDAO.deleteByCandidate(candidate.getId());
			if (candidate.getCandidate_SkillSets() != null && candidate.getCandidate_SkillSets().size() > 0) {
				for (Candidate_SkillSet skillSet : candidate.getCandidate_SkillSets()) {
					skillSet.setCandidate(candidate);
					candidate_SkillSetDAO.attachDirty(skillSet);
				}
			}
			//delete and insert target skillset
			candidate_TargetSkillSetDAO.deleteByCandidate(candidate.getId());
			if (candidate.getCandidate_TargetSkillSets() != null && candidate.getCandidate_TargetSkillSets().size() > 0) {
				for (Candidate_TargetSkillSet targetSkillSet : candidate.getCandidate_TargetSkillSets()) {
					targetSkillSet.setCandidate(candidate);
					candidate_TargetSkillSetDAO.attachDirty(targetSkillSet);
				}
			}
			//delete and insert Role set
			candidate_RoleSetDAO.deleteByCandidate(candidate.getId());
			if (candidate.getCandidate_RoleSets() != null && candidate.getCandidate_RoleSets().size() > 0) {
				for (Candidate_RoleSet roleSet : candidate.getCandidate_RoleSets()) {
					roleSet.setCandidate(candidate);
					candidate_RoleSetDAO.attachDirty(roleSet);
				}
			}
			//delete and insert Skill set group
			candidate_SkillSetGroupDAO.deleteByCandidate(candidate.getId());
			if (candidate.getCandidate_SkillSetGroups() != null && candidate.getCandidate_SkillSetGroups().size() > 0) {
				for (Candidate_SkillSetGroup skillSet : candidate.getCandidate_SkillSetGroups()) {
					skillSet.setCandidate(candidate);
					candidate_SkillSetGroupDAO.attachDirty(skillSet);
				}
			}
			
			candidateDAO.merge(candidate);
			
		}else{
			if (candidate.getP1comments() != null && !candidate.getP1comments().equals(""))
				candidate.setP1comments(new SimpleDateFormat("MM.dd.yy").format(createdDate) +" "+ name +". "+ candidate.getP1comments());
			if (candidate.getP2comments() != null && !candidate.getP2comments().equals(""))
				candidate.setP2comments(new SimpleDateFormat("MM.dd.yy").format(createdDate) +" "+ name +". "+ candidate.getP2comments());
			
			if ((candidate.getP1comments() != null && candidate.getP1comments().length()  > 1000 ) ||
					(candidate.getP2comments() != null && candidate.getP2comments().length()  > 1000 )) {
	   			response.setErrorMessage("Max length(1000) exceeded for comments.");
	   			response.setSuccess(Boolean.FALSE);
	   			return ;
			}
			candidateDAO.attachDirty(candidate);
			
			//insert skill set
			if (candidate.getCandidate_SkillSets() != null && candidate.getCandidate_SkillSets().size() > 0) {
				for (Candidate_SkillSet skillSet : candidate.getCandidate_SkillSets()) {
					skillSet.setCandidate(candidate);
					candidate_SkillSetDAO.attachDirty(skillSet);
				}
			}
			//insert target skill set
			if (candidate.getCandidate_TargetSkillSets() != null && candidate.getCandidate_TargetSkillSets().size() > 0) {
				for (Candidate_TargetSkillSet targetSkillSet : candidate.getCandidate_TargetSkillSets()) {
					targetSkillSet.setCandidate(candidate);
					candidate_TargetSkillSetDAO.attachDirty(targetSkillSet);
				}
			}
			//insert role set
			if (candidate.getCandidate_RoleSets() != null && candidate.getCandidate_RoleSets().size() > 0) {
				for (Candidate_RoleSet roleSet : candidate.getCandidate_RoleSets()) {
					roleSet.setCandidate(candidate);
					candidate_RoleSetDAO.attachDirty(roleSet);
				}
			}
			//insert Skill set group
			if (candidate.getCandidate_SkillSetGroups() != null && candidate.getCandidate_SkillSetGroups().size() > 0) {
				for (Candidate_SkillSetGroup skillSet : candidate.getCandidate_SkillSetGroups()) {
					skillSet.setCandidate(candidate);
					candidate_SkillSetGroupDAO.attachDirty(skillSet);
				}
			}
		}
		response.setSuccess(Boolean.TRUE);
	}

	@Transactional
	public List<Candidate> getAllCandidates(String loginId, SearchFilter filter) {
		return candidateDAO.getAllCandidates(loginId,filter);
	}

	@Transactional
	public List<Certification> getCertifications(SearchFilter filter) {
		return certificationDAO.findByCriteria(filter);
	}

	@Transactional
	public void deleteCertificate(Response response, int id) {
		Certification certification = certificationDAO.findById(id);
		certificationDAO.delete(certification);
	}

	@Transactional
	public void saveCertificates(Response response, List<Certification> certificates) {
		for (Certification certificate : certificates) {
			certificationDAO.merge(certificate);
		}
		response.setReturnVal(certificates);
	}

	@Transactional
	public void giveOnboarding(Response response, int candidateId) {
		candidateDAO.giveOnboarding(candidateId);
		Candidate candidate = candidateDAO.findById(candidateId);
		response.setReturnVal(candidate);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getAllContractors() {
		return candidateDAO.getAllContractors();
	}
	
	@Transactional
	public Candidate findById(Integer candidateId) {
		return candidateDAO.findById(candidateId);
	}

	@Transactional
	public List<Candidate> findByIds(String candidateIds) {
		return candidateDAO.findByIds(candidateIds);
	}

	@Transactional
	public Candidate findCandidateName(Integer candidateId) {
		return candidateDAO.findCandidateName(candidateId);
	}

	@Transactional
	public void immigrationVerifyEmails(String loginId, Timestamp createdDate, String jobId,String[] candidateIds) {
		for (String candidateId : candidateIds) {
			Candidate candidate = candidateDAO.findById(Integer.parseInt(candidateId));
			Boolean verified = true;
			if (candidate.getPriority() != null && (candidate.getPriority() == 0 || candidate.getPriority() == 1) ) {
				if (candidate.getImmigrationVerified() == null) 
					verified = false;
				if (candidate.getImmigrationVerified().length() == 0 || candidate.getImmigrationVerified().equalsIgnoreCase("NO")) 
					verified = false;
			}
			
			if (! verified) {
				String subject = candidate.getFirstName()+' '+candidate.getLastName()+" Immigration status not verified";
				String message = candidate.getFirstName()+' '+candidate.getLastName()+" is submitted to Job ID : "+jobId+
						"<br>His/Her Immigration status <b>"+candidate.getImmigrationStatus()+"</b> has not been verified, Please verify asap.";
				
				//delete from email table of this same job opening alert if exist
				String query = "Delete from email where alertName ='Job Submission' and date(alertDate) = date(now()) and refId = "+candidateId;
				reportDAO.updateEmailRecords(query);
				
				List<User> admins = userDAO.getEmailAdmin();
				for (User user : admins) {
					// save alert in email table for each admin
					query="insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,loginId,alertInDays,refId,application,priority) " +
							"values('"+user.getEmailId()+"','" +createdDate.toString()+"','"+createdDate.toString()+"','"+subject+"','"+message+"',"+true+",'"+"Job Submission"+"','"+
							"AD HOC"+"','"+loginId+"',"+0+",'"+candidateId+"','recruit',2)";
					
					System.out.println(query);
					reportDAO.updateEmailRecords(query);
				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List findProjBillingsByCriteria(SearchFilter filter) {
		return candidateDAO.findTNEProjBillingsByCriteria(filter);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getSuggestedCriteria(int id) {
		return candidateDAO.getSuggestedCriteria(id);
	}

	@SuppressWarnings("rawtypes")
	@Transactional
	public List getNegativeCriteria(SearchFilter filter) {
		return candidateDAO.getNegativeCriteria(filter);
	}

	@Transactional
	public void saveMarketingCandidates(String candidateIds, String vendorId, String fromEmailId, String toEmailIds) {
		try {
			for (String candidateId : candidateIds.split(",")) {
				MarketingCandidates marketingCandidate = new MarketingCandidates();
				Candidate candidate = new Candidate();
				candidate.setId(Integer.parseInt(candidateId));
				Client vendor =  new Client();
				vendor.setId(Integer.parseInt(vendorId));
				marketingCandidate.setCandidate(candidate);
				marketingCandidate.setVendor(vendor);
				marketingCandidate.setEmailFrom(fromEmailId);
				marketingCandidate.setEmailTo(toEmailIds);
				marketingCandidate.setMarketedDate(UtilityHelper.getDate());
				marketingCandidatesDAO.attachDirty(marketingCandidate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally {
			System.out.println("------In finally Block -----------");
			System.gc();
		}
		
		
		
		
	}

}
