package com.recruit.candidate.service;


import java.math.BigDecimal;
import java.sql.Timestamp;

import com.recruit.candidate.service.Candidate;

public class Certification implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private String name;
	private String status;
	private String sponsoredBy;
	private String reimbursementStatus;
	private Timestamp dateAchieved;
	private String version;
	private String certificationComments;
	private String epicUserWeb;
	private String latestNVT;
	private String expensesStatus;
	private String paymentDetails;
	private String epicInvoiceNo;
	private String expenseComments;
	private String verified;
	private String createdUser;
	private String lastUpdatedUser;
	private Timestamp created;
	private Timestamp lastUpdated;
	private String epicInvoiceComments;
	private BigDecimal epicInvoiceAmount;
	private BigDecimal cpsPaymentToEpic;
	private BigDecimal expensesIncurred;
	private BigDecimal expensesPaid;
	private Integer settingsId;

	private Candidate candidate;
	
	public Certification() {}
	
	public Certification(Candidate candidate) {
		this.candidate = candidate;
	}

	public Certification(String name, String status,String sponsoredBy, String reimbursementStatus,Timestamp dateAchieved, String version, String certificationComments,Candidate candidate,
			String epicUserWeb, String latestNVT, String expensesStatus, String paymentDetails, String epicInvoiceNo, String expenseComments, String verified,
			String epicInvoiceComments, BigDecimal epicInvoiceAmount, BigDecimal cpsPaymentToEpic, BigDecimal expensesIncurred, BigDecimal expensesPaid,
			Integer settingsId, String createdUser,String lastUpdatedUser,Timestamp created,Timestamp lastUpdated) {
		this.name = name;
		this.status = status;
		this.sponsoredBy = sponsoredBy;
		this.reimbursementStatus = reimbursementStatus;
		this.dateAchieved = dateAchieved;
		this.version = version;
		this.certificationComments = certificationComments;
		this.candidate = candidate;
		this.epicUserWeb = epicUserWeb;
		this.latestNVT = latestNVT;
		this.expensesStatus = expensesStatus;
		this.paymentDetails = paymentDetails;
		this.epicInvoiceNo = epicInvoiceNo;
		this.expenseComments = expenseComments;
		this.verified = verified;
		this.createdUser = createdUser;
		this.created = created;
		this.lastUpdatedUser = lastUpdatedUser;
		this.lastUpdated = lastUpdated;
		this.epicInvoiceComments = epicInvoiceComments;
		this.epicInvoiceAmount = epicInvoiceAmount;
		this.cpsPaymentToEpic = cpsPaymentToEpic;
		this.expensesIncurred = expensesIncurred;
		this.expensesPaid = expensesPaid;
		this.settingsId = settingsId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSponsoredBy() {
		return sponsoredBy;
	}

	public void setSponsoredBy(String sponsoredBy) {
		this.sponsoredBy = sponsoredBy;
	}

	public String getReimbursementStatus() {
		return reimbursementStatus;
	}

	public void setReimbursementStatus(String reimbursementStatus) {
		this.reimbursementStatus = reimbursementStatus;
	}

	public Timestamp getDateAchieved() {
		return dateAchieved;
	}

	public void setDateAchieved(Timestamp dateAchieved) {
		this.dateAchieved = dateAchieved;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCertificationComments() {
		return certificationComments;
	}

	public void setCertificationComments(String certificationComments) {
		this.certificationComments = certificationComments;
	}

	public String getEpicUserWeb() {
		return epicUserWeb;
	}

	public void setEpicUserWeb(String epicUserWeb) {
		this.epicUserWeb = epicUserWeb;
	}

	public String getLatestNVT() {
		return latestNVT;
	}

	public void setLatestNVT(String latestNVT) {
		this.latestNVT = latestNVT;
	}

	public String getExpensesStatus() {
		return expensesStatus;
	}

	public void setExpensesStatus(String expensesStatus) {
		this.expensesStatus = expensesStatus;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public String getEpicInvoiceNo() {
		return epicInvoiceNo;
	}

	public void setEpicInvoiceNo(String epicInvoiceNo) {
		this.epicInvoiceNo = epicInvoiceNo;
	}

	public String getExpenseComments() {
		return expenseComments;
	}

	public void setExpenseComments(String expenseComments) {
		this.expenseComments = expenseComments;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public String getVerified() {
		return verified;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getEpicInvoiceComments() {
		return epicInvoiceComments;
	}

	public void setEpicInvoiceComments(String epicInvoiceComments) {
		this.epicInvoiceComments = epicInvoiceComments;
	}

	public BigDecimal getEpicInvoiceAmount() {
		return epicInvoiceAmount;
	}

	public void setEpicInvoiceAmount(BigDecimal epicInvoiceAmount) {
		this.epicInvoiceAmount = epicInvoiceAmount;
	}

	public BigDecimal getCpsPaymentToEpic() {
		return cpsPaymentToEpic;
	}

	public void setCpsPaymentToEpic(BigDecimal cpsPaymentToEpic) {
		this.cpsPaymentToEpic = cpsPaymentToEpic;
	}

	public BigDecimal getExpensesIncurred() {
		return expensesIncurred;
	}

	public void setExpensesIncurred(BigDecimal expensesIncurred) {
		this.expensesIncurred = expensesIncurred;
	}

	public BigDecimal getExpensesPaid() {
		return expensesPaid;
	}

	public void setExpensesPaid(BigDecimal expensesPaid) {
		this.expensesPaid = expensesPaid;
	}

	public Integer getSettingsId() {
		return settingsId;
	}

	public void setSettingsId(Integer settingsId) {
		this.settingsId = settingsId;
	}

	@Override
	public String toString() {
		return "Certification [id=" + id + ", name="+name+ ", status="+status+ ", sponsoredBy="+sponsoredBy+ ", reimbursementStatus="+reimbursementStatus + 
				", dateAchieved="+dateAchieved+ ", version="+version+ ", certificationComments="+certificationComments+ ", epicInvoiceNo="+ epicInvoiceNo+ ", epicUserWeb="+ epicUserWeb +
				", latestNVT=" + latestNVT + ", expensesStatus=" + expensesStatus + ", paymentDetails=" + paymentDetails + ", expenseComments=" + expenseComments +
				", verified=" + verified + ", createdUser=" + createdUser + ", lastUpdatedUser=" + lastUpdatedUser + ", created=" + created + ", lastUpdated=" + lastUpdated +
				", epicInvoiceComments=" + epicInvoiceComments + ", epicInvoiceAmount=" + epicInvoiceAmount + ", cpsPaymentToEpic=" + cpsPaymentToEpic + 
				", expensesIncurred=" + expensesIncurred + ", expensesPaid=" + expensesPaid + ", settingsId=" + settingsId
				+"]";
	}

}