package com.recruit.candidate.service;

public class Candidate_TargetSkillSet implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer skillSetId;

	private Candidate candidate;

	public Candidate_TargetSkillSet() {}
	
	public Candidate_TargetSkillSet(Integer id, Integer skillSetId , Candidate candidate) {
		this.id = id;
		this.candidate = candidate;
		this.skillSetId = skillSetId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSkillSetId() {
		return skillSetId;
	}

	public void setSkillSetId(Integer skillSetId) {
		this.skillSetId = skillSetId;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	@Override
	public String toString() {
		return "TargetSkillset [id=" + id  + ", skillSetId=" + skillSetId +"]";
	}


}