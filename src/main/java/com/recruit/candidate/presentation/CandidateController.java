package com.recruit.candidate.presentation;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.recruit.base.presentation.BaseController;
import com.recruit.base.service.Param;
import com.recruit.base.service.ParamParser;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.service.Candidate;
import com.recruit.candidate.service.CandidateService;
import com.recruit.candidate.service.Certification;
import com.recruit.reports.service.ReportsService;
import com.recruit.user.service.User;
import com.recruit.user.service.UserEmail;
import com.recruit.user.service.UserService;
import com.recruit.util.JSONException;
import com.recruit.util.JSONParser;
import com.recruit.util.JsonParserUtility;
import com.recruit.util.SendEmails;
import com.recruit.util.UtilityHelper;
import com.recruit.util.WriteExcel;

@Controller
@RequestMapping("/candidate")
@PreAuthorize("hasAnyRole('ADMIN','RECRUITER')")
public class CandidateController extends BaseController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	CandidateService candidateService;
	
	@Autowired
	UserService userService;

	@Autowired
	WriteExcel writeExcel;

	@Autowired
	ReportsService reportsService;

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	
	@RequestMapping(value = "/getCandidates", method = {RequestMethod.POST,RequestMethod.GET })
	public String getCandidates(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		String loginId =UtilityHelper.getLoginId(request);
		List<Candidate> candidates = candidateService.getCandidates(filter,loginId);
		return returnJson(map, candidates);
	}

	@RequestMapping(value = "/getAllCandidates", method = {RequestMethod.POST,RequestMethod.GET })
	public String getAllCandidates(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		String loginId =UtilityHelper.getLoginId(request);
		List<Candidate> candidates = candidateService.getAllCandidates(loginId,filter);
		return returnJson(map, candidates);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getAllContractors", method = {RequestMethod.POST,RequestMethod.GET })
	public String getAllContractors(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		List candidates = candidateService.getAllContractors();
		return returnJson(map, JsonParserUtility.toCandidateListJSON(candidates).toString());
	}
	
	@RequestMapping(value = "/saveCandidates",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveCandidates(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		System.out.println("----------------------"+json);
		List<Candidate> candidates = CandidateParser.parseJSON(json);
		String loginId =UtilityHelper.getLoginId(request);
		User user = userService.loggedUser(request);
		String name = user.getFirstName().substring(0,1)+user.getLastName().substring(0,1);
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		System.out.println(candidates);
		candidateService.saveCandidates(candidates,loginId,createdDate,name);
		response.setSuccess(Boolean.TRUE);
		response.setReturnVal(candidates);
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/deleteCandidate/{id}", method = RequestMethod.GET)
	public String deleteCandidate(@PathVariable("id") Integer id, Map<String, Object> map){
		candidateService.deleteCandidate(response,id);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/saveCandidate",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveCandidate(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		Candidate candidate = (Candidate) JSONParser.parseJSON(json, Candidate.class);
		String loginId =UtilityHelper.getLoginId(request);
		User user = userService.loggedUser(request);
		String name = user.getFirstName().substring(0,1)+user.getLastName().substring(0,1);

		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		candidate.setLastUpdated(createdDate);
		candidate.setLastUpdatedUser(loginId);
		if(candidate.getId() == null )
			candidate.setCreated(createdDate);
		candidateService.saveCandidate(candidate,name,createdDate,response);
		response.setReturnVal(candidate);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/excelExport", method = {RequestMethod.POST,RequestMethod.GET })
	public String excelExport(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request,HttpServletResponse res) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		String loginId =UtilityHelper.getLoginId(request);
		List<Candidate> candidates = candidateService.getCandidates(filter,loginId);
		Map<String, Param> filterMap = filter.getParamMap();
		String columns[] = filterMap.get("columns").getStrValue().split(",");
		String contextPath="", fileName = "Candidates";
		  try{
		      ServletContext servletContext = session.getServletContext();
		      contextPath = servletContext.getRealPath(File.separator);
		      System.out.println("File system context path (in TestFilter): " + contextPath);
		      
		      UtilityHelper.deleteExcelFiles(contextPath, "Job Openings");
		      
		      writeExcel.setOutputFile(contextPath+fileName+".xls");
		      //Writing to Excel File
			  writeExcel.writeCandidates(candidates,columns);
			  System.out.println("Download Path ------ " + contextPath+fileName+".xls");
		  }
		  catch(Exception ex){
			  System.err.println("---------- **** Exception while generating Excel file **** ---------");
			  System.err.println(ex.getMessage());
			  ex.printStackTrace();
			  response.setSuccess(false);
		  }
		  response.setSuccess(true);
		  response.setReturnVal(fileName+".xls");
		  return returnJson(map, response);
	}

	@RequestMapping(value = "/getCertifications", method = {RequestMethod.POST,RequestMethod.GET })
	public String getCertifications(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Certification>  certifications = candidateService.getCertifications(filter);
		return returnJson(map, JsonParserUtility.toCertifications(certifications).toString());
	}
	
	@RequestMapping(value = "/deleteCertificate/{id}", method = {RequestMethod.POST,RequestMethod.GET })
	public String deleteCertificate(@PathVariable("id") String id, HttpSession session, Map<String, Object> map){
		candidateService.deleteCertificate(response, Integer.parseInt(id));
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/saveCertificates", method = RequestMethod.POST)
	public String saveCertificates(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		List<Certification> certificates = CertificateParser.parseJSON(json);
		
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		for (Certification certification : certificates) {
			certification.setLastUpdated(createdDate);
			certification.setLastUpdatedUser(loginId);
			if (certification.getId()== null || certification.getId()==0 ) {
				certification.setCreated(createdDate);
				certification.setCreatedUser(loginId);
			}
		}

		candidateService.saveCertificates(response,certificates);
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/giveOnboarding/{id}", method = {RequestMethod.POST,RequestMethod.GET })
	public String giveOnboarding(@PathVariable("id") String id, HttpSession session, Map<String, Object> map){
		candidateService.giveOnboarding(response,Integer.parseInt(id));
		response.setSuccess(true);
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/immigrationVerifyEmails", method = RequestMethod.POST)
	public String immigrationVerifyEmails(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		SearchFilter filter = ParamParser.parse(json);
		Map<String, Param> filterMap = filter.getParamMap();
		String candidateIds[] = filterMap.get("candidateId").getStrValue().split(",");
		String jobId = filterMap.get("jobId").getStrValue();
		String loginId =UtilityHelper.getLoginId(request);
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		candidateService.immigrationVerifyEmails(loginId,createdDate,jobId,candidateIds);
		response.setSuccess(true);
		return returnJson(map, response);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getProjectBillingRates", method = {RequestMethod.POST,RequestMethod.GET })
	public String getProjectBillingRates(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List projects = candidateService.findProjBillingsByCriteria(filter);
		return returnJson(map, JsonParserUtility.toProjectJson(projects).toString());
	}


	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getSuggestedCriteria", method = {RequestMethod.POST,RequestMethod.GET })
	public String getSuggestedCriteria(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		int id = Integer.parseInt(json);
		List list = candidateService.getSuggestedCriteria(id);
		response.setReturnVal(JsonParserUtility.toSuggestedCriteria(list).toString());
		response.setSuccess(true);
		return returnJson(map, response);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getNegativeCriteria", method = {RequestMethod.POST,RequestMethod.GET })
	public String getNegativeCriteria(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List list = candidateService.getNegativeCriteria(filter);
		response.setReturnVal(JsonParserUtility.toSuggestedCriteria(list).toString());
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/sendMarketingEmail", method = RequestMethod.POST)
	public String sendHomeEmail(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request){
		SearchFilter filter = ParamParser.parse(json);
		Map<String, Param> filterMap = filter.getParamMap();
		String emailIds = "", subject = "", message = "";
		String fromEmailId = filterMap.get("from").getStrValue();
		emailIds = filterMap.get("to").getStrValue();
		subject = filterMap.get("subject").getStrValue();
		message = filterMap.get("message").getStrValue();
		String candidateIds = filterMap.get("candidateIds").getStrValue();
		String vendorId = filterMap.get("vendorId").getStrValue();
		String cc = "";
		if (filterMap.get("cc") != null)
			cc = filterMap.get("cc").getStrValue();
		String emails[] = emailIds.split(",");
		UserEmail email = reportsService.findEmailByEmailId(fromEmailId);
		
		response = SendEmails.sendSeperateEMail(response, email, subject, message, emails,cc);
		
		candidateService.saveMarketingCandidates(candidateIds,vendorId,fromEmailId,emailIds);
		
		return returnJson(map, response);
	}

	@RequestMapping(value = "/marketCandidates", method = RequestMethod.POST)
	public String marketCandidates(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request){
		SearchFilter filter = ParamParser.parse(json);
		Map<String, Param> filterMap = filter.getParamMap();
		String fromEmailId = filterMap.get("from").getStrValue();
		String toEmailIds = filterMap.get("to").getStrValue();
		String candidateIds = filterMap.get("candidateIds").getStrValue();
		String vendorId = filterMap.get("vendorId").getStrValue();
		candidateService.saveMarketingCandidates(candidateIds,vendorId,fromEmailId,toEmailIds);
		response.setSuccess(true);
		return returnJson(map, response);
	}

}
