package com.recruit.candidate.presentation;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.candidate.service.Candidate;
import com.recruit.candidate.service.Certification;
import com.recruit.util.SqlTimestampConverter;

public class CertificateParser {

	public static List<Certification> parseJSON(String json){
        List<Certification> certifications = new ArrayList<Certification>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	Certification certificate = new Certification();
            	if(rObj.has("id")){
            		certificate.setId(rObj.getInt("id"));
            	}	
            	if (rObj.has("candidateId")) {
            		Candidate candidate = new Candidate();
            		candidate.setId(rObj.getInt("candidateId"));
            		certificate.setCandidate(candidate);
				}
            	if (rObj.has("name")) {
					certificate.setName(rObj.getString("name"));
				}
            	if (rObj.has("status")) {
					certificate.setStatus(rObj.getString("status"));
				}
            	if (rObj.has("reimbursementStatus")) {
					certificate.setReimbursementStatus(rObj.getString("reimbursementStatus"));
				}
            	if (rObj.has("sponsoredBy")) {
					certificate.setSponsoredBy(rObj.getString("sponsoredBy"));
				}
            	if (rObj.has("version")) {
					certificate.setVersion(rObj.getString("version"));
				}
            	if (rObj.has("dateAchieved") && !rObj.getString("dateAchieved").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		certificate.setDateAchieved((Timestamp)c.fromString(rObj.getString("dateAchieved")));
            	} 
            	if (rObj.has("certificationComments")) {
            		certificate.setCertificationComments(rObj.getString("certificationComments"));
				}
            	if (rObj.has("epicInvoiceNo")) {
					certificate.setEpicInvoiceNo(rObj.getString("epicInvoiceNo"));
				}
            	if (rObj.has("epicUserWeb")) {
					certificate.setEpicUserWeb(rObj.getString("epicUserWeb"));
				}
            	if (rObj.has("expensesStatus")) {
					certificate.setExpensesStatus(rObj.getString("expensesStatus"));
				}
            	if (rObj.has("expenseComments")) {
					certificate.setExpenseComments(rObj.getString("expenseComments"));
				}
            	if (rObj.has("latestNVT")) {
					certificate.setLatestNVT(rObj.getString("latestNVT"));
				}
            	if (rObj.has("paymentDetails")) {
					certificate.setPaymentDetails(rObj.getString("paymentDetails"));
				}
            	if (rObj.has("verified")) {
					certificate.setVerified(rObj.getString("verified"));
				}
            	if (rObj.has("createdUser")) {
					certificate.setCreatedUser(rObj.getString("createdUser"));
				}
            	if (rObj.has("created") && !rObj.getString("created").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		certificate.setCreated((Timestamp)c.fromString(rObj.getString("created")));
            	} 
            	if (rObj.has("epicInvoiceComments")) {
					certificate.setEpicInvoiceComments(rObj.getString("epicInvoiceComments"));
				}
            	if(rObj.has("epicInvoiceAmount")){ 
            		Double d = rObj.getDouble("epicInvoiceAmount");
            		certificate.setEpicInvoiceAmount(new BigDecimal(d));
            	}
            	if(rObj.has("cpsPaymentToEpic")){ 
            		Double d = rObj.getDouble("cpsPaymentToEpic");
            		certificate.setCpsPaymentToEpic(new BigDecimal(d));
            	}
            	if(rObj.has("expensesIncurred")){ 
            		Double d = rObj.getDouble("expensesIncurred");
            		certificate.setExpensesIncurred(new BigDecimal(d));
            	}
            	if(rObj.has("expensesPaid")){ 
            		Double d = rObj.getDouble("expensesPaid");
            		certificate.setExpensesPaid(new BigDecimal(d));
            	}
            	if (rObj.has("settingsId")) {
					certificate.setSettingsId(rObj.getInt("settingsId"));
				}
             	certifications.add(certificate);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return certifications;
	}
}
