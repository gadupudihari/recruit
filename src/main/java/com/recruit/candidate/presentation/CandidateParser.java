package com.recruit.candidate.presentation;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.util.SqlTimestampConverter;
import com.recruit.candidate.service.Candidate;

public class CandidateParser {

	public static List<Candidate> parseJSON(String json){
        List<Candidate> candidates = new ArrayList<Candidate>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	Candidate candidate = new Candidate();
            	if(rObj.has("id")){
            		candidate.setId(rObj.getInt("id"));     
            	}
            	if(rObj.has("created") && !rObj.getString("created").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		candidate.setCreated((Timestamp)c.fromString(rObj.getString("created")));
            	}
            	if(rObj.has("lastUpdated") && !rObj.getString("lastUpdated").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		candidate.setLastUpdated((Timestamp)c.fromString(rObj.getString("lastUpdated")));
            	}
            	if(rObj.has("availability") && !rObj.getString("availability").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		candidate.setAvailability((Timestamp)c.fromString(rObj.getString("availability")));
            	}
            	if(rObj.has("submittedDate") && !rObj.getString("submittedDate").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		candidate.setSubmittedDate((Timestamp)c.fromString(rObj.getString("submittedDate")));
            	}
            	if(rObj.has("startDate") && !rObj.getString("startDate").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		candidate.setStartDate((Timestamp)c.fromString(rObj.getString("startDate")));
            	}
            	if (rObj.has("p1comments")) {
					candidate.setP1comments(rObj.getString("p1comments"));
				}
            	if (rObj.has("p2comments")) {
					candidate.setP2comments(rObj.getString("p2comments"));
				}
            	if (rObj.has("lastUpdatedUser")) {
					candidate.setLastUpdatedUser(rObj.getString("lastUpdatedUser"));
				}
            	if (rObj.has("contactAddress")) {
					candidate.setContactAddress(rObj.getString("contactAddress"));
				}
            	if (rObj.has("contactNumber")) {
					candidate.setContactNumber(rObj.getString("contactNumber"));
				}
            	if (rObj.has("currentJobTitle")) {
					candidate.setCurrentJobTitle(rObj.getString("currentJobTitle"));
				}
            	if (rObj.has("emailId")) {
					candidate.setEmailId(rObj.getString("emailId"));
				}
            	if (rObj.has("firstName")) {
					candidate.setFirstName(rObj.getString("firstName"));
				}
            	if (rObj.has("lastName")) {
					candidate.setLastName(rObj.getString("lastName"));
				}
            	if (rObj.has("source")) {
					candidate.setSource(rObj.getString("source"));
				}
            	if (rObj.has("priority")) {
					candidate.setPriority(rObj.getInt("priority"));
				}
            	if (rObj.has("highestQualification")) {
					candidate.setHighestQualification(rObj.getString("highestQualification"));
				}
            	if (rObj.has("employer")) {
					candidate.setEmployer(rObj.getString("employer"));
				}
            	if (rObj.has("skillSet")) {
					candidate.setSkillSet(rObj.getString("skillSet"));
				}
            	if (rObj.has("referral")) {
					candidate.setReferral(rObj.getString("referral"));
				}
            	if (rObj.has("marketingStatus")) {
					candidate.setMarketingStatus(rObj.getString("marketingStatus"));
				}
            	if (rObj.has("type")) {
					candidate.setType(rObj.getString("type"));
				}
            	if (rObj.has("relocate")) {
					candidate.setRelocate(rObj.getBoolean("relocate"));
				}
            	if (rObj.has("travel")) {
					candidate.setTravel(rObj.getBoolean("travel"));
				}
            	if (rObj.has("goodCandidateForRemote")) {
					candidate.setGoodCandidateForRemote(rObj.getBoolean("goodCandidateForRemote"));
				}
            	if (rObj.has("cityAndState")) {
					candidate.setCityAndState(rObj.getString("cityAndState"));
				}
            	if (rObj.has("followUp")) {
					candidate.setFollowUp(rObj.getString("followUp"));
				}
            	if (rObj.has("education")) {
					candidate.setEducation(rObj.getString("education"));
				}
            	if (rObj.has("immigrationStatus")) {
					candidate.setImmigrationStatus(rObj.getString("immigrationStatus"));
				}
            	if (rObj.has("openToCTH")) {
					candidate.setOpenToCTH(rObj.getString("openToCTH"));
				}
            	if (rObj.has("employmentType")) {
					candidate.setEmploymentType(rObj.getString("employmentType"));
				}
            	if (rObj.has("expectedRate")) {
					candidate.setExpectedRate(rObj.getString("expectedRate"));
				}
            	if (rObj.has("aboutPartner")) {
					candidate.setAboutPartner(rObj.getString("aboutPartner"));
				}
            	if (rObj.has("personality")) {
					candidate.setPersonality(rObj.getString("personality"));
				}
            	if (rObj.has("alert")) {
					candidate.setAlert(rObj.getString("alert"));
				}
            	if (rObj.has("confidentiality")) {
					candidate.setConfidentiality(rObj.getInt("confidentiality"));
				}
            	if (rObj.has("employeeId")) {
					candidate.setEmployeeId(rObj.getInt("employeeId"));
				}
            	if(rObj.has("rateFrom")){
            		Double d = rObj.getDouble("rateFrom");
            		candidate.setRateFrom(new BigDecimal(d));
            	}
            	if(rObj.has("rateTo")){
            		Double d = rObj.getDouble("rateTo");
            		candidate.setRateTo(new BigDecimal(d));
            	}
            	if (rObj.has("location")) {
					candidate.setLocation(rObj.getString("location"));
				}
            	if (rObj.has("immigrationVerified")) {
					candidate.setImmigrationVerified(rObj.getString("immigrationVerified"));
				}
            	if (rObj.has("communication")) {
					candidate.setCommunication(rObj.getString("communication"));
				}
            	if (rObj.has("experience")) {
					candidate.setExperience(rObj.getString("experience"));
				}
            	if (rObj.has("targetRoles")) {
					candidate.setTargetRoles(rObj.getString("targetRoles"));
				}
            	if (rObj.has("targetSkillSet")) {
					candidate.setTargetSkillSet(rObj.getString("targetSkillSet"));
				}
            	if (rObj.has("currentRoles")) {
					candidate.setCurrentRoles(rObj.getString("currentRoles"));
				}
            	if (rObj.has("resumeHelp")) {
					candidate.setResumeHelp(rObj.getString("resumeHelp"));
				}
            	if (rObj.has("interviewHelp")) {
					candidate.setInterviewHelp(rObj.getString("interviewHelp"));
				}
            	if (rObj.has("jobHelp")) {
					candidate.setJobHelp(rObj.getString("jobHelp"));
				}
            	if (rObj.has("resumeHelpComments")) {
					candidate.setResumeHelpComments(rObj.getString("resumeHelpComments"));
				}
            	if (rObj.has("interviewHelpComments")) {
					candidate.setInterviewHelpComments(rObj.getString("interviewHelpComments"));
				}
            	if (rObj.has("jobHelpComments")) {
					candidate.setJobHelpComments(rObj.getString("jobHelpComments"));
				}
            	if (rObj.has("oldSkillSet")) {
            		candidate.setOldSkillSet(rObj.getString("oldSkillSet"));
				}
            	if (rObj.has("otherSkillSet")) {
					candidate.setOtherSkillSet(rObj.getString("otherSkillSet"));
				}
            	if (rObj.has("version")) {
					candidate.setVersion(rObj.getInt("version"));
				}
            	if (rObj.has("gender")) {
					candidate.setGender(rObj.getString("gender"));
				}
            	candidates.add(candidate);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return candidates;
	}

}
