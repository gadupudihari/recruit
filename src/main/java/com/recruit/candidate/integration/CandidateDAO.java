package com.recruit.candidate.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.service.Candidate;


/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class CandidateDAO {

	private static final Log log = LogFactory.getLog(CandidateDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Candidate transientInstance) {
		log.debug("persisting Candidate instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Candidate instance) {
		log.debug("attaching dirty Candidate instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Candidate persistentInstance) {
		log.debug("deleting Candidate instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Candidate merge(Candidate detachedInstance) {
		log.debug("merging Candidate instance");
		try {
			Candidate result = (Candidate) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Candidate findById(java.lang.Integer id) {
		log.debug("getting Candidate instance with id: " + id);
		try {
			Candidate instance = (Candidate) sessionFactory.getCurrentSession()
					.get("com.recruit.candidate.service.Candidate", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Candidate> findByExample(Candidate instance) {
		log.debug("finding Candidate instance by example");
		try {
			List<Candidate> results = (List<Candidate>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.candidate.service.Candidate").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Candidate> findByCriteria(SearchFilter filter, String loginId) {
		try {
			String query = "select *," +
					"(select group_concat(s.name SEPARATOR ' -- ') from recruit_certification c,recruit_settings s where s.id=c.settingsId and candidateId=t.id) allCertifications," +
					"(select group_concat(id) from recruit_contacts where candidateId=t.id and candidateDeleted = false) contactId," +
					"(select group_concat(skillSetId) from recruit_candidate_skillset where candidateId=t.id ) skillSetIds," +
					"(select group_concat(skillSetId) from recruit_candidate_targetskillset where candidateId=t.id ) targetSkillSetIds," +
					"(select group_concat(roleSetId) from recruit_candidate_roleset where candidateId=t.id ) roleSetIds," +
					"(select group_concat(skillsetgroupId) from recruit_candidate_skillsetgroup where candidateId=t.id ) skillSetGroupIds" +
					" from recruit_candidate t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			String paramQuery = "";
			if (map.get("id") != null) {
				if (map.get("id").getStrValue().contains(",")) 
					query += " and id in(" +map.get("id").getStrValue()+ ")";
				else
					query += " and id like '" +map.get("id").getStrValue()+ "'";
			}
			if (map.get("type") != null) {
				paramQuery += " and type like '"+map.get("type").getStrValue()+"'";
			}
			if (map.get("marketingStatus") != null) {
				if (map.get("marketingStatus").getStrValue().contains(",")) 
					paramQuery += " and marketingStatus  in(" +map.get("marketingStatus").getStrValue()+ ")";
				else
					paramQuery += " and marketingStatus  like '%" +map.get("marketingStatus").getStrValue()+ "%'";
			}
			if (map.get("priority") != null) {
				if (map.get("priority").getStrValue().contains(",")) 
					paramQuery += " and priority  in(" +map.get("priority").getStrValue()+ ")";
				else
					paramQuery += " and priority  like '" +map.get("priority").getStrValue()+ "'";
			}
			if (map.get("priorityUpto") != null) {
				paramQuery += " and priority <= '"+map.get("priorityUpto").getStrValue()+"'";
			}
			if (map.get("availability") != null) {
				paramQuery += " and availability like '"+map.get("availability").getStrValue()+"'" ;
			}
			if (map.get("relocate") != null) {
				if (map.get("relocate").getStrValue().equalsIgnoreCase("YES")) {
					paramQuery += " and relocate = true" ;
				}else if (map.get("relocate").getStrValue().equalsIgnoreCase("NO")) {
					paramQuery += " and relocate = false" ;	
				}else
					paramQuery += " and relocate ="+map.get("relocate").getStrValue() ;
			}
			if (map.get("travel") != null) {
				if (map.get("travel").getStrValue().equalsIgnoreCase("YES")) {
					paramQuery += " and travel = true" ;
				}else if (map.get("travel").getStrValue().equalsIgnoreCase("NO")) {
					paramQuery += " and travel = false" ;	
				}else
					paramQuery += " and travel ="+map.get("travel").getStrValue() ;
			}
			if (map.get("referral") != null) {
				if (map.get("referral").getStrValue().charAt(0) == '-'){
					if(map.get("referral").getStrValue().equalsIgnoreCase("-N/A"))
						paramQuery += " and referral not in ('N/A','')" ;
					else
					paramQuery += " and referral not like '%"+map.get("referral").getStrValue().substring(1)+"%'" ;
				}else
					paramQuery += " and referral like '%"+map.get("referral").getStrValue()+"%'" ;
			}
			if (map.get("source") != null) {
				if (map.get("source").getStrValue().charAt(0)== '-'){
					if(map.get("source").getStrValue().equalsIgnoreCase("-N/A"))
						paramQuery += " and source not in ('N/A','')" ;
					else
						paramQuery += " and source not like '%"+map.get("source").getStrValue().substring(1)+"%'" ;
				}else
					paramQuery += " and source like '%"+map.get("source").getStrValue()+"%'" ;
			}
			if (map.get("candidateId") != null) {
				paramQuery += " and id in ("+map.get("candidateId").getStrValue()+")" ;
			}
			if (map.get("availabilityDateFrom") != null) {
				paramQuery += " and date(availability) >= '"+map.get("availabilityDateFrom").getStrValue()+"'";
			}
			if (map.get("availabilityDateTo") != null) {
				paramQuery += " and date(availability) <= '"+map.get("availabilityDateTo").getStrValue()+"'";
			}
			if (loginId != null) {
				paramQuery += " and (NOT FIND_IN_SET(confidentiality,(select if(excludeConfidential is null,'',excludeConfidential) from recruit_authority where loginId='"+loginId+"')) " +
						"or confidentiality is null or marketingStatus = '1. Active')";
			}
			if (map.get("confidentiality") != null) {
				paramQuery += " and confidentiality like '"+map.get("confidentiality").getStrValue()+"'";
			}
			if (map.get("vendorId") != null) {
				paramQuery += " and id in (select candidateId from recruit_sub_requirements where deleted = false and submissionStatus != 'Shortlisted' and vendorId="+map.get("vendorId").getStrValue()+")";
			}
			if (map.get("clientId") != null) {
				paramQuery += " and id in (select s.candidateId FROM recruit_requirements r,recruit_sub_requirements s where s.deleted = false and s.submissionStatus != 'Shortlisted' and r.id=s.requirementId and r.clientId="+map.get("clientId").getStrValue()+")";
			}
			if (map.get("certifications") != null) {
				paramQuery += " and id in (select candidateId FROM recruit_certification where name='"+map.get("certifications").getStrValue()+"')";
			}
			if (map.get("skillSetId") != null) {
				paramQuery += " and id in (select candidateId from recruit_candidate_skillset where skillSetId= '"+map.get("skillSetId").getStrValue()+"')";
			}
			if (map.get("skillSetGroupId") != null) {
				paramQuery += " and id in (select candidateId FROM recruit_candidate_skillsetgroup where skillSetGroupId='"+map.get("skillSetGroupId").getStrValue()+"')";
			}
			if (map.get("cityAndState") != null) {
				paramQuery += " and cityAndState like '%"+map.get("cityAndState").getStrValue()+"%'";
			}
			if (map.get("gender") != null) {
				query += " and gender like '"+map.get("gender").getStrValue()+"'";
			}
			if (map.get("resumeHelp") != null) {
				query += " and resumeHelp like '"+map.get("resumeHelp").getStrValue()+"'";
			}
			
			if (map.get("search") != null) {
				String concatString = "concat_ws (firstName,lastName,emailId,contactNumber,contactAddress,priority,currentJobTitle,type,p1comments,p2comments,date_format(created,'%m/%d/%Y')," +
						"date_format(lastUpdated,'%m/%d/%Y'),lastUpdatedUser,employer,highestQualification,referral,marketingStatus,date_format(availability,'%m/%d/%Y')," +
						"source,relocate,travel,cityAndState,followUp,education,immigrationStatus,openToCTH,date_format(startDate,'%m/%d/%Y'),employmentType,expectedRate,aboutPartner," +
						"personality,date_format(submittedDate,'%m/%d/%Y'),alert,confidentiality,rateFrom,rateTo,location," +
						"immigrationVerified,communication,experience,targetRoles,currentRoles,resumeHelp,interviewHelp,jobHelp," +
						"resumeHelpComments,interviewHelpComments,jobHelpComments,resumeLink) ";
				
				String searchQuery ="";
				if (map.get("search").getStrValue().charAt(0)== '-')
					searchQuery += concatString +" not like '%"+map.get("search").getStrValue().substring(1)+"%'";
				else
					searchQuery += concatString +" like '%"+map.get("search").getStrValue()+"%'";
				
				String words[] = map.get("search").getStrValue().split(" ");
				if (words.length > 1) {
					for (int i = 0; i < words.length; i++) {
						searchQuery += " OR "+concatString+" like '%"+words[i]+"%'";
					}
				}
				
				String synonym = getSynonym(map.get("search").getStrValue());
				if (synonym != null) {
					String synonyms[] = synonym.split(",");
					for (int i = 0; i < words.length; i++) {
						searchQuery += " OR "+concatString+" like '%"+synonyms[i]+"%'";
					}
				}
				
				paramQuery += " and ("+searchQuery+")";
			}
			query = query +paramQuery+ " order by marketingStatus,priority ";
			
			System.out.println("Candidate : "+query);
			List<Candidate> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Candidate.class))
					.setMaxResults(filter.getPageSize())
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public String getSynonym(String word){
		try {
			String query = "select group_concat(synonyms) from recruit_synonym t where name ='"+word+"'";
			
			List<String> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			
			if (list.size() > 0 && list.get(0) != null && !list.get(0).equalsIgnoreCase("")) {
				return list.get(0);
			}
			return null;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Candidate> getAllCandidates(String loginId, SearchFilter filter) {
		try {
			/*String query = "select *,(select group_concat(name SEPARATOR ' -- ') from recruit_certification where candidateId=t.id) allCertifications " +
			"from recruit_candidate t where 1=1 ";*/

			String query = "select id,firstName,lastName,employeeId,emailId,priority,relocate,travel,availability,goodCandidateForRemote," +
					"cityAndState,followUp,immigrationStatus,immigrationVerified,openToCTH,startDate,expectedRate,alert,location,targetRoles," +
					"currentRoles,resumeHelp,interviewHelp,jobHelp,gender,marketingStatus,experience,communication,source,referral,p1comments,p2comments,technicalScore," +
					"(select group_concat(s.name SEPARATOR ' -- ') from recruit_certification c,recruit_settings s where s.id=c.settingsId " +
					"and candidateId=t.id) allCertifications," +
					"(select group_concat(skillSetId) from recruit_candidate_skillset where candidateId=t.id ) skillSetIds," +
					"(select group_concat(skillSetId) from recruit_candidate_targetskillset where candidateId=t.id ) targetSkillSetIds," +
					"(select group_concat(roleSetId) from recruit_candidate_roleset where candidateId=t.id ) roleSetIds," +
					"(select group_concat(skillsetgroupId) from recruit_candidate_skillsetgroup where candidateId=t.id ) skillSetGroupIds " +
					" from recruit_candidate t where 1=1 ";
			
			Map<String, Param> map = filter.getParamMap();
			if (map.get("candidateIds") != null ) {
				query += " and t.id in ("+map.get("candidateIds").getStrValue()+")";
			}
			
			if (map.get("filterPotential") != null) {
				if (map.get("marketingStatus") != null)
					query += " and marketingStatus in('1. Active','2. Yet to Start')";
				
				String requirementId = map.get("requirementId").getStrValue();
				//client or client all and module
				if (map.get("filterPotential").getStrValue().equalsIgnoreCase("client") || map.get("filterPotential").getStrValue().equalsIgnoreCase("clientAll")){
					query += " and id in (select candidateId from recruit_sub_requirements where submissionStatus != 'Shortlisted' and deleted = false " +
							"and requirementId in (select id from recruit_requirements where clientId =" +
							"(select clientId from recruit_requirements t,recruit_clients c where c.id=t.clientId and t.id ="+requirementId+")" +
							"and matchingCount(concat(module,',',secondaryModule) ,(select concat(module,',',secondaryModule) from recruit_requirements where id ="+requirementId+")) !=0 ))";
					
				}else if (map.get("filterPotential").getStrValue().equalsIgnoreCase("location")){
					//location
					query += " and location = (select location from recruit_requirements where id ="+requirementId+") ";

				}else if (map.get("filterPotential").getStrValue().equalsIgnoreCase("certification")){
					//certifications
					query += " and id in(select candidateId from recruit_certification where " +
							"matchingCount(name,(select certifications from recruit_requirements where id ="+requirementId+")) != 0)";

				}else if (map.get("filterPotential").getStrValue().equalsIgnoreCase("vendor")){
					//vendor, module
					query += " and id in (select c.id from recruit_candidate c,recruit_sub_requirements s,recruit_requirements r " +
							"where s.submissionStatus != 'Shortlisted' and s.deleted = false and c.id=s.candidateId " +
							"and r.id=s.requirementId and s.vendorId in (select vendorId from recruit_sub_requirements where deleted = false and requirementId="+requirementId+")" +
							"and matchingCount(concat(r.module,',',r.secondaryModule),(select concat(module,',',secondaryModule) from recruit_requirements where id ="+requirementId+")) !=0 ) ";
					
				}else if (map.get("filterPotential").getStrValue().equalsIgnoreCase("project")){
					query += " and id in (select e.candidateId from project p,billingrate b,employeebillingrate eb,employee e " +
							" where p.id=b.projectId and b.id=eb.billingRateId and e.id=eb.employeeId and e.candidateId is not null " +
							" and p.clientId=(select clientId from recruit_requirements where id ="+requirementId+") " +
							" and matchingCount(p.module,(select concat(module,',',secondaryModule) from recruit_requirements where id ="+requirementId+")) !=0)";
				}
				
			}else{
				if (map.get("marketingStatus") != null) {
					if (map.get("marketingStatus").getStrValue().contains(",")) 
						query += " and marketingStatus  in(" +map.get("marketingStatus").getStrValue()+ ")";
					else
						query += " and marketingStatus  like '%" +map.get("marketingStatus").getStrValue()+ "%'";
				}
				if (map.get("RelocateOrTravel") != null && map.get("RelocateOrTravel").getStrValue().equals("true")) {
					query += " and (relocate = true or travel = true )";
				}
			}
			if (map.get("gender") != null) {
				query += " and gender like '"+map.get("gender").getStrValue()+"'";
			}
			if (map.get("resumeHelp") != null) {
				query += " and resumeHelp like '"+map.get("resumeHelp").getStrValue()+"'";
			}

			query += " order by firstName ";

			System.out.println("Candidate : "+query);
			List<Candidate> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Candidate.class))
					.setMaxResults(filter.getPageSize())
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	public void giveOnboarding(int candidateId) {
		try {
			String query = "insert into user (firstName,lastName,emailId,active) select firstName,lastName,emailId,true from recruit_candidate where id="+candidateId;
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
			
			query = "insert into employee (userId,candidateId,cellPhone,currentImmigrationStatus,created,userName) " +
					"select (select id from user u where u.firstName=c.firstName and u.lastName=c.lastName),id,contactNumber,immigrationStatus,now(),'Recruit' " +
					"from recruit_candidate c where id="+candidateId;
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
			
			query = "update recruit_candidate set employeeId = (select id from employee where candidateId="+candidateId+") where id="+candidateId;
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
						
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Candidate> getAllContractors() {
		try {
			String query = "select id,concat(firstName,' ',lastName) from recruit_candidate t order by firstName ";
			
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Candidate> findByIds(String candidateIds) {
		try {
			String query = "select * from recruit_candidate t where 1=1 and id in ("+candidateIds+")";

			List<Candidate> list = sessionFactory.getCurrentSession().createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Candidate.class)).list(); 
			
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Candidate findCandidateName(Integer candidateId) {
		try {
			String query = "select id,firstName,lastName from recruit_candidate t where 1=1 and id in ("+candidateId+")";

			List<Candidate> list = sessionFactory.getCurrentSession().createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Candidate.class)).list(); 
			
			return list.get(0);
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findTNEProjBillingsByCriteria(SearchFilter filter) {
		try {	
			String query = " SELECT projectCode, projectName, vendorName, projectStatus, employer, employeeRate, vendorRate, projectStartDate," +
					" projectEndDate, approxEndDate, invoicingTerms, paymentTerms, projectComments from TNERepProjectBillingRatesView t1 " +
					" where t1.empRateStartDate = (select max(empRateStartDate) from tnerepprojectbillingratesview t2 where t1.projectId=t2.projectid and t1.employeeid=t2.employeeid)";
			
			Map<String, Param> map = filter.getParamMap();
				
			if (map.get("candidateId") != null) {
				query += " and employeeId = (select id from employee where candidateId= "+ map.get("candidateId").getStrValue()+")"  ;
			}
			if (map.get("vendorId") != null) {
				query += " and projectId in (select id from project where r_vendorId =" + map.get("vendorId").getStrValue()+") ";
			}
			if (map.get("clientId") != null) {
				query += " and projectId in (select id from project where clientId =" + map.get("clientId").getStrValue()+") ";
			}
			
			query = query + " order by field(projectStatus,'Active','Inactive','Closed'),projectStartDate desc";
			
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.setMaxResults(filter.getPageSize())
					.list();
			System.out.println(query);
			System.out.println(list.size());
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public void updateEmployee(Candidate candidate) {
		try {
			String query = "update employee set cellPhone='"+candidate.getContactNumber()+"', currentImmigrationStatus='"+candidate.getImmigrationStatus()+"'" +
					" where id="+candidate.getEmployeeId();
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
			
			query = "update user set emailId='"+candidate.getEmailId()+"' where id= (select userid from employee where candidateId="+candidate.getId()+")";
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
						
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public List getSuggestedCriteria(int id) {
		try {
					//location jobOpeningStatus ('01.In-progress','02.Resume Ready')
			String query = "select 'location' type,CAST(count(*) AS CHAR) value,group_concat(id) ids from recruit_requirements where jobOpeningStatus in " +
					"('01.In-progress','02.Resume Ready') and location !='' and location = (select location from recruit_candidate where id = "+id+") " +
					" union " +

					//client all jobOpeningStatus
					"select 'clientAll',count(id),group_concat(id) from recruit_requirements where id not in " +
					"(select requirementId from recruit_sub_requirements where submissionStatus != 'Shortlisted' and deleted = false and candidateId="+id+") " +
					"and clientId in (select r1.clientId from recruit_sub_requirements s1,recruit_requirements r1,recruit_clients c1 " +
					"where s1.submissionStatus != 'Shortlisted' and s1.deleted = false and s1.requirementId = r1.id and r1.clientId=c1.id and c1.name != 'n/a' and s1.candidateId="+id+") and module in (select r1.module " +
					"from recruit_sub_requirements s1,recruit_requirements r1 where s1.submissionStatus != 'Shortlisted' and s1.deleted = false and s1.requirementId=r1.id and s1.candidateId="+id+" and module != '')"+
					" union " +
					
					//client jobOpeningStatus ('01.In-progress','02.Resume Ready')
					"select 'client',count(id),group_concat(id) from recruit_requirements " +
					"where id not in (select requirementId from recruit_sub_requirements where submissionStatus != 'Shortlisted' and deleted = false and candidateId="+id+") " +
					"and clientId in (select r1.clientId from recruit_sub_requirements s1,recruit_requirements r1,recruit_clients c1 " +
					"where s1.submissionStatus != 'Shortlisted' and s1.deleted = false and s1.requirementId = r1.id and r1.clientId=c1.id and c1.name != 'n/a' and s1.candidateId="+id+") and module in (select r1.module " +
					"from recruit_sub_requirements s1,recruit_requirements r1 where s1.submissionStatus != 'Shortlisted' and s1.deleted = false and s1.requirementId=r1.id and s1.candidateId="+id+" and module != '') " +
					"and jobOpeningStatus in ('01.In-progress','02.Resume Ready')"+
					" union " +
					
					//vendor all jobOpeningStatus
					"select 'vendor',count(distinct r.id),group_concat(distinct r.id) from recruit_requirements r,recruit_sub_vendors s " +
					"where s.requirementId=r.id and r.id not in (select requirementId from recruit_sub_requirements where submissionStatus != 'Shortlisted' and deleted = false and candidateId="+id+") " +
					"and s.vendorId in (select s.vendorId from recruit_sub_requirements s,recruit_requirements r where s.submissionStatus != 'Shortlisted' and s.deleted = false and s.candidateId="+id+") and module in (select " +
					"r1.module from recruit_sub_requirements s1,recruit_requirements r1 where s1.submissionStatus != 'Shortlisted' and s1.deleted = false and s1.requirementId = r1.id and s1.candidateId="+id+" )"+
					" union " +
					
					//project and module
					"select 'project',count(distinct id),group_concat(distinct id) from recruit_requirements " +
					"where id not in (select requirementId from recruit_sub_requirements where submissionStatus != 'Shortlisted' and deleted = false and candidateId="+id+") " +
					"and clientId in (select p.clientId from project p,billingrate b,employeebillingrate eb,employee e where p.id=b.projectId and " +
					"b.id=eb.billingRateId and e.id=eb.employeeId and e.candidateId="+id+") and module in (select p.module from project p,billingrate b," +
					"employeebillingrate eb,employee e where p.id=b.projectId and b.id=eb.billingRateId and e.id=eb.employeeId and e.candidateId="+id+" )"+
					" union " +

					//certifications
					"select 'certifications',count(distinct r.id),group_concat(distinct r.id) from recruit_requirements r, recruit_certification c " +
					"where matchingCount(c.name,certifications) != 0 and r.jobOpeningStatus in ('01.In-progress','02.Resume Ready') and c.candidateId="+id ;
			
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public List getNegativeCriteria(SearchFilter filter) {
		try {
			Map<String, Param> map = filter.getParamMap();
			String requirementId = map.get("requirementId").getStrValue();
			String candidateIds = map.get("candidateIds").getStrValue();
			//same client and diffrent module
			String query = "select 'client',group_concat(candidateId),count(candidateId),r.clientId,r.module from recruit_sub_requirements s,recruit_requirements r " +
					"where s.submissionStatus != 'Shortlisted' and s.deleted = false and s.requirementId=r.id and candidateId in ("+candidateIds+") " +
					" and r.id in (select id from recruit_requirements where clientId = " +
					"(select clientId from recruit_requirements t,recruit_clients c where c.id=t.clientId and c.name != 'n/a' and t.id ="+requirementId+") " +
					"and matchingCount((select module from recruit_requirements where id ="+requirementId+"),module) ) = 0 and module != '' " +
					" union " +
					//same vendor and diffrent module
					"select 'vendor',group_concat(distinct concat(c.id,' -- ',s.vendorId,' -- ',module) SEPARATOR ' :: '),count(distinct c.id),s.vendorId,r.module " +
					"from recruit_candidate c,recruit_sub_requirements s,recruit_requirements r where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
					" and c.id=s.candidateId and r.id=s.requirementId " +
					" and s.vendorId in (select vendorId from recruit_sub_vendors where requirementId="+requirementId+") " +
					" and matchingCount((select module from recruit_requirements where id ="+requirementId+"),r.module) = 0 " +
					" and r.id != "+requirementId+" and c.id in ("+candidateIds+") and module != ''" +
					" union " +
					//same vendor and same client
					"select 'Client_vendor',group_concat(distinct concat(c.id,' -- ',s.vendorId,' -- ',r.clientId) SEPARATOR ' :: '),count(distinct c.id),s.vendorId,r.module" +
					" from  recruit_candidate c,recruit_sub_requirements s,recruit_requirements r where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
					" and c.id=s.candidateId and r.id=s.requirementId and r.clientId=" +
					" (select clientId from recruit_requirements t,recruit_clients c where c.id=t.clientId and c.name != 'n/a' and t.id ="+requirementId+") " +
					" and vendorId in (select vendorId from recruit_sub_vendors where requirementId="+requirementId+") " +
					" and c.id in ("+candidateIds+") and r.id != "+requirementId;			
			
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
		} catch (RuntimeException e) {
	log.error("get failed", e);
	throw e;
}
}

}