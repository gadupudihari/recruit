package com.recruit.candidate.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.service.Certification;


/**
 * Home object for domain model class User.
 * @see com.timesheetz.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class CertificationDAO {

	private static final Log log = LogFactory.getLog(CertificationDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Certification transientInstance) {
		log.debug("persisting Certification instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Certification instance) {
		log.debug("attaching dirty Certification instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Certification persistentInstance) {
		log.debug("deleting Certification instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Certification merge(Certification detachedInstance) {
		log.debug("merging Certification instance");
		try {
			Certification result = (Certification) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Certification findById(java.lang.Integer id) {
		log.debug("getting Certification instance with id: " + id);
		try {
			Certification instance = (Certification) sessionFactory.getCurrentSession()
					.get("com.recruit.candidate.service.Certification", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Certification> findByExample(Certification instance) {
		log.debug("finding Certification instance by example");
		try {
			List<Certification> results = (List<Certification>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.candidate.service.Certification").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Certification> findByEmployeeId(int employeeId) {
		try {
			List<Certification> results = (List<Certification>) sessionFactory					
					.getCurrentSession()
					.createQuery(" from Certification where employee.id = :_eId")
					.setParameter("_eId",employeeId)
					.list();
			return results;
		} catch (RuntimeException re) {
			log.error("find by employee Id failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Certification> findByCriteria(SearchFilter filter) {
		try {
			String query = "from Certification t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("candidateId") != null) {
				query += " and t.candidate.id like '" + map.get("candidateId").getStrValue()+ "'";
			}
			if (map.get("candidateName") != null) {
				query += " and concat(t.candidate.firstName,' ',t.candidate.lastName) like '" + map.get("candidateName").getStrValue()+ "'";
			}
			System.out.println("certification : "+query);
			List<Certification> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}
	
}