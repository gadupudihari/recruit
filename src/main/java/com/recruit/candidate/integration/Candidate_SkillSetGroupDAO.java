package com.recruit.candidate.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.service.Candidate_SkillSetGroup;

/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class Candidate_SkillSetGroupDAO {

	private static final Log log = LogFactory.getLog(Candidate_SkillSetGroupDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Candidate_SkillSetGroup  transientInstance) {
		log.debug("persisting Candidate_SkillSetGroup instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Candidate_SkillSetGroup instance) {
		log.debug("attaching dirty Candidate_SkillSetGroup instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Candidate_SkillSetGroup persistentInstance) {
		log.debug("deleting Candidate_SkillSetGroup instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Candidate_SkillSetGroup merge(Candidate_SkillSetGroup detachedInstance) {
		log.debug("merging Candidate_SkillSetGroup instance");
		try {
			Candidate_SkillSetGroup result = (Candidate_SkillSetGroup) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Candidate_SkillSetGroup findById(java.lang.Integer id) {
		log.debug("getting Candidate_SkillSetGroup instance with id: " + id);
		try {
			Candidate_SkillSetGroup instance = (Candidate_SkillSetGroup) sessionFactory.getCurrentSession()
					.get("com.recruit.candidate.service.Candidate_SkillSetGroup", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Candidate_SkillSetGroup> findByExample(Candidate_SkillSetGroup instance) {
		log.debug("finding Requirement instance by example");
		try {
			List<Candidate_SkillSetGroup> results = (List<Candidate_SkillSetGroup>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.candidate.service.Candidate_SkillSetGroup").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findByCriteria(SearchFilter filter) {
		try {
			String query = "select distinct s.* from recruit_candidate_skillsetgroup s where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("candidateId") != null) {
				query += " and s.candidateId like '"+map.get("candidateId").getStrValue()+"'";
			}

			System.out.println("Research : "+query);
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	public void deleteByCandidate(Integer candidateId) {
		try {
			String query = "delete from recruit_candidate_skillsetgroup where candidateId = "+candidateId;
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
		} catch (RuntimeException re) {
			log.error("find by employee Id failed", re);
			throw re;
		}
	}

}