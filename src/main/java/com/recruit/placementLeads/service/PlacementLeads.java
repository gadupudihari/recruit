package com.recruit.placementLeads.service;


import java.sql.Timestamp;

public class PlacementLeads implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer priority;
	private Timestamp date;
	private String person;
	private String sourceOfInformation;
	private Timestamp source_Sublevel;
	private String typeOfInformation;
	private String information;
	private String hospital_Entity;
	private String location;
	private String liveDate;
	private String link1;
	private String link2;
	private String outreach;
	private Timestamp created;
	private Timestamp lastUpdated;
	private String lastUpdatedUser;
	private String comments;
	private String state;
	
	public PlacementLeads() {}
	
	public PlacementLeads(Integer id, Integer priority, Timestamp date,String person, String sourceOfInformation, Timestamp source_Sublevel,String typeOfInformation, String information,
			String hospital_Entity, String location, String liveDate,String link1, String link2, String outreach, Timestamp created,Timestamp lastUpdated, String lastUpdatedUser,String comments,
			String state) {
		this.id = id;
		this.priority = priority;
		this.date = date;
		this.person = person;
		this.sourceOfInformation = sourceOfInformation;
		this.source_Sublevel = source_Sublevel;
		this.typeOfInformation = typeOfInformation;
		this.information = information;
		this.hospital_Entity = hospital_Entity;
		this.location = location;
		this.liveDate = liveDate;
		this.link1 = link1;
		this.link2 = link2;
		this.outreach = outreach;
		this.created = created;
		this.lastUpdated = lastUpdated;
		this.lastUpdatedUser = lastUpdatedUser;
		this.comments = comments;
		this.state = state;
	}

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public String getSourceOfInformation() {
		return sourceOfInformation;
	}

	public void setSourceOfInformation(String sourceOfInformation) {
		this.sourceOfInformation = sourceOfInformation;
	}

	public Timestamp getSource_Sublevel() {
		return source_Sublevel;
	}

	public void setSource_Sublevel(Timestamp source_Sublevel) {
		this.source_Sublevel = source_Sublevel;
	}

	public String getTypeOfInformation() {
		return typeOfInformation;
	}

	public void setTypeOfInformation(String typeOfInformation) {
		this.typeOfInformation = typeOfInformation;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getHospital_Entity() {
		return hospital_Entity;
	}

	public void setHospital_Entity(String hospital_Entity) {
		this.hospital_Entity = hospital_Entity;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLiveDate() {
		return liveDate;
	}

	public void setLiveDate(String liveDate) {
		this.liveDate = liveDate;
	}

	public String getLink1() {
		return link1;
	}

	public void setLink1(String link1) {
		this.link1 = link1;
	}

	public String getLink2() {
		return link2;
	}

	public void setLink2(String link2) {
		this.link2 = link2;
	}

	public String getOutreach() {
		return outreach;
	}

	public void setOutreach(String outreach) {
		this.outreach = outreach;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "PlacementLeads [id=" + id + ", priority=" + priority + ", date=" + date + ", person=" + person + ", sourceOfInformation=" + sourceOfInformation
				+ ", source_Sublevel=" + source_Sublevel + ", typeOfInformation=" + typeOfInformation + ", information=" + information + ", hospital_Entity=" + hospital_Entity
				+ ", location=" + location + ", liveDate=" + liveDate + ", link1=" + link1 + ", link2=" + link2 + ", outreach=" + outreach + ", created=" + created 
				+ ", lastUpdated=" + lastUpdated + ", lastUpdatedUser=" + lastUpdatedUser + ", comments=" + comments + ", state=" + state 
				+ "]";
	}



}