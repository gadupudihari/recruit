package com.recruit.placementLeads.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.recruit.base.service.SearchFilter;
import com.recruit.placementLeads.integration.PlacementLeadsDAO;

@Service
public class PlacementLeadsService {

	@Autowired
	PlacementLeadsDAO placementLeadsDAO;
	
	@Transactional
	public List<PlacementLeads> getplacementLeadss(SearchFilter filter) {
		return placementLeadsDAO.findByCriteria(filter);
	}

	@Transactional
	public void savePlacements(List<PlacementLeads> list, String loginId,Timestamp createdDate) {
		for (PlacementLeads placementLeads  : list) {
			placementLeads.setLastUpdated(createdDate);
			placementLeads.setLastUpdatedUser(loginId);
			if (placementLeads.getId() == null)
				placementLeads.setCreated(createdDate);
			placementLeadsDAO.attachDirty(placementLeads);
		}
	}

	@Transactional
	public void deletePlacement(Integer id) {
		PlacementLeads placementLeads = placementLeadsDAO.findById(id);
		placementLeadsDAO.delete(placementLeads);
	}

	@Transactional
	public void savePlacement(PlacementLeads placement) {
		placementLeadsDAO.persist(placement);
	}

}
