package com.recruit.placementLeads.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.placementLeads.service.PlacementLeads;


/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class PlacementLeadsDAO {

	private static final Log log = LogFactory.getLog(PlacementLeadsDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(PlacementLeads transientInstance) {
		log.debug("persisting PlacementLeads instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(PlacementLeads instance) {
		log.debug("attaching dirty PlacementLeads instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(PlacementLeads persistentInstance) {
		log.debug("deleting PlacementLeads instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public PlacementLeads merge(PlacementLeads detachedInstance) {
		log.debug("merging PlacementLeads instance");
		try {
			PlacementLeads result = (PlacementLeads) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public PlacementLeads findById(java.lang.Integer id) {
		log.debug("getting PlacementLeads instance with id: " + id);
		try {
			PlacementLeads instance = (PlacementLeads) sessionFactory.getCurrentSession()
					.get("com.recruit.placementLeads.service.PlacementLeads", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<PlacementLeads> findByExample(PlacementLeads instance) {
		log.debug("finding PlacementLeads instance by example");
		try {
			List<PlacementLeads> results = (List<PlacementLeads>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.placementLeads.service.PlacementLeads").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<PlacementLeads> findByCriteria(SearchFilter filter) {
		try {
			String query = "from PlacementLeads t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();
			
			if (map.get("priority") != null) {
				query += " and priority = '"+map.get("priority").getStrValue()+"'";
			}
			if (map.get("typeOfInformation") != null) {
				query += " and typeOfInformation like '%"+map.get("typeOfInformation").getStrValue()+"%'";
			}
			if (map.get("state") != null) {
				query += " and state like '"+map.get("state").getStrValue()+"'";
			}
			
			if (map.get("search") != null) {
				query +=" and Concat( CASE WHEN priority is null THEN '' ELSE priority END,' ', " +
							"CASE WHEN date is null THEN '' ELSE date END,' ', " +
							"CASE WHEN person is null THEN '' ELSE person END,' '," +
							"CASE WHEN sourceOfInformation is null THEN '' ELSE sourceOfInformation END,' '," +
							"CASE WHEN source_Sublevel is null THEN '' ELSE source_Sublevel END,' '," +
							"CASE WHEN typeOfInformation is null THEN '' ELSE typeOfInformation END,' '," +
							"CASE WHEN information is null THEN '' ELSE information END,' '," +
							"CASE WHEN hospital_Entity is null THEN '' ELSE hospital_Entity END,' '," +
							"CASE WHEN location is null THEN '' ELSE location END,' '," +
							"CASE WHEN liveDate is null THEN '' ELSE liveDate END,' '," +
							"CASE WHEN link1 is null THEN '' ELSE link1 END,' '," +
							"CASE WHEN link2 is null THEN '' ELSE link2 END,' '," +
							"CASE WHEN outreach is null THEN '' ELSE link1 END,' '," +
							"CASE WHEN created is null THEN '' ELSE created END,' '," +
							"CASE WHEN lastUpdated is null THEN '' ELSE lastUpdated END,' '," +
							"CASE WHEN lastUpdatedUser is null THEN '' ELSE lastUpdatedUser END,' '," +
							"CASE WHEN state is null THEN '' ELSE state END,' '," +
							"CASE WHEN comments is null THEN '' ELSE comments END" +
						") like '%"+map.get("search").getStrValue()+"%'";
			}

			query += "order by date desc";
			
			System.out.println("PlacementLeads : "+query);
			List<PlacementLeads> list = sessionFactory.getCurrentSession()
					.createQuery(query).setMaxResults(filter.getPageSize())
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}
	
}