package com.recruit.placementLeads.presentation;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.recruit.base.presentation.BaseController;
import com.recruit.base.service.Param;
import com.recruit.base.service.ParamParser;
import com.recruit.base.service.SearchFilter;
import com.recruit.placementLeads.presentation.PlacementLeadsParser;
import com.recruit.placementLeads.service.PlacementLeads;
import com.recruit.placementLeads.service.PlacementLeadsService;
import com.recruit.util.JSONException;
import com.recruit.util.JSONParser;
import com.recruit.util.JsonParserUtility;
import com.recruit.util.UtilityHelper;
import com.recruit.util.WriteExcel;

@Controller
@RequestMapping("/placementLeads")
@PreAuthorize("hasAnyRole('ADMIN','RECRUITER')")
public class PlacementLeadsController extends BaseController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	PlacementLeadsService placementLeadsService;

	@Autowired
	WriteExcel writeExcel;

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	
	@RequestMapping(value = "/getPlacementLeads", method = {RequestMethod.POST,RequestMethod.GET })
	public String getPlacementLeads(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<PlacementLeads> leads = placementLeadsService.getplacementLeadss(filter);
		return returnJson(map, JsonParserUtility.toPlacementJSON(leads).toString());
	}

	@RequestMapping(value = "/savePlacementLeads",  method = {RequestMethod.POST,RequestMethod.GET })
	public String savePlacementLeads(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		List<PlacementLeads> leads = PlacementLeadsParser.parseJSON(json);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		placementLeadsService.savePlacements(leads, loginId, createdDate);
		response.setSuccess(Boolean.TRUE);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/deletePlacement/{id}", method = RequestMethod.GET)
	public String deletePlacement(@PathVariable("id") Integer id, Map<String, Object> map){
		placementLeadsService.deletePlacement(id);
		response.setSuccess(true);
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/savePlacement",  method = {RequestMethod.POST,RequestMethod.GET })
	public String savePlacement(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		PlacementLeads placement =  (PlacementLeads) JSONParser.parseJSON(json, PlacementLeads.class);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@"))
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		placement.setLastUpdated(createdDate);
		placement.setLastUpdatedUser(loginId);
		if (placement.getId() == null) 
			placement.setCreated(createdDate);
		
		placementLeadsService.savePlacement(placement);
		response.setSuccess(Boolean.TRUE);
		response.setReturnVal(placement);
		return returnJson(map, response);
	}

	
	@RequestMapping(value = "/placementsExcelExport", method = {RequestMethod.POST,RequestMethod.GET })
	public String placementsExcelExport(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request,HttpServletResponse res) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<PlacementLeads> leads = placementLeadsService.getplacementLeadss(filter);
		Map<String, Param> filterMap = filter.getParamMap();
		
		String columns[] = filterMap.get("columns").getStrValue().split(",");
		String contextPath="", fileName = "Placement Leads";
		  try{
		      ServletContext servletContext = session.getServletContext();
		      contextPath = servletContext.getRealPath(File.separator);
		      System.out.println("File system context path (in TestFilter): " + contextPath);
		      
		      UtilityHelper.deleteExcelFiles(contextPath, "Placement Leads");
		      
		      writeExcel.setOutputFile(contextPath+fileName+".xls");
		      //Writing to Excel File
			  writeExcel.writePlacements(leads,columns);
			  System.out.println("Download Path ------ " + contextPath+fileName+".xls");
		  }
		  catch(Exception ex){
			  System.err.println("---------- **** Exception while generating Excel file **** ---------");
			  System.err.println(ex.getMessage());
			  ex.printStackTrace();
			  response.setSuccess(false);
		  }
		  response.setSuccess(true);
		  response.setReturnVal(fileName+".xls");
		  return returnJson(map, response);
	}

}