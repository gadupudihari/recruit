package com.recruit.placementLeads.presentation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.placementLeads.service.PlacementLeads;
import com.recruit.util.SqlTimestampConverter;

public class PlacementLeadsParser {

	public static List<PlacementLeads> parseJSON(String json){
        List<PlacementLeads> list = new ArrayList<PlacementLeads>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	PlacementLeads placementLeads = new PlacementLeads();
            	if(rObj.has("id")){
            		placementLeads.setId(rObj.getInt("id"));     
            	}
            	if(rObj.has("date") && !rObj.getString("date").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		placementLeads.setDate((Timestamp)c.fromString(rObj.getString("date")));
            	}
            	if (rObj.has("source_Sublevel") && !rObj.getString("source_Sublevel").equals("null")) {
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		placementLeads.setSource_Sublevel((Timestamp)c.fromString(rObj.getString("source_Sublevel")));
				}
            	if(rObj.has("created") && !rObj.getString("created").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		placementLeads.setCreated((Timestamp)c.fromString(rObj.getString("created")));
            	}
            	if(rObj.has("lastUpdated") && !rObj.getString("lastUpdated").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		placementLeads.setLastUpdated((Timestamp)c.fromString(rObj.getString("lastUpdated")));
            	}
            	if(rObj.has("liveDate")){
            		placementLeads.setLiveDate(rObj.getString("liveDate"));
            	}
            	if (rObj.has("lastUpdatedUser")) {
            		placementLeads.setLastUpdatedUser(rObj.getString("lastUpdatedUser"));
				}
            	if (rObj.has("location")) {
            		placementLeads.setLocation(rObj.getString("location"));
				}
            	if (rObj.has("hospital_Entity")) {
            		placementLeads.setHospital_Entity(rObj.getString("hospital_Entity"));
				}
            	if (rObj.has("information")) {
            		placementLeads.setInformation(rObj.getString("information"));
				}
            	if (rObj.has("link1")) {
            		placementLeads.setLink1(rObj.getString("link1"));
				}
            	if (rObj.has("link2")) {
            		placementLeads.setLink2(rObj.getString("link2"));
				}
            	if (rObj.has("outreach")) {
            		placementLeads.setOutreach(rObj.getString("outreach"));
				}
            	if (rObj.has("person")) {
            		placementLeads.setPerson(rObj.getString("person"));
				}
            	if (rObj.has("sourceOfInformation")) {
            		placementLeads.setSourceOfInformation(rObj.getString("sourceOfInformation"));
				}
            	if (rObj.has("typeOfInformation")) {
            		placementLeads.setTypeOfInformation(rObj.getString("typeOfInformation"));
				}
            	if (rObj.has("priority")) {
            		placementLeads.setPriority(rObj.getInt("priority"));
				}
            	if (rObj.has("comments")) {
					placementLeads.setComments(rObj.getString("comments"));
				}
            	if (rObj.has("state")) {
					placementLeads.setState(rObj.getString("state"));
				}
            	list.add(placementLeads);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return list;
	}


}
