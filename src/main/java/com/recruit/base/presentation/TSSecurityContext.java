package com.recruit.base.presentation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.dataengine.security.SecurityContext;

public class TSSecurityContext implements SecurityContext {

	User user;

	public TSSecurityContext(User user) {
		this.user = user;
	}

	
	public List<String> getRoles() {
		List<String> roles = new ArrayList<String>();
		for (GrantedAuthority auth : user.getAuthorities()) {
			roles.add(auth.getAuthority());
		}
		return roles;
	}

	
	public String getUserId() {
		return this.user.getUsername();
	}

	
	public boolean isEnabled() {
		return true;
	}

}
