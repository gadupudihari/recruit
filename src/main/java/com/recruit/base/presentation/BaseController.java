package com.recruit.base.presentation;

import java.util.List;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.security.core.userdetails.User;
import com.recruit.util.JSONException;
import com.recruit.util.JSONParser;

public class BaseController {

	@Autowired
	protected Response response;

	@SuppressWarnings("rawtypes")
	protected String returnJson(Map<String, Object> map, Object obj){
		try {
			if(obj == null){
				map.put("json", "");
			}else if(obj instanceof List){
				map.put("json", JSONParser.listToJSON((List)obj));
			}else if(obj instanceof Response){
				map.put("json", ((Response)obj).toJson());				
			}else if(obj instanceof String){
				map.put("json", obj.toString());				
			}else{
				map.put("json", JSONParser.toJSON(obj));
			}			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "json";
	}
	
	
	public static User getPrincipal() {
		User obj = (User) (SecurityContextHolder.getContext()).getAuthentication().getPrincipal();
		return  obj;
	}
}
