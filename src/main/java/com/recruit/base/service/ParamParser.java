package com.recruit.base.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ParamParser {

	public static SearchFilter parse(String json) {
		SearchFilter filter = new SearchFilter();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
			JSONObject fObj = (JSONObject) tokenizer.nextValue();
			filter.setPageNo(fObj.getInt("pageNo"));
			filter.setPageSize(fObj.getInt("pageSize"));
			
            JSONArray pObjs = (JSONArray) fObj.getJSONArray("params");           
			for (int i = 0; i < pObjs.length(); i++) {
				JSONObject obj = pObjs.getJSONObject(i);
				Param param = new Param();
				if (obj.has("id")) {
					param.setId(obj.getString("id"));
				}
				if (obj.has("operand")) {
					param.setOperand(obj.getString("operand"));
				}else{
					param.setOperand(obj.getString("="));
				}
				if (obj.has("strValue")) {
					param.setStrValue(obj.getString("strValue"));
				}
				if (obj.has("dataType")) {
					String dataType = obj.getString("dataType");
					//Convert to object based on the datatype
				}else{
					param.setValue(obj.getString("strValue"));
				}
				filter.getParams().add(param);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filter;
	}
}
