package com.recruit.base.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchFilter {

	private List<Param> params;
	private List<String> orderBy;
	private int pageSize;
	private int pageNo;
	private String defaultOrderBy;

	public SearchFilter(){
		params = new ArrayList<Param>();
	}
	
	public List<Param> getParams() {
		return params;
	}

	public void setParams(List<Param> params) {
		this.params = params;
	}

	public List<String> getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(List<String> orderBy) {
		this.orderBy = orderBy;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	
	public Map<String, Param> getParamMap() {
		Map<String, Param> paramMap = new HashMap<String, Param>();
		for (Param param : getParams()) {
			paramMap.put(param.getId(), param);
		}
		return paramMap;
	}
	
	@Override
	public String toString() {
		return "SearchFilter [params=" + params + ", orderBy=" + orderBy + ", pageSize=" + pageSize
				+ ", pageNo=" + pageNo + "]";
	}

	public void setDefaultOrderBy(String defaultOrderBy) {
		this.defaultOrderBy = defaultOrderBy;
	}

	public String getDefaultOrderBy() {
		return defaultOrderBy;
	}

}
