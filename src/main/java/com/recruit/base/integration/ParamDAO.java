package com.recruit.base.integration;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;

@Repository
public class ParamDAO {

	public void setCriteria(Criteria criteria, SearchFilter filter) {
		
		for(Param param: filter.getParams()){
			if("in".equals(param.getOperand())){
				//criteria.add(Restrictions.in(param.getId(), param.getValue()));
			}else if ("like".equals(param.getOperand())){
				criteria.add(Restrictions.like(param.getId(), '%'+param.getValue().toString()+'%'));
			}else if (">".equals(param.getOperand())){
				criteria.add(Restrictions.ge(param.getId(), param.getValue()));
			}else if ("<".equals(param.getOperand())){
				criteria.add(Restrictions.le(param.getId(), param.getValue()));
			}else if ("!=".equals(param.getOperand())){
				criteria.add(Restrictions.ne(param.getId(), param.getValue()));
			}else{
				criteria.add(Restrictions.eq(param.getId(), param.getValue()));
			}
		}
		
//		if(DataType.isEqual(dataType, DataType.INT)){
//			param.setValue(Integer.parseInt(value));
//		}else if(DataType.isEqual(dataType, DataType.DOUBLE)){
//			param.setValue(Double.parseDouble(value));
//		}else{
//			param.setValue(value);
//		}
		
		if (filter.getOrderBy() != null && filter.getOrderBy().size() > 0) {
			if ("asc".equalsIgnoreCase(filter.getDefaultOrderBy())) {
				criteria.addOrder(Order.asc(filter.getOrderBy().get(0)));
			}else{
				criteria.addOrder(Order.desc(filter.getOrderBy().get(0)));
			}
		}
		criteria.setFirstResult(filter.getPageNo()*filter.getPageSize());
		criteria.setMaxResults(filter.getPageSize());		
	}

	
}
