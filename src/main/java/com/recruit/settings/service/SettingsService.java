package com.recruit.settings.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.recruit.base.presentation.Response;
import com.recruit.base.service.SearchFilter;
import com.recruit.settings.integration.ModuleDAO;
import com.recruit.settings.integration.SettingsDAO;
import com.recruit.settings.integration.SkillsetGroupDAO;
import com.recruit.user.integration.UserDAO;
import com.recruit.user.integration.UserEmailDAO;
import com.recruit.user.service.User;
import com.recruit.user.service.UserEmail;
import com.recruit.util.Encriptor;

@Service
public class SettingsService{

	@Autowired
	private SettingsDAO settingsDAO;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private UserEmailDAO userEmailDAO;

	@Autowired
	private ModuleDAO moduleDAO;
	
	@Autowired
	private SkillsetGroupDAO skillsetGroupDAO;
	
	@Transactional
	public List<Settings> list() {
		return settingsDAO.findByCriteria();
	}

	@Transactional
	public void save(Settings settings, Response response) {
		//check is exist with same name and value and type
		if (settingsDAO.checkExists(settings)) {
			response.setSuccess(false);
			response.setErrorMessage("Given values already exists with same type");
			return;
		}
		if (settings.getSkillsetGroup() != null && settings.getSkillsetGroup().getId() == null ) {
			skillsetGroupDAO.attachDirty(settings.getSkillsetGroup());
		}
		
		settingsDAO.attachDirty(settings);
		response.setSuccess(true);
	}

	@Transactional
	public List<Settings> searchSettings(SearchFilter filter) {
		return settingsDAO.findByCriteria(filter);
	}
	
	@Transactional
	public void deleteCategory(Integer id) {
		Settings settings = settingsDAO.findById(id);
		settingsDAO.delete(settings);
	}

	@Transactional
	public List<Settings> getCategory(SearchFilter filter) {
		return settingsDAO.findByType(filter);
	}

	@Transactional
	public List<Settings> findSettingsByName(String employer,String type) {
		return settingsDAO.findByName(employer,type);
	}

	@Transactional
	public void saveEmailDetails(Response response, UserEmail email, String loginId) {
/*		if (userEmailDAO.checkEmailExists(email.getEmailId())) {
			response.setErrorMessage("Email id already Exist.");
			response.setSuccess(false);
			return;
		}*/
		userEmailDAO.deleteByEmailId(email.getEmailId());
		User user = userDAO.findByLoginId(loginId).get(0);
		email.setUser(user);
		String password = Encriptor.encryptString(email.getPassword());
		email.setPassword(password);
		userEmailDAO.attachDirty(email);
		response.setSuccess(true);
		response.setReturnVal(email);
	}

	@Transactional
	public List<UserEmail> getUserEmail(String loginId) {
		return userEmailDAO.findByLoginId(loginId);
	}

	@Transactional
	public List<Module> getModule(SearchFilter filter) {
		return moduleDAO.findByCriteria(filter);
	}

	@Transactional
	public void saveModule(Module module) {
		moduleDAO.attachDirty(module);
	}

	@Transactional
	public List<Module> getModuleCategory(SearchFilter filter) {
		return moduleDAO.getModuleCategory(filter);
	}

	@Transactional
	public List<SkillsetGroup> searchSkillsetGroup() {
		return skillsetGroupDAO.findByCriteria();
	}

	@Transactional
	public void saveSkillsetGroup(SkillsetGroup skillset) {
		skillsetGroupDAO.attachDirty(skillset);
	}
	
}
