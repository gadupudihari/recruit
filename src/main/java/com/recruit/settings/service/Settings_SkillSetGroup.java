package com.recruit.settings.service;

public class Settings_SkillSetGroup implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;
	private Integer id;
	private Integer skillSetGroupId;
	
	private Settings settings;

	public Settings_SkillSetGroup() {}
	
	public Settings_SkillSetGroup(Integer id, Integer skillSetGroupId , Settings settings) {
		this.id = id;
		this.settings = settings;
		this.skillSetGroupId = skillSetGroupId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSkillSetGroupId() {
		return skillSetGroupId;
	}

	public void setSkillSetGroupId(Integer skillSetGroupId) {
		this.skillSetGroupId = skillSetGroupId;
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	@Override
	public String toString() {
		return "Requirement [id=" + id  + ", skillSetGroupId=" + skillSetGroupId +"]";
	}

}