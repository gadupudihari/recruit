package com.recruit.settings.service;

import java.sql.Timestamp;


// Generated Sep 11, 2011 8:31:09 AM by Hibernate Tools 3.3.0.GA

/**
 * Firm generated by hbm2java
 */
public class Counter implements java.io.Serializable {


	private static final long serialVersionUID = 3517973064872709390L;
	private Integer id;
	private String tableName;
	private Timestamp date;
	private Integer counter;
	private String comments;

	public Counter() {
	}

	public Counter(Integer id, String tableName, Timestamp date, Integer counter, String comments) {
		this.id = id;
		this.tableName = tableName;
		this.date = date;
		this.counter = counter;
		this.comments = comments;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public Integer getCounter() {
		return counter;
	}

	public void setCounter(Integer counter) {
		this.counter = counter;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "Counter [id=" + id + ", tableName=" + tableName + ", date=" + date + ", counter=" + counter + ", comments=" + comments
				+ "]";
	}

}