package com.recruit.settings.service;

public class SkillsetGroup implements java.io.Serializable {

	private static final long serialVersionUID = 3517973064872709390L;
	private Integer id;
	private String name;
	private String type;

	public SkillsetGroup() {
	}

	public SkillsetGroup(Integer id, String name, String type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "SkillsetGroup [id=" + id + ", name=" + name + ", type=" + type + "]";
	}

}
