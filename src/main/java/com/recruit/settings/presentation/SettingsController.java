package com.recruit.settings.presentation;


import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.recruit.base.presentation.BaseController;
import com.recruit.base.presentation.Response;
import com.recruit.base.service.Param;
import com.recruit.base.service.ParamParser;
import com.recruit.base.service.SearchFilter;
import com.recruit.settings.service.Module;
import com.recruit.settings.service.Settings;
import com.recruit.settings.service.SettingsService;
import com.recruit.settings.service.SkillsetGroup;
import com.recruit.user.service.User;
import com.recruit.user.service.UserEmail;
import com.recruit.user.service.UserService;
import com.recruit.util.JSONException;
import com.recruit.util.JSONParser;
import com.recruit.util.JsonParserUtility;
import com.recruit.util.UtilityHelper;

@Controller
@RequestMapping("/settings")
@PreAuthorize("hasAnyRole('ADMIN','RECRUITER')")
public class SettingsController extends BaseController {

	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private SettingsService settingsService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/search", method = {RequestMethod.POST,RequestMethod.GET })
	public String search(@RequestParam("json") String json,Map<String, Object> map) throws JSONException {
		List<Settings> settings = settingsService.list();
		response.setSuccess(true);
		response.setReturnVal(JsonParserUtility.toSettingsJson(settings).toString());
		return returnJson(map, JsonParserUtility.toSettingsJson(settings).toString());
	}
	
	@RequestMapping(value = "/searchSkillsetGroup", method = {RequestMethod.POST,RequestMethod.GET })
	public String searchSkillsetGroup(@RequestParam("json") String json,Map<String, Object> map) throws JSONException {
		List<SkillsetGroup> skillsetGroups = settingsService.searchSkillsetGroup();
		return returnJson(map, skillsetGroups);
	}
	
	@RequestMapping(value = "/save", method = {RequestMethod.POST,RequestMethod.GET })
	public String save(@RequestParam("json") String json, Map<String, Object> map){
		response = new Response();
		Settings settings = (Settings)JSONParser.parseJSON(json, Settings.class);
		settingsService.save(settings,response);
		response.setReturnVal(JsonParserUtility.toJSON(settings).toString());
		return returnJson(map, response);
	}

	@RequestMapping(value = "/saveSkillsetGroup", method = {RequestMethod.POST,RequestMethod.GET })
	public String saveSkillsetGroup(@RequestParam("json") String json, Map<String, Object> map){
		SkillsetGroup skillset = (SkillsetGroup)JSONParser.parseJSON(json, SkillsetGroup.class);
		settingsService.saveSkillsetGroup(skillset);
		response.setSuccess(true);
		response.setReturnVal(skillset);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/searchSettings", method = {RequestMethod.POST,RequestMethod.GET })
	public String searchSettings(@RequestParam("json") String json,Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Settings> settings = settingsService.searchSettings(filter);
		response.setReturnVal(JsonParserUtility.toSettingsJson(settings).toString());
		return returnJson(map, JsonParserUtility.toSettingsJson(settings).toString());
	}
	
	@RequestMapping(value = "/deleteCategory/{id}", method = {RequestMethod.POST,RequestMethod.GET })
	public String deleteCategory(@PathVariable("id") Integer id, Map<String, Object> map) throws JSONException {
		settingsService.deleteCategory(id);
		response.setSuccess(Boolean.TRUE);
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/getCategory", method = {RequestMethod.POST,RequestMethod.GET })
	public String getCategory(@RequestParam("json") String json,Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Settings> list = settingsService.getCategory(filter);
		return returnJson(map, JsonParserUtility.toSettingsJson(list).toString());
	}

	@RequestMapping(value = "/getModuleCategory", method = {RequestMethod.POST,RequestMethod.GET })
	public String getModuleCategory(@RequestParam("json") String json,Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Module> list = settingsService.getModuleCategory(filter);
		return returnJson(map, JsonParserUtility.toModuleSettingsJson(list).toString());
	}
	
	
	@RequestMapping(value = "/saveXMLAlert", method = {RequestMethod.POST,RequestMethod.GET })
	public String saveXMLAlert(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		String alertname=" ",alertTitle=" ",reminderdays=" ",alerttype=" ",alertLogic=" ",emailsubject=" ",emailbody=" ",mailto=" ",mailtoRole=" ",mailtoignore=" ",priority=" ";
		SearchFilter filter = ParamParser.parse(json);
		Map<String, Param> filterMap = filter.getParamMap();
		if (filterMap.get("alertname") != null) 
			alertname = filterMap.get("alertname").getStrValue();
		if (filterMap.get("alertTitle") != null) 
			alertTitle = filterMap.get("alertTitle").getStrValue();
		if (filterMap.get("reminderdays") != null) 
			reminderdays = filterMap.get("reminderdays").getStrValue();
		if (filterMap.get("alerttype") != null) 
			alerttype = filterMap.get("alerttype").getStrValue();
		if (filterMap.get("alertLogic") != null) 
			alertLogic = filterMap.get("alertLogic").getStrValue();
		if (filterMap.get("emailsubject") != null) 
			emailsubject = filterMap.get("emailsubject").getStrValue();
		if (filterMap.get("emailbody") != null) 
			emailbody = filterMap.get("emailbody").getStrValue();
		if (filterMap.get("mailto") != null) 
			mailto = filterMap.get("mailto").getStrValue();
		if (filterMap.get("mailtoRole") != null) 
			mailtoRole = filterMap.get("mailtoRole").getStrValue();
		if (filterMap.get("mailtoignore") != null) 
			mailtoignore = filterMap.get("mailtoignore").getStrValue();
		if (filterMap.get("priority") != null) 
			priority = filterMap.get("priority").getStrValue();
		
		try {
			ServletContext servletContext = request.getSession().getServletContext();
			String contextPath = servletContext.getRealPath(File.separator);

			File xmlFile = new File(contextPath+"\\Xml files\\alerts.xml");
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			
	    	NodeList allAlertsList = doc.getElementsByTagName("alert");
			for (int temp = 0; temp < allAlertsList.getLength(); temp++) {
		        Node alertNode = allAlertsList.item(temp);
		        
		        if (alertNode.getNodeType() == Node.ELEMENT_NODE) {
		            Element eElement = (Element) alertNode;
		            
                    NodeList alertNames = eElement.getElementsByTagName("alertname");
                    if (alertNames.item(0).getChildNodes().item(0).getNodeValue().equalsIgnoreCase(alertname)) {
                    	
            			Node root = alertNode;
            			NodeList list = root.getChildNodes();
            			for (int i = 0; i < list.getLength(); i++) {
                            Node node = list.item(i);
            			   if ((node.getNodeName()).equalsIgnoreCase("alertname")) 
            				   node.setTextContent(alertname);
            			   if ((node.getNodeName()).equalsIgnoreCase("alertTitle")) 
            				   node.setTextContent(alertTitle);
            			   if ((node.getNodeName()).equalsIgnoreCase("reminderdays")) 
            				   node.setTextContent(reminderdays);
            			   if ((node.getNodeName()).equalsIgnoreCase("alerttype")) 
            				   node.setTextContent(alerttype);
            			   if ((node.getNodeName()).equalsIgnoreCase("alertLogic")) 
            				   node.setTextContent(alertLogic);
            			   if ((node.getNodeName()).equalsIgnoreCase("emailsubject")) 
            				   node.setTextContent(emailsubject);
            			   if ((node.getNodeName()).equalsIgnoreCase("emailbody")) 
            				   node.setTextContent(emailbody);
            			   if ((node.getNodeName()).equalsIgnoreCase("mailto")) 
            				   node.setTextContent(mailto);
            			   if ((node.getNodeName()).equalsIgnoreCase("mailtoRole")) 
            				   node.setTextContent(mailtoRole);
            			   if ((node.getNodeName()).equalsIgnoreCase("mailtoignore")) 
            				   node.setTextContent(mailtoignore);
            			   if ((node.getNodeName()).equalsIgnoreCase("priority")) 
            				   node.setTextContent(priority);
            			}
					}
		        }
		    }
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(xmlFile.getAbsolutePath()));
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} 
		response.setSuccess(true);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/getCustomAlerts", method = RequestMethod.POST)
	public String getCustomAlerts(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		try {
			ServletContext servletContext = request.getSession().getServletContext();
			String contextPath = servletContext.getRealPath(File.separator);
			File xmlFile = new File(contextPath+"\\Xml files\\alerts.xml");

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(xmlFile);
			doc.getDocumentElement().normalize();

			doc.getDocumentElement().normalize();
	    	NodeList nList = doc.getElementsByTagName("alert");
	    	
	    	JSONObject mainObj = new JSONObject();
	    	JSONArray array = new JSONArray();
			for (int temp = 0; temp < nList.getLength(); temp++) {
				JSONObject obj = new JSONObject();
		        Node nNode = nList.item(temp);
		        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		            Element eElement = (Element) nNode;
		            
		            if(eElement.getElementsByTagName("alertname").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("alertname", eElement.getElementsByTagName("alertname").item(0).getChildNodes().item(0).getNodeValue());
		            
		            if(eElement.getElementsByTagName("alertTitle").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("alertTitle", eElement.getElementsByTagName("alertTitle").item(0).getChildNodes().item(0).getNodeValue());
		            
		            if(eElement.getElementsByTagName("reminderdays").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("reminderdays", eElement.getElementsByTagName("reminderdays").item(0).getChildNodes().item(0).getNodeValue());
		            
		            if(eElement.getElementsByTagName("alerttype").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("alerttype", eElement.getElementsByTagName("alerttype").item(0).getChildNodes().item(0).getNodeValue());
		            
		            if(eElement.getElementsByTagName("alertLogic").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("alertLogic", eElement.getElementsByTagName("alertLogic").item(0).getChildNodes().item(0).getNodeValue());
		            
		            if(eElement.getElementsByTagName("emailsubject").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("emailsubject", eElement.getElementsByTagName("emailsubject").item(0).getChildNodes().item(0).getNodeValue());
		            
		            if(eElement.getElementsByTagName("emailbody").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("emailbody", eElement.getElementsByTagName("emailbody").item(0).getChildNodes().item(0).getNodeValue());
		            
		            if(eElement.getElementsByTagName("mailto").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("mailto", eElement.getElementsByTagName("mailto").item(0).getChildNodes().item(0).getNodeValue());
		            
		            if(eElement.getElementsByTagName("mailtoRole").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("mailtoRole", eElement.getElementsByTagName("mailtoRole").item(0).getChildNodes().item(0).getNodeValue());
		            
		            if(eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("mailtoignore", eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue());

		            if(eElement.getElementsByTagName("priority").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("priority", eElement.getElementsByTagName("priority").item(0).getChildNodes().item(0).getNodeValue());
		            
                   	array.put(obj);
		        }
		    }
			mainObj.put("rows", array);
			mainObj.put("size", nList.getLength());
			mainObj.put("success", Boolean.TRUE);
			
			response.setReturnVal(mainObj.toString());
			response.setSuccess(Boolean.TRUE);
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
		}
		return returnJson(map, response);
	}

	
	@RequestMapping(value = "/unsubscribe", method = {RequestMethod.POST,RequestMethod.GET })
	public String unsubscribe(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		String alertname=json;
		String mailtoignore = "";
		String loginId = UtilityHelper.getLoginId(request);
		List<User> users = userService.findByLoginId(loginId);
		String emailId = users.get(0).getEmailId();
		
		try {
			ServletContext servletContext = request.getSession().getServletContext();
			String contextPath = servletContext.getRealPath(File.separator);

			File xmlFile = new File(contextPath+"\\Xml files\\alerts.xml");
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			
	    	NodeList allAlertsList = doc.getElementsByTagName("alert");
			for (int temp = 0; temp < allAlertsList.getLength(); temp++) {
		        Node alertNode = allAlertsList.item(temp);
		        
		        if (alertNode.getNodeType() == Node.ELEMENT_NODE) {
		            Element eElement = (Element) alertNode;
		            
                    NodeList alertNames = eElement.getElementsByTagName("alertname");
                    if (alertNames.item(0).getChildNodes().item(0).getNodeValue().equalsIgnoreCase(alertname)) {
                    	//Checking email alredy ignored, If not will be appended
                    	mailtoignore = eElement.getElementsByTagName("mailtoignore").item(0).getChildNodes().item(0).getNodeValue();
                    	String ignoreIds[] = mailtoignore.split(",");
                    	if (Arrays.asList(ignoreIds).contains(emailId)) {
                    		response.setSuccess(false);
                    		response.setErrorMessage("You are alredy unsubscribed");
                    		return returnJson(map, response);
						}else{
							mailtoignore = mailtoignore+","+emailId;
						}
                    	
            			Node root = alertNode;
            			NodeList list = root.getChildNodes();
            			for (int i = 0; i < list.getLength(); i++) {
                            Node node = list.item(i);
             			   if ((node.getNodeName()).equalsIgnoreCase("mailtoignore")) 
            				   node.setTextContent(mailtoignore);
            			}
					}
		        }
		    }
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(xmlFile.getAbsolutePath()));
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} 
		response.setSuccess(true);
		response.setReturnVal("\""+mailtoignore+"\"");
		return returnJson(map, response);
	}
	
	@RequestMapping(value = "/getUserEmail", method = {RequestMethod.POST,RequestMethod.GET })
	public String getUserEmail(@RequestParam("json") String json,Map<String, Object> map,HttpServletRequest request) throws JSONException {
		String loginId =UtilityHelper.getLoginId(request);
		List<UserEmail> emails = settingsService.getUserEmail(loginId);
		response.setReturnVal(emails);
		return returnJson(map, emails);
	}
	
	
	@RequestMapping(value = "/saveEmailDetails",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveEmailDetails(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		UserEmail email = (UserEmail) JSONParser.parseJSON(json, UserEmail.class);
		String loginId =UtilityHelper.getLoginId(request);	
		settingsService.saveEmailDetails(response,email,loginId);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/getModule", method = {RequestMethod.POST,RequestMethod.GET })
	public String getModule(@RequestParam("json") String json,Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Module> modules = settingsService.getModule(filter);
		return returnJson(map, JsonParserUtility.toModuleJSON(modules).toString());
	}

	@RequestMapping(value = "/saveModule", method = {RequestMethod.POST,RequestMethod.GET })
	public String saveModule(@RequestParam("json") String json, Map<String, Object> map){
		response = new Response();
		Module module = (Module)JSONParser.parseJSON(json, Module.class);
		settingsService.saveModule(module);
		response.setReturnVal(module);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/getCustomColumns", method = RequestMethod.POST)
	public String getCustomColumns(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		try {
			ServletContext servletContext = request.getSession().getServletContext();
			String contextPath = servletContext.getRealPath(File.separator);
			File xmlFile = new File(contextPath+"\\Xml files\\CandidateTemplate.xml");

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(xmlFile);
			doc.getDocumentElement().normalize();

			doc.getDocumentElement().normalize();
	    	NodeList nList = doc.getElementsByTagName("Template");
	    	
	    	JSONObject mainObj = new JSONObject();
	    	JSONArray array = new JSONArray();
			for (int temp = 0; temp < nList.getLength(); temp++) {
				JSONObject obj = new JSONObject();
		        Node nNode = nList.item(temp);
		        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		            Element eElement = (Element) nNode;
		            
		            if(eElement.getElementsByTagName("columns").item(0).getChildNodes().item(0).getNodeValue() != null)
		            	obj.put("columns", eElement.getElementsByTagName("columns").item(0).getChildNodes().item(0).getNodeValue());
		            
                   	array.put(obj);
		        }
		    }
			mainObj.put("rows", array);
			mainObj.put("size", nList.getLength());
			mainObj.put("success", Boolean.TRUE);
			
			response.setReturnVal(mainObj.toString());
			response.setSuccess(Boolean.TRUE);
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
		}
		return returnJson(map, response);
	}

	
}