package com.recruit.settings.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.settings.service.Settings_SkillSetGroup;

/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class Settings_SkillSetGroupDAO {

	private static final Log log = LogFactory.getLog(Settings_SkillSetGroupDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Settings_SkillSetGroup  transientInstance) {
		log.debug("persisting Settings_SkillSetGroup instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Settings_SkillSetGroup instance) {
		log.debug("attaching dirty Settings_SkillSetGroup instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Settings_SkillSetGroup persistentInstance) {
		log.debug("deleting Settings_SkillSetGroup instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Settings_SkillSetGroup merge(Settings_SkillSetGroup detachedInstance) {
		log.debug("merging Settings_SkillSetGroup instance");
		try {
			Settings_SkillSetGroup result = (Settings_SkillSetGroup) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Settings_SkillSetGroup findById(java.lang.Integer id) {
		log.debug("getting Settings_SkillSetGroup instance with id: " + id);
		try {
			Settings_SkillSetGroup instance = (Settings_SkillSetGroup) sessionFactory.getCurrentSession()
					.get("com.recruit.settings.service.Settings_SkillSetGroup", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("rawtypes")
	public List findByCriteria(SearchFilter filter) {
		try {
			String query = "select distinct s.* from recruit_settings_skillsetgroup s where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("candidateId") != null) {
				query += " and s.candidateId like '"+map.get("candidateId").getStrValue()+"'";
			}

			System.out.println("Research : "+query);
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	public void deleteBySettingsId(Integer settingsId) {
		try {
			String query = "delete from recruit_settings_skillsetgroup where settingsId = "+settingsId;
			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
		} catch (RuntimeException re) {
			log.error("find by employee Id failed", re);
			throw re;
		}
	}

}