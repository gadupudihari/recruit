package com.recruit.settings.integration;

// Generated Sep 11, 2011 8:31:09 AM by Hibernate Tools 3.3.0.GA


import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.settings.service.Settings;

/**
 * Home object for domain model class Settings.
 * @see com.recruit.settings.service.Settings
 * @author Hibernate Tools
 */
@Repository
public class SettingsDAO {

	private static final Log log = LogFactory.getLog(SettingsDAO.class);


	@Autowired
	private  SessionFactory sessionFactory;

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Settings transientInstance) {
		log.debug("persisting Settings instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Settings instance) {
		log.debug("attaching dirty Settings instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Settings persistentInstance) {
		log.debug("deleting Settings instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Settings merge(Settings detachedInstance) {
		log.debug("merging Settings instance");
		try {
			Settings result = (Settings) sessionFactory.getCurrentSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Settings findById(java.lang.Integer id) {
		log.debug("getting Settings instance with id: " + id);
		try {
			Settings instance = (Settings) sessionFactory.getCurrentSession().get(
					"com.recruit.settings.service.Settings", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Settings> findByCriteria() {
		try {
			
			String query = "from Settings as e where 1=1 group by type";

			List<Settings> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Settings> findByCriteria(SearchFilter filter) {
		try {
			String query = "from Settings as e where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("type") != null) {
				query += " and e.type like '" + map.get("type").getStrValue()+"' " ;
			}
			System.out.println("query for Settings is"+query);
			List<Settings> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Settings> findByType(SearchFilter filter) {
		try {
			String query = "from Settings as e where 1=1 ";
			Map<String, Param> map = filter.getParamMap();
			if (map.get("type") != null) {
				query += " and e.type like '" + map.get("type").getStrValue()+"' " ;
			}
			query += " order by e.name";
			System.out.println("query for Settings is "+query);
			List<Settings> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Settings> findByName(String name,String type) {
		try {
			String query = "from Settings as e where 1=1 and e.name like '"+name+"' and e.type like '"+type+"'" ;
			List<Settings> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Settings> findByNameType(String name, String type) {
		try {
			String query = "from Settings as e where 1=1 and e.name like '"+name+"' and e.type like '"+type+"'";
			List<Settings> list = sessionFactory.getCurrentSession().createQuery(query).list();
			return list;	
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}

		
	}

	@SuppressWarnings("unchecked")
	public boolean checkExists(Settings settings) {
		try {
			String query = "from Settings as e where 1=1 and e.name like '"+settings.getName()+"' " +
					" and e.value like '"+settings.getValue()+"' " +
					" and e.type like '"+settings.getType()+"' ";
			if (settings.getId() != null )
				query += " and e.id !="+settings.getId();
			
			List<Settings> list = sessionFactory.getCurrentSession().createQuery(query).list();
			
			if (list.size() > 0)
				return true;
			return false;	
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}

		
	}

}
