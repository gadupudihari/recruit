package com.recruit.settings.integration;

// Generated Sep 11, 2011 8:31:09 AM by Hibernate Tools 3.3.0.GA


import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.settings.service.SkillsetGroup;

/**
 * Home object for domain model class SkillsetGroup.
 * @see com.recruit.settings.service.Settings
 * @author Hibernate Tools
 */
@Repository
public class SkillsetGroupDAO {

	private static final Log log = LogFactory.getLog(SkillsetGroupDAO.class);


	@Autowired
	private  SessionFactory sessionFactory;

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(SkillsetGroup transientInstance) {
		log.debug("persisting SkillsetGroup instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(SkillsetGroup instance) {
		log.debug("attaching dirty SkillsetGroup instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(SkillsetGroup persistentInstance) {
		log.debug("deleting SkillsetGroup instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SkillsetGroup merge(SkillsetGroup detachedInstance) {
		log.debug("merging SkillsetGroup instance");
		try {
			SkillsetGroup result = (SkillsetGroup) sessionFactory.getCurrentSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public SkillsetGroup findById(java.lang.Integer id) {
		log.debug("getting SkillsetGroup instance with id: " + id);
		try {
			SkillsetGroup instance = (SkillsetGroup) sessionFactory.getCurrentSession().get(
					"com.recruit.SkillsetGroup.service.SkillsetGroup", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SkillsetGroup> findByCriteria() {
		try {
			
			String query = "from SkillsetGroup as e ";

			List<SkillsetGroup> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SkillsetGroup> findByCriteria(SearchFilter filter) {
		try {
			String query = "from SkillsetGroup as e where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("type") != null) {
				query += " and e.type like '" + map.get("type").getStrValue()+"' " ;
			}
			System.out.println("query for SkillsetGroup is"+query);
			List<SkillsetGroup> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SkillsetGroup> findByType(SearchFilter filter) {
		try {
			String query = "from SkillsetGroup as e where 1=1 ";
			Map<String, Param> map = filter.getParamMap();
			if (map.get("type") != null) {
				query += " and e.type like '" + map.get("type").getStrValue()+"' " ;
			}
			query += " order by e.name";
			System.out.println("query for SkillsetGroup is "+query);
			List<SkillsetGroup> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SkillsetGroup> findByName(String name,String type) {
		try {
			String query = "from SkillsetGroup as e where 1=1 and e.name like '"+name+"' and e.type like '"+type+"'" ;
			List<SkillsetGroup> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SkillsetGroup> findByNameType(String name, String type) {
		try {
			String query = "from SkillsetGroup as e where 1=1 and e.name like '"+name+"' and e.type like '"+type+"'";
			List<SkillsetGroup> list = sessionFactory.getCurrentSession().createQuery(query).list();
			return list;	
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}

		
	}

	@SuppressWarnings("unchecked")
	public boolean checkExists(SkillsetGroup skillset) {
		try {
			String query = "from SkillsetGroup as e where 1=1 and e.name like '"+skillset.getName()+"' " +
					" and e.type like '"+skillset.getType()+"' ";
			if (skillset.getId() != null )
				query += " and e.id !="+skillset.getId();
			
			List<SkillsetGroup> list = sessionFactory.getCurrentSession().createQuery(query).list();
			
			if (list.size() > 0)
				return true;
			return false;	
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}

		
	}

}
