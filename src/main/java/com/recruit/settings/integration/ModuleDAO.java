package com.recruit.settings.integration;

// Generated Sep 11, 2011 8:31:09 AM by Hibernate Tools 3.3.0.GA


import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.settings.service.Module;

/**
 * Home object for domain model class Settings.
 * @see com.recruit.settings.service.Settings
 * @author Hibernate Tools
 */
@Repository
public class ModuleDAO {

	private static final Log log = LogFactory.getLog(ModuleDAO.class);


	@Autowired
	private  SessionFactory sessionFactory;

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Module transientInstance) {
		log.debug("persisting Module instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Module instance) {
		log.debug("attaching dirty Module instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Module persistentInstance) {
		log.debug("deleting Module instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Module merge(Module detachedInstance) {
		log.debug("merging Module instance");
		try {
			Module result = (Module) sessionFactory.getCurrentSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Module findById(java.lang.Integer id) {
		log.debug("getting Module instance with id: " + id);
		try {
			Module instance = (Module) sessionFactory.getCurrentSession().get(
					"com.recruit.settings.service.Module", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Module> findByCriteria() {
		try {
			
			String query = "from Module as e where 1=1 group by type";

			List<Module> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Module> findByCriteria(SearchFilter filter) {
		try {
			String query = "from Module as e where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("type") != null) {
				query += " and e.settings.type like '" + map.get("type").getStrValue()+"' " ;
			}
			if (map.get("settingsId") != null) {
				query += " and e.settings.id like '" + map.get("settingsId").getStrValue()+"' " ;
			}
			System.out.println("query for Module is"+query);
			List<Module> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Module> findByType(String type) {
		try {
			String query = "from Module as e where 1=1 ";
			if (type != null) {
				query += " and e.type like '" +type +"' " ;
			}
			query += " order by e.name";
			System.out.println("query for Module is"+query);
			List<Module> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Module> getModuleCategory(SearchFilter filter) {
		try {
			String query = "select m.inthelp1,m.inthelp2,m.trainer,m.resumeHelp,s.id,s.name,s.value,s.type from recruit_settings s" +
					" left join recruit_module m on settingsId = s.id where 1=1 ";
			
			Map<String, Param> map = filter.getParamMap();
			if (map.get("type") != null) {
				query += " and s.type like '" +map.get("type").getStrValue() +"' " ;
			}
			query += " order by s.name";
			System.out.println("query for Module is"+query);
			List<Module> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Module.class)).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

}
