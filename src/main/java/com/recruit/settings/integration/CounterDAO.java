package com.recruit.settings.integration;

// Generated Sep 11, 2011 8:31:09 AM by Hibernate Tools 3.3.0.GA


import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.settings.service.Counter;


/**
 * Home object for domain model class Settings.
 * @see com.timesheetz.settings.service.Settings
 * @author Hibernate Tools
 */
@Repository
public class CounterDAO {

	private static final Log log = LogFactory.getLog(CounterDAO.class);


	@Autowired
	private  SessionFactory sessionFactory;

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Counter transientInstance) {
		log.debug("persisting Counter instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Counter instance) {
		log.debug("attaching dirty Counter instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Counter persistentInstance) {
		log.debug("deleting Counter instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Counter merge(Counter detachedInstance) {
		log.debug("merging Counter instance");
		try {
			Counter result = (Counter) sessionFactory.getCurrentSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Counter findById(java.lang.Integer id) {
		log.debug("getting Counter instance with id: " + id);
		try {
			Counter instance = (Counter) sessionFactory.getCurrentSession().get(
					"com.timesheetz.settings.service.Counter", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Counter> findByCriteria(SearchFilter filter) {
		try {
			String query = "from Counter as e where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("tableName") != null) {
				query += " and e.tableName like '" + map.get("tableName").getStrValue()+"' " ;
			}
			
			System.out.println("query for Counter is"+query);
			List<Counter> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			return list;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public Counter findByTableName(String tableName) {
		try {
			String query = "from Counter as e where e.tableName like '" + tableName+"'";
			
			System.out.println("query for Counter is "+query);
			List<Counter> list = sessionFactory.getCurrentSession()
					.createQuery(query).list();
			if (list.size() > 0) {
				return list.get(0);
			}
			return null;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

}
