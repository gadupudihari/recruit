package com.recruit.interview.presentation;

import java.io.File;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.recruit.base.presentation.BaseController;
import com.recruit.base.service.Param;
import com.recruit.base.service.ParamParser;
import com.recruit.base.service.SearchFilter;
import com.recruit.interview.service.Interview;
import com.recruit.interview.service.InterviewService;
import com.recruit.util.JSONException;
import com.recruit.util.JSONParser;
import com.recruit.util.JsonParserUtility;
import com.recruit.util.SqlTimestampConverter;
import com.recruit.util.UtilityHelper;
import com.recruit.util.WriteExcel;

@Controller
@RequestMapping("/interview")
@PreAuthorize("hasAnyRole('ADMIN','RECRUITER')")
public class InterviewController extends BaseController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	InterviewService interviewService;
	
	@Autowired
	WriteExcel writeExcel;
	
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	
	@RequestMapping(value = "/getInterviews", method = {RequestMethod.POST,RequestMethod.GET })
	public String getInterviews(@RequestParam("json") String json, Map<String, Object> map) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Interview> interviews = interviewService.getInterviews(filter);
		return returnJson(map, JsonParserUtility.toInterviewJSON(interviews).toString());
	}

	@RequestMapping(value = "/saveInterviews",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveInterviews(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		List<Interview> interviews = InterviewParser.parseJSON(json);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		interviewService.save(interviews, loginId, createdDate);
		response.setSuccess(Boolean.TRUE);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/saveInterview",  method = {RequestMethod.POST,RequestMethod.GET })
	public String saveInterview(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request){
		String stringTime = "", stringDate="", currentTime="";
		if (json.indexOf("\"currentTime\"") != -1) {
			currentTime = json.substring(json.indexOf("\"currentTime\"")+15,json.indexOf("\"currentTime\"")+23);
			json = json.replaceAll("\"currentTime\":\""+currentTime+"\",", "");
		}
		if (json.indexOf("\"time\"") != -1) {
			stringTime = json.substring(json.indexOf("\"time\"")+8,json.indexOf("\"time\"")+16);
			json = json.replaceAll("\"time\":\""+stringTime+"\",", "");
		}
		if (json.indexOf("\"interviewDate\"") != -1) {
			stringDate = json.substring(json.indexOf("\"interviewDate\"")+17,json.indexOf("\"interviewDate\"")+36);
			json = json.replaceAll("\"interviewDate\":\""+stringDate+"\",", "");
		}
		System.out.println(currentTime + "----------"+stringTime);
		Interview interview = (Interview) JSONParser.parseJSON(json, Interview.class);
		String loginId =UtilityHelper.getLoginId(request);	
		if (loginId.contains("@")) {
			loginId = loginId.substring(0,loginId.indexOf('@'));	
		}
		java.sql.Timestamp createdDate = UtilityHelper.getDate();
		if (interview.getId()== null) 
			interview.setCreated(createdDate);
		interview.setLastUpdated(createdDate);
		interview.setLastUpdatedUser(loginId);
		if (currentTime.length()> 0) {
			SqlTimestampConverter c = new SqlTimestampConverter();
			interview.setCurrentTime((Time)c.fromStringToTime(currentTime));
		}
		if (stringTime.length()> 0) {
			SqlTimestampConverter c = new SqlTimestampConverter();
			interview.setTime((Time)c.fromStringToTime(stringTime));
		}
		if (stringDate.length() > 0) {
    		SqlTimestampConverter c = new SqlTimestampConverter();
    		interview.setInterviewDate((Date)c.fromStringToDate(stringDate));
		}
		interviewService.saveInterview(response,interview);
		return returnJson(map, response);
	}

	@RequestMapping(value = "/deleteInterview/{id}", method = RequestMethod.GET)
	public String deleteInterview(@PathVariable("id") Integer id, Map<String, Object> map){
		interviewService.deleteInterview(response,id);
		return returnJson(map, response);
	}

	
	@RequestMapping(value = "/interviewExcelExport", method = {RequestMethod.POST,RequestMethod.GET })
	public String interviewExcelExport(@RequestParam("json") String json, Map<String, Object> map,HttpSession session,HttpServletRequest request,HttpServletResponse res) throws JSONException {
		SearchFilter filter = ParamParser.parse(json);
		List<Interview> interviews = interviewService.getInterviews(filter);
		Map<String, Param> filterMap = filter.getParamMap();
		String columns[] = filterMap.get("columns").getStrValue().split(",");
		String contextPath="", fileName = "Interviews";
		  try{
		      ServletContext servletContext = session.getServletContext();
		      contextPath = servletContext.getRealPath(File.separator);
		      System.out.println("File system context path (in TestFilter): " + contextPath);
		      System.out.println("---------------------"+interviews.size());
		      
		      //WriteExcel test = new WriteExcel();
		      UtilityHelper.deleteExcelFiles(contextPath, "Interviews");
		      
		      writeExcel.setOutputFile(contextPath+fileName+".xls");
		      //Writing to Excel File
		      writeExcel.writeInterviews(interviews,columns);
			  System.out.println("Download Path ------ " + contextPath+fileName+".xls");
		  }
		  catch(Exception ex){
			  System.err.println("---------- **** Exception while generating Excel file **** ---------");
			  System.err.println(ex.getMessage());
			  ex.printStackTrace();
			  response.setSuccess(false);
		  }
		  response.setSuccess(true);
		  response.setReturnVal(fileName+".xls");
		  return returnJson(map, response);
	}

	//send email alert of interviews saved to admins
	@RequestMapping(value = "/sendInterviewAlert", method = {RequestMethod.POST,RequestMethod.GET })
	public String sendInterviewAlert(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		String loginId = UtilityHelper.getLoginId(request);
		Timestamp currentDate = UtilityHelper.getDate();
		interviewService.sendInterviewAlert(json,loginId,currentDate);
		return returnJson(map, response);
	}
	
	//Insert projects in aj for which inteview cadidate is accepted
	@RequestMapping(value = "/insertProjects", method = {RequestMethod.POST,RequestMethod.GET })
	public String insertProjects(@RequestParam("json") String json, Map<String, Object> map,HttpServletRequest request) throws JSONException {
		String loginId = UtilityHelper.getLoginId(request);
		Timestamp currentDate = UtilityHelper.getDate();
		interviewService.insertProjects(json,loginId,currentDate);
		return returnJson(map, response);
	}
	
}