package com.recruit.interview.presentation;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.recruit.candidate.service.Candidate;
import com.recruit.client.service.Client;
import com.recruit.interview.service.Interview;
import com.recruit.requirement.service.Requirement;
import com.recruit.util.SqlTimestampConverter;

public class InterviewParser {

	public static List<Interview> parseJSON(String json){
        List<Interview> interviews = new ArrayList<Interview>();
		JSONTokener tokenizer = new JSONTokener(json);
		try {
            JSONArray jExpenses = (JSONArray) tokenizer.nextValue();           
            for(int i=0 ; i<jExpenses.length(); i++){
            	JSONObject rObj = jExpenses.getJSONObject(i);
            	Interview interview = new Interview();
            	if(rObj.has("id")){
            		interview.setId(rObj.getInt("id"));     
            	}
            	if(rObj.has("created") && !rObj.getString("created").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		interview.setCreated((Timestamp)c.fromString(rObj.getString("created")));
            	}
            	if(rObj.has("lastUpdated") && !rObj.getString("lastUpdated").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		interview.setLastUpdated((Timestamp)c.fromString(rObj.getString("lastUpdated")));
            	}
            	if(rObj.has("time") && !rObj.getString("time").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		interview.setTime((Time)c.fromStringToTime(rObj.getString("time")));
            	}
            	if(rObj.has("interviewDate") && !rObj.getString("interviewDate").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		interview.setInterviewDate((Date)c.fromStringToDate(rObj.getString("interviewDate")));
            	}
            	if(rObj.has("currentTime") && !rObj.getString("currentTime").equals("null")){
            		SqlTimestampConverter c = new SqlTimestampConverter();
            		interview.setCurrentTime((Time)c.fromStringToTime(rObj.getString("currentTime")));
            	}
            	if (rObj.has("timeZone")) {
            		interview.setTimeZone(rObj.getString("timeZone"));
				}
            	if (rObj.has("comments")) {
            		interview.setComments(rObj.getString("comments"));
				}
            	if (rObj.has("lastUpdatedUser")) {
            		interview.setLastUpdatedUser(rObj.getString("lastUpdatedUser"));
				}
            	if (rObj.has("status")) {
					interview.setStatus(rObj.getString("status"));
				}
            	if (rObj.has("projectInserted")) {
					interview.setProjectInserted(rObj.getBoolean("projectInserted"));
				}
            	if (rObj.has("interview")) {
					interview.setInterview(rObj.getString("interview"));
				}
            	if (rObj.has("contactId")) {
					interview.setContactId(rObj.getString("contactId"));
				}
            	if (rObj.has("candidateId")) {
            		Candidate candidate = new Candidate();
            		candidate.setId(rObj.getInt("candidateId"));
            		interview.setCandidate(candidate);
				}else
					interview.setCandidate(null);
            	if (rObj.has("clientId")) {
            		Client client = new Client();
            		client.setId(rObj.getInt("clientId"));
            		interview.setClient(client);
				}else
					interview.setClient(null);
            	if (rObj.has("vendorId")) {
            		Client vendor = new Client();
            		vendor.setId(rObj.getInt("vendorId"));
            		interview.setVendor(vendor);
				}else
					interview.setVendor(null);
            	if (rObj.has("requirementId")) {
					Requirement requirement = new Requirement();
					requirement.setId(rObj.getInt("requirementId"));
					interview.setRequirement(requirement);
				}else
					interview.setRequirement(null);
            	
            	interviews.add(interview);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
        return interviews;
	}

}
