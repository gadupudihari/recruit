package com.recruit.interview.integration;

// Generated Sep 9, 2011 2:35:44 PM by Hibernate Tools 3.3.0.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.recruit.base.service.Param;
import com.recruit.base.service.SearchFilter;
import com.recruit.client.service.Client;
import com.recruit.interview.service.Interview;



/**
 * Home object for domain model class User.
 * @see com.recruit.user.service.User
 * @author Hibernate Tools
 */
@Repository
public class InterviewDAO {

	private static final Log log = LogFactory.getLog(InterviewDAO.class);

	@Autowired
	private  SessionFactory sessionFactory;

	public void persist(Interview transientInstance) {
		log.debug("persisting Interview instance");
		try {
			if(transientInstance.getId()!=null){
				sessionFactory.getCurrentSession().update(transientInstance);
			}else{
				sessionFactory.getCurrentSession().persist(transientInstance);
			}
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Interview instance) {
		log.debug("attaching dirty Interview instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}


	public void delete(Interview persistentInstance) {
		log.debug("deleting Interview instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Interview merge(Interview detachedInstance) {
		log.debug("merging Interview instance");
		try {
			Interview result = (Interview) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Interview findById(java.lang.Integer id) {
		log.debug("getting Interview instance with id: " + id);
		try {
			Interview instance = (Interview) sessionFactory.getCurrentSession()
					.get("com.recruit.interview.service.Interview", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Interview> findByExample(Client instance) {
		log.debug("finding Client instance by example");
		try {
			List<Interview> results = (List<Interview>) sessionFactory.getCurrentSession()
					.createCriteria("com.recruit.interview.service.Interview").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Interview> findByCriteria(SearchFilter filter) {
		try {
			String query = "from Interview t where 1=1 ";
			Map<String, Param> map = filter.getParamMap();

			if (map.get("id") != null) {
				query += " and t.id like '"+map.get("id").getStrValue()+"'";
			}
			if (map.get("skipId") != null) {
				query += " and t.id not like '"+map.get("skipId").getStrValue()+"'";
			}
			if (map.get("candidate") != null) {
				query += " and t.candidate.id like '"+map.get("candidate").getStrValue()+"'";
			}
			if (map.get("client") != null) {
				query += " and t.client.id like '"+map.get("client").getStrValue()+"'";
			}
			if (map.get("vendor") != null) {
				query += " and t.vendor.id like '"+map.get("vendor").getStrValue()+"'";
			}
			if (map.get("requirementId") != null) {
				if (map.get("requirementId").getStrValue().equalsIgnoreCase("Empty"))
					query += " and t.requirement.id is null ";
				else
					query += " and t.requirement.id like '"+map.get("requirementId").getStrValue()+"'";
			}
			if (map.get("interviewer") != null) {
				query += " and concat(',',t.contactId,',') like '%,"+map.get("interviewer").getStrValue()+",%'";
			}
			if (map.get("interviewDateFrom") != null) {
				query += " and t.interviewDate >= '"+map.get("interviewDateFrom").getStrValue()+"'";
			}
			if (map.get("interviewDateTo") != null) {
				query += " and t.interviewDate <= '"+map.get("interviewDateTo").getStrValue()+"'";
			}
			if (map.get("status") != null) {
				query += " and t.status = '"+map.get("status").getStrValue()+"'";
			}
			if (map.get("search") != null) {
				query +=" and Concat( CASE WHEN interviewer is null THEN '' ELSE interviewer END,' ', " +
							"CASE WHEN interviewDate is null THEN '' ELSE interviewDate END,' ', " +
							"CASE WHEN time is null THEN '' ELSE time END,' '," +
							"CASE WHEN created is null THEN '' ELSE created END,' '," +
							"CASE WHEN lastUpdated is null THEN '' ELSE lastUpdated END,' '," +
							"CASE WHEN lastUpdatedUser is null THEN '' ELSE lastUpdatedUser END,' '," +
							"CASE WHEN comments is null THEN '' ELSE comments END" +
						") like '%"+map.get("search").getStrValue()+"%'";
			}
			query += " order by interviewDate desc";
			
			System.out.println("Interview : "+query);
			List<Interview> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.setMaxResults(filter.getPageSize())
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Interview> findInterviews() {
		try {
			String query = "from Interview t where 1=1 and interviewDate = date_add(curdate(), interval 1 day)";
			
			List<Interview> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Interview> findbyCandidateId(Integer candidateId) {
		try {
			String query = "from Interview t where 1=1 and t.candidate.id like '"+candidateId+"'";
			
			System.out.println("Interview : "+query);
			List<Interview> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			return list;
			
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public Integer getAjVendorByName(String vendorName) {
		try {
			String query = "SELECT id FROM vendor where name = '"+vendorName+"'";
			
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			if (list.size() > 0)
				return (Integer)list.get(0);
			return null;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public Integer insertAjVendor(Client vendor) {
		try {
			String query = "insert into vendor(name,type,phone,address,email,city,state) select name,'INCOMING',contactNumber,address,email,city,state from recruit_clients where id="+vendor.getId();

			sessionFactory.getCurrentSession().createSQLQuery(query).executeUpdate();
			
			query = "SELECT id FROM vendor where name = '"+vendor.getName()+"'";
			
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			if (list.size() > 0)
				return (Integer)list.get(0);
			return null;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	public void insertAjProject(String projectQuery) {
		try {
			sessionFactory.getCurrentSession().createSQLQuery(projectQuery).executeUpdate();
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public boolean projectCodeExists(String code) {
		try {
			String query = "SELECT * FROM project where code = '"+code+"'";
			
			List list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.list();
			if (list.size() > 0)
				return true;
			return false;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean checkByCandidateJob(Integer candidateId, Integer requirementId) {
		try {
			String query = "from Interview t where t.candidate.id like '"+candidateId+"' and t.requirement.id like '"+requirementId+"' ";
			
			System.out.println("Interview : "+query);
			List<Interview> list = sessionFactory.getCurrentSession()
					.createQuery(query)
					.list();
			if(list.size() > 0)
				return true;
			return false;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Interview findBySubmissionId(int submissionId) {
		try {
			String query = "select id,interviewDate,time,currentTime,projectInserted,approxStartDate," +
					"(select group_concat(concat(firstname,' ',lastname)) from recruit_contacts c where find_in_set(id,t.contactId)) contactId " +
					" from recruit_interviews t where t.candidateid = (select candidateId from recruit_sub_requirements where deleted = false and id ="+submissionId+") " +
					" and requirementId = (select requirementid from recruit_sub_requirements where deleted = false and id ="+submissionId+")";
			
			System.out.println("Interview : "+query);
			List<Interview> list = sessionFactory.getCurrentSession()
					.createSQLQuery(query)
					.setResultTransformer(Transformers.aliasToBean(Interview.class))
					.list();
			if (list.size() > 0)
				list.get(0);
			return null;
		} catch (RuntimeException e) {
			log.error("get failed", e);
			throw e;
		}
	}
	
}