package com.recruit.interview.service;


import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import com.recruit.candidate.service.Candidate;
import com.recruit.client.service.Client;
import com.recruit.requirement.service.Requirement;

public class Interview implements java.io.Serializable{

	private static final long serialVersionUID = 6072286477030287927L;

	private Integer id;
	private Date interviewDate;
	private Time time;
	private Time currentTime;
	private String timeZone;
	private String comments;
	private String lastUpdatedUser;
	private Timestamp created;
	private Timestamp lastUpdated;
	private String status; 
	private Boolean projectInserted;
	private String interview;
	private Timestamp approxStartDate;
	private String contactId;
	private String employeeFeedback;
	private String vendorFeedback;
	
	private Candidate candidate;
	private Client client;
	private Client vendor;
	private Requirement requirement;
	
	public Interview() {}
	
	public Interview(int id) {
		this.id = id;
	}


	public Interview(Integer id, Date interviewDate, Time time, String comments, String lastUpdatedUser, String status, Boolean projectInserted,
			Timestamp created, Timestamp lastUpdated, String interview, Time currentTime, String timeZone, Timestamp approxStartDate,
			Candidate candidate, Client client, Client vendor,String contactId, Requirement requirement, String employeeFeedback, String vendorFeedback) {
		this.id = id;
		this.interviewDate = interviewDate;
		this.time = time;
		this.comments = comments;
		this.lastUpdatedUser = lastUpdatedUser;
		this.created = created;
		this.lastUpdated = lastUpdated;
		this.status = status;
		this.projectInserted = projectInserted;
		this.interview = interview;
		this.approxStartDate = approxStartDate;
		this.employeeFeedback = employeeFeedback;
		this.vendorFeedback = vendorFeedback;
		
		this.candidate = candidate;
		this.client = client;
		this.vendor = vendor;
		this.contactId = contactId;
		this.requirement = requirement;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getInterviewDate() {
		return interviewDate;
	}

	public void setInterviewDate(Date interviewDate) {
		this.interviewDate = interviewDate;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Client getVendor() {
		return vendor;
	}

	public void setVendor(Client vendor) {
		this.vendor = vendor;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public Requirement getRequirement() {
		return requirement;
	}

	public void setRequirement(Requirement requirement) {
		this.requirement = requirement;
	}

	public Boolean getProjectInserted() {
		return projectInserted;
	}

	public void setProjectInserted(Boolean projectInserted) {
		this.projectInserted = projectInserted;
	}

	public String getInterview() {
		return interview;
	}

	public void setInterview(String interview) {
		this.interview = interview;
	}

	public Time getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(Time currentTime) {
		this.currentTime = currentTime;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public Timestamp getApproxStartDate() {
		return approxStartDate;
	}

	public void setApproxStartDate(Timestamp approxStartDate) {
		this.approxStartDate = approxStartDate;
	}

	public String getEmployeeFeedback() {
		return employeeFeedback;
	}

	public void setEmployeeFeedback(String employeeFeedback) {
		this.employeeFeedback = employeeFeedback;
	}

	public String getVendorFeedback() {
		return vendorFeedback;
	}

	public void setVendorFeedback(String vendorFeedback) {
		this.vendorFeedback = vendorFeedback;
	}

	@Override
	public String toString() {
		return "Interview [id=" + id + ", interviewDate=" + interviewDate+ ", time=" + time + ", status=" + status + ", projectInserted=" + projectInserted 
				+ ", comments=" + comments + ", created=" + created+ ", lastUpdated=" + lastUpdated +", lastUpdatedUser=" + lastUpdatedUser 
				+ ", interview=" + interview + ", currentTime=" + currentTime + ", timeZone=" + timeZone + ", approxStartDate=" + approxStartDate 
				+ ", employeeFeedback=" + employeeFeedback + ", vendorFeedback=" + vendorFeedback
				+ "]";
	}
}