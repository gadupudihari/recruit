package com.recruit.interview.service;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.recruit.base.presentation.Response;
import com.recruit.base.service.SearchFilter;
import com.recruit.candidate.integration.CandidateDAO;
import com.recruit.candidate.service.Candidate;
import com.recruit.client.integration.ClientDAO;
import com.recruit.client.integration.ContactDAO;
import com.recruit.client.service.Client;
import com.recruit.client.service.Contact;
import com.recruit.interview.integration.InterviewDAO;
import com.recruit.reports.integration.ReportDAO;
import com.recruit.requirement.integration.RequirementsDAO;
import com.recruit.requirement.integration.Sub_RequirementDAO;
import com.recruit.requirement.service.Requirement;
import com.recruit.requirement.service.Sub_Requirement;
import com.recruit.user.integration.UserDAO;
import com.recruit.user.service.User;
import com.recruit.util.SendEmails;
import com.recruit.util.UtilityHelper;

@Service
public class InterviewService {

	@Autowired
	InterviewDAO interviewDAO;

	@Autowired
	UserDAO userDAO;
	
	@Autowired
	RequirementsDAO requirementsDAO;

	@Autowired
	CandidateDAO candidateDAO;
	
	@Autowired
	ReportDAO reportDAO;

	@Autowired
	Sub_RequirementDAO sub_RequirementDAO;

	@Autowired
	ClientDAO clientDAO;
	
	@Autowired
	ContactDAO contactDAO;
	
	@Transactional
	public List<Interview> getInterviews(SearchFilter filter) {
		return interviewDAO.findByCriteria(filter);
	}

	@Transactional
	public void save(List<Interview> interviews, String loginId,Timestamp createdDate) {
		for (Interview interview : interviews) {
			interview.setLastUpdatedUser(loginId);
			interview.setLastUpdated(createdDate);
			if (interview.getId() == null)
				interview.setCreated(createdDate);
			interviewDAO.persist(interview);
		}
	}

	@Transactional
	public void deleteInterview(Response response, Integer id) {
		Interview interview = interviewDAO.findById(id);
		interviewDAO.delete(interview);
		response.setSuccess(true);
	}

	//returns the interviews whose interview date is tomorrow
	@Transactional
	public List<Interview> findInterviews() {
		return interviewDAO.findInterviews();
	}

	// send interview email to all admins
	@SuppressWarnings("deprecation")
	@Transactional
	public void sendInterviewAlert(String json, String loginId, Timestamp currentDate) {
		try {
			String[] interviewIds = json.split(",");
			for (int i = 0; i < interviewIds.length; i++) {
				Interview interview = interviewDAO.findById(Integer.parseInt(interviewIds[i]));
				// if InterviewDate is less than current date 
				if (interview.getInterviewDate() != null && interview.getCandidate() != null && currentDate.getTime() > interview.getInterviewDate().getTime()) {
					String candidate = interview.getCandidate().getFirstName() + ' ' +interview.getCandidate().getLastName();
					List<User> admins = userDAO.getEmailAdmin();

					String subject = "Interview scheduled for "+candidate+" on "+interview.getInterviewDate();
					String body = "Interview scheduled for "+candidate+" on <b>"+interview.getInterviewDate()+"</b>";
					if (interview.getTime() != null){
						subject = "Interview scheduled for "+candidate+" on "+interview.getInterviewDate()+" at "+
								new SimpleDateFormat("h:mm a").format(interview.getTime())+" PST";
						body = "Interview scheduled for "+candidate+" on <b>"+interview.getInterviewDate()+" at "+
								new SimpleDateFormat("h:mm a").format(interview.getCurrentTime())+" "+interview.getTimeZone();
						if(! interview.getTimeZone().equals("PST"))
							body += " ~ "+new SimpleDateFormat("h:mm a").format(interview.getTime())+" PST";
						body += "</b>";
					}
					
					String vendor ="",client="", postingtitle="",module="",guide="",interviewer="",cityandstate="";
					if (interview.getVendor() != null)
						vendor = interview.getVendor().getName();
					if (interview.getClient() != null)
						client = interview.getClient().getName();
					if (interview.getRequirement() != null){
						postingtitle = interview.getRequirement().getPostingTitle();
						module = interview.getRequirement().getModule();
						cityandstate = interview.getRequirement().getCityAndState();
						if(interview.getRequirement().getSub_Requirements() != null && interview.getRequirement().getSub_Requirements().size() > 0){
							for (Sub_Requirement sub_Requirement : interview.getRequirement().getSub_Requirements()) {
								if (interview.getCandidate().getId() == sub_Requirement.getCandidateId()){
									guide = sub_Requirement.getGuide();
								}
							}
						}
					}
					List<Contact> contacts = contactDAO.findByIds(interview.getContactId());
					for (Contact contact : contacts) {
						interviewer += contact.getFirstName()+" "+contact.getLastName()+", ";
					}
					if (interviewer.length() > 0) 
						interviewer = interviewer.substring(0,interviewer.length()-2);
					
					body += "<br><br><table cellpadding='5' bgcolor='#575252'>" +
							"<tr bgcolor='#FFFFFF'><td>Vendor</td><td>"+vendor+"</td></tr>"+
							"<tr bgcolor='#FFFFFF'><td>Client</td><td>"+client+"</td></tr>"+
							"<tr bgcolor='#FFFFFF'><td>Posting title</td><td>"+postingtitle+"</td></tr>"+
							"<tr bgcolor='#FFFFFF'><td>Modules</td><td>"+module+"</td></tr>"+
							"<tr bgcolor='#FFFFFF'><td>Guide</td><td>"+guide+"</td></tr>"+
							"<tr bgcolor='#FFFFFF'><td>Interviewers</td><td>"+interviewer+"</td></tr>"+
							"<tr bgcolor='#FFFFFF'><td>City & State</td><td>"+cityandstate+"</td></tr></table>";

					String allEmailIds ="" ;
					Date interviewTime = new Date(); 
					if (interview.getTime() != null) {
						interviewTime.setDate(interview.getInterviewDate().getDate());
						interviewTime.setMonth(interview.getInterviewDate().getMonth());
						interviewTime.setYear(interview.getInterviewDate().getYear());
						interviewTime.setHours(interview.getTime().getHours());
						interviewTime.setMinutes(interview.getTime().getMinutes());
						interviewTime.setSeconds(interview.getTime().getSeconds());
					}
					//created date is today and interview date is today or tomorrow and time is greater than current time send email immediately
					//only if created date and last update date are same (checking for hours and minutes only 60*24)
					if ((interview.getCreated() != null && interview.getInterviewDate() != null && interview.getTime() != null ) 
							&& ((currentDate.getTime() - interview.getCreated().getTime())/(1000*60*60*24) == 0 ) //created date is current date 
							&& (interview.getCreated().getTime()/(60*24) == interview.getLastUpdated().getTime()/(60*24) ) // created date and last updated date
							&& ((((currentDate.getTime() - interview.getInterviewDate().getTime())/(1000*60*60*24) == 0 ) && (currentDate.before(interviewTime)))
									|| ((currentDate.getTime() - interview.getInterviewDate().getTime())/(1000*60*60*24) == 1 ))) {
						for (User user : admins) {
							allEmailIds += user.getEmailId()+",";
							
							//delete from email table of this same interview alert if exist
							String query = "Delete from email where alertName ='Interview' and date(alertDate) = date(now()) and refId = "+interview.getCandidate().getId();
							reportDAO.updateEmailRecords(query);
							
							query="insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,loginId,alertInDays,refId,application) " +
									"values('"+user.getEmailId()+"','" +currentDate.toString()+"','"+currentDate.toString()+"','"+subject+"',\""+body+"\","+false+",'"+"Interview"+"','"+
									"AD HOC"+"','"+loginId+"',"+0+",'"+interview.getCandidate().getId()+"','recruit')";
							
							reportDAO.updateEmailRecords(query);
						}//for
						//sending all email once
						if(allEmailIds.length() > 0){
							allEmailIds = allEmailIds.substring(0,allEmailIds.length()-1);
							SendEmails.sendHtmlEMail("Alert : "+subject, "Hi,"+"<br><br>"+body+"<br><br>"+"Regards"+"<br>"+"Team", allEmailIds);
						}
					}else if ((currentDate.getTime() - interview.getInterviewDate().getTime())/(1000*60*60*24) < 7 ) {
						//interview date is in the last 7 days email at the end of the day
						for (User user : admins) {
								//delete from email table of this same interview alert if exist
								String query = "Delete from email where alertName ='Interview' and date(alertDate) = date(now()) and refId = "+interview.getId();
								reportDAO.updateEmailRecords(query);

								//if interview date is in last 7 days it has to be logged into email table
								//it can not inserted by predefined methods because email class is not defined in recruit
								query="insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,loginId,alertInDays,refId,application) " +
										"values('"+user.getEmailId()+"','" +currentDate.toString()+"','"+currentDate.toString()+"','"+subject+"','"+body+"',"+true+",'"+"Interview"+"','"+
										"AD HOC"+"','"+loginId+"',"+0+",'"+interview.getId()+"','recruit')";
								
								System.err.println(query);
								reportDAO.updateEmailRecords(query);
						}//for
					}
				}//if
			}//for
		} catch (Exception e) {
			// TODO: handle exception
		}
	}//sendInterviewAlert

	@Transactional
	public Response saveInterview(Response response, Interview interview) {
		response.setSuccessMessage("Interview saved successfully.");
		if (interview.getRequirement() != null && interview.getRequirement().getId() != null) {
			Requirement requirement = requirementsDAO.findById(interview.getRequirement().getId());
			if (requirement == null) {
				response.setSuccess(false);
				response.setErrorMessage("Job opening does not exist for the given Job id");
				return response;
			}else {
				Boolean sub_updated = false;
				Boolean sub_exist = false;
				//requirement exists check for submission
				if (requirement.getSub_Requirements().size() > 0 && interview.getCandidate().getId()!= null && interview.getCandidate().getId() != null) {
					for (Sub_Requirement submission : requirement.getSub_Requirements()) {
						//filter shortlisted and status already updated 
						// status != Shortlisted and its length = 0
						if (! submission.getSubmissionStatus().equalsIgnoreCase("Shortlisted") && submission.getCandidateId().equals(interview.getCandidate().getId())) {
							sub_exist = true;
							if (submission.getSubmissionStatus().length() == 0 || submission.getSubmissionStatus().equalsIgnoreCase("Submitted")) {
								submission.setSubmissionStatus("I-Scheduled");
								submission.setStatusUpdated(UtilityHelper.getDate());
								submission.setLastUpdated(UtilityHelper.getDate());
								submission.setLastUpdatedUser("System");
								sub_RequirementDAO.attachDirty(submission);
								sub_updated = true;
							}
						}
					}
				}
				if (sub_updated) //status updated
					response.setSuccessMessage("Interview saved successfully, Corresponding Submission status is updated");
				else if (sub_exist && !sub_updated) //submission exist, but not updated 
					response.setSuccessMessage("Interview saved successfully.");
				else if (!sub_exist ) //submission not exist
					response.setSuccessMessage("Interview saved successfully, Submission does not exists for the corresponding candidate");
				else
					response.setErrorMessage("Interview saved successfully.");
			}
		}
		if (interview.getId() == null ) {
			interviewDAO.attachDirty(interview);
			response.setSuccessMessage("Interview saved successfully. --- Share to team ---");
		}else{
			Interview preInterview = interviewDAO.findById(interview.getId());
			interview.setCreated(preInterview.getCreated());
			interviewDAO.merge(interview);
		}
		response.setSuccess(true);
		response.setReturnVal(interview);
		return response;
	}

	@Transactional
	public void insertProjects(String json, String loginId,Timestamp currentDate) {
		String[] submissionIds = json.split(",");
		for (String submissionId : submissionIds) {
			Sub_Requirement sub_Requirement = sub_RequirementDAO.findById(Integer.parseInt(submissionId));
			if(sub_Requirement.getProjectInserted())
				return;
			Requirement requirement = sub_Requirement.getRequirement();
			Client client = sub_Requirement.getRequirement().getClient();
			Interview interview = interviewDAO.findBySubmissionId(Integer.parseInt(submissionId));

			Candidate candidate = null;
			Client vendor = null;
			String  interviewer = "";
			if (candidate == null )
				candidate = candidateDAO.findById(sub_Requirement.getCandidateId());
			if (vendor == null)
				vendor = clientDAO.findById(sub_Requirement.getVendorId());
				
			//get vendor id to insert project from aj using recruit vendor name
			Integer AjVendorId = interviewDAO.getAjVendorByName(vendor.getName());
			if (AjVendorId == null) {
				//if vendor not exist insert new vendor in AJ and get that id
				AjVendorId = interviewDAO.insertAjVendor(vendor);
			}
			//Insert employee if not exist
			if (candidate.getEmployeeId() == null || candidate.getEmployeeId() == 0 ) {
				candidateDAO.giveOnboarding(candidate.getId());
			}
			
			//project code
			String code = client.getName();
			if (code.indexOf(' ') > 0)
				code = code.substring(0,code.indexOf(" "));
			code += "-"+ vendor.getName() +"-"+ candidate.getEmployer();
			if (candidate.getFirstName().indexOf(" ") > 0)
				code += "-"+ candidate.getFirstName().substring(0,candidate.getFirstName().indexOf(" ")) + candidate.getLastName().charAt(0);
			else
				code += "-"+ candidate.getFirstName() + candidate.getLastName().charAt(0);
			
			//project comments
			String comments = "From Recruit--> " ;
					
			if (requirement != null && requirement.getPostingTitle() != null && requirement.getPostingTitle() != "")
				comments += "Posting Title : "+requirement.getPostingTitle() +", ";
			else
				comments += "Posting Title : N/A, ";

			if (requirement != null && requirement.getModule() != null && requirement.getModule() != "")
				comments += "Module : "+requirement.getModule() +", ";
			else
				comments += "Module : N/A, ";

			if (interviewDAO.projectCodeExists(code)) {
				code = code.substring(0,code.indexOf('-'))+'1'+code.substring(code.indexOf('-'));
			}
			
			String projectQuery;
			// query for inserting a project
			if (interview != null){
				interviewer = interview.getContactId();
				
				projectQuery = "insert into project(vendorId,code,name,employer,employeeType,projectcomments,source,created,lastUpdated,userName,startDate) " +
						"values( "+AjVendorId+",'"+code+"','"+client.getName()+"','"+candidate.getEmployer()+"','"+
						candidate.getType()+"','"+comments+"','RECRUIT', now(), now(),'AJ','"+interview.getApproxStartDate()+"')";
			}else{
				projectQuery = "insert into project(vendorId,code,name,employer,employeeType,projectcomments,source,created,lastUpdated,userName) " +
						"values( "+AjVendorId+",'"+code+"','"+client.getName()+"','"+candidate.getEmployer()+"','"+
						candidate.getType()+"','"+comments+"','RECRUIT', now(), now(),'AJ')";
			}
			System.out.println(projectQuery);
			interviewDAO.insertAjProject(projectQuery);
			
			//after project insertion in interview projectInserted is to be updated
			sub_Requirement.setProjectInserted(true);
			sub_RequirementDAO.merge(sub_Requirement);
			
			String subject = "Interview Success Alert";
			String body = candidate.getFirstName() + " "+ candidate.getLastName() +
					" interview is marked C-Accepted. The following are the project details " +
					"<br>Vendor : " + vendor.getName() +
					"<br>Client : " + client.getName() +
					"<br>Job ID : " + requirement.getId() +
					"<br>Interviewer : " + interviewer ;
			if(interview == null)
				body +="<br>Interview Date : N/A" ;
			else
				body +="<br>Interview Date : " + new SimpleDateFormat("MM/dd/yyyy").format(interview.getInterviewDate()) ;
			body += "<br><br><b>This Project is imported to AJ, please make sure that all the required documents are in place.</b>";
			
			List<User> admins = userDAO.getEmailAdmin();
			for (User user : admins) {
				//it has to be logged into email table
				//it can not inserted by predefined methods because email class is not defined in recruit
				String emailQuery="insert into Email (emailId,createdDate,alertDate,subject,message,active,alertName,alertType,loginId,alertInDays,refId,application,priority) " +
						"values('"+user.getEmailId()+"','" +currentDate.toString()+"','"+currentDate.toString()+"','"+subject+"','"+body+"',"+true+",'"+"Interview"+"','"+
						"AD HOC"+"','"+loginId+"',"+0+",'"+candidate.getId()+"','recruit',2)";
			
				reportDAO.updateEmailRecords(emailQuery);
			}
			
		}
		
	}

	@Transactional
	public Interview findById(int id) {
		return interviewDAO.findById(id);
	}

	@Transactional
	public Sub_Requirement findSub_RequirementById(int submissionId) {
		return sub_RequirementDAO.findById(submissionId);
	}

	@Transactional
	public Interview findBySubmissionId(int submissionId) {
		return interviewDAO.findBySubmissionId(submissionId);
	}
}
