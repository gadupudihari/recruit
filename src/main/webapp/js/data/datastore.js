//App Config
appConfig ={
	appWidth : 1200
};

//TODO: Need to move to a base class
function loadByCriteria(criteria){
	this.load({
	    params:{
	        start:0,
	        limit: config.pageSize,
	        json : Ext.JSON.encode(criteria)
	    }
	});
}
var reader = {type: 'json',root: 'rows',totalProperty: 'size'};
//var reader1 = {type: 'json',root: 'rows',totalProperty: 'size'};

//Work list store
Ext.define('tz.model.Module', {
    extend: 'Ext.data.Model',
    fields : [ {name: 'id',type:'int',useNull:true},{name: 'settingsId',type:'int',useNull:true},
               {name: 'inthelp1',type:'int',useNull:true},{name: 'inthelp2',type:'int',useNull:true},
               {name: 'trainer',type:'int',useNull:true},{name: 'resumeHelp',type:'int',useNull:true},
               {name: 'name',type:'string'},{name: 'value',type:'string'},
               {name: 'type',type:'string'},{name: 'notes',type:'string'},
               {name: 'module',type:'string'},{name: 'account',type:'string'},
            ]
});

Ext.define('tz.store.ModuleSettingsStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Module',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getModule?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    sorters: [{property: 'endDate',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
});



//Work list store
Ext.define('tz.model.WorkList', {
    extend: 'Ext.data.Model',
    fields : [ {name: 'id',type:'int'},{name: 'requirementId',type:'int'},
               {name: 'userId',type:'int'},{name: 'assignedUserId',type:'int'},
               {name: 'comments',type:'string'},{name: 'createdUser' , type:'string'},
               {name: 'lastUpdatedUser' , type:'string'},{name: 'deletedUser' , type:'string'},
               {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'deleted', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'postingDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               'candidateId','vendorId','clientId','cityAndState','jobOpeningStatus',
               'postingTitle','location','addInfoTodos','module','hotness',
            ]
});

Ext.define('tz.store.WorkListStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.WorkList',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getWorkList?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    sorters: [{property: 'endDate',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
});


/*** User Email DataModel ***/
Ext.define('tz.model.UserEmail', {
    extend: 'Ext.data.Model',
    fields : [ 
              {name: 'id', type: 'int'},
              {name: 'emailId', type: 'string'},
              {name: 'password', type: 'string'},
              {name: 'domain', type: 'string'},
              {name: 'smtp', type: 'string'},
              {name: 'comments', type: 'string'},
              {name: 'portNo', type: 'int'},
              ]
});


// Create the data store for the projects
Ext.define('tz.store.UserEmailStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.UserEmail',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getUserEmail?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    sorters: [{property: 'eMailId',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
});


/*** ProjectBillingRates DataModel ***/
Ext.define('tz.model.TNEProjectBillingRates', {
    extend: 'Ext.data.Model',
    fields : [ 
               {name: 'projectCode',type:'string'},
               {name: 'projectName',type:'string'},
               {name: 'vendorName',type:'string'},
               {name: 'projectStatus' , type:'string'},
               {name: 'employer',type:'string'},
               {name: 'employeeRate',type:'float'},
               {name: 'vendorRate',type:'float'},
               {name: 'invoicingTerms',type:'float'},
               {name: 'paymentTerms',type:'float'},
               {name: 'projectComments' , type:'string'},
               {name: 'projectStartDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'}, 
               {name: 'projectEndDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'approxEndDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               ]
});


// Create the data store for the projects
Ext.define('tz.store.TNEProjectSummaryStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.TNEProjectBillingRates',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/candidate/getProjectBillingRates?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    //sorters: [{property: 'projectStatus',direction: 'DESC'}],
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.model.Messages', {
    extend: 'Ext.data.Model',
    fields : [  {name: 'id', type: 'int'},
                {name: 'eMailId', type: 'string'},
                {name: 'createdDate', type: 'string'},
                {name: 'subject', type: 'string'},
                {name: 'message', type: 'string'},
                {name: 'alertInDays', type: 'int'},
                {name: 'refId', type: 'int'},
                {name: 'alertDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'completedDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'deletedDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'status',type: 'int'},
                {name: 'priority', type: 'int'},
                {name: 'pinned' , type:'boolean',useNull:true},
                {name: 'group', type: 'string'},
                {name: 'alertName', type: 'string'},
                {name: 'employeeName', type: 'string'},
                {name: 'hyperlink', type: 'string'},
             ]
});

//Create the data store for the alerts
Ext.define('tz.store.MessagesStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Messages',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/reports/getMessages?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    loadByCriteria: loadByCriteria,
    groupField:'status',
    getGroupString: function(instance) {
    	var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    	if (instance.get('status') == 1 ){
    		instance.data.group = 'Today'; 
    		return 'Today';
    	}else if (instance.get('status') == 2 ){
    		instance.data.group = 'Yesterday'; 
    		return 'Yesterday';
    	}else if (instance.get('status') == 3 ){
    		instance.data.group = 'This Week'; 
    		return 'This Week';
    	}else if (instance.get('status') == 4 ){
    		instance.data.group = 'This Month'; 
    		return 'This Month';
    	}else if (instance.get('status') >= 5 && instance.get('status') <= 10 ){
    		//if (instance.get('alertDate').getFullYear() == (new Date()).getFullYear())
    			//return months[instance.get('alertDate').getMonth()];
    		//else
    		instance.data.group = months[instance.get('alertDate').getMonth()]+' '+instance.get('alertDate').getFullYear(); 
   			return months[instance.get('alertDate').getMonth()]+' '+instance.get('alertDate').getFullYear();
    	}else{
    		instance.data.group = 'Earlier'; 
    		return 'Earlier';
    	}
    },
    
});

Ext.define('tz.model.Certification', {
    extend: 'Ext.data.Model',
    fields : [ {name: 'id',type:'int'},{name: 'candidateId',type:'int'},'candidate',
               {name: 'settingsId',type:'int',useNull:true},
               {name: 'dateAchieved', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'name',type:'string'},{name: 'status',type:'string'},
               {name: 'sponsoredBy',type:'string'},{name: 'reimbursementStatus',type:'string'},
               {name: 'version',type:'string'},{name: 'certificationComments',type:'string'},
               {name: 'epicUserWeb',type:'string'},{name: 'latestNVT',type:'string'},
               {name: 'expensesStatus',type:'string'},{name: 'paymentDetails',type:'string'},
               {name: 'epicInvoiceNo',type:'string'},{name: 'expenseComments',type:'string'},
               {name: 'verified',type:'string'},{name: 'epicInvoiceComments',type:'string'},
               {name: 'epicInvoiceAmount' , type:'float'},{name: 'cpsPaymentToEpic' , type:'float'},
               {name: 'expensesIncurred' , type:'float'},{name: 'expensesPaid' , type:'float'},
               {name: 'createdUser' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},
               {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.CertificationStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Certification',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/candidate/getCertifications?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    sorters: [{property: 'endDate',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.model.QuickAccess', {
    extend: 'Ext.data.Model',
    fields : [ 'id',
           	   {name: 'name' , type:'string'},
           	   {name: 'type' , type:'string'},
           	   {name: 'referenceId' , type:'string'}
            ]
});

Ext.define('tz.store.QuickAccessStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.QuickAccess',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/quickAccess/getAllEmployees?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    sorters: [{property: 'type',direction: 'ASC'},{property: 'name',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
});


//Sub_Vendor data model
Ext.define('tz.model.Sub_Vendor', {
    extend: 'Ext.data.Model',
    fields : [ 'id','requirementId','vendorId','contactId','email','cellPhone'
            ]
});

Ext.define('tz.store.Sub_VendorStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Sub_Vendor',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getSubVendors?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'date',direction: 'DESC'}],
});

//Research data model
Ext.define('tz.model.Sub_Requirement', {
    extend: 'Ext.data.Model',
    fields : [ 'id','requirementId','candidateId','vendorId','submittedTime',
              	{name: 'lastUpdatedUser' , type:'string'},{name: 'submissionStatus', type: 'string'},
              	{name: 'comments', type: 'string'},{name: 'resumeLink', type: 'string'},
              	{name: 'guide', type: 'string'},{name: 'reference', type: 'string'},
               	{name: 'submittedDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               	{name: 'postingDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               	{name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'statusUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'projectStartDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'immigrationVerified', type: 'string'},
                {name: 'projectInserted' , type:'boolean'},{name: 'deleted' , type:'boolean'},
                'submittedRate1','submittedRate2', 'candidateExpectedRate','candidate','reqCityAndState',
                'cityAndState', 'relocate', 'travel', 'availability', 'vendor','client',
                'postingTitle','location','addInfoTodos','module','hotness','resumeHelp','interviewHelp','jobHelp',
                'clientId','contactId','emailId','interviewDate','alert','contact',
                {name: 'score', type: 'int',useNull:true}
            ]
});

Ext.define('tz.store.Sub_RequirementStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Sub_Requirement',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getSubRequirements?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'deleted',direction: 'ASC'},{property: 'submittedDate',direction: 'DESC'}],
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			if(s.data.items == null || s.data.items.lenght == 0){
				return;
			}
			for ( var i=0; i<s.data.items.length; i++) {
				var rec = s.data.items[i];
				if (rec.data.submittedTime == null || rec.data.submittedTime == ''){
					rec.data.submittedTime = timeToString(rec.data.submittedDate);
				}
			}
		}
	}

});

Ext.define('tz.store.HighContactsStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Sub_Requirement',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getHighContacts?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'statusUpdated',direction: 'DESC'}],
});

//Research data model
Ext.define('tz.model.Research', {
    extend: 'Ext.data.Model',
    fields : [ 'id','requirementId',{name: 'serialNo',type:'int',useNull:true},
               	{name: 'task' , type:'string'},{name: 'status' , type:'string'},
               	{name: 'comments' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},
               	{name: 'potentialCandidates' , type:'string'},
               	{name: 'date', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               	{name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.ResearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Research',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getTasks?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'serialNo',direction: 'DESC'}],
});

//Interview data model
Ext.define('tz.model.Interview', {
    extend: 'Ext.data.Model',
    fields : [ 'id','clientId','vendorId','candidateId','contactId','requirementId','interviewDate','time','currentTime',
               'client','vendor','candidate',
               	{name: 'interviewer' , type:'string'},{name: 'status' , type:'string'},{name: 'timeZone' , type:'string'},
    			{name: 'comments' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},
    			{name: 'skillSet' , type:'string'},{name: 'skillSetIds' , type:'string'},
    			{name: 'interview' , type:'string'},{name: 'projectInserted' , type:'boolean'},
    			{name: 'employeeFeedback' , type:'string'},{name: 'vendorFeedback' , type:'string'},
    			{name: 'approxStartDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.InterviewStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Interview',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/interview/getInterviews?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'interviewDate',direction: 'DESC'}],
});

//contacts data model
Ext.define('tz.model.Contact', {
    extend: 'Ext.data.Model',
    fields : [ 'id','clientId','candidateId','candidate','contactName',
               	{name: 'confidentiality',type:'int',useNull:true},{name: 'priority',type:'int',useNull:true},
               	{name: 'firstName' , type:'string'},{name: 'lastName' , type:'string'},
               	{name: 'email' , type:'string'},{name: 'jobTitle' , type:'string'},{name: 'clientName' , type:'string'},
               	{name: 'workPhone' , type:'string'},{name: 'internetLink' , type:'string'},{name: 'score',type:'int',useNull:true},
               	{name: 'cellPhone' , type:'string'},{name: 'type' , type:'string'},{name: 'extension' , type:'string'},
    			{name: 'comments' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},{name: 'interviews' , type:'int'},
    			{name: 'type' , type:'string'},{name: 'role' , type:'string'},{name: 'gender' , type:'string'},
    			{name: 'module' , type:'string'},{name: 'SMEComments' , type:'string'},{name: 'placementComments', type: 'string'},
                {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'candidateDeleted' , type:'boolean',useNull:true},
                'city','state','clientType',
                {name: 'skillSet' , type:'string'},{name: 'targetSkillSet', type: 'string'},
                {name: 'certifications', type: 'string'},{name: 'targetRoles', type: 'string'},
                {name: 'roleSet', type: 'string'},{name: 'roleSetIds', type: 'string'},
                {name: 'canRoleSet', type: 'string'},{name: 'canRoleSetIds', type: 'string'},
                {name: 'skillSetGroup', type: 'string'},{name: 'skillSetGroupIds', type: 'string'},
                {name: 'smeType', type: 'string'},{name: 'source', type: 'string'},
                {name: 'referral', type: 'string'},{name: 'resumeLink', type: 'string'},
            ]
});

Ext.define('tz.store.ContactStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Contact',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/client/getContacts?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'firstName',direction: 'ASC'}],
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			for ( var i=0; i<s.data.items.length; i++) {
				var contact = s.data.items[i].data;
				contact.fullName = contact.firstName+' '+contact.lastName;
			}
		}
	} 
});

Ext.define('tz.store.ContactSearchStore', {
	extend : 'Ext.data.Store',
	fields : [ 'id','clientId',{name: 'fullName' , type:'string'},{name: 'email' , type:'string'},
	           {name: 'cellPhone' , type:'string'},{name: 'internetLink' , type:'string'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/client/getAllContacts?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    sorters: [{property: 'fullName',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.model.Client', {
    extend: 'Ext.data.Model',
    fields : [ 'id',{name: 'name' , type:'string'},{name: 'contactNumber' , type:'string'},{name: 'score',type:'int',useNull:true},
               	{name: 'recruiter',type:'int',useNull:true},{name: 'confidentiality',type:'int',useNull:true},
    			{name: 'fax' , type:'string'},{name: 'website' , type:'string'},{name: 'email' , type:'string'},
    			{name: 'industry' , type:'string'},{name: 'about' , type:'string'},
    			{name: 'type' , type:'string'},{name: 'address' , type:'string'},
    			{name: 'comments' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},
    			{name: 'contractorId' , type:'string'},{name: 'fullTimeId' , type:'string'},
    			{name: 'city', type: 'string'},{name: 'state', type: 'string'},
    			{name: 'referenceId', type: 'string'},
                {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'helpCandidate' , type:'string'},{name: 'helpCandidateComments' , type:'string'},
                {name: 'skillSetGroupIds', type: 'string'},
            ]
});

Ext.define('tz.store.ClientStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Client',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/client/getClients?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },
    sorters: [{property: 'score',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.store.ClientSearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Client',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/client/getAllClients?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'name',direction: 'ASC'}],
});

Ext.define('tz.model.Candidate', {
    extend: 'Ext.data.Model',
    fields : [ 'id','contacts','candidate_SkillSets','candidate_TargetSkillSets','initials',
               {name: 'priority',type:'int',useNull:true},
               {name: 'confidentiality',type:'int',useNull:true},
               {name: 'employeeId',type:'int',useNull:true},
               {name: 'version' , type:'int',useNull:true},
               {name: 'firstName' , type:'string'},
               {name: 'lastName' , type:'string'},
               {name: 'gender' , type:'string'},
               {name: 'emailId' , type:'string'},
               {name: 'contactNumber' , type:'string'},
               {name: 'contactAddress' , type:'string'},
               {name: 'currentJobTitle' , type:'string'},
               {name: 'source' , type:'string'},
               {name: 'type' , type:'string'},
               {name: 'p1comments' , type:'string'},
               {name: 'p2comments' , type:'string'},
               {name: 'lastUpdatedUser' , type:'string'},
               {name: 'highestQualification' , type:'string'},
               {name: 'education', type: 'string'},
               {name: 'referral' , type:'string'},
               {name: 'followUp', type: 'string'},
               {name: 'openToCTH', type: 'string'},
               {name: 'marketingStatus' , type:'string'},
               {name: 'relocate' , type:'boolean',useNull:true},
               {name: 'travel' , type:'boolean',useNull:true},
               {name: 'goodCandidateForRemote' , type:'boolean',useNull:true},
               {name: 'cityAndState', type: 'string'},
               {name: 'location', type: 'string'},
               {name: 'immigrationStatus', type: 'string'},
               {name: 'immigrationVerified', type: 'string'},
               {name: 'employmentType', type: 'string'},
               {name: 'employer' , type:'string'},
               {name: 'expectedRate', type: 'string'},
               {name: 'rateFrom', type:'float',useNull:true},
               {name: 'rateTo', type:'float',useNull:true},
               {name: 'aboutPartner', type: 'string'},
               {name: 'communication', type: 'string'},
               {name: 'experience', type: 'string'},
               {name: 'personality', type: 'string'},
               {name: 'alert', type: 'string'},
               {name: 'availability', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'startDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'submittedDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'req_submittedDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'matchScore', type: 'int'},
               {name: 'matchScoreDetails', type: 'string'},
               {name: 'submissions', type: 'string'},
               {name: 'skillSet' , type:'string'},
               {name: 'targetRoles', type: 'string'},
               {name: 'targetSkillSet', type: 'string'},
               {name: 'currentRoles', type: 'string'},
               {name: 'oldSkillSet', type: 'string'},
               {name: 'allCertifications', type: 'string'},
               {name: 'otherSkillSet', type: 'string'},
               {name: 'resumeHelp', type: 'string'},
               {name: 'interviewHelp', type: 'string'},
               {name: 'jobHelp', type: 'string'},
               {name: 'resumeHelpComments', type: 'string'},
               {name: 'interviewHelpComments', type: 'string'},
               {name: 'jobHelpComments', type: 'string'},
               {name: 'contactId' , type:'string'},
               {name: 'resumeLink', type: 'string'},
               {name: 'contactDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'technicalScore',type:'int',useNull:true},
               {name: 'skillSetIds', type: 'string'},
               {name: 'targetSkillSetIds', type: 'string'},
               {name: 'roleSet', type: 'string'},
               {name: 'roleSetIds', type: 'string'},
               {name: 'skillSetGroupIds', type: 'string'},
               {name: 'billingRange', type: 'string'},
            ]
});

Ext.define('tz.store.CandidateStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Candidate',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/candidate/getCandidates?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },
    sorters: [{property: 'marketingStatus',direction: 'ASC'},{property: 'priority',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			for ( var i=0; i<s.data.items.length; i++) {
				var candidate = s.data.items[i].data;
				candidate.fullName = candidate.firstName+' '+candidate.lastName;
			}
		},
	} 
});

Ext.define('tz.store.CandidateSearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Candidate',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/candidate/getAllCandidates?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    sorter : 'firstName',
    sorters: [{property: 'firstName',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			for ( var i=0; i<s.data.items.length; i++) {
				var candidate = s.data.items[i].data;
				candidate.fullName = candidate.firstName+' '+candidate.lastName;
			}
		},
	} 
});

Ext.define('tz.store.ContractorsStore', {
	extend : 'Ext.data.Store',
	fields : [ 'id',{name: 'fullName' , type:'string'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/candidate/getAllContractors?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    sorter : 'fullName',
    sorters: [{property: 'fullName',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.model.Requirement', {
    extend: 'Ext.data.Model',
    fields : [ 'id','candidateId','vendorId','contactId','clientId','recruiter','sub_Requirements',
               	{name: 'resume' , type:'int',useNull:true},{name: 'version' , type:'int',useNull:true},
               	{name: 'requirementNo' , type:'int',useNull:true},
               	{name: 'submittedDate' , type:'string'},
               	{name: 'hotness' , type:'string'},
    			{name: 'location' , type:'string'},{name: 'requirement' , type:'string'},
    			{name: 'module' , type:'string'},{name: 'contactPerson' , type:'string'},
    			{name: 'communication' , type:'string'},{name: 'researchComments' , type:'string'},
    			{name: 'addInfo_Todos' , type:'string'},{name: 'source' , type:'string'},
    			{name: 'rateSubmitted' , type:'string'},{name: 'postingTitle' , type:'string'},
    			{name: 'jobOpeningStatus' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},
    			{name: 'comments' , type:'string'},{name: 'employer' , type:'string'},{name: 'publicLink' , type:'string'},
    			{name: 'submissionDate' , type:'string'},{name: 'experience' , type:'string'},
    			{name: 'helpCandidate' , type:'string'},{name: 'helpCandidateComments' , type:'string'},
                {name: 'postingDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'secondaryModule', type: 'string'},{name: 'targetRoles', type: 'string'},
                {name: 'certifications', type: 'string'},{name: 'certificationIds', type: 'string'},
                {name: 'cityAndState', type: 'string'},{name: 'alert', type: 'string'},
                {name: 'remote' , type:'boolean',useNull:true},{name: 'relocate' , type:'boolean',useNull:true},{name: 'travel' , type:'boolean',useNull:true},
                {name: 'tAndEPaid' , type:'boolean',useNull:true},{name: 'tAndENotPaid' , type:'boolean',useNull:true},
                {name: 'matchScore', type: 'int'},{name: 'matchScoreDetails', type: 'string'},
                {name: 'shortlistedCount' , type:'int'},{name: 'shortlistedCandidates', type: 'string'},
                {name: 'pendingTasksCount', type: 'int'},{name: 'pendingTasks', type: 'string'},
                {name: 'skillSet' , type:'string'},{name: 'skillSetIds', type: 'string'},
            ]
});

Ext.define('tz.store.RequirementStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Requirement',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getRequirements?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.store.RequirementSearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Requirement',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getAllRequirements?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.model.PlacementLeads', {
    extend: 'Ext.data.Model',
    fields : [ 'id',{name: 'priority',type:'int',useNull:true},
               {name: 'person' , type:'string'},{name: 'sourceOfInformation' , type:'string'},
    			{name: 'typeOfInformation' , type:'string'},{name: 'information' , type:'string'},
    			{name: 'hospital_Entity' , type:'string'},{name: 'location' , type:'string'},
    			{name: 'state', type: 'string'},{name: 'link1' , type:'string'},{name: 'link2' , type:'string'},
    			{name: 'outreach' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},{name: 'comments' , type:'string'},
    			{name: 'liveDate', type: 'string'},
    			{name: 'date', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'source_Sublevel', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.PlacementLeadsStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.PlacementLeads',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/placementLeads/getPlacementLeads?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    sorters: [{property: 'date',direction: 'DESC'}],
    loadByCriteria: loadByCriteria,
});

/*** SkillsetGroup DataModel ***/
Ext.define('tz.model.SkillsetGroup', {
    extend: 'Ext.data.Model',
    fields : [ {name: 'id',type:'int'},
               {name: 'name',type:'string'},
               {name: 'type',type:'string'},
               ]
});

//Create the data store for the Settings
Ext.define('tz.store.SkillsetGroupStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.SkillsetGroup',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/searchSkillsetGroup?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },
    sorters: [{property: 'name',direction: 'ASC'}],
    loadByCriteria: loadByCriteria
});

/*** Settings DataModel ***/
Ext.define('tz.model.Settings', {
    extend: 'Ext.data.Model',
    fields : [ {name: 'id',type:'int'},{name: 'orderNo',type:'int',useNull:true},
               {name: 'name',type:'string'},{name: 'value',type:'string'},
               {name: 'type',type:'string'},{name: 'notes',type:'string'},
               {name: 'module',type:'string'},{name: 'account',type:'string'},
               {name: 'defaultSelection', type:'boolean',useNull:true},
               {name: 'dependency', type:'boolean',useNull:true},
               {name: 'tableName',type:'string'},
               {name: 'acronym',type:'string'},
               {name: 'skillsetGroupId',type:'int',useNull:true},
               {name: 'skillsetGroupName',type:'string'},
               ]
});

//Create the data store for the Settings
Ext.define('tz.store.SettingsStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/search?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },
    loadByCriteria: loadByCriteria
});

//Create the data store for the Settings Grid
Ext.define('tz.store.SettingsSearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/searchSettings?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },
    loadByCriteria: loadByCriteria
});

//Vendor Type combo in Vendors 
Ext.define('tz.store.ClientTypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'name',direction: 'ASC'}],
});

//Marketing Status in Recruiting 
Ext.define('tz.store.MarketingStatusStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'value',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

//Hotness in Recruiting 
Ext.define('tz.store.HotnessStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'name',direction: 'ASC'}],
});


//Information Type in Recruiting 
Ext.define('tz.store.InformationTypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'value',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

//Job opening Status in job openings 
Ext.define('tz.store.JobOpeningStatusStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

//Qualification in candidates
Ext.define('tz.store.QualificationStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

// Candidate Type in candidates
Ext.define('tz.store.CandidateTypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

//Immigration type combo in candidates 
Ext.define('tz.store.ImmigrationTypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.store.EmploymentTypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'name',direction: 'ASC'}],
});

//Employer combo 
Ext.define('tz.store.EmployerStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'name',direction: 'ASC'}],
});

Ext.define('tz.store.ResearchStatusStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
	sorters: [{property: 'orderNo',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

//Certification status combo in certifications
Ext.define('tz.store.CertificationStatusStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

//Certification status combo in certifications
Ext.define('tz.store.CertificationSponsoredStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

//Candidate Source in candidates
Ext.define('tz.store.CandidateSourceStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

//Submission status in submitted candidates
Ext.define('tz.store.SubmissionStatusStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'orderNo',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

//State in placements
Ext.define('tz.store.StateStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.store.CommunicationStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.store.PersonalityStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.store.ExperienceStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'id',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.store.TargetRolesStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});


Ext.define('tz.store.SkillSetStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Module',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getModuleCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'name',direction: 'ASC'}],
});

Ext.define('tz.store.CertificationsStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.store.ContactTypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.store.SMETypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});

Ext.define('tz.store.ContactRoleStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'order',direction: 'ASC'}],
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/getCategory?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
});


Ext.define('tz.model.User', {
    extend: 'Ext.data.Model',
    fields : [ {name: 'id', type: 'int'},'firstName', 'lastName','active','emailId','alternateEmailId','loginId','password','authority','excludeConfidential','fullName' ]
});

Ext.define('tz.store.UserStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.User',
	//buffered: true,
	pageSize: 200,
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/user/getUsers?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    sorters: [{property: 'firstName',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			for ( var i=0; i<s.data.items.length; i++) {
				var record = s.data.items[i].data;
				record.fullName = record.firstName+' '+record.lastName;
			}
		},
	} 
});

Ext.define('tz.store.UserSearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.User',
	//buffered: true,
	pageSize: 200,
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/user/list?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    sorters: [{property: 'firstName',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			for ( var i=0; i<s.data.items.length; i++) {
				var user = s.data.items[i].data;
				user.fullName = user.firstName+' '+user.lastName;
			}
		}
	} 
});

/**
 * Master list of all the data stores here
 */
Ext.define('tz.DataStores', {
    constructor: function(name) {
    	
    	// Static Data Stores
    	//The data store containing the list of states
    	this.boolStore = Ext.create('Ext.data.Store', {
    	    fields: ['value', 'name'],
    	    data : [
    	        {"name":"YES", "value":true},
    	        {"name":"NO", "value":false}
    	    ]
    	});
    	
    	this.yesNoStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"name":"YES", "value":"YES"},
	    	        {"name":"NO", "value":"NO"},
	    	        {"name":"N/A", "value":"N/A"},
	    	    ]
        });

    	this.verifiedStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"name":"YES", "value":"YES"},
	    	        {"name":"NO", "value":"NO"},
	    	        {"name":"Not Needed", "value":"Not Needed"},
	    	    ]
        });

    	this.yesOrNoStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"name":"YES", "value":"YES"},
	    	        {"name":"NO", "value":"NO"},
	    	    ]
        });

    	this.helpCandidateStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"name":"100-Yes", "value":"100-Yes"},
	    	        {"name":"0-No", "value":"0-No"},
	    	        {"name":"Other", "value":"Other"},
	    	    ]
        });

    	this.authorityStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"name":"ADMIN", "value":"ADMIN"},
	    	        {"name":"RECRUITER", "value":"RECRUITER"}
	    	    ]
        });
    	
    	this.jobResumeStore = Ext.create('Ext.data.Store', {
    	    fields: ['name', 'value'],
    	    data : [
    	            {"name":null, "value":'Select--'},
	    	        {"name":0, "value":0},
	    	        {"name":10, "value":10},
	    	        {"name":25, "value":25},
	    	        {"name":100, "value":100},
	    	    ]
    	}); 

    	this.candidateLocationStore = Ext.create('Ext.data.Store', {
    	    fields: ['name', 'value'],
    	    data : [
	    	        {"name":"Northeast", "value":"Northeast"},
	    	        {"name":"Northwest", "value":"Northwest"},
	    	        {"name":"West", "value":"West"},
	    	        {"name":"Midwest", "value":"Midwest"},
	    	        {"name":"Southeast", "value":"Southeast"},
	    	        {"name":"Southwest", "value":"Southwest"},
	    	    ]
    	}); 

        this.monthStore = Ext.create('Ext.data.Store', {
            fields: ['label', 'value'],
            data : [
	    	        {"label":"1 Week", "value":"One Week"},
	    	        {"label":"2 Weeks", "value":"Two Weeks"},
	    	        {"label":"1 Month", "value":"One Month"},
	    	        {"label":"3 Months", "value":"Three Months"},
	    	    ]
        });

        this.domineStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"value":"Gmail", "name":"Gmail"},
	    	        {"value":"Yahoo", "name":"Yahoo"},
	    	        {"value":"Outlook", "name":"Outlook"},
	    	        {"value":"Other", "name":"Other"},
	    	    ]
        });
        
    	this.moduleStore = Ext.create('Ext.data.Store', {
    	    fields: ['name', 'value'],
    	    data : [
	    	        {"name":"Home", "value":"Home"},
	    	        {"name":"Job Openings", "value":"Job Openings"},
	    	        {"name":"Candidates", "value":"Candidates"},
	    	        {"name":"Clients/Vendors", "value":"Clients/Vendors"},
	    	        {"name":"Contacts", "value":"Contacts"},
	    	        {"name":"Interviews", "value":"Interviews"},
	    	        {"name":"Placement Leads", "value":"Placement Leads"},
	    	        {"name":"Reports", "value":"Reports"},
	    	        {"name":"Users", "value":"Users"},
	    	        {"name":"Settings", "value":"Settings"},
	    	    ]
    	});

    	this.genderStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"name":"Male", "value":"Male"},
	    	        {"name":"Female", "value":"Female"}
	    	    ]
        });

    	this.allGenderStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"name":"Male", "value":"Male"},
	    	        {"name":"Female", "value":"Female"},
	    	        {"name":"", "value":"All"},
	    	    ]
        });

    	// Define the vendor store here
    	this.userStore = new tz.store.UserStore();
    	this.recruiterStore = new tz.store.UserStore();
    	this.userSearchStore = new tz.store.UserSearchStore();
    	this.settingsStore = new tz.store.SettingsStore(); 
    	this.settingsSearchStore = new tz.store.SettingsSearchStore();
    	this.candidateStore = new tz.store.CandidateStore();
    	this.requirementStore = new tz.store.RequirementStore();
    	this.placementLeadsStore = new tz.store.PlacementLeadsStore();
    	this.marketingStatusStore = new tz.store.MarketingStatusStore();
    	this.hotnessStore = new tz.store.HotnessStore();
    	this.informationTypeStore = new tz.store.InformationTypeStore();
    	this.jobOpeningStatusStore = new tz.store.JobOpeningStatusStore();
    	this.clientTypeStore = new tz.store.ClientTypeStore();
    	this.clientStore = new tz.store.ClientStore();
    	this.vendorStore = new tz.store.ClientSearchStore();
    	this.suggstedVendorStore = new tz.store.ClientStore();
    	this.qualificationStore = new tz.store.QualificationStore();
    	this.candidateTypeStore = new tz.store.CandidateTypeStore();
    	this.contactStore = new tz.store.ContactStore();
    	this.contactSearchStore = new tz.store.ContactSearchStore();
    	this.contactMaleStore = new tz.store.ContactStore();
    	this.contactFemaleStore = new tz.store.ContactStore();
    	this.clientSearchStore = new tz.store.ClientSearchStore();
    	this.client_vendorStore = new tz.store.ClientSearchStore();
    	this.candidateSearchStore = new tz.store.CandidateSearchStore();
    	this.candidateSearchStore2 = new tz.store.CandidateSearchStore();
    	this.interviewStore = new tz.store.InterviewStore();
    	this.immigrationTypeStore = new tz.store.ImmigrationTypeStore();
    	this.employmentTypeStore = new tz.store.EmploymentTypeStore();
    	this.req_candidateStore = new tz.store.CandidateStore();
    	this.candidate_requirementStore = new tz.store.RequirementStore();
    	this.suggestedRequirementStore = new tz.store.RequirementStore();
    	this.requirementSearchStore = new tz.store.RequirementStore();
    	this.shortlistedJobsStore = new tz.store.RequirementStore();
    	this.employerStore = new tz.store.EmployerStore();
    	this.researchStore = new tz.store.ResearchStore();
    	this.researchReportStore = new tz.store.ResearchStore();
    	this.researchStatusStore = new tz.store.ResearchStatusStore();
    	this.subRequirementStore = new tz.store.Sub_RequirementStore();
    	this.subRequirementHomeStore = new tz.store.Sub_RequirementStore();
    	this.subVendorStore = new tz.store.Sub_VendorStore();
    	this.potentialCandidatesStore = new tz.store.CandidateSearchStore();
    	this.shortlistedCandidatesStore = new tz.store.Sub_RequirementStore();
    	this.quickAccessStore = new tz.store.QuickAccessStore(); 	
    	this.quickCandidateStore = new tz.store.CandidateStore();
    	this.quickRequirementStore = new tz.store.Sub_RequirementStore();
    	this.quickVendorStore = new tz.store.ClientStore();
    	this.quickClientStore = new tz.store.ClientStore();
    	this.quickContactStore = new tz.store.ContactStore();
    	this.quickInterviewStore = new tz.store.InterviewStore();
    	this.quickCertificationStore = new tz.store.CertificationStore();
    	this.quickProjectStore = new tz.store.TNEProjectSummaryStore();
    	
    	this.certificationStore = new tz.store.CertificationStore();
    	this.certificationStatusStore = new tz.store.CertificationStatusStore();
    	this.certificationSponsoredStore = new tz.store.CertificationSponsoredStore();
    	this.contractorsStore = new tz.store.ContractorsStore();
    	this.candidateSourceStore = new tz.store.CandidateSourceStore();
    	this.submissionStatusStore = new tz.store.SubmissionStatusStore();
    	this.stateStore = new tz.store.StateStore();
    	this.messagesStore = new tz.store.MessagesStore();
    	this.messagesPopupStore = new tz.store.MessagesStore();
    	this.tneProjectSummaryStore = new tz.store.TNEProjectSummaryStore();
    	
    	this.homeCandidateStore = new tz.store.CandidateStore();
    	this.homeRequirementStore1 = new tz.store.RequirementSearchStore();
    	this.homeRequirementStore2 = new tz.store.Sub_RequirementStore();
    	this.candidateEmailStore = new tz.store.CandidateStore();
    	this.highContactsStore = new tz.store.HighContactsStore();
    	
    	this.communicationStore = new tz.store.CommunicationStore();
    	this.personalityStore = new tz.store.PersonalityStore();
    	this.skillSetStore = new tz.store.SkillSetStore();
    	this.targetRolesStore = new tz.store.TargetRolesStore();
    	this.experienceStore = new tz.store.ExperienceStore();
    	this.certificationsStore = new tz.store.CertificationsStore();
    	this.userEmailStore = new tz.store.UserEmailStore();
    	this.contactTypeStore = new tz.store.ContactTypeStore();
    	this.smeTypeStore = new tz.store.SMETypeStore();
    	this.contactRoleStore = new tz.store.ContactRoleStore();
    	this.workListStore = new tz.store.WorkListStore();
    	this.moduleSettingsStore = new tz.store.ModuleSettingsStore();
    	this.skillsetGroupStore = new tz.store.SkillsetGroupStore();
    	this.homeCandidateStore2 = new tz.store.CandidateStore();
    	this.reqSmeContactStore = new tz.store.ContactStore();
    }
});