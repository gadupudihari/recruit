Ext.define('tz.ui.BaseFormPanel', {
	extend : 'Ext.form.Panel',
	initComponent : function() {
		this.callParent(arguments);
	},

	clearMessage : function(msg) {
		if (this.message.el) {
			this.message.el.dom.innerHTML = '';
		}
	},
	
	updateMessage : function(msg) {
		if (this.message.el) {
			this.message.el.dom.innerHTML = '<div class="msgPanel"><p ><img src="images/icon_checkmark_dark.gif"/>&nbsp;&nbsp;' + msg + '</p></div>';
		}
	},
	
	updateInfo : function(msg) {
		if (this.message.el) {
			this.message.el.dom.innerHTML = '<div class="infoMsgPanel"><p ><img src="images/icon_info.gif"/>&nbsp;&nbsp;' + msg + '</p></div>';
		}
	},

	updateError : function(msg) {
		if (this.message.el) {
			this.message.el.dom.innerHTML = '<div class="errorMsgPanel"><p ><img src="images/icon_error.gif"/>&nbsp;&nbsp;' + msg + '</p></div>';
		}
	},
	
	getMessageComp : function(msg) {
		if (!msg) {
			msg = '';
		}
		this.message = new Ext.Component({
			xtype : 'component',
			html : ''
		});
		return this.message;
	},

	updateHeading : function(msg) {
		if (this.heading.el) {
			this.heading.el.dom.innerHTML = '<div class="subHeading">' + msg + '</div><br/>';
		}
	},

	getHeadingComp : function(msg) {
		if (!msg) {
			msg = '';
		}
		this.heading = new Ext.Component({
			xtype : 'component',
			html : ''
		});
		return this.heading;
	}

});



