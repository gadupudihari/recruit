Ext.define('tz.ui.InterviewMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.interviewGridPanel = new tz.ui.InterviewGridPanel({manager:me});
    	me.interviewSearchPanel = new tz.ui.InterviewSearchPanel({manager:me});
    	me.interviewDetailPanel = new tz.ui.InterviewDetailPanel({manager:me});
    	me.requirementSharePanel = new tz.ui.RequirementSharePanel({manager:me});
    	
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [ this.interviewSearchPanel,
 				        {
		   	        		height: 10,
		   	        		border : false
 				        },
 				        this.interviewGridPanel
 				        ]
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
	            items: [me.interviewDetailPanel]
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
	            items: [me.requirementSharePanel]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen : function(){
		this.getLayout().setActiveItem(0);

		var browserHeight = app.mainViewport.height;
    	var searchPanelHt = Ext.getCmp('interviewSearchPanel').getHeight();
		var headerHt = app.mainViewport.items.items[0].getHeight();
    	Ext.getCmp('interviewGridPanel').setHeight(browserHeight - searchPanelHt - headerHt - 40);

    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.contractorsStore.loadByCriteria();
    	ds.contactSearchStore.loadByCriteria(filter);

    	params = new Array();
    	params.push(['type','=', 'Client']);
    	filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.clientSearchStore.loadByCriteria(filter);

    	params = new Array();
    	params.push(['type','=', 'Vendor']);
    	filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.vendorStore.loadByCriteria(filter);

    	if (ds.interviewStore.getCount() ==0) 
    		this.interviewSearchPanel.search();
	},
	
	showInterview : function() {
		this.getLayout().setActiveItem(1);
	},
	
	showSharePanel : function() {
		this.getLayout().setActiveItem(2);
		this.requirementSharePanel.form.reset();
	},

	
});
