Ext.define('tz.ui.InterviewDetailPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 10,
    title: '',
    anchor:'100% 100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},
    
    initComponent: function() {
        var me = this;
        me.newPlacement = false;
		me.closeInterview = false;
		me.modified = false;
		me.opendFrom ='';
		
		me.deleteButton = new Ext.Button({
            text: 'Delete',
            tooltip: 'Delete Interview',
            iconCls : 'btn-delete',
            scope : this,
            tabIndex:27,
            handler: function(){
    			this.deleteConfirm();
    		}
        });

        me.saveAndCloseButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & Close',
            tooltip: 'Save & Close',
            iconCls : 'btn-save',
            tabIndex:26,
            scope: this,
	        handler: function() {
				this.saveAndClose();
			}
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Save',
                        tooltip: 'Save Interview',
                        iconCls : 'btn-save',
                        scope: this,
                        tabIndex:25,
            	        handler: this.checkForExist
                    },'-',
                    me.saveAndCloseButton,'-',me.deleteButton,
                    '-',{
                    	xtype: 'button',
                    	text: 'Share',
                    	tooltip : 'View Interview',
                    	icon : 'images/icon_share.png',
                    	tabIndex:45,
                    	scope : this,
                    	handler: function(){
                    		this.shareInterview();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Close',
                        tooltip: 'Close Interview',
                        iconCls : 'btn-close',
                        tabIndex:28,
                        scope : this,
                        handler: function(){
                        	this.closeForm();
                        }
                    }
                ]
            }
        ];

		me.candidateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Candidate',
		    anyMatch:true,
		    queryMode: 'local',
		    forceSelection:true,
		    displayField: 'fullName',
		    valueField: 'id',
		    name : 'candidateId',
		    tabIndex:3,
		});

		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client',
		    queryMode: 'local',
		    anyMatch:true,
		    forceSelection:true,
            displayField: 'name',
            valueField: 'id',
		    name : 'clientId',
		    tabIndex:5,
		});

		me.vendorCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Vendor',
		    queryMode: 'local',
		    anyMatch:true,
		    forceSelection:true,
            displayField: 'name',
            valueField: 'id',
		    name : 'vendorId',
		    tabIndex:7,
		});

		me.contactCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Interviewer',
            store: ds.contactSearchStore,
            multiSelect: true,
            forceSelection:true,
            anyMatch:true,
		    queryMode: 'local',
            displayField: 'fullName',
            valueField: 'id',
		    name : 'contactId',
		    tabIndex:9,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					this.form.findField('selectedInterviewers').setValue(combo.rawValue);
				}
		    },
		});

        me.items = [this.getMessageComp(), this.getHeadingComp(),
            {
	   	    	 xtype:'fieldset',
	   	    	 title : 'Interview',
	   	         collapsible: false,
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [{
	   	            	 xtype: 'container',
	   	            	 layout : 'table',
	   	            	 items: [{
	   	            		 xtype:'numberfield',
	   	            		allowDecimals: false,
	   	            		 fieldLabel: 'Job Id',
	   	            		 tabIndex:1,
	   	            		 minValue : 1,
	   	            		 name: 'requirementId'
	   	            	 },{
	   	            		 xtype: 'box',
	   	            		 tabIndex:2,
	   	            		 padding : '0 0 0 10%',
	   	            		 name: 'requirementLink',
	   	            		 align : 'right',
	   	            		 autoEl: {
	   	            			 html: '<a href="#">'+'<u>Go</u>'+'</a>'
	   	            		 },
	   	            		 listeners: {
	   	            			 render: function(c){
	   	            				 c.getEl().on('click', function(){me.showRequirement()}, c, {stopEvent: true});
	   	            			 }
	   	            		 }
	   	            	 }]
	   	             },{
	   	            	 xtype: 'container',
	   	            	 layout : 'table',
	   	            	 items: [me.candidateCombo,
	   	            	         {
			   	            		 xtype: 'box',
			   	            		 tabIndex:2,
			   	            		 padding : '0 0 0 10%',
			   	            		 name: 'candidateLink',
			   	            		 align : 'right',
			   	            		 autoEl: {
			   	            			 html: '<a href="#">'+'<u>Go</u>'+'</a>'
			   	            		 },
			   	            		 listeners: {
			   	            			 render: function(c){
			   	            				 c.getEl().on('click', function(){me.showCandidate()}, c, {stopEvent: true});
			   	            			 }
			   	            		 }	
	   	            	         },{
	   	            	        	 xtype: 'button',
	   	            	        	 iconCls : 'btn-add',
	   	            	        	 tooltip : 'Add new Candidate',
	   	            	        	 tabIndex:4,
	   	            	        	 margin: '0 0 0 10',
	   	            	        	 scope: this,
	   	            	        	 handler: function(){
	   	            	        		 this.addCandidate();
	   	            	        	 }
	   	            	         }]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout : 'table',
		   	    	    	items: [me.clientCombo,
		   	    	    	        {
				   	            		 xtype: 'box',
				   	            		 tabIndex:2,
				   	            		 padding : '0 0 0 10%',
				   	            		 name: 'clientLink',
				   	            		 align : 'right',
				   	            		 autoEl: {
				   	            			 html: '<a href="#">'+'<u>Go</u>'+'</a>'
				   	            		 },
				   	            		 listeners: {
				   	            			 render: function(c){
				   	            				 c.getEl().on('click', function(){me.showClient()}, c, {stopEvent: true});
				   	            			 }
				   	            		 }	
		   	            	         },{
   	    	    						xtype: 'button',
   	    	    						iconCls : 'btn-add',
   	    	    						tooltip : 'Add new Client',
   	    	    						tabIndex:6,
		   	    	    				margin: '0 0 0 10',
   	    	    						scope: this,
   	    	    						handler: function(){
   	    	    							this.addClient('Client');
   	    	    						}
		   	    	    	        }]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout : 'table',
		   	    	    	items: [me.vendorCombo,
		   	    	    	        {
				   	            		 xtype: 'box',
				   	            		 tabIndex:2,
				   	            		 padding : '0 0 0 10%',
				   	            		 name: 'vendorLink',
				   	            		 align : 'right',
				   	            		 autoEl: {
				   	            			 html: '<a href="#">'+'<u>Go</u>'+'</a>'
				   	            		 },
				   	            		 listeners: {
				   	            			 render: function(c){
				   	            				 c.getEl().on('click', function(){me.showVendor()}, c, {stopEvent: true});
				   	            			 }
				   	            		 }	
		   	            	         },{
		   	    	    				xtype: 'button',
		   	    	    				iconCls : 'btn-add',
		   	    	    				tooltip : 'Add new Vendor',
		   	    	    				tabIndex:8,
		   	    	    				margin: '0 0 0 10',
		   	    	    				scope: this,
		   	    	    				handler: function(){
		   	    	    					this.addClient('Vendor');
		   	    	    				}
		   	    	    	        }]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout : 'table',
		   	    	    	items: [me.contactCombo,
		   	    	    	        {
				   	            		 xtype: 'box',
				   	            		 tabIndex:2,
				   	            		 padding : '0 0 0 10%',
				   	            		 name: 'contactLink',
				   	            		 align : 'right',
				   	            		 autoEl: {
				   	            			 html: '<a href="#">'+'<u>Go</u>'+'</a>'
				   	            		 },
				   	            		 listeners: {
				   	            			 render: function(c){
				   	            				 c.getEl().on('click', function(){me.showContact()}, c, {stopEvent: true});
				   	            			 }
				   	            		 }	
		   	            	         },{
		   	    	    				xtype: 'button',
		   	    	    				iconCls : 'btn-add',
		   	    	    				tooltip : 'Add new Contact',
		   	    	    				tabIndex:10,
		   	    	    				margin: '0 0 0 10',
		   	    	    				scope: this,
		   	    	    				handler: function(){
		   	    	    					this.addContact();
		   	    	    				}
		   	    	    	        }]
		   	    	    },{
		   	            	 xtype: 'textarea',
		   	                 width:300,
		   	                 height:35,
		   	                 readOnly : true,
		   	                 fieldLabel: 'Selected Interviewers',
		   	                 name: 'selectedInterviewers',
		   	    	    },{
		   	            	 xtype: 'textarea',
		   	                 width:650,
		   	                 height:150,
		   	            	 maxLength:4000,
		   	            	 enforceMaxLength:true,
		   	                 fieldLabel: 'Interview',
		   	                 name: 'interview',
		   	                 emptyText : 'Enter Interview Details (Questions and Answers)',
		   	                 tabIndex:11,
		   	                 listeners:{
		   	                	 change:function(field) {
		   	                		 while (field.value.indexOf('"') != -1) {
		   	                			field.value = field.value.replace('"','');	
		   	                		 }
		   	                		 field.setValue(field.value);
		   	                	 }		    
		   	                 }
		   	    	    },{
		   	            	 xtype: 'textarea',
		   	                 width:650,
		   	                 height:100,
		   	            	 maxLength:800,
		   	            	 enforceMaxLength:true,
		   	                 fieldLabel: 'Employee Feedback',
		   	                 name: 'employeeFeedback',
		   	                 tabIndex:12,
		   	                 listeners:{
		   	                	 change:function(field) {
		   	                		 while (field.value.indexOf('"') != -1) {
		   	                			field.value = field.value.replace('"','');	
		   	                		 }
		   	                		 field.setValue(field.value);
		   	                	 }		    
		   	                 }
		   	    	    },{
		   	            	 xtype: 'textarea',
		   	                 width:650,
		   	                 height:100,
		   	            	 maxLength:800,
		   	            	 enforceMaxLength:true,
		   	                 fieldLabel: 'Vendor Feedback',
		   	                 name: 'vendorFeedback',
		   	                 tabIndex:13,
		   	                 listeners:{
		   	                	 change:function(field) {
		   	                		 while (field.value.indexOf('"') != -1) {
		   	                			field.value = field.value.replace('"','');	
		   	                		 }
		   	                		 field.setValue(field.value);
		   	                	 }		    
		   	                 }
		   	    	    }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [{
	   	            	 xtype: 'datefield',
	   	            	 format: 'm/d/Y',
	   	                 fieldLabel: 'Interview Date',
	   	                 name: 'interviewDate',
	   	                 tabIndex:14,
	   	             },{
	   	            	 xtype: 'timefield',
	   	            	 increment: 1,
	   	            	 anchor: '100%',
	   	                 fieldLabel: 'Local Time',
	   	                 name: 'currentTime',
	   	                 tabIndex:15,
	   	                 listeners:{
	   	                	 select:function(field) {
	   	                		 var time = me.form.findField('currentTime').getValue();
	   	                		 var timeZone = me.form.findField('timeZone').getValue().timeZone;
	   	                		 var pstTime = me.getPSTTime(time, timeZone);
	   	                		 me.form.findField('time').setValue(pstTime)
	   	                	 }		    
	   	                 }
	   	             },{
   	             		xtype: 'radiogroup',
   	             		width: 300,
   	             		fieldLabel: 'Time Zone',
   	             		name: 'timeZone',
   	             		columns: 3,
   	             		value : false,
   	             		vertical: false,
	   	                 listeners:{
	   	                	 change:function(field) {
	   	                		 var time = me.form.findField('currentTime').getValue();
	   	                		 var timeZone = me.form.findField('timeZone').getValue().timeZone;
	   	                		 var pstTime = me.getPSTTime(time, timeZone);
	   	                		 me.form.findField('time').setValue(pstTime)
	   	                	 }		    
	   	                 },
   	             		items: [{ 
   	             			xtype: 'radiofield', 
   	             			name: 'timeZone', 
   	             			boxLabel: 'HST', 
   	             			inputValue: 'HST',
   	             			tabIndex:16,
   	             		},{ 
   	             			xtype: 'radiofield', 
   	             			name: 'timeZone', 
   	             			boxLabel: 'PST', 
   	             			inputValue: 'PST',
   	             			checked : true,
   	             			tabIndex:17,
   	             		},{ 
   	             			xtype: 'radiofield', 
   	             			name: 'timeZone', 
   	             			boxLabel: 'MST', 
   	             			inputValue: 'MST',
   	             			tabIndex:18,
   	             		},{ 
   	             			xtype: 'radiofield', 
   	             			name: 'timeZone', 
   	             			boxLabel: 'CST', 
   	             			inputValue: 'CST',
   	             			tabIndex:19,
   	             		},{ 
   	             			xtype: 'radiofield', 
   	             			name: 'timeZone', 
   	             			boxLabel: 'EST', 
   	             			inputValue: 'EST',
   	             			tabIndex:20,
   	             		}],
   	             	},{
	   	            	 xtype: 'timefield',
	   	            	 increment: 1,
	   	            	 anchor: '100%',
	   	                 fieldLabel: 'PST Time',
	   	                 name: 'time',
	   	                 readOnly : true,
	   	             },
	   	             {
	   	            	 xtype: 'datefield',
	   	            	 format: 'm/d/Y',
	   	                 fieldLabel: 'Approx.Start Date',
	   	                 name: 'approxStartDate',
	   	                 tabIndex:21,
	   	             },{
	   	            	 xtype: 'textarea',
	   	            	 height : 60,
	   	            	 width : 350,
	   	            	 maxLength:500,
	   	            	 enforceMaxLength:true,
	   	                 fieldLabel: 'Comments',
	   	                 name: 'comments',
	   	                 tabIndex:22,
	   	             },{
	   	            	 xtype: 'displayfield',
	   	            	 fieldLabel: 'Skill Set',
	   	            	 name: 'skillSet',
	   	            	 tabIndex:23,
	   	             },{
	   	     	        xtype: 'numberfield',
	   	    	        hidden:true,
	   	    	        name: 'id'
	   	    	    },{
	   	    	        hidden:true,
	   	    	        name: 'projectInserted',
	   	    	        xtype:'displayfield',
	   	    	        value: false
	   	    	    },{
	   	    	    	xtype:'datefield',
	   	    	    	fieldLabel: 'Created',
	   	    	    	renderer: Ext.util.Format.dateRenderer('m/d/Y'),
	   	    	    	readOnly :true,
	   	    	    	name: 'created',
	   	    	    	hidden:true,
	   	    	    },{
	   	     	        xtype: 'textfield',
	   	    	        hidden:true,
	   	    	        name: 'status'
	   	    	    }]
	   	         }]
	   	     }
        ];
        me.callParent(arguments);
    },

    loadForm : function(record) {
    	this.modified = false;
    	this.clearMessage();
    	this.form.reset();
		this.loadRecord(record);
		this.deleteButton.enable();
		this.form.findField('requirementId').setReadOnly(false);

		if (record.data.skillSetIds != null && record.data.skillSetIds != '') {
			var skillSetIds = record.data.skillSetIds.split(',');
			var skillSet ='';
			for ( var i = 0; i < skillSetIds.length; i++) {
				skillSet += ds.skillSetStore.getById(Number(skillSetIds[i])).data.value;
				if (skillSetIds.length > (i+1))
					skillSet += ",";
			}
			this.form.findField('skillSet').setValue(skillSet);
		}
		
		var contactIds = record.data.contactId.split(',');
		contactIds = contactIds.map(Number);
		this.contactCombo.setValue(contactIds);
		
		if (record.data.time != null && record.data.time != '') {
			var hours = record.data.time.split(':')[0];
			var minutes = record.data.time.split(':')[1];
			if (hours == 0 && minutes == 0)
				time = '12:00 AM';
			else{
				if (hours > 12) 
					time = Number(hours) -12 +':'+minutes ;
				else
					time = Number(hours) +':'+minutes ;
				 if (hours > 11)
					 time += ' PM';
				 else
					 time += ' AM';
			}
			this.form.findField('time').setValue(time);
		}
		if (record.data.currentTime != null && record.data.currentTime != '') {
			var hours = record.data.currentTime.split(':')[0];
			var minutes = record.data.currentTime.split(':')[1];
			if (hours == 0 && minutes == 0)
				time = '12:00 AM';
			else{
				if (hours > 12) 
					time = Number(hours) -12 +':'+minutes ;
				else
					time = Number(hours) +':'+minutes ;
				 if (hours > 11)
					 time += ' PM';
				 else
					 time += ' AM';
			}
			 
			this.form.findField('currentTime').setValue(time);
		}
		this.form.findField('timeZone').setValue({timeZone:record.data.timeZone});
	},
    
	closeForm: function(){
		if (this.opendFrom == 'Recruit')
			app.setActiveTab(0,'li_requirements');
		else if (this.opendFrom == 'Home')
			app.setActiveTab(0,'li_home');
		
		if (this.manager.itemId == "interviewMgmtPanel"){
			this.manager.getLayout().setActiveItem(0);
			this.manager.interviewSearchPanel.search();
		}else{
			if (this.opendFrom == 'panel')
				app.requirementMgmtPanel.getLayout().setActiveItem(1);
			else
				app.requirementMgmtPanel.getLayout().setActiveItem(0);
		}
	},

	saveAndNewPlacement : function() {
		this.newPlacement = true;
		this.checkForExist();
	},
	
	saveAndClose : function() {
		this.closeInterview = true;
		this.checkForExist();
	},
	
	checkForExist : function() {
		var values = this.getValues();
    	var params = new Array();
    	params.push(['candidate','=', values.candidateId]);
    	params.push(['requirementId','=', values.requirementId]);
    	params.push(['skipId','=', values.id]);
    	var filter = getFilter(params);
    	filter.pageSize=50;
    	app.interviewService.getInterviews(Ext.JSON.encode(filter),this.onGetInterviews,this);
	},
	
	onGetInterviews : function(data) {
		if (data.success && data.rows.length > 0) {
			var panel = this;
			Ext.MessageBox.show({
				title:'Confirm',
	            msg: "Interview already exist for same candidate for the given job. Do you want to add another interview for the same job ?",
	            buttons: Ext.MessageBox.YESNO,
	            icon: Ext.MessageBox.INFO,
	            fn: function(btn) {
	            	if (btn == 'yes') {
	            		panel.save();
					}
	            }
	        });
		}else
			this.save();
	},
	
    save : function(){
    	if(this.form.isValid( )){
			var otherFieldObj = {}, candidateObj = {}, clientObj = {}, vendorObj = {}, contactObj = {}, requirementObj = {};
			var candidate = '', client = '', vendor = '', contact = '', requirement = '';
			
			var itemslength = this.form.getFields().getCount();
			var i = 0;
			while (i < itemslength) {
				var fieldName = this.form.getFields().getAt(i).name;
				if (fieldName == 'id') {
					var idVal = this.form.findField(fieldName).getValue();
					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0)
						otherFieldObj[fieldName] = this.form.findField(fieldName).getValue();
				}else if (fieldName == 'time' || fieldName == 'currentTime') {
	    			var value = this.form.findField(fieldName).getValue();
	    			if (value != null && value.toString().length > 8) {
	    				var hour    = value.getHours();
	    			    var minute  = value.getMinutes();
	    			    var second  = value.getSeconds(); 
	    			    if(hour.toString().length == 1) 
	    			        var hour = '0'+hour;
	    			    if(minute.toString().length == 1) 
	    			        var minute = '0'+minute;
	    			    if(second.toString().length == 1) 
	    			        var second = '0'+second;
	    			    var dateTime = hour+':'+minute+':'+second;
	    			    otherFieldObj[fieldName] = dateTime;
	    			}

				}else 
					otherFieldObj[fieldName] = this.form.findField(fieldName).getValue();
				i = i+1;
			}
			otherFieldObj['timeZone'] = this.form.findField('timeZone').getValue().timeZone;
			otherFieldObj['contactId'] = this.contactCombo.getValue().toString();
			if (otherFieldObj.interviewDate == null)
				delete otherFieldObj.interviewDate;
			if (otherFieldObj.projectInserted == null)
				delete otherFieldObj.projectInserted;
			
			delete otherFieldObj.candidateId;
			delete otherFieldObj.clientId;
			delete otherFieldObj.vendorId;
			delete otherFieldObj.requirementId;
			delete otherFieldObj.skillSet;
			delete otherFieldObj.selectedInterviewers;
			delete otherFieldObj.requirementLink;
			delete otherFieldObj.candidateLink;
			delete otherFieldObj.clientLink;
			delete otherFieldObj.vendorLink;
			delete otherFieldObj.contactLink;

			otherFieldObj = Ext.JSON.encode(otherFieldObj);
			otherFieldObj = otherFieldObj.substring(1,otherFieldObj.length-1);
			var interview = '';
			var idVal = this.form.findField('candidateId').getValue();
			if(idVal != '' && idVal != null && idVal >= 0) {
				candidateObj['id'] = idVal;
				candidateObj = Ext.JSON.encode(candidateObj);
				candidate = ',"candidate":' + candidateObj;
			}
			var idVal = this.form.findField('clientId').getValue();
			if(idVal != '' && idVal != null && idVal >= 0) {
				clientObj['id'] = idVal;
				clientObj = Ext.JSON.encode(clientObj);
				client = ',"client":' + clientObj;
			}
			var idVal = this.form.findField('vendorId').getValue();
			if(idVal != '' && idVal != null && idVal >= 0) {
				vendorObj['id'] = idVal;
				vendorObj = Ext.JSON.encode(vendorObj);
				vendor = ',"vendor":' + vendorObj;
			}
			var idVal = this.form.findField('requirementId').getValue();
			if(idVal != '' && idVal != null && idVal >= 0) {
				requirementObj['id'] = idVal;
				requirementObj = Ext.JSON.encode(requirementObj);
				requirement = ',"requirement":' + requirementObj;
			}
			
			otherFieldObj = '{' + otherFieldObj + requirement + candidate + client + vendor + '}';
			app.interviewService.saveInterview(otherFieldObj,this.onSave,this);
    	} else {
    		this.updateError('Please fix the errors');
    	}
	},
	
	onSave : function(data){
		this.modified = true;
		if (data.success) { 
			var rec = data.returnVal;
			this.form.findField('id').setValue(rec.id);
			this.updateMessage(data.successMessage);
			app.interviewService.sendInterviewAlert(rec.id);
			if (this.closeInterview) {
				this.closeInterview = false;
				this.closeForm();
			}
		}else {
			this.updateError(data.errorMessage);
		}	
	},	
	
	deleteConfirm : function(){
		Ext.Msg.confirm("Delete Interview","Do you want to delete the Interview?", this.deleteInterview, this);
	},
	
	deleteInterview: function(dat){
		if(dat=='yes'){
			var id = this.form.findField('id').getValue();
			app.interviewService.deleteInterview(id, this.onDeleteInterview, this);
		}
	},
	
	onDeleteInterview : function(data){
		if (!data.success) {
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			this.closeForm();
		}	
	},
	
	getPSTTime : function(time,timeZone) {
		var pstTime = '';
		if ((time != null && time != '') && (timeZone != null && timeZone != '') ){
			switch (timeZone) {
			case 'HST':
				pstTime = new Date( time.getTime() + (3 * 60*60*1000));
				break;
			case 'MST':
				pstTime = new Date( time.getTime() + (-1 * 60*60*1000));
				break;
			case 'CST':
				pstTime = new Date( time.getTime() + (-2 * 60*60*1000));
				break;
			case 'EST':
				pstTime = new Date( time.getTime() + (-3 * 60*60*1000));
				break;
			default:
				pstTime = time;
				break;
			}
		}
		pstTime = dateToTime(pstTime);
		return pstTime;
	},

	getConvertedTime : function(pstTime,timeZone) {
		var outTime = '';
		if ((time != null && time != '') && (timeZone != null && timeZone != '') ){
			switch (timeZone) {
			case 'HST':
				outTime = new Date( pstTime.getTime() + (-2 * 60*60*1000));
				break;
			case 'MST':
				outTime = new Date( pstTime.getTime() + (1 * 60*60*1000));
				break;
			case 'CST':
				outTime = new Date( pstTime.getTime() + (2 * 60*60*1000));
				break;
			case 'EST':
				outTime = new Date( pstTime.getTime() + (3 * 60*60*1000));
				break;
			default:
				outTime = pstTime;
				break;
			}
		}
		outTime = dateToTime(outTime);
		return outTime;
	},

	addCandidate : function() {
		var manager = this.manager;
		if (Ext.getCmp('addCandidate') == null) {
			var candidateWindow = Ext.create('Ext.window.Window', {
			    title: 'Add Candidate',
			    id : 'addCandidate',
			    width: 320,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
		   	                 	xtype:'textfield',
		   	                 	fieldLabel: 'First Name *',
		   	                 	name: 'firstName',
		   	                 	allowBlank:false,
		   	                 	maxLength:45,
		   	                 	enforceMaxLength:true,
		   	                 	tabIndex:101,
		   	             	},{
		   	             		xtype:'textfield',
		   	             		fieldLabel: 'Last Name *',
		   	             		name: 'lastName',
								allowBlank:false,
								tabIndex:102,
								maxLength:45,
								enforceMaxLength:true,
		   	             	},{
		   	             		xtype:'textfield',
		   	             		fieldLabel: 'Email *',
		   	             		name: 'emailId',
		   	             		allowBlank:false,
		   	             		vtype:'email',
		   	             		tabIndex:103,
		   	             		maxLength:45,
		   	             		enforceMaxLength:true,
		   	             	},{
				                xtype: 'combobox',
				                queryMode: 'local',
				                editable: false,
				    		    allowBlank:false,
				    		    displayField: 'value',
				    		    valueField: 'name',
				                store: ds.marketingStatusStore,
				                lazyRender: true,
				    		    fieldLabel: "Marketing Status *",
				                name: 'marketingStatus',
				                tabIndex:104,
			        		}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
			    	tooltip:'Save Candidate',
			    	tabIndex:105,
		            handler: function(){
		            	var candidate = {};
		            	for ( var i = 0; i < 4; i++ ) {
		            		var component = Ext.getCmp('addCandidate').items.items[0].items.items[0].items.items[0].items.items[i];
		            		if(! component.isValid()){
		            			Ext.Msg.alert('Error','Please fix the errors.');
		            			return false;
		            		}else{
		            			candidate[component.name] = component.value;
		            		}
						}
		            	candidate.version = 0; 
		            	candidate = Ext.JSON.encode(candidate);
		            	app.candidateService.saveCandidate(candidate,manager.interviewDetailPanel.onAddCandidate,manager.interviewDetailPanel);
		            }
		        }]
			});
		}
		Ext.getCmp('addCandidate').show();
	},
	
	onAddCandidate : function(data) {
		if (data.success) {
			Ext.getCmp('addCandidate').close();
			var rec = {};
			rec.id = data.returnVal.id;
			rec.fullName = data.returnVal.firstName + ' '+data.returnVal.lastName;
	   	 	this.candidateCombo.store.add(rec);
	   	 	this.candidateCombo.store.sort();
	    	params = new Array();
	    	filter = getFilter(params);
	    	filter.pageSize=1000;
			ds.contractorsStore.loadByCriteria(filter);
		}
	},
	
	addClient : function(type) {
		var manager = this.manager;
		if (Ext.getCmp('addClient') == null) {
			var clientWindow = Ext.create('Ext.window.Window', {
			    title: 'Add Client',
			    id : 'addClient',
			    width: 320,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
		   	                 	xtype:'textfield',
		   	                 	fieldLabel: 'Name *',
		   	                 	name: 'name',
		   	                 	allowBlank:false,
		   	                 	maxLength:45,
		   	                 	enforceMaxLength:true,
		   	                 	tabIndex:201,
		   	             	},{
		   	             		xtype:'textfield',
		   	             		fieldLabel: 'Email *',
		   	             		name: 'email',
		   	             		allowBlank:false,
		   	             		vtype:'email',
		   	             		tabIndex:202,
		   	             		maxLength:45,
		   	             		enforceMaxLength:true,
		   	             	},{
			        			xtype: 'displayfield',
			        			name: 'type',  
			        			fieldLabel: 'Type',
			        			tabIndex:203,
			        			value : type,
			        		}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
			    	tooltip:'Save '+type,
			    	tabIndex:205,
		            handler: function(){
		            	var client = {};
		            	for ( var i = 0; i < 3; i++ ) {
		            		var component = Ext.getCmp('addClient').items.items[0].items.items[0].items.items[0].items.items[i];
		            		if(! component.isValid()){
		            			Ext.Msg.alert('Error','Please fix the errors.');
		            			return false;
		            		}else{
		            			client[component.name] = component.value;
		            		}
						}
		            	client = Ext.JSON.encode(client);
		            	app.clientService.saveclient(client,manager.interviewDetailPanel.onAddClient,manager.interviewDetailPanel);
		            }
		        }]
			});
		}
		Ext.getCmp('addClient').show();
	},

	onAddClient : function(data) {
		if (data.success) {
			var rec = Ext.create( 'tz.model.Client',{
	   	 		id : data.returnVal.id,
	   	 		type : data.returnVal.type,
	   	 		name : data.returnVal.name
	   	 	});				
			if (data.returnVal.type == "Client"){
				this.clientCombo.store.add(rec);
				this.clientCombo.store.sort();
			}else{
				this.vendorCombo.store.add(rec);
				this.vendorCombo.store.sort();
			}
	    	Ext.getCmp('addClient').close();
	    	params = new Array();
	    	params.push(['type','=', 'Client']);
	    	filter = getFilter(params);
	    	filter.pageSize=1000;
	    	ds.clientSearchStore.loadByCriteria(filter);

	    	params = new Array();
	    	params.push(['type','=', 'Vendor']);
	    	filter = getFilter(params);
	    	filter.pageSize=1000;
	    	ds.vendorStore.loadByCriteria(filter);
	    	
	    	params = new Array();
	    	var filter = getFilter(params);
	    	filter.pageSize=1000;
			ds.client_vendorStore.loadByCriteria(filter);
		}
	},

	addContact : function() {
		var manager = this.manager;
		if (Ext.getCmp('addContact') == null) {
			var contactWindow = Ext.create('Ext.window.Window', {
			    title: 'Add Contact',
			    id : 'addContact',
			    width: 320,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
		   	                 	xtype:'textfield',
		   	                 	fieldLabel: 'First Name *',
		   	                 	name: 'firstName',
		   	                 	allowBlank:false,
		   	                 	maxLength:45,
		   	                 	enforceMaxLength:true,
		   	                 	tabIndex:301,
		   	             	},{
		   	             		xtype:'textfield',
		   	             		fieldLabel: 'Last Name *',
		   	             		name: 'lastName',
								allowBlank:false,
								tabIndex:302,
								maxLength:45,
								enforceMaxLength:true,
		   	             	},{
		   	             		xtype:'textfield',
		   	             		fieldLabel: 'Email',
		   	             		name: 'email',
		   	             		//allowBlank:false,
		   	             		vtype:'email',
		   	             		tabIndex:303,
		   	             		maxLength:45,
		   	             		enforceMaxLength:true,
		   	             	},{
		   		                xtype: 'combobox',
		   		                fieldLabel: 'Client/Vendor',
		   		                name : 'clientId',
		   		                queryMode: 'local',
		   		                anyMatch:true,
		   		                triggerAction: 'all',
		   		                displayField: 'name',
		   		                valueField: 'id',
		   		                emptyText:'Select...',
		   		                listClass: 'x-combo-list-small',
		   		                store: ds.client_vendorStore,
		   		                tabIndex:304,
		   		            },{
		   	             		xtype:'textfield',
		   	             		fieldLabel: 'Job Title',
		   	             		name: 'jobTitle',
		   	             		maxLength:45,
		   	             		enforceMaxLength:true,
		   	             		tabIndex:305,
		   	             	}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
			    	tooltip:'Save Contact',
			    	tabIndex:305,
		            handler: function(){
		            	var contact = {};
		            	for ( var i = 0; i < 5; i++ ) {
		            		var component = Ext.getCmp('addContact').items.items[0].items.items[0].items.items[0].items.items[i];
		            		if(! component.isValid()){
		            			Ext.Msg.alert('Error','Please fix the errors.');
		            			return false;
		            		}else{
		            			contact[component.name] = component.value;
		            		}
						}
		    			var clientObj = {};
		    			var client = '';

		            	if (contact.clientId != null && contact.clientId != '' && contact.clientId != 0) {
		    				clientObj['id'] = contact.clientId;
		    				clientObj = Ext.JSON.encode(clientObj);
		    				client = ',"client":' + clientObj;
						}
		            	delete contact.clientId;
		            	contact = Ext.JSON.encode(contact);
		            	contact = contact.substring(1,contact.length-1);
		            	contact = '{' + contact + client + '}';
		            	app.contactService.saveContact(contact,manager.interviewDetailPanel.onAddContact,manager.interviewDetailPanel);
		            }
		        }]
			});
		}
		Ext.getCmp('addContact').show();
	},
	
	onAddContact : function(data) {
		Ext.getCmp('addContact').close();
    	params = new Array();
    	filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.contactSearchStore.loadByCriteria(filter);
	},
	
	showRequirement : function() {
		var requirementId = this.form.findField('requirementId').getValue();
		if (requirementId != null && requirementId != '') {
			app.homeMgmtPanel.editRequirement(requirementId);
			//app.requirementMgmtPanel.requirementDetailPanel.openedFrom = 'Interviews';
		}else
			this.updateError('Job id not found');
	},
	
	shareInterview : function() {
		this.manager.showSharePanel();
		var values = this.form.getValues();
		var candidate ="", vendor="", client ="", interviewer ="";
		var candidateId = this.candidateCombo.getValue();
		if (candidateId != null && candidateId != ''){
			candidate = ds.contractorsStore.getById(Number(candidateId)).data.fullName;
		}
		var vendorId = this.vendorCombo.getValue();
		if (vendorId != null && vendorId != '') {
			vendor = ds.vendorStore.getById(Number(vendorId)).data.name;
		}
		var clientId = this.clientCombo.getValue();
		if (clientId != null && clientId != '') {
			client = ds.clientSearchStore.getById(Number(clientId)).data.name;
		}
		var contactId = this.contactCombo.getValue();
		if (contactId != null && contactId != '') {
			contactId = contactId.toString().split(',');
			var contacts = new Array();
			for ( var i = 0; i < contactId.length; i++) {
				var record = ds.contactSearchStore.getById(Number(contactId[i]));
				if (record.data.internetLink != '')
					contacts.push("<a href="+record.data.internetLink+" target='_blank'>"+record.data.fullName+"</a>");
				else
					contacts.push(record.data.fullName);
			}
			interviewer = contacts.toString();
		}
		
		var detailsTable = "<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>"+
					"<tr><th>Job Id</th><td>"+ values.requirementId +'</td></tr>'+
					"<tr><th>Candidate</th><td>"+candidate+'</td></tr>'+
   	            	"<tr><th>Resume</th><td></td></tr>"+
					"<tr><th>Client</th><td>" +client+'</td></tr>'+
					"<tr><th>Vendor</th><td>" +vendor+'</td></tr>'+
					"<tr><th>Interviewer</th><td>" +interviewer+'</td></tr>'+
					"<tr><th>Interview Date</th><td>" +values.interviewDate+'</td></tr>'+
					"<tr><th>Local Time</th><td>" +values.currentTime+'</td></tr>'+
					"<tr><th>Timezone</th><td>" +values.timeZone+'</td></tr>'+
					"<tr><th>PST Time</th><td>" +values.time+'</td></tr>'+
					"<tr><th>MST Time</th><td>" +this.getConvertedTime(new Date (new Date().toDateString() + ' ' + values.time),'MST')+'</td></tr>'+
					"<tr><th>CST Time</th><td>" +this.getConvertedTime(new Date (new Date().toDateString() + ' ' + values.time),'CST')+'</td></tr>'+
					"<tr><th>EST Time</th><td>" +this.getConvertedTime(new Date (new Date().toDateString() + ' ' + values.time),'EST')+'</td></tr>'+
					"<tr><th>HST Time</th><td>" +this.getConvertedTime(new Date (new Date().toDateString() + ' ' + values.time),'HST')+'</td></tr>'+
					"<tr><th>Interview</th><td><br>" +values.interview+'</td></tr>'+
					"<tr><th>Employee Feedback</th><td><br>" +values.employeeFeedback+'</td></tr>'+
					"<tr><th>Vendor Feedback</th><td><br>" +values.vendorFeedback+'</td></tr></table>';
		
		this.manager.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').setValue(detailsTable);
		this.manager.requirementSharePanel.fileName = candidate;
		this.manager.requirementSharePanel.data = '';
		this.manager.requirementSharePanel.openedFrom = 'Interview Panel';
		this.manager.requirementSharePanel.subject = candidate+' Interview with '+client+' - '+vendor;
		this.manager.requirementSharePanel.emailShortlisted.disable();
		this.manager.requirementSharePanel.emailConfidential.disable();
		
		if (values.requirementId != null && values.requirementId != '') {
			var params = new Array();
	    	params.push(['id','=', values.requirementId]);
	    	var filter = getFilter(params);
	    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onGetRequirements,this);
		}
	},
	
	onGetRequirements : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = Ext.create('tz.model.Requirement', data.rows[0]);
			record = record.data;
			if (record.clientId != null && record.clientId != '') {
				var client = ds.clientSearchStore.getById(Number(record.clientId)).data.name;
			}else
				var client = '';
			
			var ids = record.vendorId;
			if (ids != '') {
				ids = ids.split(',');
				var names ="";
				for ( var i = 0; i < ids.length; i++) {
					var rec = ds.vendorStore.getById(parseInt(ids[i]));
					if (rec)
						names += rec.data.name +", ";	
				}
			}
			var vendor = names.substring(0,names.length-2);
			var values = record;
			var detailsTable = "<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>"+
					"<tr><th>Posting Date </th><td>"+Ext.Date.format(new Date(values.postingDate),'m/d/Y')+'</td></tr>'+
					"<tr><th>Posting Title</th><td>" +values.postingTitle+'</td></tr>'+
					"<tr><th>City & State</th><td>" +values.cityAndState+'</td></tr>'+
					"<tr><th>Alert</th><td>" +values.addInfo_Todos+'</td></tr>'+
					"<tr><th>Source</th><td>" +values.source+'</td></tr>'+
					"<tr><th>Module</th><td>" +values.module+'</td></tr>'+
					"<tr><th>Skill Set</th><td>" +values.skillSet+'</td></tr>'+
					"<tr><th>Role Set</th><td>" +values.targetRoles+'</td></tr>'+
					"<tr><th>Mandatory Certifications</th><td>" +values.certifications+'</td></tr>';
			if(values.remote)
				detailsTable += "<tr><th>Remote</th><td>Yes</td></tr>" ;
			else
				detailsTable += "<tr><th>Remote</th><td>No</td></tr>" ;

			if(values.relocate)
				detailsTable += "<tr><th>Relocation Needed</th><td>Yes</td></tr>" ;
			else
				detailsTable += "<tr><th>Relocation Needed</th><td>No</td></tr>" ;

			if(values.travel)
				detailsTable += "<tr><th>Travel Needed</th><td>Yes</td></tr>" ;
			else
				detailsTable += "<tr><th>Travel Needed</th><td>No</td></tr>" ;

			detailsTable += "<tr><th>Ok to train Candidate</th><td>" + values.helpCandidate +'</td></tr>'+
							"<tr><th>Roles and Responsibilities</th><td>" +values.requirement.replace(/(?:\r\n|\r|\n)/g, '<br>')+'</td></tr></table>';

			var interview = this.manager.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').getValue();
			var fulldetails = interview + "<br><b>Job Opening Details: </b>"+detailsTable;

			if (values.sub_Requirements != null && values.sub_Requirements.length > 0) {
				var candidateId = this.candidateCombo.getValue();
				for ( var i = 0; i < values.sub_Requirements.length; i++) {
					if (values.sub_Requirements[i].candidateId == candidateId) {
						fulldetails = fulldetails.replace('<tr><th>Resume</th><td></td></tr>',"<tr><th>Resume</th><td><a href="+values.sub_Requirements[i].resumeLink+" target='_blank'>View</a></td></tr>");
						break;
					}
				}
			}
			
			this.manager.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').setValue(fulldetails);

		}
	},

	showCandidate : function(candidateId) {
		var candidateId = this.candidateCombo.getValue();
		if (typeof (candidateId) != 'undefined' && candidateId != null && candidateId >= 0){
			var params = new Array();
			params.push(['id','=', candidateId]);
			var filter = getFilter(params);
			app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidate,this);
		}
	},
	
	onGetCandidate : function(data) {
		if (data.rows.length > 0) {
			app.setActiveTab(0,'li_candidates');
			app.candidateMgmtPanel.getLayout().setActiveItem(1);
			app.candidateMgmtPanel.candidatePanel.clearMessage();
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			app.candidateMgmtPanel.candidatePanel.loadForm(record);
			app.candidateMgmtPanel.candidatePanel.openedFrom = "Interview";
		}
	},

	showContact : function() {
		var contactId = this.contactCombo.getValue().toString();
		if (contactId != '' && contactId != null && contactId >= 0){
			if (contactId.search(',') != -1 )
				contactId = contactId.split(',')[0];
	    	var params = new Array();
	    	params.push(['id','=', contactId]);
	    	var filter = getFilter(params);
	    	app.contactService.getContacts(Ext.JSON.encode(filter),this.onGetContact,this);
		}
	},
	
	onGetContact : function(data) {
		if (data.success && data.rows.length > 0) {
	    	app.setActiveTab(0,'li_contacts');
	    	app.contactMgmtPanel.getLayout().setActiveItem(1);
			var contact = Ext.create('tz.model.Contact', data.rows[0]);
			app.contactMgmtPanel.contactDetailPanel.loadForm(contact);
			app.contactMgmtPanel.contactDetailPanel.opendFrom='Interview';
		}
	},

	showClient : function() {
		var clientId = this.clientCombo.getValue();
		if (typeof (clientId) != 'undefined' && clientId != null && clientId >= 0){
			var params = new Array();
			params.push(['id','=', clientId]);
			var filter = getFilter(params);
			app.clientService.getClients(Ext.JSON.encode(filter),this.onGetVendor,this);
		}
	},

	showVendor : function() {
		var vendorId = this.vendorCombo.getValue();
		if (typeof (vendorId) != 'undefined' && vendorId != null && vendorId >= 0){
			var params = new Array();
			params.push(['id','=', vendorId]);
			var filter = getFilter(params);
			app.clientService.getClients(Ext.JSON.encode(filter),this.onGetVendor,this);
		}
	},
	
	onGetVendor : function(data) {
		if (data.rows.length > 0) {
	    	app.setActiveTab(0,'li_clients');
	    	app.clientMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Client', data.rows[0]);
	    	app.clientMgmtPanel.clientPanel.loadForm(record);
	    	app.clientMgmtPanel.clientPanel.openedFrom = 'Interview';
		}
	},


});
