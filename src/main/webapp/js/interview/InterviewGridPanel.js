Ext.define('tz.ui.InterviewGridPanel', {
    extend: 'Ext.grid.Panel',
    id : 'interviewGridPanel',
    title: 'Interviews',
    itemId: 'interviews',
    frame:true,
    anchor:'100%',
    //height:560,
    width :'100%',
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value && record.data.id != null){
    			this.modifiedIds.push(record.data.id);
    		}
    		if (editor.context.field == 'time') {
    			var value = record.data.time;
    			if (value.toString().length > 8) {
    				var hour    = value.getHours();
    			    var minute  = value.getMinutes();
    			    var second  = value.getSeconds(); 
    			    if(hour.toString().length == 1) 
    			        var hour = '0'+hour;
    			    if(minute.toString().length == 1) 
    			        var minute = '0'+minute;
    			    if(second.toString().length == 1) 
    			        var second = '0'+second;
    			    
    			    var dateTime = hour+':'+minute+':'+second; 
    			    record.data.time = dateTime;
    			    this.getView().refresh();
    			}
    		}else if (editor.context.field == 'requirementId') {
    			if (record.data.requirementId != null && record.data.requirementId != 0) {
    				this.checkExists(record);
    			}
			}else if (editor.context.field == 'status' && record.data.id != null && event.originalValue != event.value && event.value == 'C-Accepted') {
				this.projectIds.push(record.data.id);
			}
        }
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.interviewStore;
        me.modifiedIds = new Array();
        me.projectIds = new Array();
        me.record ='';
        
        me.columns = [
            {
            	xtype : 'actioncolumn',
            	width : 30,
            	items : [{
            		icon : 'images/icon_edit.gif',
            		tooltip : 'Edit/View Interview',
            		padding : 10,
            		scope : this,
            		handler : function(grid, rowIndex, colIndex) {
            			var record = ds.interviewStore.getAt(rowIndex);
            			me.editInterview(record);
            		} 
            	}]
            },{
				header : 'Job Id',
				dataIndex : 'requirementId',
	            width :  60,
	            renderer: interviewRender,
			},{
				header : 'Candidate',
				dataIndex : 'candidate',
	            width :  150,
	            renderer:function(value, metadata, record){
                	if(value != null && value != ''){
                		metadata.tdAttr = 'data-qtip="'+value+'<br><b>Open Snapshot</b>"';
                		return '<a href=# style="color: #000000"> ' + value + '</a>';
                	}
            		return 'n/a';
                }
			},{
				header : 'Client',
				dataIndex : 'client',
	            width :  150,
	            renderer: interviewRender,
	            renderer:function(value, metadata, record){
                	if(value != null && value != ''){
                		metadata.tdAttr = 'data-qtip="'+value+'<br><b>Open Snapshot</b>"';
                		return '<a href=# style="color: #000000"> ' + value + '</a>';
                	}
            		return 'n/a';
                }
			},{
				header : 'Vendor',
				dataIndex : 'vendor',
	            width :  150,
	            renderer: interviewRender,
	            renderer:function(value, metadata, record){
                	if(value != null && value != ''){
                		metadata.tdAttr = 'data-qtip="'+value+'<br><b>Open Snapshot</b>"';
                		return '<a href=# style="color: #000000"> ' + value + '</a>';
                	}
            		return 'n/a';
                }
			},{
                dataIndex: 'contactId',
                text: 'Interviewer',
                width:150,
	            renderer:function(value, metadata, record){
                	if (value != null && value != '') {
    					var ids = value.toString().split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.fullName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return names.substring(0,names.length-2);
                	}
                	return 'N/A';
                }
            },{
                dataIndex: 'interviewDate',
                text: 'Interview Date',
                width:90,
                renderer: interviewDateRender,
            },{
                dataIndex: 'currentTime',
                text: 'Local Time',
                width:70,
                renderer: interviewRender,
            },{
                dataIndex: 'timeZone',
                text: 'Time Zone',
                width:70,
                renderer: interviewRender,
            },{
                dataIndex: 'time',
                text: 'PST Time',
                width:70,
                renderer: interviewRender,
            },{
                dataIndex: 'status',
                text: 'Interview Status',
                width:100,
                renderer: interviewRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'interview',
                text: 'Interview',
                width:200,
                renderer: interviewRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'employeeFeedback',
                text: 'Employee Feedback',
                width:120,
                renderer: interviewRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'vendorFeedback',
                text: 'Vendor Feedback',
                width:120,
                renderer: interviewRender,
            },{
                dataIndex: 'approxStartDate',
                text: 'Approx.Start Date',
                width:90,
                renderer: interviewDateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:200,
                renderer: interviewRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: interviewRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                renderer: lastUpdatedRender,
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                renderer: lastUpdatedRender,
                text: 'Last Updated',
                width:150,
            }
        ];

        me.viewConfig = {
        		stripeRows: false,
        		style: {overflow:'auto', overflowX: 'hidden'},
        		
                listeners: {
                	celldblclick: function (view, cell, cellIndex, record, row, rowIndex, e) {
                    	var colHeading = view.panel.headerCt.getHeaderAtIndex(cellIndex).text;
                    	if(colHeading == "Client"){
                    		var clientId = record.data.clientId;
                    		if (clientId != null && clientId != '')
                    			app.homeMgmtPanel.homeDetailPanel.showClient_Vendor(clientId, 'Client');
                    	}else if (colHeading == "Vendor") {
                    		var vendorId = record.data.vendorId;
                    		if (vendorId != null && vendorId != '')
                    			app.homeMgmtPanel.homeDetailPanel.showClient_Vendor(vendorId, 'Vendor');
                    	}else if (colHeading == "Candidate") {
                    		var candidateId = record.data.candidateId;
                    		if (candidateId != null && candidateId != '')
                    			app.homeMgmtPanel.homeDetailPanel.showClient_Vendor(candidateId, 'Candidate');
						}
                    },
                }

        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        tooltip: 'Add New Interview',
                        iconCls : 'btn-add',
                        tabIndex:15,
                        handler: function(){
                    		me.addInterview();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Delete',
                        tooltip: 'Delete Selected Interview',
                        iconCls : 'btn-delete',
                        tabIndex:17,
                        handler: function(){
                    		me.deleteConfirm();
                    	}
                    },'-',{
                        xtype:'button',
                    	itemId: 'grid-excel-button',
                    	iconCls : 'btn-report-excel',
                        text: 'Export to Excel',
                        tooltip : 'Export to Excel',
                        tabIndex:18,
                        handler: function(){
                        	me.getExcelExport();
                        	/*var vExportContent = me.getExcelXml();
                            document.location='data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);*/
                        }
                    },'->',me.showCount
                    ]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        ds.contractorsStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.clientSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.contactSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			this.showCount.setValue(msg);
       	}, me);

        me.callParent(arguments);

        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);

    },
    
/*    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],
*/    
    getExcelExport : function() {
    	var values = app.interviewMgmtPanel.interviewSearchPanel.getValues();
    	var params = new Array();
    	params.push(['candidate','=', values.candidate]);
    	params.push(['client','=', values.client]);
    	params.push(['vendor','=', values.vendor]);
    	params.push(['interviewer','=', values.interviewer]);
    	if (values.interviewDateFrom != undefined && values.interviewDateFrom != '') {
    		var interviewDateFrom = new Date(values.interviewDateFrom);
        	params.push(['interviewDateFrom','=',interviewDateFrom]);
    	}
    	if (values.interviewDateTo != undefined && values.interviewDateTo != '') {
    		var interviewDateTo = new Date(values.interviewDateTo);
        	params.push(['interviewDateTo','=',interviewDateTo]);
    	}
    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;" && !this.columns[i].hidden) {
				columns.push(this.columns[i].text + ':'+this.columns[i].width);
			}
		}
    	params.push(['columns','=', columns.toString()]);

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.requirementService.interviewExcelExport(Ext.encode(filter));
	},

    addInterview : function() {
    	this.manager.showInterview();
    	this.manager.interviewDetailPanel.clearMessage();
    	this.manager.interviewDetailPanel.form.reset();
    	this.setStores();
    	this.manager.interviewDetailPanel.deleteButton.disable();
    	
/*	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.Interview',{
   	 		id : null,
   	 		comments :'',
   	 		lastUpdatedUser :null,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});
        this.store.insert(0, rec);
*/
    },

    editInterview : function(record) {
    	this.manager.showInterview();
    	this.setStores();
    	this.manager.interviewDetailPanel.loadForm(record);
    },
        
	// this function assigns stores to drop downs
	setStores : function() {
        var vendorStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
        });
        var clientStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
        });
        var candidateStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'fullName'],
        });
        
        ds.clientSearchStore.clearFilter(true);
        ds.vendorStore.clearFilter(true);
        ds.contractorsStore.clearFilter(true);
        
        clientStore.add(ds.clientSearchStore.data.items);
        vendorStore.add(ds.vendorStore.data.items);
        candidateStore.add(ds.contractorsStore.data.items);
        this.manager.interviewDetailPanel.vendorCombo.bindStore(vendorStore);
        this.manager.interviewDetailPanel.clientCombo.bindStore(clientStore);	
        this.manager.interviewDetailPanel.candidateCombo.bindStore(candidateStore);
	},
	
    saveInterviews : function() {
    	var records = new Array();
    	for ( var i = 0; i < this.store.data.length ; i++) {
			var record = this.store.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}
    		if (record.data.clientId == '' || record.data.clientId == null)
				delete record.data.clientId;
    		if (record.data.vendorId == '' || record.data.vendorId == null)
				delete record.data.vendorId;
    		if (record.data.candidateId == '' || record.data.candidateId == null)
				delete record.data.candidateId;
    		if (record.data.contactId == '' || record.data.contactId == null)
				delete record.data.contactId;
    		if (record.data.requirementId == '' || record.data.requirementId == null)
				delete record.data.requirementId;
    		
    		if (this.modifiedIds.indexOf(record.data.id) != -1)
    			records.push(record.data);	
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Interviews to save');
    		return;
		}
    	records = Ext.JSON.encode(records);
    	app.interviewService.saveInterviews(records,this.onSave,this);
	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error','Saved Successfully');
		this.sendInterviewAlert(this.modifiedIds);
		this.insertProjects(this.projectIds);
		this.modifiedIds =  new Array();
		this.projectIds =  new Array();
		app.interviewMgmtPanel.interviewSearchPanel.search();
	},
	
	insertProjects : function(projectIds) {
		app.interviewService.insertProjects(projectIds);
	},
	
	sendInterviewAlert : function(modifiedIds) {
		app.interviewService.sendInterviewAlert(modifiedIds);
	},
	
	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select an Interview to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Interview","Do you delete selected Interview?", this.deleteInterview, this);
		}
	},
	
	deleteInterview: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.interviewService.deleteInterview(selectedRecord.data.id, this.onDeleteInterview, this);
		}
	},
	
	onDeleteInterview : function(data){
		if (!data.success) {
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.interviewMgmtPanel.interviewSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	},
	
	checkExists : function(record) {
		this.record = record;
    	var params = new Array();
    	params.push(['id','=', record.data.requirementId]);
    	var filter = getFilter(params);
    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onGetRequirements,this);
	},
	
	onGetRequirements : function(data) {
		if (data.success && data.rows.length > 0) {
			//Requirement exists
		}else{
			ds.interviewStore.getById(this.record.data.id).data.requirementId= null;	
			this.getView().refresh();
			Ext.Msg.alert('Error',"Job opening does not exist for the given Job id");
		}
			
	}

});

function interviewRender(value, metadata, record) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null || value == "") 
		return 'N/A';
    return value;
}

function interviewDateRender(value, metadata, record) {
	if (value != null && value != "") {
		if (typeof(value) == "string")
			value = value.replace(/-/g, "/");
	    metadata.tdAttr = 'data-qtip="' + Ext.Date.format(new Date(value),'m/d/Y') + '"';
	    return Ext.Date.format(new Date(value),'m/d/Y');
	}
}

function lastUpdatedRender(value, metadata, record) {
    metadata.tdAttr = 'data-qtip="' + Ext.Date.format(new Date(value),'m/d/Y H:i:s A') + '"';
    return Ext.Date.format(new Date(value),'m/d/Y H:i:s A');
}

