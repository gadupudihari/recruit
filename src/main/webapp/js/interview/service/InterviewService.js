Ext.define('tz.service.InterviewService', {
	extend : 'tz.service.Service',
		
	saveInterviews : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/interview/saveInterviews',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    getInterviews : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/interview/getInterviews',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    deleteInterview : function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/interview/deleteInterview/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    saveInterview : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/interview/saveInterview',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    sendInterviewAlert : function(values){
		Ext.Ajax.request({
			url : 'remote/interview/sendInterviewAlert',
			params : {
				json : values
			},
		});
    },
    
    insertProjects : function(values){
		Ext.Ajax.request({
			url : 'remote/interview/insertProjects',
			params : {
				json : values
			},
		});
    },
    
    interviewSucceedEmail : function(values) {
		Ext.Ajax.request({
			url : 'remote/reminder/interviewSucceedEmail',
			params : {
				json : values
			},
		});
    }
});

