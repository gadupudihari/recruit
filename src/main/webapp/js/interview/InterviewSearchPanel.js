Ext.define('tz.ui.InterviewSearchPanel', {
    extend: 'Ext.form.Panel',
    id : 'interviewSearchPanel',
    bodyPadding: 10,
    title: '',
    frame:true,
    layout:'anchor',
    anchor:'100%',
    autoScroll:true,
    buttonAlign:'left',
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth: 80},
//creating view in init function
    initComponent: function() {
        var me = this;
        
		me.candidateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Candidate',
		    anyMatch:true,
		    store: ds.contractorsStore,
		    queryMode: 'local',
		    displayField: 'fullName',
		    valueField: 'id',
		    name : 'candidate',
		    tabIndex:1,
		});

		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client',
		    anyMatch:true,
            store: ds.clientSearchStore,
		    queryMode: 'local',
            displayField: 'name',
            valueField: 'id',
		    name : 'client',
		    tabIndex:2,
		});

		me.vendorCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Vendor',
		    anyMatch:true,
            store: ds.vendorStore,
		    queryMode: 'local',
            displayField: 'name',
            valueField: 'id',
		    name : 'vendor',
		    tabIndex:3,
		});

		me.contactCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Interviewer',
		    anyMatch:true,
            store: ds.contactSearchStore,
		    queryMode: 'local',
            displayField: 'fullName',
            valueField: 'id',
		    name : 'interviewer',
		    tabIndex:4,
		});

		me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [me.candidateCombo,me.clientCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [me.vendorCombo,me.contactCombo]
	   	         },{
	   	        	 xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [{
	   	            	 xtype:'datefield',
	   	            	 fieldLabel: 'Interview Date From',
	   	            	 name: 'interviewDateFrom',
	   	            	 labelWidth :117,
	   	            	 tabIndex:5
	   	             },{
	   	            	 xtype:'datefield',
		   	        	 fieldLabel: 'Interview Date To',
		   	        	 name: 'interviewDateTo',
		   	        	 labelWidth :117,
		   	        	 tabIndex:6
	   	             }]
	   	         },{
	   	        	 xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [{
	   	            	 xtype:'textfield',
	   	            	 fieldLabel: 'Job Id',
	   	            	 name: 'jobId',
	   	            	 labelWidth :117,
	   	            	 tabIndex:7,
	   	            	 inputAttrTpl: " data-qtip='Type \"Empty\" to display interviews whose Job id is not given.' ",
	   	             }]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	tooltip : 'Search for Interviews',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:10,
			},{
				text : 'Reset',
				tooltip : 'Reset and Search for Interviews',
				scope: this,
				handler: this.reset,
				tabIndex:11,
			},{
				text : 'Clear',
				tooltip : 'Clear all filters',
				scope: this,
				tabIndex:12,
	             handler: function() {
	            	 this.form.reset();
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['candidate','=', values.candidate]);
    	params.push(['client','=', values.client]);
    	params.push(['vendor','=', values.vendor]);
    	params.push(['interviewer','=', values.interviewer]);
    	params.push(['status','=', values.status]);
    	params.push(['requirementId','=', values.jobId]);
    	if (values.interviewDateFrom != undefined && values.interviewDateFrom != '') {
    		var interviewDateFrom = new Date(values.interviewDateFrom);
        	params.push(['interviewDateFrom','=',interviewDateFrom]);
    	}
    	if (values.interviewDateTo != undefined && values.interviewDateTo != '') {
    		var interviewDateTo = new Date(values.interviewDateTo);
        	params.push(['interviewDateTo','=',interviewDateTo]);
    	}

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=50;
    	ds.interviewStore.loadByCriteria(filter);
    	app.interviewMgmtPanel.interviewGridPanel.modifiedIds = new Array();
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
