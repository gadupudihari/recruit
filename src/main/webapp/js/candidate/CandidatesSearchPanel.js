Ext.define('tz.ui.CandidatesSearchPanel', {
    extend: 'Ext.form.Panel',
    id : 'candidatesSearchPanel',
    bodyPadding: 10,
    title: '',
    frame:true,
    layout:'anchor',
    anchor:'100%',
    autoScroll:true,
    buttonAlign:'left',
//creating view in init function
    initComponent: function() {
        var me = this;
        
		me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    store: ds.candidateTypeStore,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
		    name : 'type',
		    tabIndex:9,
		    labelWidth :100,
		});

		me.marketingStatusCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: "Marketing Status",
            name: 'marketingStatus',
            store: ds.marketingStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
		    value : ['1. Active','2. Yet to Start','3. Future'],
		    tabIndex:8,
		    labelWidth :100,
		});

		me.relocateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Relocate",
            name: 'relocate',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
		    store: ds.yesOrNoStore,
		    tabIndex:6,
		});

		me.travelCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Travel",
            name: 'travel',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
		    store: ds.yesOrNoStore,
		    tabIndex:5,
		});

		me.certificationsCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Certifications",
            name: 'certifications',
            anyMatch:true,
	        tabIndex:4,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.certificationsStore,
		});

        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Search',
	   	                 name: 'searchCandidates',
	   	                 tabIndex:1,
	   	                 inputAttrTpl: " data-qtip='Prefix minus (-) to exclude' ",
	   	             },{
	   	                 xtype:'numberfield',
	   	                 fieldLabel: 'Priority',
	   	                 name: 'priority',
	   	                 allowDecimals: false,
	   	                 value : 0,
	   	                 tabIndex:2,
	   	             },{
   	                 	xtype:'numberfield',
   	                 allowDecimals: false,
   	                 	fieldLabel: 'Confidentiality',
   	                 	name: 'confidentiality',
   	                 	tabIndex:3,
   	                 	minValue: 1,
   	                 	maxValue: 9,
   	                 	listeners: {
   	                 		afterrender: function() {
   	                 			if (app.userRole == 'ADMIN')
   	                 				this.show();
   	                 			else
   	                 				this.hide();
   	                 		}
   	                 	}
   	             	},me.certificationsCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [me.travelCombo,me.relocateCombo,
	   	                     {
	   	            	 xtype:'textfield',
	   	                 fieldLabel: 'Referral',
	   	                 name: 'referral',
	   	                 tabIndex:7,
	   	                 inputAttrTpl: " data-qtip='Prefix minus (-) to exclude' ",
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [me.marketingStatusCombo,me.typeCombo,
	   	                     {
	   	            	 xtype:'textfield',
	   	                 fieldLabel: 'Source',
	   	                 name: 'source',
	   	                 tabIndex:10,
	   	                 inputAttrTpl: " data-qtip='Prefix minus (-) to exclude' ",
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Availability Date From',
	   	                 name: 'availabilityDateFrom',
	   	                 tabIndex:11,
	   	                 labelWidth :120,
	   	             },{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Availability Date To',
	   	                 name: 'availabilityDateTo',
	   	                 tabIndex:12,
	   	                 labelWidth :120,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Unique Ids (Ex:1,2)',
	   	                 name: 'id',
	   	                 labelWidth :120,
	   	                 tabIndex:13
	   	             }]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	tooltip : 'Search',
            	tooltip : 'Search for candidates',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:20,
			},{
				text : 'Reset',
				tooltip : 'Reset and search for candidates',
				scope: this,
				handler: this.reset,
				tabIndex:21,
			},{
				text : 'Clear',
				tooltip : 'Clear all filters',
				scope: this,
				tabIndex:21,
	             handler: function() {
	            	 this.form.reset();
	            	 this.travelCombo.setValue(null);
	            	 this.marketingStatusCombo.setValue(null);
	            	 this.form.findField('priority').setValue(null);
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['type','=', values.type]);

    	var marketingStatus = values.marketingStatus.toString();
    	while (marketingStatus.length >0) {
        	if (marketingStatus.substr(marketingStatus.length-1,marketingStatus.length) == ",")
        		marketingStatus = marketingStatus.substr(0,marketingStatus.length-1);
			else
				break;
		}
    	if (marketingStatus.search(',') != -1) 
    		marketingStatus = "'"+marketingStatus.replace(/,/g, "','")+"'";
    	
    	params.push(['marketingStatus','=', marketingStatus]);

    	params.push(['priority','=', values.priority]);
   		params.push(['relocate','=', values.relocate]);
   		params.push(['travel','=', values.travel]);
    	params.push(['search','=', values.searchCandidates]);
    	params.push(['confidentiality','=', values.confidentiality]);
    	params.push(['referral','=', values.referral]);
    	params.push(['source','=', values.source]);
    	params.push(['certifications','=', values.certifications]);
    	if (values.availabilityDateFrom != undefined && values.availabilityDateFrom != '') {
    		var availabilityDateFrom = new Date(values.availabilityDateFrom);
        	params.push(['availabilityDateFrom','=',availabilityDateFrom]);
    	}
    	if (values.availabilityDateTo != undefined && values.availabilityDateTo != '') {
    		var availabilityDateTo = new Date(values.availabilityDateTo);
        	params.push(['availabilityDateTo','=',availabilityDateTo]);
    	}
    	var id = values.id;
    	id = id.replace(',,',",");
    	while (id.length >0) {
        	if (id.substr(id.length-1,id.length) == ",")
        		id = id.substr(0,id.length-1);
			else
				break;
		}
    	if (id.search(',') != -1) 
    		id = "'"+id.replace(/,/g, "','")+"'";

    	params.push(['id','=', id]);

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=100;
    	ds.candidateStore.loadByCriteria(filter);
	},
	
	reset:function(){
		app.candidateMgmtPanel.loadStores();
		this.form.reset();
		this.search();
	},
	
});
