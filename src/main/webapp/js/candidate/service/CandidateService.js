Ext.define('tz.service.CandidateService', {
	extend : 'tz.service.Service',
		
	saveCandidates : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/saveCandidates',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    deleteCandidate : function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/deleteCandidate/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    saveCandidate : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/saveCandidate',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    excelExport: function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/excelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },
    
    onSuccess : function(err) {
    	//Opening the Excel Filename returned from Java, This initiates download on the Browser
    	if (err.responseText.search(false) != -1) {
    		app.invoiceService.onFailure(err);
			return;
		}
    	if (err.responseText.search('.pdf') != -1) {
			var i = err.responseText.indexOf('.pdf');
			var w = window.open(err.responseText.slice(29,i+4));
		}else{
	    	var i=err.responseText.indexOf('xls');
	    	window.open(err.responseText.slice(29,i+3),'_self','',true);
		}
    	app.loadGenerateMask.hide();
    	this.onAjaxResponse;
    },
    
    onFailure : function(err) {
    	app.loadGenerateMask.hide();
    	if (err.responseText.search('.pdf') != -1) {
            Ext.MessageBox.show({
                title: 'Error',
                msg: 'Pdf Generation failed',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
		}else{
	        Ext.MessageBox.show({
	            title: 'Error',
	            msg: 'Excel export failed',
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
        this.onAjaxResponse;
    },

    saveCertificates : function(certificates,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/saveCertificates',
			params : {
				json : certificates
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    deleteCertificate: function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/deleteCertificate/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    giveOnboarding : function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/giveOnboarding/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    getCandidates : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/getCandidates',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    getAllCandidates : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/getAllCandidates',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    getSuggestedCriteria : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/getSuggestedCriteria',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    getNegativeCriteria : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/getNegativeCriteria',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    sendMarketingEmail : function(values,  cb, scope) {
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/sendMarketingEmail',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.sendMask.show();
    },

    marketCandidates : function(values,  cb, scope) {
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/marketCandidates',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
});
