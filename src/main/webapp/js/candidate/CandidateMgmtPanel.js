Ext.define('tz.ui.CandidateMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.candidatesGridPanel = new tz.ui.CandidatesGridPanel({manager:me});
    	me.candidatesSearchPanel = new tz.ui.CandidatesSearchPanel({manager:me});
    	me.candidatePanel = new tz.ui.CandidatePanel({manager:me});
    	me.requirementSharePanel = new tz.ui.RequirementSharePanel({manager:me});
    	
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [this.candidatesSearchPanel,
 				        {
		   	        		height: 10,
		   	        		border : false
 				        },
 				        this.candidatesGridPanel
 				        ]
            },{
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [this.candidatePanel]
            },{
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [this.requirementSharePanel]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen: function(){
    	//this.getLayout().setActiveItem(0);

    	var browserHeight = app.mainViewport.height;
    	var searchPanelHt = Ext.getCmp('candidatesSearchPanel').getHeight();
		var headerHt = app.mainViewport.items.items[0].getHeight();
    	Ext.getCmp('candidatesGridPanel').setHeight(browserHeight - searchPanelHt - headerHt - 30);

    	if (ds.candidateStore.getCount() ==0) 
    		this.candidatesSearchPanel.search();
		
		var columns = this.candidatesGridPanel.getView().getGridColumns();
		if (app.userRole == 'ADMIN'){
			for ( var i = 0; i < columns.length ; i++) {
				if(columns[i].dataIndex == "confidentiality"){
					columns[i].hidden = false;
					break;
				}
			}
		}else{
			for ( var i = 0; i < columns.length ; i++) {
				if(columns[i].dataIndex == "confidentiality"){
					columns[i].setText('');
					columns[i].hidden = true;
					break;
				}
			}
		}
		this.candidatesGridPanel.getView().refresh();
	},
	
	showCandidate:function(rowIndex){
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var record = ds.candidateStore.getAt(rowIndex);
		}
		this.candidatePanel.loadForm(record);
	},
	
	showSharePanel : function() {
		this.getLayout().setActiveItem(2);
		this.requirementSharePanel.form.reset();
	},

	showQuickAccess : function() {
    	ds.quickAccessStore.clearFilter();
		var employee = this.quickSearchPanel.quickAccessCombo.getValue();
		if (employee == null) {
			return false;
		}
		app.showSnapshot(employee);
	},

	loadStores : function() {
		ds.skillsetGroupStore.loadByCriteria();
		ds.marketingStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Marketing Status'])));
		ds.qualificationStore.loadByCriteria(getFilter(new Array(['type','=', 'Qualification'])));
    	ds.candidateTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Type'])));
    	ds.immigrationTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Immigration'])));
    	ds.employmentTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Employment Type'])));
    	ds.certificationStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Status'])));
    	ds.certificationSponsoredStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Sponsored'])));
    	ds.candidateSourceStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Source'])));
    	ds.communicationStore.loadByCriteria(getFilter(new Array(['type','=', 'Communication'])));
    	ds.personalityStore.loadByCriteria(getFilter(new Array(['type','=', 'Personality'])));
    	ds.experienceStore.loadByCriteria(getFilter(new Array(['type','=', 'Experience'])));
    	ds.targetRolesStore.loadByCriteria(getFilter(new Array(['type','=', 'Target Roles'])));
    	ds.certificationsStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Name'])));
    	
    	var params = new Array();
    	params.push(['type','=', 'Skill Set/Target Skill Set']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.skillSetStore.loadByCriteria(filter);
	}
	
});
