Ext.define('tz.ui.CandidatesGridPanel', {
    extend: 'Ext.grid.Panel',
    id : 'candidatesGridPanel',
    title: 'Candidates',
    //frame:true,
    anchor:'100%',
    //height:560,
    width :'100%',
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value && record.data.id != null){
    			this.modifiedIds.push(record.data.id);
    		}
    		if(editor.context.field == 'contactNumber'){
    			record.data.contactNumber = record.data.contactNumber.replace(/[^0-9]/g, "");
            	record.data.contactNumber = record.data.contactNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
            	this.getView().refresh();
            }else if (editor.context.field == 'certifications') {
            	while (record.data.certifications.indexOf('"') != -1) {
            		record.data.certifications = record.data.certifications.replace('"','');	
				}
                this.getView().refresh();
            }
            var scrollPosition = this.getEl().down('.x-grid-view').getScroll();
        	this.getView().refresh();
        	//And then after the record is refreshed scroll to the same position
        	this.getEl().down('.x-grid-view').scrollTo('top', scrollPosition.top, false);
        },
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.candidateStore;
        me.modifiedIds = new Array();
        me.emailFilter ;
        me.candidates = new Array();
        me.emailIds = new Array();
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :30,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'Edit Candidate',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					app.candidateMgmtPanel.showCandidate(rowIndex);
    				}
    			}]
            },{
            	xtype: 'actioncolumn',
            	width :35,
            	items : [{
            		icon : 'images/icon_reschedule.gif',
            		tooltip : 'View Candidate',
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			this.shareRequirement(this.store.getAt(rowIndex));
            		}
            	}]
            },{
				text : 'Unique Id',
				dataIndex : 'id',
				width :  40,
		        renderer: candidateRender,
		        hidden : true
			},{
                xtype: 'gridcolumn',
                dataIndex: 'priority',
                text: 'Priority',
                width : 40,
                renderer : numberRender,
            },{
				text : 'Experience',
				dataIndex : 'experience',
				width :  50,
                renderer:function(value, metadata, record){
                	if (value != null) {
    					metadata.tdAttr = 'data-qtip="' + value + '"';
    					return value;
					}
                	return 'N/A';
                }
			},{
				text : 'Communication',
				dataIndex : 'communication',
				width :  70,
				renderer: candidateRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'firstName',
                text: 'Candidate',
                renderer:function(value, metadata, record){
                	metadata.style = 'background-color:#'+getCandidateBackGround(record)+';'
            		var tooltip = "Contact No : "+ record.data.contactNumber+"<br>City & State : "+ record.data.cityAndState+"<br>Gender : "+ record.data.gender+
            			"<br>Immigration Status : "+ record.data.immigrationStatus+"<br>Immigration Verified : "+ record.data.immigrationVerified+
            			"<br>Expected Rate : "+ record.data.expectedRate+"<br>Alert : <span style='color:red;'>" + record.data.alert + "</span>";
                	metadata.tdAttr = 'data-qtip="' + tooltip + '"';
                	return record.data.firstName+' '+record.data.lastName;
                }
            },{
				text : 'Resume Help',
				width:70,
				dataIndex : 'resumeHelp',
				renderer:function(value, metadata, record){
                	metadata.style = 'background-color:#'+getCandidateBackGround(record)+';'
                	metadata.tdAttr = 'data-qtip="' + value + '"';
                	return value;
                }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'marketingStatus',
                text: 'Marketing Status',
                width:55,
                renderer:function(value, metadata, record){
                	metadata.style = 'background-color:#'+getCandidateBackGround(record)+';'
                	if (record.data.submissions != null && record.data.submissions != '')
                		metadata.tdAttr = 'data-qtip="' + record.data.submissions + '"';
                	else
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                	if (value != '1. Active' && value != '2. Yet to Start' )
                		metadata.style = 'background-color:lightgray;'
            		return value;
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'alert',
                text: 'Alert',
                width:70,
                renderer:function(value, metadata, record){
                	metadata.style = 'background-color:#'+getCandidateBackGround(record)+';'
                	metadata.tdAttr = 'data-qtip="' + value + '"';
                    if (value == null || value=="") 
                    	return 'N/A' ;
                    return '<span style="color:red;">' + value + '</span>';
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'source',
                text: 'Source',
                width:80,
            	renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + "<b>Source : </b>" +value +"<br><b>Referral : </b>"+record.data.referral+ '"';
                	if (value == 'Referral' || value == 'Employee Referral')
                		return 'R-'+record.data.referral;
                	return value;
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'allCertifications',
                text: 'Certifications',
                width:120,
                renderer:function(value, metadata, record){
          			var tooltip = "";
                	if(value != null && value != ''){
                		tooltip = "<b>Certifications :</b><br>"+value.replace(/ -- /g, '<br />')+'<br><br>';
                	}
            		if (record.data.skillSetIds != null  && record.data.skillSetIds != '') {
    					var skillSet = record.data.skillSetIds.split(',');
    					tooltip += "<b>Skill Set: </b><br>"
    					for ( var i = 1; i <= skillSet.length; i++) {
    						tooltip += ds.skillSetStore.getById(Number(skillSet[i-1])).data.value+',';
    						if (i%3 == 0)
    							tooltip += "<br>";
    					}
    					tooltip += "<br><br>";
    				}
            		if (record.data.roleSetIds != null  && record.data.roleSetIds != '') {
    					var roleSet = record.data.roleSetIds.split(',');
    					tooltip += "<b>Roleset: </b><br>";
    					for ( var i = 1; i <= roleSet.length; i++) {
    						tooltip += ds.targetRolesStore.getById(Number(roleSet[i-1])).data.value+',';
    						if (i%3 == 0)
    							tooltip += "<br>";
    					}
    					tooltip += "<br><br>";
    				}
            		if (record.data.targetSkillSetIds != null  && record.data.targetSkillSetIds != '') {
    					var targetSkillSet = record.data.targetSkillSetIds.split(',');
    					tooltip += "<b>Target Skill Set: </b><br>";
    					for ( var i = 1; i <= targetSkillSet.length; i++) {
    						tooltip += ds.skillSetStore.getById(Number(targetSkillSet[i-1])).data.value+',';
    						if (i%3 == 0)
    							tooltip += "<br>";
    					}
    					tooltip += "<br><br>";
    				}
            		if (record.data.targetRoles != null  && record.data.targetRoles != '') {
    					var targetRoles = record.data.targetRoles.split(',');
    					tooltip += "<b>Target Roles: </b><br>";
    					for ( var i = 1; i <= targetRoles.length; i++) {
    						tooltip += targetRoles[i-1]+',';
    						if (i%3 == 0)
    							tooltip += "<br>";
    					}
    				}
            	    metadata.tdAttr = 'data-qtip="' + tooltip + '"';
                    return value.replace(/ -- /g, ', ') ;
                }
            },{
				text : 'Availability',
				dataIndex : 'availability',
				width :  68,
				renderer: endDateRenderer,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'emailId',
                text: 'Email Id',
                width:60,
                hidden : true,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'contactNumber',
                text: 'Contact No',
                width:80,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'gender',
                text: 'Gender',
                width:55,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'p1comments',
                text: 'P1 Comments',
                width:90,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'p2comments',
                text: 'P2 Comments',
                width:90,
                hidden : true,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'expectedRate',
                text: 'Expected Rate',
                width:75,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'rateFrom',
                text: 'Rate From',
                width:75,
                renderer: numberRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'rateTo',
                text: 'Rate To',
                width:75,
                renderer: numberRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'technicalScore',
                text: 'Technical Score',
                width : 40,
                renderer : numberRender,
            },{
				text : 'Will Relocate',
				dataIndex : 'relocate',
				width :  55,
				renderer: activeRenderer,
			},{
				text : 'Will Travel',
				dataIndex : 'travel',
				width :  55,
				renderer: activeRenderer,
			},{
				text : 'Good Candidate for Remote',
				dataIndex : 'goodCandidateForRemote',
				width :  80,
				renderer: activeRenderer,
			},{
				text : 'Open To CTH',
				dataIndex : 'openToCTH',
				width :  75,
				renderer: candidateRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'cityAndState',
                text: 'City & State',
                width:90,
                renderer: candidateRender,
            },{
				text : 'Location',
				dataIndex : 'location',
				width :  80,
				renderer: candidateRender,
			},{
				text : 'Follow Up',
				dataIndex : 'followUp',
				width :  60,
				renderer: followUpRenderer,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'personality',
                text: 'Personality',
                width:65,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'contactAddress',
                text: 'Contact Address',
                width:100,
                renderer: candidateRender,
            },{
				text : 'Initial Contact Date',
				dataIndex : 'contactDate',
				width :  68,
				renderer: dateRender,
			},{
				text : 'Start Date',
				dataIndex : 'startDate',
				width :  68,
				renderer: dateRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'education',
                text: 'Education',
                width:150,
                renderer: candidateRender,
            },{
            	header : 'Resume Link',
            	dataIndex : 'resumeLink',
            	renderer: linkRenderer,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'immigrationStatus',
                text: 'Immigration Status',
                renderer: candidateRender,
            },{
				text : 'Immigration Verified',
				dataIndex : 'immigrationVerified',
				width :  100,
				renderer: candidateRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'type',
                text: 'Type',
                width:100,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'employer',
                text: 'Employer',
                width:80,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'highestQualification',
                text: 'Highest Qualification Held',
                width:120,
                renderer: candidateRender,
            },{
				text : 'Employment Type',
				width:70,
				dataIndex : 'employmentType',
				renderer: candidateRender,
			},{
				text : 'Current Roles',
				width:70,
				dataIndex : 'currentRoles',
				renderer: candidateRender,
			},{
				text : 'Target Roles',
				width:70,
				dataIndex : 'targetRoles',
				renderer: candidateRender,
			},{
				text : 'Target Skill Set',
				width:80,
				dataIndex : 'targetSkillSet',
				renderer: candidateRender,
			},{
				text : 'Interview Help',
				width:80,
				dataIndex : 'interviewHelp',
				renderer: candidateRender,
			},{
				text : 'Job Help',
				width:80,
				dataIndex : 'jobHelp',
				renderer: candidateRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'aboutPartner',
                text: 'About Partner',
                width:65,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'currentJobTitle',
                text: 'Current Job Title',
                width:120,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'confidentiality',
                text: 'Confidentiality',
                width : 45,
                hideable : false,
                renderer : numberRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: candidateRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:150,
            }
        ];
        me.viewConfig = {
        	stripeRows: true,
        };
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        	tabIndex:31,
		    listeners: {
                render: function(c) {
                    new Ext.ToolTip({
                        target: c.getEl(),
                        maxWidth: 1000,
                        dismissDelay: 60000,
                        html: 'Default : Displaying P0 candidates with Marketing status P0 candidate with Marketing status 1. Active, 2. Yet to Start, 3. Future.' 
                    });
                }
            }
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        tooltip: 'Add New',
                        iconCls : 'btn-add',
                        tabIndex:25,
                        handler: function(){
                    		me.addCandidate();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Save',
                        tooltip: 'Save',
                        iconCls : 'btn-save',
                        hidden : true,
                        tabIndex:26,
                        handler: function(){
                    		me.saveCandidates();
                    	}
                    },{
                        xtype: 'button',
                        text: 'Delete',
                        tooltip: 'Delete',
                        iconCls : 'btn-delete',
                        tabIndex:27,
                        handler: function(){
                    		me.deleteConfirm();
                    	}
                    },'-',{
                        xtype:'button',
                    	itemId: 'grid-excel-button',
                    	iconCls : 'btn-report-excel',
                        text: 'Export to Excel',
                        tooltip: 'Export to Excel',
                        tabIndex:28,
                        handler: function(){
                        	me.getExcelExport();
                        }
                    },'-',{
        	        	xtype:'button',
        	        	iconCls : 'btn-email',
        	        	text: 'Email',
        	        	tooltip:'Email to candidates',
        	        	tabIndex:29,
        	        	handler: function(){
        	        		me.showEmail(me.store);
        	        	}
        	        },'-',{
        	        	text : 'Refresh',
        	        	tooltip : 'Refresh',
        	        	icon : 'images/arrow_refresh.png',
        	        	scope: this,
        	        	tabIndex:30,
        	        	handler: function(){
        	        		app.candidateMgmtPanel.candidatesSearchPanel.form.reset();
        	        		app.candidateMgmtPanel.loadStores();
        	        	}
        	        },'->',me.showCount
                    ]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });

        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			this.showCount.setValue(msg);
			this.loadSubmissions();
       	}, me);

        me.callParent(arguments);
               
        
        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);
        
    },
    
/*    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],*/
    
    getExcelExport : function() {
    	var values = app.candidateMgmtPanel.candidatesSearchPanel.getValues();
    	var params = new Array();
    	var marketingStatus = values.marketingStatus.toString();
    	while (marketingStatus.length >0) {
        	if (marketingStatus.substr(marketingStatus.length-1,marketingStatus.length) == ",")
        		marketingStatus = marketingStatus.substr(0,marketingStatus.length-1);
			else
				break;
		}
    	if (marketingStatus.search(',') != -1) 
    		marketingStatus = "'"+marketingStatus.replace(/,/g, "','")+"'";
    	
    	params.push(['marketingStatus','=', marketingStatus]);

    	params.push(['priority','=', values.priority]);
   		params.push(['relocate','=', values.relocate]);
   		params.push(['travel','=', values.travel]);
    	params.push(['search','=', values.searchCandidates]);
    	params.push(['confidentiality','=', values.confidentiality]);
    	params.push(['referral','=', values.referral]);
    	params.push(['source','=', values.source]);
    	if (values.availabilityDateFrom != undefined && values.availabilityDateFrom != '') {
    		var availabilityDateFrom = new Date(values.availabilityDateFrom);
        	params.push(['availabilityDateFrom','=',availabilityDateFrom]);
    	}
    	if (values.availabilityDateTo != undefined && values.availabilityDateTo != '') {
    		var availabilityDateTo = new Date(values.availabilityDateTo);
        	params.push(['availabilityDateTo','=',availabilityDateTo]);
    	}
    	var id = values.id;
    	id = id.replace(',,',",");
    	while (id.length >0) {
        	if (id.substr(id.length-1,id.length) == ",")
        		id = id.substr(0,id.length-1);
			else
				break;
		}
    	if (id.search(',') != -1) 
    		id = "'"+id.replace(/,/g, "','")+"'";

    	params.push(['id','=', id]);

    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;" && this.columns[i].text != '' && !this.columns[i].hidden && this.columns[i].text != "Confidentiality") {
				columns.push(this.columns[i].text + ':'+this.columns[i].width);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.candidateService.excelExport(Ext.encode(filter));
	},

    addCandidate : function() {
	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.Candidate',{
   	 		id : null,
   	 		experience : null,
   	 		relocate : true,
   	 		travel : true,
   	 		followUp : null,
   	 		confidentiality : 5,
   	 		version : 0,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});
   	 	app.candidateMgmtPanel.getLayout().setActiveItem(1);
   	 	app.candidateMgmtPanel.candidatePanel.loadForm(rec);
        //this.store.insert(0, rec);
	},
    
    saveCandidates : function() {
    	var records = new Array();
    	for ( var i = 0; i < ds.candidateStore.data.length ; i++) {
			var record = ds.candidateStore.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}
    		if (record.data.priority == null )
				delete record.data.priority;
    		if (record.data.confidentiality == null )
				delete record.data.confidentiality;
    		if (record.data.employeeId == null )
				delete record.data.employeeId;
    		if (record.data.rateFrom == null )
				delete record.data.rateFrom;
    		if (record.data.rateTo == null )
				delete record.data.rateTo;
    		if (record.data.relocate == null )
				delete record.data.relocate;
    		if (record.data.travel == null )
				delete record.data.travel;
    		if (record.data.goodCandidateForRemote == null )
				delete record.data.goodCandidateForRemote;

    		if (this.modifiedIds.indexOf(record.data.id) != -1)
    			records.push(record.data);	
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Candidates to save');
    		return;
		}else{
			for ( var i = 0; i < records.length; i++) {
				var record = records[i];
	    		if (record.firstName == null || record.firstName == ""){
	    			Ext.Msg.alert('Error','Candidate First Name should not be blank');
	    			return false;
	    		}else if (record.lastName == null || record.lastName == "") {
	    			Ext.Msg.alert('Error','Candidate Last Name should not be blank');
	    			return false;
				}else if (record.emailId == null || record.emailId == "") {
	    			Ext.Msg.alert('Error','Candidate Email Id should not be blank');
	    			return false;
				}else if (record.marketingStatus == null || record.marketingStatus == "") {
	    			Ext.Msg.alert('Error','Candidate marketing status should not be blank');
	    			return false;
				}else if (record.rateTo != 0 && record.rateTo < record.rateFrom) {
	    			Ext.Msg.alert('Error','Rate to should be greater than rate from.');
	    			return false;
				}
			}
	    	records = Ext.JSON.encode(records);
	    	app.candidateService.saveCandidates(records,this.onSave,this);
		}
	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error','Saved Successfully');
		this.modifiedIds =  new Array();
		app.candidateMgmtPanel.candidatesSearchPanel.search();
	},
	
	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Candidate to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Candidate","Do you delete selected Candidate?", this.deleteCandidate, this);
		}
	},
	
	deleteCandidate: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.candidateService.deleteCandidate(selectedRecord.data.id, this.onDeleteCandidate, this);
		}
	},
	
	onDeleteCandidate : function(data){
		if (!data.success) {
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.candidateMgmtPanel.candidatesSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	},

	showEmail : function(store) {
		var emailWindowPanel = new tz.ui.EmailWindowPanel();
		this.candidates = store.collect('fullName');
		this.emailIds = store.collect('emailId');

		emailWindowPanel.candidates = this.candidates;
		emailWindowPanel.emailIds = this.emailIds;

		var container = new Ext.container.Container({
			layout: {
		        type: 'table',
		        columns: 4,
		    },
		    items: []
		});
		var items = [];
		for ( var i = 0; i < this.candidates.length; i++) {
	        var field = new Ext.form.Label({
	        	id:this.emailIds[i],
	            html:this.candidates[i]+'&nbsp <a onclick="javascript:app.candidateMgmtPanel.candidatesGridPanel.removeEmailId(\''+this.emailIds[i]+'\');" href="#"></a>',
	            cls:'email-labels',  
	            style: {
	            	color: '#FFFFFF',
	            	backgroundColor:'#4C4C4C'
	            },
	        });
	        items.push(field);
		}

		container.items.add(items);
		emailWindowPanel.candidatesPanel.add(container);

		var addEmailButton = new Ext.Button({
			id : 'addEmailButton_seperate',
            text: '',
            iconCls : 'btn-add',
            handler: function(){
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').reset();
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').reset();
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').show();
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').show();
            	Ext.getCmp('addEmailButton_seperate').hide();
            	Ext.getCmp('saveEmailButton_seperate').show();
            	emailWindow.items.items[0].candidatesPanel.doLayout();
    		}
        });

		var saveEmailButton = new Ext.Button({
			margin : '0 0 0 10',
            id : 'saveEmailButton_seperate',
            icon : 'images/accept.png',
            hidden : true,
            handler: function(){
            	if (! emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').isValid() || 
            			! emailWindow.items.items[0].candidatesPanel.form.findField('emailId').isValid() ) {
            		Ext.Msg.alert('Error','Please fix the errors.');
            		return false;
				}
            	var candidateName = emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').getValue();
            	var emailId = emailWindow.items.items[0].candidatesPanel.form.findField('emailId').getValue();
            	var field = new Ext.form.Label({
    	        	id: emailId,
    	            html: candidateName+'&nbsp <a onclick="javascript:app.candidateMgmtPanel.candidatesGridPanel.removeEmailId(\''+emailId+'\');" href="#"></a>',
    	            cls:'email-labels',  
    	            style: {
    	            	color: '#FFFFFF',
    	            	backgroundColor:'#4C4C4C'
    	            },
    	        });
            	emailWindow.items.items[0].candidatesPanel.items.items[0].items.add(field);
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').hide();
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').hide();
            	Ext.getCmp('addEmailButton_seperate').show();
            	Ext.getCmp('saveEmailButton_seperate').hide();
            	emailWindow.items.items[0].candidatesPanel.doLayout();
            	app.candidateMgmtPanel.candidatesGridPanel.emailIds.push(emailId);
            	app.candidateMgmtPanel.candidatesGridPanel.candidates.push(candidateName);
    		}
        });

		var container2 = new Ext.container.Container({
			fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth :50},
			layout: {
		        type: 'table',
		        columns: 4,
		    },
		    items: [addEmailButton,
		            {
		    	xtype: 'textfield',
				name: 'candidateName',
				fieldLabel: 'Name',
				allowBlank: false,
				hidden : true,
				labelWidth :50
			},{
				xtype: 'textfield',
				name: 'emailId',
				fieldLabel: 'Email Id',
				allowBlank: false,
				hidden : true,
				vtype: 'email',
				labelWidth :60
			},
			saveEmailButton
			]
		});
		emailWindowPanel.candidatesPanel.add(container2);
		
		if (Ext.getCmp('emailCandidateWindow') == null) {
			var emailWindow = Ext.create('Ext.window.Window', {
			    title: 'Email',
			    id : 'emailCandidateWindow',
			    width: 1000,	
			    height : app.mainViewport.height-90,
			    modal: true,
			    autoScroll:true,
			    items: [emailWindowPanel],
			    buttons: [{
			    	text: 'Send All',
		            handler: function(){
		            	if (emailWindow.items.items[0].form.isValid()) {
		            		var params = new Array();
		            		params.push(['from','=', emailWindow.items.items[0].fromEmailCombo.getValue()]);
		            		params.push(['to','=', app.candidateMgmtPanel.candidatesGridPanel.emailIds.toString()]);
		            		params.push(['candidates','=', app.candidateMgmtPanel.candidatesGridPanel.candidates.toString()]);
		            		params.push(['subject','=', emailWindow.items.items[0].form.findField('subject').getValue()]);
		            		params.push(['message','=', emailWindow.items.items[0].form.findField('message').getValue()]);
			            	var filter = getFilter(params);
			            	app.candidateMgmtPanel.candidatesGridPanel.sendEmailConform(filter);
						}//if
		            }//handler
		        }]//buttons
			});
		}
		Ext.getCmp('emailCandidateWindow').show();
	},

	sendEmailConform : function(filter) {
		this.emailFilter = filter;
		Ext.Msg.confirm("Confirm","Do you want to send email to "+this.emailIds.length+" candidates?", this.sendEmail, this);
	},

	sendEmail : function(dat) {
		if (dat=='yes') {
			var filter = this.emailFilter;
			app.reportsService.sendHomeEmail(Ext.encode(filter),this.onSendEmail,this);	
		}
	},
	
	onSendEmail : function(data) {
		if (data.success) {
			Ext.getCmp('emailCandidateWindow').close();
			Ext.Msg.alert('Success','Email sent successfully.');
			var filter = this.emailFilter;
			app.reportsService.sendSummaryEmail(Ext.encode(filter));
		}else
			Ext.Msg.alert('Error',data.errorMessage);
	},
	
	removeEmailId : function(emailId) {
		//Ext.getCmp('jobEmailPanel').remove(Ext.getCmp(id),true);
		Ext.getCmp(emailId).hide();
		this.emailIds.splice(this.emailIds.indexOf(emailId), 1);
		this.candidates.splice(this.emailIds.indexOf(emailId), 1);
	},

	shareRequirement : function(record) {
		if (record != null) {
			record = record.data;
			var detailsTable = "<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>"+
								"<tr><th>Name</th><td>"+record.firstName+' '+record.lastName+'</td></tr>'+
								"<tr><th>Email id</th><td>" +record.emailId+'</td></tr>'+
								"<tr><th>Contact number</th><td>" +record.contactNumber+'</td></tr>'+
								"<tr><th>City & State</th><td>" +record.cityAndState+'</td></tr>'+
								"<tr><th>Immigration Status</th><td>" +record.immigrationStatus+'</td></tr>'+
								"<tr><th>Contact Address</th><td>" +record.contactAddress+'</td></tr>'+
								"<tr><th>Current Job title</th><td>" +record.currentJobTitle+'</td></tr>';

			if (record.availability != null && record.availability != '')
				detailsTable += "<tr><th>Availability</th><td>" +Ext.Date.format(new Date(record.availability),'m/d/Y')+'</td></tr>';
			else
				detailsTable += "<tr><th>Availability</th><td>N/A" +'</td></tr>';
			if (record.startDate != null && record.startDate != '')
				detailsTable += "<tr><th>Start Date</th><td>" +Ext.Date.format(new Date(record.startDate),'m/d/Y')+'</td></tr>';
			else
				detailsTable += "<tr><th>Start Date</th><td>N/A" +'</td></tr>';
			
			detailsTable += "<tr><th>Experience</th><td>" +record.experience+'</td></tr>'+
						"<tr><th>Marketing Status</th><td>" +record.marketingStatus+'</td></tr>'+
						"<tr><th>Communication</th><td>" +record.communication+'</td></tr>'+
						"<tr><th>Personality</th><td>" +record.personality+'</td></tr>'+
						"<tr><th>Expected Rate</th><td>" +record.expectedRate+ " From : </b>"+record.rateFrom +" To : </b>"+record.rateTo +'</td></tr>'+
						"<tr><th>Education</th><td>" +record.education+'</td></tr>'+
						"<tr><th>Highest Qualification Held</th><td>" +record.highestQualification+'</td></tr>'+
						"<tr><th>Employment Type</th><td>" +record.employmentType+'</td></tr>'+
						"<tr><th>Skill Set</th><td>" +record.skillSet+'</td></tr>'+
						"<tr><th>Role Set</th><td>" +record.currentRoles+'</td></tr>'+
						"<tr><th>Target Skill Set</th><td>" +record.targetSkillSet+'</td></tr>'+
						"<tr><th>Target Roles</th><td>" +record.targetRoles+'</td></tr>'+
						"<tr><th>Certifications</th><td>" +record.allCertifications.replace(/ -- /g, ' , ')+'</td></tr></table>';

			this.manager.showSharePanel();
			this.manager.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').setValue(detailsTable);
			this.manager.requirementSharePanel.fileName = record.firstName+' '+record.lastName;
			this.manager.requirementSharePanel.data = '';
			this.manager.requirementSharePanel.openedFrom = 'Candidates Grid';
		}

		this.manager.requirementSharePanel.emailShortlisted.disable();
		this.manager.requirementSharePanel.emailConfidential.disable();
	},
	
	loadSubmissions : function() {
    	var params = new Array();
    	var candidateIds = this.store.collect('id');
    	if (candidateIds.length > 0) {
        	params.push(['table','=', "submissions"]);
        	params.push(['candidateId','=', candidateIds.toString()]);
        	var filter = getFilter(params);
        	filter.pageSize=1000;
        	app.requirementService.getSubRequirements(Ext.JSON.encode(filter),this.onLoadSubmissions,this);
		}
	},

	onLoadSubmissions : function(data) {
		if (data.rows.length > 0) {
			var submissionsStore = new tz.store.Sub_RequirementStore();
			submissionsStore.add(data.rows);
	    	var headings = "<tr style='background-color:#dddddd;font-weight:bold;'>" +
			"<td>Vendor</td>" +
			"<td>Client</td>" +
			"<td>Submitted Date</td>" +
			"<td>Submitted Rate-1</td>" +
			"<td>Submitted Rate-2</td>" +
			"<td>Posting Date</td>" +
			"<td>Posting Title</td>" +
			"<td>Location</td></tr>";

    		var candidateIds = this.store.collect('id');
    		for ( var i = 0; i < candidateIds.length; i++) {
    			submissionsStore.clearFilter();
    			submissionsStore.filterBy(function(rec) {
    			    return rec.get('candidateId') === Number(candidateIds[i]);
    			});
    			var table ="";
    			for ( var j = 0; j < submissionsStore.getCount() && j< 10; j++) {
    				var rec = submissionsStore.getAt(j);
					table += "<tr bgcolor='#ffffff'><td>"+rec.data.vendor+"</td>" +
							"<td>"+rec.data.client+"</td>" +
							"<td>"+Ext.Date.format(rec.data.submittedDate,'m/d/Y')+"</td>" +
							"<td>"+rec.data.submittedRate1+"</td>" +
							"<td>"+rec.data.submittedRate2+"</td>" +
							"<td>"+Ext.Date.format(rec.data.postingDate,'m/d/Y')+"</td>" +
							"<td>"+rec.data.postingTitle+"</td>" +
							"<td>"+rec.data.location+"</td></tr>" ;
				}
    			if (table != ''){
    				table = "<table cellpadding='5' bgcolor='#575252'>"+headings+table+'</table>';
    				var vendors = submissionsStore.collect('vendor');
    				vendors.sort();
    				var clients = submissionsStore.collect('client');
    				clients.sort();
    				var vendorsList = '';
    				for ( var j = 0; j < vendors.length; j++) {
    	    			submissionsStore.filterBy(function(rec) {
    	    			    return rec.get('candidateId') === Number(candidateIds[i]) && rec.get('vendor') === vendors[j];
    	    			});
    	    			vendorsList += vendors[j]+' - '+submissionsStore.getCount()+', ';
					}
    				vendorsList = vendorsList.substring(0,vendorsList.length-2);
    				table += '<br><big><b>Submitted Vendors :</b> '+vendorsList+'</big>';
    				table += '<br><br><big><b>Submitted Clients :</b> '+clients.toString()+'</big>';
    			}
    			else
    				table = "No submissions";
    			var employeeName = this.store.getById(candidateIds[i]).data.fullName;
    			this.store.getById(Number(candidateIds[i])).data.submissions = '<b>'+employeeName+' previous submissions : </b>'+table;
			}
    		this.getView().refresh();
		}
	}
	
});

function booleanRenderer(val, metadata, record, rowIndex, colIndex, store) {
	if (val == true){
		metadata.tdAttr = 'data-qtip="' + 'YES' + '"';
		return  'YES' ;
	}else if (val == false){
		metadata.tdAttr = 'data-qtip="' + 'NO' + '"';
		return 'NO';
	}
	return val;
}

function activeRenderer(val, metadata, record, rowIndex, colIndex, store) {
	if (val == true) {
		metadata.tdAttr = 'data-qtip="' + "Yes" + '"';
		return '<span style="color:87E320;">' + "Yes" + '</span>';
	}else if (val == false) {
		metadata.tdAttr = 'data-qtip="' + "No" + '"';
		return '<span style="color:red;">' + "No" + '</span>';
	}
	return 'N/A';
}

function followUpRenderer(val, metadata, record, rowIndex, colIndex, store) {
	metadata.tdAttr = 'data-qtip="' + val + '"';
	if (val == "YES") {
		return '<span style="color:red;">' + val + '</span>';
	} 
	if (val == "NO") {
		return '<span style="color:#87E320;">' + val + '</span>';
	}
	return val;
}

function candidateRender(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null || value == "") 
		return 'N/A';
    return value;
}

function endDateRenderer(value, metadata, record) {
	if (value != null) {
		metadata.tdAttr = 'data-qtip="' + Ext.Date.format(value,'m/d/Y') + '"';
		var date1 = new Date(value);
		var date2 = new Date();
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		//value = value.getMonth()+1 + "/" + value.getDate() + "/" + value.getFullYear();
		if (diffDays <30 && diffDays > 1) {
			return '<span style="color:#E46D0A">' + Ext.Date.format(value,'m/d/Y') + '</span>';
		}else
			return Ext.Date.format(value,'m/d/Y');
	}
}

function candidateAlertRender(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null || value=="") 
    	return 'N/A' ;
    return '<span style="color:red;">' + value + '</span>';
}
