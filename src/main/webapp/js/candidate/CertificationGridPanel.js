Ext.define('tz.ui.CertificationGridPanel', {
    extend: 'Ext.grid.Panel',
    title: 'Certifications',
    frame:true,
    anchor:'100%',
    height:350,
    width :'100%',    
    listeners: {
    	edit: function(editor, event){
    		var record = event.record;
    		if(event.originalValue != event.value){
    			this.modifiedIds.push(record.data.id);
    		}
        	this.getView().refresh();
        },
        beforeedit: function(editor, event) {
      		var record = event.record;
      		if (record.data.sponsoredBy == 'Own') {
				if (event.column.dataIndex == 'latestNVT' || event.column.dataIndex == 'reimbursementStatus' || event.column.dataIndex == 'version' 
					|| event.column.dataIndex == 'epicUserWeb' || event.column.dataIndex == 'expensesStatus' || event.column.dataIndex == 'cpsPaymentToEpic' 
					|| event.column.dataIndex == 'paymentDetails' || event.column.dataIndex == 'epicInvoiceAmount' || event.column.dataIndex == 'epicInvoiceNo' 
					|| event.column.dataIndex == 'epicInvoiceComments' || event.column.dataIndex == 'verified' || event.column.dataIndex == 'expensesIncurred'
					|| event.column.dataIndex == 'expensesPaid' || event.column.dataIndex == 'expenseComments' ) {
					return false;
				}
			}
				
        }
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.certificationStore;
        me.modifiedIds = new Array();
        
        me.columns = [
			{
				xtype: 'datecolumn',
				header : 'Date Achieved',
				dataIndex : 'dateAchieved',
				width:80,
	            renderer: Ext.util.Format.dateRenderer('m/d/Y'),
	            editor: {
	                xtype: 'datefield',
	                format: 'm/d/y'
	            }
			},{
				text : 'Certification Name',
				dataIndex : 'settingsId',
				width :  120,
				editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                displayField: 'value',
	                valueField: 'id',
	                emptyText:'Select...',
	    		    store : ds.certificationsStore,
	                lazyRender: true,
	                anyMatch:true,
	                listClass: 'x-combo-list-small'
                },
                renderer:function(value, metadata, record){
                	var rec = ds.certificationsStore.getById(value);
                	if(rec){
                	    return rec.data.value;
                	}else{
                		return value;	
                	}
	            }
			},{
				text : 'Old Name',
				dataIndex : 'name',
				width :  120,
			},{
				text : 'Status',
				dataIndex : 'status',
				width:80,
				editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                displayField: 'value',
	                valueField: 'name',
	                emptyText:'Select...',
	    		    store : ds.certificationStatusStore,
	                lazyRender: true,
	                listClass: 'x-combo-list-small'
                },
                renderer:function(value, metadata, record){
                	var rec = ds.certificationStatusStore.getAt(ds.certificationStatusStore.findExact('name',value));
                	if(rec){
                	    metadata.tdAttr = 'data-qtip="' + value + '"';
                	    if (rec.data.value == 'Approval Process') 
                	    	rec.data.value.style = "background-color:#d9ead3;border-color: #d9ead3;";
                	    else if (value == 'Yet to Start') 
                	    	metadata.style = "background-color:#fff2cc;border-color: #fff2cc;";
                	    else if (rec.data.value == 'Certification in progress') 
                	    	metadata.style = "background-color:#cfe2f3;border-color: #cfe2f3;";
                	    else if (rec.data.value == 'Certified') 
                	    	metadata.style = "background-color:#ead1dc;border-color: #ead1dc;";
                	    else if (rec.data.value == 'Issue') 
                	    	metadata.style = "background-color:#ff9900;border-color: #ff9900;";
                	    
                	    return rec.data.value;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;	
                	}
	            }
			},{
				text : 'Sponsored By',
				dataIndex : 'sponsoredBy',
				width :  120,
				renderer : sponsoredRenderer,
				editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                displayField: 'value',
	                valueField: 'name',
	                emptyText:'Select...',
	    		    store : ds.certificationSponsoredStore,
	                lazyRender: true,
	                listClass: 'x-combo-list-small'
                },
			},{
				text : 'Latest NVT',
				dataIndex : 'latestNVT',
				width:80,
				editor: {
                    xtype: 'textfield',
                    maxLength:45,
  	                enforceMaxLength:true,
                }
			},{
				text : 'Reimbursement Status',
				dataIndex : 'reimbursementStatus',
				width :  120,
				editor: {
                    xtype: 'textfield',
                    maxLength:45,
  	                enforceMaxLength:true,
                }
			},{
				text : 'Version',
				dataIndex : 'version',
				width:80,
				editor: {
                    xtype: 'textfield',
                    maxLength:20,
  	                enforceMaxLength:true,
                }
			},{
                dataIndex: 'certificationComments',
                text: 'Certification Comments',
                labelAlign:'center',
    			align : 'left',
                width:150,
                editor: {
                    xtype: 'textarea',
                    height :50,
                    maxLength:500,
  	                enforceMaxLength:true,
                }
            },{
				text : 'Epic User Web',
				dataIndex : 'epicUserWeb',
				editor: {
                    xtype: 'textfield',
                    maxLength:100,
  	                enforceMaxLength:true,
                }
			},{
				text : 'Expenses Paid or not',
				dataIndex : 'expensesStatus',
				renderer : greenTooltipRenderer,
				editor: {
                    xtype: 'textfield',
                    maxLength:45,
  	                enforceMaxLength:true,
                }
			},{
				text : 'CPS Payment to Epic',
				dataIndex : 'cpsPaymentToEpic',
				renderer : Ext.util.Format.usMoney,
				width:90,
				editor: {
                    xtype: 'numberfield',
                }
			},{
				text : 'Payment to Trainer',
				dataIndex : 'paymentDetails',
				renderer : greenTooltipRenderer,
				editor: {
                    xtype: 'textfield',
                    maxLength:100,
  	                enforceMaxLength:true,
                }
			},{
				text : 'Epic Invoice Amt',
				dataIndex : 'epicInvoiceAmount',
				renderer : function(value){
					return '<b>'+Ext.util.Format.usMoney(value)+'</b>';
				},
				width:90,
				editor: {
                    xtype: 'numberfield',
                }
			},{
				text : 'Epic Invoice No',
				dataIndex : 'epicInvoiceNo',
				renderer : greenTooltipRenderer,
				width:90,
				editor: {
                    xtype: 'textfield',
                    maxLength:100,
  	                enforceMaxLength:true,
                }
			},{
				text : 'Epic Invoice Comments',
				dataIndex : 'epicInvoiceComments',
				width:120,
				editor: {
                    xtype: 'textarea',
                    height :50,
                    maxLength:250,
  	                enforceMaxLength:true,
                }
			},{
				text : 'Verified',
				dataIndex : 'verified',
				width:90,
				editor: {
                    xtype: 'textfield',
                    maxLength:45,
  	                enforceMaxLength:true,
                }
			},{
				text : 'Expenses Incurred',
				dataIndex : 'expensesIncurred',
				renderer : Ext.util.Format.usMoney,
				width:90,
				editor: {
                    xtype: 'numberfield',
                }
			},{
				text : 'Expenses Paid',
				dataIndex : 'expensesPaid',
				renderer : function(value){
					return '<b>'+Ext.util.Format.usMoney(value)+'</b>';
				},
				width:90,
				editor: {
                    xtype: 'numberfield',
                }
			},{
				text : 'Expense Comments',
				dataIndex : 'expenseComments',
				flex : 1,
				editor: {
                    xtype: 'textarea',
                    height :50,
                    maxLength:250,
  	                enforceMaxLength:true,
                }
			},{
				xtype: 'gridcolumn',
				dataIndex: 'createdUser',
				text: 'Created User',
				width:100,
			},{
				xtype: 'gridcolumn',
				dataIndex: 'lastUpdatedUser',
				text: 'Last Updated User',
				width:100,
			},{
				xtype: 'datecolumn',
				dataIndex: 'created',
				format: "m/d/Y H:i:s A",
				text: 'Created',
				hidden:true,
				width:150,
			},{
				xtype: 'datecolumn',
				dataIndex: 'lastUpdated',
				format: "m/d/Y H:i:s A",
				text: 'Last Updated',
				hidden:true,
				width:150,
             }
        ];
        
        me.saveButton = new Ext.Button({
            text: 'Save',
            iconCls : 'btn-save',
            tooltip : 'Save',
            tabIndex:72,
            scope : this,
            handler: function(){
          	  this.save();
            }
        });
        
        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [{
        		xtype: 'button',
        		text: 'Add Certification',
        		tooltip: 'Add new Certification',
        		scope : this,
        		iconCls : 'btn-add',
        		tabIndex:71,
        		handler : me.addNew
        	},'-'
        	,me.saveButton
        	,'-',{
        		text : 'Delete',
        		iconCls : 'btn-delete',
        		tooltip : 'Delete selected Certification',
        		tabIndex:73,
        		scope: this,
        		handler: function() {
        			this.deleteConfirm();
        		}
        	},'-',{
        		xtype: 'button',
        		text: 'Reset',
        		tooltip: 'Reset',
        		tabIndex:74,
        		scope: this,
        		handler : function(grid, rowIndex, colIndex) {
        			this.loadCertifications();
        		} 
        	}]
        }];
   
        me.viewConfig = {
        		stripeRows: true,
        };

        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        me.callParent(arguments);
    }, 
    
    plugins: [
    	{
    		ptype : 'cellediting',
    		clicksToEdit: 2
    	}
    ],
	
    loadCertifications : function() {
    	this.modifiedIds = new Array();
    	var candidateId = this.manager.myPanel.form.findField('id').getValue();
    	if (candidateId != null) {
        	var params = new Array();
        	params.push(['candidateId','=', candidateId]);
        	var filter = getFilter(params);
        	filter.pageSize=100;
        	ds.certificationStore.loadByCriteria(filter);
		}
	},
    
    addNew : function() {
    	var candidateId = this.manager.myPanel.form.findField('id').getValue();
    	if (candidateId == 0 || candidateId == null || candidateId == '') {
    		Ext.Msg.alert('Error','Please save candidate before adding certification');
    		return false;
		}
   	 	var r = Ext.create( 'tz.model.Certification',{
   	 		candidateId : candidateId,
   	 		dateAchieved :''
	     });
   	 	
        this.store.insert(0, r);
	},
	
	save : function() {
		var certificates = new Array();
    	for ( var i = 0; i < this.store.data.length ; i++) {
			var record = this.store.getAt(i);

			if (record.data.id == 0) {
				delete record.data.id;
				certificates.push(record.data);
			}
			if (record.data.settingsId == null || record.data.settingsId == 0) {
	    		Ext.Msg.alert('Error','Please select certification name');
	    		return false;
			}
    		if (this.modifiedIds.indexOf(record.data.id) != -1) {
    			certificates.push(record.data);	
			}
		}
    	if (certificates.length == 0) {
    		Ext.Msg.alert('Error','There are no updated certifications to save');
    		return false;
		}

    	certificates = Ext.JSON.encode(certificates);
    	app.candidateService.saveCertificates(certificates , this.onSave, this);
	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
			this.loadCertifications();
		}else
			Ext.Msg.alert('Error',data.errorMessage);
	},

	
	deleteConfirm : function() {
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Certificate to delete.');
		} else{
			Ext.Msg.confirm("Certificate", "This will delete the selected Certificate!!!\n Do you want to continue?", this.deleteImmigration, this);
		}
	},

	deleteImmigration: function(dat) {
		if (dat == 'yes') {
			var id = this.getView().getSelectionModel().getSelection()[0].data.id;
			app.candidateService.deleteCertificate(id,this.onDelete,this);
		}
	},
	
	onDelete : function(data) {
		if (data.success) {
			Ext.MessageBox.show({
				title : 'Success',
	            msg: 'Certificate deleted successfully',
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.INFO
	        });
			this.loadCertifications();
		}else{
			Ext.MessageBox.show({
				title : 'Error',
	            msg: 'Delete Falied',
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
	},
	
	certificateValid : function() {
		for(var i = 0; i < ds.certificationStore.getCount(); i++) {
			var record = ds.certificationStore.getAt(i);
			if (record.data.employeeId == "" || record.data.employeeId == null || record.data.employeeId == 0) {
				Ext.Msg.alert('Error','Please select Employee');
				return false;
			}
		}
		return true;
	}
	
});

function greenTooltipRenderer(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    metadata.style = "background-color:#d9ead3;";
    return value;
}
function sponsoredRenderer(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == 'CPS') 
    	metadata.style = "background-color:#9fc5e8;";
    else if (value.toLocaleLowerCase().search('independent') != -1) 
    	metadata.style = "background-color:#ffe599;";
    
    return value;
}
