Ext.define('tz.ui.CandidatePanel', {
	extend : 'tz.ui.BaseFormPanel',
	
	bodyPadding: 10,
    title: '',
    anchor:'100% 100%',
    
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 160},
	
	initComponent : function() {
		var me = this;
		me.modified = false;
		me.closeButtonClicked = false;
		me.openedFrom ="";
		me.candidate = "";
		me.locationCriteria = null;
		me.clientCriteria = null;
		me.certificationCriteria = null;
		me.clientAllCriteria = null;
		me.vendorCriteria = null;
		me.projectCriteria = null;
		me.emailUpdated = false;
		me.phoneUpdated = false;
		me.immigrationUpdated = false;
		 
		me.certificationGridPanel = new tz.ui.CertificationGridPanel({manager:me});
		me.projectGridPanel = new tz.ui.ProjectGridPanel({manager:me});
		
		me.immigrationCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Immigration Status",
			name: 'immigrationStatus',
			tabIndex:8,
			queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
			store: ds.immigrationTypeStore,
			forceSelection:true,
			allowBlank:true,
		    listeners: {
				scope:this,
				select: function(combo,value){ 
					if(combo.value == 'CITIZENSHIP')
						this.myPanel.form.findField('immigrationVerified').setValue({immigrationVerified:'Not Needed'});
					else if(combo.value == 'N/A')
						this.myPanel.form.findField('immigrationVerified').setValue({immigrationVerified:'NO'});
					else
						this.myPanel.form.findField('immigrationVerified').reset();
				},
				change: function(combo,value){
					me.immigrationUpdated = true;
				}
		    }
		});

		me.qualificationCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Highest Qualification Held',
		    store: ds.qualificationStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    name : 'highestQualification',
		    tabIndex:30,
		});
		
		me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    store: ds.candidateTypeStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    name : 'type',
		    tabIndex:29,
		});
		
		me.currentJobCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Current Job Title",
            name: 'currentJobTitle',
		    store: ds.currentJobStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:6,
		});

		me.marketingStatusCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Marketing Status *",
            name: 'marketingStatus',
            store: ds.marketingStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:false,
		    valueField: 'name',
		    tabIndex:32,
		});

		me.aboutPartnerCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "About Partner",
            name: 'aboutPartner',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:43,
		});

		me.followUpCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Follow Up",
            name: 'followUp',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:31,
		});

		me.openToCTHCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Open To CTH",
            name: 'openToCTH',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:33,
		});

		me.employmentTypeCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
			fieldLabel: "Employment Type",
            name: 'employmentType',
	        tabIndex:42,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.employmentTypeStore,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					this.form.findField('selectedEmploymentType').setValue(combo.rawValue);
				}
		    }
		});

		me.sourceCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Source",
            name: 'source',
            store: ds.candidateSourceStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:28,
		});

		me.locationCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Location",
            name: 'location',
            store: ds.candidateLocationStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:7,
		});

		me.communicationCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Communication",
            name: 'communication',
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.communicationStore,
            editable: false,
		    allowBlank:true,
		    //width : 220,
		    tabIndex:35,
		});

		me.personalityCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Personality",
            name: 'personality',
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.personalityStore,
            editable: false,
		    allowBlank:true,
		    //width : 220,
		    tabIndex:41,
		});

		me.experienceCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Experience",
            name: 'experience',
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.experienceStore,
            editable: false,
		    allowBlank:true,
		    //width : 220,
		    tabIndex:26,
		});

		me.targetRolesCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Target Roles",
            name: 'selectedTargetRoles',
	        tabIndex:54,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.targetRolesStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var targetRoles = this.form.findField('targetRoles').getValue();
					if (targetRoles == null || targetRoles == '' )
						targetRoles = new Array();
					else
						targetRoles = targetRoles.split(',');
					if (targetRoles.indexOf(combo.value) == -1) {
						targetRoles.push(combo.value);
					}else{
						targetRoles.splice(targetRoles.indexOf(combo.value), 1);
					}
					me.targetRolesCombo.reset();
					this.form.findField('targetRoles').setValue(targetRoles.toString());
				},
		    }
		});

		me.roleSetCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Role Set",
            name: 'selectedRoleset',
	        tabIndex:50,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'id',
            store: ds.targetRolesStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var roleSetIds = this.myPanel.form.findField('roleSetIds').getValue();
					if (roleSetIds == null || roleSetIds == '' )
						roleSetIds = new Array();
					else
						roleSetIds = roleSetIds.split(',');
					if (roleSetIds.indexOf(combo.value.toString()) == -1) 
						roleSetIds.push(combo.value.toString());
					else
						roleSetIds.splice(roleSetIds.indexOf(combo.value.toString()), 1);
					var roleSets = new Array();
					for ( var i = 0; i < roleSetIds.length; i++) {
						roleSets.push(ds.targetRolesStore.getById(Number(roleSetIds[i])).data.name);
					}
					me.roleSetCombo.reset();
					this.myPanel.form.findField('roleSetIds').setValue(roleSetIds.toString());
					this.myPanel.form.findField('roleSet').setValue(roleSets.toString());
				},
		    }
		});

		me.targetSkillSetCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Target Skill Set",
            name: 'selectedTargetSkillSet',
	        tabIndex:52,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'id',
            store: ds.skillSetStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var skillSetIds = this.myPanel.form.findField('targetSkillSetIds').getValue();
					if (skillSetIds == null || skillSetIds == '' )
						skillSetIds = new Array();
					else
						skillSetIds = skillSetIds.split(',');
					if (skillSetIds.indexOf(combo.value.toString()) == -1) {
						skillSetIds.push(combo.value.toString());
					}else{
						skillSetIds.splice(skillSetIds.indexOf(combo.value.toString()), 1);
					}
					var skillsets = new Array();
					for ( var i = 0; i < skillSetIds.length; i++) {
						skillsets.push(ds.skillSetStore.getById(Number(skillSetIds[i])).data.name);
					}
					me.targetSkillSetCombo.reset();
					this.myPanel.form.findField('targetSkillSetIds').setValue(skillSetIds.toString());
					this.myPanel.form.findField('targetSkillSet').setValue(skillsets.toString());
				},
		    }
		});

		me.skillSetCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Skill Set",
            name: 'selectedSkillSet',
	        tabIndex:48,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'id',
            store: ds.skillSetStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    enableKeyEvents : true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var skillSetIds = this.myPanel.form.findField('skillSetIds').getValue();
					if (skillSetIds == null || skillSetIds == '' )
						skillSetIds = new Array();
					else
						skillSetIds = skillSetIds.split(',');
					if (skillSetIds.indexOf(combo.value.toString()) == -1) {
						skillSetIds.push(combo.value.toString());
					}else{
						skillSetIds.splice(skillSetIds.indexOf(combo.value.toString()), 1);
					}
					var skillsets = new Array();
					for ( var i = 0; i < skillSetIds.length; i++) {
						skillsets.push(ds.skillSetStore.getById(Number(skillSetIds[i])).data.name);
					}
					me.skillSetCombo.reset();
					this.myPanel.form.findField('skillSetIds').setValue(skillSetIds.toString());
					this.myPanel.form.findField('skillSet').setValue(skillsets.toString());
				},
		    }
		});

		me.skillSetGroupCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Skill Set Group",
            name: 'selectedSkillSetGroup',
	        tabIndex:48,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'id',
            store: ds.skillsetGroupStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    enableKeyEvents : true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var skillSetIds = this.myPanel.form.findField('skillSetGroupIds').getValue();
					if (skillSetIds == null || skillSetIds == '' )
						skillSetIds = new Array();
					else
						skillSetIds = skillSetIds.split(',');
					if (skillSetIds.indexOf(combo.value.toString()) == -1) {
						skillSetIds.push(combo.value.toString());
					}else{
						skillSetIds.splice(skillSetIds.indexOf(combo.value.toString()), 1);
					}
					var skillsets = new Array();
					for ( var i = 0; i < skillSetIds.length; i++) {
						skillsets.push(ds.skillsetGroupStore.getById(Number(skillSetIds[i])).data.name);
					}
					me.skillSetGroupCombo.reset();
					this.myPanel.form.findField('skillSetGroupIds').setValue(skillSetIds.toString());
					this.myPanel.form.findField('skillSetGroup').setValue(skillsets.toString());
				},
		    }
		});

		me.resumeHelpCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Resume Help",
            name: 'resumeHelp',
	        tabIndex:56,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.helpCandidateStore,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				change: function(combo,value){
					this.interviewHelpCombo.setValue(combo.value);
					this.jobHelpCombo.setValue(combo.value);
					if (combo.value == 'Other')
						this.form.findField('resumeHelpComments').show();
					else{
						this.form.findField('resumeHelpComments').hide();
						this.form.findField('resumeHelpComments').reset();
					}
					me.myPanel.doLayout();
				}
		    }
		});

		me.interviewHelpCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Interview Help",
            name: 'interviewHelp',
	        tabIndex:58,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.helpCandidateStore,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					if (combo.value == 'Other')
						this.form.findField('interviewHelpComments').show();
					else{
						this.form.findField('interviewHelpComments').hide();
						this.form.findField('interviewHelpComments').reset();
					}
				}
		    }
		});

		me.jobHelpCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Job Help",
            name: 'jobHelp',
	        tabIndex:60,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.helpCandidateStore,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					if (combo.value == 'Other')
						this.form.findField('jobHelpComments').show();
					else{
						this.form.findField('jobHelpComments').hide();
						this.form.findField('jobHelpComments').reset();
					}
				}
		    }
		});

		me.relocateCheckbox = Ext.create('Ext.form.Checkbox', {
        	boxLabel : 'Will Relocate',
            name : 'relocate',
            inputValue: '0',
            checked :true,
            labelAlign :'right',
            itemId : 'relocate',
            width : 120,
            tabIndex:15,
        });

		me.travelCheckbox = Ext.create('Ext.form.Checkbox', {
        	boxLabel : 'Will Travel',
            name : 'travel',
            inputValue: '0',
            checked :true,
            labelAlign :'right',
            itemId : 'travel',
            width : 110,
            tabIndex:16,
        });

		me.goodCandidateCheckbox = Ext.create('Ext.form.Checkbox', {
        	boxLabel : 'Good Candidate for Remote',
            name : 'goodCandidateForRemote',
            inputValue: '0',
            checked :false,
            labelAlign :'right',
            itemId : 'goodCandidateForRemote',
            width : 250,
            tabIndex:17,
        });

        me.criteriaPanel = Ext.create('Ext.form.Panel', {
        	frame:false,
        	xtype: 'container',
        	bodyPadding: 10,
        	border : 1,
        	items:[{
        		xtype : 'fieldset',
				border : 0,
				layout : 'column',
				items : [{
	   	        	 xtype: 'container',
	   	        	 columnWidth:1,
	   	             items: [{
	 					xtype:'displayfield',
						fieldLabel: '',
						height : 10,
						name: 'clientCriteria',
						value : "<b>Special criteria : </b><br><table cellpadding='2'>" +
								"<tr><td><div id='candidateCriteria_client'/></td></tr>" +
								"<tr><td><div id='candidateCriteria_clientAll'/></td></tr>" +
								"<tr><td><div id='candidateCriteria_location'/></td></tr>" +
								"<tr><td><div id='candidateCriteria_certification'/></td></tr>" +
								"<tr><td><div id='candidateCriteria_vendor'/></td></tr>" +
								"<tr><td><div id='candidateCriteria_project'/></td></tr>" +
								"</table>",
					}]
	   	         }]
        	}]
	     });

        me.showSuggestedCount = new Ext.form.field.Display({
        	fieldLabel: '',
        	name : 'showSuggestedCount'
        });

        me.suggestionsGrid = Ext.create('Ext.grid.Panel', {
            title: 'Suggested Jobs Openings',
            cls : 'suggested-Orange',
            frame:false,
            anchor:'98%',
            height:290,
            width:'100%',
            store: ds.suggestedRequirementStore,
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
    	        	xtype:'button',
    	        	icon : 'images/icon_copy.png',
    	        	text: 'Shortlist',
    	        	tabIndex : 80,
    	        	tooltip:'Shortlist this candidate for the selected job',
    	        	handler: function(){
    	        		me.shortlistCandidate();
    	        	}
    	        },'-',{
            		xtype: 'button',
            		text: 'Load',
            		tooltip: 'Load Suggested Jobs',
            		tabIndex : 81,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			me.loadRequirements();
				    	me.getSpecialCriteria();
            		} 
            	},'-',{
            		xtype: 'button',
            		text: 'Reset',
            		tooltip: 'Reset',
            		tabIndex : 81,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			me.loadRequirements();
            		} 
            	},'-',{
            		xtype: 'button',
            		text: 'Help',
            		tabIndex : 82,
            		tooltip: 'Help',
            		iconCls : 'icon_info',
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
         		        var legendWin = Ext.create("Ext.window.Window", {
         					title: 'Match score',
         					modal: true,
         					height      : 600,
         					width       : 800,
         					bodyStyle   : 'padding: 1px;',
         					html: "&nbsp<table cellpadding='5' bgcolor='#575252'>" +
         							"<tr bgcolor='#dfe8f6'> <th >Job Opening</th> <th >Candidate</th> <th >Score</th> <th >Logic</th> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Skill Set</td> <td >Skill set</td> <td >2</td> <td >for each match</td> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Skill Set</td> <td >Target Skill set</td> <td >1</td> <td >for each match</td> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Module</td> <td >Skill set</td> <td >3</td> <td >for each match</td> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Role Set</td> <td >Role Set</td> <td >3</td> <td >for  1 or more matches</td> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Role Set</td> <td >Target Role</td> <td >2</td> <td >for  1 or more matches</td> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Mandatory Certification</td> <td >Certification</td> <td >5</td> <td >for each match</td> </tr> </table>" +
         							"<br>" +
         							"&nbspA score of <b>6</b> will be added for <b>0.Highest Priority</b> jobs in the above list, a score of <b>4 for 1.Strong Req</b>,  <b>3 for 2.Explore or Hard</b>, will be added" +
         							"<br><br>&nbsp--The above match score is calculated for the jobs whose <b>Job Opening status</b> is '01.In-progress' and '02.Resume Ready'" +
         							"<br><br>&nbsp--If the Job is a <b>Remote Role</b> then match score is calculated for all the candidates with any <b>Marketing status</b>" +
         							"<br><br>&nbsp--If candidate is not <b>willing to Relocate/Travel</b>, the Job is <b>Relocation Needed</b> and <b>Travel needed</b>, Job will be ignored" +
         							"<br><br><b>&nbsp---------Special criteria - These are displayed separately as a link on the top---------</b>" +
         							"<br><br>&nbsp--Jobs which are local to candidate" +
         							"<br><br>&nbsp--Jobs with Same Client & Same Module Submissions ('01.In-progress', '02.Resume Ready')" +
         							"<br><br>&nbsp--Jobs with Same Client & Same Module Submissions (Any Job opening Status)" +
         							"<br><br>&nbsp--Jobs with certifications that candidate had ('01.In-progress', '02.Resume Ready')" +
         							"<br><br>&nbsp--Jobs with Same Vendor & Same Module Submission ( Any Job opening satatus )" +
         							"<br><br>&nbsp--Jobs with with Same Client & Same Module Project ( Any Job opening satatus )"
         				});
         				legendWin.show();
         		    } 
            	},'->',me.showSuggestedCount]
            }],
            
            columns: [{
            	xtype: 'actioncolumn',
            	width :35,
            	items : [{
            		icon : 'images/icon_edit.gif',
            		tooltip : 'View Requirement',
            		padding: 50,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			me.getRequirmentTab(me.suggestionsGrid.store.getAt(rowIndex));
            		}
            	}]
            },{dataIndex: 'matchScore',text: 'Match Score',align : 'center',width : 50,
                renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + record.data.matchScoreDetails + '"';
                	return value;
                }
          	},
            {text : 'Unique Id',dataIndex : 'id',width :  45,renderer: numberRender,},
            {dataIndex: 'hotness',text: 'Hotness',width :  45,
                renderer:function(value, metadata, record){
                	if (value == 0) {
                		metadata.style = "color:#009AD0;";
					}
                	var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.value.substring(rec.data.value.indexOf('.')+1)+ '"';
                		return rec.data.value.substring(rec.data.value.indexOf('.')+1);
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
	            }
		    },{xtype: 'datecolumn',text : 'Posting Date',dataIndex : 'postingDate',width :  60,format: "m/d/Y h:i:s A",},
		    {text : 'Job Opening Status',dataIndex : 'jobOpeningStatus',width :  60,
		    	renderer:function(value, metadata, record){
    				metadata.style = 'background-color:#'+getBackGround(record)+';'
    				if (value == '01.In-progress' && record.data.submittedDate != null && record.data.submittedDate != ""){
    					var count = record.data.submittedDate.split(',').length;
    					metadata.tdAttr = 'data-qtip="' + value.substring(value.indexOf('.')+1)+'<br>'+Ext.String.format('{0} Submission{1} made', count, count !== 1 ? 's' : '')+ '"';
    				}else
    					metadata.tdAttr = 'data-qtip="' + value.substring(value.indexOf('.')+1)+ '"';
            		return value.substring(value.indexOf('.')+1);
    			}
    		},{ dataIndex: 'postingTitle',text: 'Posting Title',
    			renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + value + '"';
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
            		return value;
                }
		    },{dataIndex: 'module',text: 'Module',width :  60,
                renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + value + '"';
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
            		return value;
                }
            },{dataIndex: 'shortlistedCount',text: 'Shortlisted Candidates',width :  60,align : 'center',
                renderer:function(value, metadata, record){
                	if (record.data.shortlistedCandidates != null && record.data.shortlistedCandidates != ''){
    					var ids = record.data.shortlistedCandidates.toString().split(',');
    					var tooltip='';
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contractorsStore.getById(parseInt(ids[i]));
    						if (rec){
    							tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
    						}
    					}
    					metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
                	}
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
            		return value;
                }
            },{header : 'Client',dataIndex : 'clientId',width :  120,
            	renderer:function(value, metadata, record){
                	var rec = ds.clientSearchStore.getById(value);
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
                	if(rec){
                		metadata.tdAttr = 'data-qtip="'+rec.data.name+'"';
                		return rec.data.name ;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{header : 'Vendor',dataIndex : 'vendorId',width :  120,
				renderer:function(value, metadata, record){
					metadata.style = 'background-color:#'+getBackGround(record)+';'
	            	if (value != null && value != '') {
						var ids = value.toString().split(',');
						var names ="";
						for ( var i = 0; i < ids.length; i++) {
							var rec = ds.vendorStore.getById(parseInt(ids[i]));
							if (rec)
								names += rec.data.name +", ";	
						}
                		metadata.tdAttr = 'data-qtip="'+names.substring(0,names.length-2)+'"';
                		if (ids.length > 1)
                			return '('+ids.length+') '+ names.substring(0,names.length-2) ;
                		return names.substring(0,names.length-2) ;
					}
	            	return 'N/A';
                }
			},
			{text : "Alert",dataIndex : 'addInfo_Todos',width :  80,renderer: alertRender,},
			{dataIndex: 'rateSubmitted',text: 'Max Bill Rate',renderer: renderValue,},
			{text : 'Location',dataIndex : 'location',width :  80,renderer: renderValue,},
			{dataIndex: 'cityAndState',text: 'City & State',renderer: renderValue,},
			{text : 'Roles and Responsibilities',dataIndex : 'requirement',width :  150,renderer: requirementRenderer},
			{dataIndex: 'skillSet',text: 'Skill Set',width:250,renderer: reqSkillsetRender,},
			{header : 'Candidate',dataIndex : 'candidateId',width :  120,
                renderer:function(value, metadata, record){
                	if (value != null && value != '') {
                		var ids = value.toString().split(',');
    					var names ="",tooltip='';
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contractorsStore.getById(parseInt(ids[i]));
    						if (rec){
    							names += rec.data.fullName+", ";	
    							tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
    						}
    					}
    					metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
    					return '('+ids.length+') '+ names.substring(0,names.length-2);
                	}
                	return 'N/A';
                }
			},{text : 'Submitted Date',dataIndex : 'submittedDate',width :  60,
                renderer:function(value, metadata, record){
                	if (value != null && value != "") {
                		var req_submittedDate = value.split(',');
                		var dates = new Array();
                		for ( var i = 0; i < req_submittedDate.length; i++) {
                			var id_dates = req_submittedDate[i].split('-');
       						var rec = ds.contractorsStore.getById(parseInt(id_dates[0]));
       						if (rec){
       							var letters = rec.data.fullName.match(/\b(\w)/g);
       							var joinName = letters.join('');
       							dates.push(id_dates[1]+'-'+joinName);
       						}
       								
    					}
                		var tooltip = dates.toString();
                		tooltip = tooltip.replace(/,/g, "<br>")
    					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
    					return dates.toString();
					}
                	return value;
                }
			},{text : 'Resume',dataIndex : 'resume',width :  60,
                renderer:function(value, metadata, record){
                	if (value != null) {
    					metadata.tdAttr = 'data-qtip="' + value + '%"';
    					return value+'%';
					}
                	return 'N/A';
                }

    		},
    		{dataIndex: 'certifications',text: 'Certifications',width:120,renderer: renderValue,},
    		{text : 'Employer',dataIndex : 'employer',width :  40,renderer: employerRenderer,},
    		{dataIndex: 'comments',text: 'Comments',renderer: renderValue,width:150,},
    		{text : 'Communication Required',dataIndex : 'communication',width :  80,renderer: renderValue,},
    		{text : 'Public Link',dataIndex : 'publicLink',renderer: linkRenderer,width :  80,},
    		{text : 'Source',dataIndex : 'source',renderer: renderValue,},
    		{header : 'Contact Person',dataIndex : 'contactId',width :  120,
                renderer:function(value, metadata, record){
                	if (value != null) {
    					var ids = value.toString().split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.fullName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return names.substring(0,names.length-2);
                	}
                }
			},{text : 'Last Updated User',dataIndex : 'lastUpdatedUser',hidden :true,width:80,
				renderer:function(value, metadata, record){
               		metadata.tdAttr = 'data-qtip="' + value + '"';
                	return value;
                }
			},
			{xtype: 'datecolumn',dataIndex: 'created',text: 'Created',format: "m/d/Y H:i:s A",width:100,hidden :true,},
			{xtype: 'datecolumn',dataIndex: 'lastUpdated',format: "m/d/Y H:i:s A",text: 'Last Updated',width:100,hidden :true,}
			],
                  
            viewConfig : {
            	stripeRows: false,
            }
        });

        me.jobOpeningsGrid = Ext.create('Ext.grid.Panel', {
            title: 'Submitted Jobs',
            frame:true,
            anchor:'98%',
            height:250,
            width:'100%',
            store: ds.candidate_requirementStore,

            columns: [{
            	xtype: 'actioncolumn',
            	width :40,
            	items : [{
            		icon : 'images/icon_edit.gif',
            		tooltip : 'View Requirement',
            		padding: 50,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			me.getRequirmentTab(me.jobOpeningsGrid.store.getAt(rowIndex));
            		}
            	}]
            },
            {xtype: 'gridcolumn',dataIndex: 'hotness',text: 'Hotness',width :  45,
            	renderer:function(value, metadata, record){
            		if (value == 0) {
            			metadata.style = "color:#009AD0;";
            		}
            		var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
            		if(rec){
            			metadata.tdAttr = 'data-qtip="' + rec.data.value+ '"';
            			return rec.data.value;
            		}else{
            			metadata.tdAttr = 'data-qtip="' + value + '"';
            			return value;	
            		}
            	}
            },
            {text : 'Unique Id',dataIndex : 'id',width :  45,renderer: numberRender,},
            {text : 'Job Opening Status',dataIndex : 'jobOpeningStatus',width :  60,renderer: renderValue,},
            {xtype: 'datecolumn',text : 'Posting Date',dataIndex : 'postingDate',width :  60,renderer: dateRender,},
            {xtype: 'gridcolumn',dataIndex: 'postingTitle',text: 'Posting Title',renderer: renderValue,},
            {xtype: 'gridcolumn',dataIndex: 'location',text: 'Location',renderer: renderValue,},
            {header : 'Client',dataIndex : 'clientId',width :  120,
            	renderer:function(value, metadata, record){
            		var rec = ds.clientSearchStore.getById(value);
            		if(rec){
            			metadata.tdAttr = 'data-qtip="' + rec.data.name + '"';
            			return rec.data.name;
            		}else{
            			metadata.tdAttr = 'data-qtip="' + value + '"';
            			return value;
            		}
            	}
            },
            {header : 'Vendor',dataIndex : 'vendorId',width :  120,
            	renderer:function(value, metadata, record){
            		if (value != null) {
            			var ids = value.toString().split(',');
            			var names ="";
            			for ( var i = 0; i < ids.length; i++) {
            				var rec = ds.vendorStore.getById(parseInt(ids[i]));
            				if (rec)
            					names += rec.data.name +", ";	
            			}
            			metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
            			return names.substring(0,names.length-2);
            		}
            	}
            },
            {header : 'Contact Person',dataIndex : 'contactId',width :  120,
            	renderer:function(value, metadata, record){
            		if (value != null) {
            			var ids = value.toString().split(',');
            			var names ="";
            			for ( var i = 0; i < ids.length; i++) {
            				var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
            				if (rec)
            					names += rec.data.fullName +", ";	
            			}
            			metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
            			return names.substring(0,names.length-2);
            		}
            	}
            },
            {text : 'Roles and Responsibilities',dataIndex : 'requirement',width :  150,renderer: requirementRenderer},
            {header : 'Sub Cands',dataIndex : 'candidateId',width :  120,
            	renderer:function(value, metadata, record){
                	if (value != null && value != '') {
    					var ids = value.toString().split(',');
    					var names ="",tooltip='';
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contractorsStore.getById(parseInt(ids[i]));
    						if (rec){
    							names += rec.data.fullName+", ";	
    							tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
    						}
    					}
    					metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
    					return '('+ids.length+') '+ names.substring(0,names.length-2);
					}
                	return 'N/A';
                }
            },
            {xtype: 'gridcolumn',text : 'Submitted Date',dataIndex : 'submittedDate',width :  60,
                renderer:function(value, metadata, record){
                	if (value != null && value != "") {
                		var req_submittedDate = value.split(',');
                		var dates = new Array();
                		for ( var i = 0; i < req_submittedDate.length; i++) {
                			var id_dates = req_submittedDate[i].split('-');
       						var rec = ds.contractorsStore.getById(parseInt(id_dates[0]));
       						if (rec){
       							var letters = rec.data.fullName.match(/\b(\w)/g);
       							var joinName = letters.join('');
       							dates.push(id_dates[1]+'-'+joinName);
       						}
       								
    					}
                		var tooltip = dates.toString();
                		tooltip = tooltip.replace(/,/g, "<br>")
    					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
    					return dates.toString();
					}
                	return value;
                }
			},
            {xtype: 'gridcolumn',dataIndex: 'module',text: 'Module',width :  60,renderer: renderValue,},
            {text : 'Employer',dataIndex : 'employer',width :  40,renderer: employerRenderer,},
            {xtype: 'gridcolumn',dataIndex: 'comments',text: 'Comments',renderer: renderValue,width:150,},
            {text : 'Public Link',dataIndex : 'publicLink',renderer: linkRenderer,width :  80,},
            {text : 'Source',dataIndex : 'source',renderer: renderValue,}
            ],
                  
            viewConfig : {
            	stripeRows: false,
            }
        });
		
        me.showShortlistedCount = new Ext.form.field.Display({
        	fieldLabel: '',
        	name : 'showShortlistedCount'
        });

        me.shortlistedCandidates = Ext.create('Ext.grid.Panel', {
        	title: 'Shortlisted Jobs',
        	cls : 'shortlisted-yellow',
            frame:false,
            anchor:'98%',
            height:250,
            width:'100%',
            store: ds.shortlistedJobsStore,
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Reset',
            		tooltip: 'Reset',
            		tabIndex : 85,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
        					me.loadShortlisted(me.myPanel.form.findField('id').getValue());
        				} 
            	},'->',me.showShortlistedCount]
            }],

            columns: [
                      {
                			xtype : 'actioncolumn',
                			hideable : false,
                			width : 30,
                			items : [{
                				icon : 'images/icon_delete.gif',
                				tooltip : 'Remove',
                				padding : 10,
                				scope : this,
                				handler : function(grid, rowIndex, colIndex) {
                					var record = me.shortlistedCandidates.store.getAt(rowIndex);
               						this.removeShortlistedJob(record.data.id)	
                				} 
                			}],
                      },{
                    	  xtype: 'actioncolumn',
                    	  width :35,
                    	  items : [{
                    		  icon : 'images/icon_edit.gif',
                    		  tooltip : 'Edit Requirement',
                    		  padding: 50,
                    		  scope: this,
                    		  handler : function(grid, rowIndex, colIndex) {
                    			  me.getRequirmentTab(me.shortlistedCandidates.store.getAt(rowIndex));
                    		  }
                    	  }],
                    	  renderer:function(value, metadata, record){
                    		  me.shortlistedCandidates.columns[1].items[0].tooltip = 'Edit job '+record.data.id ;
                    	  }
                      },
                      {dataIndex: 'matchScore',text: 'Match Score',align : 'center',width : 50,
                          renderer:function(value, metadata, record){
                          	metadata.tdAttr = 'data-qtip="' + record.data.matchScoreDetails + '"';
                          	return value;
                          }
                      },
                      {xtype: 'gridcolumn',dataIndex: 'hotness',text: 'Hotness',width :  45,
                          renderer:function(value, metadata, record){
                          	if (value == 0) {
                          		metadata.style = "color:#009AD0;";
          					}
                          	var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
                          	if(rec){
                          		metadata.tdAttr = 'data-qtip="' + rec.data.value.substring(rec.data.value.indexOf('.')+1)+ '"';
                          		return rec.data.value.substring(rec.data.value.indexOf('.')+1);
                          	}else{
                          		metadata.tdAttr = 'data-qtip="' + value + '"';
                          		return value;
                          	}
          	            }
          		    },
          		    {xtype: 'datecolumn',text : 'Posting Date',dataIndex : 'postingDate',width :  60,format: "m/d/Y h:i:s A",},
          		    {text : 'Job Opening Status',dataIndex : 'jobOpeningStatus',width :  60,
              			renderer:function(value, metadata, record){
              				metadata.style = 'background-color:#'+getBackGround(record)+';'
              				if (value == '01.In-progress' && record.data.submittedDate != null && record.data.submittedDate != ""){
              					var count = record.data.submittedDate.split(',').length;
              					metadata.tdAttr = 'data-qtip="' + value.substring(value.indexOf('.')+1)+'<br>'+Ext.String.format('{0} Submission{1} made', count, count !== 1 ? 's' : '')+ '"';
              				}else
              					metadata.tdAttr = 'data-qtip="' + value.substring(value.indexOf('.')+1)+ '"';
                      		return value.substring(value.indexOf('.')+1);
              			}
              		},
              		{xtype: 'gridcolumn',dataIndex: 'postingTitle',text: 'Posting Title',
                          renderer:function(value, metadata, record){
                          	metadata.tdAttr = 'data-qtip="' + value + '"';
                          	metadata.style = 'background-color:#'+getBackGround(record)+';'
                      		return value;
                          }
          		    },
          		    {xtype: 'gridcolumn',dataIndex: 'module',text: 'Module',width :  60,
          		    	renderer:function(value, metadata, record){
          		    		metadata.tdAttr = 'data-qtip="' + value + '"';
          		    		metadata.style = 'background-color:#'+getBackGround(record)+';'
                      		return value;
          		    	}
          		    },
          		    {header : 'Client',dataIndex : 'clientId',width :  120,
          	            renderer:function(value, metadata, record){
          	            	var rec = ds.clientSearchStore.getById(value);
                          	metadata.style = 'background-color:#'+getBackGround(record)+';'
                          	if(rec){
                          		metadata.tdAttr = 'data-qtip="'+rec.data.name+'"';
                          		return rec.data.name;
                          	}else{
                          		metadata.tdAttr = 'data-qtip="' + value + '"';
                          		return value;
                          	}
                          }
          			},
          			{header : 'Vendor',dataIndex : 'vendorId',width :  120,
          	            renderer:function(value, metadata, record){
          					metadata.style = 'background-color:#'+getBackGround(record)+';'
          	            	if (value != null && value != '') {
          						var ids = value.toString().split(',');
          						var names ="";
          						for ( var i = 0; i < ids.length; i++) {
          							var rec = ds.vendorStore.getById(parseInt(ids[i]));
          							if (rec)
          								names += rec.data.name +", ";	
          						}
                          		metadata.tdAttr = 'data-qtip="'+names.substring(0,names.length-2)+'"';
                          		if (ids.length > 1)
                          			return  '('+ids.length+') '+ names.substring(0,names.length-2) ;
                          		return names.substring(0,names.length-2) ;
          					}
          	            	return 'N/A';
                          }
          			},
          			{text : "Alert",dataIndex : 'addInfo_Todos',width :  80,renderer: alertRender,},
          			{xtype: 'gridcolumn',dataIndex: 'rateSubmitted',text: 'Max Bill Rate',renderer: renderValue,},
          			{text : 'Location',dataIndex : 'location',width :  80,renderer: renderValue,},
          			{xtype: 'gridcolumn',dataIndex: 'cityAndState',text: 'City & State',renderer: renderValue,},
          			{text : 'Roles and Responsibilities',dataIndex : 'requirement',width :  150,renderer: requirementRenderer},
          			{xtype: 'gridcolumn',dataIndex: 'skillSet',text: 'Skill Set',width:250,renderer: reqSkillsetRender,},
          			{text : 'Resume',dataIndex : 'resume',width :  60,
                          renderer:function(value, metadata, record){
                          	if (value != null) {
              					metadata.tdAttr = 'data-qtip="' + value + '%"';
              					return value+'%';
          					}
                          	return 'N/A';
                          }

              		},
              		{text : 'Employer',dataIndex : 'employer',width :  40,renderer: employerRenderer,},
              		{text : 'Communication Required',dataIndex : 'communication',width :  80,renderer: renderValue,},
              		{header : 'Contact Person',dataIndex : 'contactId',width :  120,
          	            renderer:function(value, metadata, record){
          	            	if (value != null) {
              					var ids = value.toString().split(',');
              					var names ="";
              					for ( var i = 0; i < ids.length; i++) {
              						var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
              						if (rec)
              							names += rec.data.fullName +", ";	
              					}
              					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
              					return names.substring(0,names.length-2);
                          	}
                          }
          			}
                  ],
        });

		me.submissionStatusCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Status',
		    store: ds.submissionStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'submissionStatus',
		    tabIndex:90,
		    labelWidth :40,
		    width :140,
		    //value : ['Submitted','Withdrawn','I-Scheduled','I-Succeeded','I-Failed','C-Accepted','C-Declined'],
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
		});

        me.submissionCombo = Ext.create('Ext.form.ComboBox', {
        	matchFieldWidth: false,
            fieldLabel: 'Submitted Date',
            store: ds.monthStore,
            name : 'submissionCombo',
            queryMode: 'local',
            displayField: 'label',
            valueField: 'value',
            labelAlign: 'right',
            tabIndex :91,
            labelWidth :80,
            width :180,
            value : 'One Week',
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
        });

        me.showSubmittedCount = new Ext.form.field.Display({
        	fieldLabel: '',
        	name : 'showSubmittedCount'
        });

        me.submittedCandidates = Ext.create('Ext.grid.Panel', {
        	title: 'Submitted Jobs',
        	cls : 'submitted-green',
            frame:false,
            anchor:'98%',
            height:250,
            width:'100%',
            store: ds.subRequirementStore,
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [me.submissionStatusCombo,me.submissionCombo,
	        	        {
	        				xtype: 'button',
	        				text: 'Search',
	        				tooltip: 'Search',
	        				tabIndex :92,				
	        				iconCls : 'btn-Search',
	        				scope: this,
	        				handler : function(grid, rowIndex, colIndex) {
	        					me.loadSubRequirments(me.myPanel.form.findField('id').getValue());
	        				} 
	        	        },'-',{
	        	        	xtype: 'button',
	        	        	text: 'Reset',
	        	        	tooltip: 'Reset',
	        				tabIndex :93,
	        	        	scope: this,
	        	        	handler : function(grid, rowIndex, colIndex) {
	        	        		me.submissionCombo.reset();
	        	        		me.submissionStatusCombo.reset();
	        	        		me.loadSubRequirments(me.myPanel.form.findField('id').getValue());
	        	        	} 
	        	        },'->',me.showSubmittedCount]
            }],

            columns: [
                  {
                  	xtype: 'actioncolumn',
                	width :40,
                	items : [{
                		icon : 'images/icon_edit.gif',
                		tooltip : 'View Requirement',
                		padding: 50,
                		scope: this,
                		handler : function(grid, rowIndex, colIndex) {
                			me.editRequirement(me.submittedCandidates.store.getAt(rowIndex).data.requirementId);
                		}
                	}],
              	  renderer:function(value, metadata, record){
            		  me.submittedCandidates.columns[0].items[0].tooltip = 'Edit job '+record.data.requirementId ;
            	  }
                },{xtype: 'gridcolumn',dataIndex: 'hotness',text: 'Hotness',width :  45,
                	  renderer:function(value, metadata, record){
                		  if (Number(value) == 0) {
                			  metadata.style = "color:#009AD0;";
                		  }
                		  var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
                		  if(rec){
                			  metadata.tdAttr = 'data-qtip="' + rec.data.value.substring(rec.data.value.indexOf('.')+1)+ '"';
                			  return rec.data.value.substring(rec.data.value.indexOf('.')+1);
                		  }else{
                			  metadata.tdAttr = 'data-qtip="' + value + '"';
                			  return value;
                		  }
                	  }
                  },
                  {xtype: 'datecolumn',text : 'Submitted Date',dataIndex : 'submittedDate',width :  60,format: "m/d/Y",},
                  {header : 'Resume Link', dataIndex : 'resumeLink', renderer: linkRenderer, width :  50, },
                  {header : 'Submission Status',dataIndex : 'submissionStatus',width :  70,
                	  renderer:function(value, metadata, record){
                		  metadata.tdAttr = 'data-qtip="' + value + '"';
                		  metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
                		  return value;
                	  }
                  },
                  {header : 'Vendor',dataIndex : 'vendor',width :  80,
                	  renderer:function(value, metadata, record){
                		  metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
                		  if (value != ''){
                			  metadata.tdAttr = 'data-qtip="'+value+'"';
                			  return value;
                		  }
                	  }
                  },
                  {xtype: 'gridcolumn',dataIndex: 'reqCityAndState',text: 'City & State',
                	  renderer:function(value, metadata, record){
                		  metadata.tdAttr = 'data-qtip="' + value + '"';
                		  metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
                		  return value;
                	  }
                  },
                  {header : 'Client',dataIndex : 'client',width :  80,
                	  renderer:function(value, metadata, record){
                		  metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
                		  if (value != ''){
                			  metadata.tdAttr = 'data-qtip="'+value+'"';
                			  return value;
                		  }
                	  }
                  },
                  {text : "Alert",dataIndex : 'addInfoTodos',width :  70,
                	  renderer:function(value, metadata, record){
                		  metadata.tdAttr = 'data-qtip="' + value + '"';
                		  metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
                		  if (value == null || value=="") 
                			  return 'N/A' ;
                		  return '<span style="color:red;">' + value + '</span>';
                	  }
                  },
                  {xtype: 'gridcolumn',dataIndex: 'postingTitle',text: 'Posting Title',
                	  renderer:function(value, metadata, record){
                		  metadata.tdAttr = 'data-qtip="' + value + '"';
                		  return value;
                	  }
                  },
                  {xtype: 'gridcolumn',dataIndex: 'module',text: 'Module',
                	  renderer:function(value, metadata, record){
                		  metadata.tdAttr = 'data-qtip="' + value + '"';
                		  return value;
                	  }
                  },
                  {xtype: 'datecolumn',text : 'Posting Date',dataIndex : 'postingDate',width :  60,format: "m/d/Y H:i:s A",},
                  {header : 'Submitted Rate-1',dataIndex : 'submittedRate1',},
                  {header : 'Submitted Rate-2',dataIndex : 'submittedRate2',width :  60,},
                  {dataIndex: 'candidateExpectedRate',text: 'Expected Rate',width :  50,},
                  {dataIndex: 'statusUpdated',text: 'Status Updated Date',width :  60,xtype: 'datecolumn',format:'m/d/Y',}
                  ]
        });

        me.myPanel = Ext.create('Ext.form.Panel', {
        	bodyPadding: 0,
        	border : 0,
        	frame:false,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
        	items:[{
				xtype:'fieldset',
				border : 0,
				collapsible: false,
				layout: 'column',
				items :[{
					xtype: 'container',
					columnWidth:.5,
					items :[{
		        		xtype : 'fieldset',
						title : 'Candidate Information',
						collapsible : true,
						defaultType : 'textfield',
						layout : 'column',
						items : [{
							xtype: 'container',
							columnWidth:1,
							items: [{
								xtype:'displayfield',
								fieldLabel: 'Unique Id',
								readOnly :true,	
								name: 'id',
								tabIndex:1,
							},{
								xtype: 'container',
								layout: {
							        type: 'table',
							        columns: 2,
							        tableAttrs: {
							            style: {
							                width: '100%',
							                labelWidth : 100
							            }
							        }
							    },
								items: [{
									xtype:'textfield',
									fieldLabel: 'First Name *',
									name: 'firstName',
									allowBlank:false,
									maxLength:45,
									enforceMaxLength:true,
									tabIndex:2,
								},{
									xtype:'textfield',
									fieldLabel: 'Last Name *',
									name: 'lastName',
									allowBlank:false,
									tabIndex:3,
									maxLength:45,
									enforceMaxLength:true,
								},{
									xtype:'textfield',
									fieldLabel: 'Email *',
									name: 'emailId',
									allowBlank:false,
									vtype:'email',
									tabIndex:4,
									maxLength:45,
									enforceMaxLength:true,
									listeners:{
										change: function(combo,value){
											me.emailUpdated = true;
										}
									}
								},{
									xtype:'textfield',
									fieldLabel: 'Contact Number',
									name: 'contactNumber',
									maskRe :  /[0-9]/,
									tabIndex:5,
									maxLength:12,
									enforceMaxLength:true,
									listeners:{
										change : function(field,newValue,oldValue){
											var value = me.form.findField('contactNumber').getValue();
											if (value != "" ) {
												value = value.replace(/[^0-9]/g, "");
												value = value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
												me.form.findField('contactNumber').setValue(value);
											}
											me.phoneUpdated = true;
										}
									}
								},{
									xtype:'textfield',
									fieldLabel: "City & State",
									name: 'cityAndState',
									tabIndex:6,
									maxLength:90,
									enforceMaxLength:true,
								},
								me.locationCombo,
								me.immigrationCombo,
								{
									xtype: 'radiogroup',
									//width: 400,
									fieldLabel: 'Gender',
									name: 'gender',
									columns: 2,
									value : false,
									vertical: true,
									items: [{ 
										xtype: 'radiofield', 
										boxLabel: 'Male', 
										name: 'gender', 
										inputValue: 'Male',
										width: 60,
										tabIndex:9,
									},{ 
										xtype: 'radiofield', 
										boxLabel: 'Female', 
										name: 'gender', 
										inputValue: 'Female',
										width: 60,
										tabIndex:10,
									}],
								},{
									xtype: 'radiogroup',
									//width: 400,
									fieldLabel: 'Immigration Verified',
									name: 'immigrationVerified',
									columns: 3,
									value : false,
									vertical: true,
									items: [{ 
										xtype: 'radiofield', 
										boxLabel: 'YES', 
										name: 'immigrationVerified', 
										inputValue: 'YES',
										width: 50,
										tabIndex:11,
									},{ 
										xtype: 'radiofield', 
										boxLabel: 'NO', 
										name: 'immigrationVerified', 
										inputValue: 'NO',
										width: 50,
										tabIndex:12,
									},{ 
										xtype: 'radiofield', 
										boxLabel: 'Not Needed', 
										name: 'immigrationVerified', 
										inputValue: 'Not Needed',
										width: 100,
										tabIndex:13,
									}],
								},{
									xtype:'textarea',
									fieldLabel: 'Contact Address',
									name: 'contactAddress',
									tabIndex:14,
									height : 50,
									width : 320,
									maxLength:200,
									enforceMaxLength:true,
								}]
							},{
								xtype: 'container',
								layout :'table',
								padding : '5 0 5 120',
								columns: 2,
								items: [me.relocateCheckbox,me.travelCheckbox,me.goodCandidateCheckbox]
							},{
								xtype: 'container',
								layout: {
							        type: 'table',
							        columns: 2,
							        tableAttrs: {
							            style: {
							                width: '100%',
							            }
							        }
							    },
								items: [{
			   	             		xtype: 'radiogroup',
			   	             		fieldLabel: 'Comments',
			   	             		//labelAlign : 'top',
			   	             		//width : 180,
			   	             		name: 'commentsCheckbox',
			   	             		columns: 1,
			   	             		value : false,
			   	             		vertical: true,
			   	             		items: [{ 
			   	             			xtype: 'radiofield',
			   	             			boxLabel: 'P1', 
			   	             			name: 'commentsCheckbox', 
			   	             			inputValue: 'p1',
			   	             			tabIndex:18,
			   	             		},{ 
			   	             			xtype: 'radiofield',
			   	             			boxLabel: 'P2', 
			   	             			name: 'commentsCheckbox', 
			   	             			inputValue: 'p2',
			   	             			tabIndex:19,
			   	             		}],
			   	             	},{
									xtype:'textarea',
									fieldLabel: '',
									name: 'comments',
									tabIndex:20,
			   	             		width : 450,
			   	             		height : 60,
									maxLength:200,
									enforceMaxLength:true,
								}]
							},{
								xtype: 'container',
								layout : 'fit',
								items: [{
			   	             		xtype:'textarea',
			   	             		fieldLabel: "P1 Comments",
			   	             		//labelAlign : 'top',
			   	             		name: 'p1comments',
			   	             		readOnly : true,
			   	             		width : 500,
			   	             		height : 100,
			   	             	},{
			   	             		xtype:'textarea',
			   	             		fieldLabel: "P2 Comments",
			   	             		//labelAlign : 'top',
			   	             		name: 'p2comments',
			   	             		readOnly : true,
			   	             		width : 500,
			   	             		height : 100,
			   	             	}]
							}]
						}]
		        	},{
		        		xtype : 'fieldset',
						title : 'Professional Information',
						bodyPadding: 5, 
						collapsible : true,
						defaultType : 'textfield',
						layout : 'column',
						items : [{
							xtype: 'container',
							columnWidth:1,
							items: [{
								xtype: 'container',
								layout: {
							        type: 'table',
							        columns: 2,
							        tableAttrs: {
							            style: {
							                width: '100%',
							                labelWidth : 100
							            }
							        }
							    },
								items: [{
			   	                 	xtype:'numberfield',
			   	                 	allowDecimals: false,
			   	                 	fieldLabel: 'Confidentiality',
			   	                 	name: 'confidentiality',
			   	                 	tabIndex:21,
			   	                 	minValue: 1,
			   	                 	maxValue: 9,
			   	                 	listeners: {
			   	                 		afterrender: function() {
			   	                 			if (app.userRole == 'ADMIN')
			   	                 				this.show();
			   	                 			else
			   	                 				this.hide();
			   	                 		}
			   	                 	}
			   	             	},{
			   	             		xtype:'numberfield',
			   	             		fieldLabel: 'Priority',
			   	             		name: 'priority',
			   	             		allowDecimals: false,
			   	             		tabIndex:22,
			   	             		value :0,
			   	             		minValue: 0,
			   	             		maxValue: 100
			   	             	},{
			   	                 	xtype:'textfield',
			   	                 	fieldLabel: "Current Job Title",
			   	                 	name: 'currentJobTitle',
			   	                 	tabIndex:23,
			   	                 	maxLength:45,
			   	                 	enforceMaxLength:true,
			   	             	},{
			   	             		xtype:'textfield',
			   	             		fieldLabel: "Employer",
			   	             		name: 'employer',
			   	             		tabIndex:24,
			   	             		maxLength:45,
			   	             		enforceMaxLength:true,
			   	             	},{
			   	             		xtype:'datefield',
			   	             		fieldLabel: 'Availability',
			   	             		name: 'availability',
			   	             		tabIndex:25,
			   	             	},
			   	             	me.experienceCombo,
			   	             	{
			   	             		xtype:'datefield',
			   	             		fieldLabel: 'Start Date',
			   	             		name: 'startDate',
			   	             		tabIndex:27,
			   	             	},
			   	             	me.sourceCombo,me.typeCombo,me.qualificationCombo,me.followUpCombo,me.marketingStatusCombo,me.openToCTHCombo,
			   	             	{
			   	             		xtype:'textfield',
			   	             		fieldLabel: "Education",
			   	             		name: 'education',
			   	             		tabIndex:34,
			   	                 	maxLength:150,
			   	                 	enforceMaxLength:true,
			   	             	},
			   	             	me.communicationCombo,
			   	             	{
			   	             		xtype:'textfield',
			   	             		fieldLabel: "Expected Rate",
			   	             		name: 'expectedRate',
			   	             		tabIndex:36,
			   	             		maxLength:100,
			   	             		enforceMaxLength:true,
			   	             	},{
			   	             		xtype:'textfield',
			   	             		fieldLabel: "Referral/More info",
			   	             		name: 'referral',
			   	             		tabIndex:37,
			   	                 	maxLength:45,
			   	                 	enforceMaxLength:true,
			   	             	},{
									xtype: 'container',
									layout :'table',
									items: [{
				   	                 	xtype:'numberfield',
				   	                 	fieldLabel: 'Rate Range From',
				   	                 	tabIndex:38,
				   	                 	width : 250,
				   	                 	minValue : 0,
				   	                 	name: 'rateFrom',
				   	             	},{
				   	                 	xtype:'numberfield',
				   	                 	labelWidth : 40,
				   	                 	width : 150,
				   	                 	minValue : 0,
				   	                 	fieldLabel: 'To',
				   	                 	tabIndex:39,
				   	                 	name: 'rateTo',
										validator: function (value) {
											if (value > 0 && value < me.form.findField('rateFrom').getValue())
												return 'Rate range to should be greater than range from';
											else
												return true;
								        }
				   	             	}]
			   	             	},{
			   	             		xtype:'textfield',
			   	             		fieldLabel: "Alert",
			   	             		name: 'alert',
			   	             		tabIndex:40,
			   	                 	maxLength:45,
			   	                 	enforceMaxLength:true,
			   	                 	fieldStyle: "color: red;",
			   	             	},
			   	             	me.personalityCombo,me.employmentTypeCombo,
			   	             	{
			   	             		xtype:'textarea',
			   	             		fieldLabel: "Selected Employment Type",
			   	             		name: 'selectedEmploymentType',
			   	             		readOnly : true,
			   	             		width : 400,
			   	             		height : 40,
			   	             	},
			   	             	me.aboutPartnerCombo,
						        {
									xtype: 'container',
									layout :'table',
									items: [{
				   	             		xtype:'textfield',
				   	             		fieldLabel: "Resume Link",
				   	             		name: 'resumeLink',
				   	             		tabIndex:44,
				   	             		maxLength:250,
				   	             		enforceMaxLength:true,
				   	             	},{
				   	            		 xtype: 'box',
				   	            		 tabIndex:45,
				   	            		 padding : '0 0 0 10%',
				   	            		 name: 'resumeLinkHyperlink',
				   	            		 align : 'right',
				   	            		 autoEl: {
				   	            			 html: '<a href="#">'+'<u>Go</u>'+'</a>'
				   	            		 },
				   	            		 listeners: {
				   	            			 render: function(c){
				   	            				 c.getEl().on('click', function(){
				   	            	            	var url = me.myPanel.form.findField('resumeLink').getValue();
				   	            	            	if (url == null || url == '') {
				   	            	            		me.updateError('Link not exist');
				   	            	            		return false;
				   	            					}
				   	            	            	window.open(url,"_blank");
				   	            	            }, c, {stopEvent: true});
				   	            			 }
				   	            		 }	
		   	            	         }]
			   	             	},{
			   	             		xtype:'datefield',
			   	             		fieldLabel: 'Initial Contact Date',
			   	             		name: 'contactDate',
			   	             		tabIndex:46,
			   	             	},{
			   	             		xtype:'numberfield',
			   	             	allowDecimals: false,
			   	             		fieldLabel: 'Technical Score',
			   	             		name: 'technicalScore',
			   	             		tabIndex:47,
			   	             		minValue: 0,
			   	             		maxValue: 10
			   	             	},{
			   	             		xtype:'textfield',
			   	             		fieldLabel: "Billing Range",
			   	             		name: 'billingRange',
			   	             		tabIndex:48,
			   	             		maxLength:45,
			   	             		enforceMaxLength:true,
			   	             	},{
		   	             		xtype : 'datefield',
		   	             		name : 'created',
		   	             		hidden : true
		   	             	},{
		   	             		xtype : 'numberfield',
		   	             		name : 'employeeId',
		   	             		hidden : true
		   	             	},{
		   	            	 xtype:'numberfield',
		   	            	allowDecimals: false,
		   	            	 hidden : true ,
		   	            	 name: 'version',
		   	            	 value: 0
		   	             }]
						}]
						}]
		        	}]
				},{
					xtype: 'container',
					padding : '0 0 0 10',
					columnWidth:.5,
					items: [me.suggestionsGrid,
					        {
								border : false,
   	            	 			height: 5,
					        },
					        me.criteriaPanel,
					        {
								border : false,
   	            	 			height: 5,
					        },
					        me.shortlistedCandidates,
					        {
					        	border : false,
	   	            	 		height: 5,
           		         	},
           		         	me.submittedCandidates
           		         	]
				}]
        	},{
        		xtype : 'fieldset',
        		title : 'Skill Set & Roles',
				collapsible : true,
				defaultType : 'textfield',
				layout : 'column',
				items : [{
					xtype: 'container',
					columnWidth:1,
					items: [
   	             	{
	   	    	    	xtype: 'container',
	   	    	    	layout :'table',
	   	    	    	items: [me.skillSetGroupCombo,
	   	    	             	{
	   	             		xtype: 'textarea',
	   	             		fieldLabel: 'Selected Skill Set Group',
	   	             		name: 'skillSetGroup',
	   	             		readOnly : true,
	   	             		width : 500,
	   	             		height : 60,
	   	             	},{
	   	             		xtype: 'textfield',
	   	             		fieldLabel: 'Skillset Group Ids',
	   	             		name: 'skillSetGroupIds',
	   	             		readOnly : true,
	   	             		hidden : true,
	   	             	},{
	   	             		margin : '0 0 0 10',
		   	            	xtype: 'button',
			   	            iconCls : 'btn-delete',
			   	            tooltip : 'Clear skill set group',
			   	            tabIndex : 49,
	            	        handler: function() {
	            	        	me.form.findField('skillSetGroup').reset();
	            	        	me.form.findField('skillSetGroupIds').reset();
							}
		   	            }]
	   	    	    },{
	   	    	    	xtype: 'container',
	   	    	    	layout :'table',
	   	    	    	items: [me.skillSetCombo,
	   	    	             	{
	   	             		xtype: 'textarea',
	   	             		fieldLabel: 'Selected Skill Set',
	   	             		name: 'skillSet',
	   	             		readOnly : true,
	   	             		width : 500,
	   	             		height : 60,
	   	             		maxLength:2000,
	   	             	},{
	   	             		xtype: 'textfield',
	   	             		fieldLabel: 'Skillset Ids',
	   	             		name: 'skillSetIds',
	   	             		readOnly : true,
	   	             		hidden : true,
	   	             	},{
	   	             		margin : '0 0 0 10',
		   	            	xtype: 'button',
			   	            iconCls : 'btn-delete',
			   	            tooltip : 'Clear skill set',
			   	            tabIndex : 49,
	            	        handler: function() {
	            	        	me.form.findField('skillSet').reset();
	            	        	me.form.findField('skillSetIds').reset();
							}
		   	            }]
	   	    	    },{
	   	    	    	xtype: 'container',
	   	    	    	layout :'table',
	   	    	    	items: [me.roleSetCombo,
	   	 	             	{
		             		xtype: 'textarea',
		             		fieldLabel: 'Selected Role Set',
		             		name: 'roleSet',
		             		readOnly : true,
		             		width : 500,
		             		height : 50,
		             		maxLength:1000,
		             	},{
	   	             		xtype: 'textfield',
	   	             		fieldLabel: 'Roleset Ids',
	   	             		name: 'roleSetIds',
	   	             		readOnly : true,
	   	             		hidden : true,
	   	             	},{
	   	             		margin : '0 0 0 10',
		   	            	xtype: 'button',
			   	            iconCls : 'btn-delete',
			   	            tooltip : 'Clear Role Set',
			   	            tabIndex : 51,
	            	        handler: function() {
	            	        	me.form.findField('roleSet').reset();
	            	        	me.form.findField('roleSetIds').reset();
							}
		   	            }]
	   	    	    },{
	   	    	    	xtype: 'container',
	   	    	    	layout :'table',
	   	    	    	items: [me.targetSkillSetCombo,
	   	    	             	{
	   	             		xtype: 'textarea',
	   	             		fieldLabel: 'Selected Target Skill Set',
	   	             		name: 'targetSkillSet',
	   	             		readOnly : true,
	   	             		width : 500,
	   	             		height : 60,
	   	             		maxLength:1000,
	   	             	},{
	   	             		xtype: 'textfield',
	   	             		fieldLabel: 'Target Skillset Ids',
	   	             		name: 'targetSkillSetIds',
	   	             		readOnly : true,
	   	             		hidden : true,
	   	             	},{
	   	             		margin : '0 0 0 10',
		   	            	xtype: 'button',
			   	            iconCls : 'btn-delete',
			   	            tooltip : 'Clear Target Skill Set',
			   	            tabIndex : 53,
	            	        handler: function() {
	            	        	me.form.findField('targetSkillSet').reset();
	            	        	me.form.findField('targetSkillSetIds').reset();
							}
		   	            }]
	   	    	    },{
	   	    	    	xtype: 'container',
	   	    	    	layout :'table',
	   	    	    	items: [me.targetRolesCombo,
	   	 	             	{
		             		xtype: 'textarea',
		             		fieldLabel: 'Selected Target Roles',
		             		name: 'targetRoles',
		             		readOnly : true,
		             		width : 500,
		             		height : 50,
		             		maxLength:1000,
		             	},{
	   	             		margin : '0 0 0 10',
		   	            	xtype: 'button',
			   	            iconCls : 'btn-delete',
			   	            tooltip : 'Clear Target Roles',
			   	            tabIndex : 55,
	            	        handler: function() {
	            	        	me.form.findField('targetRoles').reset();
							}
		   	            }]
	   	    	    },{
	   	    	    	xtype: 'container',
	   	    	    	layout :'table',
	   	    	    	items: [me.resumeHelpCombo,
	   	    	             	{
		             		xtype: 'textarea',
		             		fieldLabel: 'Comments',
		             		name: 'resumeHelpComments',
		             		hidden : true,
		             		width : 500,
		             		tabIndex:57,
		             		height : 30,
	   	             		maxLength:100,
	   	             		enforceMaxLength:true,
		             	}]
	   	    	    },{
	   	    	    	xtype: 'container',
	   	    	    	layout :'table',
	   	    	    	items: [me.interviewHelpCombo,
	   	 	             	{
		             		xtype: 'textarea',
		             		fieldLabel: 'Comments',
		             		name: 'interviewHelpComments',
		             		hidden : true,
		             		width : 500,
		             		tabIndex:59,
		             		height : 30,
	   	             		maxLength:100,
	   	             		enforceMaxLength:true,
		             	}]
	   	    	    },{
	   	    	    	xtype: 'container',
	   	    	    	layout :'table',
	   	    	    	items: [me.jobHelpCombo,
	   	 	             	{
		             		xtype: 'textarea',
		             		fieldLabel: 'Comments',
		             		name: 'jobHelpComments',
		             		hidden : true,
		             		width : 500,
		             		tabIndex:61,
		             		height : 30,
	   	             		maxLength:100,
	   	             		enforceMaxLength:true,
		             	}]
	   	    	    },{
	   	    	    	xtype: 'container',
	   	    	    	layout :'table',
	   	    	    	items: [{
	   	             		xtype:'textarea',
	   	             		fieldLabel: "Other Skill Set",
	   	             		name: 'otherSkillSet',
	   	             		tabIndex:62,
	   	             		maxLength:500,
	   	             		enforceMaxLength:true,
	   	             		width : 500,
	   	             		height : 50,
	   	             	},{
	   	             		xtype:'textarea',
	   	             		fieldLabel: "Old Skill Set",
	   	             		name: 'oldSkillSet',
	   	             		hidden : true,
	   	             		maxLength:2000,
	   	             		enforceMaxLength:true,
	   	             		readOnly : true,
	   	             		width : 500,
	   	             		height : 50,
	   	             	},{
	   	             		xtype:'textarea',
	   	             		name: 'currentRoles',
	   	             		hidden : true,
	   	             	}]
	   	    	    }]
				}]
        	}]
        });

        
		me.items = [ me.getMessageComp(), me.getHeadingComp(), 
		             {
						xtype: 'container',
						columnWidth:1,
						items: [me.myPanel]
		             },{
		            	 xtype: 'container',
		            	 items:[{
		            		 xtype : 'fieldset',
		            		 title : 'Certifications',
		            		 id : 'Candidate_Certifications_Fieldset',
		            		 collapsible : true,
		            		 layout : 'column',
		            		 items : [{
		            			 xtype: 'container',
		            			 columnWidth:1,
		            			 items: [me.certificationGridPanel]
		            		 }]
		            	 },{
		            		 xtype : 'fieldset',
		            		 title : 'Job Openings',
		            		 id : 'Candidate_Jobopenings_Fieldset',
		            		 collapsible : true,
		            		 layout : 'column',
		            		 items : [{
		            			 xtype: 'container',
		            			 columnWidth:1,
		            			 items: [me.jobOpeningsGrid]
		            		 }]
		            	 },{
		            		 xtype : 'fieldset',
		            		 title : 'Projects',
		            		 id : 'Candidate_Projects_Fieldset',
		            		 collapsible : true,
		            		 layout : 'column',
		            		 items : [{
		            			 xtype: 'container',
		            			 columnWidth:1,
		            			 items: [me.projectGridPanel]
		            		 }]
		            	 }]
		             }];
		
        me.saveButton = new Ext.Button({
			text : 'Save',
			tooltip : 'Save',
			iconCls : 'btn-save',
			tabIndex:63,
			scope: this,
	        handler: this.saveCandidate
        });

        me.saveAndCloseButton = new Ext.Button({
			text : 'Save & Close',
			tooltip : 'Save & Close',
			iconCls : 'btn-save',
			tabIndex:64,
			scope: this,
	        handler: function() {
	        	this.closeButtonClicked = true;
	        	this.saveCandidate();	
			}
        });

        me.deleteButton = new Ext.Button({
			text : 'Delete',
			tooltip : 'Delete Candidate',
			iconCls : 'btn-delete',
			tabIndex:65,
			scope: this,
	        handler: this.deleteConfirm
        });

        me.onBoardingButton = new Ext.Button({
			text : 'Onboarding',
			tooltip : 'Onboarding into AJ',
			iconCls : 'btn-payment',
			tabIndex:66,
			scope: this,
	        handler: this.onboardingConfirm
        });

        me.shareButton = new Ext.Button({
            text: 'Share',
            tooltip: 'Share form',
            icon : 'images/icon_share.png',
            tabIndex:67,
            scope : this,
            handler: this.shareCandidate
        });

        me.refreshButton = new Ext.Button({
        	text : 'Refresh',
        	tooltip : 'Refresh form',
        	icon : 'images/arrow_refresh.png',
			tabIndex:68,
			scope: this,
	        handler: function() {
	        	app.candidateMgmtPanel.loadStores();
            	var id = me.myPanel.form.findField('id').getValue();
            	me.myPanel.form.reset();
            	me.certificationGridPanel.store.removeAll();
            	me.projectGridPanel.store.removeAll();
            	me.getCandidate(id);
			}
        });

        me.makeSMEButton = new Ext.Button({
        	text : 'Make SME/Contact/Reference',
        	tooltip : 'Make SME/Contact/Reference',
        	icon : 'images/icon_lookup.gif',
			tabIndex:69,
			scope: this,
	        handler: function() {
	        	this.makeSME();
			}
        });

        me.viewSMEButton = new Ext.Button({
        	text : 'View SME/Contact/Reference',
        	tooltip : 'View SME/Contact/Reference',
        	icon : 'images/icon_edit.gif',
			tabIndex:69,
			scope: this,
			handler: this.viewContact
        });

        me.closeButton = new Ext.Button({
			text : 'Close',
			tooltip : 'Close candidate',
			iconCls : 'btn-close',
			tabIndex:71,
			scope: this,
	        handler: this.checkForDirty
        });

		me.tbar =  [ this.saveButton,'-',this.saveAndCloseButton,'-',this.deleteButton,'-',this.onBoardingButton,'-',this.shareButton,'-',me.refreshButton,'-',
		             this.makeSMEButton,this.viewSMEButton,'-',this.closeButton ];

		this.callParent(arguments);
	},
	
	checkForDirty : function() {
		if (this.candidate =="")
			this.closeFormDirty();
		this.closeButtonClicked = true;
		var formDirty = false;
		var itemslength = this.myPanel.form.getFields().getCount();
		var i = 0;
		while (i < itemslength) {
			var fieldName = this.myPanel.form.getFields().getAt(i).name;
			value = this.myPanel.form.findField(fieldName).getValue();
			
			if (fieldName == 'created' || fieldName == 'selectedEmploymentType' || fieldName == 'immigrationVerified' || fieldName == 'commentsCheckbox' 
				|| fieldName == 'selectedTargetRoles' || fieldName == 'selectedTargetSkillSet' || fieldName == 'selectedRoleset' || fieldName == 'selectedSkillSet'
				|| fieldName.search('Criteria') != -1 || fieldName == 'showSuggestedCount' || fieldName == 'showShortlistedCount' || fieldName == 'showSubmittedCount' 
				|| fieldName == 'submissionCombo' || fieldName == 'submissionStatus' || fieldName == 'showSuggestedCount' || fieldName == 'gender' || fieldName =='skillSet'
				|| fieldName =='skillSetIds' || fieldName =='targetSkillSetIds' || fieldName =='targetSkillSet' || fieldName =='resumeLinkHyperlink' 
				|| fieldName =='roleSetIds' || fieldName =='roleSet' || fieldName =='skillSetGroupIds' || fieldName =='skillSetGroup') {
			}else{
				if (value == null || this.candidate.data[fieldName] == null) {
					if (value == null && this.candidate.data[fieldName] != null && this.candidate.data[fieldName] == "") {
						//if value is null and previous value is not null previous value must be empty string
					}else if (this.candidate.data[fieldName] == null && value != null && value == "") {
						//if previous value is null and value is not null then value must be empty string
					}else if (value == null && this.candidate.data[fieldName] == null) {
						// if both are null no issue
					}else{
						formDirty = true;
					}
				}else if (fieldName == 'availability' || fieldName == 'startDate' || fieldName == 'submittedDate' ) {
					if (Ext.Date.format(new Date(this.candidate.data[fieldName]),'m/d/y') != Ext.Date.format(value,'m/d/y'))
						formDirty = true;
				}else if (this.candidate.data[fieldName].toString() != value.toString()) {
					formDirty = true;
				}
			}
			
			i = i+1;
		}

    	var records = new Array();
    	for ( var i = 0; i < this.certificationGridPanel.store.data.length ; i++) {
			var record = this.certificationGridPanel.store.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.certificationGridPanel.modifiedIds.indexOf(record.data.id) != -1) {
				records.push(record.data);
			}
		}
    	if (records.length >0){
			Ext.Msg.alert('Warning','Please save or reset Certifications before closing.');
			return false;
		}
		
		if (formDirty) {
			Ext.MessageBox.show({
				title:'Confirm',
	            msg: "Do you want to save Candidate changes?",
	            buttonText: {                        
	                yes: 'Save and Close',
	                no: 'Close without Saving'
	            },
	            icon: Ext.MessageBox.INFO,
	            fn: function(btn) {
	            	if (btn == 'yes') {
	            		if(app.candidatePanel != null)
	            			app.candidatePanel.saveCandidate();
	            		else
	            			app.candidateMgmtPanel.candidatePanel.saveCandidate();
	            	}else if (btn == 'no') {
	            		if(app.candidatePanel != null)
	            			app.candidatePanel.closeFormDirty();
	            		else
	            			app.candidateMgmtPanel.candidatePanel.closeFormDirty();
					}
	            }
	        });
		}else
			this.closeFormDirty();
	},
	
	closeFormDirty: function(){
		if (app.candidatePanel != null) 
			this.openedFrom = app.candidatePanel.openedFrom;
		
		if (this.openedFrom == "Recruit" || this.openedFrom == "Job Opening") {
			this.openedFrom ='';
			app.setActiveTab(0,'li_requirements');
			app.requirementMgmtPanel.requirementDetailPanel.loadFromCandidates();
			app.candidateMgmtPanel.getLayout().setActiveItem(0);
			
			if(this.modified){
				var params = new Array();
		    	var filter = getFilter(params);
		    	filter.pageSize=1000;
		    	app.loadMask.show();
		    	ds.candidateSearchStore.loadByCriteria(filter);
		        ds.candidateSearchStore.on('load', function(store, records, options) {
		        	app.loadMask.hide();
		        	app.requirementMgmtPanel.requirementDetailPanel.loadCandidatesStore();
		       	}, this);
			}
		}else if (this.openedFrom == "Home" ) {
			app.setActiveTab(0,'li_home');
			app.candidateMgmtPanel.getLayout().setActiveItem(0);
		}else {
			if(app.candidateMgmtPanel == null){
				app.setActiveTab(0,'li_candidates');
			}
			if(this.modified){
				app.candidateMgmtPanel.getLayout().setActiveItem(0);
				app.candidateMgmtPanel.candidatesSearchPanel.search();
			}else
				app.candidateMgmtPanel.getLayout().setActiveItem(0);
		}
		this.openedFrom ='';
	},
	
    loadForm : function(record) {
    	this.modified = false;
    	this.closeButtonClicked = false;
    	this.clearMessage();
    	this.employmentTypeCombo.store.clearFilter();
    	this.targetRolesCombo.store.clearFilter();
    	this.targetSkillSetCombo.store.clearFilter();
    	this.skillSetCombo.store.clearFilter();
    	this.roleSetCombo.store.clearFilter();
    	this.suggestionsGrid.store.removeAll();
    	this.shortlistedCandidates.store.removeAll();
    	this.submittedCandidates.store.removeAll();
		this.certificationGridPanel.store.removeAll();
		this.jobOpeningsGrid.store.removeAll();
		this.projectGridPanel.store.removeAll();

    	this.myPanel.form.reset();
		this.myPanel.loadRecord(record);
		if (record.data.employmentType != null)
			this.employmentTypeCombo.setValue(record.data.employmentType.split(','));
		else
			this.employmentTypeCombo.setValue(record.data.employmentType);
		
		this.myPanel.form.findField('immigrationVerified').setValue({immigrationVerified:record.data.immigrationVerified});
		this.myPanel.form.findField('gender').setValue({gender:record.data.gender});
		if (record.data.skillSetIds != null && record.data.skillSetIds != '') {
			var skillSetIds = record.data.skillSetIds.split(',');
			var skillSet ='';
			for ( var i = 0; i < skillSetIds.length; i++) {
				skillSet += ds.skillSetStore.getById(Number(skillSetIds[i])).data.value;
				if (skillSetIds.length > (i+1))
					skillSet += ",";
			}
			this.myPanel.form.findField('skillSet').setValue(skillSet);
		}
		if (record.data.targetSkillSetIds != null && record.data.targetSkillSetIds != '') {
			var skillSetIds = record.data.targetSkillSetIds.split(',');
			var skillSet ='';
			for ( var i = 0; i < skillSetIds.length; i++) {
				skillSet += ds.skillSetStore.getById(Number(skillSetIds[i])).data.value;
				if (skillSetIds.length > (i+1))
					skillSet += ",";
			}
			this.myPanel.form.findField('targetSkillSet').setValue(skillSet);
		}
		if (record.data.roleSetIds != null && record.data.roleSetIds != '') {
			var roleSetIds = record.data.roleSetIds.split(',');
			var roleSet ='';
			for ( var i = 0; i < roleSetIds.length; i++) {
				roleSet += ds.targetRolesStore.getById(Number(roleSetIds[i])).data.value;
				if (roleSetIds.length > (i+1))
					roleSet += ",";
			}
			this.myPanel.form.findField('roleSet').setValue(roleSet);
		}

		if (record.data.skillSetGroupIds != null && record.data.skillSetGroupIds != '') {
			var skillSetGroupIds = record.data.skillSetGroupIds.split(',');
			var skillSetGroup ='';
			for ( var i = 0; i < skillSetGroupIds.length; i++) {
				skillSetGroup += ds.skillsetGroupStore.getById(Number(skillSetGroupIds[i])).data.name;
				if (skillSetGroupIds.length > (i+1))
					skillSetGroup += ",";
			}
			this.myPanel.form.findField('skillSetGroup').setValue(skillSetGroup);
		}

		if (app.candidatePanel != null){
			app.candidatePanel.openedFrom = "Candidate";
			this.openedFrom = "Candidate";
		}else
			this.openedFrom = "Candidate";
		
		this.emailUpdated = false;
		this.phoneUpdated = false;
		this.immigrationUpdated = false;

		//this.saveAndCloseButton.hide();
		this.saveButton.show();
		if (record.data.contactId != '' && record.data.contactId != null) {
			this.makeSMEButton.hide();
			this.viewSMEButton.show();
		}else{
			this.makeSMEButton.show();
			this.viewSMEButton.hide();
		}
		
		this.candidate = record;

		if (record.data.id != null && record.data.id != '' && record.data.id != 0) {
	    	this.deleteButton.enable();
			var params = new Array();
	    	params.push(['candidateId','=', record.data.id]);
	    	var filter = getFilter(params);
	    	filter.pageSize=1000;
	    	ds.candidate_requirementStore.loadByCriteria(filter);
	    	ds.tneProjectSummaryStore.loadByCriteria(filter);
	    	this.certificationGridPanel.loadCertifications();
	    	
	    	this.jobOpeningsGrid.store.on('load', function(store, records, options) {
	    		if (store.getCount() >0)
	    			Ext.getCmp('Candidate_Jobopenings_Fieldset').expand();
	    		else
	    			Ext.getCmp('Candidate_Jobopenings_Fieldset').collapse();
	       	}, this);

	    	this.projectGridPanel.store.on('load', function(store, records, options) {
	    		if (store.getCount() >0)
	    			Ext.getCmp('Candidate_Projects_Fieldset').expand();
	    		else
	    			Ext.getCmp('Candidate_Projects_Fieldset').collapse();
	       	}, this);

	        this.certificationGridPanel.store.on('load', function(store, records, options) {
	    		if (store.getCount() >0)
	    			Ext.getCmp('Candidate_Certifications_Fieldset').expand();
	    		else
	    			Ext.getCmp('Candidate_Certifications_Fieldset').collapse();
	       	}, this);

	    	
	    	if(record.data.employeeId == null)
	    		this.onBoardingButton.enable();
	    	else
	    		this.onBoardingButton.disable();

	    	this.loadShortlisted(record.data.id);
			this.loadSubRequirments(record.data.id);
		}else{
			this.myPanel.form.findField('version').setValue(0);

			this.deleteButton.disable();
			this.onBoardingButton.disable();
		}
	},

	loadRequirements : function() {
    	var params = new Array();
    	params.push(['jobOpeningStatus','=', "'01.In-progress','02.Resume Ready'"]);
    	params.push(['exceptShortlist','=', this.myPanel.form.findField('id').getValue()]);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.requirementSearchStore.loadByCriteria(filter);
    	app.loadMask.show();
        ds.requirementSearchStore.on('load', function(store, records, options) {
        	app.loadMask.hide();
        	this.loadSuggested();
       	}, this);
	},
	
	loadSuggested : function() {
    	this.suggestionsGrid.store.removeAll();

    	var requirementArray = new Array();
    	for ( var i = 0; i < ds.requirementSearchStore.getCount(); i++) {
			var requirement = ds.requirementSearchStore.getAt(i).data;
			requirement = this.calculateMatching(requirement);
			
			var priority = 0;
			if (requirement.matchScore >= 1 ) {
				if (requirement.hotness == '00') 
					priority = 6;
				else if (requirement.hotness == '01') 
					priority = 4;
				else if (requirement.hotness == '02') 
					priority = 3;
				
				requirement.matchScore = requirement.matchScore + priority;
				requirement.matchScoreDetails += '<br>Hotness : '+priority;
				requirementArray.push(requirement);
			}
		}
    	this.suggestionsGrid.store.add(requirementArray);
		this.suggestionsGrid.store.sort('matchScore', 'DESC');
		this.showSuggestedCount.setValue(this.suggestionsGrid.store.getCount()+' Suggested jobs');
		this.suggestionsGrid.getView().refresh();
		//this.getNegativeCriteria();
	},
	
	calculateMatching : function(requirement) {
    	var canSkillSet = this.myPanel.form.findField('skillSetIds').getValue().split(',');
    	var canRoleset = this.myPanel.form.findField('roleSetIds').getValue().split(',');
    	var canTargetSkillSet = this.myPanel.form.findField('targetSkillSetIds').getValue().split(',');
    	var canTargetRoles = this.myPanel.form.findField('targetRoles').getValue().split(',');
    	var canCertifications = this.certificationGridPanel.store.collect('settingsId');

		var skillset = 0, module =0, certification =0, targetRole =0, targetSkillset=0, currentRoles = 0 , priority = 0;
		var matchScore = 0;

		//skillset - skillset - 2
		if (requirement.skillSetIds != null && requirement.skillSetIds != '' && canSkillSet != null && canSkillSet != '') {
			var reqSkillset = requirement.skillSetIds.split(',');
			for ( var j = 0; j < reqSkillset.length; j++) {
				if (canSkillSet.indexOf(reqSkillset[j]) != -1) {
					skillset = skillset+2;
				}
			}
		}
		
		//skillset - module - 3
		if (requirement.module != null && requirement.module != '' && canSkillSet != null && canSkillSet != '') {
			var reqModule = requirement.module.split(',');
			var reqSecModule = requirement.secondaryModule.split(',');
			for ( var j = 0; j < reqModule.length; j++) {
				var moduleId = ds.skillSetStore.getAt(ds.skillSetStore.findExact('name',reqModule[j])).data.id;
				if (canSkillSet.indexOf(moduleId.toString()) != -1 || canSkillSet.indexOf(moduleId.toString()) != -1) {
					module = module+3;
				}
			}
		}
		
		//target skillset - skillset (Target skillset - Role set) - 1
		if (requirement.skillSetIds != null && requirement.skillSetIds != '' && canTargetSkillSet != null && canTargetSkillSet != '') {
			var reqSkillset = requirement.skillSetIds.split(',');
			for ( var j = 0; j < reqSkillset.length; j++) {
				if (canTargetSkillSet.indexOf(reqSkillset[j]) != -1) {
					targetSkillset = targetSkillset+1;
				}
			}
		}
		
		//targetRoles - targetRoles (Target Roles - Role set) - 2 for any
		if (requirement.targetRoles != null && requirement.targetRoles != '' && canTargetRoles != null && canTargetRoles != '') {
			reqTargetRoles = requirement.targetRoles.split(',');
			for ( var j = 0; j < reqTargetRoles.length; j++) {
				if (canTargetRoles.indexOf(reqTargetRoles[j]) != -1) {
					targetRole = 2;
				}
			}
		}

		//Roleset - targetRoles (Role set - Role set) - 3 for any
		if (requirement.targetRoles != null && requirement.targetRoles != '' && canRoleset != null && canRoleset != '') {
			reqTargetRoles = requirement.targetRoles.split(',');
			for ( var j = 0; j < reqTargetRoles.length; j++) {
				var reqRolesetId = ds.targetRolesStore.getAt(ds.targetRolesStore.findExact('name',reqTargetRoles[j])).data.id;
				if (canRoleset.indexOf(reqRolesetId.toString()) != -1) {
					currentRoles = 3;
				}
			}
		}

		//certifications - certifications - 5 
		if (requirement.certificationIds != null && requirement.certificationIds != '' && canCertifications != null && canCertifications != '') {
			reqCertifications = requirement.certificationIds.split(','); 
			for ( var j = 0; j < reqCertifications.length; j++) {
				if (canCertifications.indexOf(Number(reqCertifications[j])) != -1) {
					certification = certification + 5;
				}
			}
		}
		
		matchScore = skillset + module + certification + targetRole + targetSkillset + currentRoles ;
		
			
		requirement.matchScore = matchScore;
		requirement.matchScoreDetails = 'Skill set : '+skillset +'<br>Role set : '+currentRoles+ '<br>Module : '+module+
									'<br>Target Skill set : '+ targetSkillset + '<br>Target Role : '+targetRole+'<br>Certification :' + certification ;
			
		return requirement;
	},

	getNegativeCriteria : function() {
		if (this.suggestionsGrid.store.getCount() > 0) {
	    	var params = new Array();
	    	var requirementIds = this.suggestionsGrid.store.collect('id').toString();
	    	params.push(['requirementIds','=', requirementIds]);
	    	params.push(['candidateId','=', this.myPanel.form.findField('id').getValue()]);
	    	var filter = getFilter(params);
			app.candidateService.getNegativeCriteria(Ext.JSON.encode(filter),this.onGetNegativeCriteria,this);
		}
	},
	
	onGetNegativeCriteria : function(data) {
		if (data.success && data.returnVal.rows.length > 0) {
			for ( var i = 0; i < this.suggestionsGrid.store.getCount(); i++) {
				this.suggestionsGrid.store.getAt(i).data.matchIssue = false;
				this.suggestionsGrid.store.getAt(i).data.matchIssueNotes = '';
			}
			for ( var int = 0; int < data.returnVal.rows.length; int++) {
				if (data.returnVal.rows[int].type == 'client' && data.returnVal.rows[int].value != null) {
					var canIds = data.returnVal.rows[int].value.split(',');
					var client = ds.clientSearchStore.getById(Number(data.returnVal.rows[int].clientId)).data.name;
					var module = data.returnVal.rows[int].module;
					for ( var i = 0; i < canIds.length; i++) {
						this.suggestionsGrid.store.getById(Number(canIds[i])).data.matchIssue = true;
						this.suggestionsGrid.store.getById(Number(canIds[i])).data.matchIssueNotes = '-Sub to '+client+' with a different module ('+module+') <br>';
					}
				}else if (data.returnVal.rows[int].type == 'vendor' && data.returnVal.rows[int].value != null) {
					var canIds = data.returnVal.rows[int].value.split(' :: ');
					for ( var i = 0; i < canIds.length; i++) {
						if (canIds[i].split(' -- ')[1] >= 3) {
							var vendor = ds.vendorStore.getById(Number(canIds[i].split(' -- ')[1])).data.name;
							var module = canIds[i].split(' -- ')[2];	
							this.suggestionsGrid.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssue = true;
							if(this.suggestionsGrid.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssueNotes == null)
								this.suggestionsGrid.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssueNotes ='';
							this.suggestionsGrid.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssueNotes += '-Sub to '+vendor+' with a different module ('+module+') <br>';
						}
					}
				}
			}
		}
		this.suggestionsGrid.getView().refresh();
	},

	
	getSpecialCriteria : function(requirementId) {
		var candidateId = this.myPanel.form.findField('id').getValue();
		if (candidateId != null && candidateId != '')
			app.candidateService.getSuggestedCriteria(candidateId,this.onGetSuggestedCriteria,this);
	},
	
	onGetSuggestedCriteria : function(data) {

		if (data.success && data.returnVal.rows.length > 0) {
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var rec = data.returnVal.rows[i];
				if (rec.type == 'location' ){
					this.locationCriteria = rec.ids;
					if(rec.value > 0)
						document.getElementById('candidateCriteria_location').innerHTML = '<a onclick="javascript:app.candidateMgmtPanel.candidatePanel.filterPotential(\'locationCriteria\')">'
							+rec.value + ' Jobs which are local to candidate</a>';
					else
						document.getElementById('candidateCriteria_location').innerHTML = '';
				}else if (rec.type == 'client'){
					this.clientCriteria = rec.ids;
					if(rec.value > 0)
						document.getElementById('candidateCriteria_client').innerHTML = '<a onclick="javascript:app.candidateMgmtPanel.candidatePanel.filterPotential(\'clientCriteria\')">'
							+rec.value+ ' Jobs with Same Client & Same Module Submissions (01.In-progress, 02.Resume Ready)</a>';
					else
						document.getElementById('candidateCriteria_client').innerHTML = '';
				}else if (rec.type == 'certification'){
					this.certificationCriteria = rec.ids;
					if(rec.value > 0)
						document.getElementById('candidateCriteria_certification').innerHTML = '<a onclick="javascript:app.candidateMgmtPanel.candidatePanel.filterPotential(\'certificationCriteria\')">'
							+rec.value + ' Jobs with certifications that candidate had (01.In-progress, 02.Resume Ready)</a>';
					else
						document.getElementById('candidateCriteria_certification').innerHTML = '';
				}else if (rec.type == 'clientAll'){
					this.clientAllCriteria = rec.ids;
					if(rec.value > 0)
						document.getElementById('candidateCriteria_clientAll').innerHTML = '<a onclick="javascript:app.candidateMgmtPanel.candidatePanel.filterPotential(\'clientAllCriteria\')">'
							+rec.value + ' Jobs with Same Client & Same Module Submissions (Any Job opening Status)</a>';
					else
						document.getElementById('candidateCriteria_clientAll').innerHTML = '';
				}else if (rec.type == 'vendor'){
					this.vendorCriteria = rec.ids;
					if(rec.value > 0)
						document.getElementById('candidateCriteria_vendor').innerHTML = '<a onclick="javascript:app.candidateMgmtPanel.candidatePanel.filterPotential(\'vendorCriteria\')">'
							+rec.value + ' Jobs with Same Vendor & Same Module Submission (Any Job opening satatus)</a>';
					else
						document.getElementById('candidateCriteria_vendor').innerHTML = '';
				}else if (rec.type == 'project'){
					this.projectCriteria = rec.ids;
					if(rec.value > 0)
						document.getElementById('candidateCriteria_project').innerHTML = '<a onclick="javascript:app.candidateMgmtPanel.candidatePanel.filterPotential(\'projectCriteria\')">'
							+rec.value + ' Jobs with Same Client & Same Module Project (Any Job opening satatus)</a>';
					else
						document.getElementById('candidateCriteria_project').innerHTML = '';
				}
			}
		}
		this.criteriaPanel.doLayout();
	},
	
	filterPotential : function(type) {
    	var params = new Array();
    	params.push(['id','=', this[type]]);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.requirementSearchStore.loadByCriteria(filter);
    	app.loadMask.show();
    	this.suggestionsGrid.store.removeAll();
    	
        ds.requirementSearchStore.on('load', function(store, records, options) {
        	app.loadMask.hide();
        	var requirementArray = new Array();
        	for ( var i = 0; i < ds.requirementSearchStore.getCount(); i++) {
    			var requirement = ds.requirementSearchStore.getAt(i).data;
    			requirement = this.calculateMatching(requirement);
    			
    			var priority = 0;
    			if (requirement.hotness == '00') 
    				priority = 6;
    			else if (requirement.hotness == '01') 
    				priority = 4;
    			else if (requirement.hotness == '02') 
    				priority = 3;
    			requirement.matchScore = requirement.matchScore + priority;
    			requirement.matchScoreDetails += '<br>Hotness : '+priority;
    			requirementArray.push(requirement);
        	}
        	this.suggestionsGrid.store.add(requirementArray);
    		this.suggestionsGrid.store.sort('matchScore', 'DESC');
    		this.showSuggestedCount.setValue(this.suggestionsGrid.store.getCount()+' Suggested jobs');
    		this.suggestionsGrid.getView().refresh();
       	}, this);
	},
	
	saveCandidate: function(){
		this.clearMessage();
		if(! this.form.isValid()){
			this.updateError('Please fix the errors.');
			return false;
		}
		var immigrationStatus = this.myPanel.form.findField('immigrationStatus').getValue();
		var immigrationVerified = this.myPanel.form.findField('immigrationVerified').getValue().immigrationVerified;
		if (immigrationStatus != null && immigrationStatus != '' && immigrationStatus != 'CITIZENSHIP' && ['YES','NO'].indexOf(immigrationVerified) == -1 ) {
			this.updateError('Please select immigration verified (YES/NO)');
			return false;
		}
		var itemslength = this.myPanel.form.getFields().getCount();
   		var i = 0;    		
   		var candidate = {};
   		
   		while (i < itemslength) {
   			var fieldName = this.myPanel.form.getFields().getAt(i).name;
   				if(fieldName == 'id' ) {
   					var idVal = this.myPanel.form.findField(fieldName).getValue();
   					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0 && idVal != '') {
   						candidate[fieldName] = this.myPanel.form.findField(fieldName).getValue();
   					}
   				} else {
   					candidate[fieldName] = this.myPanel.form.findField(fieldName).getValue();
   				}
   			i = i + 1;
   		}
   		candidate['immigrationVerified'] = this.myPanel.form.findField('immigrationVerified').getValue().immigrationVerified;
   		candidate['gender'] = this.myPanel.form.findField('gender').getValue().gender;
   		
   		if (this.form.findField('comments').getValue() != null && this.form.findField('comments').getValue() != "" && this.form.findField('commentsCheckbox').getValue().commentsCheckbox == null){
			this.updateError('Please select comments priority (P1/P2).');
			return false;
   		}else if (this.form.findField('comments').getValue() != null && this.form.findField('comments').getValue() != ""){
   			if (this.form.findField('commentsCheckbox').getValue().commentsCheckbox == "p1")
   				candidate.p1comments = candidate.comments + '\n'+candidate.p1comments;
   			else
   				candidate.p2comments = candidate.comments + '\n'+candidate.p2comments;
   		}
   			
   		delete candidate.commentsCheckbox;
   		delete candidate.comments;
   		
   		if (candidate.priority == null )
   			delete candidate.priority;
   		if (candidate.confidentiality == null )
   			delete candidate.confidentiality;
   		if (candidate.employeeId == null )
   			delete candidate.employeeId;
   		if (candidate.rateFrom == null )
   			delete candidate.rateFrom;
   		if (candidate.rateTo == null )
   			delete candidate.rateTo;
   		if (candidate.technicalScore == null )
   			delete candidate.technicalScore;
   		
   		candidate.employmentType = candidate.selectedEmploymentType;
		while (candidate.employmentType.indexOf(', ') != -1) {
			candidate.employmentType = candidate.employmentType.replace(', ',',');
		}
   		delete candidate.selectedEmploymentType;

   		delete candidate.selectedSkillSetGroup;
   		delete candidate.skillSetGroupIds;
   		delete candidate.skillSetGroup;
   		delete candidate.selectedSkillSet;
   		delete candidate.skillSetIds;
   		delete candidate.roleSetIds;
   		delete candidate.roleSet;
   		delete candidate.selectedTargetSkillSet;
   		delete candidate.targetSkillSetIds;
   		delete candidate.selectedTargetRoles;
   		delete candidate.selectedRoleset;
   		delete candidate.clientCriteria;
   		delete candidate.clientAllCriteria;
   		delete candidate.locationCriteria;
   		delete candidate.certificationCriteria;
   		delete candidate.vendorCriteria;
   		delete candidate.projectCriteria;
   		delete candidate.showSuggestedCount;
   		delete candidate.showShortlistedCount;
   		delete candidate.showSubmittedCount;
   		delete candidate.submissionCombo;
   		delete candidate.submissionStatus;
   		delete candidate.resumeLinkHyperlink;

   		//skillset values
		var skillSetIds = this.myPanel.form.findField('skillSetIds').getValue();
		if (skillSetIds == null || skillSetIds == '' )
			skillSetIds = new Array();
		else
			skillSetIds = skillSetIds.split(',');
		var candidate_skillSet = new Array();
		for ( var i = 0; i < skillSetIds.length; i++) {
			var record = {};
			record.skillSetId = Number(skillSetIds[i]);
			candidate_skillSet.push(record);
		}

		//target skillset values
		var skillSetIds = this.myPanel.form.findField('targetSkillSetIds').getValue();
		if (skillSetIds == null || skillSetIds == '' )
			skillSetIds = new Array();
		else
			skillSetIds = skillSetIds.split(',');
		var candidate_TargetSkillSets = new Array();
		for ( var i = 0; i < skillSetIds.length; i++) {
			var record = {};
			record.skillSetId = Number(skillSetIds[i]);
			candidate_TargetSkillSets.push(record);
		}
		
		//Role set values
		var roleSetIds = this.myPanel.form.findField('roleSetIds').getValue();
		if (roleSetIds == null || roleSetIds == '' )
			roleSetIds = new Array();
		else
			roleSetIds = roleSetIds.split(',');
		var candidate_roleSets = new Array();
		for ( var i = 0; i < roleSetIds.length; i++) {
			var record = {};
			record.roleSetId = Number(roleSetIds[i]);
			candidate_roleSets.push(record);
		}

   		//skillset group values
		var skillSetIds = this.myPanel.form.findField('skillSetGroupIds').getValue();
		if (skillSetIds == null || skillSetIds == '' )
			skillSetIds = new Array();
		else
			skillSetIds = skillSetIds.split(',');
		var candidate_skillSetGroup = new Array();
		for ( var i = 0; i < skillSetIds.length; i++) {
			var record = {};
			record.skillSetGroupId = Number(skillSetIds[i]);
			candidate_skillSetGroup.push(record);
		}

		candidate_skillSet = Ext.JSON.encode(candidate_skillSet);
		candidate_roleSets = Ext.JSON.encode(candidate_roleSets);
		candidate_TargetSkillSets = Ext.JSON.encode(candidate_TargetSkillSets);
		candidate_skillSetGroup = Ext.JSON.encode(candidate_skillSetGroup);
    	var candidateObj = Ext.JSON.encode(candidate);
    	
    	candidateObj =  '{"candidate_SkillSets":{"com.recruit.candidate.service.Candidate_SkillSet":'+candidate_skillSet+'},'+
							'"candidate_RoleSets":{"com.recruit.candidate.service.Candidate_RoleSet":'+candidate_roleSets+'},'+
							'"candidate_TargetSkillSets":{"com.recruit.candidate.service.Candidate_TargetSkillSet":'+candidate_TargetSkillSets+'},'+
							'"candidate_SkillSetGroups":{"com.recruit.candidate.service.Candidate_SkillSetGroup":'+candidate_skillSetGroup+'},'+
							candidateObj.substring(1,candidateObj.length);
    	
		app.candidateService.saveCandidate(candidateObj, this.onSaveCandidate, this);
	},
	
	onSaveCandidate: function(data){
		this.modified = true;
		if(!data.success){
			var panel = this;
			if (data.errorMessage == "Version mismatch") {
				Ext.MessageBox.show({
					title:'Confirm',
		            msg: "This record has been changed by another user since you opened it, " +
		            		"do you want to update this record with latest version or do you want to override and change the record with your new updates",
		            buttonText: {                        
		                yes : "Update latest version",
		                no : "Override with new updates"
		            },
		            icon: Ext.MessageBox.INFO,
		            fn: function(btn) {
		            	if (btn == 'yes') {
		            		panel.getCandidate(data.returnVal.id);
		            	}else if (btn == 'no') {
		            		panel.myPanel.form.findField('version').setValue(Number(data.returnVal.version) + 1);
		            		panel.saveCandidate();
						}
		            }
		        });
			}
			else{
				Ext.MessageBox.show({
					title:'Error',
					msg: data.errorMessage,
					buttons: Ext.MessageBox.OK,
					icon: Ext.MessageBox.ERROR
				});
			}
		}else{
			ds.contractorsStore.loadByCriteria();
			if (this.openedFrom == "Recruit") {
				app.requirementMgmtPanel.requirementsGrid.getRecruitmentTab();
			}else{
				this.myPanel.getForm().findField('id').setValue(data.returnVal.id);
				this.myPanel.getForm().findField('version').setValue(data.returnVal.version);
				this.myPanel.getForm().findField('p1comments').setValue(data.returnVal.p1comments);
				this.myPanel.getForm().findField('p2comments').setValue(data.returnVal.p2comments);
				this.myPanel.getForm().findField('comments').reset();
				this.myPanel.getForm().findField('commentsCheckbox').reset();
				this.candidate.data = data.returnVal;
				this.updateMessage('Saved successfully');
				
				if (data.returnVal.employeeId > 0 && (this.emailUpdated || this.phoneUpdated || this.immigrationUpdated ) ) {
					this.updateMessage('Saved successfully, Email, Phone, immigration values have not been saved, they were send to AJ HR for approval.');
					var message = "";
					if (this.emailUpdated)
						message += "<br>Email : " + this.myPanel.form.findField('emailId').getValue();
					if (this.phoneUpdated)
						message += "<br>Phone : " + this.myPanel.form.findField('contactNumber').getValue();
					if (this.immigrationUpdated)
						message += "<br>ImmigrationStatus : " + this.myPanel.form.findField('immigrationStatus').getValue();
					
		        	var params = new Array();
		        	params.push(['candidateId','=', this.myPanel.form.findField('id').getValue()]);
		        	params.push(['message','=', message]);
		        	var filter = getFilter(params);
					app.reportsService.candidateUpdateRequest(Ext.JSON.encode(filter));
				}
				
				this.emailUpdated = false;
				this.phoneUpdated = false;
				this.immigrationUpdated = false;
				this.deleteButton.enable();
				
				if (this.closeButtonClicked) 
					this.closeFormDirty();
			}
		}
		this.body.scrollTo('top', 0);
	},
	
	deleteConfirm : function()
	{
		Ext.Msg.confirm("This will delete the Candidate", "Do you want to continue?", this.deleteCandidate, this);
	},
	
	onDelete : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			app.candidateMgmtPanel.getLayout().setActiveItem(0);
			app.candidateMgmtPanel.candidatesSearchPanel.search();
		}
		this.body.scrollTo('top', 0);
	},
	
	deleteCandidate: function(dat){
		if(dat=='yes'){
			var candidateId = this.myPanel.form.findField('id').getValue();
			app.candidateService.deleteCandidate(candidateId, this.onDelete, this);
		}
	},

	getRequirmentTab : function(record) {
		app.setActiveTab(0,'li_requirements');
		app.requirementMgmtPanel.getLayout().setActiveItem(1);
		app.requirementMgmtPanel.requirementDetailPanel.loadForm(record);
		app.requirementMgmtPanel.requirementDetailPanel.openedFrom ='Candidate';
	},
	
	onboardingConfirm : function() {
		Ext.Msg.confirm("Confirm", "Do you want to convert this candidate to employee?", this.giveOnboarding, this);
	},
	
	giveOnboarding : function(dat) {
		if(dat=='yes'){
			var candidateId = this.myPanel.form.findField('id').getValue();
			app.candidateService.giveOnboarding(candidateId, this.onGiveOnboarding, this);
		}
	},
	
	onGiveOnboarding : function(data) {
		this.modified = true;
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			this.updateMessage('Candidate converted to employee. Please verify in AJ');
			this.myPanel.form.findField('employeeId').setValue(data.returnVal.employeeId);
			this.onBoardingButton.disable();
		}
		this.body.scrollTo('top', 0);
	},
	
    shortlistCandidate : function() {
    	var selectedRecord = this.suggestionsGrid.getView().getSelectionModel().getSelection()[0];
    	if (selectedRecord == null) {
    		this.updateError('Please select a Job.');
    		return false;
		}
		var selectedRecord = this.suggestionsGrid.getView().getSelectionModel().getSelection()[0];
		var candidateId = this.myPanel.form.findField('id').getValue();
		var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : selectedRecord.data.id,
   	 		candidateId : candidateId,
   	 		submissionStatus : 'Shortlisted',
   	 	});
        delete rec.data.id;
        delete rec.data.vendorId;
		var records = new Array();
		records.push(rec.data);
    	records = Ext.JSON.encode(records);
    	app.requirementService.saveShortlisted(records,this.onSaveShortlisted,this);
	},

	onSaveShortlisted : function(data) {
		if (data.success) {
			this.updateMessage('Candidate Short listed successfully');
			this.loadRequirements();
			this.loadShortlisted(this.myPanel.form.findField('id').getValue());
		}else if (data.errorMessage == 'Candidate already shortlisted.Please refresh.')
			this.updateError('This candidate has already been shortlisted to the selected job opening');
		else
			this.updateError(data.errorMessage);
	},

	loadShortlisted : function(candidateId) {
    	if (candidateId != null && candidateId != ""){
        	var params = new Array();
        	params.push(['shortlistedCandidateId','=', candidateId]);
        	params.push(['jobOpeningStatus','=', "'01.In-progress','02.Resume Ready'"]);
        	var filter = getFilter(params);
        	this.shortlistedCandidates.store.loadByCriteria(filter);

        	this.shortlistedCandidates.store.on('load', function(store, records, options) {
            	this.showShortlistedCount.setValue(records.length+' Shortlisted Jobs');
            	for ( var i = 0; i < this.shortlistedCandidates.store.getCount(); i++) {
        			var requirement = this.shortlistedCandidates.store.getAt(i).data;
        			requirement = this.calculateMatching(requirement);
        			
        			var priority = 0;
        			if (requirement.matchScore >= 1 ) {
        				if (requirement.hotness == '00') 
        					priority = 6;
        				else if (requirement.hotness == '01') 
        					priority = 4;
        				else if (requirement.hotness == '02') 
        					priority = 3;
        				
        				requirement.matchScore = requirement.matchScore + priority;
        				requirement.matchScoreDetails += '<br>Hotness : '+priority;
        			}
        		}
           	}, this);
    	}else{
    		this.shortlistedCandidates.store.removeAll();
    	}
    	this.shortlistedCandidates.store.sort('matchScore', 'DESC');
    	this.shortlistedCandidates.getView().refresh();
    	app.candidateMgmtPanel.candidatePanel.shortlistedCandidates.getView().refresh();
	},
	
	loadSubRequirments : function(candidateId) {
    	if (candidateId != null && candidateId != ""){
        	var params = new Array();
        	params.push(['candidateId','=', candidateId]);
        	params.push(['table','=', "submissions"]);
        	if (this.submissionStatusCombo.getValue() != null) {
            	var openings = this.submissionStatusCombo.getValue().toString();
            	if (openings != '')
            		openings = "'"+openings.replace(/,/g, "','")+"'";
            	params.push(['submissionStatus','=', openings]);
        	}
        	if (this.submissionCombo.getValue() != null && this.submissionCombo.getValue() != '') {
       		 	var date = new Date();
       		 	params.push(['submittedDateTo','=',date]);
       		 	if (this.submissionCombo.getValue() == 'One Week')
       		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.DAY, -6)]);
       		 	else if (this.submissionCombo.getValue() == 'Two Weeks')
       		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.DAY, -13)]);
       		 	else if (this.submissionCombo.getValue() == 'One Month') 
       		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.MONTH, -1)]);
       		 	else if (this.submissionCombo.getValue() == 'Three Months') 
       		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.MONTH, -3)]);
        	}
        	var filter = getFilter(params);
        	this.submittedCandidates.store.loadByCriteria(filter);

        	this.submittedCandidates.store.on('load', function(store, records, options) {
            	this.showSubmittedCount.setValue(records.length+' Submissions');
           	}, this);

    	}else{
    		ds.subRequirementStore.removeAll();
    	}
	},

	removeShortlistedJob : function(requirementId) {
		var params = new Array();
		params.push(['candidateId','=', this.myPanel.form.findField('id').getValue()]);
    	params.push(['table','=', "Shortlisted"]);
    	params.push(['requirementId','=', requirementId]);
		var filter = getFilter(params);
		filter.pageSize=50;
		app.requirementService.removeShortlistedJob(Ext.JSON.encode(filter),this.onRemoveShortlistedJob,this);
	},
	
	onRemoveShortlistedJob : function(data) {
		if (data.success) {
			this.loadShortlisted(this.myPanel.form.findField('id').getValue());
		}else
			this.updateError('Delete failed');	
	},
	
	shareCandidate : function() {
		this.manager.showSharePanel();
		var record = this.candidate.data;
		if (record != null) {
			var values = this.myPanel.form.getValues();
			var detailsTable = "Name : "+values.firstName+' '+values.lastName+
						"<br><br>Email id : " +values.emailId+
						"<br><br>Contact number : " +values.contactNumber+
						"<br><br>City & State : " +values.cityAndState+
						"<br><br>Immigration Status : " +values.immigrationStatus+
						"<br><br>Contact Address : " +values.contactAddress+
						"<br><br>Current Job title : " +values.currentJobTitle;
			
			if (values.availability != null && values.availability != '')
				detailsTable += "<br><br>Availability : " +Ext.Date.format(new Date(values.availability),'m/d/Y');
			else
				detailsTable += "<br><br>Availability : N/A" ;
			if (values.startDate != null && values.startDate != '')
				detailsTable += "<br><br>Start Date : " +Ext.Date.format(new Date(values.startDate),'m/d/Y');
			else
				detailsTable += "<br><br>Start Date : N/A" ;
			
			detailsTable += "<br><br>Experience : " +values.experience+
						"<br><br>Marketing Status : " +values.marketingStatus+
						"<br><br>Communication : " +values.communication+
						"<br><br>Personality : " +values.personality+
						"<br><br>Expected Rate : " +values.expectedRate+ " From : "+values.rateFrom +" To : "+values.rateTo +
						"<br><br>Education : " +values.education+
						"<br><br>Highest Qualification Held : " +values.highestQualification+
						"<br><br>Employment Type : " +values.employmentType+
						"<br><br>Skill Set : " +values.skillSet+
						"<br><br>Role Set : " +values.roleSet+
						"<br><br>Target Skill Set : " +values.targetSkillSet+
						"<br><br>Target Roles : " +values.targetRoles+
						"<br><br>Certifications : " ;
			var certIds = this.certificationGridPanel.store.collect('settingsId');
			for ( var i = 0; i < certIds.length; i++) {
				detailsTable += ds.certificationsStore.getById(certIds[i]).data.value;
				if ((i+1) < certIds.length)
					detailsTable += ", ";
			}
			
			if (this.shortlistedCandidates.store.getCount() > 0) {
				detailsTable += "<br><br>Shortlisted Jobs : <table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>" +
						"<tr><th style='font-weight:bold;'>Posting Date</th>" +
						"<th style='font-weight:bold;'>Job Opening Status</th>" +
						"<th style='font-weight:bold;'>Posting Title</th>" +
						"<th style='font-weight:bold;'>Module</th>" +
						"<th style='font-weight:bold;'>Client</th>" +
						"<th style='font-weight:bold;'>Vendor</th></tr>"

				for ( var i = 0; i < this.shortlistedCandidates.store.getCount(); i++) {
					var record = this.shortlistedCandidates.store.getAt(i).data;
					
					detailsTable += "<tr>" +
					"<td>"+Ext.Date.format(new Date(record.postingDate),'m/d/Y')+"</td>" +
					"<td>"+record.jobOpeningStatus+"</td>" +
					"<td>"+record.postingTitle+"</td>" +
					"<td>"+record.module+"</td>" ;
					
					if (record.clientId != null && record.clientId != ''){
	                	var rec = ds.clientSearchStore.getById(Number(record.clientId));
	                	if(rec)
	                		detailsTable += "<td>"+rec.data.name+"</td>"; 
	                	else
	                		detailsTable += "<td>N/A</td>" ;
					}else
                		detailsTable += "<td>N/A</td>" ;

					if (record.vendorId != null && record.vendorId != ''){
						var ids = record.vendorId.toString().split(',');
						var names ="";
						for ( var j = 0; j < ids.length; j++) {
							var rec = ds.vendorStore.getById(Number(ids[j]));
							if (rec)
								names += rec.data.name +", ";	
						}
						detailsTable += "<td>"+names.substring(0,names.length-2)+"</td>";
					}else
                		detailsTable += "<td>N/A</td>" ;
					
					detailsTable += "</tr>" ;
				}
				detailsTable += "</table>";
			}
			
			if (this.submittedCandidates.store.getCount() > 0) {
				detailsTable += "<br><br>Submitted Jobs : <table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>" +
						"<tr><th style='font-weight:bold;'>Submitted Date</th>" +
						"<th style='font-weight:bold;'>Submission Status</th>" +
						"<th style='font-weight:bold;'>Vendor</th>" +
						"<th style='font-weight:bold;'>City & State</th>" +
						"<th style='font-weight:bold;'>Client</th>" +
						"<th style='font-weight:bold;'>Alert</th>" +
						"<th style='font-weight:bold;'>Posting Title</th>" +
						"<th style='font-weight:bold;'>Module</th>" +
						"<th style='font-weight:bold;'>Posting Date</th>" +
						"</tr>"

				for ( var i = 0; i < this.submittedCandidates.store.getCount(); i++) {
					var record = this.submittedCandidates.store.getAt(i).data;
					detailsTable += "<tr>" +
								"<td>"+Ext.Date.format(new Date(record.submittedDate),'m/d/Y')+"</td>" +
								"<td>"+record.submissionStatus+"</td>" +
								"<td>"+record.vendor+"</td>" +
								"<td>"+record.reqCityAndState+"</td>" +
								"<td>"+record.client+"</td>" +
								"<td>"+record.addInfoTodos+"</td>" +
								"<td>"+record.postingTitle+"</td>" +
								"<td>"+record.module+"</td>" +
								"<td>"+Ext.Date.format(new Date(record.postingDate),'m/d/Y')+"</td>" +
								"</tr>" ;
				}
				detailsTable += "</table>";
			}
			
			this.manager.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').setValue(detailsTable);
			this.manager.requirementSharePanel.fileName = values.firstName+' '+values.lastName;
			this.manager.requirementSharePanel.data = '';
			this.manager.requirementSharePanel.openedFrom = 'Candidates Panel';
		}

		this.manager.requirementSharePanel.emailShortlisted.disable();
		this.manager.requirementSharePanel.emailConfidential.disable();
	},

	getCandidate : function(candidateId) {
		if (candidateId != null  && candidateId != '') {
			var params = new Array();
			params.push(['id','=', candidateId]);
			var filter = getFilter(params);
			filter.pageSize=50;
			app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidate,this);
		}
	},
	
	onGetCandidate : function(data) {
		if (data.rows.length > 0) {
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			this.loadForm(record);
		}
	},

	makeSME : function() {
		var candidateId = this.myPanel.form.findField('id').getValue();
		var firstName = this.myPanel.form.findField('firstName').getValue();
		var lastName = this.myPanel.form.findField('lastName').getValue();
		if (candidateId != null && candidateId != '') {
	    	var params = new Array();
	    	params.push(['firstName','=', firstName]);
	    	params.push(['lastName','=', lastName]);
	    	var filter = getFilter(params);
	    	app.contactService.getContacts(Ext.JSON.encode(filter),this.onGetSME,this);
		}else
			this.updateError('Please save candidate.');
	},

	onGetSME : function(data) {
		var certIds = this.certificationGridPanel.store.collect('settingsId');
		var certifications ="";
		for ( var i = 0; i < certIds.length; i++) {
			certifications += ds.certificationsStore.getById(certIds[i]).data.value;
			if ((i+1) < certIds.length)
				certifications += "<br>";
		}
		if (data.success && data.rows.length > 0) {
			var contact = Ext.create('tz.model.Contact', data.rows[0]);
			contact.data.candidateId = this.myPanel.form.findField('id').getValue();
			contact.data.skillSet = this.myPanel.form.findField('skillSet').getValue();
			contact.data.module = this.myPanel.form.findField('skillSet').getValue();
			contact.data.canRoleSet = this.myPanel.form.findField('roleSet').getValue();
			contact.data.targetSkillSet = this.myPanel.form.findField('targetSkillSet').getValue();
			contact.data.targetRoles = this.myPanel.form.findField('targetRoles').getValue();
			contact.data.certifications = certifications;
			contact.data.candidateDeleted = false;
			contact.data.source = this.myPanel.form.findField('source').getValue();
			contact.data.referral = this.myPanel.form.findField('referral').getValue();
			contact.data.resumeLink = this.myPanel.form.findField('resumeLink').getValue();
			contact.data.skillSetIds = this.myPanel.form.findField('skillSetIds').getValue();
			contact.data.roleSetIds = this.myPanel.form.findField('roleSetIds').getValue();
			contact.data.skillSetGroupIds = this.myPanel.form.findField('skillSetGroupIds').getValue();
		}else{
	   	 	var contact = Ext.create( 'tz.model.Contact',{
	   			candidateId : this.myPanel.form.findField('id').getValue(),
	   			firstName : this.myPanel.form.findField('firstName').getValue(),
	   			lastName : this.myPanel.form.findField('lastName').getValue(),
	   			email : this.myPanel.form.findField('emailId').getValue(),
	   			cellPhone : this.myPanel.form.findField('contactNumber').getValue(),
	   			candidateId : this.myPanel.form.findField('id').getValue(),
	   			skillSet : this.myPanel.form.findField('skillSet').getValue(),
	   			module : this.myPanel.form.findField('skillSet').getValue(),
	   			canRoleSet : this.myPanel.form.findField('roleSet').getValue(),
	   			targetSkillSet : this.myPanel.form.findField('targetSkillSet').getValue(),
	   			targetRoles : this.myPanel.form.findField('targetRoles').getValue(),
	   			certifications : certifications,
	   			candidateDeleted : false,
	   			gender : this.myPanel.form.findField('gender').getValue().gender,
	   			confidentiality : this.myPanel.form.findField('confidentiality').getValue(),
	   			jobTitle : this.myPanel.form.findField('currentJobTitle').getValue(),
	   			source : this.myPanel.form.findField('source').getValue(),
	   			referral : this.myPanel.form.findField('referral').getValue(),
	   			resumeLink : this.myPanel.form.findField('resumeLink').getValue(),
				skillSetIds : this.myPanel.form.findField('skillSetIds').getValue(),
				roleSetIds : this.myPanel.form.findField('roleSetIds').getValue(),
				skillSetGroupIds : this.myPanel.form.findField('skillSetGroupIds').getValue(),
	   	 	});
		}
		app.setActiveTab(0,'li_contacts');
		app.contactMgmtPanel.getLayout().setActiveItem(1);
		app.contactMgmtPanel.contactDetailPanel.loadForm(contact);
		app.contactMgmtPanel.contactDetailPanel.opendFrom = 'Candidate';
	},
	
	viewContact : function() {
		var candidateId = this.myPanel.form.findField('id').getValue();
		if (candidateId != null && candidateId != '') {
	    	var params = new Array();
	    	params.push(['candidateId','=', candidateId]);
	    	var filter = getFilter(params);
	    	app.contactService.getContacts(Ext.JSON.encode(filter),this.onGetContact,this);
		}
	},
	
	onGetContact : function(data) {
		if (data.success && data.rows.length > 0) {
	    	app.setActiveTab(0,'li_contacts');
	    	app.contactMgmtPanel.getLayout().setActiveItem(1);
			var contact = Ext.create('tz.model.Contact', data.rows[0]);
			app.contactMgmtPanel.contactDetailPanel.loadForm(contact);
			app.contactMgmtPanel.contactDetailPanel.opendFrom='Candidate';
		}
	},

	editRequirement : function(requirementId) {
		var params = new Array();
    	params.push(['id','=', requirementId]);
    	var filter = getFilter(params);
    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onGetRequirements,this);
	},
	
	onGetRequirements : function(data) {
		if (data.success && data.rows.length > 0) {
			app.setActiveTab(0,'li_requirements');
			app.requirementMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Requirement', data.rows[0]);
			app.requirementMgmtPanel.requirementDetailPanel.loadForm(record);
			app.requirementMgmtPanel.requirementDetailPanel.openedFrom = 'Candidate';
		}
	},

});
