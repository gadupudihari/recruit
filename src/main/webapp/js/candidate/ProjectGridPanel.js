//Define a project grid
Ext.define('tz.ui.ProjectGridPanel', {
	extend : 'Ext.grid.Panel',
	title: 'List of Projects',
    height:250,
    anchor:'100%',
    width :'100%',
	frame:true,
	
	initComponent : function() {
		
		this.store = ds.tneProjectSummaryStore;
		
		this.columns = [{
			text : 'Project Name',
			width : 130,
			dataIndex : 'projectName'
		},{
			text : 'Project Code',
			width : 200,
			dataIndex : 'projectCode'
		},{
			text : 'Vendor Name',
			width:100,
			dataIndex : 'vendorName'
		},{
			text : 'Project Status',
			width : 100,
			dataIndex : 'projectStatus',
			renderer: statusRenderer
		} ,{
			text : 'Proj StartDate',
			width : 100,
			xtype: 'datecolumn',   
			format:'m/d/Y',
			dataIndex : 'projectStartDate'
		},{
			text : 'Proj EndDate',
			width : 100,
			xtype: 'datecolumn',   
			format:'m/d/Y',
			dataIndex : 'projectEndDate'
		},{
			text : 'Appr. EndDate',
			width : 100,
			xtype: 'datecolumn',   
			format:'m/d/Y',
			dataIndex : 'approxEndDate',
		},{
			text : 'Vendor Rate',
			align : 'right',
			width : 80,
			dataIndex : 'vendorRate',
			renderer : Ext.util.Format.usMoney
		},{
			text : 'Employee Rate',
			align : 'right',
			width : 80,
			dataIndex : 'employeeRate',
			renderer:employeeRateRenderer
		},{
			text : 'Employer',
			width : 70,
			dataIndex : 'employer',
			renderer: employerRenderer,
		},{
			text : 'Invoicing Terms',
			width : 90,
			dataIndex : 'invoicingTerms',
			renderer: weeksRenderer,
		},{
			text : 'Payment Terms',
			width : 90,
			dataIndex : 'paymentTerms',
			renderer: weeksRenderer,
		},{
			text : 'Project Comments',
			width : 175,
			dataIndex : 'projectComments',
		}];

		Ext.apply(this, {
			columns : this.columns,
			viewConfig : {
				stripeRows : true
			}
		});

        this.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

		this.callParent(arguments);
        
	},

});

function employerRenderer(val) {
	  if (val == "TCS") {
	        return '<span style="color:green;">' + val + '</span>';
	    }
	  if (val == "CPS") {
	        return '<span style="color:blue;">' + val + '</span>';
	    }  
	    return val;
}


function employeeRateRenderer(val) {
	val = val/(52*40);
	var decimals = 4;
	var currencySign ='$';
	var negativeSign = '',
	
    format = ",0",
    i = 0;
	v = val - 0;
	if (v < 0) {
	    v = -v;
	    negativeSign = '-';
	}
	decimals = decimals || Ext.util.Format.currencyPrecision;
	format += format + (decimals > 0 ? '.' : '');
	for (; i < decimals; i++) {
	    format += '0';
	}
	v = Ext.util.Format.number(v, format); 
	if ((Ext.util.Format.currencyAtEnd) === true) {
	    return Ext.String.format("{0}{1}{2}", negativeSign, v, currencySign || Ext.util.Format.currencySign);
	} else {
	    return Ext.String.format("{0}{1}{2}", negativeSign, currencySign || Ext.util.Format.currencySign, v);
	}
}
function statusRenderer(val) {
    if (val == "Inactive") {
        return '<span style="color:red;">' + val + '</span>';
    }else if(val == "Active") {
    	return '<span style="color:green;">' + val + '</span>';
    }
    return val;
}
function weeksRenderer(val) {
	return parseInt(val/7) + ' Weeks';
} 
