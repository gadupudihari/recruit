Ext.define('tz.ui.ReportsMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',
    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;
        
    	me.reportsDetailPanel = new tz.ui.ReportsDetailPanel({manager:me});
		
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,    	                 
            	border:false,  
            	items: [me.reportsDetailPanel]
            }
        ];
        me.callParent(arguments);
    },


	initScreen: function(){
		this.getLayout().setActiveItem(0);
		this.reportsDetailPanel.readReportsFile();
		var params = new Array();
    	var filter = getFilter(params);
    	ds.researchReportStore.loadByCriteria(filter);
	},
	
});
