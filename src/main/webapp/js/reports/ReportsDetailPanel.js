Ext.define('tz.ui.ReportsDetailPanel',{
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    anchor:'100%',
    autoScroll:true,
//creating view in init function
    initComponent: function() {
        var me = this;
        me.availableCandidates = '';
        me.candidateIds = '';
        me.vendorIds = new Array();
        
	    Ext.define('dataview_model', {
	        extend    : 'Ext.data.Model',
	        fields  : [
	            {name: 'count',       type: 'string'},
	            {name: 'maxcolumns',  type: 'string'}
	        ]
	    });

	    Ext.create('Ext.data.Store', {
	    	storeId : 'viewStore',
            model    : 'dataview_model',
            data    : [
                {count: '7', maxcolumns: '10'}
            ],
        });

        var tpl = new Ext.XTemplate(
            "<table width='100%' border='1'>" +
            "</table>"
        );

        me.dataView = Ext.create('Ext.view.View', {
        	    store: Ext.data.StoreManager.lookup('viewStore'),
        	    tpl: tpl,
        	    itemSelector: 'div.thumb-wrap',
        	    emptyText: 'No images available',
        	    renderTo: Ext.getBody()
        	});
        
        me.searchPanel = Ext.create('Ext.form.Panel', {
        	region: 'north',
        	collapsible: true,
        	bodyPadding: 10,
        	border : 1,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
            frame:true,
            buttonAlign:'left',
            buttons : [
                  {
                  	text : 'Search',
                  	scope: this,
      				handler: function() {
      					this.showReport();	
					},
          	        tabIndex:6,
      			},{
      				text : 'Reset',
      				scope: this,
      				handler: function() {
      					this.reset();	
					},
      				tabIndex:7,
      			},{
      				text : 'Clear',
      				scope: this,
      				tabIndex:8,
      	             handler: function() {
      	            	 this.searchPanel.form.reset();
      	             }
      			}
      		],
        });
        
        me.reportsStore = Ext.create('Ext.data.Store', {
        	fields: ['label', 'value'],
        });
        
        me.reportsGrid = Ext.create('Ext.grid.Panel', {
        	title : 'Reports',
        	region: 'west', 
        	collapsible: true,
        	width: 250,
        	xtype: 'panel',
        	store : me.reportsStore,
        	viewConfig: {
        		style: {overflow:'auto', overflowX: 'hidden'}
        	},
        	columns: [{ dataIndex: 'label', flex:1,
        				renderer:function(value, metadata, record){
        					return '<a href=#>'+value+'</a>'; 
        				}
        	}],
            listeners : {
                cellclick: function( grid, td, cellIndex, record, tr, rowIndex, e, eOpts ){
                	app.reportsMgmtPanel.reportsDetailPanel.showReport();
                }
            }

        }), 
        
        me.myPanel = Ext.create('Ext.panel.Panel', {
            width: '100%',
            title: '',
            layout: 'border',
            items: [
                    me.reportsGrid,
                    {
                    	xtype: 'panel',
  	            		split: true,
  	            		autoScroll:true,
  	            		region: 'center',
  	            		items: [me.searchPanel,
  	            		        {
  	            					border : false,
  	            					height : 10
  	            		        },
  	            		        me.dataView
  	            		        ]
                    }],
                    renderTo: Ext.getBody()
        });

		var browserHeight = app.mainViewport.height;
		var headerHt = app.mainViewport.items.items[0].getHeight();
    	me.myPanel.setHeight(browserHeight - headerHt - 50);
        
        me.items = [me.myPanel]; 
        
        me.listeners = {
                afterRender: function(thisForm, options){
                    this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                        enter: this.showReport,
                        scope: this
                    });
                }
            } 
        
        me.callParent(arguments);
    },

    showReport : function(report) {
    	if (this.reportsGrid.getView().getSelectionModel().getSelection()[0] != null ) {
        	report = this.reportsGrid.getView().getSelectionModel().getSelection()[0].data.label;
		}else{
			report = 'Candidates for Marketing 1';
		}
    	if (this.searchPanel.title != report)
    		this.createFilters(report);
    	switch (report) {
		case 'Available Candidates':
			this.searchCandidates();
			break;
		case 'Research Tasks':
			this.loadCurrentJobs();
			break;
		case 'Activity Report':
			this.loadActivityReport();
			break;
		case 'Data Report':
			this.loadDataReport();
			break;
		case 'Certifications':
			this.loadCertifications();
			break;
		case 'Submission Status':
			this.loadSubmission();
			break;
		case 'Missing Data':
			this.loadMissingData();
			break;
		case 'Statistics of Candidates':
			this.loadCandidatesStatistics();
			break;
		case 'Statistics of Vendor':
			this.loadVendorStatistics();
			break;
		case 'References':
			this.getReferences();
			break;
		case 'Related Client/Vendors':
			this.getRelated();
			break;
		case 'Candidates for Marketing 1':
			this.getMarketingVendors();
			break;
		case 'Candidates for Marketing 2':
			this.getMarketing();
			break;
		default:
			break;
		}
	},
    
    readReportsFile : function() {
    	var xmlDoc= this.loadXMLDoc("reports.xml");
    	var reports = new Array();
    	this.reportsStore.removeAll();
    	
    	for ( var i = 0; xmlDoc.getElementsByTagName("report") [i] != null ; i++) {
    		var report=xmlDoc.getElementsByTagName("report") [i];
        	var name = report.getElementsByTagName('name')[0].textContent;
        	reports.push({"label":name,"value":name});
		}
    	this.reportsStore.add(reports);
    	this.showReport();
	},
    
	loadXMLDoc : function(filename) {
		 var xmlDoc;
		 try {
			 var xmlhttp = new XMLHttpRequest();
			 xmlhttp.open('GET', filename, false);
			 xmlhttp.setRequestHeader('Content-Type', 'text/xml');
			 xmlhttp.send('');
			 xmlDoc = xmlhttp.responseXML;
		 } catch (e) {
			 try {
				 xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			 } catch (e) {
				 console.error(e.message);
			 }
		 }
		 return xmlDoc;
	},
	
	createFilters : function(selectedReport) {
		var tpl = new Ext.XTemplate();
		this.dataView.tpl = tpl;
		this.dataView.refresh()

		var xmlDoc= this.loadXMLDoc("reports.xml");
		var items = [];
		this.searchPanel.removeAll();
		var container = new Ext.container.Container({
			layout: {
		        type: 'table',
		        columns: 3,
		        tableAttrs: {
		            style: {
		                width: '100%',
		                labelWidth : 120
		            }
		        }
		    },
		    items: []
		});
		
		for ( var i = 0; xmlDoc.getElementsByTagName("report") [i] != null ; i++) {
    		var report=xmlDoc.getElementsByTagName("report") [i];
        	var name = report.getElementsByTagName('name')[0].textContent;
        	if (selectedReport == name) {
        		this.searchPanel.setTitle(name);
        		for ( var j = 0; report.getElementsByTagName('filter')[j] != null; j++) {
    				var filter = report.getElementsByTagName('filter')[j];
    				var filterLabel = filter.getAttribute('label');
    				var filterName = filter.getAttribute('name');
    				var type  = filter.getAttribute('type');
    				var defaultValue  = filter.getAttribute('default');
    				switch (type.toLowerCase()) {
					case 'textfield':
				        var field = new Ext.form.field.Text({
				        	fieldLabel: filterLabel,
				        	name : filterName,
				        	labelWidth : 120,
				        	value : defaultValue
				        });
				        items.push(field);
						break;
					case 'datefield':
				        var field = new Ext.form.field.Date({
				        	fieldLabel: filterLabel,
				        	name : filterName,
				        	labelWidth : 120
				        });
				        items.push(field);
						break;
					case 'combobox':
				        var comboStore = Ext.create('Ext.data.Store', {
				        	fields: ['label'],
				        	loadAll : function(query){
				           		app.reportsService.getComboValues(query, this.onLoadAll, this);
				            },
				            onLoadAll: function(data){
				            	data = Ext.decode(data);
				        		if (data.success  && data.returnVal.rows) {
									var comboArray = new Array();
				        			for ( var m = 0; m < data.returnVal.rows.length; m++) {
										comboArray.push({"label":data.returnVal.rows[m]});
									}
				        			this.loadData(comboArray);
				        		} else {
				        			console.log('Unable to load the data');
				        		}
				            }
				        });
				        if (filter.getAttribute('values') == null || filter.getAttribute('values') =="") {
				        	var query = filter.getAttribute('query');
							comboStore.loadAll(query);
						}else{
							var comboArray = new Array();
					        comboArray = filter.getAttribute('values').split(',');
					        for ( var k = 0; k < comboArray.length; k++) {
					        	comboArray[k]= {"label":comboArray[k]};
							}
					        comboStore.removeAll();
					        comboStore.add(comboArray);
						}
				        
						var field = Ext.create('Ext.form.ComboBox', {
						    fieldLabel: filterLabel,
				            name: filterName,
						    queryMode: 'local',
						    displayField: 'label',
						    valueField: 'label',
						    store: comboStore,
						    labelWidth :120,
						    tabIndex:3,
						    value : defaultValue
						});

				        items.push(field);
						break;
					default:
						break;
					}
				}
			}
		}
		container.items.add(items);
		this.searchPanel.add(container);
		if(items.length > 0 )
			this.searchPanel.show();
		else
			this.searchPanel.hide();
	},
	
    searchCandidates : function() {
    	app.loadMask.show();
    	var values = this.searchPanel.getValues();
    	var query = "SELECT id 'Candidate Id'," ;
    	if(values.candidateName == 'Show')
    		query += "concat(firstName,' ',lastname) Candidate, ";
    	query +="(select group_concat(s.name SEPARATOR ' -- ') from recruit_certification c1,recruit_settings s where s.id=c1.settingsId and c1.candidateId=c.id) Certifications," +
    			"date_format(availability,'%m/%d/%Y') Availability, " +
    			"if(employmentType like '%1099%',if(rateTo is null or rateto =0,rateFrom,RateTo)+10, " +
    			"if(employmentType like '%W2%',if(rateTo is null or rateto =0,rateFrom,RateTo)+20,if(rateTo is null or rateto =0,rateFrom,RateTo))) Rate, " +
    			"if(relocate,'Yes','No') 'Will Relocate',if(travel,'Yes','No') 'Will Travel',cityAndState 'City & State',highestQualification 'Highest Qualification'," +
    			"replace(skillSet, ',', '<br>') Skillset " +
    			"FROM recruit_candidate c where 1=1";
    	if (values.candidateId != null && values.candidateId != "" )
    		query += " and id ="+values.candidateId;

    	if (values.marketingStatus != null && values.marketingStatus != "" ){
    		var marketingStatus = values.marketingStatus;
        	while (marketingStatus.length >0) {
            	if (marketingStatus.substr(marketingStatus.length-1,marketingStatus.length) == ",")
            		marketingStatus = marketingStatus.substr(0,marketingStatus.length-1);
    			else
    				break;
    		}
        	if (marketingStatus.search(',') != -1){ 
        		marketingStatus = "'"+marketingStatus.replace(/,/g, "','")+"'";
        		query += " and marketingStatus in ("+marketingStatus+")";
        	}else{
        		query += " and marketingStatus like '"+marketingStatus+"'";
        	}
    	}
    	if (values.priority != null && values.priority != "" )
    		query += " and priority like '"+values.priority+"'";
    	if (values.relocate != null && values.relocate != "" ){
    		if (values.relocate == 'Yes')
    			query += " and relocate = true";
    		else if (values.relocate == 'No')
    			query += " and relocate = false";
    	}
    	if (values.travel != null && values.travel != "" ){
    		if (values.travel == 'Yes')
    			query += " and travel = true";
    		else if (values.travel == 'No')
    			query += " and travel = false";

    	}
    	if (values.skillset != null && values.skillset != "" )
    		query += " and skillset like '%"+values.skillset+"%'";
    	
    	if (values.availabilityDateFrom != undefined && values.availabilityDateFrom != '') {
    		var availabilityDateFrom = Ext.Date.format(new Date(values.availabilityDateFrom) , 'Y-m-d');
    		query += " and date(availability) >= '"+availabilityDateFrom+"'";
    	}
    	if (values.availabilityDateTo != undefined && values.availabilityDateTo != '') {
    		var availabilityDateTo = Ext.Date.format(new Date(values.availabilityDateTo) , 'Y-m-d');
    		query += " and date(availability) <= '"+availabilityDateTo+"'";
    	}
    	if (values.certification != null  && values.certification != '') {
			if (values.certification == 'Exists')
				query += " and id in (select candidateId from recruit_certification)";
			else if (values.certification == 'None')
				query += " and id not in (select candidateId from recruit_certification)";
		}
    	query += " order by availability desc";
    	app.reportsService.getResult(query,this.onGetResult,this);
	},
	
	loadActivityReport : function() {
		var count = "select count(newValue) from recruit_eventlogging where tableName='Requirements' and newValue=name";
    	if ((this.searchPanel.getValues().dateFrom != undefined && this.searchPanel.getValues().dateFrom != '') || 
    			(this.searchPanel.getValues().dateTo != undefined && this.searchPanel.getValues().dateTo != '')) {
    		if (this.searchPanel.getValues().dateFrom != undefined && this.searchPanel.getValues().dateFrom != '') {
        		var dateFrom = Ext.Date.format(new Date(this.searchPanel.getValues().dateFrom) , 'Y-m-d');
        		count += " and date(loggedOn) >= '"+dateFrom+"'";
			}
    		if (this.searchPanel.getValues().dateTo != undefined && this.searchPanel.getValues().dateTo != '') {
        		var dateTo = Ext.Date.format(new Date(this.searchPanel.getValues().dateTo) , 'Y-m-d');
        		count += " and date(loggedOn) <= '"+dateTo+"'";
			}
    	}else
    		count += " and date(loggedOn) = curdate()";
    	
    	var query = "select name ' ',("+count+") ' ' from recruit_settings where type = 'Job Opening Status' order by name";
		app.reportsService.getResult(query,this.onGetActivityResult,this);
	},

	onGetActivityResult : function(data) {
		app.loadMask.show();
		if (data.search('"success":false') != -1) {
			data = Ext.decode(data);
			this.showError(data.errorMessage);
		}else{
			var table =data.substring(29,data.search('</table>')+8);
			table = table.replace("<table width='100%'","<table width='30%'")
			table = "<table width='30%'  noOfCols='2' style='border:1px solid black;border-collapse:collapse;'>" +
					"<tr><td height='25' style='background-color:#FFFF99;font-weight:bold;border:1px solid black;' >Change of Status to </td></tr>"+
					table+
					"</table>";
			
			var tpl = new Ext.XTemplate(
	    		table
	        );
	      	this.dataView.tpl = tpl;
		}
      	this.dataView.refresh()
      	app.loadMask.hide();
	},
	
	onGetResult : function(data) {
		app.loadMask.show();
		if (data.search('"success":false') != -1) {
			data = Ext.decode(data);
			this.showError(data.errorMessage);
		}else{
			var table =data.substring(29,data.search('</table>')+8);
			
			if (this.reportsGrid.getView().getSelectionModel().getSelection()[0] == null || this.reportsGrid.getView().getSelectionModel().getSelection()[0].data.label == 'Available Candidates'){
				table = table.replace("<table width='100%' style='border:1px solid black;border-collapse:collapse;' >",
										"<table width='100%' style='border:1px solid black;border-collapse:collapse;' ><tbody id= 'AVC'>");
				table = table.replace("</table>","</tbody></table>");
				table = '<table width="100%"><tr><td align="right"><button onclick="javascript:app.reportsMgmtPanel.reportsDetailPanel.exportExcel(\'AVC\')"">'+
							'<img src="images/icon_excel.gif" alt=""> Export to Excel</button></td></tr></table>'+table;
				
			}
			
			var tpl = new Ext.XTemplate(
	    		table
	        );
	      	this.dataView.tpl = tpl;
		}
      	this.dataView.refresh();
      	app.loadMask.hide();
	},

	reset:function(){
		this.form.reset();
		var tpl = new Ext.XTemplate();
		this.dataView.tpl = tpl;
		this.dataView.refresh();
		this.showReport();
	},
	
	showError : function(error) {
		var tpl = new Ext.XTemplate(
				"<table>" +
				"<tr><td style='color:red;'><font size='3'>"+error+"</font></td></tr>" +
                "</table>"
        );
		this.dataView.tpl = tpl;
		this.dataView.refresh()
	},
	
	loadCurrentJobs : function() {
		//vendor is considered as skillSet
		//client is considered as helpCandidate 
		//candidate is considered as publicLink
		
    	var values = this.searchPanel.getValues();
		var query = "SELECT r.id,r.hotness,r.cityAndState,r.postingTitle,date_format(r.postingdate,'%m/%d') postingdate,r.module,r.addInfo_Todos alert," +
		"t.task,t.status,t.comments,t.lastUpdatedUser,(select name from recruit_clients where id=r.clientId) client," +
		"(select group_concat(name) from recruit_clients c,recruit_sub_vendors s where c.id=s.vendorId and s.requirementId =r.id) vendor" +
		" FROM recruit_requirements r,recruit_researchtasks t  where r.id=t.requirementId ";

    	if (values.status != null && values.status != "" ){
        	var status = values.status.toString();
        	if (status != '')
        		status = "'"+status.replace(/,/g, "','")+"'";
        	query += " and t.status in ("+status+") ";
    	}
    	if (values.postingDate != null && values.postingDate != "") {
    		var date = new Date();
			if (values.postingDate == '1 Week')
				query += " and date(r.postingdate) >= '"+Ext.Date.format(Ext.Date.add(date, Ext.Date.DAY, -6),'Y-m-d')+"'";
			if (values.postingDate == '2 Weeks')
				query += " and date(r.postingdate) >= '"+Ext.Date.format(Ext.Date.add(date, Ext.Date.DAY, -13),'Y-m-d')+"'";
			if (values.postingDate == '1 Month')
				query += " and date(r.postingdate) >= '"+Ext.Date.format(Ext.Date.add(date, Ext.Date.MONTH, -1),'Y-m-d')+"'";
			if (values.postingDate == '3 Months')
				query += " and date(r.postingdate) >= '"+Ext.Date.format(Ext.Date.add(date, Ext.Date.MONTH, -3),'Y-m-d')+"'";
		}
    	query += " order by r.postingdate desc,r.id";
		app.reportsService.loadCurrentReport(query,this.onGetCurrentResult,this);
	},
	
	onGetCurrentResult : function(data) {
		app.loadMask.show();
		if (data.success && data.rows.length > 0) {
			var tasksHeadings = "<col width='10%'> <col width='35%'> <col width='35%'> <col width='10%'> <col width='10%'>"+
								"<tr bgcolor='#FFFFFF' style='font-weight:bold;text-align:center;'>"+
								"<td>Status</td>"+
								"<td>Task</td>"+
								"<td>Comments</td>"+
								"<td>Updated by</td></tr>";
			
			var preId ="", table ="", count=0;
			for ( var i = 0; i < data.rows.length; i++) {
				var record = data.rows[i];
				var requirement = "<tr bgcolor='#FFFCBC'><td colspan='4'>"+"Job Id : "+record.id+", Hotness : "+record.hotness+
						", Date : "+record.postingDate+", Title : "+record.postingTitle+", Module : "+record.module+
						", Client : "+record.client+", Vendor : "+record.vendor+", Alert : <span style='color:red;'>"+record.alert+"</span>"+
						", City & State : "+record.cityAndState+"</td></tr>";
				if (preId == "") {
					preId = record.id;
					count=1;
					table = "<table cellpadding='5' width='85%' bgcolor='#575252'>"+requirement+'<tr bgcolor="#FFFFFF" ><td>';
				}
				if (preId != record.id) {
					table += "</td></tr></table><br><table cellpadding='5' width='85%' bgcolor='#575252'>"+requirement+'<tr bgcolor="#FFFFFF" ><td>';
					preId = record.id;
					count=1;
				}
				table += count +'. '+ record.status+' - ' + record.task+' - ' + record.comments + '<br>';
				count++;
			}
			table += "</td></tr></table>";
			
			var tpl = new Ext.XTemplate(
					table
			);
		}else{
			var tpl = new Ext.XTemplate();
		}
		this.dataView.tpl = tpl;
		this.dataView.refresh();
      	app.loadMask.hide();
	},
	
	loadDataReport : function() {
		var query = 'select id,firstName,lastName,emailId,contactNumber,priority,currentJobTitle,skillSet,relocate,travel,followUp,startDate,availability,"New" p1comments '
				+ 'from recruit_candidate where date(created) = curdate() and priority =0 union '
				+ 'select id,firstName,lastName,emailId,contactNumber,priority,currentJobTitle,skillSet,relocate,travel,followUp,startDate,availability,"Active" p1comments '
				+ 'from recruit_candidate where priority =0 union '
				+ 'select id,firstName,lastName,emailId,contactNumber,priority,currentJobTitle,skillSet,relocate,travel,followUp,startDate,availability,"Available" p1comments '
				+ 'from recruit_candidate where availability between curdate() and date_add(curdate(), interval 45 day)';
		
		app.reportsService.loadDataReport(query,this.onGetDataResult,this);
	},
	
	onGetDataResult : function(data) {
		if (data.success) {
			app.loadMask.hide();
			var newCandidate = '', activeCandidate = '', availableCandidate = '', headingsRow='';
			var newCount = 0, activeCount = 0, availableCount = 0;

			headingsRow += '<tr>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Id</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Priority</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">First Name</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Last Name</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Availability</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Start Date</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Email Id</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Contact Number</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Current Job Title</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Will Relocate</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Will Travel</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Follow Up</th>';
			headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Skill Set</th>';
			headingsRow += '</tr>';
			
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var candidate = data.returnVal.rows[i];
				if (candidate.p1comments == "New") {
					newCandidate += '<tr>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.id+'</td>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.priority+'</td>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.firstName+'</td>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.lastName+'</td>';
					if (candidate.availability != '' && candidate.availability != null)
						newCandidate += '<td style="border:1px solid black;">'+Ext.Date.format(new Date(candidate.availability),'m/d/Y')+'</td>';
					else
						newCandidate += '<td style="border:1px solid black;"></td>';
					if (candidate.startDate != '' && candidate.startDate != null)
						newCandidate += '<td style="border:1px solid black;">'+Ext.Date.format(new Date(candidate.startDate),'m/d/Y')+'</td>';	
					else
						newCandidate += '<td style="border:1px solid black;"></td>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.emailId+'</td>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.contactNumber+'</td>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.currentJobTitle+'</td>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.relocate+'</td>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.travel+'</td>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.followUp+'</td>';
					newCandidate += '<td style="border:1px solid black;">'+candidate.skillSet+'</td>';
					newCandidate += '</tr>';
					newCount = newCount+1;
				}else if (candidate.p1comments == "Active") {
					activeCandidate += '<tr>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.id+'</td>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.priority+'</td>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.firstName+'</td>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.lastName+'</td>';
					if (candidate.availability != '' && candidate.availability != null)
						activeCandidate += '<td style="border:1px solid black;">'+Ext.Date.format(new Date(candidate.availability),'m/d/Y')+'</td>';
					else
						activeCandidate += '<td style="border:1px solid black;"></td>';
					if (candidate.startDate != '' && candidate.startDate != null)
						activeCandidate += '<td style="border:1px solid black;">'+Ext.Date.format(new Date(candidate.startDate),'m/d/Y')+'</td>';	
					else
						activeCandidate += '<td style="border:1px solid black;"></td>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.emailId+'</td>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.contactNumber+'</td>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.currentJobTitle+'</td>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.relocate+'</td>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.travel+'</td>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.followUp+'</td>';
					activeCandidate += '<td style="border:1px solid black;">'+candidate.skillSet+'</td>';
					activeCandidate += '</tr>';
					activeCount = activeCount+1;
				}else if (candidate.p1comments == "Available") {
					availableCandidate += '<tr>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.id+'</td>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.priority+'</td>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.firstName+'</td>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.lastName+'</td>';
					if (candidate.availability != '' && candidate.availability != null)
						availableCandidate += '<td style="border:1px solid black;">'+Ext.Date.format(new Date(candidate.availability),'m/d/Y')+'</td>';
					else
						availableCandidate += '<td style="border:1px solid black;"></td>';
					if (candidate.startDate != '' && candidate.startDate != null)
						availableCandidate += '<td style="border:1px solid black;">'+Ext.Date.format(new Date(candidate.startDate),'m/d/Y')+'</td>';	
					else
						availableCandidate += '<td style="border:1px solid black;"></td>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.emailId+'</td>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.contactNumber+'</td>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.currentJobTitle+'</td>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.relocate+'</td>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.travel+'</td>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.followUp+'</td>';
					availableCandidate += '<td style="border:1px solid black;">'+candidate.skillSet+'</td>';
					availableCandidate += '</tr>';
					availableCount = availableCount+1;
				}
			}
			
			var table = '<table width="100%" style="border:1px solid black;border-collapse:collapse;" >'+headingsRow
						+'<tr><td height="25" colSpan="13" style="background-color:#f3f3f3;font-weight:bold;border:1px solid black;">New Candidates : '+newCount+'</td></tr>'+newCandidate
						+'<tr><td height="25" colSpan="13" style="background-color:#f3f3f3;font-weight:bold;border:1px solid black;">Active Candidates : '+activeCount+'</td></tr>'+activeCandidate
						+'<tr><td height="25" colSpan="13" style="background-color:#f3f3f3;font-weight:bold;border:1px solid black;">Candidates Becoming available in next 45 days : '+availableCount+
						'</td></tr>'+availableCandidate+'</table>';

			var tpl = new Ext.XTemplate(
		    		table
		        );
	      	this.dataView.tpl = tpl;
	      	this.dataView.refresh();
	      	app.loadMask.hide();
		} 
	},
	
	loadCertifications : function() {
		var values = this.searchPanel.getValues();
    	var params = new Array();
    	params.push(['candidateName','=', values.candidate]);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.certificationStore.loadByCriteria(filter);
    	
        ds.certificationStore.on('load', function(store, records, options) {
        	this.onLoadCertifications(records);
       	}, this);

	},
	
	onLoadCertifications : function(records) {
		app.loadMask.show();
		var certifications = '', headingsRow='';
		
		headingsRow += '<tr>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Candidate</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Unique Id</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Date Achieved</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Certification Name</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Status</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Sponsored By</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Reimbursement Status</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Version</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Comments</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Epic User Web</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Latest NVT</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Expenses Paid or not</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Payment to Trainer</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Epic Invoice No</th>';
		headingsRow += '<th height="25" style="background-color:#FFFF99;font-weight:bold;border:1px solid black;">Expense Comments</th>';
		headingsRow += '</tr>';
		
		for ( var i = 0; i < records.length; i++) {
			var record = records[i].data;
			certifications += '<tr>';
			
			certifications += '<td style="border:1px solid black;">'+record.candidate+'</td>';
			certifications += '<td style="border:1px solid black;">'+record.id+'</td>';
			if (record.dateAchieved != '' && record.dateAchieved != null)
				certifications += '<td style="border:1px solid black;">'+Ext.Date.format(new Date(record.dateAchieved),'m/d/Y')+'</td>';
			else
				certifications += '<td style="border:1px solid black;"></td>';
			certifications += '<td style="border:1px solid black;">'+record.name+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.status+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.sponsoredBy+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.reimbursementStatus+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.version+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.certificationComments+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.epicUserWeb+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.latestNVT+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.expensesStatus+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.paymentDetails+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.epicInvoiceNo+'</td>'; 
			certifications += '<td style="border:1px solid black;">'+record.expenseComments+'</td>'; 
			certifications += '</tr>';
		}
		var table = '<table width="100%" style="border:1px solid black;border-collapse:collapse;" >'+headingsRow+certifications+'</table>';
		var tpl = new Ext.XTemplate(
	    		table
	        );
      	this.dataView.tpl = tpl;
      	this.dataView.refresh();
      	app.loadMask.hide();
	},
	
	loadSubmission : function name() {
		var params ='';
		if (this.searchPanel.getValues().dateFrom != undefined && this.searchPanel.getValues().dateFrom != ''){
			var dateFrom = Ext.Date.format(new Date(this.searchPanel.getValues().dateFrom) , 'Y-m-d')
			params += " and submittedDate >= '"+dateFrom+"'";
		}else
			params += " and submittedDate > date_add(date_sub(last_day(now()),interval 2 month),interval 1 day)";
		
		if (this.searchPanel.getValues().dateTo != undefined && this.searchPanel.getValues().dateTo != ''){
			var dateTo = Ext.Date.format(new Date(this.searchPanel.getValues().dateTo) , 'Y-m-d')
			params += " and submittedDate <= '"+dateTo+"'";
		}else
			params += " and submittedDate <= last_day(now())";
		
		var query = "select r.id,date_format(r.postingDate,'%m/%d/%Y'),date_format(s.submittedDate,'%m/%d/%Y %H:%i:%s'),r.jobOpeningStatus,r.location,r.module,r.hotness,submittedRate1," +
				" (select name from recruit_clients where id =r.clientId) client,(select name from recruit_clients where id =s.vendorId) vendor," +
				" (select concat(firstName,' ',lastName) from recruit_candidate where id =s.candidateId) candidate,if(date(submittedDate)=date(now()),'Today','Yesterday') day," +
				" 0 count, CAST(TIME_FORMAT(SEC_TO_TIME((TIME_TO_SEC(timediff(submittedDate,postingDate)))),'%H:%i') AS char) time,r.requirement,r.postingTitle,submittedRate2" +
				" from recruit_requirements r,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false and s.requirementId=r.id and (date(submittedDate) = date(curdate()) " +
				" or if(DAYOFWEEK(now()) = 2,date(submittedDate) = date(date_sub(now(),interval 3 day)),date(submittedDate) = date(date_sub(now(),interval 1 day))) )" +
				" union" +
				" select id,date_format(postingDate,'%m/%d/%Y'),date_format(submittedDate,'%m/%d/%Y'),'','','','','','','','','',count(id),SEC_TO_TIME(avg(time)),'','','' from (" +
				" SELECT id,postingDate,submittedDate,(TIME_TO_SEC(timediff(submittedDate,postingDate))) time FROM (" +
				" SELECT r.id,postingDate,s.submittedDate FROM recruit_requirements r,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false " +
				"and s.requirementId=r.id "+ params+" )tab1 where submittedDate is not null and postingDate is not null)tab2 group by date(submittedDate)" +
				" union " +
				" select * from (select r.id,date_format(r.postingDate,'%m/%d/%Y') postingDate,'' submittedDate,r.jobOpeningStatus,r.location,r.module,r.hotness,'' submittedRate1, " +
				" (select name from recruit_clients where id =r.clientId) client, " +
				" (select group_concat(c.name) from recruit_clients c,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false" +
				" and c.id=s.vendorId and s.requirementId =r.id) vendor, " +
				" (select group_concat(concat(firstName,' ',lastName)) from recruit_candidate c,recruit_sub_requirements s where s.submissionStatus != 'Shortlisted' and s.deleted = false" +
				" and c.id=s.candidateId and s.requirementId =r.id) candidate," +
				" 'Open',0,'',r.requirement,r.postingTitle,'' submittedRate2 from recruit_requirements r where r.hotness in ('00','01') and datediff(now(),r.postingDate) <=30 " +
				" order by r.postingDate DESC,hotness desc )tab";
		
		app.reportsService.loadSubmissionReport(query,this.onGetSubmission,this);
		
	},
	
	onGetSubmission : function(data) {
		if (data.success) {
			var todayTable='', yesterdayTable ='',openTable ='', openSubmittedTable ='';
			var values = new Array();
			var dates = new Array();
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var record = data.returnVal.rows[i];
				if (record.day=='Today') {
					todayTable += "<tr bgcolor='#ffffff'><td>"+record.id+"</td>" +
									"<td>"+record.postingDate+"</td>"+
									"<td>"+record.submittedDate+"</td>"+
									"<td>"+record.time+" Hrs</td>"+
									"<td>"+record.jobOpeningStatus+"</td>"+
									"<td>"+record.location+"</td>"+
									"<td>"+record.client+"</td>"+
									"<td>"+record.vendor+"</td>"+
									"<td>"+record.candidate+"</td>"+
									"<td>"+record.submittedRate1+"</td>"+
									"<td>"+record.submittedRate2+"</td>"+
									"<td>"+record.module+"</td>"+
									"<td>"+record.hotness+"</td></tr>";
				}else if (record.day=='Yesterday'){
					yesterdayTable += "<tr bgcolor='#ffffff'><td>"+record.id+"</td>" +
									"<td>"+record.postingDate+"</td>"+
									"<td>"+record.submittedDate+"</td>"+
									"<td>"+record.time+" Hrs</td>"+
									"<td>"+record.jobOpeningStatus+"</td>"+
									"<td>"+record.location+"</td>"+
									"<td>"+record.client+"</td>"+
									"<td>"+record.vendor+"</td>"+
									"<td>"+record.candidate+"</td>"+
									"<td>"+record.submittedRate1+"</td>"+
									"<td>"+record.submittedRate2+"</td>"+
									"<td>"+record.module+"</td>"+
									"<td>"+record.hotness+"</td></tr>";
				}else if (record.day=='Open') {
					if (record.client == null || record.client == 'null')
						record.client = '';
					if (record.vendor == null || record.vendor == 'null')
						record.vendor = '';
					if (record.candidate == null || record.candidate == 'null')
						record.candidate = '';
					
					if (record.jobOpeningStatus == '03.Submitted') {
						openSubmittedTable += "<tr bgcolor='#ffffff'><td>"+record.id+"</td>" +
												"<td>"+record.hotness+"</td>"+
												"<td>"+record.postingDate+"</td>"+
												"<td>"+record.postingTitle+"</td>"+
												"<td>"+record.jobOpeningStatus+"</td>"+
												"<td>"+record.location+"</td>"+
												"<td>"+record.module+"</td>"+
												"<td>"+record.requirement.substr(0,99)+"</td>"+
												"<td>"+record.client+"</td>"+
												"<td>"+record.vendor+"</td>"+
												"<td>"+record.candidate+"</td></tr>";
					}else{
						openTable += "<tr bgcolor='#ffffff'><td>"+record.id+"</td>" +
						"<td>"+record.hotness+"</td>"+
						"<td>"+record.postingDate+"</td>"+
						"<td>"+record.postingTitle+"</td>"+
						"<td>"+record.jobOpeningStatus+"</td>"+
						"<td>"+record.location+"</td>"+
						"<td>"+record.module+"</td>"+
						"<td>"+record.requirement.substr(0,99)+"</td>"+
						"<td>"+record.client+"</td>"+
						"<td>"+record.vendor+"</td>"+
						"<td>"+record.candidate+"</td></tr>";
					}
					
				}else{
					values.push([record.submittedDate,record.count,record.time])
					dates.push(record.submittedDate);
				}
			}
			
			var headings = "<tr ><td style='background-color:#dddddd;font-weight:bold;'>Unique Id</td>" +
							"<td style='background-color:#dddddd;font-weight:bold;'>Posting Date</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Submitted Date</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Turnaround Time</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Job Opening Status</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Location</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Client</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Vendor</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Candidate</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Submitted Rate-1</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Submitted Rate-2</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Module</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Hotness</td></tr>";

			var openHeadings = "<tr><td style='background-color:#dddddd;font-weight:bold;'>Unique Id</td>" +
							"<td style='background-color:#dddddd;font-weight:bold;'>Hotness</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Posting Date</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Posting Title</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Job Opening Status</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Location</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Module</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Roles and Responsibilities</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Client</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Vendor</td>"+
							"<td style='background-color:#dddddd;font-weight:bold;'>Candidate</td></tr>";

			if (todayTable != "")
				todayTable = "<br><table cellpadding='5' bgcolor='#575252'>"+headings+todayTable+"</table>";
			else
				todayTable = "N/A<br>";
			
			if (yesterdayTable != "")
				yesterdayTable = "<br><table cellpadding='5' bgcolor='#575252'>"+headings+yesterdayTable+"</table>";
			else
				yesterdayTable = "N/A<br>";
			
			if (openTable != "") 
				openTable = "<br><table cellpadding='5' bgcolor='#575252'>"+openHeadings+openTable+"</table>";
			else
				openTable = "N/A<br>";

			if (openSubmittedTable != "") 
				openSubmittedTable = "<br><table cellpadding='5' bgcolor='#575252'>"+openHeadings+openSubmittedTable+"</table>";
			
			var table ='';
			
			if (this.searchPanel.getValues().dateFrom != undefined && this.searchPanel.getValues().dateFrom != ''){
				var dateFrom = new Date(this.searchPanel.getValues().dateFrom);
			}else
				var dateFrom = new Date((new Date()).setMonth((new Date()).getMonth() - 1));
			
			if (this.searchPanel.getValues().dateTo != undefined && this.searchPanel.getValues().dateTo != '') {
				var dateTo = new Date(this.searchPanel.getValues().dateTo);
			}else
				dateTo = new Date();

			
			while (true){
				if (dateFrom.getFullYear() > dateTo.getFullYear()) 
					break;
				else if (dateFrom.getFullYear() == dateTo.getFullYear() && dateFrom.getMonth() > dateTo.getMonth() )
					break;
					
				table += monthlyCalender(dateTo, values, dates);
				table += "<br>";
				dateTo = new Date(dateTo.getFullYear(),dateTo.getMonth()-1,1);
				
			}
			
			// if dates are given today, yesterday and Open Job openings will not be shown
			if ((this.searchPanel.getValues().dateFrom == null || this.searchPanel.getValues().dateFrom == '') && (this.searchPanel.getValues().dateTo == null || this.searchPanel.getValues().dateTo == ''))
				table = '<b>Today : </b>'+todayTable + '<br><b>Yesterday : </b>'+yesterdayTable+'<br>'+table +'<br><b>Open Job openings in the last 30 days: </b>'+ openSubmittedTable +'<br>' +openTable;
			
			var tpl = new Ext.XTemplate(
		    	table
			);
	      	this.dataView.tpl = tpl;
	      	this.dataView.refresh();
		}
	},
	
	toNearestHour : function(time) {
    	var hours = Number(time.split(":")[0]);
    	var minutes = Number(time.split(":")[1]);
        if (minutes >= 30)
        	hours++;
        return hours;
    },
    
    loadMissingData : function() {
		var table = this.searchPanel.form.findField('table').getValue();
		if (table == 'Job Openings') {
			var query = "select 'All' field,count(*) count from recruit_requirements union " +
					" select * from ( " +
					" select 'Client' field,count(*) count from recruit_requirements where ISNULL(clientId) or clientId = '' or clientId='n/a' " +
					" union select 'Posting Date',count(*) from recruit_requirements where ISNULL(postingDate) " +
					" union select 'City & State',count(*) from recruit_requirements where ISNULL(cityAndState) or cityAndState = '' or cityAndState='n/a'" +
					" union select 'Location',count(*) from recruit_requirements where ISNULL(location) or location = '' or location='n/a'" +
					" union select 'Roles & Responsibilities',count(*) from recruit_requirements where ISNULL(requirement) or requirement = '' or requirement='n/a'" +
					" union select 'Hotness',count(*) from recruit_requirements where ISNULL(hotness) or hotness = '' or hotness='n/a'" +
					" union select 'Module',count(*) from recruit_requirements where ISNULL(module) or module = '' or module='n/a'" +
					" union select 'Communication',count(*) from recruit_requirements where ISNULL(communication) or communication = '' or communication='n/a'" +
					" union select 'Alert',count(*) from recruit_requirements where ISNULL(addInfo_Todos) or addInfo_Todos = '' or addInfo_Todos='n/a'" +
					" union select 'Source',count(*) from recruit_requirements where ISNULL(source) or source = '' or source='n/a'" +
					" union select 'Max Bill Rate',count(*) from recruit_requirements where ISNULL(rateSubmitted) or rateSubmitted = '' or rateSubmitted='n/a'" +
					" union select 'Posting Title',count(*) from recruit_requirements where ISNULL(postingTitle) or postingTitle = '' or postingTitle='n/a'" +
					" union select 'Job Opening Status',count(*) from recruit_requirements where ISNULL(jobOpeningStatus) or jobOpeningStatus = '' or jobOpeningStatus='n/a'" +
					" union select 'Comments',count(*) from recruit_requirements where ISNULL(comments) or comments = '' or comments='n/a'" +
					" union select 'Employer',count(*) from recruit_requirements where ISNULL(employer) or employer = '' or employer='n/a'" +
					" union select 'PublicLink',count(*) from recruit_requirements where ISNULL(publicLink) or publicLink = '' or publicLink='n/a'" +
					" union select 'Research Comments',count(*) from recruit_requirements where ISNULL(researchComments) or researchComments = '' or researchComments='n/a'" +
					" union select 'Resume',count(*) from recruit_requirements where ISNULL(resume) " +
					" union select 'Experience',count(*) from recruit_requirements where ISNULL(experience) or experience = '' or experience='n/a'" +
					" union select 'Ok to train candidate',count(*) from recruit_requirements where ISNULL(helpCandidate) or helpCandidate = '' or helpCandidate='n/a'" +
					" union select 'Ok to train candidate Comments',count(*) from recruit_requirements where ISNULL(helpCandidateComments) or helpCandidateComments = '' or helpCandidateComments='n/a'" +
					" union select 'Skill Set',count(*) from recruit_requirements where ISNULL(skillSet) or skillSet = '' or skillSet='n/a'" +
					" union select 'Target Roles',count(*) from recruit_requirements where ISNULL(targetRoles) or targetRoles = '' or targetRoles='n/a'" +
					" union select 'Mandatory Certifications',count(*) from recruit_requirements where ISNULL(certifications) or certifications = '' or certifications='n/a'" +
					" union select 'Remote',count(*) from recruit_requirements where ISNULL(remote) " +
					" union select 'Relocate',count(*) from recruit_requirements where ISNULL(relocate) " +
					" union select 'Travel',count(*) from recruit_requirements where ISNULL(travel) " +
					" union select 'T&E Paid',count(*) from recruit_requirements where ISNULL(tAndEPaid) " +
					" union select 'T&E Not Paid',count(*) from recruit_requirements where ISNULL(tAndENotPaid) " +
					" order by count desc) tab";
			
		}else if (table == "Candidates") {
			var query = "select 'All' field,count(*) count from recruit_candidate union " +
					" select * from (" +
					" select 'First Name' field,count(*) count from recruit_candidate where ISNULL(firstName) or firstName = '' or firstName='n/a'" +
					" union select 'Last Name',count(*) from recruit_candidate where ISNULL(lastName) or lastName = '' or lastName='n/a'" +
					" union select 'Email Id',count(*) from recruit_candidate where ISNULL(emailId) or emailId = '' or emailId='n/a'" +
					" union select 'Contact Number',count(*) from recruit_candidate where ISNULL(contactNumber) or contactNumber = '' or contactNumber='n/a'" +
					" union select 'Contact Address',count(*) from recruit_candidate where ISNULL(contactAddress) or contactAddress = '' or contactAddress='n/a'" +
					" union select 'Priority',count(*) from recruit_candidate where ISNULL(priority)" +
					" union select 'Current Job Title',count(*) from recruit_candidate where ISNULL(currentJobTitle) or currentJobTitle = '' or currentJobTitle='n/a'" +
					" union select 'Type',count(*) from recruit_candidate where ISNULL(type) or type = '' or type='n/a'" +
					" union select 'P1 comments',count(*) from recruit_candidate where ISNULL(p1comments) or p1comments = '' or p1comments='n/a'" +
					" union select 'P2 comments',count(*) from recruit_candidate where ISNULL(p2comments) or p2comments = '' or p2comments='n/a'" +
					" union select 'Employer',count(*) from recruit_candidate where ISNULL(employer) or employer = '' or employer='n/a'" +
					" union select 'Highest Qualification',count(*) from recruit_candidate where ISNULL(highestQualification) or highestQualification = '' or highestQualification='n/a'" +
					" union select 'Referral',count(*) from recruit_candidate where ISNULL(referral) or referral = '' or referral='n/a'"+
					" union select 'Marketing Status',count(*) from recruit_candidate where ISNULL(marketingStatus) or marketingStatus = '' or marketingStatus='n/a'"+
					" union select 'Availability',count(*) from recruit_candidate where ISNULL(availability) or availability = '' or availability='n/a'"+
					" union select 'Source',count(*) from recruit_candidate where ISNULL(source) or source = '' or source='n/a'"+
					" union select 'Relocate',count(*) from recruit_candidate where ISNULL(relocate) "+
					" union select 'Travel',count(*) from recruit_candidate where ISNULL(travel) "+
					" union select 'Good Candidate for Remote',count(*) from recruit_candidate where ISNULL(goodCandidateForRemote) "+
					" union select 'City & State',count(*) from recruit_candidate where ISNULL(cityAndState) or cityAndState = '' or cityAndState='n/a'"+
					" union select 'Follow Up',count(*) from recruit_candidate where ISNULL(followUp) or followUp = '' or followUp='n/a'"+
					" union select 'Education',count(*) from recruit_candidate where ISNULL(education) or education = '' or education='n/a'"+
					" union select 'Immigration Status',count(*) from recruit_candidate where ISNULL(immigrationStatus) or immigrationStatus = '' or immigrationStatus='n/a'"+
					" union select 'Immigration Verified',count(*) from recruit_candidate where ISNULL(immigrationVerified) or immigrationVerified = '' or immigrationVerified='n/a'"+
					" union select 'Open to CTH',count(*) from recruit_candidate where ISNULL(openToCTH) or openToCTH = '' or openToCTH='n/a'"+
					" union select 'Start Date',count(*) from recruit_candidate where ISNULL(startDate) "+
					" union select 'Employment Type',count(*) from recruit_candidate where ISNULL(employmentType) or employmentType = '' or employmentType='n/a'"+
					" union select 'Expected Rate',count(*) from recruit_candidate where ISNULL(expectedRate) or expectedRate = '' or expectedRate='n/a'"+
					" union select 'About Partner',count(*) from recruit_candidate where ISNULL(aboutPartner) or aboutPartner = '' or aboutPartner='n/a'"+
					" union select 'Personality',count(*) from recruit_candidate where ISNULL(personality) or personality = '' or personality='n/a'"+
					" union select 'Alert',count(*) from recruit_candidate where ISNULL(alert) or alert = '' or alert='n/a'"+
					" union select 'Confidentiality',count(*) from recruit_candidate where ISNULL(confidentiality) "+
					" union select 'Rate From',count(*) from recruit_candidate where ISNULL(rateFrom) "+
					" union select 'Rate To',count(*) from recruit_candidate where ISNULL(rateTo) "+
					" union select 'Location',count(*) from recruit_candidate where ISNULL(location) or location = '' or location='n/a'"+
					" union select 'Communication',count(*) from recruit_candidate where ISNULL(communication) or communication = '' or communication='n/a'"+
					" union select 'Experience',count(*) from recruit_candidate where ISNULL(experience) or experience = '' or experience='n/a'"+
					" union select 'TargetRoles',count(*) from recruit_candidate where ISNULL(targetRoles) or targetRoles = '' or targetRoles='n/a'"+
					" union select 'SkillSet',count(*) from recruit_candidate where ISNULL(skillSet) or skillSet = '' or skillSet='n/a'"+
					" union select 'Target SkillSet',count(*) from recruit_candidate where ISNULL(targetSkillSet) or targetSkillSet = '' or targetSkillSet='n/a'"+
					" union select 'Current Roles',count(*) from recruit_candidate where ISNULL(currentRoles) or currentRoles = '' or currentRoles='n/a'"+
					" union select 'ResumeHelp',count(*) from recruit_candidate where ISNULL(resumeHelp) or resumeHelp = '' or resumeHelp='n/a'"+
					" union select 'Interview Help',count(*) from recruit_candidate where ISNULL(interviewHelp) or interviewHelp = '' or interviewHelp='n/a'"+
					" union select 'Job Help',count(*) from recruit_candidate where ISNULL(jobHelp) or jobHelp = '' or jobHelp='n/a'"+
					" union select 'Resume Help Comments',count(*) from recruit_candidate where ISNULL(resumeHelpComments) or resumeHelpComments = '' or resumeHelpComments='n/a'"+
					" union select 'Interview Help Comments',count(*) from recruit_candidate where ISNULL(interviewHelpComments) or interviewHelpComments = '' or interviewHelpComments='n/a'"+
					" union select 'Job Help Comments',count(*) from recruit_candidate where ISNULL(jobHelpComments) or jobHelpComments = '' or jobHelpComments='n/a'"+
					" union select 'Other SkillSet',count(*) from recruit_candidate where ISNULL(otherSkillSet) or otherSkillSet = '' or otherSkillSet='n/a' " +
					" order by count desc )tab";
		}else if (table == "Clients/Vendors") {
			var query = "select 'All' field,count(*) count from recruit_clients union" +
					" select * from (" +
					" select 'Name' field, count(*) count from recruit_clients where ISNULL(name) or name = '' or name='n/a'" +
					" union select 'Contact Number', count(*) from recruit_clients where ISNULL(contactNumber) or contactNumber = '' or contactNumber='n/a'" +
					" union select 'Fax', count(*) from recruit_clients where ISNULL(fax) or fax = '' or fax='n/a'" +
					" union select 'Website', count(*) from recruit_clients where ISNULL(website) or website = '' or website='n/a'" +
					" union select 'Email', count(*) from recruit_clients where ISNULL(email) or email = '' or email='n/a'" +
					" union select 'Industry', count(*) from recruit_clients where ISNULL(industry) or industry = '' or industry='n/a'" +
					" union select 'About', count(*) from recruit_clients where ISNULL(about) or about = '' or about='n/a'" +
					" union select 'Type', count(*) from recruit_clients where ISNULL(type) or type = '' or type='n/a'" +
					" union select 'Address', count(*) from recruit_clients where ISNULL(address) or address = '' or address='n/a'" +
					" union select 'Comments', count(*) from recruit_clients where ISNULL(comments) or comments = '' or comments='n/a'" +
					" union select 'Score', count(*) from recruit_clients where ISNULL(score) or score = '' or score='n/a'" +
					" union select 'State', count(*) from recruit_clients where ISNULL(state) or state = '' or state='n/a'" +
					" union select 'City', count(*) from recruit_clients where ISNULL(city) or city = '' or city='n/a'" +
					" union select 'Confidentiality', count(*) from recruit_clients where ISNULL(confidentiality) or confidentiality = '' or confidentiality='n/a'" +
					" union select 'ReferenceId', count(*) from recruit_clients where ISNULL(referenceId) or referenceId = '' or referenceId='n/a'" +
					" union select 'Ok to train candidate', count(*) from recruit_clients where ISNULL(helpCandidate) or helpCandidate = '' or helpCandidate='n/a'" +
					" union select 'Ok to train candidate Comments', count(*) from recruit_clients where ISNULL(helpCandidateComments) or helpCandidateComments = '' or helpCandidateComments='n/a'" +
					" order by count desc )tab";
		}else if (table == 'Contacts') {
			var query = "select 'All' field,count(*) count from recruit_contacts union" +
					" select * from (" +
					" select 'Client/Vendor' ,count(*) count from recruit_contacts where ISNULL(clientId) or clientId = '' or clientId='n/a'" +
					" union select 'First Name',count(*) from recruit_contacts where ISNULL(firstName) or firstName = '' or firstName='n/a'" +
					" union select 'Last Name',count(*) from recruit_contacts where ISNULL(lastName) or lastName = '' or lastName='n/a'" +
					" union select 'Email',count(*) from recruit_contacts where ISNULL(email) or email = '' or email='n/a'" +
					" union select 'Job Title',count(*) from recruit_contacts where ISNULL(jobTitle) or jobTitle = '' or jobTitle='n/a'" +
					" union select 'Work Phone',count(*) from recruit_contacts where ISNULL(workPhone) or workPhone = '' or workPhone='n/a'" +
					" union select 'Internet Link',count(*) from recruit_contacts where ISNULL(internetLink) or internetLink = '' or internetLink='n/a'" +
					" union select 'Cell Phone',count(*) from recruit_contacts where ISNULL(cellPhone) or cellPhone = '' or cellPhone='n/a'" +
					" union select 'Comments',count(*) from recruit_contacts where ISNULL(comments) or comments = '' or comments='n/a'" +
					" union select 'Extension',count(*) from recruit_contacts where ISNULL(extension) or extension = '' or extension='n/a'" +
					" union select 'Score',count(*) from recruit_contacts where ISNULL(score) or score = '' or score='n/a'" +
					" union select 'Confidentiality',count(*) from recruit_contacts where ISNULL(confidentiality)" +
					" union select 'Type',count(*) from recruit_contacts where ISNULL(type) or type = '' or type='n/a'" +
					" order by count desc )tab";
		}else if (table == 'Interviews') {
			var query = "select 'All' field,count(*) count from recruit_interviews union" +
					" select * from (" +
					" select 'Candidate',count(*) count from recruit_interviews where ISNULL(candidateId) " +
					" union select 'Client',count(*) from recruit_interviews where ISNULL(clientId) " +
					" union select 'Vendor',count(*) from recruit_interviews where ISNULL(vendorId) " +
					" union select 'Contact',count(*) from recruit_interviews where ISNULL(contactId) " +
					" union select 'Job Id',count(*) from recruit_interviews where ISNULL(requirementId) " +
					" union select 'Interviewer',count(*) from recruit_interviews where ISNULL(interviewer) or interviewer = '' or interviewer='n/a'" +
					" union select 'InterviewDate',count(*) from recruit_interviews where ISNULL(interviewDate)" +
					" union select 'Time',count(*) from recruit_interviews where ISNULL(time) or time = '' or time='n/a'" +
					" union select 'Local Time',count(*) from recruit_interviews where ISNULL(currentTime) or currentTime = '' or currentTime='n/a'" +
					" union select 'Time Zone',count(*) from recruit_interviews where ISNULL(timeZone) or timeZone = '' or timeZone='n/a'" +
					" union select 'Status',count(*) from recruit_interviews where ISNULL(status) or status = '' or status='n/a'" +
					" union select 'Comments',count(*) from recruit_interviews where ISNULL(comments) or comments = '' or comments='n/a'" +
					" union select 'Interview',count(*) from recruit_interviews where ISNULL(interview) or interview = '' or interview='n/a'" +
					" union select 'Approx. Start Date',count(*) from recruit_interviews where ISNULL(approxStartDate) or approxStartDate = '' or approxStartDate='n/a'" +
					" order by count desc )tab";
		}
		app.reportsService.getMissingData(query,this.onGetResult,this);

    },
    
    loadCandidatesStatistics : function() {
		var query = "select * from ( " +
				"select name,type,count(if(priority=0,1,null)) P0Candidates,count(if(priority=1,1,null)) P1Candidates,count(if(priority=2,1,null)) P2Candidates from (" +
				"select distinct s.name,s.type,c.id,c.priority from recruit_settings s,recruit_candidate c " +
				"where find_in_set(s.name,c.skillset) and c.priority in (0,1,2) and s.type ='Skill Set/Target Skill Set'" +
				")a group by name order by if(name like 'Epic%',0,1) ,P0Candidates desc,P1Candidates desc,P2Candidates desc" +
				")a where P0Candidates > 0 or P1Candidates > 0 or P2Candidates > 0" +
				" union " +
				"select * from (" +
				"select name,type,count(if(priority=0,1,null)) P0Candidates,count(if(priority=1,1,null)) P1Candidates,count(if(priority=2,1,null)) P2Candidates from (" +
				"select distinct s.name,s.type,c.id,c.priority from recruit_settings s,recruit_candidate c " +
				"where find_in_set(s.name,c.currentRoles) and c.priority in (0,1,2) and s.type ='Target Roles'" +
				")a group by name order by if(name like 'Epic%',0,1) ,P0Candidates desc,P1Candidates desc,P2Candidates desc" +
				")a where P0Candidates > 0 or P1Candidates > 0 or P2Candidates > 0 ";
		
    	var params = new Array();
    	params.push(['query','=', query]);
    	params.push(['tableWidth','=', '50%']);
    	var filter = getFilter(params);
		app.reportsService.getStatistics(Ext.JSON.encode(filter),this.onLoadCandidatesStatistics,this);
	},
    
	onLoadCandidatesStatistics : function(data) {
		if (data.success && data.returnVal.rows.length > 0) {
			var table1 = '', table2 = '';
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var record = data.returnVal.rows[i];
				if(record.value1 == "Skill Set/Target Skill Set"){
					table1 += "<tr bgcolor='#ffffff'><td>"+record.value0+"</td>" +
							"<td align='center'>"+record.value2+"</td>" +
							"<td align='center'>"+record.value3+"</td>" +
							"<td align='center'>"+record.value4+"</td></tr>" ;
				}else if (record.value1 == "Target Roles") {
					table2 += "<tr bgcolor='#ffffff'><td>"+record.value0+"</td>" +
							"<td align='center'>"+record.value2+"</td>" +
							"<td align='center'>"+record.value3+"</td>" +
							"<td align='center'>"+record.value4+"</td></tr>" ;
				}
			}
			if (table1 != '') {
				table1 = "<table cellpadding='5' bgcolor='#575252'>" +
						"<tr bgcolor='#dddddd'><th>Skill set</th><th>P0 Candidates</th><th>P1 Candidates</th><th>P2 Candidates</th>" +
						table1 + "</table>";
			}
			if (table2 != '') {
				table2 = "<table cellpadding='5' bgcolor='#575252'>" +
						"<tr bgcolor='#dddddd'><th>Role set</th><th>P0 Candidates</th><th>P1 Candidates</th><th>P2 Candidates</th></tr>" +
						table2 + "</table>";
			}
			var table = '<table width="100%"><tr><td align="right"><button onclick="javascript:app.reportsMgmtPanel.reportsDetailPanel.exportExcel(\'SC\')">'+
						'<img src="images/icon_excel.gif" alt=""> Export to Excel</button></td></tr></table>'+
						"<table><tbody id= 'SC'><tr><td valign='top'>"+table1+"</td><td valign='top'>"+table2+"</td></tr></tbody><table>"
			var tpl = new Ext.XTemplate(
					table
			);
			this.dataView.tpl = tpl;
	      	this.dataView.refresh();
		}
	},
	
    loadVendorStatistics : function() {
		var query = "select score Priority,name Vendor,count(distinct requirementId) Reqs,count(id) Subs," +
				"count(if(submissionStatus in ('C-Accepted','C-Declined','I-Failed','I-Scheduled','I-Succeeded','I-Withdrawn'),1,null)) Interviews," +
				"count(if(submissionStatus='C-Accepted',1,null)) Placements from (" +
				"select v.name,v.score ,s.* from recruit_sub_requirements s,recruit_clients v where s.vendorId=v.id" +
				") tab where 1=1  ";

		var values = this.searchPanel.getValues();
		if (values.vendor != null && values.vendor != '')
			query += " and name like '%"+values.vendor+"%' ";
		query += " group by name order by Reqs Desc,priority,name";
		var params = new Array();
		params.push(['query','=', query]);
		params.push(['tableWidth','=', '60%']);
		var filter = getFilter(params);
		app.reportsService.getStatistics(Ext.JSON.encode(filter),this.onLoadVendorStatistics,this);
    },
    
    onLoadVendorStatistics : function(data) {
		if (data.success && data.returnVal.rows.length > 0) {
			var table = '', table2 = '';
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var record = data.returnVal.rows[i];
				table += "<tr bgcolor='#ffffff'><td align='center'>"+record.value0+"</td>" +
							"<td>"+record.value1+"</td>" +
							"<td align='center'>"+record.value2+"</td>" +
							"<td align='center'>"+record.value3+"</td>" +
							"<td align='center'>"+record.value4+"</td>" +
							"<td align='center'>"+record.value5+"</td></tr>" ;
			}
			if (table != '') {
				table = '<table width="100%"><tr><td align="right"><button onclick="javascript:app.reportsMgmtPanel.reportsDetailPanel.exportExcel(\'SV\')">'+
				'<img src="images/icon_excel.gif" alt=""> Export to Excel</button></td></tr></table>'+
				"<table cellpadding='5' bgcolor='#575252'><tbody id= 'SV'>" +
				"<tr bgcolor='#dddddd'><th>Priority</th><th>Vendor</th><th>Reqs</th><th>Subs</th><th>Interviews</th><th>Placements</th></tr>" +
				table + "</tbody></table>";
			}
			var tpl = new Ext.XTemplate(
					table
			);
			this.dataView.tpl = tpl;
	      	this.dataView.refresh();
		}
	},
	
	getReferences : function() {
		var query = "select c.id 'Reference Id',concat(c.firstname,' ',c.lastname) Name,c.email EmailId,cellphone 'Contact No'," +
				" group_concat(concat(c1.firstname,' ',c1.lastname)) Candidates,group_concat(v.name) Vendor from recruit_contacts c,recruit_sub_requirements s," +
				" recruit_candidate c1,recruit_clients v where find_in_set(c.id,s.reference ) > 0 and c1.id=s.candidateId and v.id=s.vendorId" ;

		var values = this.searchPanel.getValues();
		if (values.vendor != null && values.vendor != '')
			query += " and v.name like '%"+values.vendor+"%' ";
		if (values.candidate != null && values.candidate != '')
			query += " and concat(c1.firstname,' ',c1.lastname) like '%"+values.candidate+"%' ";
		query += " group by c.id order by c.firstName";
		app.reportsService.getResult(query,this.onGetResult,this);
	},
	
	getRelated : function() {
		this.searchPanel.form.findField('client').allowBlank = false;
		if (this.searchPanel.form.isValid()) {
			var values = this.searchPanel.getValues();
			var record = ds.client_vendorStore.getAt(ds.client_vendorStore.findExact('name',values.client));
			if (record != null) {
				var query = "";
				if (record.data.type == 'Vendor') {
					query = " select id,name,contactNumber,email,city,state,helpCandidate,score,type,'Submission' relation from recruit_clients " +
							" where id in (select r.clientId FROM recruit_requirements r,recruit_sub_vendors s,recruit_clients c where r.id=s.requirementId" +
							" and r.clientid = c.id and c.name!='n/a' and s.vendorId = "+ record.data.id +") " ;
				}else if (record.data.type == 'Client') {
					query = " select id,name,contactNumber,email,city,state,helpCandidate,score,type,'Submission' relation from recruit_clients " +
							" where id in (select s.vendorId FROM recruit_requirements r,recruit_sub_vendors s,recruit_clients c where r.id=s.requirementId " +
							" and r.clientid = c.id and c.name!='n/a' and r.clientId = "+ record.data.id +")" ;
				}
				query += " union " +
						" select id,name,contactNumber,email,city,state,helpCandidate,score,type,'Related' from recruit_clients " +
						" where find_in_set(id,(select referenceId from recruit_clients where id = "+ record.data.id +"))" +
						" order by relation desc,name" ;

		    	var params = new Array();
		    	params.push(['query','=', query]);
		    	params.push(['tableWidth','=', '50%']);
		    	var filter = getFilter(params);
				app.reportsService.getStatistics(Ext.JSON.encode(filter),this.onGetRelated,this);
			}
		}
	},

	onGetRelated : function(data) {
		if (data.success && data.returnVal.rows.length > 0) {
			var table1 = '', table2 = '';
			var ids = new Array();
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var record = data.returnVal.rows[i];
				if (ids.indexOf(record.value0) == -1) {
					ids.push(record.value0);
					if (record.value6 == null)
						record.value6 = '';
					if (record.value7 == null)
						record.value7 = '';
					var tableRow = "<tr bgcolor='#ffffff'><td>"+record.value1+"</td>" +
									"<td align='center'>"+record.value2+"</td>" +
									"<td align='center'>"+record.value3+"</td>" +
									"<td align='center'>"+record.value4+"</td>" +
									"<td align='center'>"+record.value5+"</td>" +
									"<td align='center'>"+record.value6+"</td>" +
									"<td align='center'>"+record.value7+"</td></tr>" ;

					if(record.value9 == "Submission"){
						table1 += tableRow ;
					}else if (record.value9 == "Related") {
						table2 += tableRow ;
					}
					
				}
			}
			if (table1 != '') {
				table1 = "<b>"+data.returnVal.rows[0].value8+"s from Job openings :</b><br><table cellpadding='5' bgcolor='#575252'>" +
						"<tr bgcolor='#dddddd'><th>Name</th><th>Contact Number</th><th>Email Id</th><th>City</th><th>State</th><th>Ok to train Candidate</th><th>Priority</th>" +
						table1 + "</table>";
			}
			if (table2 != '') {
				table2 = "<br><b>Other Related "+data.returnVal.rows[0].value8+"s :</b><br><table cellpadding='5' bgcolor='#575252'>" +
						"<tr bgcolor='#dddddd'><th>Name</th><th>Contact Number</th><th>Email Id</th><th>City</th><th>State</th><th>Ok to train Candidate</th><th>Priority</th>" +
						table2 + "</table>";
			}
			var table = '<table width="100%"><tr><td align="right"><button onclick="javascript:app.reportsMgmtPanel.reportsDetailPanel.exportExcel(\'RCV\')">'+
						'<img src="images/icon_excel.gif" alt=""> Export to Excel</button></td></tr></table>'+
						"<table><tbody id= 'RCV'><tr><td valign='top'>"+table1+"</td></tr><tr><td valign='top'>"+table2+"</td></tr></tbody><table>"
			var tpl = new Ext.XTemplate(
					table
			);
			this.dataView.tpl = tpl;
	      	this.dataView.refresh();
		}else{
			this.showError("Related Client/Vendors are not found for the given Client/Vendor.");
		}
	},
	
	
	exportExcel : function(tableBody) {
		str="<table border='2px'><tr>";
		
		var myTableBody = document.getElementById(tableBody);
		var rowCount = myTableBody.rows.length;
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE "); 

		for(var i=0; i<rowCount; i++) 
		{   
			str=str+myTableBody.rows[i].innerHTML+"</tr>";
		}	 

		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // for Internet Explorer
		{
		   txtArea1.document.open("txt/html","replace");
		   txtArea1.document.write(str);
		   txtArea1.document.close();
		   txtArea1.focus(); 
		   sa=txtArea1.document.execCommand("SaveAs",true,"Html_to_Excel.xls");
		}  
		else	//other browsers like firefox, Chrome, Safari
		  sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(str));  
                      
	return (sa);
	},
	
	getMarketing : function() {
		var query = "SELECT distinct c.id,c.firstName,c.lastName,group_concat(distinct v.name order by v.name)," +
				"group_concat(distinct v.id order by v.id),group_concat(distinct s.name)," +
				"(select group_concat(skillSetId) from recruit_candidate_skillset where candidateId=c.id ) skillSetIds," +
				"(select group_concat(skillsetgroupId) from recruit_candidate_skillsetgroup where candidateId=c.id ) skillSetGroupIds " +
				"FROM recruit_candidate c,recruit_candidate_skillsetgroup cs,recruit_clients v,recruit_vendor_skillsetgroup vs,recruit_skillsetgroup s " +
				"where c.id=cs.candidateId and v.id=vs.vendorId and cs.skillSetGroupId = vs.skillSetGroupId and vs.skillSetGroupId = s.id " +
				"and cs.skillSetGroupId = s.id and (c.id,v.id) not in (select candidateId,vendorId from recruit_marketingcandidates) " +
				"";

		var values = this.searchPanel.getValues();
		if (values.vendor != null && values.vendor != '')
			query += " and v.name like '%"+values.vendor+"%' ";
		if (values.removeVendor != null && values.removeVendor != ''){
			var vendors = values.removeVendor.split(",");
			var vendorQuery = "";
			for (i = 0; i < vendors.length; i++) {
				vendorQuery += " v.name not like '%"+vendors[i]+"%'";
				if (i+1 < vendors.length)
					vendorQuery += " and ";
			}
			query += " and ("+vendorQuery+")";	
		}
		query += " group by c.id order by firstName";
		
    	var params = new Array();
    	params.push(['query','=', query]);
    	var filter = getFilter(params);
		app.reportsService.getStatistics(Ext.JSON.encode(filter),this.onGetMarketing,this);
	},
	
	onGetMarketing : function(data) {
		if (data.success && data.returnVal.rows.length > 0) {
			var table = '<table width="100%"><tr><td align="right"><button onclick="javascript:app.reportsMgmtPanel.reportsDetailPanel.emailCandidates(\'SV\')">'+
				'<img src="images/email.png" alt=""> Email Candidates</button></td></tr></table>'+
				"<table cellpadding='5' bgcolor='#575252'>" +
				"<col width='10%'> <col width='15%'> <col width='15%'> <col width='15%'> <col width='35%'>" +
				"<tr bgcolor='#FFFF99'><th width='10%'>Candidate Id</th><th width='15%'>Name</th>" +
				"<th width='15%'>Skill Set</th><th width='15%'>Skill Set Group</th><th width='35%'>Vendors</th></tr>" ;

			var candidateIds = new Array();
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var record = data.returnVal.rows[i]
				var candidateId = record.value0;
				var candidateName = record.value1 + ' '+record.value2;
				var vendorNames = record.value3;
				var vendorIds = record.value4;
				var skillsetGroup = record.value5;
				var skillsetIds = record.value6;
				var skillsetGroupIds = record.value7;
				candidateIds.push(candidateId);
				
				var skillset = new Array();
        		if (skillsetIds != null  && skillsetIds != '') {
					skillsetIds = skillsetIds.split(',');
					for ( var j = 0; j < skillsetIds.length; j++) {
						skillset.push(ds.skillSetStore.getById(Number(skillsetIds[j])).data.value);
					}
				}
				var skillsetGroup = new Array();
        		if (skillsetGroupIds != null  && skillsetGroupIds != '') {
        			skillsetGroupIds = skillsetGroupIds.split(',');
					for ( var j = 0; j < skillsetGroupIds.length; j++) {
						skillsetGroup.push(ds.skillsetGroupStore.getById(Number(skillsetGroupIds[j])).data.name);
					}
				}
				table += "<tr bgcolor='#ffffff'>" +
						"<td>"+candidateId+"</td>" +
						"<td>"+candidateName+"</td>" +
						"<td>"+skillset.toString()+"</td>" +
						"<td>"+skillsetGroup.toString()+"</td>" +
						"<td>"+vendorNames+"</td>" +
						"</tr>"; 
				
			}
			table += "</table>";
			this.candidateIds = candidateIds;
			var tpl = new Ext.XTemplate(
					table
			);
			this.dataView.tpl = tpl;
	      	this.dataView.refresh();

		}
	},

	getMarketingVendors : function() {
		var query = "SELECT distinct v.id,v.name,group_concat(distinct concat(c.firstName,' ',c.lastName) order by c.firstName)," +
				"group_concat(distinct c.id order by c.id),group_concat(distinct s.name)," +
				"(select group_concat(skillsetgroupId) from recruit_vendor_skillsetgroup where vendorId=v.id ) skillSetGroupIds " +
				"FROM recruit_candidate c,recruit_candidate_skillsetgroup cs,recruit_clients v,recruit_vendor_skillsetgroup vs,recruit_skillsetgroup s " +
				"where c.id=cs.candidateId and v.id=vs.vendorId and cs.skillSetGroupId = vs.skillSetGroupId and vs.skillSetGroupId = s.id " +
				"and cs.skillSetGroupId = s.id and (c.id,v.id) not in (select candidateId,vendorId from recruit_marketingcandidates) " +
				"group by v.id order by v.Name";

    	var params = new Array();
    	params.push(['query','=', query]);
    	var filter = getFilter(params);
		app.reportsService.getStatistics(Ext.JSON.encode(filter),this.onGetMarketingVendors,this);
	},
	
	onGetMarketingVendors : function(data) {
		if (data.success && data.returnVal.rows.length > 0) {
			var table = "<table cellpadding='5' bgcolor='#575252'>" +
				"<col width='10%'> <col width='15%'> <col width='15%'> <col width='15%'> <col width='35%'>" +
				"<tr bgcolor='#FFFF99'><th width='10%'>Vendor Id</th><th width='15%'>Vendor Name</th>" +
				"<th width='15%'>Skill Set Group</th><th width='35%'>Candidates</th></tr>" ;

			var vendorIds = new Array();
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var record = data.returnVal.rows[i];
				var vendorId = record.value0;
				var vendorName = record.value1;
				var candidateNames = record.value2;
				var candidateIds = record.value3;
				var skillsetGroup = record.value4;
				var skillsetGroupIds = record.value5;
				
				vendorIds.push({"vendorId":vendorId,"candidateIds":candidateIds});
				
				var skillsetGroup = new Array();
        		if (skillsetGroupIds != null  && skillsetGroupIds != '') {
        			skillsetGroupIds = skillsetGroupIds.split(',');
					for ( var j = 0; j < skillsetGroupIds.length; j++) {
						skillsetGroup.push(ds.skillsetGroupStore.getById(Number(skillsetGroupIds[j])).data.name);
					}
				}
				table += "<tr bgcolor='#ffffff'>" +
						"<td>"+vendorId+"</td>" +
						"<td><a onclick='javascript:app.reportsMgmtPanel.reportsDetailPanel.emailVendors(\""+vendorId+"\")' >"+vendorName+"</td>"+
						"<td>"+skillsetGroup.toString()+"</td>" +
						"<td>"+candidateNames+"</td>" +
						"</tr>"; 
				
			}
			table += "</table>";
			this.vendorIds = vendorIds;
			var tpl = new Ext.XTemplate(
					table
			);
			this.dataView.tpl = tpl;
	      	this.dataView.refresh();

		}
	},

	emailCandidates : function() {
		var candidateIds = this.candidateIds.toString();
		app.setActiveTab(0,'li_home');
		app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.form.reset();
		app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.form.findField('marketingStatus').setValue('');
		app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.form.findField('priority').setValue(null);
		app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.form.findField('id').setValue(candidateIds);
		app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.search('yes');
		app.homeMgmtPanel.homeDetailPanel.alertsTab.setActiveTab(4);
		app.homeMgmtPanel.homeDetailPanel.candidatesGrid2.vendorId = null;
	},
	
	emailVendors : function(vendorId) {
		for ( var i = 0; i < this.vendorIds.length; i++) {
			if (this.vendorIds[i].vendorId == vendorId){
				var candidateIds = this.vendorIds[i].candidateIds;
				app.setActiveTab(0,'li_home');
				app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.form.reset();
				app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.form.findField('marketingStatus').setValue('');
				app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.form.findField('priority').setValue(null);
				app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.form.findField('id').setValue(candidateIds);
				app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.search('yes');
				app.homeMgmtPanel.homeDetailPanel.alertsTab.setActiveTab(4);
				app.homeMgmtPanel.homeDetailPanel.candidatesGrid2.vendorId = Number(vendorId);
			}
		}
	}
	
});

function monthlyCalender(date,values,dates) {
	var month=date.getMonth(); // read the current month
	var year=date.getFullYear(); // read the current year

	date=new Date(year, month, 01);//Year , month,date format
	var first_day=date.getDay(); //, first day of present month
	date.setMonth(month+1,0); // Set to next month and one day backward.
	var last_date=date.getDate(); // Last date of present month

	var dy=1; // day variable for adjustment of starting date.
	
	months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	var date_today = months[month]+' '+year;

	var weeklyHeader = "<tr style='background-color:#dddddd;font-weight:bold;'>" +
						"<td width='100px' >Sunday</td>" +
						"<td width='100px' >Monday</td>" +
						"<td width='100px' >Tuesday</td>" +
						"<td width='100px' >Wednesday</td>" +
						"<td width='100px' >Thursday</td>" +
						"<td width='100px' >Friday</td>" +
						"<td width='100px' >Saturday</td>"+
						"<td width='120px' >Weekly average</td>";


	var table = "<table cellpadding='5' bgcolor='#575252'>" +
				"<tr><th colspan='8' style='background-color:#dddddd;font-weight:bold;'>"+date_today+"</th></tr>"+weeklyHeader;

	var weeklyCount =0, weeklyHors =0, weeklyEventCount=0;
	for(i=0;i<=41;i++){
		if (i%7==0 && dy > last_date )
			break;

		if((i%7)==0 ){
			// if week is over then start a new line
			if (weeklyEventCount == 0)
				weeklyEventCount = 1;

			if (i > 0)
				table +="<td style='text-align:center;background: #d6ffc7;'>"+ weeklyCount +" Submissions<br>"+(weeklyHors/weeklyEventCount).toFixed(0)+" Hrs (Average)</td>";
			table += "</tr><tr bgcolor='#ffffff'>";
			weeklyCount = 0;
			weeklyHors = 0;
			weeklyEventCount=0;
		}
		if((i>= first_day) && (dy<= last_date)){
			var today = Ext.Date.format(new Date(year,month,dy),'m/d/Y');
			if (dates.indexOf(today) != -1) {
				var events = values[dates.indexOf(today)];
				var hours = toNearestHour(events[2]);
				if ((new Date()).getDate() == dy && (new Date()).getMonth() == month && (new Date()).getFullYear() == year )
					table +="<td style='text-align:center;background: #ffff99;'><b>"+ dy +"</b>"+ "<br>"+events[1]+" Submissions<br>"+hours+" Hrs</td>";
				else
					table +="<td style='text-align:center;background: #d2e0ff;'><b>"+ dy +"</b>"+ "<br>"+events[1]+" Submissions<br>"+hours+" Hrs</td>";
    			weeklyCount += events[1];
    			weeklyHors += hours;
    			weeklyEventCount++;
			}else{
				if ((new Date()).getDate() == dy && (new Date()).getMonth() == month && (new Date()).getFullYear() == year )
					table +="<td style='text-align:center;vertical-align:top;background: #ffff99;' ><b>"+ dy +"</b></td>";
				else
					table +="<td style='text-align:center;vertical-align:top;' ><b>"+ dy +"</b></td>";
			}
				
			dy=dy+1;
		}else {
			table +="<td></td>";
		} // Blank dates.
	}
	if (weeklyEventCount == 0)
		weeklyEventCount = 1;

	table +="<td style='text-align:center;background: #d6ffc7;'>"+ weeklyCount +" Submissions<br>"+(weeklyHors/weeklyEventCount).toFixed(0)+" Hrs (Average)</td>";
	table += "</tr></table>";

	return table;
}

function toNearestHour(time) {
	var hours = Number(time.split(":")[0]);
	var minutes = Number(time.split(":")[1]);
    if (minutes >= 30)
    	hours++;
    return hours;
}
