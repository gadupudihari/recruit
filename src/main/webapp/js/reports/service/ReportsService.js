Ext.define('tz.service.ReportsService', {
	extend : 'tz.service.Service',
		
    getResult : function(query,  cb, scope){
	    this.onAjaxRawResponse = Ext.bind(this.onAjaxRawResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/getResult',
			params : {
				json : (query)
			},
			success: this.onAjaxRawResponse,
			failure: this.onAjaxRawResponse
		});
		app.loadMask.show();
    },

    getStatistics : function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/getStatistics',
			params : {
				json : (filter)
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    getComboValues : function(query,  cb, scope){
	    this.onAjaxRawResponse = Ext.bind(this.onAjaxRawResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/getComboValues',
			params : {
				json : (query)
			},
			success: this.onAjaxRawResponse,
			failure: this.onAjaxRawResponse
		});
		app.loadMask.show();
    },

    loadDataReport : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/loadDataReport',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },    

    loadCurrentReport : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/loadCurrentReport',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    loadCertifications : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/loadCertifications',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    loadSubmissionReport : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/loadSubmissionReport',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    pinMessage : function(values ){
		Ext.Ajax.request({
			url : 'remote/reports/pinMessage',
			params : {
				json : values
			},
		});
    },

    unPinMessage : function(values ){
		Ext.Ajax.request({
			url : 'remote/reports/unPinMessage',
			params : {
				json : values
			},
		});
    },

    sweepAllEmail : function(values,  cb, scope) {
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/sweepAllEmail',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.processingMask.show();
    },

    completeMessage : function(values ){
		Ext.Ajax.request({
			url : 'remote/reports/completeMessage',
			params : {
				json : values
			},
		});
    },

    activeMessage : function(values ){
		Ext.Ajax.request({
			url : 'remote/reports/activeMessage',
			params : {
				json : values
			},
		});
    },

    sendEmail : function(values,  cb, scope) {
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/sendEmail',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.sendMask.show();
    },

    sendHomeEmail : function(values,  cb, scope) {
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/sendHomeEmail',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.sendMask.show();
    },

    getSubmissionReport: function(object ,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/getSubmissionReport',
			params : {
				json : object
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadMask.show();
    },

    onSuccess : function(err) {
    	//Opening the Excel Filename returned from Java, This initiates download on the Browser
    	if (err.responseText.search('"success":false') != -1) {
    		app.reportsService.onFailure(err);
			return;
		}
    	if (err.responseText.search('.pdf') != -1) {
	    	fileStart = err.responseText.indexOf('returnVal');
	    	fileEnd = err.responseText.indexOf('successMessage');
	    	window.open(err.responseText.slice(fileStart+12,fileEnd-4),'_blank',true);
		}else{
	    	fileStart = err.responseText.indexOf('returnVal');
	    	fileEnd = err.responseText.indexOf('successMessage');
	    	window.open('Submission Reports\\'+err.responseText.slice(fileStart+12,fileEnd-4),'_blank',true);
		}
    	app.loadGenerateMask.hide();
    	this.onAjaxResponse;
    },
    
    onFailure : function(err) {
    	app.loadGenerateMask.hide();
    	if (err.responseText.search('.pdf') != -1)
    		msg = "Pdf Generation failed";
    	else if (Ext.JSON.decode(err.responseText).errorMessage.length > 0 && Ext.JSON.decode(err.responseText).errorMessage == "Report does not exists")
    		msg = "The report is too older, Please verify your email.";
    	else
    		msg = "Oops! Something went wrong. Please try again later.";
    	Ext.MessageBox.show({
    		title: 'Error',
    		msg: msg,
    		buttons: Ext.MessageBox.OK,
    		icon: Ext.MessageBox.ERROR
    	});
        this.onAjaxResponse;
    },

    sendSummaryEmail : function(values ){
		Ext.Ajax.request({
			url : 'remote/reports/sendSummaryEmail',
			params : {
				json : values
			},
		});
    },

    getMissingData : function(query,  cb, scope){
	    this.onAjaxRawResponse = Ext.bind(this.onAjaxRawResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/getMissingData',
			params : {
				json : (query)
			},
			success: this.onAjaxRawResponse,
			failure: this.onAjaxRawResponse
		});
		app.loadMask.show();
    },

    generatePDF : function(tneDetailObj,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/pdfExport',
			params : {
				json : tneDetailObj
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },

    candidateUpdateRequest : function(values ){
		Ext.Ajax.request({
			url : 'remote/reminder/candidateUpdateRequest',
			params : {
				json : values
			},
		});
    },
    
});
