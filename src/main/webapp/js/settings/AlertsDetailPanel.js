Ext.define('tz.ui.AlertsDetailPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 10,
    title: '',
    anchor:'100% 100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 100},
    
    
    initComponent: function() {
        var me = this;  

        me.dockedItems = [{
        	xtype: 'toolbar',
            dock: 'top',
            items: [{
            	xtype: 'button',
                text: 'Save',
                iconCls : 'btn-save',
                scope: this,
                tabIndex:53,
    	        handler: this.saveAlert
            },'-',{
                xtype: 'button',
                text: 'Reset',
                scope: this,
		        handler: function(){
					this.form.reset();
					this.clearMessage();
				}
            },'-',{
                xtype: 'button',
                text: 'Unsubscribe',
                icon : 'images/email_unsub.png',
                scope: this,
		        handler: function(){
		        	this.unsubscribeConfirm();
				}
            },'-',{
            	xtype: 'button',
            	text: 'Close',
            	iconCls : 'btn-close',
            	scope: this,
            	handler: function(){
            		app.settingsMgmtPanel.initScreen();
            	}
            }]
        }];
    		
        me.alertsStore = Ext.create('Ext.data.Store', {
            fields: ['alertname','alertTitle','reminderdays','alerttype','alertLogic','emailsubject','emailbody','mailto','mailtoRole','mailtoignore','priority']
        });

        me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Alert Name',
		    store : me.alertsStore,
		    labelAlign: 'right',
		    queryMode: 'local',
		    displayField: 'alertname',
		    valueField: 'alertname',
		    name : 'alertname',
            allowBlank : false,
            anyMatch : true,
        	forceSelection : true,
		    tabIndex:1,
        	listeners:{
        		select:function(combo, record,index){
        			me.loadForm(record[0]);
        		},scope: this
		    }
		});

        me.items = [this.getMessageComp(), this.getHeadingComp(),
                    {
	             xtype: 'container',
	             columnWidth:1,
	             items: [me.typeCombo,
	                     {
	            	 		xtype: 'container',
	            	 		layout : 'table',
	            	 		items: [{
	            	 			xtype:'textfield',
	            	 			fieldLabel: 'Alert Title',
	            	 			name: 'alertTitle',
	            	 			allowBlank : false,
	            	 			tabIndex:2,
	            	 		},{
	            	 			xtype:'displayfield',
	            	 			padding : '0 0 0 10',
	            	 			fieldStyle :'background-color: #D8D8D8;',
	            	 			value : 'Title of the alert',
	            	 		}]
	                     },{
	                    	xtype: 'container',
	                    	layout : 'table',
				            items: [{
				            	xtype:'textfield',
				            	fieldLabel: 'Reminder Days',
				            	name: 'reminderdays',
				            	allowBlank : false,
				                tabIndex:3,
				                maskRe :  /[0-9,-]/,
		       	                listeners:{
		       	                	change : function(field,newValue,oldValue){
		       	                		if (newValue.search(',,' != -1))
		       	                			this.setValue(newValue.replace(/,,/g, ","));
		       	                	}
		 			            }

				             },{
				            	xtype:'displayfield',
	            	 			padding : '0 0 0 10',
	            	 			fieldStyle :'background-color: #D8D8D8;',
				                value : 'Comma seperated no of reminder days',
				             }]
	                     },{
	                    	 xtype: 'container',
	                    	 layout : 'table',
	                    	 items: [{
	                    		 xtype:'textfield',
	                    		 fieldLabel: 'Alert Type',
	                    		 name: 'alerttype',
	                    		 tabIndex:4,
	                    	 },{
	                    		 xtype:'displayfield',
	                    		 padding : '0 0 0 10',
	                    		 fieldStyle :'background-color: #D8D8D8;',
	                    		 value : 'Type of the alert',
	                    	 }]
	                     },{
	                    	xtype: 'container',
	                    	layout : 'table',
				            items: [{
				            	xtype:'textfield',
				            	fieldLabel: 'Priority',
				            	name: 'priority',
				            	allowDecimals: false,
				            	allowBlank : false,
				                tabIndex:3,
				                maskRe :  /[0-9]/,
		       	                listeners:{
		       	                	change : function(field,newValue,oldValue){
		       	                		if (newValue.search(',,' != -1))
		       	                			this.setValue(newValue.replace(/,,/g, ","));
		       	                	}
		 			            }

				             },{
				            	xtype:'displayfield',
	            	 			padding : '0 0 0 10',
	            	 			fieldStyle :'background-color: #D8D8D8;',
				                value : 'Priority for the alert',
				             }]
	                     },{
	                    	 xtype: 'container',
	                    	 layout : 'table',
	                    	 items: [{
	                    		 xtype:'textfield',
	                    		 fieldLabel: 'Email To',
		    	                 name: 'mailto',
		    	                 tabIndex:5,
	                    	 },{
		    	            	 xtype:'displayfield',
	                    		 padding : '0 0 0 10',
	                    		 fieldStyle :'background-color: #D8D8D8;',
		    	                 value : 'Enter the email ids to whom this alert should be sent, In order to enter this you need to clear Role field.',
	                    	 }]
	                     },{
	                    	 xtype: 'container',
				             layout : 'table',
				             items: [{
		    	                 xtype:'textfield',
		    	                 fieldLabel: 'Role',
		    	                 name: 'mailtoRole',
		    	                 tabIndex:6,
		    	             },{
		    	            	 xtype:'displayfield',
	                    		 padding : '0 0 0 10',
	                    		 fieldStyle :'background-color: #D8D8D8;',
		    	                 value : 'All the users under this role will get this alert, currently ADMIN role is accepted',
		    	             }]
	                     },{
				             xtype: 'container',
				             layout : 'table',
				             items: [{
		    	                 xtype:'textfield',
		    	                 fieldLabel: 'Ignore Email Ids',
		    	                 name: 'mailtoignore',
		    	                 tabIndex:7,
		    	             },{
		    	            	 xtype:'displayfield',
	                    		 padding : '0 0 0 10',
	                    		 fieldStyle :'background-color: #D8D8D8;',
		    	                 value : 'Enter the email ids which needs to be ignored from this alert',
		    	             }]
	                     },{
				             xtype: 'container',
				             layout : 'table',
				             items: [{
				            	 xtype:'textarea',
		    	                 fieldLabel: 'Alert Logic',
		    	                 name: 'alertLogic',
		    	                 tabIndex:8,
		    	                 height : 56,
		    	                 width : 350
		    	             },{
		    	            	 xtype:'displayfield',
	                    		 padding : '0 0 0 10',
	                    		 fieldStyle :'background-color: #D8D8D8;',
		    	                 value : 'Logic of the alert',
		    	             }]
	                     },{
				             xtype: 'container',
				             layout : 'table',
				             items: [{
		    	                 xtype:'textarea',
		    	                 fieldLabel: 'Email Subject',
		    	                 name: 'emailsubject',
		    	                 tabIndex:9,
		    	                 height : 56,
		    	                 width : 350
		    	             },{
		    	            	 xtype:'displayfield',
	                    		 padding : '0 0 0 10',
	                    		 fieldStyle :'background-color: #D8D8D8;',
		    	                 value : 'Subject to be included in the email',
		    	             }]
	                     },{
	                    	 xtype: 'container',
	                    	 layout : 'table',
	                    	 items: [{
	                    		 xtype:'textarea',
	                    		 fieldLabel: 'Email Body',
	                    		 name: 'emailbody',
	                    		 tabIndex:10,
	                    		 height : 56,
	                    		 width : 350
	                    	 },{
	                    		 xtype:'displayfield',
	                    		 padding : '0 0 0 10',
	                    		 fieldStyle :'background-color: #D8D8D8;',
	                    		 value : 'Body to be included in the email',
	                    	 }]
	                     }]
                    }];
        
        me.callParent(arguments);
    },

    saveAlert: function(){
    	if(this.form.isValid( )){
			var params = new Array();
			var itemslength = this.form.getFields().getCount();
			var i = 0;
			
			while (i < itemslength) {
				var fieldName = this.form.getFields().getAt(i).name;
				if (this.form.findField(fieldName).getValue() == null || this.form.findField(fieldName).getValue() == '')
					params.push([fieldName ,'=', ' ']);
				else
					params.push([fieldName ,'=', this.form.findField(fieldName).getValue()]);
				
				i = i+1;
			}
			var filter = getFilter(params);
			app.settingsService.saveXMLAlert(Ext.JSON.encode(filter),this.onSaveAlert,this);
    	} else {
    		this.updateError('Please fix the errors');
    	}
    	this.doLayout();
	},
	
	onSaveAlert: function(data){
		if (data.success) { 
			this.updateMessage('Saved Successfully');
			this.loadAlertsStore();
		}  else {
			this.updateError('Save Failed');
		}	
		this.body.scrollTo('top', 0);
		this.doLayout();
	},

	loadForm: function(data){
		this.clearMessage();
		this.loadRecord(data);
	},
	
    loadAlertsStore : function() {
    	this.alertsStore.removeAll();
    	app.settingsService.getCustomAlerts('',this.onGetCustomAlerts,this);
	},
	
	onGetCustomAlerts : function(data) {
		if (data.success && data.returnVal.rows) {
			this.alertsStore.add(data.returnVal.rows);	
		}
	},

	unsubscribeConfirm : function() {
		var alertName = this.typeCombo.getValue();
		if (alertName == null || alertName == "") {
			this.updateError('Please select an alert to unsubscribe.');
			this.doLayout();
			return false;
		}
		Ext.Msg.confirm("Confirm", "Do you want to unsubscribe from this email alert?", this.unsubscribe, this);
	},

	unsubscribe : function(dat) {
		if (dat == 'yes') {
			var alertName = this.typeCombo.getValue();
			app.settingsService.unsubscribe(alertName,this.onUnsubscribe,this);
		}
	},
	
	onUnsubscribe : function(data) {
		if (data.success) {
			this.updateMessage('Unsubscribed Successfully');
			this.form.findField('mailtoignore').setValue(data.returnVal);
			this.loadAlertsStore();
		}else
			this.updateError(data.errorMessage);
		this.doLayout();
	}
});