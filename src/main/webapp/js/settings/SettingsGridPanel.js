Ext.define('tz.ui.SettingsGridPanel', {
    extend: 'Ext.grid.Panel',
    title: 'Step 1 : Select any item to edit',
    frame:true,
    anchor:'60%',
    height:250,
    //width :'100%',
    listeners: {
    	itemclick: function(dv, record, item, index, e) {
    		app.settingsMgmtPanel.settingsDetailPanel.loadSettings(record);                                       
    	}
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.settingsSearchStore;
        
        me.columns = [
			{
			    dataIndex: 'name',
			    text: 'Name',
			    sortable : true,
			    width : 130
			},{
                dataIndex: 'value',
                text: 'Value',
                sortable : true,
                width : 130
            },{
                dataIndex: 'type',
                text: 'Type',
                sortable : true,
                width : 130
            },{
                dataIndex: 'skillsetGroupName',
                text: 'Skillset Group Name',
                sortable : true,
                width : 90
            },{
                dataIndex: 'account',
                text: 'Account',
                sortable : true,
                width : 90
            },{
                dataIndex: 'acronym',
                text: 'Acronym',
                sortable : true,
                width : 50
            },{
                dataIndex: 'module',
                text: 'Module',
                sortable : true,
                width : 90
            },{
                dataIndex: 'orderNo',
                text: 'Order',
                sortable : true,
                width : 50
            },{
                dataIndex: 'defaultSelection',
                text: 'Default',
                sortable : true,
                width : 55,
                renderer: activeRenderer
            },{
                dataIndex: 'dependency',
                text: 'Dependency',
                sortable : true,
                width : 55,
                renderer: activeRenderer
            },{
                dataIndex: 'notes',
                text: 'Notes',
                sortable : true,
                width : 200
            }
        ];
        
        me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    store: ds.settingsStore,
		    labelAlign: 'right',
		    queryMode: 'local',
		    displayField: 'type',
		    valueField: 'type',
		    name : 'type',
		    anyMatch:true,
		    tabIndex:1
		});

        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [me.typeCombo,'-',
        	        {
        				text : 'Search',
        				iconCls : 'btn-Search',
        				tabIndex:4,
        				handler: function(){
        					me.search();
        				}
        	        },'-',{
        	        	text : 'Reset',
        	        	tabIndex:5,
        	        	handler: function(){
        	        		me.typeCombo.reset();
        	        		me.search();
        	        	}
        	        },'-',{
        				xtype: 'button',
        				iconCls : 'icon_info',
        				tooltip : 'Description',
        				tabIndex:4,
        				margin: '0 0 0 10',
        				scope: this,
        				handler: function(){
        					this.showDescription();
        				}
        	        },'-',{
        	        	xtype: 'button',
        	        	text: 'Close',
        	        	iconCls : 'btn-close',
        	        	scope: this,
        	        	handler: function(){
        	        		app.settingsMgmtPanel.initScreen();
        	        	}
        	        }]
        }];

        
        me.viewConfig = {
        		stripeRows:false 
        }
        
        me.on('scrollershow', function(scroller) {
        	  if (scroller && scroller.scrollEl) {
        	    scroller.clearManagedListeners(); 
        	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
        	  }
        });
        
        me.callParent(arguments);
    },
    
    search : function() {
    	app.settingsMgmtPanel.settingsDetailPanel.getForm().reset();
    	// Set the params
    	var params = new Array();
    	params.push(['type','like', this.typeCombo.getValue()]);

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=100;
    	ds.settingsSearchStore.loadByCriteria(filter);
	},
    
	showDescription : function() {
		var type = this.typeCombo.getValue();
		var message ='';
		switch (type) {
		case "Candidate Immigration": 
			message = 'Immigration in Candidates';
			break;
		case "Candidate Source": 
			message = 'Source in Candidates';
			break;
		case "Candidate Type": 
			message = 'Type in Candidates';
			break;
		case "Certification Name": 
			message = 'Certification Name in Certifications in Candidates and Job Openings';
			break;
		case "Certification Sponsored": 
			message = 'Sponsored By in Certifications in Candidates';
			break;
		case "Certification Status": 
			message = 'Status in Certifications in Candidates';
			break;
		case "Client Type": 
			message = 'Type in Clients/Vendors';
			break;
		case "Communication": 
			message = 'Communication in Candidates and Job Openings';
			break;
		case "Contact Role": 
			message = 'Role in Contacts';
			break;
		case "Contact Type": 
			message = 'Type in Contacts';
			break;
		case "Employer": 
			message = 'Employer in Job Openings';
			break;
		case "Employment Type": 
			message = 'Employment Type in Candidates';
			break;
		case "Experience": 
			message = 'Experience in Candidates';
			break;
		case "Hotness": 
			message = 'Hotness in Job Openings';
			break;
		case "Information Type": 
			message = 'Type of Information in Placement Leads';
			break;
		case "Job Opening Status": 
			message = 'Job Opening Status in Job Openings';
			break;
		case "Marketing Status": 
			message = 'Marketing Status in Candidates';
			break;
		case "Personality": 
			message = 'Personality in Job Openings and Candidates';
			break;
		case "Qualification": 
			message = 'Qualification in Candidates';
			break;
		case "Research Status": 
			message = 'Status in Tasks in Job Openings';
			break;
		case "Skill Set/Target Skill Set": 
			message = 'Module, Skill set, Target Skill set in Job Openings and Candidates';
			break;
		case "State": 
			message = 'State in Client/Vendors';
			break;
		case "Submission Status": 
			message = 'Status in Submissions in Job Openings';
			break;
		case "Target Roles": 
			message = 'Target Roles, Role set in Job Openings and Candidates';
			break;
		default:
			return false;
			break;
		}
		
		Ext.MessageBox.show({
			title:'Information',
            msg: message,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
	}

});

