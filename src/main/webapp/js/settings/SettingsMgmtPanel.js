Ext.define('tz.ui.SettingsMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',
    activeItem: 0,
    layout:'card',
    title: '',
    padding: 10,
    border:false,
    frame : false,
 
    initComponent: function() {
    	var me = this;
        
        me.settingsDetailPanel = new tz.ui.SettingsDetailPanel({manager:me});
    	me.settingsGridPanel = new tz.ui.SettingsGridPanel({manager:me});
    	me.settingsPanel = new tz.ui.SettingsPanel({manager:me});
    	me.alertsDetailPanel = new tz.ui.AlertsDetailPanel({manager:me});
    	me.moduleDetailPanel = new tz.ui.ModuleDetailPanel({manager:me});
    	
        me.emailListPanel = Ext.create('Ext.form.Panel', {
        	title : 'Default Alerts',
            frame:true,
            autoScroll:true,
        	bodyCls: 'fred',
        	xtype: 'htmleditor',
        	items : [{
	   	    	 xtype:'container',
		   	 	 layout: 'column',        
		   	 	 items :[{
   	                 xtype:'displayfield',
   	                 fieldLabel: '',
   	                 name: 'status',
   	                 value: 'APPROVED'
   	 			}]
		   	 }]
	     });

        me.items = [{
        			xtype: 'panel',
        			layout:'anchor',
        			autoScroll:true,    	                 
        			centered:true,
        			border:false,  
        			bodyStyle: 'padding:6% 0 0 40%',
        			items: [me.settingsPanel]
        		},{
					xtype: 'panel',
                    autoScroll:true,
                    border:false,
                    layout:'anchor',
                    items: [
                            me.getMessageComp(),
			   	   	 		me.settingsGridPanel,
			   	   	 		{
			   	   	 			height: 10,
			   	   	 			border : false
			   	   	 		},
			   	   	 		me.settingsDetailPanel,
			   	   	 		{
			   	   	 			height: 10,
			   	   	 			border : false
			   	   	 		},
			   	   	 		me.moduleDetailPanel
			   	   	 		]
        	},{
        			xtype:'container',
        			title: '',
        			collapsible: true,
        			layout: 'column',
        			items :[{
        				xtype: 'container',
        				columnWidth:.25,
        				items: [me.emailListPanel]
        			},{
        				xtype: 'container',
        				columnWidth:.75,
        				padding:'0 0 0 10',
        				items: [me.alertsDetailPanel,]
        			}]
        	}
        ];
    	
        me.callParent(arguments);
    },
    
    initScreen: function(){
    	this.clearMessage();
    	this.getLayout().setActiveItem(0);
    },
    
    appSettings : function () {
    	this.getLayout().setActiveItem(1);
    	this.settingsGridPanel.search();
		ds.settingsStore.loadByCriteria();
		ds.skillsetGroupStore.loadByCriteria();

    	var params = new Array();
    	params.push(['gender','=', 'Male']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.contactMaleStore.loadByCriteria(filter);
    	
    	var params = new Array();
    	params.push(['gender','=', 'Female']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.contactFemaleStore.loadByCriteria(filter);

    },

    showEmailPanel: function(){
		this.getLayout().setActiveItem(2);
    	var browserHeight = app.mainViewport.height;
		var headerHt = app.mainViewport.items.items[0].getHeight();
		this.emailListPanel.setHeight(browserHeight - headerHt - 25);
		this.alertsDetailPanel.loadAlertsStore();
    	this.getAlertsList();
	},

    getAlertsList : function() {
    	app.settingsService.getCustomAlerts('',this.onGetCustomAlerts,this);
	},

	onGetCustomAlerts : function(data) {
		if (data.success && data.returnVal.rows) {
			var titles = '';
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				titles += '<a onclick="javascript:app.settingsMgmtPanel.loadAlert(\''+data.returnVal.rows[i].alertname+'\')">'+(i+1)+'. '+data.returnVal.rows[i].alertTitle+'</a><br><br>';
			}
			this.emailListPanel.form.findField('status').setValue(titles);
		}
	},

	loadAlert : function(alertname) {
		this.alertsDetailPanel.alertsStore.clearFilter();
		var alert = this.alertsDetailPanel.alertsStore.getAt(this.alertsDetailPanel.alertsStore.findExact('alertname',alertname));
		this.alertsDetailPanel.loadForm(alert);
	}

});
