Ext.define('tz.ui.ModuleDetailPanel', {
    extend: 'Ext.form.Panel',
    bodyPadding: 10,
    title: 'Step 3 : Add dependency',
    anchor:'60%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'rigth',labelWidth :100},
    hidden : true,

    initComponent: function() {
        var me = this;
        
        me.inthelp1Combo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Interview Help 1',
        	name : 'inthelp1',
        	displayField: 'fullName',
            valueField: 'id',
            listClass: 'x-combo-list-small',
            store: ds.contactMaleStore,
		    queryMode: 'local',
		    typeAhead: true,
		    anyMatch:true,
		    //allowBlank:false,
		});

        me.inthelp2Combo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Interview Help 2',
        	name : 'inthelp2',
        	displayField: 'fullName',
            valueField: 'id',
            listClass: 'x-combo-list-small',
            store: ds.contactFemaleStore,
		    queryMode: 'local',
		    typeAhead: true,
		    anyMatch:true,
		    //allowBlank:false,
		});
  
        me.trainerCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Trainer',
        	name : 'trainer',
        	displayField: 'fullName',
            valueField: 'id',
            listClass: 'x-combo-list-small',
            store: ds.contactSearchStore,
		    queryMode: 'local',
		    typeAhead: true,
		    anyMatch:true,
		});
     
        me.resumeHelpCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Resume Help',
        	name : 'resumeHelp',
        	displayField: 'fullName',
            valueField: 'id',
            listClass: 'x-combo-list-small',
            store: ds.contactSearchStore,
		    queryMode: 'local',
		    typeAhead: true,
		    anyMatch:true,
		});

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        scope: this,
                        tabIndex:11,
                        handler: function(){
                			this.save();
                		}
                    },'-',{
                        xtype: 'button',
                        text: 'Reset',
                        iconCls : 'btn-refresh',
                        scope : this,
                        tabIndex:12,
                        handler: function(){
                        	var settingsId = this.form.findField('settingsId').getValue();
                			this.form.reset();
                			app.settingsMgmtPanel.clearMessage();
                        	this.form.findField('settingsId').setValue(settingsId);
                		}
                    }
                ]
            }
        ];
        me.items = [{
        	xtype:'fieldset',
        	border : 0,
        	collapsible: false,
        	layout: 'column',
        	items :[{
        		xtype:'container',
        		columnWidth:1,
        		items :[
       	        {
        			xtype: 'container',
        			layout: {
        				type: 'table',
        				columns: 2,
        				tableAttrs: {
        					style: {
        						width: '100%',
        						labelWidth : 100
        					}
        				}
        			},
        			items: [
        			me.inthelp1Combo,me.inthelp2Combo,me.trainerCombo,me.resumeHelpCombo,
        			{
        				xtype: 'numberfield',
        				hidden:true,
        				name: 'id',
        			},{
        				xtype: 'numberfield',
        				hidden:true,
        				name: 'settingsId',
        			}]
       	        }]	   	     
        	}]
        }];
        me.callParent(arguments);
    },
    
    save : function() {
    	if(this.form.isValid( )){
    		var moduleObj = {};
    		var settings = {};
    		var itemslength = this.form.getFields().getCount();
    		var i = 0;
			while (i < itemslength) {
				var fieldName = this.form.getFields().getAt(i).name;
				if(fieldName == 'id' || fieldName == 'inthelp1' || fieldName == 'inthelp2' || fieldName == 'trainer' || fieldName == 'resumeHelp' ) {
					var idVal = this.form.findField(fieldName).getValue();
					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0) {
						moduleObj[fieldName] = this.form.findField(fieldName).getValue();
					}
				}else if (fieldName == 'settingsId') {
					var idVal = this.form.findField(fieldName).getValue();
					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0) {
						settings['id'] = this.form.findField(fieldName).getValue();
					}
				}else
					moduleObj[fieldName] = this.form.findField(fieldName).getValue();
				i++;
			}
			
			moduleObj = Ext.JSON.encode(moduleObj);
			settings = Ext.JSON.encode(settings);
			moduleObj = '{"settings": '+settings +',' + moduleObj.substring(1,moduleObj.length);
			app.settingsService.saveModule(moduleObj,this.onSave,this);
    	}else
    		app.settingsMgmtPanel.updateError('Please fix the errors');
	},

    onSave : function(data) {
		if (data.success) { 
			var employee = data.returnVal;
			this.form.findField('id').setValue(employee.id);
			app.settingsMgmtPanel.updateMessage('Saved Successfully');
		} else {
			app.settingsMgmtPanel.updateError('Save Failed');
		}	
	},
    
	getModule : function(settingsId) {
		var params = new Array();
    	params.push(['settingsId','=', settingsId]);
    	var filter = getFilter(params);
    	app.settingsService.getModule(Ext.JSON.encode(filter),this.loadForm,this);
	},
	
    loadForm : function(data) {
    	ds.contactMaleStore.clearFilter();
    	ds.contactFemaleStore.clearFilter();
    	ds.contactSearchStore.clearFilter();
    	if (data.success && data.rows.length > 0) {
    		var record = Ext.create('tz.model.Module', data.rows[0]);
        	this.form.reset();
        	this.getForm().loadRecord(record);
		}
	},
    
});