Ext.define('tz.ui.SettingsPanel', {
	extend: 'Ext.form.Panel',
    //extend: 'tz.ui.BaseFormPanel',
        title: 'Settings',
        frame:true,
        anchor:'30%',
        height:'100%',
        autoScroll:false,
        layout: {
            type: 'vbox',
            align: 'center',
            pack: 'center'
        },

        initComponent: function() {
            var me = this;
            
           me.appSettings = new Ext.Button({
          	 xtype: 'button',
               text: 'Application Settings',
               margin: '0 0 0 20',
               width : 150,
               height : 35,
               handler: this.appSettings
           });
           
    	   me.emailAlerts = new Ext.Button({
    		   xtype: 'button',
               text: 'Email alerts',
               margin: '20 0 0 20',
               width : 150,
               height : 35,
               handler: this.alerts
    	   });  

    	   me.items = [{
	   	    	 xtype:'container',
	   	    	 width:200,
	   	    	 border:true,
	   	         layout: 'column',	
	   	         autoHeight:true,
	   	         items :[{
	   	        	 xtype: 'container',
	   	             columnWidth:1,
	   	             items: [
	   	                     me.appSettings,
	   	                     me.emailAlerts
	   	                    ]
	   	         }]
            }];
            
            me.callParent(arguments);
        },

        appSettings : function () {
        	app.settingsMgmtPanel.appSettings();
        },
	
        alerts : function() {
        	app.settingsMgmtPanel.showEmailPanel();
        },

});
