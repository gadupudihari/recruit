Ext.define('tz.ui.SettingsDetailPanel', {
    extend: 'Ext.form.Panel',
    bodyPadding: 10,
    title: 'Step 2 : Edit or add a new type.',
    anchor:'60%',
    height:270,
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth :100},
    initComponent: function() {
        var me = this;
        
        me.deleteButton = new Ext.Button({
            text: ' ',
            handleMouseEvents:false,
            width : 50,
            listeners: {
                dblclick : {
                    fn: function(e, t) {
                    	var status = me.deleteConfirm();
                    },
                    element: 'el',
                    scope: this
                }
            },
        });
        
        me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type *',
		    name : 'type',
		    store: ds.settingsStore,
		    queryMode: 'local',
		    displayField: 'type',
		    valueField: 'type',
		    allowBlank:false,
		    anyMatch:true,
			enforceMaxLength:true,
		    tabIndex:6,
		    listeners:{
		    	select:function(combo, record,index){
		    		if(combo.getValue() == 'Skill Set/Target Skill Set'){
		    			this.skillsetGroupCombo.show();
		    		}else{
		        		 this.skillsetGroupCombo.hide();
		        		 this.skillsetGroupCombo.reset();
		        	 }
		         },scope: this
		    }		});

        me.skillsetGroupCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Select/Type a new Skillset Group',
		    name : 'skillsetGroupName',
		    store: ds.skillsetGroupStore,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'id',
		    allowBlank:true,
		    anyMatch:true,
		    maxLength:45,
		    hidden : true,
			enforceMaxLength:true,
		    tabIndex:6,
		    listeners:{
		    	select:function(combo, record,index){
		        	 this.form.findField('skillsetGroupId').setValue(combo.value);
		    	},scope: this
		    }
		});

        me.moduleCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Module',
		    name : 'module',
		    store: ds.moduleStore,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
		    //allowBlank:false,
		    forceSelection:true,
		    tabIndex:12,
		});

        me.defalultCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Default',
		    name : 'defaultSelection',
		    store: ds.boolStore,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
		    forceSelection:true,
		    tabIndex:13,
		});

        me.dependencyCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Dependency',
		    name : 'dependency',
		    store: ds.boolStore,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
		    forceSelection:true,
		    value : false,
		    tabIndex:14,
		});

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        iconCls : 'btn-add',
                        scope : this,
                        tabIndex:20,
                        handler: function(){
                			this.form.reset();
                		}
                    },'-',{
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        scope: this,
                        tabIndex:21,
                        handler: function(){
                			this.saveConfirm();
                		}
                    },'-',{
                        xtype: 'button',
                        text: 'Reset',
                        iconCls : 'btn-refresh',
                        scope : this,
                        tabIndex:22,
                        handler: function(){
                			this.form.reset();
                			app.settingsMgmtPanel.clearMessage();
                		}
                    },'->',me.deleteButton
                ]
            }
        ];
        me.items = [{
        	xtype:'fieldset',
        	border : 0,
        	collapsible: false,
        	layout: 'column',
        	items :[{
        		xtype:'container',
        		columnWidth:.5,
        		items :[me.typeCombo]
        	},{
        		xtype:'container',
        		columnWidth:.5,
        		items :[me.skillsetGroupCombo]	   	     
        	}]
        },{
        	xtype:'fieldset',
        	border : 0,
        	collapsible: false,
        	layout: 'column',
        	items :[{
        		xtype:'container',
        		columnWidth:.5,
        		items :[{
        				xtype: 'textfield',
        				name: 'name',
        				fieldLabel: 'Name *',
        				allowBlank:false,
        				maxLength:150,
        				enforceMaxLength:true,
        				tabIndex:7
        			},{
        				xtype: 'textfield',
        				name: 'account',
        				fieldLabel: 'Account',
        				maxLength:45,
        				enforceMaxLength:true,
        				tabIndex:9
        			},{
        				xtype: 'numberfield',
        				name: 'orderNo',
        				fieldLabel: 'Order',
        				//allowBlank:false,
        				tabIndex:11
        			},
        			me.defalultCombo,
        			{
        				xtype: 'textarea',
        				name: 'notes',
        				fieldLabel: 'Notes',
        				maxLength:200,
        				width : 350,
        				height : 40,
        				enforceMaxLength:true,
        				tabIndex:15
        			}]
        	},{
        		xtype:'container',
        		columnWidth:.5,
        		items :[{
    				xtype: 'textfield',
    				name: 'value',
    				fieldLabel: 'Value *',
    				allowBlank:false,
    				maxLength:150,
    				enforceMaxLength:true,
    				tabIndex:8
    			},{
    				xtype: 'textfield',
    				name: 'acronym',
    				fieldLabel: 'Acronym',
    				maxLength:45,
    				enforceMaxLength:true,
    				tabIndex:10,
    			},
    			me.moduleCombo,me.dependencyCombo,
    			{
    				xtype: 'displayfield',
    				name: 'tableName',
    				fieldLabel: 'Table Name',
    				maxLength:45,
    				enforceMaxLength:true,
    				tabIndex:16
    			},{
    				xtype: 'numberfield',
    				hidden:true,
    				name: 'id',
    				listeners: {
    					change: function(field, value) {
    						if(value == null){
    							me.deleteButton.disable();
    							me.form.findField('name').setReadOnly(false);
    						}else{
    							me.deleteButton.enable();
    							me.form.findField('name').setReadOnly(true);
    						}
    					}
    				}
    			},{
    				xtype: 'numberfield',
    				hidden:true,
    				name: 'skillsetGroupId',
    			}]	   	     
        	}]
        }];
        me.callParent(arguments);
    },
    
    saveConfirm : function() {
    	if(!this.form.isValid( )){
    		app.settingsMgmtPanel.updateError('Please fix the errors');
    		return;
    	}
    	var selectedRecord = app.settingsMgmtPanel.settingsGridPanel.getSelectionModel().getSelection()[0];
    	if (selectedRecord != null && this.form.findField('id').getValue() != null) {
			if (selectedRecord.data.type != this.typeCombo.getValue()) {
				Ext.Msg.confirm("Type is changed", "Do you want to continue?", this.save, this);
			}else
				this.save('yes');
		}else
			this.save('yes');
	},
    
    save : function(dat) {
    	if (dat =='yes') {
        	if(this.form.isValid( )){
        		var setting = {},skillsetGroup={};
        		var itemslength = this.form.getFields().getCount();
        		var i = 0;
    			while (i < itemslength) {
    				var fieldName = this.form.getFields().getAt(i).name;
    				if(fieldName == 'id') {
    					var idVal = this.form.findField(fieldName).getValue();
    					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0) {
    						setting['id'] = this.form.findField(fieldName).getValue();
    					}
    				}else
    					setting[fieldName] = this.form.findField(fieldName).getValue();
    				i++;
    			}
    			setting.type = setting.type.charAt(0).toUpperCase() + setting.type.slice(1);
    			setting.acronym = setting.acronym.toUpperCase() ;
    			
    			var idVal = this.form.findField('skillsetGroupId').getValue();
    			if(idVal != '' && idVal != null && idVal >= 0) {
    				skillsetGroup['id'] = idVal;
    			}
    			if (this.skillsetGroupCombo.getValue() != null && this.skillsetGroupCombo.getValue() != '') {
        			skillsetGroup['name'] = this.skillsetGroupCombo.getRawValue();
    				skillsetGroup['type'] = this.typeCombo.getValue();
				}
    			var skillset ='';
    			if (skillsetGroup.id != null || skillsetGroup.name != null) {
    				skillsetGroup = Ext.JSON.encode(skillsetGroup);
    				skillset = '"skillsetGroup":' + skillsetGroup + ',';
				}
    			if (setting.orderNo == null || setting.orderNo == '')
    				delete setting.orderNo;
    			delete setting.skillsetGroupId;
    			delete setting.skillsetGroupName;
    			setting = Ext.JSON.encode(setting);
    			setting = '{'+ skillset + setting.substring(1,setting.length-1) + '}';
    			app.settingsService.save(setting,this.onSave,this);
        	}else
        		app.settingsMgmtPanel.updateError('Please fix the errors');
    	}
    },

    onSave : function(data) {
		if (data.success) { 
			this.form.reset();
			ds.settingsStore.loadByCriteria();
			ds.skillsetGroupStore.loadByCriteria();
			app.settingsMgmtPanel.settingsGridPanel.search();
			app.settingsMgmtPanel.updateMessage('Saved Successfully, Reset/Refresh to see new values in the dropdown');
			this.manager.moduleDetailPanel.form.reset();
			this.manager.moduleDetailPanel.hide();
		}  else {
			app.settingsMgmtPanel.updateError(data.errorMessage);
		}	
	},
    
    loadSettings : function(settings) {
    	this.record = settings;
    	this.type = settings.data.type;
    	this.form.reset();
    	this.getForm().loadRecord(settings);
    	if (settings.data.type ==  "Skill Set/Target Skill Set") 
    		this.skillsetGroupCombo.show();
		else
    		this.skillsetGroupCombo.hide();

    	if (settings.data.dependency) {
			this.manager.moduleDetailPanel.show();
			this.manager.moduleDetailPanel.form.reset();
			this.manager.moduleDetailPanel.form.findField('settingsId').setValue(settings.data.id);
			this.manager.moduleDetailPanel.getModule(settings.data.id);
		}else{
			this.manager.moduleDetailPanel.form.reset();
			this.manager.moduleDetailPanel.hide();
		}
	},
    
	deleteConfirm : function() {
		var idVal = this.form.findField('id').getValue();
		if (idVal != null && idVal != '' && idVal != 0)
			Ext.Msg.confirm("Confirm", "Are you sure to delete this setting ? Deleting this will affect in many places. ", this.deleteCategory, this);
	},
	
	deleteCategory : function(dat) {
		if(dat=='yes'){
			var idVal = this.form.findField('id').getValue();
			app.settingsService.deleteCategory(idVal,this.onDelete,this);
		}
	},
	
	onDelete : function(data) {
		if (data.success) {
			app.settingsMgmtPanel.settingsGridPanel.search();
			this.form.reset();
			app.settingsMgmtPanel.updateMessage('Deleted Successfully');
		}  else {
			app.settingsMgmtPanel.updateError(data.errorMessage);
		}	
	}
});