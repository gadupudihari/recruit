Ext.define('tz.ui.SettingsSearchPanel', {
    extend: 'Ext.form.Panel',
    bodyPadding: 10,
    title: 'Step 1 : Search for a type',
    frame:true,
    anchor:'50% 22%',
    buttonAlign:'left',
    //width:'100%',
    //height:150, 
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},

    initComponent: function() {
        var me = this;
        
       
        me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    store: ds.settingsStore,
		    labelAlign: 'right',
		    queryMode: 'local',
		    displayField: 'type',
		    valueField: 'type',
		    name : 'type',
		    anyMatch:true,
		    tabIndex:1
		});
        
        me.items = [
            {
   	             xtype: 'container',
   	             layout: 'column',
   	             columnWidth:.5,
   	             items: [me.typeCombo]
	   	     }
        ]; 
        me.buttons = [ 
            {
            	text : 'Search',
            	scope: this,
            	tabIndex:2,
    	        handler: this.search
			},{
				text : 'Reset',
				scope: this,
				tabIndex:3,
				handler: this.reset
			},{
				text : 'Clear',
				scope: this,
				tabIndex:4,
	             handler: function() {
	            	 this.form.reset();
	             }
			}
		];
        
        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [{
        		xtype: 'button',
        		text: 'Close',
        		iconCls : 'btn-close',
        		scope : this,
        		handler: function(){
        			app.settingsMgmtPanel.initScreen();
        		}
        	}]
        }];
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	app.settingsMgmtPanel.settingsDetailPanel.getForm().reset();
    	// Set the params
    	var params = new Array();
    	params.push(['type','like', this.typeCombo.getValue()]);

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=100;
    	ds.settingsSearchStore.loadByCriteria(filter);
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
	
});
