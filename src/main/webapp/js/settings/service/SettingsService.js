Ext.define('tz.service.SettingsService', {
	extend : 'tz.service.Service',
		
	save : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/save',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

	saveSkillsetGroup : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/saveSkillsetGroup',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    getCategory : function(storeType,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/getCategory',
			params : {
				json : storeType
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    getModuleCategory : function(storeType,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/getModuleCategory',
			params : {
				json : storeType
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    getCustomAlerts : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/getCustomAlerts',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    saveXMLAlert : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/saveXMLAlert',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    unsubscribe : function(values,  cb, scope) {
    	this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/unsubscribe',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    saveEmailDetails : function(values,  cb, scope) {
    	this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/saveEmailDetails',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    getModule : function(values,  cb, scope) {
    	this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/getModule',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    saveModule : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/saveModule',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    getCustomColumns : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/getCustomColumns',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

});


