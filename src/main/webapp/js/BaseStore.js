Ext.define('tz.ui.BaseStore', {
	extend : 'Ext.data.Store',
	proxy: {
        type: 'ajax',
        reader: {
        	type: 'json',
            root: 'rows',
            url : this.url,
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    loadByCriteria: function(criteria){
		this.load({
		    params:{
		        start:0,
		        limit: config.pageSize,
		        json : Ext.JSON.encode(criteria)
		    }
		});
	},
	initComponent: function(){
		this.callParent(arguments);
	}
});



Ext.define('tz.ui.PagingToolbar', {
	extend : 'Ext.PagingToolbar',
    displayInfo: true,
    displayMsg: 'Displaying topics {0} - {1} of {2}',
    emptyMsg: "No topics to display"
});
