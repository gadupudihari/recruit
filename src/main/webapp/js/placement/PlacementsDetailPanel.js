Ext.define('tz.ui.PlacementsDetailPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 10,
    title: '',
    anchor:'100% 95%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'left'},
    
    initComponent: function() {
        var me = this;
        me.newPlacement = false;
		me.copyPlacement = false;
		me.modified = false;
		
		me.informationTypeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type of Information',
		    store: ds.informationTypeStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
            valueField: 'name',
		    name : 'typeOfInformation',
		    labelWidth :125,
		    tabIndex:5,
		    labelWidth :125,
		});

        me.stateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'State',
		    store: ds.stateStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    forceSelection:true,
		    name : 'state',
		    labelWidth :125,
		    tabIndex:8,
		    labelWidth :125,
		});

		me.deleteButton = new Ext.Button({
            text: 'Delete',
            iconCls : 'btn-delete',
            scope : this,
            tabIndex:18,
            handler: function(){
    			this.deleteConfirm();
    		}
        });

        me.saveAndNewButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & New',
            iconCls : 'btn-save',
            tabIndex:16,
            scope: this,
	        handler: function() {
				this.saveAndNewPlacement();
			}
        });
        me.saveAndCopyButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & Copy',
            iconCls : 'btn-save',
            tabIndex:17,
            scope: this,
	        handler: function() {
				this.saveAndCopyPlacement();	
			}
        });

		
        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        scope: this,
                        tabIndex:15,
            	        handler: this.save
                    },
                    '-',me.saveAndNewButton,'-',me.saveAndCopyButton,'-',me.deleteButton,'-',
                    {
                        xtype: 'button',
                        text: 'Close',
                        iconCls : 'btn-close',
                        tabIndex:19,
                        scope : this,
                        handler: function(){
                        	this.closeForm();
                        }
                    }
                ]
            }
        ];
    	
        me.items = [this.getMessageComp(), this.getHeadingComp(),
            {
	   	    	 xtype:'fieldset',
	   	         collapsible: false,
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.3,
	   	             items: [{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Date',
	   	                 name: 'date',
	   	                 tabIndex:1,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Person',
	   	                 name: 'person',
	   	                 tabIndex:2,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Source of Information',
	   	                 name: 'sourceOfInformation',
	   	                 tabIndex:3,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Source Sublevel',
	   	                 name: 'source_Sublevel',
	   	                 tabIndex:4,
	   	                 labelWidth :125,
	   	             },
	   	             me.informationTypeCombo,
	   	             {
	   	            	 xtype: 'numberfield',
	   	            	 name: 'priority',
	   	            	allowDecimals: false,
	   	            	 fieldLabel: 'Priority',
	   	            	 minValue: 0,
	   	            	 maxValue: 100,
	   	            	 labelWidth :125,
	   	            	 tabIndex:6,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Hospital / Entity',
	   	                 name: 'hospital_Entity',
	   	                 tabIndex:7,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Location',
	   	                 name: 'location',
	   	                 tabIndex:8,
	   	                 maxLength:100,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },
	   	             me.stateCombo,
	   	             {
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Go Live Date',
	   	                 name: 'liveDate',
	   	                 maxLength:50,
	   	                 enforceMaxLength:true,
	   	                 tabIndex:9,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Link 1',
	   	                 name: 'link1',
	   	                 vtype:'url',
	   	                 tabIndex:10,
	   	                 maxLength:200,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Link 2',
	   	                 name: 'link2',
	   	                 vtype:'url',
	   	                 tabIndex:11,
	   	                 maxLength:200,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textarea',
	   	                 fieldLabel: 'Comments',
	   	                 name: 'comments',
	   	                 tabIndex:12,
	   	                 labelWidth :125,
	   	                 maxLength:300,
	   	                 width:400,
	   	                 height:70,
	   	                 enforceMaxLength:true,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.6,
	   	             items: [{
	   	                 xtype:'textarea',
	   	                 fieldLabel: 'Information',
	   	                 name: 'information',
	   	                 tabIndex:13,
	   	                 maxLength:1000,
	   	                 width:750,
	   	                 height:170,
	   	                 enforceMaxLength:true,
	   	             },{
	   	                 xtype:'textarea',
	   	                 fieldLabel: 'Outreach',
	   	                 name: 'outreach',
	   	                 tabIndex:14,
	   	                 maxLength:1000,
	   	                 width:750,
	   	                 height:150,
	   	                 enforceMaxLength:true,
	   	             },{
	   	     	        xtype: 'numberfield',
	   	    	        hidden:true,
	   	    	        name: 'id'
	   	    	    },{
	   	     	        xtype: 'datefield',
	   	    	        hidden:true,
	   	    	        name: 'created'
	   	    	    }]
	   	         }]
	   	     }
        ];
        me.callParent(arguments);
    },

    loadForm : function(record) {
    	this.modified = false;
    	this.clearMessage();
    	this.form.reset();
		this.loadRecord(record);
		this.deleteButton.enable();
	},
    
	closeForm: function(){
		app.placementMgmtPanel.getLayout().setActiveItem(0);
		if (this.modified) {
			app.placementMgmtPanel.placementSearchPanel.search();
		}
	},

	saveAndNewPlacement : function() {
		this.newPlacement = true;
		this.save();
	},
	
	saveAndCopyPlacement : function() {
		this.copyPlacement = true;
		this.save();
	},
	
    save : function(){
    	if(this.form.isValid( )){
			var otherFieldObj = {};
			var itemslength = this.form.getFields().getCount();
			var i = 0;
			while (i < itemslength) {
				var fieldName = this.form.getFields().getAt(i).name;
				if (fieldName == 'id' ) {
					var idVal = this.form.findField(fieldName).getValue();
					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0)
						otherFieldObj[fieldName] = this.form.findField(fieldName).getValue();
				}else 
					otherFieldObj[fieldName] = this.form.findField(fieldName).getValue();
				i = i+1;
			}
	   		if (otherFieldObj.priority == null )
	   			delete otherFieldObj.priority;

			otherFieldObj = Ext.JSON.encode(otherFieldObj);
			app.placementService.savePlacement(otherFieldObj,this.onSave,this);
    	} else {
    		this.updateError('Please fix the errors');
    	}
	},
	
	onSave : function(data){
		this.modified = true;
		if (data.success) { 
			var rec = data.returnVal;
			this.form.findField('id').setValue(rec.id)
			this.deleteButton.enable();
			this.updateMessage('Saved Successfully');
			if(this.newPlacement == true || this.copyPlacement == 'true'){
				this.clearMessage();
				this.newPlacement = false;
				this.updateMessage('Saved successfully');
				this.getForm().reset();
				this.deleteButton.disable();
			}
			if(this.copyPlacement == true || this.copyPlacement == 'true'){
				this.clearMessage();
				this.copyPlacement = false; 
				this.updateMessage('Saved previous Placement successfully');
				this.form.findField('id').setValue(null);
				this.deleteButton.disable();
			}
		}else {
			this.updateError('Save Failed');
		}	
	},	
	
	onDelete : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			this.modified = true;
			this.closeForm();
		}
	},
	
	deleteConfirm : function(){
		Ext.Msg.confirm("Confirm","Do you want to delete this Placement Lead?", this.deletePlacement, this);
	},
	
	deletePlacement : function(dat){
		if(dat=='yes'){
			var idVal = this.getValues().id;
			app.placementService.deletePlacement(idVal, this.onDelete, this);
		}
	},

});