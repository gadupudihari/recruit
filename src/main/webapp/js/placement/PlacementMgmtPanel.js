Ext.define('tz.ui.PlacementMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.placementSearchPanel = new tz.ui.PlacementSearchPanel({manager:me});
    	me.placementsGrid = new tz.ui.PlacementsGrid({manager:me});
        me.placementsDetailPanel = new tz.ui.PlacementsDetailPanel({manager:me});
        
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,    	                 
            	centered:true,
            	border:false,  
            	items: [ this.placementSearchPanel,
 					    {
		   	        		height: 15,
		   	        		border : false
 					    },
 					    this.placementsGrid
 					    ]
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
	            items: [me.placementsDetailPanel]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen: function(){
		this.getLayout().setActiveItem(0);

		var browserHeight = app.mainViewport.height;
    	var searchPanelHt = Ext.getCmp('placementSearchPanel').getHeight();
		var headerHt = app.mainViewport.items.items[0].getHeight();
    	Ext.getCmp('placementsGrid').setHeight(browserHeight - searchPanelHt - headerHt - 40);

    	if (ds.placementLeadsStore.getCount() ==0) 
    		this.placementSearchPanel.search();	

		ds.informationTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Information Type'])));
		ds.stateStore.loadByCriteria(getFilter(new Array(['type','=', 'State'])));
	},
	
	showPlacements : function(rowIndex) {
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var record = ds.placementLeadsStore.getAt(rowIndex);
		}
		this.placementsDetailPanel.loadForm(record);
	},
	
});
