Ext.define('tz.ui.PlacementSearchPanel', {
    extend: 'Ext.form.Panel',
    id : 'placementSearchPanel',
    bodyPadding: 10,
    title: '',
    frame:true,
    anchor:'100%',
    autoScroll:true,
    buttonAlign:'left',
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},
    
    initComponent: function() {
        var me = this;
        
        me.informationTypeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type of Information',
		    store: ds.informationTypeStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'typeOfInformation',
		    labelWidth :125,
		    tabIndex:3,
		    labelWidth :125,
		});

        me.stateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'State',
		    store: ds.stateStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'state',
		    labelWidth :125,
		    tabIndex:4,
		    labelWidth :125,
		});

        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Search',
	   	                 name: 'searchPlacements',
	   	                 tabIndex:1,
	   	             },{
	   	                 xtype:'numberfield',
	   	                 fieldLabel: 'Priority',
	   	                 name: 'priority',
	   	                 allowDecimals: false,
	   	                 tabIndex:2,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [me.informationTypeCombo,me.stateCombo]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:10,
			},{
				text : 'Reset',
				scope: this,
				handler: this.reset,
				tabIndex:11,
			},{
				text : 'Clear',
				scope: this,
				tabIndex:12,
				handler: function() {
					this.form.reset();
				}
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.form.getFieldValues();
    	var params = new Array();
    	params.push(['typeOfInformation','=', values.typeOfInformation]);
    	if (values.priority != null)
    		params.push(['priority','=', values.priority.toString()]);
    	params.push(['search','=', values.searchPlacements]);
    	params.push(['state','=', values.state]);

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=200;
    	ds.placementLeadsStore.loadByCriteria(filter);
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
