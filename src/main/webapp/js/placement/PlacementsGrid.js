Ext.define('tz.ui.PlacementsGrid', {
    extend: 'Ext.grid.Panel',
    id : 'placementsGrid',
    title: 'Placement Leads',
    frame:true,
    anchor:'100%',
    //height:550,
    width :'100%',
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value && record.data.id != null){
    			this.modifiedIds.push(record.data.id);
    		}
        }
    },

    initComponent: function() {
        var me = this;
        me.store = ds.placementLeadsStore;
        me.modifiedIds = new Array();
        
        me.columns = [
                      {
                    	  xtype: 'actioncolumn',
                    	  width :40,
                    	  items : [{
                    		  icon : 'images/icon_edit.gif',
                    		  tooltip : 'View Placement',
                    		  padding: 50,
                    		  scope: this,
                    		  handler : function(grid, rowIndex, colIndex) {
                    			  app.placementMgmtPanel.showPlacements(rowIndex);
                    		  }
                    	  }]
                      },{
                    	xtype: 'datecolumn',
                    	text : 'Date',
          				dataIndex : 'date',
          				width :  100,
          				renderer: dateRender,
          				editor: {
          	                xtype: 'datefield',
          	                format: 'm/d/y'
          	            }
          			},{
          				text : 'Source of Information',
          				dataIndex : 'sourceOfInformation',
          				width :  80,
          		        renderer: placementsRender,
          		        editor: {
          	                xtype: 'textfield',
          	                maxLength:45,
         	                enforceMaxLength:true,
          	            }
          			},{
          				text : 'Source Sublevel',
          				dataIndex : 'source_Sublevel',
          				renderer: sourceSublevelRender,
          				xtype: 'datecolumn',
          				editor: {
          	                xtype: 'datefield',
          	                format: 'm/d/y'
          	            }
          			},{
                        xtype: 'gridcolumn',
                        dataIndex: 'priority',
                        text: 'Priority',
                        width : 80,
          		        renderer: numberRender,
        				editor: {
                            xtype: 'numberfield',
                            minValue: 0,
                            maxValue: 100
                        }
                    },{
          		        xtype: 'gridcolumn',
          		        dataIndex: 'typeOfInformation',
          		        text: 'Type of Information',
          		        renderer: placementsRender,
          		        editor: {
          		        	xtype: 'combobox',
          	                queryMode: 'local',
          	                typeAhead: true,
          	                forceSelection:true,
          	                triggerAction: 'all',
          	                selectOnTab: true,
          	                displayField: 'value',
          	                valueField: 'name',
          	                emptyText:'Select...',
          	    		    store : ds.informationTypeStore,
          	                lazyRender: true,
          	                listClass: 'x-combo-list-small'
          	            }
          		    },{
          				text : 'Information',
          				dataIndex : 'information',
          				width :  120,
          		        renderer: placementsRender,
          		        editor: {
          	                xtype: 'textarea',
          	                height : 60,
          	                maxLength:1000,
         	                enforceMaxLength:true,
          	            }
          			},{
          				header : 'Hospital / Entity',
          				dataIndex : 'hospital_Entity',
          	            width :  80,
          		        renderer: placementsRender,
          		        editor: {
          	                xtype: 'textfield',
          	                maxLength:45,
         	                enforceMaxLength:true,
          	            }
          			},{
          				text : 'Location',
          				dataIndex : 'location',
          				width :  100,
          		        renderer: placementsRender,
          		        editor: {
          	                xtype: 'textfield',
          	                maxLength:150,
         	                enforceMaxLength:true,
          	            }
          			},{
          				text : 'State',
          				dataIndex : 'state',
          				width :  100,
          		        renderer: placementsRender,
          		        editor: {
          		        	xtype: 'combobox',
          	                queryMode: 'local',
          	                typeAhead: true,
          	                forceSelection:true,
          	                triggerAction: 'all',
          	                selectOnTab: true,
          	                displayField: 'value',
          	                valueField: 'name',
          	                emptyText:'Select...',
          	    		    store : ds.stateStore,
          	                lazyRender: true,
          	                listClass: 'x-combo-list-small'
          	            }
          			},{
          				text : 'Go Live Date',
          				dataIndex : 'liveDate',
          				renderer: placementsRender,
          				width :  80,
          				editor: {
          	                xtype: 'textfield',
          	                maxLength:50,
         	                enforceMaxLength:true,
          	            }
          			},{
          				text : 'Link 1',
          				dataIndex : 'link1',
          				renderer: linkRenderer,
          				width :  150,
          				editor: {
          	                xtype: 'textfield',
          	                maxLength:200,
         	                enforceMaxLength:true,
         	                vtype:'url',
          				},
          			},{
          				text : 'Link 2',
          				dataIndex : 'link2',
          				renderer: linkRenderer,
          				width :  80,
          				editor: {
          	                xtype: 'textfield',
          	                maxLength:200,
         	                enforceMaxLength:true,
         	                vtype:'url',
          				},
          			},{
          				xtype: 'gridcolumn',
		                dataIndex: 'outreach',
		                text: 'Outreach',
		                width:150,
          		        renderer: placementsRender,
		                editor: {
		                    xtype: 'textarea',
		                    height : 60,
		                    maxLength:1000,
		   	                enforceMaxLength:true,
		                }
		            },{
          				xtype: 'gridcolumn',
		                dataIndex: 'comments',
		                text: 'Comments',
		                width:150,
          		        renderer: placementsRender,
		                editor: {
		                    xtype: 'textarea',
		                    height : 60,
		                    maxLength:300,
		   	                enforceMaxLength:true,
		                }
		            },{
          		        xtype: 'gridcolumn',
          		        dataIndex: 'person',
          		        text: 'Person',
          		        renderer: placementsRender,
          		        editor: {
          	                xtype: 'textfield',
          	                maxLength:45,
         	                enforceMaxLength:true,
          	            }
          		    },{
          				text : 'Last Updated User',
          				dataIndex : 'lastUpdatedUser',
          		        renderer: placementsRender,
          			},{
                        xtype: 'datecolumn',
                        dataIndex: 'created',
                        text: 'Created',
                        format: "m/d/Y H:i:s A",
                        width:100,
                    },{
                        xtype: 'datecolumn',
                        dataIndex: 'lastUpdated',
                        format: "m/d/Y H:i:s A",
                        text: 'Last Updated',
                        width:100,
                    }
                  ];
        
        
        me.viewConfig = {
        		forceFit:true,
                stripeRows: true,
                listeners: {
                    dblClick: function (view, cell, cellIndex, record, row, rowIndex, e) {
                    	var colHeading = view.panel.headerCt.getHeaderAtIndex(cellIndex).text;
                    	if (colHeading =="Link 1" ||colHeading=="Link 2") {
                    		url = cell.textContent;
                    		//window.open(url,"_blank");
    					}
                    },
                }
        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Add New',
            		iconCls : 'btn-add',
            		tabIndex:13,
            		handler: function(){
            			me.addPlacement();
            		}
            	},'-',{
            		xtype: 'button',
            		text: 'Save',
            		iconCls : 'btn-save',
            		tabIndex:14,
            		handler: function(){
            			me.savePlacements();
            		}
            	},'-',{
                    xtype: 'button',
                    text: 'Delete',
                    iconCls : 'btn-delete',
                    tabIndex:15,
                    handler: function(){
                		me.deleteConfirm();
                	}
                },'-',{
            		xtype:'button',
                	itemId: 'grid-excel-button',
                	iconCls : 'btn-report-excel',
                	text: 'Export to Excel',
                	tabIndex:16,
                	handler: function(){
                		me.getExcelExport();
                	}
            	},'->',me.showCount]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			this.showCount.setValue(msg);
       	}, me);

        me.callParent(arguments);

        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);

    },
    
    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],

    getExcelExport : function() {
    	var values = app.placementMgmtPanel.placementSearchPanel.getValues();
    	var params = new Array();
    	params.push(['date','=', values.date]);
    	params.push(['person','=', values.person]);
    	params.push(['search','=', values.searchPlacements]);
    	params.push(['state','=', values.state]);
    	
    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;" && !this.columns[i].hidden) {
				columns.push(this.columns[i].text + ':'+this.columns[i].width);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.requirementService.placementsExcelExport(Ext.encode(filter));
	},

    addPlacement : function() {
	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.PlacementLeads',{
   	 		id : null,
   	 		hospital_Entity :'N/A',
   	 		location :'N/A',
   	 		liveDate :'N/A',
   	 		outreach :'N/A',
   	 		comments :'N/A',
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});

        this.store.insert(0, rec);
	},
    
    savePlacements : function() {
    	var records = new Array();

    	for ( var i = 0; i < ds.placementLeadsStore.data.length ; i++) {
			var record = ds.placementLeadsStore.getAt(i);
    		if (record.data.priority == null )
				delete record.data.priority;

    		if (record.data.id == 0 || record.data.id == null) {
    			delete record.data.id;
    			records.push(record.data);
			}
    		if (this.modifiedIds.indexOf(record.data.id) != -1) {
    			records.push(record.data);	
			}
		}
    	
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Placement Leads to save');
    		return;
		}

    	records = Ext.JSON.encode(records);
    	app.placementService.savePlacements(records,this.onSave,this);

	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error','Saved Successfully');
		this.modifiedIds =  new Array();
		app.placementMgmtPanel.placementSearchPanel.search();
	},

	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Placement Lead to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Placement Lead","Do you delete selected Placement Lead?", this.deletePlacement, this);
		}
	},
	
	deletePlacement: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.placementService.deletePlacement(selectedRecord.data.id, this.onDeletePlacement, this);
		}
	},
	
	onDeletePlacement : function(data){
		if (!data.success) { 
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.placementMgmtPanel.placementSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	},
	
});

function linkRenderer(value, metadata, record, rowIndex, colIndex, store) {
	metadata.tdAttr = 'data-qtip="' + value + '"';
	return '<a href='+value+' target="_blank">'+ value +'</a>'
}

function placementsRender(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null || value == "") 
		return 'N/A';
    return value;
}
function sourceSublevelRender(value, metadata, record, rowIndex, colIndex, store) {
	metadata.tdAttr = 'data-qtip="' + Ext.Date.format(value,'m/d/Y') + '"';
	return Ext.Date.format(value,'m/d/Y');
}
