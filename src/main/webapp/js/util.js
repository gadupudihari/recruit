
function isThere(obj, attr){
	if(!obj[attr] || obj[attr] ==''){
		delete obj[attr];
	}
}

function fixConsole(alertFallback)
{    
    if (typeof console === "undefined")
    {
        console = {}; // define it if it doesn't exist already
    }
    if (typeof console.log === "undefined") 
    {
        if (alertFallback) { console.log = function(msg) { alert(msg); }; } 
        else { console.log = function() {}; }
    }
    if (typeof console.dir === "undefined") 
    {
        if (alertFallback) 
        { 
            // THIS COULD BE IMPROVEDů maybe list all the object properties?
            console.dir = function(obj) { alert("DIR: "+obj); }; 
        }
        else { console.dir = function() {}; }
    }
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };

fixConsole();


function getFilter(params){
	var pArray = new Array();
	for(var i=0; i< params.length; i++){
		var p = params[i];
		if(p[2] && p[2] != ''){
			pArray.push(new Object({
					id : p[0],
					operand : p[1],
					strValue : p[2]
				}
			));
		}
	}

	var filter = {
			pageNo : 0,
			pageSize : 50,
			params : pArray
	};
	
	return filter;

}

function formatDate(d) {
	if(d){
		return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
	}
}