//local
Ext.Loader.setConfig({
    enabled: true
});

Ext.define('app.global.Vars', {
    singleton: true,
    dirtyFlag: undefined
});

Ext.Loader.setPath('Ext.ux', 'js/extjs/ux');
 
Ext.require([
             'Ext.selection.CellModel',
             'Ext.grid.*',
             'Ext.data.*',
             'Ext.util.*',
             'Ext.state.*',
             'Ext.form.*',
             'Ext.ux.CheckColumn',
             'Ext.ux.form.NumericField'
         ]);


Ext.define('tz.App', {
    constructor: function(name) {   
    	
    	//Services
    	this.employeeService = new tz.service.EmployeeService();
		this.settingsService = new tz.service.SettingsService();
		this.requirementService = new tz.service.RequirementService();
		this.candidateService = new tz.service.CandidateService();
		this.placementService = new tz.service.PlacementService();
		this.clientService = new tz.service.ClientService();
		this.contactService = new tz.service.ContactService();
		this.interviewService = new tz.service.InterviewService();
		this.reportsService = new tz.service.ReportsService();
		this.quickAccessService = new tz.service.QuickAccessService();
		
    	//Default loading mask
    	this.loadMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
    	this.saveMask = new Ext.LoadMask(Ext.getBody(), {msg:"Saving..."});
    	this.loadGenerateMask = new Ext.LoadMask(Ext.getBody(), {msg:"Generating Please wait..."});
    	this.processingMask = new Ext.LoadMask(Ext.getBody(), {msg:"Processing..."});
    	this.sendMask = new Ext.LoadMask(Ext.getBody(), {msg:"Sending..."});
    	
    	//this.iPLoggerGridPanel = new tz.ui.IPLoggerGridPanel();
    	
    	this.loginId=LoggedId;
    	this.userRole = userRole;
    	this.windowXPosition = 0;
    	this.windowYPosition = 0;
    	this.windowId =0;
    	
    	if(userRole == 'ADMIN' ){
        	this.navigationStr =
        		/*'<div id=\'cssmenu\'>'+
    			'<div id="bg-one"></div><div id="bg-two"></div><div id="bg-three"></div><div id="bg-four"></div>'+
    			'<ul>'+
    			'<li class=\'active\' id="li_home"><a onclick="javascript:app.setActiveTab(0, \'li_home\')" id="li_home" href="#home" title="Home" style="color:#FFFFFF;font-size: 14px;"><span>Home</span></a></li>'+
    			'<li id="li_requirements"><a onclick="javascript:app.setActiveTab(9, \'li_requirements\')" id="li_requirements" href="#jobOpenings" title="Job Openings" style="color:#FFFFFF;font-size: 14px;"><span>Job Openings</span></a></li>'+
    			'<li id="li_candidates"><a onclick="javascript:app.setActiveTab(4, \'li_candidates\')" id="li_candidates" href="#candidates" title="Candidates" style="color:#FFFFFF;font-size: 14px;"><span>Candidates<span></a></li>'+
    			'<li id="li_clients"><a onclick="javascript:app.setActiveTab(2, \'li_clients\')" id="li_clients" href="#clients" title="Clients" style="color:#FFFFFF;font-size: 14px;"><span>Clients/Vendors</span></a></li>'+
    			'<li id="li_contacts"><a onclick="javascript:app.setActiveTab(1, \'li_contacts\')" id="li_contacts" href="#contacts" title="Contacts" style="color:#FFFFFF;font-size: 14px;"><span>Contacts</span></a></li>'+
				'<li id="li_interviews"><a onclick="javascript:app.setActiveTab(3, \'li_interviews\')" id="li_interviews" href="#interviews" title="Interviews" style="color:#FFFFFF;font-size: 14px;"><span>Interviews</span></a></li>'+
				'<li id="li_placements"><a onclick="javascript:app.setActiveTab(9, \'li_placements\')" id="li_placements" href="#placements" title="Placement Leads" style="color:#FFFFFF;font-size: 14px;"><span>Placement Leads</span></a></li>'+
				'<li id="li_reports"><a onclick="javascript:app.setActiveTab(8, \'li_reports\')" id="li_reports" href="#reports" title="Reports" style="color:#FFFFFF;font-size: 14px;"><span>Reports</span></a></li>'+
				'<li id="li_users"><a onclick="javascript:app.setActiveTab(9, \'li_users\')" id="li_users" href="#tools" title="Users" style="color:#FFFFFF;font-size: 14px;"><span>Users</span></a></li>'+
				'<li id="li_settings"><a onclick="javascript:app.setActiveTab(9, \'li_settings\')" id="li_settings" href="#settings" title="Settings" style="color:#FFFFFF;font-size: 14px;"><span>Settings</span></a></li>'+
    			'</ul>'+
    			'</div>';*/
        		
        		
    			'<table style="background: #3E6B05;border-spacing: 0;"><tr class="headingRow"> '+
    				'<td id="li_home1" class="headingTab" ><li><a onclick="javascript:app.setActiveTab(9, \'li_home\')" id="li_home" href="#home" title="Home" style="color:#FFFFFF;font-size: 15px;">Home</a></li></td>'+
    				'<td id="li_requirements1" class="headingTab" <li><a onclick="javascript:app.setActiveTab(9, \'li_requirements\')" id="li_requirements" href="#jobOpenings" title="Job Openings" style="color:#FFFFFF;font-size: 15px;">Job Openings</a></li></td>'+
					'<td id="li_candidates1" class="headingTab" ><li><a onclick="javascript:app.setActiveTab(3, \'li_candidates\')" id="li_candidates" href="#candidates" title="candidates" style="color:#FFFFFF;font-size: 15px;">Candidates</a></li></td>'+
					'<td id="li_clients1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_clients\')" id="li_clients" href="#clients" title="Clients" style="color:#FFFFFF;font-size: 15px;">Clients/Vendors</a></li></td>'+
					'<td id="li_contacts1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_contacts\')" id="li_contacts" href="#contacts" title="Contacts" style="color:#FFFFFF;font-size: 15px;">Contacts</a></li></td>'+
					'<td id="li_interviews1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_interviews\')" id="li_interviews" href="#interviews" title="Interviews" style="color:#FFFFFF;font-size: 15px;">Interviews</a></li></td>'+
					'<td id="li_placements1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_placements\')" id="li_placements" href="#placements" title="Placement Leads" style="color:#FFFFFF;font-size: 15px;">Placement Leads</a></li></td>'+
					'<td id="li_reports1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_reports\')" id="li_reports" href="#reports" title="Reports" style="color:#FFFFFF;font-size: 15px;">Reports</a></li></td>'+
					'<td id="li_users1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_users\')" id="li_users" href="#users" title="Users" style="color:#FFFFFF;font-size: 15px;">Users</a></li></td>'+
					'<td id="li_settings1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_settings\')" id="li_settings" href="#settings" title="Settings" style="color:#FFFFFF;font-size: 15px;">Settings</a></li></td>'+
				'</tr></table>';
    	}else if (userRole == 'RECRUITER' ) {
        	this.navigationStr =
    			'<table style="background: #3E6B05;border-spacing: 0;"><tr class="headingRow"> '+
    				'<td id="li_home1" class="headingTab" onMouseOver="this.bgColor=\'#363636\'" onMouseOut="this.bgColor=\'#157fcc\'"><li><a onclick="javascript:app.setActiveTab(9, \'li_home\')" id="li_home" href="#home" title="Home" style="color:#FFFFFF;font-size: 15px;">Home</a></li></td>'+
    				'<td id="li_requirements1" class="headingTab" ><li><a onclick="javascript:app.setActiveTab(9, \'li_requirements\')" id="li_requirements" href="#jobOpenings" title="Job Openings" style="color:#FFFFFF;font-size: 15px;">Job Openings</a></li></td>'+
					'<td id="li_candidates1" class="headingTab" ><li><a onclick="javascript:app.setActiveTab(3, \'li_candidates\')" id="li_candidates" href="#candidates" title="candidates" style="color:#FFFFFF;font-size: 15px;">Candidates</a></li></td>'+
					'<td id="li_clients1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_clients\')" id="li_clients" href="#clients" title="Clients" style="color:#FFFFFF;font-size: 15px;">Clients/Vendors</a></li></td>'+
					'<td id="li_contacts1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_contacts\')" id="li_contacts" href="#contacts" title="Contacts" style="color:#FFFFFF;font-size: 15px;">Contacts</a></li></td>'+
					'<td id="li_interviews1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_interviews\')" id="li_interviews" href="#interviews" title="Interviews" style="color:#FFFFFF;font-size: 15px;">Interviews</a></li></td>'+
					'<td id="li_placements1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_placements\')" id="li_placements" href="#placements" title="Placement Leads" style="color:#FFFFFF;font-size: 15px;">Placement Leads</a></li></td>'+
					'<td id="li_reports1" class="headingTab"><li><a onclick="javascript:app.setActiveTab(9, \'li_reports\')" id="li_reports" href="#reports" title="Reports" style="color:#FFFFFF;font-size: 15px;">Reports</a></li></td>'+
				'</tr></table>';
		}
    	
    	this.centerLayout = new Ext.create('Ext.panel.Panel',{
   			 region: 'center',
			 id: 'mainLayoutPanel',
			 border: false,
			 layout: 'card',
			 items: [ 
			          {
			              xtype : "component",
			              itemId: 'reportMgmtPanel',
			              autoEl : {
			                  tag : "iframe",
			              }
			          }
			          ]
		 });
    	   
    	var options = '';
    	for ( var i = 0; i < ds.quickAccessStore.getCount(); i++) {
    		options += '<option>' + ds.quickAccessStore.getAt(i).data.name + '</option>';
		}

    	//This layout will have all the panels enclosed
    	this.mainViewport = Ext.create('Ext.container.Viewport',{
    		 layout: 'border',
    		 items: [{
    			 region: 'north',
    			 html:  
					'<div id="nhdrwrapinner" class="header_center_image">'+
					  '<div id="nhdrwrapsizer">'+
					    '<div id="gsea">'+
						  '<form id="sfrm" name="f" method="get" action="/search" >'+
						    '<table id="gsea_table" cellpadding="0" cellspacing="0">'+
								'<tbody>'+
									'<tr>'+
										'<td align="center" class="gseain" valign="top" width="80%">'+
										'<img src="/recruit/images/Full-AJ-recruit-logo.PNG" height="40" width="100" ></img>'+
										'</td>'+
										'<td width="5%" style="text-align: right;font-weight:bold;padding-top: 15px;">'+
										'<a onclick="javascript:app.showQuickAdd()">Quickadd</a>'+
										'</td>'+ 
										'<td width="5%" style="text-align: right;font-weight:bold;padding-top: 15px;">'+
										'<a onclick="javascript:app.showQuickSearch()">Snapshot</a>'+
										'</td>'+ 
										'<td width="10%">'+
										'<div style="height:1em;text-align: right;">Logged in as '+LoggedId+ '&nbsp;&nbsp;</div><div id="loginIdVal" style="display:none">'+LoggedId+'</div>'+
										'<p style="text-align: right;">'+
											'<aclass="help"> Help</a>&nbsp;|'+
											'<a onclick="javascript:app.setActiveTab(9, \'li_changePassword\')" id="li_changePassword" class="feedback cboxElement">Change Password</a>&nbsp;|'+
											'<a href="/recruit/j_spring_security_logout">Logout</a>&nbsp;'+
										'</p>'+
										'</td>'+ 
									'</tr>'+
								'</tbody>'+
					          '</table>'+
						   '</form>'+
					     '</div>'+
					  '</div>'+
					'</div> '+   			   				 
					'<div class="headingBar"> '+
						this.navigationStr+
					'</div>'+
					'<div class="colorBar"><div class="canvas"></div></div>'  ,					
    			 border: false,
    			 margins: '0 0 5 0'
    		 },this.centerLayout]
    	});		 
    	console.log(userRole);
    },

    //Changes the active tab based on the user selection and flips the card layout
    setActiveTab: function(index, refId){
    	this.mainLayout = Ext.getCmp('mainLayoutPanel');
    	
    	if(refId && document.getElementById(refId)){
    		if (refId == 'li_changePassword')
    			document.getElementById(refId).className ="passwordNavigationBar";
    		else{
    			document.getElementById(refId).className ="activeNavigationBar";
    			document.getElementById(refId+"1").style.backgroundColor = "#363636";
    		}
    	}
    	
    	if(!this.prevId){
    		this.prevId = "li_home";
    	}
    	if(refId != this.prevId){
    		document.getElementById(this.prevId).className = ""; //"gbzt";
    		document.getElementById(this.prevId+"1").style.backgroundColor = "#157fcc";
    	}
    	this.prevId = refId;
    	
    	//Invoke the init action on the corresponding panel
    	//TODO : Not a clean way 
    	if(refId == "li_contacts"){
    		this.getContactsTab();
    	}else if(refId == "li_changePassword"){
    		this.getPasswordTab();
    	}else if (refId == "li_settings") {
    		this.getSettingsTab();
    	}else if (refId == "li_requirements") {
    		this.getRecruitmentTab();
		}else if (refId == "li_candidates") {
			this.getCandidateTab();		
		}else if (refId == "li_placements") {
			this.getPlacementTab();
		}else if (refId == "li_clients") {
			this.getClientTab();
		}else if (refId == "li_users") {
			this.getUserTab();
		}else if(refId == "li_home"){
   			this.getHomeTab();
		}else if (refId == "li_interviews") {
			this.getInterviewsTab();
		}else if (refId == "li_reports") {
			this.getReportsTab();
		}else if (refId == "li_quickAccess") {
			this.getQuickAccessTab();
		}else if (refId == "li_home") {
			this.getHomeTab()
		}
    },

    getInterviewsTab : function() {
		if(this.interviewMgmtPanel == null){
			this.interviewMgmtPanel = new tz.ui.InterviewMgmtPanel({itemId:'interviewMgmtPanel'});
    		this.centerLayout.add(this.interviewMgmtPanel);
		}
		this.centerLayout.getLayout().setActiveItem('interviewMgmtPanel');
		this.interviewMgmtPanel.initScreen();
	},
	
    getHomeTab : function() {
    	this.centerLayout.getLayout().setActiveItem(0);
		if(this.homeMgmtPanel == null){
			this.homeMgmtPanel = new tz.ui.HomeMgmtPanel({itemId:'homeMgmtPanel'});
    		this.centerLayout.add(this.homeMgmtPanel);
		}
		this.centerLayout.getLayout().setActiveItem('homeMgmtPanel');
		this.homeMgmtPanel.initScreen();
		this.homeMgmtPanel.setHeights();
	},
    
    getPasswordTab : function() {
		if(this.employeePasswordPanel == null){
			this.employeePasswordPanel = new tz.ui.EmployeePasswordPanel({itemId:'employeePasswordPanel'});
    		this.centerLayout.add(this.employeePasswordPanel);
		}
		this.centerLayout.getLayout().setActiveItem('employeePasswordPanel');
		this.employeePasswordPanel.initScreen();
    },
    
    getContactsTab : function() {
		if(this.contactMgmtPanel == null){
			this.contactMgmtPanel = new tz.ui.ContactMgmtPanel({itemId:'contactMgmtPanel'});
    		this.centerLayout.add(this.contactMgmtPanel);
		}
		this.centerLayout.getLayout().setActiveItem('contactMgmtPanel');
		this.contactMgmtPanel.initScreen();
	},
    
    getSettingsTab : function() {
		if(this.settingsMgmtPanel == null){
			this.settingsMgmtPanel = new tz.ui.SettingsMgmtPanel({itemId:'settingsMgmtPanel'});
    		this.centerLayout.add(this.settingsMgmtPanel);
		}
		this.centerLayout.getLayout().setActiveItem('settingsMgmtPanel');
		this.settingsMgmtPanel.initScreen();
	},

    getRecruitmentTab : function() {
		if(this.requirementMgmtPanel == null){
			this.requirementMgmtPanel = new tz.ui.RequirementMgmtPanel({itemId:'requirementMgmtPanel'});
    		this.centerLayout.add(this.requirementMgmtPanel);
		}    		
		this.centerLayout.getLayout().setActiveItem('requirementMgmtPanel');
		this.requirementMgmtPanel.initScreen();
	},

    getCandidateTab : function() {
		if(this.candidateMgmtPanel == null){
			this.candidateMgmtPanel = new tz.ui.CandidateMgmtPanel({itemId:'candidateMgmtPanel'});
    		this.centerLayout.add(this.candidateMgmtPanel);
		}    		
		this.centerLayout.getLayout().setActiveItem('candidateMgmtPanel');
		this.candidateMgmtPanel.initScreen();
	},

    getPlacementTab : function() {
		if(this.placementMgmtPanel == null){
			this.placementMgmtPanel = new tz.ui.PlacementMgmtPanel({itemId:'placementMgmtPanel'});
    		this.centerLayout.add(this.placementMgmtPanel);
		}    		
		this.centerLayout.getLayout().setActiveItem('placementMgmtPanel');
		this.placementMgmtPanel.initScreen();
	},

    getClientTab : function() {
		if(this.clientMgmtPanel == null){
			this.clientMgmtPanel = new tz.ui.ClientMgmtPanel({itemId:'ClientMgmtPanel'});
    		this.centerLayout.add(this.clientMgmtPanel);
		}    		
		this.centerLayout.getLayout().setActiveItem('ClientMgmtPanel');
		this.clientMgmtPanel.initScreen();
	},

    getUserTab : function() {
		if(this.userMgmtPanel == null){
			this.userMgmtPanel = new tz.ui.UserMgmtPanel({itemId:'userMgmtPanel'});
    		this.centerLayout.add(this.userMgmtPanel);
		}    		
		this.centerLayout.getLayout().setActiveItem('userMgmtPanel');
		this.userMgmtPanel.initScreen();
	},
    
	getReportsTab : function() {
		if(this.reportsMgmtPanel == null){
			this.reportsMgmtPanel = new tz.ui.ReportsMgmtPanel({itemId:'reportsMgmtPanel'});
    		this.centerLayout.add(this.reportsMgmtPanel);
		}    		
		this.centerLayout.getLayout().setActiveItem('reportsMgmtPanel');
		this.reportsMgmtPanel.initScreen();
	},
	
	getQuickAccessTab : function() {
		if(this.quickAccessMgmtPanel == null){
			this.quickAccessMgmtPanel = new tz.ui.QuickAccessMgmtPanel({itemId:'quickAccessMgmtPanel'});
    		this.centerLayout.add(this.quickAccessMgmtPanel);
		}    		
		this.centerLayout.getLayout().setActiveItem('quickAccessMgmtPanel');
		this.quickAccessMgmtPanel.initScreen();
	},
	
	showQuickSearch : function() {
		if (this.checkWindowCount() >= 10){
			Ext.Msg.alert('Error','You have exceeded maximum no of windows(10).Please close one of them.');
			return ;
		}

        var quickAccessCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Quick Access Search',
		    store: ds.quickAccessStore,
		    padding:'0 0 0 0',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'id',
		    name : 'name',
		    //forceSelection : true,
		    labelAlign : 'right',
		    anyMatch:true,
		    width :350,
		    labelWidth :120,
   			listeners: {
		    	select:function(combo, records, eOpts) {
		    		var record = ds.quickAccessStore.getById(combo.getValue());
                	app.showSnapshot(record);
		    	}
			},
			listConfig: {
		        cls: 'grouped-list'
		      },
		      tpl: Ext.create('Ext.XTemplate',
		        '{[this.currentKey = null]}' +
		        '<tpl for=".">',
		          '<tpl if="this.shouldShowHeader(type)">' +
		            '<div class="group-header">{[this.showHeader(values.type)]}</div>' +
		          '</tpl>' +
		          '<div class="x-boundlist-item">{name}</div>',
		        '</tpl>',
		        {
		          shouldShowHeader: function(name){
		            return this.currentKey != name;
		          },
		          showHeader: function(name){
		            this.currentKey = name;
		            return name;
		          }
		        }
		      )
		});

        var quickSearchPanel = Ext.create('Ext.form.Panel', {
        	bodyPadding: 5,
        	border : 0,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
            items : [{
            	xtype:'container',
   	         	layout: 'column',
   	         	items :[quickAccessCombo,
   	                    {
                    xtype:'button',
                    margin: '0 0 0 20',
                    iconCls : 'btn-Search',
                    text: 'Search',
                    handler: function(){
                    	var record = ds.quickAccessStore.getById(quickAccessCombo.getValue());
                    	if (record != null)
                    		app.showSnapshot(record);
                    	else
                    		app.showReqSnapshot(quickAccessCombo.getValue());
                    }
                },{
                    xtype:'button',
                    margin: '0 0 0 10',
                    text: 'Clear',
                    handler: function(){
                    	quickAccessCombo.reset();
                    }
                }]
            }]
        });
  
        if (Ext.getCmp('snapshotSearchWindow') != null) {
        	Ext.getCmp('snapshotSearchWindow').close();
        }
        
		var snapshotSearchWindow = Ext.create('Ext.window.Window', {
		    title: '',
		    id : 'snapshotSearchWindow',
		    modal: false,
		    bodyPadding: 0,
		    layout: 'fit',
		    items: [quickSearchPanel],
		});
		Ext.getCmp('snapshotSearchWindow').show();
		quickAccessCombo.focus('', 10);  
		snapshotSearchWindow.alignTo(Ext.getBody(), 'tr-tr',[0,90]);
	},
	
	showSnapshot : function(record) {
		if (Ext.getCmp('snapshotSearchWindow') != null)
			Ext.getCmp('snapshotSearchWindow').close();
    	var browserHeight = app.mainViewport.height;
    	var browserWidth = app.mainViewport.width;

    	var quickAccessPanel = new tz.ui.QuickAccessPanel({itemId:'snapshot'});
    	var quickCandidateStore = new tz.store.CandidateStore();
    	var quickRequirementStore = new tz.store.Sub_RequirementStore();
    	var quickVendorStore = new tz.store.ClientStore();
    	var quickClientStore = new tz.store.ClientStore();
    	var quickContactStore = new tz.store.ContactStore();
    	var quickInterviewStore = new tz.store.InterviewStore();
    	var quickCertificationStore = new tz.store.CertificationStore();
    	var quickProjectStore = new tz.store.TNEProjectSummaryStore();
    	quickAccessPanel.candidatesGrid.bindStore(quickCandidateStore);
    	quickAccessPanel.requirementsGrid.bindStore(quickRequirementStore);
    	quickAccessPanel.vendorGrid.bindStore(quickVendorStore);
    	quickAccessPanel.clientGrid.bindStore(quickClientStore);
    	quickAccessPanel.contactGrid.bindStore(quickContactStore);
    	quickAccessPanel.interviewGrid.bindStore(quickInterviewStore);
    	quickAccessPanel.certificationGrid.bindStore(quickCertificationStore);
    	quickAccessPanel.projectGrid.bindStore(quickProjectStore);
    	quickAccessPanel.reset();
    	quickAccessPanel.quickAccessCombo.setValue(record.data.id);
    	quickAccessPanel.show_hideGrids(record.data.type);
    	quickAccessPanel.search();
    	quickAccessPanel.searchPanel.hide();
    	quickAccessPanel.myPanel.setHeight(browserHeight-140);
    	
    	if (this.windowXPosition+200 > browserWidth ){
    		this.windowXPosition = 0;
    		this.windowYPosition = -60;
    	}
    	var windowXPosition = this.windowXPosition;
    	this.windowXPosition += 150;
    	var windowYPosition = this.windowYPosition;
    	this.windowId += 1;
		var snapshotWindow = Ext.create('Ext.window.Window', {
			title: record.data.name,
			height : browserHeight-100,
			width: browserWidth*70/100,
			id : 'window'+this.windowId,
			modal: false,
			minimizable: true,
			layout: 'fit',
			border : 0,
			listeners: {
				"minimize": function (window, opts) {
					window.collapse();
					window.setWidth(150);
					window.alignTo(Ext.getBody(), 'bl-bl',[windowXPosition,windowYPosition]);
				},
				"close": function (window, opts) {
					app.arrangeWindows();
				}
			},
			tools: [{
				type: 'prev',
				handler: function (evt, toolEl, owner, tool) {
					var window = owner.up('window');
					window.alignTo(Ext.getBody(), 'tl-tl',[0,90],true);
				}
			},{
				type: 'next',
				handler: function (evt, toolEl, owner, tool) {
					var window = owner.up('window');
					window.alignTo(Ext.getBody(), 'tr-tr',[0,90],true);                
				}
			},{
				type: 'restore',
				handler: function (evt, toolEl, owner, tool) {
					var window = owner.up('window');
					window.setWidth(browserWidth*70/100);
					window.expand('', false);
					window.center();
				}
			}],
			items: [quickAccessPanel],
		});
		snapshotWindow.show();
	},
	
	showReqSnapshot : function(reqId) {
		//entered valued is number or not
		if (! isNaN(Number(reqId))) {
			if (Ext.getCmp('snapshotSearchWindow') != null)
				Ext.getCmp('snapshotSearchWindow').close();
			var quickAccessPanel = new tz.ui.QuickAccessPanel({itemId:'snapshot'});
			quickAccessPanel.showReqSnapshot(reqId);
		}else{
			Ext.Msg.alert('Error','Invalid input. Please enter properly.');
		}
	},
	
	checkWindowCount : function() {
		var count =0;
		for ( var i = 0; i <= this.windowId; i++) {
			if (Ext.getCmp('window'+i) != null && Ext.getCmp('window'+i).hidden != true) 
				count++;
		}
		return count;
	},
	
	arrangeWindows : function() {
		this.windowXPosition = 0;
		this.windowYPosition = 0;
		var browserWidth = app.mainViewport.width;
		for ( var i = 0; i <= this.windowId; i++) {
	    	if (this.windowXPosition+200 > browserWidth ){
	    		this.windowXPosition = 0;
	    		this.windowYPosition = -60;
	    	}
			if (Ext.getCmp('window'+i) != null && Ext.getCmp('window'+i).hidden != true) {
				Ext.getCmp('window'+i).alignTo(Ext.getBody(), 'bl-bl',[this.windowXPosition,this.windowYPosition]);
				this.windowXPosition += 150;
			}
		}
	},
	
	showQuickAdd : function() {
		app.homeMgmtPanel.showQuickAdd();
	},
	
    updateCrumb: function(text){
    	//document.getElementById('breadCrumb').innerHTML = text;	
    },
    
    pushCrumb: function (title, cb){    	
    	
    },
    
    popCrump: function(){
    	
    }
});

// Available throghout the app
var app;

// Datastore collection for the app
var ds;

//Util class
var util;

var config = {pageSize:50};

Ext.onReady(function() {

// any tag-based quick tips will start working.
Ext.tip.QuickTipManager.init();
// init quicktips
Ext.apply(Ext.tip.QuickTipManager.getQuickTip(), {
    // setup quicktip settings
    maxWidth: 900,      
    animate: true,
    trackMouse: true,
    showDelay: 50,		 // show after 50 ms
    dismissDelay: 60000  // hide after 10 seconds hover
});
	Ext.QuickTips.init();	

	// setup the state provider, all state information will be saved to a cookie
	Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

	// Util class
	util = new tz.Util();
	
	ds = new tz.DataStores();
	
	// Create the vendor grid
	app = new tz.App();
		
		app.setActiveTab(0, 'li_home');
		
		//Session Time out Code
		
		 Ext.ns('App');

		    App.BTN_OK = 'ok';
		    App.BTN_YES = 'yes';
		    // 1 min. before notifying the user her session will expire. Change this to a reasonable interval.
		    App.SESSION_ABOUT_TO_TIMEOUT_PROMT_INTERVAL_IN_MIN = 60;
		    // 1 min. to kill the session after the user is notified.
		    App.GRACE_PERIOD_BEFORE_EXPIRING_SESSION_IN_MIN = .25;
		    // The page that kills the server-side session variables.
		    App.SESSION_KILL_URL = 'timeout.jsp';

		    // Helper that converts minutes to milliseconds.
		    App.toMilliseconds = function (minutes) {
		        return minutes * 3 * 60 * 1000;
		    }

		    // Helper that simulates AJAX request.
		    App.simulateAjaxRequest = function () {

		        Ext.Ajax.request({
		            url: ' ',
		            success: Ext.emptyFn,
		            failure: Ext.emptyFn
		        });
		    }

		    // Helper that simulates request to kill server-side session variables.
		    App.simulateAjaxRequestToKillServerSession = function () {

		        Ext.Ajax.request({
		            url: App.SESSION_KILL_URL,
		            success: Ext.emptyFn,
		            failure: Ext.emptyFn
		        });
		    }

		    // Notifies user that the session is about to time out.
		    App.sessionAboutToTimeoutPromptTask = new Ext.util.DelayedTask(function () {

		        console.log('sessionAboutToTimeoutPromptTask');
		        App.simulateAjaxRequestToKillServerSession();

		        App.killSessionTask.delay(App.toMilliseconds(
		          App.GRACE_PERIOD_BEFORE_EXPIRING_SESSION_IN_MIN));
		    });

		    // Schedules a request to kill server-side session.
		    App.killSessionTask = new Ext.util.DelayedTask(function () {
		        console.log('killSessionTask');
		        App.simulateAjaxRequestToKillServerSession();
		    });

		    // Starts the session timeout workflow after an AJAX request completes.
		    Ext.Ajax.on('requestcomplete', function (conn, response, options) {

		        if (options.url !== App.SESSION_KILL_URL) {
		            // Reset the client-side session timeout timers.
		            // Note that you must not reset if the request was to kill the server-side session.
		            App.sessionAboutToTimeoutPromptTask.delay(App.toMilliseconds(App.SESSION_ABOUT_TO_TIMEOUT_PROMT_INTERVAL_IN_MIN));
		            App.killSessionTask.cancel();
		        } else {
		            // Notify user her session timed out.
		    	if (!(options.url !== App.SESSION_KILL_URL)){
		        	Ext.MessageBox.show({
			            title: 'Session Expired',
			            msg: 'Your session expired. Please login again !!',
						buttons: Ext.Msg.OK,
						closable :false,
			            fn: function(btn) {
							if (btn == 'ok') {
								// TODO: Show logon form here.
		                    	window.location.replace('/recruit/j_spring_security_logout');
							}
						}
			        });
		    	}
		       }
		    });

		    // The rest of your app's code would go here. I will just simulate
		    // an AJAX request so the session timeout workflow gets started.
		    App.simulateAjaxRequest();
		//End of Session Time Out Code
});

Ext.define('tz.Util', {
	constructor : function(name) {
	},
	updateMessage : function(divId, msg) {
		this.gramLogTmpl.apply(wrapper);
	}
});
