Ext.define('tz.ui.ContactMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.contactGridPanel = new tz.ui.ContactGridPanel({manager:me});
    	me.contactSearchPanel = new tz.ui.ContactSearchPanel({manager:me});
    	me.contactDetailPanel = new tz.ui.ContactDetailPanel({manager:me});
    	
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [ this.contactSearchPanel,
 				        {
		   	        		height: 10,
		   	        		border : false
 				        },
 				        this.contactGridPanel
 				        ]
            },{
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [this.contactDetailPanel]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen: function(){
    	var browserHeight = app.mainViewport.height;
    	var searchPanelHt = Ext.getCmp('contactSearchPanel').getHeight();
		var headerHt = app.mainViewport.items.items[0].getHeight();
    	Ext.getCmp('contactGridPanel').setHeight(browserHeight - searchPanelHt - headerHt - 40);
    	ds.contactTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Contact Type'])));
    	ds.smeTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'SME Type'])));
    	ds.contactRoleStore.loadByCriteria(getFilter(new Array(['type','=', 'Contact Role'])));
    	
    	this.getLayout().setActiveItem(0);
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.client_vendorStore.loadByCriteria(filter);
    	if (ds.contactStore.getCount() ==0) 
    		this.contactSearchPanel.search();	
		var columns = this.contactGridPanel.getView().getGridColumns();
		if (app.userRole == 'ADMIN'){
			for ( var i = 0; i < columns.length ; i++) {
				if(columns[i].dataIndex == "confidentiality"){
					columns[i].hidden = false;
					break;
				}
			}
		}else{
			for ( var i = 0; i < columns.length ; i++) {
				if(columns[i].dataIndex == "confidentiality"){
					columns[i].setText('');
					columns[i].hidden = true;
					break;
				}
			}
		}
		this.contactGridPanel.getView().refresh();
	},
	
	showContactPanel : function() {
		this.getLayout().setActiveItem(1);
		this.contactDetailPanel.deleteButton.disable();
	}
	
});
