Ext.define('tz.ui.ContactDetailPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 10,
    title: '',
    anchor:'100% 100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right', labelWidth : 120},
    
    initComponent: function() {
        var me = this;
		me.closeContact = false;
		me.modified = false;
		me.opendFrom ='';
		me.candidateId ='';
		me.contact = null;
		
		me.projectGridPanel = new tz.ui.ProjectGridPanel({manager:me});
		
		me.deleteButton = new Ext.Button({
            text: 'Delete',
            tooltip: 'Delete Contact',
            iconCls : 'btn-delete',
            scope : this,
            tabIndex:43,
            handler: function(){
    			this.deleteConfirm();
    		}
        });

        me.saveAndCloseButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & Close',
            tooltip: 'Save & Close',
            iconCls : 'btn-save',
            tabIndex:42,
            scope: this,
	        handler: function() {
				this.saveAndClose();
			}
        });

        me.linkCandidateButton = new Ext.Button({
        	xtype: 'button',
            text: 'Link Candidate',
            tooltip: 'Link Candidate',
            icon : 'images/icon_link.gif',
            tabIndex:44,
            scope: this,
	        handler: function() {
				this.linkCandidateConfirm();
			}
        });

        me.unlinkCandidateButton = new Ext.Button({
        	xtype: 'button',
            text: 'Unlink Candidate',
            tooltip: 'Unlink Candidate',
            icon : 'images/icon_link.gif',
            tabIndex:45,
            scope: this,
	        handler: function() {
				this.unlinkCandidateConfirm();
			}
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Save',
                        tooltip: 'Save contact',
                        iconCls : 'btn-save',
                        scope: this,
                        tabIndex:41,
            	        handler: this.save
                    },'-',
                    me.saveAndCloseButton,'-',me.deleteButton,'-',me.linkCandidateButton,me.unlinkCandidateButton,
                    '-',{
                        xtype: 'button',
                        text: 'Close',
                        tooltip: 'Close Contact',
                        iconCls : 'btn-close',
                        tabIndex:46,
                        scope : this,
                        handler: function(){
                        	this.closeForm();
                        }
                    }
                ]
            }
        ];

		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client/Vendor',
		    queryMode: 'local',
		    forceSelection:true,
            displayField: 'name',
            valueField: 'id',
		    name : 'clientId',
		    store: ds.client_vendorStore,
		    tabIndex:4,
		    listeners: {
		        change : function(combo, store){
		        	var record = this.store.getById(combo.value);
		        	if (record)
		        		me.form.findField('clientType').setValue(record.data.type);
		        	else
		        		me.form.findField('clientType').setValue('');
		        }
		    }

		});

		me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    queryMode: 'local',
		    forceSelection:true,
            displayField: 'value',
            valueField: 'name',
		    name : 'type',
		    store: ds.contactTypeStore,
		    tabIndex:8,
		    multiSelect: true,
		    //width : 350,
		    listeners: {
		        change : function(combo, newValue, oldValue){
		        	me.form.findField('SMEComments').hide();
		        	me.form.findField('placementComments').hide();
		        	Ext.getCmp('Candidate_SkillSet_Fieldset').collapse();
		        	Ext.getCmp('Contact_SkillSet_Fieldset').collapse();
		        	Ext.getCmp('Contact_Projects_Fieldset').collapse();
		        	Ext.getCmp('Contact_Comments_Fieldset').hide();
	        		me.moduleCombo.hide();
	        		me.addModuleButton.hide();
	        		me.form.findField('module').hide();
	        		me.roleSetCombo.hide();
	        		me.addRolesetButton.hide();
	        		me.form.findField('roleSet').hide();
	        		me.form.findField('priority').hide();
	        		me.roleCombo.show();
	        		me.form.findField('role').show();
		        	if (combo.value != null ){
			        	var candidateId = me.form.findField('candidateId').getValue();
			        	if (me.form.findField('candidateDeleted').getValue() == 'true')
			        		candidateId = null;
			        	Ext.getCmp('Contact_Comments_Fieldset').show();
			        	if (combo.value.toString().search('SME') != -1)
			        		me.form.findField('SMEComments').show();
			        	if (combo.value.toString().search('Contact') != -1)
			        		me.form.findField('placementComments').show();
			        	if (candidateId != null && candidateId != '' && combo.value.toString().search('SME') != -1)
			        		Ext.getCmp('Candidate_SkillSet_Fieldset').expand();
			        	if (candidateId != null && candidateId != '' && combo.value.toString().search('Contact') != -1)
			        		Ext.getCmp('Contact_Projects_Fieldset').expand();
			        	if (combo.value.toString().search('SME') == -1 && combo.value.toString().search('Contact') == -1)
			        		Ext.getCmp('Contact_Comments_Fieldset').hide();

			        	if (combo.value.toString().search('SME') != -1){
			        		me.moduleCombo.show();
			        		me.addModuleButton.show();
			        		me.form.findField('module').show();
			        		me.form.findField('priority').show();
			        		me.roleSetCombo.show();
			        		me.addRolesetButton.show();
			        		me.form.findField('roleSet').show();
			        		me.roleCombo.hide();
			        		me.form.findField('role').hide();
			        		me.smeTypeCombo.show();
			        		Ext.getCmp('Contact_SkillSet_Fieldset').expand();
			        		me.skillSetGroupCombo.show();
			        		me.form.findField('skillSetGroup').show();
						}else{
			        		me.moduleCombo.hide();
			        		me.addModuleButton.hide();
			        		me.form.findField('module').hide();
			        		me.form.findField('priority').hide();
			        		me.roleSetCombo.hide();
			        		me.addRolesetButton.hide();
			        		me.form.findField('roleSet').hide();
			        		me.form.findField('roleSetIds').reset();
			        		me.roleCombo.show();
			        		me.form.findField('role').show();
			        		me.smeTypeCombo.hide();
			        		Ext.getCmp('Contact_SkillSet_Fieldset').collapse();
			        		me.skillSetGroupCombo.hide();
			        		me.form.findField('skillSetGroup').hide();
			        		me.form.findField('skillSetGroupIds').reset();
			        	}
		        	}
		        },
		    }
		});

		me.smeTypeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'SME Type',
		    queryMode: 'local',
		    forceSelection:true,
            displayField: 'value',
            valueField: 'name',
		    name : 'smeType',
		    store: ds.smeTypeStore,
		    tabIndex:10,
		    multiSelect: true,
		    //width : 350,
		});
		
		me.roleCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Contact Type",
            name: 'selectedRole',
	        tabIndex:22,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.contactRoleStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var role = this.form.findField('role').getValue();
					if (role == null || role == '' )
						role = new Array();
					else
						role = role.split(',');
					if (role.indexOf(combo.value) == -1) {
						role.push(combo.value);
					}else{
						role.splice(role.indexOf(combo.value), 1);
					}
					me.roleCombo.reset();
					this.form.findField('role').setValue(role.toString());
				},
		    }
		});
		
		me.moduleCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Skill Set",
            name: 'selectedModule',
	        tabIndex:27,
	        padding : '30 0 0 0',
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.skillSetStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    enableKeyEvents : true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var module = this.form.findField('module').getValue();
					if (module == null || module == '' )
						module = new Array();
					else
						module = module.split(',');
					if (module.indexOf(combo.value) == -1) {
						module.push(combo.value);
					}else{
						module.splice(module.indexOf(combo.value), 1);
					}
					me.moduleCombo.reset();
					this.form.findField('module').setValue(module.toString());
				},
		    }
		});
		
		me.roleSetCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Role Set",
            name: 'selectedRoleset',
	        tabIndex:29,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'id',
            store: ds.targetRolesStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    padding : '30 0 0 0',
		    listeners: {
				scope:this,
				select: function(combo,value){
					var roleSetIds = this.form.findField('roleSetIds').getValue();
					if (roleSetIds == null || roleSetIds == '' )
						roleSetIds = new Array();
					else
						roleSetIds = roleSetIds.split(',');
					if (roleSetIds.indexOf(combo.value.toString()) == -1) 
						roleSetIds.push(combo.value.toString());
					else
						roleSetIds.splice(roleSetIds.indexOf(combo.value.toString()), 1);
					var roleSets = new Array();
					for ( var i = 0; i < roleSetIds.length; i++) {
						roleSets.push(ds.targetRolesStore.getById(Number(roleSetIds[i])).data.name);
					}
					me.roleSetCombo.reset();
					this.form.findField('roleSetIds').setValue(roleSetIds.toString());
					this.form.findField('roleSet').setValue(roleSets.toString());
				},
		    }
		});

		me.skillSetGroupCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Skill Set Group",
            name: 'selectedSkillSetGroup',
	        tabIndex:25,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'id',
            store: ds.skillsetGroupStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    enableKeyEvents : true,
		    padding : '20 0 0 0',
		    listeners: {
				scope:this,
				select: function(combo,value){
					var skillSetIds = this.form.findField('skillSetGroupIds').getValue();
					if (skillSetIds == null || skillSetIds == '' )
						skillSetIds = new Array();
					else
						skillSetIds = skillSetIds.split(',');
					if (skillSetIds.indexOf(combo.value.toString()) == -1) {
						skillSetIds.push(combo.value.toString());
					}else{
						skillSetIds.splice(skillSetIds.indexOf(combo.value.toString()), 1);
					}
					var skillsets = new Array();
					for ( var i = 0; i < skillSetIds.length; i++) {
						skillsets.push(ds.skillsetGroupStore.getById(Number(skillSetIds[i])).data.name);
					}
					me.skillSetGroupCombo.reset();
					this.form.findField('skillSetGroupIds').setValue(skillSetIds.toString());
					this.form.findField('skillSetGroup').setValue(skillsets.toString());
				},
		    }
		});

        me.addSkillsetButton = new Ext.Button({
        	iconCls : 'btn-add',
        	tooltip : 'Add Skillset Group',
        	margin: '20 0 0 10',
        	tabIndex:26,
        	scope: this,
        	handler: function(){
        		this.addSkillsetGroup();
        	}
        });

        me.addModuleButton = new Ext.Button({
        	iconCls : 'btn-add',
        	tooltip : 'Add Skill Set',
        	margin: '30 0 0 10',
        	tabIndex:28,
        	scope: this,
        	handler: function(){
        		this.addModule('Skill Set/Target Skill Set');
        	}
        });

        me.addRolesetButton = new Ext.Button({
        	iconCls : 'btn-add',
        	tooltip : 'Add Roleset',
        	margin: '30 0 0 10',
        	scope: this,
        	tabIndex:30,
        	handler: function(){
        		this.addModule('Target Roles');
        	}
        });

		me.sourceCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Source",
            name: 'source',
            store: ds.candidateSourceStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:15,
		});


        me.items = [this.getMessageComp(), this.getHeadingComp(),
            {
	   	    	 xtype:'fieldset',
	   	    	 title : 'Contact',
	   	         collapsible: false,
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.4,
	   	             items: [{
	   	             		xtype: 'container',
	   	             		layout : 'table',
	   	             		items: [{
	   	             			xtype:'textfield',
	   	             			fieldLabel: 'First Name *',
	   	             			name: 'firstName',
	   	             			allowBlank:false,
	   	             			maxLength:45,
	   	             			enforceMaxLength:true,
	   	             			tabIndex:1,
	   	             		},{
	   	             			xtype: 'box',
	   	             			tooltip : 'View Candidate',
	   	             			tabIndex:2,
	   	             			padding : '0 0 0 10%',
	   	             			name: 'clientLink',
	   	             			align : 'right',
	   	             			autoEl: {
	   	             				html: '<a href="#">'+'<u>View</u>'+'</a>'
	   	             			},
	   	             			listeners: {
	   	             				render: function(c){
	   	             					c.getEl().on('click', function(){me.showCandidate()}, c, {stopEvent: true});
	   	             				}
	   	             			}
	   	             		}]
	   	             	},{
	   	             		xtype: 'container',
	   	             		layout : 'table',
	   	             		items: [me.clientCombo,{
	   	             			xtype: 'box',
	   	             			hidden : true,
	   	             			tooltip : 'View Client/Vendor',
	   	             			tabIndex:4,
	   	             			padding : '0 0 0 10%',
	   	             			name: 'clientLink',
	   	             			align : 'right',
	   	             			autoEl: {
	   	             				html: '<a href="#">'+'<u>View</u>'+'</a>'
	   	             			},
	   	             			listeners: {
	   	             				render: function(c){
	   	             					c.getEl().on('click', function(){me.showClient()}, c, {stopEvent: true});
	   	             				}
	   	             			}
	   	             		},{
	   	             			xtype: 'button',
	   	             			iconCls : 'btn-add',
	   	             			tooltip : 'Add new Client/Vendor',
	   	             			tabIndex:5,
	   	             			margin: '0 0 0 10',
	   	             			scope: this,
	   	             			handler: function(){
	   	             				this.addClient();
	   	             			}
	   	             		}]
	   	             	},{
	   	             		xtype:'displayfield',
   	            	 		padding : '0 0 0 10',
   	            	 		fieldLabel: 'Type (Client/Vendor)',
   	            	 		name: 'clientType',
	   	             	},{
		   	            	 xtype:'numberfield',
		   	            	 fieldLabel: 'Priority',
		   	            	 name: 'priority',
		   	            	 tabIndex:9,
		   	            	 allowDecimals: false,
		   	            	 minValue: 0,
		   	            	 maxValue: 100
		   	             },{
   	             			xtype:'textfield',
   	             			fieldLabel: 'Email',
   	             			name: 'email',
   	             			//allowBlank:false,
   	             			vtype:'email',
   	             			tabIndex:11,
   	             			maxLength:45,
   	             			enforceMaxLength:true,
   	             		},{
   	             			xtype:'textfield',
   	             			fieldLabel: 'Cell Phone',
   	             			name: 'cellPhone',
   	             			maskRe :  /[0-9]/,
   	             			tabIndex:13,
   	             			maxLength:12,
   	             			enforceMaxLength:true,
   	             			listeners:{
   	             				change : function(field,newValue,oldValue){
   	             					var value = me.form.findField('cellPhone').getValue();
   	             					if (value != "" && value != null) {
   	             						value = value.replace(/[^0-9]/g, "");
   	             						value = value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
   	             						me.form.findField('cellPhone').setValue(value);
   	             					}
   	             				}
   	             			}
   	             		},
   	             		me.sourceCombo,
   	             		{
		   	            	 xtype:'textfield',
		   	                 fieldLabel: 'Internet Link',
		   	                 name: 'internetLink',
		   	                 tabIndex:17,
		                     maxLength:50,
		                     enforceMaxLength:true,
		                     vtype:'url',
		   	             },{
		   	            	 xtype:'numberfield',
		   	            	allowDecimals: false,
		   	            	 fieldLabel: 'Score',
		   	            	 name: 'score',
		   	            	 tabIndex:19,
		   	            	 value :0,
		   	            	 minValue: 0,
		   	            	 maxValue: 100
		   	             },{
		   	            	 xtype:'numberfield',
		   	            	allowDecimals: false,
		   	            	 fieldLabel: 'Confidentiality',
		   	            	 name: 'confidentiality',
		   	            	 tabIndex:21,
		   	            	 minValue: 1,
		   	            	 maxValue: 9,
		   	            	 listeners: {
		   	            		 afterrender: function() {
		   	            			 if (app.userRole == 'ADMIN')
		   	            				 this.show();
		   	            			 else
		   	            				 this.hide();
		   	            		 }
		   	            	 }
		   	             },
		   	             me.roleCombo,
		   	             ]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [{
	   	            	 xtype:'textfield',
	   	            	 fieldLabel: 'Last Name *',
	   	            	 name: 'lastName',
	   	            	 allowBlank:false,
	   	            	 tabIndex:3,
	   	            	 maxLength:45,
	   	            	 enforceMaxLength:true,
	   	             },{
	   	            	 xtype: 'radiogroup',
	   	            	 fieldLabel: 'Gender',
	   	            	 name: 'gender',
	   	            	 columns: 2,
	   	            	 value : false,
	   	            	 vertical: true,
	   	            	 items: [{ 
	   	            		 xtype: 'radiofield', 
	   	            		 boxLabel: 'Male', 
	   	            		 name: 'gender', 
	   	            		 inputValue: 'Male',
	   	            		 width: 60,
	   	            		 tabIndex:6,
	   	            	 },{ 
	   	            		 xtype: 'radiofield', 
	   	            		 boxLabel: 'Female', 
	   	            		 name: 'gender', 
	   	            		 inputValue: 'Female',
	   	            		 width: 60,
	   	            		 tabIndex:7,
	   	            	 }],
	   	             },
	   	             me.typeCombo,me.smeTypeCombo,
	   	             {
	   	            	 xtype:'textfield',
	   	            	 fieldLabel: 'Job Title',
	   	            	 name: 'jobTitle',
	   	            	 tabIndex:12,
	   	            	 maxLength:45,
	   	            	 enforceMaxLength:true,
	   	             },{
	   	            	 xtype:'textfield',
	   	                 fieldLabel: 'Extension',
	   	                 name: 'extension',
	   	                 tabIndex:14,
	                     maxLength:45,
	                     enforceMaxLength:true,
	   	             },{
	   	            	 xtype:'textfield',
	   	            	 fieldLabel: "Referral/More info",
	   	            	 name: 'referral',
	   	            	 tabIndex:16,
	   	            	 maxLength:45,
	   	            	 enforceMaxLength:true,
	   	             },{
	   	            	 xtype: 'container',
	   	            	 layout :'table',
	   	            	 items: [{
	   	            		 xtype:'textfield',
	   	            		 fieldLabel: "Resume Link",
	   	            		 name: 'resumeLink',
	   	            		 tabIndex:18,
	   	            		 maxLength:250,
	   	            		 enforceMaxLength:true,
	   	            	 },{
	   	            		 xtype: 'box',
	   	            		 tabIndex:45,
	   	            		 padding : '0 0 0 10%',
	   	            		 name: 'resumeLinkHyperlink',
	   	            		 align : 'right',
	   	            		 autoEl: {
	   	            			 html: '<a href="#">'+'<u>Go</u>'+'</a>'
	   	            		 },
	   	            		 listeners: {
	   	            			 render: function(c){
	   	            				 c.getEl().on('click', function(){
	   	            					 var url = me.form.findField('resumeLink').getValue();
	   	            					 if (url == null || url == '') {
	   	            						 me.updateError('Link not exist');
	   	            						 return false;
	   	            					 }
	   	            					 window.open(url,"_blank");
	   	            				 }, c, {stopEvent: true});
	   	            			 }
	   	            		 }	
	   	            	 }]
	   	             },{
	   	            	 xtype: 'textarea',
	   	            	 height : 60,
	   	            	 width : 400,
	   	            	 maxLength:500,
	   	            	 enforceMaxLength:true,
	   	            	 fieldLabel: 'Comments',
	   	            	 name: 'comments',
	   	            	 tabIndex:20,
	   	             },{
	   	            	 xtype: 'textarea',
	   	            	 fieldLabel: 'Selected Contact Type',
	   	            	 name: 'role',
	   	            	 readOnly : true,
	   	            	 width : 400,
	   	            	 height : 40,
	   	            	 maxLength:100,
	   	             },{
	   	            	 xtype: 'numberfield',
	   	            	 hidden:true,
	   	            	 name: 'id'
	   	             },{
	   	            	 xtype: 'numberfield',
	   	            	 hidden:true,
	   	            	 name: 'candidateId'
	   	             },{
	   	            	 xtype: 'hiddenfield',
	   	            	 hidden:true,
	   	            	 name: 'candidateDeleted'
	   	             }]
	   	         }]
            },{
	   	    	 xtype:'fieldset',
	   	    	 title : 'SME Skill Set & Roles',
	   	    	 id : 'Contact_SkillSet_Fieldset',
	   	         collapsible: false,
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.4,
	   	             items: [{
	   	             		xtype: 'container',
	   	             		layout : 'table',
	   	             		items: [me.skillSetGroupCombo,me.addSkillsetButton]
	   	             	},{
	   	             		xtype: 'container',
	   	             		layout : 'table',
	   	             		items: [me.moduleCombo,me.addModuleButton]
	   	             	},{
	   	             		xtype: 'container',
	   	             		layout : 'table',
	   	             		items: [me.roleSetCombo,me.addRolesetButton]
	   	             	}]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [{
	   	            	 xtype: 'textarea',
	   	            	 fieldLabel: 'Selected Skill Set Group',
	   	            	 name: 'skillSetGroup',
	   	            	 readOnly : true,
	   	            	 width : 400,
	   	            	 height : 60,
	   	             },{
	   	            	 xtype: 'textfield',
	   	            	 fieldLabel: 'Skillset Group Ids',
	   	            	 name: 'skillSetGroupIds',
	   	            	 readOnly : true,
	   	            	 hidden : true,
	   	             },{
	   	            	 xtype:'textarea',
	   	            	 fieldLabel: 'Selected Skill Set',
	   	            	 name: 'module',
	   	            	 width : 400,
	   	            	 height : 60,
	   	            	 maxLength:300,
	   	            	 enforceMaxLength:true,
	   	            	 readOnly : true,
	   	             },{
	   	            	 xtype: 'textarea',
	   	            	 fieldLabel: 'Selected Role Set',
	   	            	 name: 'roleSet',
	   	            	 readOnly : true,
	   	            	 width : 400,
	   	            	 height : 60,
	   	             },{
	   	            	 xtype: 'textfield',
	   	            	 fieldLabel: 'Roleset Ids',
	   	            	 name: 'roleSetIds',
	   	            	 readOnly : true,
	   	            	 hidden : true,
	   	             }]
	   	         }]
            },{
	   	    	 xtype:'fieldset',
	   	    	 title : 'SME / Contact Comments',
	   	    	 id : 'Contact_Comments_Fieldset',
	   	         collapsible: false,
	   	         layout: 'column',
	   	         items :[{
	   	        	xtype: 'container',
	   	             columnWidth:1,
	   	             items: [{
	   	            	 xtype:'textarea',
	   	        		 fieldLabel: "SME Comments",
	   	        		 name: 'SMEComments',
	                     maxLength:500,
	                     enforceMaxLength:true,
	   	        		 width : 600,
	   	        		 height : 100,
	   	        		 tabIndex:35,
	   	             },{
	   	        		 xtype:'textarea',
	   	        		 fieldLabel: "Contact Comments",
	   	        		 name: 'placementComments',
	                     maxLength:500,
	                     enforceMaxLength:true,
	   	        		 width : 600,
	   	        		 height : 100,
	   	        		 tabIndex:36,
	   	        	 }]
   	        	 }]
            },{
        		xtype : 'fieldset',
        		title : 'Candidate Skill Set & Roles',
        		id : 'Candidate_SkillSet_Fieldset',
				collapsible : false,
				defaultType : 'textfield',
				layout : 'column',
				items : [{
					xtype: 'container',
					columnWidth:1,
					items: [{
						xtype: 'displayfield',
						fieldLabel: 'Skill Set',
						name: 'skillSet',
					},{
						xtype: 'displayfield',
						fieldLabel: 'Certifications',
						name: 'certifications',
					},{
						xtype: 'displayfield',
						fieldLabel: 'Role Set',
						name: 'canRoleSet',
					},{
						xtype: 'displayfield',
						fieldLabel: 'Target Skill Set',
						name: 'targetSkillSet',
					},{
						xtype: 'displayfield',
						fieldLabel: 'Target Roles',
						name: 'targetRoles',
					}]
				}]
        	},{
            	xtype : 'fieldset',
            	id : 'Contact_Projects_Fieldset',
            	title : 'Projects',
            	collapsible : false,
            	layout : 'column',
            	items : [{
            		xtype: 'container',
            		columnWidth:1,
            		items: [me.projectGridPanel]
            	}]
            },{
            	xtype : 'fieldset',
            	id : 'Contact_Interviews_Fieldset',
            	title : 'Interviews',
            	collapsible : true,
            	layout : 'column',
            	items : [{
            		xtype: 'container',
            		columnWidth:1,
            		items: [{
                    	xtype:'displayfield',
                    	fieldLabel: '',
                    	name: 'summary',
                    }]
            	}]
            }
        ];
        me.callParent(arguments);
    },

    loadForm : function(record) {
    	this.modified = false;
    	this.clearMessage();
    	this.moduleCombo.hide();
    	this.addModuleButton.hide();
    	this.form.findField('module').hide();
    	this.roleSetCombo.hide();
    	this.addRolesetButton.hide();
    	this.form.findField('roleSet').hide();
    	this.form.findField('priority').hide();
    	this.roleCombo.show();
    	this.form.findField('role').show();
    	this.smeTypeCombo.hide();
		this.skillSetGroupCombo.hide();
		this.form.findField('skillSetGroup').hide();

    	this.form.reset();
		this.loadRecord(record);
		if (record.data.type != null)
			this.typeCombo.setValue(record.data.type.split(','));

		if (record.data.smeType != null)
			this.smeTypeCombo.setValue(record.data.smeType.split(','));

		if (record.data.roleSetIds != null && record.data.roleSetIds != '') {
			var roleSetIds = record.data.roleSetIds.split(',');
			var roleSet ='';
			for ( var i = 0; i < roleSetIds.length; i++) {
				roleSet += ds.targetRolesStore.getById(Number(roleSetIds[i])).data.value;
				if (roleSetIds.length > (i+1))
					roleSet += ",";
			}
			this.form.findField('roleSet').setValue(roleSet);
		}

		if (record.data.skillSetGroupIds != null && record.data.skillSetGroupIds != '') {
			var skillSetGroupIds = record.data.skillSetGroupIds.split(',');
			var skillSetGroup ='';
			for ( var i = 0; i < skillSetGroupIds.length; i++) {
				skillSetGroup += ds.skillsetGroupStore.getById(Number(skillSetGroupIds[i])).data.name;
				if (skillSetGroupIds.length > (i+1))
					skillSetGroup += ",";
			}
			this.form.findField('skillSetGroup').setValue(skillSetGroup);
		}

		this.deleteButton.enable();
		this.form.findField('gender').setValue({gender:record.data.gender});
		if (record.data.id != null && record.data.id == '' && record.data.id != 0)
			this.getInterviews(record.data.id);
		
		this.projectGridPanel.store.removeAll();
		Ext.getCmp('Contact_Projects_Fieldset').collapse();
		Ext.getCmp('Contact_Interviews_Fieldset').collapse();
		Ext.getCmp('Candidate_SkillSet_Fieldset').collapse();
		Ext.getCmp('Contact_SkillSet_Fieldset').collapse();
		
		if (record.data.id == null || record.data.id == '' || record.data.candidateId == null || record.data.candidateId == '' || record.data.candidateDeleted == true){
			this.linkCandidateButton.show();
			this.unlinkCandidateButton.hide();
		}else{
			this.linkCandidateButton.hide();
			this.unlinkCandidateButton.show();
		}
		if (record.data.type.search('SME') == -1 && record.data.type.search('Contact') == -1 ) {
			Ext.getCmp('Contact_Comments_Fieldset').hide();
		}else
			Ext.getCmp('Contact_Comments_Fieldset').show();
		if (record.data.type.search('SME') != -1 && record.data.candidateId != null && record.data.candidateId != '' && record.data.candidateDeleted == false) {
			Ext.getCmp('Candidate_SkillSet_Fieldset').expand();
		}
		if (record.data.candidateId != null && record.data.candidateId != '' && record.data.candidateDeleted != true ){
			this.loadProjects(record.data.candidateId);
		}

    	if (record.data.type != null  && record.data.type.toString().search('SME') != -1){
    		this.moduleCombo.show();
    		this.addModuleButton.show();
    		this.form.findField('module').show();
    		this.form.findField('priority').show();
    		this.roleSetCombo.show();
    		this.addRolesetButton.show();
    		this.form.findField('roleSet').show();
    		this.roleCombo.hide();
    		this.form.findField('role').hide();
    		this.smeTypeCombo.show();
			Ext.getCmp('Contact_SkillSet_Fieldset').expand();
    		this.skillSetGroupCombo.show();
    		this.form.findField('skillSetGroup').show();
		}else{
    		this.moduleCombo.hide();
    		this.addModuleButton.hide();
    		this.form.findField('module').hide();
    		this.form.findField('priority').hide();
    		this.roleSetCombo.hide();
    		this.addRolesetButton.hide();
    		this.form.findField('roleSet').hide();
    		this.roleCombo.show();
    		this.form.findField('role').show();
    		this.smeTypeCombo.hide();
			Ext.getCmp('Contact_SkillSet_Fieldset').collapse();
			this.skillSetGroupCombo.hide();
			this.form.findField('skillSetGroup').hide();
    	}
	},

	loadProjects : function(candidateId) {
		var params = new Array();
    	params.push(['candidateId','=', candidateId]);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	this.projectGridPanel.store.loadByCriteria(filter);
    	
    	this.projectGridPanel.store.on('load', function(store, records, options) {
    		var type = this.typeCombo.getValue();
    		if (type != null && (type.toString().search('SME') != -1 || type.toString().search('Contact') != -1 ) && store.getCount() >0 ) {
    			Ext.getCmp('Contact_Projects_Fieldset').expand();
			}
       	}, this);
	},
	
	
	getInterviews : function(contactId) {
		var params = new Array();
		params.push(['interviewer','=', contactId]);
		var filter = getFilter(params);
		filter.pageSize=500;
		app.interviewService.getInterviews(Ext.JSON.encode(filter),this.onGetInterview,this);
	},
	
	onGetInterview : function(data) {
		if (data.success && data.rows.length > 0) {
			var table = '';
			for ( var i = 0; i < data.rows.length; i++) {
				var record = data.rows[i];
				table += "<tr bgcolor='#FFFFFF'>";
				if (record.requirementId != null && record.requirementId != '')
					table += "<td>"+record.requirementId+"</td>" ;
				else
					table += "<td>n/a</td>" ;
				if (record.vendor != null && record.vendor != '')
					table += "<td>"+record.vendor+"</td>" ;
				else
					table += "<td>n/a</td>" ;
				if (record.candidate != null && record.candidate != '')
					table += "<td>"+record.candidate+"</td>" ;
				else
					table += "<td>n/a</td>" ;
				if (record.client != null && record.client != '')
					table += "<td>"+record.client+"</td>" ;
				else
					table += "<td>n/a</td>" ;
				if (record.interviewDate != null && record.interviewDate != '')
					table += "<td>"+Ext.Date.format(new Date(record.interviewDate),'m/d/Y')+"</td></tr>" ;
				else
					table += "<td>n/a</td></tr>" ;
			}
			if (table != '') {
				table = "<table cellpadding='5' bgcolor='#575252'>"+
				"<tr style='background-color:#dddddd;font-weight:bold;'>" +
				"<td>Job Id</td>" +
				"<td>Vendor</td>" +
				"<td>Candidate</td>" +
				"<td>Client</td>" +
				"<td>Interview Date</td></tr>"+table+"</table>";
			}
			this.form.findField('summary').setValue(table);
			Ext.getCmp('Contact_Interviews_Fieldset').expand();
		}
		
	},
	
	closeForm: function(){
		if (this.opendFrom == 'Candidate') {
			this.loadCandidatesTab();
		}else if (this.opendFrom == 'panel'){
			app.setActiveTab(0,'li_requirements');
			if (this.modified)
				app.requirementMgmtPanel.requirementDetailPanel.loadSubVendors(app.requirementMgmtPanel.requirementDetailPanel.requirementPanel.form.findField('id').getValue());
		}else if (this.opendFrom == 'grid'){
			app.setActiveTab(0,'li_requirements');
			if (this.modified){
		    	var params = new Array();
		    	var filter = getFilter(params);
		    	filter.pageSize=1000;
		    	ds.contactSearchStore.loadByCriteria(filter);
			}
		}else if (this.opendFrom == 'Home') {
			app.setActiveTab(0,'li_home');
		}else {
			this.manager.getLayout().setActiveItem(0);
			if (this.modified)
				this.manager.contactSearchPanel.search();
		}
	},

	saveAndClose : function() {
		this.closeContact = true;
		this.save();
	},
	
    save : function(){
    	if(this.form.isValid( )){
			var otherFieldObj = {}, clientObj = {}, candidateObj = {};
			var client = '', candidate ='';
			
			var itemslength = this.form.getFields().getCount();
			var i = 0;
			while (i < itemslength) {
				var fieldName = this.form.getFields().getAt(i).name;
				if (fieldName == 'id') {
					var idVal = this.form.findField(fieldName).getValue();
					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0)
						otherFieldObj[fieldName] = this.form.findField(fieldName).getValue();
				}else 
					otherFieldObj[fieldName] = this.form.findField(fieldName).getValue();
				i = i+1;
			}
			otherFieldObj['gender'] = this.form.findField('gender').getValue().gender;
			if (this.typeCombo.getValue() != null)
				otherFieldObj['type'] = this.typeCombo.getValue().toString();
			if (this.smeTypeCombo.getValue() != null)
				otherFieldObj['smeType'] = this.smeTypeCombo.getValue().toString();
			
			if (otherFieldObj.confidentiality == null)
				delete otherFieldObj.confidentiality;
			if (otherFieldObj.score == null)
				delete otherFieldObj.score;
			if (otherFieldObj.priority == null)
				delete otherFieldObj.priority;
			
			delete otherFieldObj.clientId;
			delete otherFieldObj.candidateId;
			delete otherFieldObj.clientType;
			delete otherFieldObj.selectedRole;
			delete otherFieldObj.selectedModule;
			delete otherFieldObj.summary;
			delete otherFieldObj.skillSet;
			delete otherFieldObj.canRoleSet;
			delete otherFieldObj.targetSkillSet;
			delete otherFieldObj.targetRoles;
			delete otherFieldObj.certifications;
	   		delete otherFieldObj.roleSetIds;
	   		delete otherFieldObj.roleSet;
	   		delete otherFieldObj.selectedRoleset;
	   		delete otherFieldObj.skillSetGroup;
	   		delete otherFieldObj.skillSetGroupIds;
	   		delete otherFieldObj.selectedSkillSetGroup;
	   		delete otherFieldObj.resumeLinkHyperlink;
	   		
			otherFieldObj = Ext.JSON.encode(otherFieldObj);
			otherFieldObj = otherFieldObj.substring(1,otherFieldObj.length-1);

			var idVal = this.form.findField('clientId').getValue();
			if(idVal != '' && idVal != null && idVal >= 0) {
				clientObj['id'] = idVal;
				clientObj = Ext.JSON.encode(clientObj);
				client = ',"client":' + clientObj;
			}

			var idVal = this.form.findField('candidateId').getValue();
			if(idVal != '' && idVal != null && idVal >= 0) {
				candidateObj['id'] = idVal;
				candidateObj = Ext.JSON.encode(candidateObj);
				candidate = ',"candidate":' + candidateObj;
			}

			//Role set values
			var roleSetIds = this.form.findField('roleSetIds').getValue();
			if (roleSetIds == null || roleSetIds == '' )
				roleSetIds = new Array();
			else
				roleSetIds = roleSetIds.split(',');
			var contact_roleSets = new Array();
			for ( var i = 0; i < roleSetIds.length; i++) {
				var record = {};
				record.roleSetId = Number(roleSetIds[i]);
				contact_roleSets.push(record);
			}
			contact_roleSets = Ext.JSON.encode(contact_roleSets);

	   		//skillset group values
			var skillSetIds = this.form.findField('skillSetGroupIds').getValue();
			if (skillSetIds == null || skillSetIds == '' )
				skillSetIds = new Array();
			else
				skillSetIds = skillSetIds.split(',');
			var contact_skillSetGroup = new Array();
			for ( var i = 0; i < skillSetIds.length; i++) {
				var record = {};
				record.skillSetGroupId = Number(skillSetIds[i]);
				contact_skillSetGroup.push(record);
			}
			contact_skillSetGroup = Ext.JSON.encode(contact_skillSetGroup);

			otherFieldObj = '{"contact_RoleSets":{"com.recruit.client.service.Contact_RoleSet":'+contact_roleSets+'},'+ 
								'"contact_SkillSetGroups":{"com.recruit.client.service.Contact_SkillSetGroup":'+contact_skillSetGroup+'},'+
								otherFieldObj + client + candidate + '}';
			app.contactService.saveContact(otherFieldObj,this.onSave,this);
    	} else {
    		this.updateError('Please fix the errors');
    	}
	},
	
	onSave : function(data){
		this.modified = true;
		if (data.success) { 
	    	var params = new Array();
	    	var filter = getFilter(params);
	    	filter.pageSize=1000;
	    	ds.contactSearchStore.loadByCriteria(filter);
			if (this.closeContact) {
				this.closeContact = false;
				this.closeForm();
			}else{
				var rec = data.returnVal;
				this.form.findField('id').setValue(rec.id);
				this.updateMessage('Saved Successfully');
				if (rec.candidate != null && rec.candidate.id != null && rec.candidate.id != '' && rec.candidateDeleted != true){
					this.linkCandidateButton.hide();
					this.unlinkCandidateButton.show();
				}
			}
		}else {
			this.updateError(data.errorMessage);
		}	
	},	
	
	deleteConfirm : function(){
		Ext.Msg.confirm("Delete Contact","Do you delete selected Contact?", this.deleteContact, this);
	},
	
	deleteContact: function(dat){
		if(dat=='yes'){
			var id = this.form.findField('id').getValue();
			app.contactService.deleteContact(id, this.onDeleteContact, this);
		}
	},
	
	onDeleteContact : function(data){
		if (!data.success) {
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			this.modified = true;
			this.closeForm();
	    	var params = new Array();
	    	var filter = getFilter(params);
	    	filter.pageSize=1000;
	    	ds.contactSearchStore.loadByCriteria(filter);
		}	
	},
	
	showClient : function() {
		var clientId = this.clientCombo.getValue();
		if (clientId != null && clientId != '') {
			var params = new Array();
			params.push(['id','=', clientId]);
			var filter = getFilter(params);
			filter.pageSize=50;
			app.clientService.getClients(Ext.JSON.encode(filter),this.onGetClient,this);
		}else
			this.updateError('Client/Vendor not found');
	},
	
	onGetClient : function(data) {
		if (data.rows.length > 0) {
	    	app.setActiveTab(0,'li_clients');
	    	app.clientMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Client', data.rows[0]);
	    	app.clientMgmtPanel.clientPanel.loadForm(record);
	    	app.clientMgmtPanel.clientPanel.openedFrom = 'ContactDetailPanel';
		}
	},

	linkCandidateConfirm : function() {
		if (this.form.findField('id').getValue() == null || this.form.findField('id').getValue() == '' || this.form.findField('id').getValue() == 0 ) {
			this.updateError('Please save the contact');
			return false;
		}
		this.clearMessage();
		var panel = this;
		var candidateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Candidate *',
		    store: ds.contractorsStore,
		    queryMode: 'local',
		    anyMatch:true,
		    forceSelection:true,
		    allowBlank:false,
		    displayField: 'fullName',
		    valueField: 'id',
		    name : 'candidateId',
   			listeners: {
		    	select:function(combo, records, eOpts) {
		    		var candidateId = combo.value;
		    		panel.checkContactExist(candidateId);
		    	}
			},
		});
        var candidatePanel = Ext.create('Ext.form.Panel', {
        	bodyPadding: 5,
        	border : 0,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 80},
            items : [{
            	xtype:'container',
   	         	layout: 'column',
   	         	items :[candidateCombo]
            }]
        });
        var panel = this;
		var candidateLinkWindow = Ext.create('Ext.window.Window', {
		    title: 'Select Candidate',
		    id : 'candidateLinkWindow',
		    modal: true,
		    bodyPadding: 0,
		    layout: 'fit',
		    items: [candidatePanel],
		    buttons: [{
		    	text: 'Add Candidate',
		    	iconCls : 'btn-add',
		    	tooltip : 'Add Reference',
	            handler: function(){
	            	panel.addCandidate();
	            }
	        }]

		});
		Ext.getCmp('candidateLinkWindow').show();
	},
	
	//check contcat 
	checkContactExist : function(candidateId) {
        if (Ext.getCmp('candidateLinkWindow') != null) 
        	Ext.getCmp('candidateLinkWindow').close();

        this.candidateId = candidateId;
        this.contact = null;
        var candidateName = ds.contractorsStore.getById(candidateId).data.fullName;
        var contactId = this.form.findField('id').getValue();
        var panel = this;
        
        //linking candidate to opened contact
        if (contactId != null && contactId != 0 && contactId != '') {
			var contactName = this.form.findField('firstName').getValue() + ' '+this.form.findField('lastName').getValue();
			if (contactName != candidateName) {
				Ext.MessageBox.show({
					title:'Confirm',
		            msg: "Contact name is not same as Candidate name. Do you want to link selected Candidate?",
		            buttonText: {yes: 'No',no: 'Yes'},
		            icon: Ext.MessageBox.QUESTION,
		            fn: function(btn) {
		            	if (btn == 'no') {
		            		panel.contact = 'Exist';
		            		panel.getCandidate();
						}
		            }
		        });
			}else{
				this.contact = 'Exist';
				this.getCandidate();
			}
		}else{
			//linking candidate to new contact
	        if (ds.contactSearchStore.findExact('fullName',candidateName) != -1) {
				Ext.MessageBox.show({
					title:'Confirm',
		            msg: "Contact already exist with the given name. Do you want to update existing contact?",
		            buttons: Ext.MessageBox.YESNO,
		            icon: Ext.MessageBox.QUESTION,
		            fn: function(btn) {
		            	if (btn == 'yes') {
		        	    	var params = new Array();
		        	    	params.push(['id','=', ds.contactSearchStore.getAt(ds.contactSearchStore.findExact('fullName',candidateName)).data.id]);
		        	    	var filter = getFilter(params);
		        	    	app.contactService.getContacts(Ext.JSON.encode(filter),panel.onGetContact,panel);
						}
		            }
		        });
			}else {
				this.getCandidate();
			}
		}
	},
	
	//if contact exist store it in some where and get candidate
	onGetContact : function(data) {
		if (data.success && data.rows.length > 0) {
			var contact = Ext.create('tz.model.Contact', data.rows[0]);
			this.contact = contact;
			this.getCandidate();
		}
	},
	
	//get Candidate
	getCandidate : function() {
		var params = new Array();
		params.push(['id','=', this.candidateId]);
		var filter = getFilter(params);
		filter.pageSize=50;
		app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidate,this);
	},
	
	onGetCandidate : function(data) {
		if (data.rows.length > 0) {
			this.modified = true;
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			record.data.allCertifications = record.data.allCertifications.replace(/ -- /g, '<br />');
			record.data.roleSet = '';
			if (record.data.roleSetIds != null && record.data.roleSetIds != ''){
				var roleSetIds = record.data.roleSetIds.toString().split(',');
				for ( var i = 0; i < roleSetIds.length; i++) {
					record.data.roleSet += ds.targetRolesStore.getById(Number(roleSetIds[i])).data.value+',';
				}
			}
			
			if (this.contact == 'Exist') {
				this.form.findField('candidateId').setValue(record.data.id);
				this.form.findField('candidateDeleted').setValue(false);
				this.form.findField('skillSet').setValue(record.data.skillSet);
				this.form.findField('canRoleSet').setValue(record.data.roleSet);
				this.form.findField('targetSkillSet').setValue(record.data.targetSkillSet);
				this.form.findField('targetRoles').setValue(record.data.targetRoles);
				this.form.findField('certifications').setValue(record.data.allCertifications);
				this.loadProjects(record.data.id);
	    		var type = this.typeCombo.getValue();
	    		if (type != null && type.toString().search('SME') != -1 ) {
	    			Ext.getCmp('Candidate_SkillSet_Fieldset').expand();
	    			Ext.getCmp('Contact_SkillSet_Fieldset').expand();
				}
			}else if (this.contact != null) {
				var contact = this.contact;
				contact.data.candidateId = record.data.id;
				contact.data.skillSet = record.data.skillSet;
				contact.data.canRoleSet = record.data.roleSet;
				contact.data.targetSkillSet = record.data.targetSkillSet;
				contact.data.targetRoles = record.data.targetRoles;
				contact.data.certifications = record.data.allCertifications;
				contact.data.candidateDeleted = false;
				this.loadForm(contact);
			}else{
		   	 	var contact = Ext.create( 'tz.model.Contact',{
		   			candidateId : record.data.id,
		   			firstName : record.data.firstName,
		   			lastName : record.data.lastName,
		   			email : record.data.emailId,
		   			cellPhone : record.data.contactNumber,
		   			gender : record.data.gender,
		   			confidentiality : record.data.confidentiality,
		   			priority : record.data.priority,
		   			skillSet : record.data.skillSet,
		   			canRoleSet : record.data.roleSet,
		   			targetSkillSet : record.data.targetSkillSet,
		   			targetRoles : record.data.targetRoles,
		   			certifications : record.data.allCertifications,
		   			candidateDeleted : false,
		   			source : record.data.source,
		   			referral : record.data.referral,
		   			resumeLink : record.data.resumeLink,
					skillSetIds : record.data.skillSetIds,
					roleSetIds : record.data.roleSetIds,
					skillSetGroupIds : record.data.skillSetGroupIds,
		   	 	});
				this.loadForm(contact);
			}
			this.save();
		}
	},

	unlinkCandidateConfirm : function() {
		Ext.Msg.confirm("Unlink Candidate","This will disable all the candidate information.\n Are you sure to unlink candidate?", this.unlinkCandidate, this);
	},

	unlinkCandidate : function(dat) {
		if (dat == 'yes') {
			this.modified = true;
	        var id = this.form.findField('id').getValue();
	        app.contactService.unlinkCandidate(id,this.onunlinkCandidate,this);
		}
	},
	
	onunlinkCandidate : function(data) {
		if (data.success) {
			var record = Ext.create('tz.model.Contact', data.returnVal);
			this.loadForm(record);
			this.updateMessage('Candidate unlinked Successfully');
		}else
			this.updateError('Candidate unlinking failed');
	},
	
	loadCandidatesTab : function() {
		if (this.modified){
			var candidateId = this.form.findField('candidateId').getValue();
			var params = new Array();
			params.push(['id','=', candidateId]);
			var filter = getFilter(params);
			filter.pageSize=50;
			app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidateData,this);
		}else
			app.setActiveTab(0,'li_candidates');
	},
	
	onGetCandidateData : function(data) {
		if (data.rows.length > 0) {
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			app.setActiveTab(0,'li_candidates');
			if (record.data.contactId != null && record.data.contactId != '') {
				app.candidateMgmtPanel.candidatePanel.makeSMEButton.hide();
				app.candidateMgmtPanel.candidatePanel.viewSMEButton.show();
			}else{
				app.candidateMgmtPanel.candidatePanel.makeSMEButton.show();
				app.candidateMgmtPanel.candidatePanel.viewSMEButton.hide();
			}
		}
	},

	addCandidate : function() {
        if (Ext.getCmp('candidateLinkWindow') != null) 
        	Ext.getCmp('candidateLinkWindow').close();

		var contcatId = this.form.findField('id').getValue();
		var firstName = this.form.findField('firstName').getValue();
		var lastName = this.form.findField('lastName').getValue();
		if (contcatId != null && contcatId != '') {
	    	var params = new Array();
	    	params.push(['firstName','=', firstName]);
	    	params.push(['lastName','=', lastName]);
	    	var filter = getFilter(params);
	    	app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidateExist,this);
		}else
			this.updateError('Please save the contact.');
	},
	
	onGetCandidateExist : function(data) {
		if (data.success && data.rows.length > 0) {
			var candidate = Ext.create('tz.model.Candidate', data.rows[0]);
		}else{
			var skillSetIds = new Array();
			var module = this.form.findField('module').getValue();
			if (module == null || module == '' )
				module = new Array();
			else
				module = module.split(',');
			for ( var i = 0; i < module.length; i++) {
				skillSetIds.push(ds.skillSetStore.getAt(ds.skillSetStore.findExact('name',module[i])).data.id);
			}

	   	 	var candidate = Ext.create( 'tz.model.Candidate',{
	   			firstName : this.form.findField('firstName').getValue(),
	   			lastName : this.form.findField('lastName').getValue(),
	   			emailId : this.form.findField('email').getValue(),
	   			contactNumber : this.form.findField('cellPhone').getValue(),
	   			gender : this.form.findField('gender').getValue().gender,
	   			confidentiality : this.form.findField('confidentiality').getValue(),
	   			priority : this.form.findField('priority').getValue(),
	   			currentJobTitle : this.form.findField('jobTitle').getValue(),
	   			source : this.form.findField('source').getValue(),
	   			referral : this.form.findField('referral').getValue(),
	   			resumeLink : this.form.findField('resumeLink').getValue(),
				skillSetIds : skillSetIds.toString(),
				roleSetIds : this.form.findField('roleSetIds').getValue(),
				skillSetGroupIds : this.form.findField('skillSetGroupIds').getValue(),
	   	 	});
		}
		app.setActiveTab(0,'li_candidates');
		app.candidateMgmtPanel.getLayout().setActiveItem(1);
		app.candidateMgmtPanel.candidatePanel.loadForm(candidate);
	},
	
	addClient : function() {
		var panel = this;
		if (Ext.getCmp('addClient') == null) {
			var clientWindow = Ext.create('Ext.window.Window', {
			    title: 'Add Client',
			    id : 'addClient',
			    width: 320,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
		   	                 	xtype:'textfield',
		   	                 	fieldLabel: 'Name *',
		   	                 	name: 'name',
		   	                 	allowBlank:false,
		   	                 	maxLength:45,
		   	                 	enforceMaxLength:true,
		   	                 	tabIndex:201,
		   	             	},
		   	             	Ext.create('Ext.form.ComboBox', {
		   	             		fieldLabel: 'Type *',
		   	             		store: ds.clientTypeStore,
		   	             		queryMode: 'local',
		   	             		displayField: 'value',
		   	             		forceSelection:true,
		   	             		allowBlank:false,
		   	             		valueField: 'name',
		   	             		name : 'type',
		   	             		tabIndex:203,
		   	             	})
		   	             	,{
		   	             		xtype:'textfield',
		   	             		fieldLabel: 'Email',
		   	             		name: 'email',
		   	             		//allowBlank:false,
		   	             		vtype:'email',
		   	             		tabIndex:202,
		   	             		maxLength:45,
		   	             		enforceMaxLength:true,
		   	             	},
		   	             	]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
			    	tooltip:'Save',
			    	tabIndex:205,
		            handler: function(){
		            	var client = {};
		            	for ( var i = 0; i < 3; i++ ) {
		            		var component = Ext.getCmp('addClient').items.items[0].items.items[0].items.items[0].items.items[i];
		            		if(! component.isValid()){
		            			Ext.Msg.alert('Error','Please fix the errors.');
		            			return false;
		            		}else{
		            			client[component.name] = component.value;
		            		}
						}
		            	client = Ext.JSON.encode(client);
		            	app.clientService.saveclient(client,panel.onAddClient,panel);
		            }
		        }]
			});
		}
		Ext.getCmp('addClient').show();
	},

	onAddClient : function(data) {
		if (data.success) {
	    	Ext.getCmp('addClient').close();
			if (data.returnVal.type == "Client"){
				ds.clientSearchStore.add(record);
			}else{
				ds.vendorStore.add(record);
			}
			var record = Ext.create('tz.model.Client', data.returnVal);
			this.clientCombo.store.add(record);
			this.clientCombo.setValue(data.returnVal.id);
		}
	},

	addModule : function(type) {
		var panel = this;
		if (type == 'Skill Set/Target Skill Set')
			var title = 'Skill Set';
		else
			var title = 'Role Set';
		
		if (Ext.getCmp('addModule') == null) {
			var settingsWindow = Ext.create('Ext.window.Window', {
			    title: 'Add '+title,
			    id : 'addModule',
			    width: 320,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
			        			xtype: 'displayfield',
			        			name: 'type',  
			        			fieldLabel: 'Type',
			        			tabIndex:203,
			        			value : type,
			        		},{
		   	                 	xtype:'textfield',
		   	                 	fieldLabel: 'Name *',
		   	                 	name: 'name',
		   	                 	allowBlank:false,
		   	                 	maxLength:45,
		   	                 	enforceMaxLength:true,
		   	                 	tabIndex:201,
		   	                 	listeners: {
		   	                 		change : function(field, newValue, oldValue){
		   	                 			Ext.getCmp('addModule').items.items[0].items.items[0].items.items[0].items.items[2].setValue(newValue);
		   	                 		}
		   	                 	}
		   	             	},{
			        			xtype: 'textfield',
			        			hidden : true,
			        			name: 'value',  
			        			fieldLabel: 'Value',
			        			tabIndex:203,
			        		}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
			    	tooltip:'Save',
			    	tabIndex:205,
		            handler: function(){
		            	var object = {};
		            	for ( var i = 0; i < 3; i++ ) {
		            		var component = Ext.getCmp('addModule').items.items[0].items.items[0].items.items[0].items.items[i];
		            		if(! component.isValid()){
		            			Ext.Msg.alert('Error','Please fix the errors.');
		            			return false;
		            		}else{
		            			object[component.name] = component.value;
		            		}
						}
		            	object = Ext.JSON.encode(object);
		            	app.settingsService.save(object,panel.onAddModule,panel);
		            }
		        }]
			});
		}
		Ext.getCmp('addModule').show();
	},

	onAddModule : function(data) {
		if (data.success) {
	    	Ext.getCmp('addModule').close();
			if (data.returnVal.type == "Skill Set/Target Skill Set"){
				var module = this.form.findField('module').getValue();
				if (module == null || module == '' )
					module = new Array();
				else
					module = module.split(',');
				module.push(data.returnVal.name);
				this.form.findField('module').setValue(module.toString());
		    	ds.skillSetStore.loadByCriteria(getFilter(new Array(['type','=', 'Skill Set/Target Skill Set'])));
			}else if (data.returnVal.type == "Target Roles"){
				var roleSetIds = this.form.findField('roleSetIds').getValue();
				if (roleSetIds == null || roleSetIds == '' )
					roleSetIds = new Array();
				else
					roleSetIds = roleSetIds.split(',');
				var roleSets = new Array();
				for ( var i = 0; i < roleSetIds.length; i++) {
					roleSets.push(ds.targetRolesStore.getById(Number(roleSetIds[i])).data.name);
				}
				roleSetIds.push(data.returnVal.id);
				roleSets.push(data.returnVal.name);
				this.form.findField('roleSetIds').setValue(roleSetIds.toString());
				this.form.findField('roleSet').setValue(roleSets.toString());
		    	ds.targetRolesStore.loadByCriteria(getFilter(new Array(['type','=', 'Target Roles'])));
			}
		}
	},
	
	showCandidate : function() {
		var candidateId = this.form.findField('candidateId').getValue();
		if (candidateId != null && candidateId !='' && candidateId != 0 ) {
			var params = new Array();
			params.push(['id','=', candidateId]);
			var filter = getFilter(params);
			filter.pageSize=50;
			app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onShowCandidate,this);
		}else
			this.updateError('Candidate is not linked to the contact.');		
	},
	
	onShowCandidate : function(data) {
		if ( data.rows.length > 0) {
			var candidate = Ext.create('tz.model.Candidate', data.rows[0]);
			app.setActiveTab(0,'li_candidates');
			app.candidateMgmtPanel.getLayout().setActiveItem(1);
			app.candidateMgmtPanel.candidatePanel.loadForm(candidate);
		}
	},
	
	addSkillsetGroup : function() {
		var panel = this;
		if (Ext.getCmp('addSkillsetGroup') == null) {
			var settingsWindow = Ext.create('Ext.window.Window', {
			    title: 'Add Skillset Group',
			    id : 'addSkillsetGroup',
			    width: 320,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
			        			xtype: 'displayfield',
			        			name: 'type',  
			        			fieldLabel: 'Type',
			        			tabIndex:203,
			        			value : 'Skill Set/Target Skill Set',
			        		},{
		   	                 	xtype:'textfield',
		   	                 	fieldLabel: 'Name *',
		   	                 	name: 'name',
		   	                 	allowBlank:false,
		   	                 	maxLength:45,
		   	                 	enforceMaxLength:true,
		   	                 	tabIndex:201,
		   	             	}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
			    	tooltip:'Save',
			    	tabIndex:205,
		            handler: function(){
		            	var object = {};
		            	for ( var i = 0; i < 2; i++ ) {
		            		var component = Ext.getCmp('addSkillsetGroup').items.items[0].items.items[0].items.items[0].items.items[i];
		            		if(! component.isValid()){
		            			Ext.Msg.alert('Error','Please fix the errors.');
		            			return false;
		            		}else{
		            			object[component.name] = component.value;
		            		}
						}
		            	object = Ext.JSON.encode(object);
		            	app.settingsService.saveSkillsetGroup(object,panel.onAddSkillsetGroup,panel);
		            }
		        }]
			});
		}
		Ext.getCmp('addSkillsetGroup').show();
	},

	onAddSkillsetGroup : function(data) {
		if (data.success) {
	    	Ext.getCmp('addSkillsetGroup').close();
			var skillSetIds = this.form.findField('skillSetGroupIds').getValue();
			if (skillSetIds == null || skillSetIds == '' )
				skillSetIds = new Array();
			else
				skillSetIds = skillSetIds.split(',');
			var skillsets = new Array();
			for ( var i = 0; i < skillSetIds.length; i++) {
				skillsets.push(ds.skillsetGroupStore.getById(Number(skillSetIds[i])).data.name);
			}
			skillSetIds.push(data.returnVal.id);
			skillsets.push(data.returnVal.name);
			this.form.findField('skillSetGroupIds').setValue(skillSetIds.toString());
			this.form.findField('skillSetGroup').setValue(skillsets.toString());
	    	ds.skillsetGroupStore.loadByCriteria();
		}
	},

});