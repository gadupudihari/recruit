Ext.define('tz.ui.ContactGridPanel', {
    extend: 'Ext.grid.Panel',
    id : 'contactGridPanel',
    title: 'Contacts',
    frame:true,
    anchor:'100%',
    //height:560,
    width :'100%',
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value && record.data.id != null){
    			this.modifiedIds.push(record.data.id);
    		}
    		if(editor.context.field == 'cellPhone'){
    			record.data.cellPhone = record.data.cellPhone.replace(/[^0-9]/g, "");
            	record.data.cellPhone = record.data.cellPhone.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
            	this.getView().refresh();
            }else if (editor.context.field == 'clientId') {
            	var client = ds.client_vendorStore.getById(record.data.clientId);
            	if (client) {
            		record.data.type= client.data.type;
            		record.data.city= client.data.city;
            		record.data.state= client.data.state;
            		this.getView().refresh();
				}else{
            		record.data.type= '';
            		record.data.city= '';
            		record.data.state= '';
            		this.getView().refresh();
				}
			}
        },
        beforeedit: function(editor, event) {
      		var record = event.record;
      		if (event.column.dataIndex == "priority" &&  record.data.type.search('SME') == -1 ) {
      			return false;
      		}
        }
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.contactStore;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
            	xtype : 'actioncolumn',
            	width : 30,
            	items : [{
            		icon : 'images/icon_edit.gif',
            		tooltip : 'Edit/View Contact',
            		padding : 10,
            		scope : this,
            		handler : function(grid, rowIndex, colIndex) {
            			var record = ds.contactStore.getAt(rowIndex);
            			me.manager.showContactPanel();
            			me.manager.contactDetailPanel.loadForm(record);
            			me.manager.contactDetailPanel.opendFrom = 'Contacts';
            		} 
            	}]
            },{
				text : 'Unique Id',
				dataIndex : 'id',
				width :  50,
		        renderer: contactRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'firstName',
                text: 'First Name',
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastName',
                text: 'Last Name',
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
				header : 'Client/Vendor Name',
				dataIndex : 'clientId',
	            width :  120,
		        editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                anyMatch:true,
	                forceSelection:true,
	                triggerAction: 'all',
	                displayField: 'name',
	                valueField: 'id',
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.client_vendorStore,
	            },
                renderer:function(value, metadata, record){
                	var rec = ds.client_vendorStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="'+rec.data.name+'<br><b>Open Snapshot</b>"';
                		return '<a href=# style="color: #000000"> ' + rec.data.name + '</a>';
                	}else{
                		metadata.tdAttr = 'data-qtip="' + 'n/a' + '"';
                		return 'n/a';
                	}
                }
			},{
                xtype: 'gridcolumn',
                text: 'Client/Vendor',
				dataIndex : 'clientType',
				renderer: contactRender,
				width :  80,
            },{
				header : 'City',
				dataIndex : 'city',
				renderer: contactRender,
				width :  80,
			},{
				header : 'State',
				dataIndex : 'state',
				renderer: contactRender,
				width :  80,
			},{
                xtype: 'gridcolumn',
                text: 'Type',
				dataIndex : 'type',
				renderer: contactRender,
				width :  90,
		        editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                forceSelection:true,
	                triggerAction: 'all',
	                displayField: 'value',
	                valueField: 'name',
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.contactTypeStore,
	            },
            },{
                xtype: 'gridcolumn',
                dataIndex: 'score',
                text: 'Score',
                width:50,
                renderer: numberRender,
                editor: {
                    xtype: 'numberfield',
                    minValue: 0,
                    maxValue: 100
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'priority',
                text: 'Priority',
                width:50,
                renderer: numberRender,
                editor: {
                    xtype: 'numberfield',
                    minValue: 0,
                    maxValue: 100
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'email',
                text: 'Email',
                width:120,
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
   	                vtype:'email',
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'gender',
                text: 'Gender',
                width:55,
                renderer: contactRender,
		        editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                forceSelection:true,
	                triggerAction: 'all',
	                displayField: 'value',
	                valueField: 'name',
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.genderStore,
	            },
            },{
                xtype: 'gridcolumn',
                dataIndex: 'skillSetGroupIds',
                text: 'Skill Set Group',
                width:100,
                renderer:function(value, metadata, record){
                	var tooltip = "";
            		if (record.data.skillSetGroupIds != null  && record.data.skillSetGroupIds != '') {
    					var skillSetGroupIds = record.data.skillSetGroupIds.split(',');
    					for ( var i = 0; i < skillSetGroupIds.length; i++) {
    						tooltip += ds.skillsetGroupStore.getById(Number(skillSetGroupIds[i])).data.name +"<br>";
    					}
    					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
    				}else
    					return 'N/A' ;
                    return tooltip.replace(/<br>/g, ', ') ;
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'module',
                text: 'Skill Set',
                width:100,
                renderer:function(value, metadata, record){
                	if (value != null && value != ''){
                		metadata.tdAttr = 'data-qtip="' + value.replace(/,/g, '<br>') + '"';
                		return value;
                	}else
                		return 'N/A' ;
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'roleSetIds',
                text: 'Role Set',
                width:100,
                renderer:function(value, metadata, record){
                	var tooltip = "";
            		if (record.data.roleSetIds != null  && record.data.roleSetIds != '') {
    					var roleSet = record.data.roleSetIds.split(',');
    					for ( var i = 1; i <= roleSet.length; i++) {
    						tooltip += ds.targetRolesStore.getById(Number(roleSet[i-1])).data.value +"<br>";
    					}
    					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
    				}else
    					return 'N/A' ;
                    return tooltip.replace(/<br>/g, ', ') ;
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'jobTitle',
                text: 'Job Title',
                width:120,
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'internetLink',
                text: 'Internet Link',
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:50,
   	                enforceMaxLength:true,
   	                vtype:'url',
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'cellPhone',
                text: 'CellPhone',
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:15,
                 	maskRe :  /[0-9]/,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'extension',
                text: 'Extension',
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:140,
                renderer: contactRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:50,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'role',
                text: 'Contact Type',
                width:100,
                renderer: contactRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'confidentiality',
                text: 'Confidentiality',
                width : 45,
                hideable : false,
                renderer : numberRender,
				editor: {
                    xtype: 'numberfield',
                    minValue: 1,
                    maxValue: 9
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'interviews',
                text: 'Interviewer',
                renderer:function(value, p, record){
                	if (value > 0) 
						return 'Yes';
                	else
                		return 'No';
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: contactRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:150,
            }
        ];

        me.viewConfig = {
        		stripeRows: false,
        		style: {overflow:'auto', overflowX: 'hidden'},
        		
                listeners: {
                	celldblclick: function (view, cell, cellIndex, record, row, rowIndex, e) {
                    	var colHeading = view.panel.headerCt.getHeaderAtIndex(cellIndex).text;
                    	if(colHeading == "Client/Vendor Name"){
                    		var clientId = record.data.clientId;
                    		var client = ds.client_vendorStore.getById(clientId);
                    		if (client != null) {
                    			app.homeMgmtPanel.homeDetailPanel.showClient_Vendor(clientId, client.data.type);	
							}
						}
                    },
                }

        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        tooltip: 'Add New Contact',
                        iconCls : 'btn-add',
                        tabIndex:16,
                        handler: function(){
                    		me.addContact();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Save',
                        tooltip: 'Save Contacts',
                        iconCls : 'btn-save',
                        tabIndex:17,
                        handler: function(){
                    		me.saveContacts();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Delete',
                        tooltip: 'Delete selected Contact',
                        iconCls : 'btn-delete',
                        tabIndex:18,
                        handler: function(){
                    		me.deleteConfirm();
                    	}
                    },'-',{
                        xtype:'button',
                    	itemId: 'grid-excel-button',
                    	iconCls : 'btn-report-excel',
                        text: 'Export to Excel',
                        tooltip: 'Export to Excel',
                        tabIndex:19,
                        handler: function(){
                        	me.getExcelExport();
                        	/*var vExportContent = me.getExcelXml();
                            document.location='data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);*/
                        }
                    },'->',me.showCount
                    ]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        ds.client_vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			this.showCount.setValue(msg);
       	}, me);

        me.callParent(arguments);

        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);

    },
    
    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],
    
    addContact : function() {
    	this.manager.showContactPanel();
    	this.manager.contactDetailPanel.clearMessage();
    	this.manager.contactDetailPanel.form.reset();
    	this.manager.contactDetailPanel.deleteButton.enable();
    	this.manager.contactDetailPanel.moduleCombo.hide();
    	this.manager.contactDetailPanel.form.findField('module').hide();
    	this.manager.contactDetailPanel.form.findField('priority').hide();

   	 	var record = Ext.create( 'tz.model.Contact',{
   	 		id : null
   	 	});
		this.manager.contactDetailPanel.loadForm(record);
		this.manager.contactDetailPanel.opendFrom = 'Contacts';
    },
    
    getExcelExport : function() {
    	var values = app.contactMgmtPanel.contactSearchPanel.getValues();
    	var params = new Array();
    	params.push(['type','=', app.contactMgmtPanel.contactSearchPanel.typeCombo.getValue()]);
    	params.push(['client','=', app.contactMgmtPanel.contactSearchPanel.clientCombo.getValue()]);
    	params.push(['score','=', values.score]);
    	params.push(['search','=', values.searchContacts]);
    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;" && this.columns[i].text != '' && !this.columns[i].hidden && this.columns[i].text != "Confidentiality") {
				columns.push(this.columns[i].text + ':'+this.columns[i].width);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	if (this.store.getSorters().length > 0) {
    		if (this.store.getSorters()[0].property == 'type' || this.store.getSorters()[0].property == 'city' || this.store.getSorters()[0].property == 'state' ) 
    			order = 'client.'+this.store.getSorters()[0].property + ' '+this.store.getSorters()[0].direction;
    		else if (this.store.getSorters()[0].property == 'clientId' ) 
    			order = 'client.id '+this.store.getSorters()[0].direction;
    		else
    			order = this.store.getSorters()[0].property + ' '+this.store.getSorters()[0].direction;
    		params.push(['order','=', order]);
		}
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.requirementService.contactExcelExport(Ext.encode(filter));
	},

	saveContacts : function() {
    	var records = new Array();
    	for ( var i = 0; i < ds.contactStore.data.length ; i++) {
			var record = ds.contactStore.getAt(i);

    		if (record.data.clientId == 0 || record.data.clientId == null)
				delete record.data.clientId;
    		if (record.data.candidateId == 0 || record.data.candidateId == null)
				delete record.data.candidateId;
    		if (record.data.candidateDeleted == null )
				delete record.data.candidateDeleted;
    		if (record.data.score == null )
				delete record.data.score;
    		if (record.data.priority == null )
				delete record.data.priority;
    		if (record.data.confidentiality == null )
				delete record.data.confidentiality;
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}else if (this.modifiedIds.indexOf(record.data.id) != -1)
    			records.push(record.data);	
		}
    	for ( var i = 0; i < records.length; i++) {
    		var record = records[i];
    		if (record.firstName == null || record.firstName == ""){
    			Ext.Msg.alert('Error','Contact First Name should not be blank');
    			return false;
    		}else if (record.lastName == null || record.lastName == "") {
    			Ext.Msg.alert('Error','Contact Last Name should not be blank');
    			return false;
    		}
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Contacts to save');
    		return;
		}
    	records = Ext.JSON.encode(records);
    	app.contactService.saveContacts(records,this.onSave,this);
	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error',data.errorMessage);
		this.modifiedIds =  new Array();
		app.contactMgmtPanel.contactSearchPanel.search();
	},
	
	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Contact to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Contact","Do you delete selected Contact?", this.deleteContact, this);
		}
	},
	
	deleteContact: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.contactService.deleteContact(selectedRecord.data.id, this.onDeleteContact, this);
		}
	},
	
	onDeleteContact : function(data){
		if (!data.success) {
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.contactMgmtPanel.contactSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	},
	
});

function contactRender(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null || value=="") 
    	return 'N/A' ;
    return value;
}
