Ext.define('tz.service.ContactService', {
	extend : 'tz.service.Service',
		
	saveContacts : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/saveContacts',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    deleteContact : function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/deleteContact/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    saveContact : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/saveContact',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

	getContacts : function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/getContacts',
			params : {
				json : filter
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    linkCandidate : function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/linkCandidate',
			params : {
				json : filter
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
	},
	
	unlinkCandidate : function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/unlinkCandidate/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
	

});

