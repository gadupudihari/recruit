Ext.define('tz.ui.ContactSearchPanel', {
    extend: 'Ext.form.Panel',
    id : 'contactSearchPanel',
    bodyPadding: 10,
    title: '',
    frame:true,
    layout:'anchor',
    anchor:'100%',
    autoScroll:true,
    buttonAlign:'left',
    fieldDefaults: { msgTarget: 'side',labelAlign: 'left',labelWidth :80,},
//creating view in init function
    initComponent: function() {
        var me = this;
        
		me.typeCombo = Ext.create('Ext.form.ComboBox', {
			name : 'type',
		    fieldLabel: 'Type',
		    forceSelection:true,
		    queryMode: 'local',
            displayField: 'value',
		    valueField: 'name',
            store: ds.clientTypeStore,
            emptyText:'Select Client/Vendor',
		    tabIndex:6,
		});

		me.contactTypeCombo = Ext.create('Ext.form.ComboBox', {
			name : 'type',
		    fieldLabel: 'Type',
		    forceSelection:true,
		    queryMode: 'local',
            displayField: 'value',
		    valueField: 'name',
            store: ds.contactTypeStore,
            emptyText:'Select contact type',
		    tabIndex:4,
		});

		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client/Vendor',
		    name : 'contact_clientId',
		    queryMode: 'local',
            displayField: 'name',
            valueField: 'id',
            emptyText:'Select...',
            listClass: 'x-combo-list-small',
            store: ds.client_vendorStore,
		    tabIndex:2,
		    anyMatch:true,
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});

		me.roleCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Contact Type",
            name: 'role',
	        tabIndex:10,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.contactRoleStore,
            anyMatch:true,
		});

		me.genderCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Gender",
            name: 'gender',
	        tabIndex:8,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.genderStore,
            anyMatch:true,
		});

        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Search',
	   	                 name: 'searchContacts',
	   	                 tabIndex:1,
	   	             },{
	   	                 xtype:'numberfield',
	   	              allowDecimals: false,
	   	                 fieldLabel: 'Score',
	   	                 name: 'score',
	   	                 tabIndex:5,
	   	             },{
	   	            	 xtype:'numberfield',
	   	            	 fieldLabel: 'Priority',
	   	            	 name: 'priority',
	   	            	allowDecimals: false,
	   	            	 tabIndex:9,
	   	            	 minValue: 0,
	   	            	 maxValue: 100
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [me.clientCombo,me.typeCombo,me.roleCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Unique Ids(Ex:1,2)',
	   	                 name: 'id',
	   	                 labelWidth :110,
	   	                 tabIndex:3
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Module',
	   	                 name: 'module',
	   	                 labelWidth :110,
	   	                 tabIndex:7,
	   	                 inputAttrTpl: " data-qtip='Enter , separated modules' ",
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [me.contactTypeCombo,me.genderCombo]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	tooltip : 'Search for Contacts',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:11,
			},{
				text : 'Reset',
				tooltip : 'Reset and Search for Contacts',
				scope: this,
				handler: this.reset,
				tabIndex:12,
			},{
				text : 'Clear',
				tooltip : 'Clear all filters',
				scope: this,
				tabIndex:13,
	             handler: function() {
	            	 this.form.reset();
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['type','=', this.typeCombo.getValue()]);
    	params.push(['contactType','=', this.contactTypeCombo.getValue()]);
    	params.push(['client','=', this.clientCombo.getValue()]);
    	params.push(['score','=', values.score]);
    	params.push(['search','=', values.searchContacts]);
    	params.push(['module','=', values.module]);
    	params.push(['priority','=', values.priority]);
    	params.push(['role','=', values.role]);
    	params.push(['gender','=', values.gender]);
    	var id = values.id;
    	id = id.replace(',,',",");
    	while (id.length >0) {
        	if (id.substr(id.length-1,id.length) == ",")
        		id = id.substr(0,id.length-1);
			else
				break;
		}
    	if (id.search(',') != -1) 
    		id = "'"+id.replace(/,/g, "','")+"'";

    	params.push(['id','=', id]);

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=200;
    	ds.contactStore.loadByCriteria(filter);
    	app.contactMgmtPanel.contactGridPanel.modifiedIds = new Array();
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
