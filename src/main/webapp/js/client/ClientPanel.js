Ext.define('tz.ui.ClientPanel', {
	extend : 'tz.ui.BaseFormPanel',
	
	bodyPadding: 10,
    title: '',
    anchor:'100% 95%',
    
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},
	
	initComponent : function() {
		 var me = this;
		 me.openedFrom ="";
		 
		me.vendorTypeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type *',
		    store: ds.clientTypeStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:false,
		    valueField: 'name',
		    name : 'type',
		    tabIndex:402,
		    listeners: {
				scope:this,
				change: function(combo,value){
					if (combo.value == 'Vendor') {
						this.helpCandidateCombo.show();
						this.form.findField('helpCandidateComments').hide();
						this.skillSetGroupCombo.show();
						this.addSkillsetButton.show();
						this.form.findField('skillSetGroup').show();
					}else {
						this.helpCandidateCombo.hide();
						this.helpCandidateCombo.reset();
						this.form.findField('helpCandidateComments').hide();
						this.form.findField('helpCandidateComments').reset();
						this.skillSetGroupCombo.hide();
						this.addSkillsetButton.hide();
						this.form.findField('skillSetGroup').hide();
						this.form.findField('skillSetGroup').reset();
					}
					this.setReferences(combo.value);
				}
		    },
		});
		
		me.contractorsCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
			anyMatch:true,
		    fieldLabel: 'Contractors',
		    store: ds.contractorsStore,
		    queryMode: 'local',
		    anyMatch:true,
		    forceSelection:true,
            displayField: 'fullName',
            valueField: 'id',
		    name : 'contractorId',
		    padding : '20 0 0 0',
		    tabIndex:422,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					this.form.findField('contractors').setValue(combo.rawValue);
				}
		    },
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});
		
		me.fullTimeCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Full Time',
		    store: ds.contractorsStore,
		    anyMatch:true,
		    queryMode: 'local',
		    forceSelection:true,
            displayField: 'fullName',
            valueField: 'id',
		    name : 'fullTimeId',
		    padding : '30 0 0 0',
		    tabIndex:423,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					this.form.findField('fullTime').setValue(combo.rawValue);
				}
		    },
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});

		me.skillSetGroupCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Skill Set Group",
            name: 'selectedSkillSetGroup',
	        tabIndex:425,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'id',
            store: ds.skillsetGroupStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    enableKeyEvents : true,
		    padding : '30 0 0 0',
		    listeners: {
				scope:this,
				select: function(combo,value){
					var skillSetIds = this.form.findField('skillSetGroupIds').getValue();
					if (skillSetIds == null || skillSetIds == '' )
						skillSetIds = new Array();
					else
						skillSetIds = skillSetIds.split(',');
					if (skillSetIds.indexOf(combo.value.toString()) == -1) {
						skillSetIds.push(combo.value.toString());
					}else{
						skillSetIds.splice(skillSetIds.indexOf(combo.value.toString()), 1);
					}
					var skillsets = new Array();
					for ( var i = 0; i < skillSetIds.length; i++) {
						skillsets.push(ds.skillsetGroupStore.getById(Number(skillSetIds[i])).data.name);
					}
					me.skillSetGroupCombo.reset();
					this.form.findField('skillSetGroupIds').setValue(skillSetIds.toString());
					this.form.findField('skillSetGroup').setValue(skillsets.toString());
				},
		    }
		});

        me.addSkillsetButton = new Ext.Button({
        	iconCls : 'btn-add',
        	tooltip : 'Add Skillset Group',
        	margin: '30 0 0 10',
        	scope: this,
        	handler: function(){
        		this.addSkillsetGroup();
        	}
        });

		me.referenceCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Related Clients/Vendors',
		    store: ds.client_vendorStore,
		    anyMatch:true,
		    queryMode: 'local',
		    forceSelection:true,
            displayField: 'name',
            valueField: 'id',
		    name : 'referenceId',
		    padding : '30 0 0 0',
		    tabIndex:424,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					this.form.findField('reference').setValue(combo.rawValue);
				}
		    },
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});
		
		me.helpCandidateCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Ok to train Candidate",
            name: 'helpCandidate',
	        tabIndex:426,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.helpCandidateStore,
		    forceSelection:true,
		    allowBlank:true,
		    padding : '30 0 0 0',
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					if (combo.value == 'Other')
						this.form.findField('helpCandidateComments').show();
					else{
						this.form.findField('helpCandidateComments').hide();
						this.form.findField('helpCandidateComments').reset();
					}
					this.doLayout();
				}
		    }
		});

		me.recruiterCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Recruiter",
            name: 'recruiter',
            store: ds.recruiterStore,
		    queryMode: 'local',
		    displayField: 'fullName',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'id',
		    tabIndex:412,
		});

		me.items = [ me.getMessageComp(), me.getHeadingComp(), {
	         xtype: 'container',
	         items:[
			         {xtype: 'hidden', name: 'id'},
	                {
				xtype : 'fieldset',
				title : 'Client/Vendor Information',
				collapsible : true,
				defaultType : 'textfield',
				layout : 'column',
				items : [
				         {
	             xtype: 'container',
	             columnWidth:.3,
	             items: [{
	                 xtype:'textfield',
	                 fieldLabel: 'Name *',
	                 name: 'name',
	                 allowBlank:false,
	                 tabIndex:401,
	                 maxLength : 50,
	                 enforceMaxLength:true,
	             },{
	                 xtype:'textfield',
	                 fieldLabel: 'Email',
	                 name: 'email',
   	                 vtype:'email',
   	                 tabIndex:403,
	                 maxLength : 100,
	                 enforceMaxLength:true,
	             },{
	                 xtype:'textfield',
	                 fieldLabel: 'Fax',
	                 name: 'fax',
	                 maxLength : 50,
	                 tabIndex:405,
	                 enforceMaxLength:true,
	             },{
	                 xtype:'textfield',
	                 fieldLabel: 'Website',
	                 name: 'website',
   	                 //vtype:'url',
   	                 tabIndex:407,
	                 maxLength : 100,
	                 enforceMaxLength:true,
	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "City",
   	                 name: 'city',
   	                 tabIndex:409,
   	                 maxLength:90,
   	                 enforceMaxLength:true,
   	             },{
   	            	 xtype:'numberfield',
   	            	allowDecimals: false,
   	            	 fieldLabel: 'Confidentiality',
   	            	 name: 'confidentiality',
   	            	 tabIndex:411,
   	            	 minValue: 1,
   	            	 maxValue: 9,
   	            	 listeners: {
   	            		 afterrender: function() {
   	            			 if (app.userRole == 'ADMIN')
   	            				 this.show();
   	            			 else
   	            				 this.hide();
   	            		 }
   	            	 }
 				},
 				me.recruiterCombo,
  	            {
 					xtype:'textareafield',
 					fieldLabel: 'Address',
 					name: 'address',
 					//width:400,
 					maxLength : 200,
 					enforceMaxLength:true,
 					tabIndex:420,
   	             },
   	             me.contractorsCombo,
   	             me.fullTimeCombo,
   	             me.referenceCombo,
   	             {
   	            	 xtype: 'container',
   	            	 layout : 'table',
   	            	 items: [me.skillSetGroupCombo,me.addSkillsetButton]
   	             },
   	             me.helpCandidateCombo,
	             ]
	         },{
	             xtype: 'container',
	             columnWidth:.5,
	             items: [this.vendorTypeCombo,
	                     {
	                 xtype:'textfield',
	                 fieldLabel: 'Contact Number',
	                 name: 'contactNumber',
	                 maxLength : 15,
	                 maskRe :  /[0-9]/,
	                 tabIndex:404,
	                 enforceMaxLength:true,
   	                 listeners:{
   	                	 change : function(field,newValue,oldValue){
   	                		 var value = me.form.findField('contactNumber').getValue();
   	                		 if (value != "" ) {
   	                			 value = value.replace(/[^0-9]/g, "");
	   	                		 value = value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
	   	                		 me.form.findField('contactNumber').setValue(value);
   	                		 }
					    }
   	                 }
	             },{
	                 xtype:'numberfield',
	                 allowDecimals: false,
	                 fieldLabel: 'Priority',
	                 name: 'score',
	                 minValue: 0,
	                 maxValue: 10,
	                 tabIndex:406,
	                 enforceMaxLength:true,
	             },{
	                 xtype:'textfield',
	                 fieldLabel: 'Industry',
	                 name: 'industry',
	                 tabIndex:408,
	                 maxLength : 50,
	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "State",
   	                 name: 'state',
   	                 tabIndex:410,
   	                 maxLength:90,
   	                 enforceMaxLength:true,
	             },{
	            	 xtype : 'textarea',
	            	 fieldLabel : 'About',
	            	 name : 'about',
	            	 width:400,
	            	 maxLength : 100,
	            	 enforceMaxLength:true,
	            	 tabIndex:413,
	             },{
	            	 xtype : 'textarea',
	            	 fieldLabel : 'Comments',
	            	 name : 'comments',
	            	 width:400,
	            	 maxLength : 250,
	            	 enforceMaxLength:true,
	            	 tabIndex:421,
				},{
   	                 xtype:'textarea',
   	                 fieldLabel: 'Selected Contractors',
   	                 name: 'contractors',
   	                 readOnly:true,
   	                 width:400,
   	                 height : 50,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: 'Selected Full Time',
   	                 name: 'fullTime',
   	                 readOnly:true,
   	                 width:400,
   	                 height : 50,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: 'Selected Clients/Vendors',
   	                 name: 'reference',
   	                 readOnly:true,
   	                 width:400,
   	                 height : 50,
   	             },{
   	            	 xtype: 'textarea',
   	            	 fieldLabel: 'Selected Skill Set Group',
   	            	 name: 'skillSetGroup',
   	            	 readOnly : true,
   	            	 width : 400,
   	                 height : 50,
   	             },{
   	            	 xtype: 'textfield',
   	            	 fieldLabel: 'Skillset Group Ids',
   	            	 name: 'skillSetGroupIds',
   	            	 readOnly : true,
   	            	 hidden : true,
   	                 height : 50,
   	             },{
   	            	 xtype: 'textarea',
   	            	 fieldLabel: 'Comments',
   	            	 name: 'helpCandidateComments',
   	            	 hidden : true,
   	            	 tabIndex:427,
   	            	 width : 400,
   	            	 maxLength:100,
   	            	 enforceMaxLength:true,
   	                 height : 50,
   	             },{
					xtype : 'datefield',
	            	 name : 'created',
	            	 hidden : true
				}]
	         }]
	        }]
	     }
	   	 ];
		
		
        me.saveButton = new Ext.Button({
			text : 'Save',
			tooltip : 'Save',
			iconCls : 'btn-save',
			tabIndex:431,
			scope: this,
	        handler: this.saveClient
        });

        me.saveAndCloseButton = new Ext.Button({
			text : 'Save & Close',
			tooltip : 'Save & Close',
			iconCls : 'btn-save',
			tabIndex:432,
			scope: this,
	        handler: this.saveClient
        });

        me.deleteButton = new Ext.Button({
			text : 'Delete',
			tooltip : 'Delete',
			iconCls : 'btn-delete',
			tabIndex:433,
			scope: this,
	        handler: this.deleteConfirm
        });

        me.closeButton = new Ext.Button({
			text : 'Close',
			tooltip : 'Close',
			iconCls : 'btn-close',
			tabIndex:434,
			scope: this,
	        handler: this.closeFormDirty
        });

		me.tbar =  [ this.saveButton,this.saveAndCloseButton ,'-', this.deleteButton , '-', this.closeButton ];

		this.callParent(arguments);
	},
	
	closeFormDirty: function(){
		if (this.openedFrom == "Recruit") {
			app.setActiveTab(0,'li_requirements');
	    	var params = new Array();
	    	var filter = getFilter(params);
	    	filter.pageSize=1000;
			ds.client_vendorStore.loadByCriteria(filter);

	    	params.push(['type','=', 'Vendor']);
	    	var filter = getFilter(params);
	    	ds.vendorStore.loadByCriteria(filter);
		}else if (this.openedFrom == "ContactDetailPanel") {
	    	app.setActiveTab(0,'li_contacts');
	    	app.contactMgmtPanel.getLayout().setActiveItem(1);
		}else{
			app.clientMgmtPanel.initScreen();
			app.clientMgmtPanel.clientSearchPanel.search();
		}
	},
	
	
    loadForm : function(record) {
    	ds.contractorsStore.clearFilter();
    	this.clearMessage();
    	this.form.reset();
		this.loadRecord(record);
		this.openedFrom = "Client";
		this.saveAndCloseButton.hide();
		this.saveButton.show();
		var contractorIds = record.data.contractorId.split(',');
		contractorIds = contractorIds.map(Number);
		this.contractorsCombo.setValue(contractorIds);
		var fullTimeIds = record.data.fullTimeId.split(',');
		contractorsCombo = fullTimeIds.map(Number);
		this.fullTimeCombo.setValue(contractorsCombo);
		
		ds.client_vendorStore.clearFilter(true);
		if (record.data.type == "Client"){
  			ds.client_vendorStore.filterBy(function(rec) {
  			    return rec.get('type') === 'Vendor';
  			});
		}else if (record.data.type == "Vendor") {
  			ds.client_vendorStore.filterBy(function(rec) {
  			    return rec.get('type') === 'Client';
  			});
		}
	    if (record.data.type == 'Vendor') {
	    	this.helpCandidateCombo.show();
			this.skillSetGroupCombo.show();
			this.addSkillsetButton.show();
			this.form.findField('skillSetGroup').show();
	    }else{
	    	this.helpCandidateCombo.hide();
			this.skillSetGroupCombo.hide();
			this.addSkillsetButton.hide();
			this.form.findField('skillSetGroup').hide();
	    }
	    this.helpCandidateCombo.setValue(record.data.helpCandidate);
		
		if (record.data.skillSetGroupIds != null && record.data.skillSetGroupIds != '') {
			var skillSetGroupIds = record.data.skillSetGroupIds.split(',');
			var skillSetGroup ='';
			for ( var i = 0; i < skillSetGroupIds.length; i++) {
				skillSetGroup += ds.skillsetGroupStore.getById(Number(skillSetGroupIds[i])).data.name;
				if (skillSetGroupIds.length > (i+1))
					skillSetGroup += ",";
			}
			this.form.findField('skillSetGroup').setValue(skillSetGroup);
		}
	    
		var referenceId = record.data.referenceId.split(',');
		referenceId = referenceId.map(Number);
		this.referenceCombo.setValue(referenceId);
		
		if (record.data.referenceId != null && record.data.referenceId != '') 
			this.form.findField('type').setReadOnly(true);
		else
			this.form.findField('type').setReadOnly(false);
		
	},

	saveClient: function(){
		this.clearMessage();
		if(! this.form.isValid()){
			this.updateError('Please fix the errors.');
			return false;
		}
		var itemslength = this.form.getFields().getCount();
   		var i = 0;    		
   		var client = {};
   		var recruiterObj = {};
   		
   		while (i < itemslength) {
   			var fieldName = this.form.getFields().getAt(i).name;
   				if(fieldName == 'id' ) {
   					var idVal = this.form.findField(fieldName).getValue();
   					if(typeof(idVal) != 'undefined' && idVal > 0) {
   						client[fieldName] = this.form.findField(fieldName).getValue();
   					}
   				}else if (fieldName == 'contractorId' || fieldName == 'fullTimeId' || fieldName == 'referenceId' ) {
					var list = this.form.findField(fieldName).getValue();
					if (list != null)
						client[fieldName] = this.form.findField(fieldName).getValue().toString();
   				}else {
   					client[fieldName] = this.form.findField(fieldName).getValue();
   				}
   			i = i + 1;
   		}
		if (client.score == null )
			delete client.score;
   		if (client.confidentiality == null )
   			delete client.confidentiality;
   		if (client.recruiter == null )
   			delete client.recruiter;
   		
   		delete client.reference;
   		delete client.skillSetGroup;
   		delete client.skillSetGroupIds;
   		delete client.selectedSkillSetGroup;
   		
   		//skillset group values
		var skillSetIds = this.form.findField('skillSetGroupIds').getValue();
		if (skillSetIds == null || skillSetIds == '' )
			skillSetIds = new Array();
		else
			skillSetIds = skillSetIds.split(',');
		var vendor_skillSetGroup = new Array();
		for ( var i = 0; i < skillSetIds.length; i++) {
			var record = {};
			record.skillSetGroupId = Number(skillSetIds[i]);
			vendor_skillSetGroup.push(record);
		}

		vendor_skillSetGroup = Ext.JSON.encode(vendor_skillSetGroup);
		var clientObj = Ext.JSON.encode(client);
		clientObj =  '{"vendor_SkillSetGroups":{"com.recruit.client.service.Vendor_SkillSetGroup":'+vendor_skillSetGroup+'},'+clientObj.substring(1,clientObj.length);
		app.clientService.saveclient(clientObj, this.onSaveClient, this);
	},
	
	onSaveClient: function(data){
		if(!data.success){
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}else{
			if (this.openedFrom == "Recruit") {
				app.requirementMgmtPanel.requirementsGrid.getRecruitmentTab();
			}else{
				this.getForm().findField('id').setValue(data.returnVal.id);
				this.updateMessage('Saved successfully');
			}
		}
		this.body.scrollTo('top', 0);
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
		ds.client_vendorStore.loadByCriteria(filter);
	},
	
	deleteConfirm : function()
	{
		Ext.Msg.confirm("This will delete the Client", "Do you want to continue?", this.deleteClient, this);
	},
	
	onDelete : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			app.clientMgmtPanel.initScreen();
			app.clientMgmtPanel.clientSearchPanel.search();
		}
		this.body.scrollTo('top', 0);
	},
	
	deleteClient: function(dat){
		if(dat=='yes'){
			var vendor = this.form.getValues();
			app.clientService.deleteClient(vendor.id, this.onDelete, this);
		}
	},
	
	setReferences : function(type) {
		this.referenceCombo.reset();
		ds.client_vendorStore.clearFilter(true);
		if (type == "Client"){
  			ds.client_vendorStore.filterBy(function(rec) {
  			    return rec.get('type') === 'Vendor';
  			});
		}else if (type == "Vendor") {
  			ds.client_vendorStore.filterBy(function(rec) {
  			    return rec.get('type') === 'Client';
  			});
		}
        var records = new Array();
        for ( var i = 0; i < ds.client_vendorStore.getCount(); i++) {
        	records.push(ds.client_vendorStore.getAt(i));
		}
        var newClient_VendorStore = new tz.store.ClientSearchStore({data: records}); 
        this.referenceCombo.bindStore(newClient_VendorStore);
	},
	
	addSkillsetGroup : function() {
		var panel = this;
		if (Ext.getCmp('addSkillsetGroup') == null) {
			var settingsWindow = Ext.create('Ext.window.Window', {
			    title: 'Add Skillset Group',
			    id : 'addSkillsetGroup',
			    width: 320,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
			        			xtype: 'displayfield',
			        			name: 'type',  
			        			fieldLabel: 'Type',
			        			tabIndex:203,
			        			value : 'Skill Set/Target Skill Set',
			        		},{
		   	                 	xtype:'textfield',
		   	                 	fieldLabel: 'Name *',
		   	                 	name: 'name',
		   	                 	allowBlank:false,
		   	                 	maxLength:45,
		   	                 	enforceMaxLength:true,
		   	                 	tabIndex:201,
		   	             	}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
			    	tooltip:'Save',
			    	tabIndex:205,
		            handler: function(){
		            	var object = {};
		            	for ( var i = 0; i < 2; i++ ) {
		            		var component = Ext.getCmp('addSkillsetGroup').items.items[0].items.items[0].items.items[0].items.items[i];
		            		if(! component.isValid()){
		            			Ext.Msg.alert('Error','Please fix the errors.');
		            			return false;
		            		}else{
		            			object[component.name] = component.value;
		            		}
						}
		            	object = Ext.JSON.encode(object);
		            	app.settingsService.saveSkillsetGroup(object,panel.onAddSkillsetGroup,panel);
		            }
		        }]
			});
		}
		Ext.getCmp('addSkillsetGroup').show();
	},

	onAddSkillsetGroup : function(data) {
		if (data.success) {
	    	Ext.getCmp('addSkillsetGroup').close();
			var skillSetIds = this.form.findField('skillSetGroupIds').getValue();
			if (skillSetIds == null || skillSetIds == '' )
				skillSetIds = new Array();
			else
				skillSetIds = skillSetIds.split(',');
			var skillsets = new Array();
			for ( var i = 0; i < skillSetIds.length; i++) {
				skillsets.push(ds.skillsetGroupStore.getById(Number(skillSetIds[i])).data.name);
			}
			skillSetIds.push(data.returnVal.id);
			skillsets.push(data.returnVal.name);
			this.form.findField('skillSetGroupIds').setValue(skillSetIds.toString());
			this.form.findField('skillSetGroup').setValue(skillsets.toString());

	    	ds.skillsetGroupStore.loadByCriteria();
		}
	},
});
