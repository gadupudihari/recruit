Ext.define('tz.ui.ClientSearchPanel', {
    extend: 'Ext.form.Panel',
    id : 'clientSearchPanel',
    bodyPadding: 10,
    title: '',
    frame:true,
    layout:'anchor',
    anchor:'100%',
    autoScroll:true,
    buttonAlign:'left',
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth: 80},
//creating view in init function
    initComponent: function() {
        var me = this;
        
		me.typeCombo = Ext.create('Ext.form.ComboBox', {
			name :'type',
		    fieldLabel: 'Type',
		    forceSelection:true,
		    queryMode: 'local',
            displayField: 'value',
		    valueField: 'name',
		    value : 'Vendor',
            store: ds.clientTypeStore,
		    tabIndex:7,
		});

		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client/Vendor',
		    name : 'client_clientId',
		    queryMode: 'local',
            displayField: 'name',
            valueField: 'id',
            emptyText:'Select...',
            listClass: 'x-combo-list-small',
            store: ds.client_vendorStore,
		    tabIndex:2,
		    anyMatch:true,
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});

		me.contractorCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Contractors',
		    store: ds.contractorsStore,
		    anyMatch:true,
		    queryMode: 'local',
            displayField: 'fullName',
            valueField: 'id',
		    name : 'contractorId',
		    tabIndex:3,
		});
		
		me.fullTimeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Full Time',
		    store: ds.contractorsStore,
		    anyMatch:true,
		    queryMode: 'local',
            displayField: 'fullName',
            valueField: 'id',
		    name : 'fullTimeId',
		    tabIndex:8,
		});

        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.2,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Search',
	   	                 name: 'searchClients',
	   	                 tabIndex:1,
	   	             },{
	   	                 xtype:'numberfield',
	   	              allowDecimals: false,
	   	                 fieldLabel: 'Priority',
	   	                 name: 'score',
	   	                 tabIndex:6,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.2,
	   	             items: [me.clientCombo,me.typeCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.2,
	   	             items: [me.contractorCombo,me.fullTimeCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.2,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'City',
	   	                 name: 'city',
	   	                 tabIndex:4,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'State',
	   	                 name: 'state',
	   	                 tabIndex:9,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.2,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Unique Ids (Ex:1,2)',
	   	                 name: 'id',
	   	                 labelWidth :115,
	   	                 tabIndex:5
	   	             }]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	tooltip : 'Search for Client/Vendor',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:8,
			},{
				text : 'Reset',
				tooltip : 'Reset and Search for Client/Vendor',
				scope: this,
				handler: this.reset,
				tabIndex:9,
			},{
				text : 'Clear',
				tooltip : 'Clear all filters',
				scope: this,
				tabIndex:10,
	             handler: function() {
	            	 this.form.reset();
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['type','=', this.typeCombo.getValue()]);
    	params.push(['id','=', this.clientCombo.getValue()]);
    	params.push(['fullTime','=', this.fullTimeCombo.getValue()]);
    	params.push(['contractor','=', this.contractorCombo.getValue()]);
    	params.push(['score','=', values.score]);
    	params.push(['search','=', values.searchClients]);
    	params.push(['city','=', values.city]);
    	params.push(['state','=', values.state]);
    	var id = values.id;
    	id = id.replace(',,',",");
    	while (id.length >0) {
        	if (id.substr(id.length-1,id.length) == ",")
        		id = id.substr(0,id.length-1);
			else
				break;
		}
    	if (id.search(',') != -1) 
    		id = "'"+id.replace(/,/g, "','")+"'";
    	params.push(['id','=', id]);
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=200;
    	ds.clientStore.loadByCriteria(filter);
    	app.clientMgmtPanel.clientGridPanel.modifiedIds = new Array();
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
