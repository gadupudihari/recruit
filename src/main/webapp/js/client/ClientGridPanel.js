Ext.define('tz.ui.ClientGridPanel', {
    extend: 'Ext.grid.Panel',
    id : 'clientGridPanel',
    title: 'Clients',
    frame:true,
    anchor:'100%',
    //height:560,
    width :'100%',
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value && record.data.id != null){
    			this.modifiedIds.push(record.data.id);
    		}
    		if(editor.context.field == 'contactNumber'){
    			record.data.contactNumber = record.data.contactNumber.replace(/[^0-9]/g, "");
            	record.data.contactNumber = record.data.contactNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
            	this.modifiedIds.push(record.data.id);
            	this.getView().refresh();
            }else if(editor.context.field == 'type' && record.data.type == 'Client'){
    			record.data.helpCandidate = null;
    			this.getView().refresh();
    		}
        }
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.clientStore;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :40,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'View client',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					app.clientMgmtPanel.showClient(rowIndex);
    				}
    			}]
            },{
				text : 'Unique Id',
				dataIndex : 'id',
				width :  70,
		        renderer: clientRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'name',
                text: 'Name',
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:50,
   	                enforceMaxLength:true,
                }
            },{
    			text : 'Type',
    			dataIndex : 'type',
                renderer: clientRender,
                editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    forceSelection:true,
                    displayField: 'value',
        		    valueField: 'name',
                    store: ds.clientTypeStore,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                },
    		},{
                xtype: 'gridcolumn',
                dataIndex: 'score',
                text: 'Priority',
                width:80,
                renderer: numberRender,
                editor: {
                    xtype: 'numberfield',
                    minValue: 0,
                    maxValue: 10
                },
            },{
                xtype: 'gridcolumn',
                dataIndex: 'city',
                text: 'City',
                width:100,
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:90,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'state',
                text: 'State',
                width:100,
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:90,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'address',
                text: 'Address',
                width:140,
                renderer: clientRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:200,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:140,
                renderer: clientRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:250,
   	                enforceMaxLength:true,
                }
            },{
				header : 'Recruiter',
				dataIndex : 'recruiter',
	            width :  100,
		        editor: {
	                xtype: 'combobox',
	                forceSelection:true,
	                queryMode: 'local',
	                triggerAction: 'all',
	                displayField: 'fullName',
	                valueField: 'id',
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.recruiterStore,
	                tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item"><b>{fullName}</b></div>','</tpl>'),
	            },
	            renderer:function(value, metadata, record){
	            	if (value != null && value != ''){
	                	var rec = ds.recruiterStore.getById(value);
	                	if(rec){
	                		metadata.tdAttr = 'data-qtip="' + rec.data.fullName + '"';
	                		return rec.data.fullName;
	                	}	
                	}else{
                		metadata.tdAttr = 'data-qtip="' + 'N/A' + '"';
                		return 'N/A';
                	}
                }
			},{
				header : 'Contractors',
				dataIndex : 'contractorId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != null && value != '') {
    					var ids = value.split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contractorsStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.fullName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					if (ids.length > 1)
    						return names.substring(0,names.length-2);
    					return '('+ids.length+') '+names.substring(0,names.length-2);
					}else
						return 'N/A';
                }
			},{
				header : 'Full Time',
				dataIndex : 'fullTimeId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != null && value != '') {
    					var ids = value.split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contractorsStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.fullName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					if (ids.length > 1)
    						return names.substring(0,names.length-2);
    					return '('+ids.length+') '+names.substring(0,names.length-2);
					}else
						return 'N/A';
                }
			},{
				header : 'Related Clients/Vendors',
				dataIndex : 'referenceId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != null && value != '') {
    					var ids = value.split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.client_vendorStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.name +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return '('+ids.length+') '+names.substring(0,names.length-2);
					}else
						return 'N/A';
                }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'contactNumber',
                text: 'Contact Number',
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:15,
                 	maskRe :  /[0-9]/,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'fax',
                text: 'Fax',
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:50,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'email',
                text: 'Email',
                width:120,
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:100,
   	                enforceMaxLength:true,
   	                vtype:'email',
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'website',
                text: 'Website',
                width:120,
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:100,
   	                enforceMaxLength:true,
   	                //vtype:'url',
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'industry',
                text: 'Industry',
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:50,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'about',
                text: 'About',
                width:140,
                renderer: clientRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:100,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'helpCandidate',
                text: 'Ok to train Candidate',
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'confidentiality',
                text: 'Confidentiality',
                width : 45,
                hideable : false,
                renderer : numberRender,
				editor: {
                    xtype: 'numberfield',
                    minValue: 1,
                    maxValue: 9
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: clientRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:150,
            }
        ];
        me.viewConfig = {
        		stripeRows: true	
        };
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        tooltip: 'Add New',
                        iconCls : 'btn-add',
                        tabIndex:11,
                        handler: function(){
                    		me.addClient();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Save',
                        tooltip: 'Save',
                        iconCls : 'btn-save',
                        tabIndex:12,
                        handler: function(){
                    		me.saveClients();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Delete',
                        tooltip: 'Delete',
                        iconCls : 'btn-delete',
                        tabIndex:13,
                        handler: function(){
                    		me.deleteConfirm();
                    	}
                    },'-',{
                        xtype:'button',
                    	itemId: 'grid-excel-button',
                    	iconCls : 'btn-report-excel',
                        text: 'Export to Excel',
                        tooltip: 'Export to Excel',
                        tabIndex:14,
                        handler: function(){
                        	me.getExcelExport();
                        }
                    },'->',me.showCount
                    ]
            }
        ];
        
        ds.contractorsStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.client_vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			me.showCount.setValue(msg);
       	}, me);

        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        me.callParent(arguments);
        
        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);
        
    },
    
    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],
    
    getExcelExport : function() {
    	var values = app.clientMgmtPanel.clientSearchPanel.getValues();
    	var params = new Array();
    	params.push(['type','=', app.clientMgmtPanel.clientSearchPanel.typeCombo.getValue()]);
    	params.push(['id','=', app.clientMgmtPanel.clientSearchPanel.clientCombo.getValue()]);
    	params.push(['fullTime','=', app.clientMgmtPanel.clientSearchPanel.fullTimeCombo.getValue()]);
    	params.push(['contractor','=', app.clientMgmtPanel.clientSearchPanel.contractorCombo.getValue()]);
    	params.push(['score','=', values.score]);
    	params.push(['search','=', values.searchClients]);
    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;" && this.columns[i].text != '' && !this.columns[i].hidden && this.columns[i].text != "Confidentiality") {
				columns.push(this.columns[i].text + ':'+this.columns[i].width);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.requirementService.clientExcelExport(Ext.encode(filter));
	},

    addClient : function() {
	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.Client',{
   	 		id : null,
   	 		comments :'N/A',
   	 		email :'',
   	 		website :'N/A',
   	 		contactNumber :'N/A',
   	 		fax :'N/A',
   	 		industry :'N/A',
   	 		about :'N/A',
   	 		address :'N/A',
   	 		cityAndState :'N/A',
   	 		helpCandidate : '100-Yes',
   	 		lastUpdatedUser :null,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});
   	 	
        this.store.insert(0, rec);
	},
    
	validateClient : function() {
    	for ( var i = 0; i < ds.clientStore.data.length ; i++) {
			var record = ds.clientStore.getAt(i);
		}
    	return true;
	},
	
    saveClients : function() {
    	var records = new Array();
    	for ( var i = 0; i < ds.clientStore.data.length ; i++) {
			var record = ds.clientStore.getAt(i);
    		if (record.data.confidentiality == null )
				delete record.data.confidentiality;
    		if (record.data.score == null )
				delete record.data.score;
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}
    		if (this.modifiedIds.indexOf(record.data.id) != -1)
    			records.push(record.data);	
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Clients to save');
    		return;
		}else{
			for ( var i = 0; i < records.length; i++) {
				var record = records[i];
	    		if (record.name == null || record.name == ""){
	    			Ext.Msg.alert('Error','Client/Vendor name should not be blank');
	    			return false;
	    		}else if (record.type == null || record.type == "") {
	    			Ext.Msg.alert('Error','Please select the type');
	    			return false;
				}
			}
	    	records = Ext.JSON.encode(records);
	    	app.clientService.saveClients(records,this.onSave,this);
		}
	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error',data.errorMessage);
		this.modifiedIds =  new Array();
		app.clientMgmtPanel.clientSearchPanel.search();
	},
	
	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Client to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Client","Do you delete selected Client?", this.deleteClient, this);
		}
	},
	
	deleteClient: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.clientService.deleteClient(selectedRecord.data.id, this.onDeleteClient, this);
		}
	},
	
	onDeleteClient : function(data){
		if (!data.success) {
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.clientMgmtPanel.clientSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');

			var params = new Array();
	    	params.push(['type','=', 'Client']);
	    	filter = getFilter(params);
	    	filter.pageSize=1000;
	    	ds.clientSearchStore.loadByCriteria(filter);

	    	params = new Array();
	    	params.push(['type','=', 'Vendor']);
	    	filter = getFilter(params);
	    	filter.pageSize=1000;
	    	ds.vendorStore.loadByCriteria(filter);
	    	
	    	params = new Array();
	    	var filter = getFilter(params);
	    	filter.pageSize=1000;
			ds.client_vendorStore.loadByCriteria(filter);

		}	
	},
	
});

function clientRender(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null || value=="") 
		return 'N/A';
    return value;
}

function dateRender(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + Ext.Date.format(value,'m/d/Y') + '"';
    return Ext.Date.format(value,'m/d/Y');
}

function numberRender(value, metadata, record, rowIndex, colIndex, store) {
	if (value != null) {
		metadata.tdAttr = 'data-qtip="' + value + '"';
		return value;
	}
	return 'N/A';
}
