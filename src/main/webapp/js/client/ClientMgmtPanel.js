Ext.define('tz.ui.ClientMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.clientGridPanel = new tz.ui.ClientGridPanel({manager:me});
    	me.clientSearchPanel = new tz.ui.ClientSearchPanel({manager:me});
    	me.clientPanel = new tz.ui.ClientPanel({manager:me});
    	
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [ this.clientSearchPanel,
 				        {
		   	        		height: 10,
		   	        		border : false
 				        },
 				        this.clientGridPanel
 				        ]
            },{
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [this.clientPanel]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen : function(){
		this.getLayout().setActiveItem(0);

		var browserHeight = app.mainViewport.height;
    	var searchPanelHt = Ext.getCmp('clientSearchPanel').getHeight();
		var headerHt = app.mainViewport.items.items[0].getHeight();
    	Ext.getCmp('clientGridPanel').setHeight(browserHeight - searchPanelHt - headerHt - 40);

    	ds.clientTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Client Type'])));
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
		ds.client_vendorStore.loadByCriteria(filter);
    	ds.contractorsStore.loadByCriteria();
    	ds.skillsetGroupStore.loadByCriteria();
    	
		if (ds.clientStore.getCount() ==0) 
    		this.clientSearchPanel.search();	

		var columns = this.clientGridPanel.getView().getGridColumns();
		if (app.userRole == 'ADMIN'){
			for ( var i = 0; i < columns.length ; i++) {
				if(columns[i].dataIndex == "confidentiality"){
					columns[i].hidden = false;
					break;
				}
			}
		}else{
			for ( var i = 0; i < columns.length ; i++) {
				if(columns[i].dataIndex == "confidentiality"){
					columns[i].setText('');
					columns[i].hidden = true;
					break;
				}
			}
		}
		this.clientGridPanel.getView().refresh();

	},
	
	showClient : function(rowIndex) {
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var record = ds.clientStore.getAt(rowIndex);
		}
		this.clientPanel.loadForm(record);
	} 
	
	
});
