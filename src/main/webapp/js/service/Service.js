Ext.define('tz.service.Service', {
    constructor: function(name) {
    	//console.log('Service const');
    },

	onAjaxResponse: function(response, args, cb, scope) {
		app.loadMask.hide();
		app.saveMask.hide();
		var data = new Object(); 
		if(response && response.responseText){
			data = eval("(" + response.responseText + ")");
		}else{
			data.errorMessage = "Failed to Connect to the server.";
		}
        cb.call(scope || window, data);
    },
    
	onAjaxRawResponse: function(response, args, cb, scope) {
		app.loadMask.hide();
		app.saveMask.hide();
		var data = new Object(); 
		if(response && response.responseText){
			data = response.responseText;
		}else{
			data.errorMessage = "Failed to Connect to the server.";
		}
        cb.call(scope || window, data);
    }
});
