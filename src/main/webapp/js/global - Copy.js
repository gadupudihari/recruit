Ext.define('tz.ui.ExpeseCombo', {
	extend : 'Ext.form.ComboBox',
	queryMode : 'local',
	triggerAction : 'all',
	displayField : 'name',
	valueField : 'value',
	forceSelection : true,
	matchFieldWidth : true,
});


Ext.define('tz.ui.EmployeeRateCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel : 'Project',
	queryMode : 'local',
	triggerAction : 'all',
	displayField : 'label',
	valueField : 'value',
	name : 'employeeBillingRatesId',
	forceSelection : true,
	allowBlank : false,
	matchFieldWidth : false,
	listConfig : {
		width : 200
	}
});

Ext.define('tz.ui.WithHoldingCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel : 'WH State',
	queryMode : 'local',
	triggerAction : 'all',
	displayField : 'withHolding',
	valueField : 'withHolding',
	name : 'withHolding'
});


Ext.define('tz.ui.EmployeeCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Employee',
    queryMode: 'local',
    displayField: 'fullName',
    valueField: 'id',
    name : 'employeeId',
    forceSelection:true,
    allowBlank:false,
	typeAhead: true,
	triggerAction: 'all',
	emptyText:'Select...',
	queryDelay:100,
	anyMatch:true,
	listConfig:{width:600},
	typeAheadDelay:30000
});

Ext.define('tz.ui.ProjectCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Project',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'name',
    name : 'projectName',
	typeAhead: true,
	triggerAction: 'all',
	emptyText:'Select...',
	queryDelay:100,
	anyMatch:true,
	listConfig:{width:600},
	typeAheadDelay:30000
});

Ext.define('tz.ui.AuthorityCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Authority',
    queryMode: 'local',
    displayField: 'authority',
    valueField: 'authority',
    name : 'authority',
	typeAhead: true,
	triggerAction: 'all',
	emptyText:'Select...',
	queryDelay:100,
	anyMatch:true,
	listConfig:{width:600},
	typeAheadDelay:30000
});

Ext.define('tz.ui.TNEStatusCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Status',
    queryMode: 'local',
    labelAlign: 'right',
    displayField: 'value',
    valueField: 'name',
    name : 'status'
});


Ext.define('tz.ui.WeekCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Week',
    queryMode: 'local',
    displayField: 'weekEnding',
    valueField: 'weekEnding',
    name : 'status',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.PaymentGeneratedCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Payment Generated',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    name : 'paymentGenerated',
    forceSelection:true,
    allowBlank:false,
    value: false,
    disabled: true
});

Ext.define('tz.ui.InvoiceGeneratedCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Invoice Generated',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    name : 'invoiceGenerated',
    forceSelection:true,
    allowBlank:false,
    value: false,
    disabled: true
});

Ext.define('tz.ui.ExpenseTypeCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Expense Type',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    name : 'type',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.SortingCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Sort By',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    name : 'sortingCombo',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.TimesheetTypeCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Timesheet Type',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    name : 'type',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.PeriodCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Choose Period',
    queryMode: 'local',
    displayField: 'label',
    valueField: 'value',
    name : 'period',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.LayoutCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Choose Layout',
    queryMode: 'local',
    displayField: 'label',
    valueField: 'value',
    name : 'layout',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.MonthCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Month',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    name : 'monthType',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.ActiveCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Active',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    name : 'status',
    value : true
});


Ext.define('tz.ui.EmpTypeCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Type of Employee',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    value:'EMPLOYEE',
    name : 'empType'
});

Ext.define('tz.ui.ImmigrationCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Immigration Status',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    name : 'immistatus'
});

Ext.define('tz.ui.DepositStatusCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Deposit Status',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    name : 'depositStatus'
});

Ext.define('tz.ui.DepositTypeCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Deposit Type',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    name : 'depositType'
});Ext.define('tz.ui.BaseStore', {
	extend : 'Ext.data.Store',
	proxy: {
        type: 'ajax',
        reader: {
        	type: 'json',
            root: 'rows',
            url : this.url,
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    loadByCriteria: function(criteria){
		this.load({
		    params:{
		        start:0,
		        limit: config.pageSize,
		        json : Ext.JSON.encode(criteria)
		    }
		});
	},
	initComponent: function(){
		this.callParent(arguments);
	}
});



Ext.define('tz.ui.PagingToolbar', {
	extend : 'Ext.PagingToolbar',
    displayInfo: true,
    displayMsg: 'Displaying topics {0} - {1} of {2}',
    emptyMsg: "No topics to display"
});
/**
*
*  Base64 encode / decode
*  http://www.webtoolkit.info/
*
**/


var Base64 = (function () {


    // private property
    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";


    // private method for UTF-8 encoding
    function utf8Encode(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    }


    // public method for encoding
    return {
        encode: (typeof btoa == 'function') ? function (input) { return btoa(input); } : function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = utf8Encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                keyStr.charAt(enc3) + keyStr.charAt(enc4);
            }
            return output;
        }
    };
})();


Ext.LinkButton = Ext.extend(Ext.Button, {
    template: new Ext.Template(
        '<table border="0" cellpadding="0" cellspacing="0" class="x-btn-wrap"><tbody><tr>',
        '<td class="x-btn-left"><i>  </i></td><td class="x-btn-center"><a  class="x-btn-text" href="{1}"  target="{2}">{0}</a></td><td  class="x-btn-right"><i> </i></td>',
        "</tr></tbody></table>"),


    onRender: function (ct, position) {
        var btn, targs = [this.text || ' ', this.href, this.target || "_self"];
        if (position) {
            btn = this.template.insertBefore(position, targs, true);
        } else {
            btn = this.template.append(ct, targs, true);
        }
        var btnEl = btn.child("a:first");
        btnEl.on('focus', this.onFocus, this);
        btnEl.on('blur', this.onBlur, this);


        this.initButtonEl(btn, btnEl);
        Ext.ButtonToggleMgr.register(this);
    },


    onClick: function (e) {
        if (e.button != 0) {
            return;
        }
        if (!this.disabled) {
            this.fireEvent("click", this, e);
            if (this.handler) {
                this.handler.call(this.scope || this, this, e);
            }
        }
    }


});


Ext.override(Ext.grid.Panel, {
    getExcelXml: function (includeHidden) {
        var worksheet = this.createWorksheet(includeHidden);
        var totalWidth = this.columns[1].getFullWidth();
        //var totalWidth = this.getColumnModel().getTotalWidth(includeHidden);
        return '<xml version="1.0" encoding="utf-8">' +
            '<ss:Workbook  xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"  xmlns:x="urn:schemas-microsoft-com:office:excel"  xmlns:o="urn:schemas-microsoft-com:office:office">' +
            '<o:DocumentProperties><o:Title>' + this.title + '</o:Title></o:DocumentProperties>' +
            '<ss:ExcelWorkbook>' +
                '<ss:WindowHeight>' + worksheet.height + '</ss:WindowHeight>' +
                '<ss:WindowWidth>' + worksheet.width + '</ss:WindowWidth>' +
                '<ss:ProtectStructure>False</ss:ProtectStructure>' +
                '<ss:ProtectWindows>False</ss:ProtectWindows>' +
            '</ss:ExcelWorkbook>' +
            '<ss:Styles>' +
                '<ss:Style ss:ID="Default">' +
                    '<ss:Alignment ss:Vertical="Top" ss:WrapText="1" />' +
                    '<ss:Font ss:FontName="arial" ss:Size="10" />' +
                    '<ss:Borders>' +
                        '<ss:Border ss:Color="#e4e4e4" ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Top" />' +
                        '<ss:Border ss:Color="#e4e4e4" ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Bottom" />' +
                        '<ss:Border ss:Color="#e4e4e4" ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Left" />' +
                        '<ss:Border ss:Color="#e4e4e4" ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Right" />' +
                    '</ss:Borders>' +
                    '<ss:Interior />' +
                    '<ss:NumberFormat />' +
                    '<ss:Protection />' +
                '</ss:Style>' +
                '<ss:Style ss:ID="title">' +
                    '<ss:Borders />' +
                    '<ss:Font />' +
                    '<ss:Alignment ss:WrapText="1" ss:Vertical="Center" ss:Horizontal="Center" />' +
                    '<ss:NumberFormat ss:Format="@" />' +
                '</ss:Style>' +
                '<ss:Style ss:ID="headercell">' +
                    '<ss:Font ss:Bold="1" ss:Size="10" />' +
                    '<ss:Alignment ss:WrapText="1" ss:Horizontal="Center" />' +
                    '<ss:Interior ss:Pattern="Solid" ss:Color="#A3C9F1" />' +
                '</ss:Style>' +
                '<ss:Style ss:ID="even">' +
                    '<ss:Interior ss:Pattern="Solid" ss:Color="#FFFFFF" />' +
                '</ss:Style>' +
                '<ss:Style ss:Parent="even" ss:ID="evendate">' +
                    '<ss:NumberFormat ss:Format="[ENG][$-409]dd\-mmm\-yyyy;@" />' +
                '</ss:Style>' +
                '<ss:Style ss:Parent="even" ss:ID="evenint">' +
                    '<ss:NumberFormat ss:Format="0" />' +
                '</ss:Style>' +
                '<ss:Style ss:Parent="even" ss:ID="evenfloat">' +
                    '<ss:NumberFormat ss:Format="0.00" />' +
                '</ss:Style>' +
                '<ss:Style ss:ID="odd">' +
                    '<ss:Interior ss:Pattern="Solid" ss:Color="#FFFFFF" />' +
                '</ss:Style>' +
                '<ss:Style ss:Parent="odd" ss:ID="odddate">' +
                    '<ss:NumberFormat ss:Format="[ENG][$-409]dd\-mmm\-yyyy;@" />' +
                '</ss:Style>' +
                '<ss:Style ss:Parent="odd" ss:ID="oddint">' +
                    '<ss:NumberFormat ss:Format="0" />' +
                '</ss:Style>' +
                '<ss:Style ss:Parent="odd" ss:ID="oddfloat">' +
                    '<ss:NumberFormat ss:Format="0.00" />' +
                '</ss:Style>' +
                '<ss:Style ss:ID="columnheaders">'+
                	'<ss:Font ss:Size="12" ss:Bold="1"/>'+
            	'</ss:Style>'+
            '</ss:Styles>' +
            worksheet.xml +
            '</ss:Workbook>';
    },


    createWorksheet: function (includeHidden) {


        //      Calculate cell data types and extra class names which affect formatting
        var cellType = [];
        var cellTypeClass = [];
        var cm = this.columns;
        var totalWidthInPixels = 0;
        var colXml = '';
        var headerXml = '';
        for (var i = 0; i < cm.length ; i++) {
        	if(cm[i].xtype == 'actioncolumn' || cm[i].text == "&#160;"){}
            else if (includeHidden || !cm[i].isHidden() || cm[i].isHidden() ) {
                var w = cm[i].width;
                totalWidthInPixels += w;
                colXml += '<ss:Column ss:AutoFitWidth="1" ss:Width="' + w + '" />';
                headerXml += '<ss:Cell ss:StyleID="headercell">' +
                    '<ss:Data ss:Type="String">' + cm[i].text + '</ss:Data>' +
                    '<ss:NamedCell ss:Name="Print_Titles" /></ss:Cell>';
                var fld = this.store.model.prototype.fields.get(cm[i].dataIndex);
                if (cm[i].dataIndex == "paymentTerms" || cm[i].dataIndex == "invoicingTerms") {
                	fld.type.type = 'auto';
				}
                if(fld!=undefined)
                switch (fld.type.type) {
                    case "int":
                        cellType.push("Number");
                        cellTypeClass.push("int");
                        break;
                    case "float":
                        cellType.push("Number");
                        cellTypeClass.push("float");
                        break;
                    case "bool":
                    case "boolean":
                        cellType.push("String");
                        cellTypeClass.push("");
                        break;
                    /*case "date":
                        cellType.push("DateTime");
                        cellTypeClass.push("date");
                        break;*/
                    default:
                        cellType.push("String");
                        cellTypeClass.push("");
                        break;
                }
            }
        }
        var visibleColumnCount = cellType.length;

        var result = {
            height: 9000,
            width: Math.floor(totalWidthInPixels * 30) + 50
        };


        //      Generate worksheet header details.
        var t = '<ss:Worksheet ss:Name="' + this.title +  '"><ss:Names><ss:NamedRange ss:Name="Print_Titles"  ss:RefersTo="=\'' + this.title + '\'!R1:R2"  /></ss:Names><ss:Table x:FullRows="1" x:FullColumns="1"  ss:ExpandedColumnCount="' + visibleColumnCount + '"  ss:ExpandedRowCount="' + (this.store.getCount() + 10) + '">' + colXml +  '<ss:Row ss:Height="38"><ss:Cell ss:StyleID="title"  ss:MergeAcross="' + (visibleColumnCount - 1) + '"><ss:Data  xmlns:html="http://www.w3.org/TR/REC-html40"  ss:Type="String"><html:B><html:U><html:Font  html:Size="15">' + this.title +  '</html:Font></html:U></html:B></ss:Data><ss:NamedCell  ss:Name="Print_Titles" /></ss:Cell></ss:Row><ss:Row  ss:AutoFitHeight="1">' + headerXml + '</ss:Row>';

        var summary = 0;
        //      Generate the data rows from the data in the Store
        for (var i = 0, it = this.store.data.items, l = it.length; i < l; i++) {
        	for ( var int = 0; int < this.features.length; int++) {
				if (this.features[int].id == 'groupSummary') {
					
					if (i==0) {
						t += '<ss:Row ss:Height="20">';
						t += '<ss:Cell ss:StyleID="columnheaders">' +
						'<ss:Data ss:Type="String"><Font/><B/>' + this.features[int].summaryGroups[summary].name+' ('+this.store.count(this.store.groupField)[it[i].data[this.store.groupField]] + ') </ss:Data>' +
	                    '<ss:NamedCell ss:Name="Print_Titles" /></ss:Cell>';
						t += '</ss:Row>';
						summary++;
					}else if (it[i].data[this.store.groupField] != it[i-1].data[this.store.groupField]) {
						t += '<ss:Row ss:Height="20">';
						t += '<ss:Cell ss:StyleID="columnheaders">' +
						'<ss:Data ss:Type="String"><Font/><B/>' + this.features[int].summaryGroups[summary].name+' ('+this.store.count(this.store.groupField)[it[i].data[this.store.groupField]] + ') </ss:Data>' +
	                    '<ss:NamedCell ss:Name="Print_Titles" /></ss:Cell>';
						t += '</ss:Row>';
						summary++;
					}
				}
			}
        	
        	
            t += '<ss:Row>';
            var cellClass = (i & 1) ? 'odd' : 'even';
            r = it[i].data;
            var k = 0;
            for (var j = 0; j < cm.length ; j++) {
            	if (cm[j].xtype == 'actioncolumn' || cm[j].text == "&#160;") {}
            	else if ( includeHidden || !cm[j].isHidden() || cm[j].isHidden()) {
                    var v = r[cm[j].dataIndex];
                    t += '<ss:Cell ss:StyleID="' + cellClass +  cellTypeClass[k] + '"><ss:Data ss:Type="' + cellType[k] + '">';
                    if (v !=null) {
	                    if (cm[j].dataIndex == "monthForPayRoll" || cm[j].dataIndex == "monthForExpense") {
	                    	var monthNames = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
	                    	t += monthNames[v.getMonth()] +'-'+v.getFullYear();
						}else if (cm[j].xtype != undefined && cm[j].xtype.search('date') != -1) {
	                        t += v.getMonth()+1 +'/'+v.getDate()+'/'+v.getFullYear();
	                    }else if (cm[j].dataIndex == "paymentTerms" || cm[j].dataIndex == "invoicingTerms") {
							t += cm[j].renderer(r[cm[j].dataIndex]);
						}else {
	                        t += v;
	                    }
                    }else
                    	t += ' ';
                    t += '</ss:Data></ss:Cell>';
                    k++;
                }
            }
            t += '</ss:Row>';
        }


        result.xml = t + '</ss:Table>' +
            '<x:WorksheetOptions>' +
                '<x:PageSetup>' +
                    '<x:Layout x:CenterHorizontal="1" x:Orientation="Landscape" />' +
                    '<x:Footer x:Data="Page &amp;P of &amp;N" x:Margin="0.5" />' +
                    '<x:PageMargins x:Top="0.5" x:Right="0.5" x:Left="0.5" x:Bottom="0.8" />' +
                '</x:PageSetup>' +
                '<x:FitToPage />' +
                '<x:Print>' +
                    '<x:PrintErrors>Blank</x:PrintErrors>' +
                    '<x:FitWidth>1</x:FitWidth>' +
                    '<x:FitHeight>32767</x:FitHeight>' +
                    '<x:ValidPrinterInfo />' +
                    '<x:VerticalResolution>600</x:VerticalResolution>' +
                '</x:Print>' +
                '<x:Selected />' +
                '<x:DoNotDisplayGridlines />' +
                '<x:ProtectObjects>False</x:ProtectObjects>' +
                '<x:ProtectScenarios>False</x:ProtectScenarios>' +
            '</x:WorksheetOptions>' +
        '</ss:Worksheet>';
        return result;
    }
}); Ext.override(Ext.form.ComboBox, {
 	doQuery: function(queryString, forceAll) {
        queryString = queryString || '';

        // store in object and pass by reference in 'beforequery'
        // so that client code can modify values.
        var me = this,
            qe = {
                query: queryString,
                forceAll: forceAll,
                combo: me,
                cancel: false
            },
            store = me.store,
            isLocalMode = me.queryMode === 'local';

        if (me.fireEvent('beforequery', qe) === false || qe.cancel) {
            return false;
        }

        // get back out possibly modified values
        queryString = qe.query;
        forceAll = qe.forceAll;

        // query permitted to run
        if (forceAll || (queryString.length >= me.minChars)) {
            // expand before starting query so LoadMask can position itself correctly
            me.expand();

            // make sure they aren't querying the same thing
            if (!me.queryCaching || me.lastQuery !== queryString) {
                me.lastQuery = queryString;

                if (isLocalMode) {
                    // forceAll means no filtering - show whole dataset.
                    if (forceAll) {
                        store.clearFilter();
                    } else {
                        // Clear filter, but supress event so that the BoundList is not immediately updated.
                        store.clearFilter(true);
                        
                        me.anyMatch = me.anyMatch === undefined? false : me.anyMatch;
                        me.caseSensitive = me.caseSensitive === undefined? false : me.caseSensitive;
                        
                        store.filter({
                            property: me.displayField,
                            value   : queryString,
                            anyMatch: me.anyMatch,
                            caseSensitive:me.caseSensitive
                            
                        });
                    }
                } else {
                    // In queryMode: 'remote', we assume Store filters are added by the developer as remote filters,
                    // and these are automatically passed as params with every load call, so we do *not* call clearFilter.
                    store.load({
                        params: me.getParams(queryString)
                    });
                }
            }

            // Clear current selection if it does not match the current value in the field
            if (me.getRawValue() !== me.getDisplayValue()) {
                me.ignoreSelection++;
                me.picker.getSelectionModel().deselectAll();
                me.ignoreSelection--;
            }

            if (isLocalMode) {
                me.doAutoSelect();
            }
            if (me.typeAhead) {
                me.doTypeAhead();
            }
        }
        return true;
    }
 
});
function isThere(obj, attr){
	if(!obj[attr] || obj[attr] ==''){
		delete obj[attr];
	}
}

function fixConsole(alertFallback)
{    
    if (typeof console === "undefined")
    {
        console = {}; // define it if it doesn't exist already
    }
    if (typeof console.log === "undefined") 
    {
        if (alertFallback) { console.log = function(msg) { alert(msg); }; } 
        else { console.log = function() {}; }
    }
    if (typeof console.dir === "undefined") 
    {
        if (alertFallback) 
        { 
            // THIS COULD BE IMPROVEDů maybe list all the object properties?
            console.dir = function(obj) { alert("DIR: "+obj); }; 
        }
        else { console.dir = function() {}; }
    }
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };

fixConsole();


function getFilter(params){
	var pArray = new Array();
	for(var i=0; i< params.length; i++){
		var p = params[i];
		if(p[2] && p[2] != ''){
			pArray.push(new Object({
					id : p[0],
					operand : p[1],
					strValue : p[2]
				}
			));
		}
	}

	var filter = {
			pageNo : 0,
			pageSize : 50,
			params : pArray
	};
	
	return filter;

}

function formatDate(d) {
	if(d){
		return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
	}
}Ext.define('tz.ui.BaseFormPanel', {
	extend : 'Ext.form.Panel',
	initComponent : function() {
		this.callParent(arguments);
	},

	clearMessage : function(msg) {
		if (this.message.el) {
			this.message.el.dom.innerHTML = '';
		}
	},
	
	updateMessage : function(msg) {
		if (this.message.el) {
			this.message.el.dom.innerHTML = '<div class="msgPanel"><p ><img src="images/icon_checkmark_dark.gif"/>&nbsp;&nbsp;' + msg + '</p></div>';
		}
	},
	
	updateInfo : function(msg) {
		if (this.message.el) {
			this.message.el.dom.innerHTML = '<div class="infoMsgPanel"><p ><img src="images/icon_info.gif"/>&nbsp;&nbsp;' + msg + '</p></div>';
		}
	},

	updateError : function(msg) {
		if (this.message.el) {
			this.message.el.dom.innerHTML = '<div class="errorMsgPanel"><p ><img src="images/icon_error.gif"/>&nbsp;&nbsp;' + msg + '</p></div>';
		}
	},
	
	getMessageComp : function(msg) {
		if (!msg) {
			msg = '';
		}
		this.message = new Ext.Component({
			xtype : 'component',
			html : ''
		});
		return this.message;
	},

	updateHeading : function(msg) {
		if (this.heading.el) {
			this.heading.el.dom.innerHTML = '<div class="subHeading">' + msg + '</div><br/>';
		}
	},

	getHeadingComp : function(msg) {
		if (!msg) {
			msg = '';
		}
		this.heading = new Ext.Component({
			xtype : 'component',
			html : ''
		});
		return this.heading;
	}

});



//App Config
appConfig ={
	appWidth : 1200
};

//TODO: Need to move to a base class
function loadByCriteria(criteria){
	this.load({
	    params:{
	        start:0,
	        limit: config.pageSize,
	        json : Ext.JSON.encode(criteria)
	    }
	});
}
var reader = {type: 'json',root: 'rows',totalProperty: 'size'};
//var reader1 = {type: 'json',root: 'rows',totalProperty: 'size'};

//Sub_Vendor data model
Ext.define('tz.model.Sub_Vendor', {
    extend: 'Ext.data.Model',
    fields : [ 'id','requirementId','vendorId','contactId'
            ]
});

Ext.define('tz.store.Sub_VendorStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Sub_Vendor',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getSubVendors?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'date',direction: 'DESC'}],
});

//Research data model
Ext.define('tz.model.Sub_Requirement', {
    extend: 'Ext.data.Model',
    fields : [ 'id','requirementId','candidateId','vendorId',
               /*{name: 'requirementId' , type:'int'},{name: 'candidateId' , type:'int'},
               {name: 'vendorId' , type:'int'},{name: 'contactId' , type:'int'},*/
               	{name: 'submittedDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.Sub_RequirementStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Sub_Requirement',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getSubRequirements?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'date',direction: 'DESC'}],
});

//Research data model
Ext.define('tz.model.Research', {
    extend: 'Ext.data.Model',
    fields : [ 'id',
               	{name: 'task' , type:'string'},{name: 'status' , type:'string'},
               	{name: 'comments' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},
               	{name: 'potentialCandidates' , type:'string'},
               	{name: 'date', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               	{name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.ResearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Research',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getTasks?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'date',direction: 'DESC'}],
});

//Interview data model
Ext.define('tz.model.Interview', {
    extend: 'Ext.data.Model',
    fields : [ 'id','clientId','vendorId','candidateId','contactId','interviewDate','time',
               	{name: 'interviewer' , type:'string'},
    			{name: 'comments' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},
                {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.InterviewStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Interview',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/interview/getInterviews?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'interviewDate',direction: 'DESC'}],
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			msg = "Displaying "+s.data.items.length +" Records";
			if (app.interviewMgmtPanel != null)
				app.interviewMgmtPanel.interviewGridPanel.showCount.setValue(msg);
		}
	} 
});

//contacts data model
Ext.define('tz.model.Contact', {
    extend: 'Ext.data.Model',
    fields : [ 'id','clientId',{name: 'firstName' , type:'string'},{name: 'lastName' , type:'string'},
               	{name: 'email' , type:'string'},{name: 'jobTitle' , type:'string'},{name: 'clientName' , type:'string'},
               	{name: 'workPhone' , type:'string'},{name: 'internetLink' , type:'string'},{name: 'score',type:'int'},
               	{name: 'cellPhone' , type:'string'},{name: 'type' , type:'string'},{name: 'extension' , type:'string'},
    			{name: 'comments' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},{name: 'interviews' , type:'int'},
                {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.ContactStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Contact',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/client/getContacts?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'firstName',direction: 'ASC'}],
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			for ( var i=0; i<s.data.items.length; i++) {
				var contact = s.data.items[i].data;
				contact.fullName = contact.firstName+' '+contact.lastName;
			}
			msg = "Displaying "+s.data.items.length +" Records";
			if (app.contactMgmtPanel != null)
				app.contactMgmtPanel.contactGridPanel.showCount.setValue(msg);
		}
	} 
});

Ext.define('tz.store.ContactSearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Contact',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/client/getContacts?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    sorters: [{property: 'firstName',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			for ( var i=0; i<s.data.items.length; i++) {
				var contact = s.data.items[i].data;
				contact.fullName = contact.firstName+' '+contact.lastName;
			}
		}
	} 
});

/*** IpLogger DataModel ***/
Ext.define('tz.model.IpLogger', {
    extend: 'Ext.data.Model',
    fields : [ {name: 'user',type:'string'},
               {name: 'ipAddress',type:'string'},
               {name: 'city',type:'string'},
               {name: 'state',type:'string'},
               {name: 'country',type:'string'},{name: 'time',type:'string'},
               {name: 'date', type: 'date', dateFormat: 'Y-m-d H:i:s.u'}]
});

//Create the data store for the IpLogger
Ext.define('tz.store.IpLoggerStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.IpLogger',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/loadIpLogger?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },
    sorters: [{property: 'date',direction: 'DESC'}],
    loadByCriteria: loadByCriteria
});

Ext.define('tz.model.Client', {
    extend: 'Ext.data.Model',
    fields : [ 'id',{name: 'name' , type:'string'},{name: 'contactNumber' , type:'string'},{name: 'score',type:'int'},
    			{name: 'fax' , type:'string'},{name: 'website' , type:'string'},{name: 'email' , type:'string'},
    			{name: 'industry' , type:'string'},{name: 'about' , type:'string'},
    			{name: 'type' , type:'string'},{name: 'address' , type:'string'},
    			{name: 'comments' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},
    			{name: 'contractorId' , type:'string'},{name: 'fullTimeId' , type:'string'},
    			{name: 'cityAndState', type: 'string'},
                {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.ClientStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Client',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/client/getClients?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			msg = "Displaying "+s.data.items.length +" Records";
			if (app.clientMgmtPanel != null)
				app.clientMgmtPanel.clientGridPanel.showCount.setValue(msg);
		}
	} 
});

Ext.define('tz.store.ClientSearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Client',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/client/getClients?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    sorters: [{property: 'name',direction: 'ASC'}],
});

Ext.define('tz.model.Candidate', {
    extend: 'Ext.data.Model',
    fields : [ 'id',{name: 'priority',type:'int'},{name: 'firstName' , type:'string'},
               {name: 'lastName' , type:'string'},{name: 'emailId' , type:'string'},
               {name: 'contactNumber' , type:'string'},{name: 'contactAddress' , type:'string'},
               {name: 'currentJobTitle' , type:'string'},{name: 'source' , type:'string'},{name: 'type' , type:'string'},
               {name: 'comments' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},
               {name: 'employer' , type:'string'},{name: 'skillSet' , type:'string'},{name: 'highestQualification' , type:'string'},
               {name: 'referral' , type:'string'},{name: 'marketingStatus' , type:'string'},{name: 'followUp', type: 'string'},
               {name: 'relocate', type: 'string'},{name: 'travel', type: 'string'},{name: 'cityAndState', type: 'string'},
               {name: 'education', type: 'string'},{name: 'immigrationStatus', type: 'string'},
               {name: 'openToCTH', type: 'string'},{name: 'employmentType', type: 'string'},{name: 'expectedRate', type: 'string'},
               {name: 'aboutPartner', type: 'string'},{name: 'personality', type: 'string'},
               {name: 'availability', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'startDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'submittedDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
               {name: 'req_submittedDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.CandidateStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Candidate',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/candidate/getCandidates?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			for ( var i=0; i<s.data.items.length; i++) {
				var candidate = s.data.items[i].data;
				candidate.fullName = candidate.firstName+' '+candidate.lastName;
			}
			msg = "Displaying "+s.data.items.length +" Records";
			if (app.candidateMgmtPanel != null)
				app.candidateMgmtPanel.candidatesGridPanel.showCount.setValue(msg);
		},
	} 
});

Ext.define('tz.store.CandidateSearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Candidate',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/candidate/getAllCandidates?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    sorter : 'firstName',
    sorters: [{property: 'firstName',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			for ( var i=0; i<s.data.items.length; i++) {
				var candidate = s.data.items[i].data;
				candidate.fullName = candidate.firstName+' '+candidate.lastName;
			}
		},
	} 
});

Ext.define('tz.model.Requirement', {
    extend: 'Ext.data.Model',
    fields : [ 'id','candidateId','vendorId','contactId',
               	{name: 'submittedDate' , type:'string'},
               	{name: 'hotness' , type:'string'},
               	{name: 'vendor' , type:'string'},{name: 'client' , type:'string'},
    			{name: 'location' , type:'string'},{name: 'requirement' , type:'string'},
    			{name: 'module' , type:'string'},{name: 'contactPerson' , type:'string'},
    			{name: 'phoneNo' , type:'string'},{name: 'researchComments' , type:'string'},
    			{name: 'addInfo_Todos' , type:'string'},{name: 'source' , type:'string'},
    			{name: 'rateSubmitted' , type:'string'},{name: 'postingTitle' , type:'string'},
    			{name: 'jobOpeningStatus' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},
    			{name: 'comments' , type:'string'},{name: 'employer' , type:'string'},{name: 'publicLink' , type:'string'},
    			{name: 'submissionDate' , type:'string'},
                {name: 'postingDate', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.RequirementStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Requirement',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/requirement/getRequirements?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			msg = "Displaying "+s.data.items.length +" Records";
			if (app.requirementMgmtPanel != null)
				app.requirementMgmtPanel.requirementsGrid.showCount.setValue(msg);
		},
	} 
});


Ext.define('tz.model.PlacementLeads', {
    extend: 'Ext.data.Model',
    fields : [ 'id',{name: 'priority',type:'int'},
               {name: 'person' , type:'string'},{name: 'sourceOfInformation' , type:'string'},
    			{name: 'typeOfInformation' , type:'string'},{name: 'information' , type:'string'},
    			{name: 'hospital_Entity' , type:'string'},{name: 'location' , type:'string'},
    			{name: 'link1' , type:'string'},{name: 'link2' , type:'string'},
    			{name: 'outreach' , type:'string'},{name: 'lastUpdatedUser' , type:'string'},{name: 'comments' , type:'string'},
    			{name: 'liveDate', type: 'string'},
    			{name: 'date', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'source_Sublevel', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'lastUpdated', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
            ]
});

Ext.define('tz.store.PlacementLeadsStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.PlacementLeads',	
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/placementLeads/getPlacementLeads?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },   
    sorters: [{property: 'date',direction: 'DESC'}],
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			msg = "Displaying "+s.data.items.length +" Records";
			if (app.placementMgmtPanel != null)
				app.placementMgmtPanel.placementsGrid.showCount.setValue(msg);
		}
	} 
});


/*** Settings DataModel ***/
Ext.define('tz.model.Settings', {
    extend: 'Ext.data.Model',
    fields : [ {name: 'id',type:'int'},
               {name: 'name',type:'string'},
               {name: 'value',type:'string'},
               {name: 'type',type:'string'},
               {name: 'notes',type:'string'},
               ]
});

//Create the data store for the Settings
Ext.define('tz.store.SettingsStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/search?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },
    loadByCriteria: loadByCriteria
});

//Create the data store for the Settings Grid
Ext.define('tz.store.SettingsSearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/settings/searchSettings?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
    },
    loadByCriteria: loadByCriteria
});

//Vendor Type combo in Vendors 
Ext.define('tz.store.ClientTypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
	loadAll : function(){
   		app.settingsService.getCategory('Client Type', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

//Submission Status in Recruiting 
Ext.define('tz.store.SubmissionStatusStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'value',direction: 'ASC'}],
	loadAll : function(){
   		app.settingsService.getCategory('Submission Status', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

//Marketing Status in Recruiting 
Ext.define('tz.store.MarketingStatusStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'value',direction: 'ASC'}],
	loadAll : function(){
   		app.settingsService.getCategory('Marketing Status', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

//Hotness in Recruiting 
Ext.define('tz.store.HotnessStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
	loadAll : function(){
   		app.settingsService.getCategory('Hotness', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

//Candidate Submission in Recruiting 
Ext.define('tz.store.CandidateSubmissionStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'value',direction: 'ASC'}],
	loadAll : function(){
   		app.settingsService.getCategory('Candidate Submission', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});


//Information Type in Recruiting 
Ext.define('tz.store.InformationTypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'value',direction: 'ASC'}],
	loadAll : function(){
   		app.settingsService.getCategory('Information Type', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

//Job opening Status in job openings 
Ext.define('tz.store.JobOpeningStatusStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
	loadAll : function(){
   		app.settingsService.getCategory('Job Opening Status', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

//Qualification in candidates
Ext.define('tz.store.QualificationStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
	loadAll : function(){
   		app.settingsService.getCategory('Qualification', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

// Candidate Source in candidates
Ext.define('tz.store.CandidateSourceStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,
	sorters: [{property: 'name',direction: 'ASC'}],
	loadAll : function(){
   		app.settingsService.getCategory('Candidate Source', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

//Immigration type combo in candidates 
Ext.define('tz.store.ImmigrationTypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
	loadAll : function(){
   		app.settingsService.getCategory('Immigration', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

Ext.define('tz.store.EmploymentTypeStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
	loadAll : function(){
   		app.settingsService.getCategory('Employment Type', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

//Employer combo 
Ext.define('tz.store.EmployerStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
	loadAll : function(){
   		app.settingsService.getCategory('Employer', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

Ext.define('tz.store.ResearchStatusStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.Settings',
	pageSize: 200,    
	loadAll : function(){
   		app.settingsService.getCategory('Research Status', this.onLoadAll, this);
    },
    onLoadAll: function(data){
		if (data.success  && data.returnVal.rows) {
			this.loadData(data.returnVal.rows);
		} else {
			console.log('Unable to load the data');
		}
    }
});

Ext.define('tz.model.User', {
    extend: 'Ext.data.Model',
    fields : [ {name: 'id', type: 'int'},'firstName', 'lastName','active','emailId','alternateEmailId','loginId','password','authority' ]
});

Ext.define('tz.store.UserStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.User',
	//buffered: true,
	pageSize: 200,
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/user/getUsers?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    sorters: [{property: 'firstName',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			msg = "Displaying "+s.data.items.length +" Records";
			if (app.userMgmtPanel != null)
				app.userMgmtPanel.userGridPanel.showCount.setValue(msg);
		}
	} 
});

Ext.define('tz.store.UserSearchStore', {
	extend : 'Ext.data.Store',
	model: 'tz.model.User',
	//buffered: true,
	pageSize: 200,
    proxy: {
        type: 'ajax',
        url: '/recruit/remote/user/list?',
        reader: {
        	type: 'json',
            root: 'rows',
            totalProperty: 'size'
        },
        simpleSortMode: true
    },
    sorters: [{property: 'firstName',direction: 'ASC'}],
    loadByCriteria: loadByCriteria,
    listeners : {
    	scope: this,
		datachanged: function(s, records){
			for ( var i=0; i<s.data.items.length; i++) {
				var user = s.data.items[i].data;
				user.fullName = user.firstName+' '+user.lastName;
			}
		}
	} 
});

/**
 * Master list of all the data stores here
 */
Ext.define('tz.DataStores', {
    constructor: function(name) {
    	
    	// Static Data Stores
    	//The data store containing the list of states
    	this.boolStore = Ext.create('Ext.data.Store', {
    	    fields: ['value', 'name'],
    	    data : [
    	        {"name":"YES", "value":true},
    	        {"name":"NO", "value":false}
    	    ]
    	});
    	
    	this.yesNoStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"name":"YES", "value":"YES"},
	    	        {"name":"NO", "value":"NO"},
	    	        {"name":"N/A", "value":"N/A"},
	    	    ]
        });

    	this.yesOrNoStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"name":"YES", "value":"YES"},
	    	        {"name":"NO", "value":"NO"},
	    	    ]
        });

    	this.monthStore = Ext.create('Ext.data.Store', {
    	    fields: ['value', 'name'],
    	    data : [
    	        {"name":"January", "value":"January"},    	        
    	        {"name":"February", "value":"February"},    	            	        
    	        {"name":"March", "value":"March"},
    	        {"name":"April", "value":"April"},
    	        {"name":"May", "value":"May"},    	            	        
    	        {"name":"June", "value":"June"},
    	        {"name":"July", "value":"July"},
    	        {"name":"August", "value":"August"},    	            	        
    	        {"name":"September", "value":"September"},
    	        {"name":"October", "value":"October"},
    	        {"name":"November", "value":"November"},    	            	        
    	        {"name":"December", "value":"December"}    	        
    	    ]
    	});

    	this.authorityStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data : [
	    	        {"name":"ADMIN", "value":"ADMIN"},
	    	        {"name":"RECRUITER", "value":"RECRUITER"}
	    	    ]
        });
    	
    	// Define the vendor store here
    	
    	this.userStore = new tz.store.UserStore();
    	this.userSearchStore = new tz.store.UserSearchStore();
    	this.settingsStore = new tz.store.SettingsStore(); 
    	this.settingsSearchStore = new tz.store.SettingsSearchStore();
    	this.candidateStore = new tz.store.CandidateStore();
    	this.requirementStore = new tz.store.RequirementStore();
    	this.placementLeadsStore = new tz.store.PlacementLeadsStore();
    	this.marketingStatusStore = new tz.store.MarketingStatusStore();
    	this.submissionStatusStore = new tz.store.SubmissionStatusStore();
    	this.hotnessStore = new tz.store.HotnessStore();
    	this.informationTypeStore = new tz.store.InformationTypeStore();
    	this.jobOpeningStatusStore = new tz.store.JobOpeningStatusStore();
    	this.clientTypeStore = new tz.store.ClientTypeStore();
    	this.clientStore = new tz.store.ClientStore();
    	this.vendorStore = new tz.store.ClientSearchStore();
    	this.ipLoggerStore = new tz.store.IpLoggerStore();
    	this.qualificationStore = new tz.store.QualificationStore();
    	this.candidateSourceStore = new tz.store.CandidateSourceStore();
    	this.contactStore = new tz.store.ContactStore();
    	this.contactSearchStore = new tz.store.ContactSearchStore();
    	this.clientSearchStore = new tz.store.ClientSearchStore();
    	this.candidateSearchStore = new tz.store.CandidateSearchStore();
    	this.interviewStore = new tz.store.InterviewStore();
    	this.immigrationTypeStore = new tz.store.ImmigrationTypeStore();
    	this.employmentTypeStore = new tz.store.EmploymentTypeStore();
    	this.req_candidateStore = new tz.store.CandidateStore();
    	this.candidate_requirementStore = new tz.store.RequirementStore();
    	this.employerStore = new tz.store.EmployerStore();
    	this.researchStore = new tz.store.ResearchStore();
    	this.researchStatusStore = new tz.store.ResearchStatusStore();
    	this.subRequirementStore = new tz.store.Sub_RequirementStore();
    	this.subVendorStore = new tz.store.Sub_VendorStore();
    }
});Ext.define('tz.service.Service', {
    constructor: function(name) {
    	//console.log('Service const');
    },

	onAjaxResponse: function(response, args, cb, scope) {
		app.loadMask.hide();
		app.saveMask.hide();
		var data = new Object(); 
		if(response && response.responseText){
			data = eval("(" + response.responseText + ")");
		}else{
			data.errorMessage = "Failed to Connect to the server.";
		}
        cb.call(scope || window, data);
    },
    
	onAjaxRawResponse: function(response, args, cb, scope) {
		app.loadMask.hide();
		app.saveMask.hide();
		var data = new Object(); 
		if(response && response.responseText){
			data = response.responseText;
		}else{
			data.errorMessage = "Failed to Connect to the server.";
		}
        cb.call(scope || window, data);
    }
});

Ext.define('tz.ui.UnderConstruction', {
	extend : 'Ext.form.Panel',
	margin : 2,
	layout: 'column',
	border : false
});
Ext.define('tz.service.EmployeeService', {
	extend : 'tz.service.Service',

	saveUser : function(user,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/user/saveUser',
			params : {
				json : user
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
	
    savePassword: function(employee,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/user/savePassword',
			params : {
				json : employee
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    resetPassword: function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/user/resetPassword/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    givingAcess: function(id, cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/user/givingAcess/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    revokeAccess: function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/user/revokeAcess/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    deleteUser: function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/user/delete/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

});

Ext.define('tz.ui.EmployeePasswordPanel', {
    extend: 'tz.ui.BaseFormPanel',
    //frame:true,
    bodyPadding: 10,
    title: 'Change Password',
    anchor:'10%',
    width:'10%',
    height :'10%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},    
    
    
    initComponent: function() {
        var me = this;
     
        me.items = [this.getMessageComp(),this.getHeadingComp(),
            {
        		xtype:'fieldset',
        		title: '',
  	        	collapsible: false,
  	        	layout: 'column',
  	        	items :[{
	   	             xtype: 'container',
	   	             columnWidth:.5,
                items: [{
		   	            	xtype:'textfield',
		  	                 inputType : 'password',
		  	                 fieldLabel: 'Current Password',
		  	                 name: 'oldPassword',
		  	                 allowBlank:false,
		  	                 tabIndex:1
		  	             },{
		   	                 xtype:'textfield',
		   	                 inputType : 'password',
		   	                 fieldLabel: 'New Password',
		   	                 name: 'newPassword',
		   	                 allowBlank:false,
		   	                 minLength : 6,
		   	                 maxLength : 25,
		   	                 tabIndex:2
		   	             },{
		   	            	xtype:'textfield',
		   	                 inputType : 'password',
		   	                 fieldLabel: 'Confirm Password',
		   	                 name: 'confirmPassword',
		   	                 minLength : 6,
		   	                 maxLength     : 25,
		   	                 allowBlank:false,
		   	                 tabIndex:3,
		   	                 validator: function() {
		   	                	 var pass1 = me.form.findField('newPassword').getValue();
		   	                	 var pass2 = me.form.findField('confirmPassword').getValue();

			                     if (pass1 == pass2)
			                    	 return true;
			                     else 
			                    	 return "Passwords do not match!";
			               }
		   	             },
                ]
  	        	}]
            }
            
        ];

        this.tbar =  [ {
            xtype: 'button',
            text: 'Save',
            iconCls : 'btn-save',
            tabIndex:4,
            scope: this,
            handler: this.savePassword
        }, '-', {
            xtype: 'button',
            text: 'Reset',
            iconCls : 'btn-refresh',
            tabIndex:5,
            scope: this,
            handler: this.reset
        } ];
        

        me.callParent(arguments);
    },
    
    reset: function(){
    	this.clearMessage();
    	this.form.findField('oldPassword').reset();
    	this.form.findField('newPassword').reset();
    	this.form.findField('confirmPassword').reset();  	
	},
	
	initScreen: function(){
		// events to register
        this.addEvents ({
            'iconlogoutclicked'      : true   // simple sample of a logout event
        });
	},
	
	savePassword: function(){
		this.clearMessage();
		var userFieldObj = {};
		var newPass = this.form.findField('newPassword').getValue();
		var confirmPass =this.form.findField('confirmPassword').getValue();
		var oldPass = this.form.findField('oldPassword').getValue();
		
		if(! this.form.isValid()){
				this.updateError('Required fields cannot be blank');
				return false;
		}
		if(oldPass == newPass ){
			this.updateError('Current Password and New Password should not be same');
					return false;
		}
		if(newPass.length<6 || confirmPass.length<6){
				this.updateError('Must have atleast 6 characters');
				return false;
		}
		
		if(newPass != confirmPass ){
			this.updateError('Passwords do not match');
					return false;
		}
		
		
		userFieldObj['password'] = newPass;
		userFieldObj['oldPassword'] = oldPass;
		userFieldObj = Ext.JSON.encode(userFieldObj);			
		var employee = userFieldObj ;
		app.employeeService.savePassword(employee, this.onSavePassword, this);
			
	},		
	
	onSavePassword: function(data){
		if(!data.success){
			this.updateError("The Current Password you gave is incorrect.");
			this.form.findField('oldPassword').setValue('');
		}else{
			//this.getForm().findField('password').setValue(data.returnVal.id);
			this.updateMessage('Password Changed Successfully');
	        this.accessLogout();
		}
	},
	
	 accessLogout : function ( ) {
	        // show dialog with confirmation
		 Ext.Msg.show({
	            title     : 'Logout',
	            msg       : 'Password Changed successfully, please login with your new password, Click Ok to Logout.',
	            buttons   : Ext.Msg.OK,
	            fn        : this.processLogout,
	            icon      : Ext.MessageBox.INFORMATION,
	            closable:false
	        });  
	    },
	 
	    processLogout : function ( btn) {
	    	if (btn == 'ok' ) self.location = '/recruit/j_spring_security_logout';  // put here your own redirect, this is going to the root page
	    	self.location = '/recruit/j_spring_security_logout'; 
	    }
	
});


 Ext.define('tz.ui.UserDetailPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 10,
    title: '',
    anchor:'60% 50%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},
    
    initComponent: function() {
        var me = this;
        me.user ="";

        me.authorityCombo = Ext.create('Ext.form.ComboBox', {
        	fieldLabel:'Authority',
        	name:'authority',
        	forceSelection:true,
            store: ds.authorityStore,
            tabIndex:5,
            displayField: 'name',
            valueField: 'value',
		    queryMode: 'local',
		    typeAhead: true,
		});

        me.deleteButton = new Ext.Button({
            text: 'Delete',
            iconCls : 'btn-delete',
            scope : this,
            tabIndex:54,
            handler: function(){
    			this.deleteConfirm();
    		}
        });

        me.resetPasswordButton = new Ext.Button({
            text: 'Reset Password',
            iconCls : 'btn-reset',
            tabIndex:55,
            scope : this,
            handler: function(){
            	this.resetPasswordConfirm();
            }
        });
        
        me.givingAcessButton = new Ext.Button({
            text: 'Give Access',
            iconCls : 'btn-access',
            tabIndex:56,
            scope : this,
            handler: function(){
            	this.givingAcessConfirm();
            }
        });

        me.revokeAccessButton = new Ext.Button({
            text: 'Revoke Access',
            iconCls : 'btn-revoke',
            tabIndex:57,
            scope : this,
            handler: function(){
            	this.revokeAccessConfirm();
            }
        });

        me.benefitsButton = new Ext.Button({
            text: 'Benefits',
            iconCls : 'btn-add',
            tabIndex:58,
            scope : this,
            handler: function(){
            	var empId= this.form.findField('id').getValue();
            	this.manager.benefitsDetailPanel.loadBenefits(empId);
            }
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        scope: this,
                        tabIndex:53,
            	        handler: this.saveUser
                    },'-',
                    me.deleteButton,'-',me.resetPasswordButton,'-',me.givingAcessButton,'-',me.revokeAccessButton,'-',
                    {
                        xtype: 'button',
                        text: 'Close',
                        iconCls : 'btn-close',
                        tabIndex:59,
                        scope : this,
                        handler: function(){
                        	app.userMgmtPanel.initScreen();
                        }
                    }
                ]
            }
        ];
    	
        me.items = [this.getMessageComp(), this.getHeadingComp(),
            {
	   	    	 xtype:'fieldset',
	   	         title: 'User Information',
	   	         collapsible: true,
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'First Name',
	   	                 name: 'firstName',
	   	                 allowBlank:false,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	                 tabIndex:1,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Email',
	   	                 name: 'emailId',
	   	                 allowBlank:false,
	   	                 vtype:'email',
	   	                 tabIndex:3,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	             },{
	   	            	 xtype: 'textfield',
	   	            	 fieldLabel: 'Login Id',
	   	            	 name: 'loginId',
	   	            	 readOnly : true
	   	             }
	   	             ]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Last Name',
	   	                 name: 'lastName',
	   	                 allowBlank:false,
	   	                 tabIndex:2,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	             },	new tz.ui.ActiveCombo({
		   	  			name : 'active',
		   				store : ds.boolStore,
		   				forceSelection:true,
		   				value : true,
	   	                tabIndex:12,
			   		 }),
			   		 me.authorityCombo
	   	             ]
	   	         }]
	   	     },
   	     {
   	        xtype: 'numberfield',
   	        hidden:true,
   	        name: 'id'
   	    },{
   	        xtype: 'textfield',
   	        hidden:true,
   	        name: 'password'
   	    }
        ];
        me.callParent(arguments);
    },

    saveUser: function(){
    	if(this.form.isValid( )){
			var otherFieldObj = {};
			var itemslength = this.form.getFields().getCount();
			var i = 0;
			while (i < itemslength) {
				var fieldName = this.form.getFields().getAt(i).name;
				if(fieldName == 'id') {
					var idVal = this.form.findField(fieldName).getValue();
					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0) {
						otherFieldObj['id'] = this.form.findField(fieldName).getValue();
					}
				} else {
					otherFieldObj[fieldName] = this.form.findField(fieldName).getValue();
				}
				i = i+1;
			}
			
			otherFieldObj = Ext.JSON.encode(otherFieldObj);
			app.employeeService.saveUser(otherFieldObj, this.onSaveUser, this);
    	} else {
    		this.updateError('Please fix the errors');
    	}
	},
	
	onSaveUser: function(data){
		if (data.success) { 
			var user = Ext.create('tz.model.User', data.returnVal);
			this.loadUser(user);
			this.updateMessage('Saved Successfully');
		}else {
			this.updateError('Save Failed');
		}	
	},

	onDelete : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			app.userMgmtPanel.initScreen();
		}
	},
	
	deleteConfirm : function()
	{
		Ext.Msg.confirm("This will delete the Employee", "Do you want to continue?", this.deleteEmployee, this);
	},
	
	deleteEmployee : function(dat){
		if(dat=='yes'){
			var idVal = this.getValues().id;
			app.employeeService.deleteUser(idVal, this.onDelete, this);
		}
	},

	onResetPassword : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			this.updateMessage('Password Reset Successful. The new password is '+data.successMessage);
		}
		
	},
	
	resetPasswordConfirm : function()
	{
		Ext.Msg.confirm("Reset Password", "Do you want to reset the password ?", this.resetPassword, this);
	},
	
	resetPassword : function(dat){
		this.clearMessage();
		if(dat=='yes'){	
			var empId = this.getValues().id;
			app.employeeService.resetPassword(empId, this.onResetPassword, this);
		}
	},

	loadUser: function(user){
		this.clearMessage();
		this.user = user;
		if(user){
			this.loadRecord(user);
			var loginId=user.data['loginId'];
			var empId=user.data['id'];
			if(!loginId){
				this.resetPasswordButton.disable();
				this.givingAcessButton.enable();
				this.revokeAccessButton.disable();
				this.authorityCombo.setReadOnly(true);
			}
			else{
				this.resetPasswordButton.enable();
				this.givingAcessButton.disable();
				this.revokeAccessButton.enable();
				this.authorityCombo.setReadOnly(false);
			}
			var params = new Array();
		} else {
			this.getForm().reset();
			this.deleteButton.disable();
			this.givingAcessButton.disable();
			this.resetPasswordButton.disable();
			this.revokeAccessButton.disable();
			this.authorityCombo.setReadOnly(true);
		}
	},
	
	onGivingAcess : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			var user = Ext.create('tz.model.User', data.returnVal);
			this.loadUser(user);
			this.updateMessage('Access Granted to this employee, Default Loginid is the email Id, Password is '+data.successMessage);
		}
	},
	
	givingAcessConfirm : function()
	{
		if (this.user.data.active) {
			Ext.Msg.confirm("Grant Access", "Do you want to give access to this employee ?", this.givingAcess, this);	
		}else{
			this.updateError("Employee must be active inorder to give access");
		}
	},
	
	givingAcess : function(dat){
		this.clearMessage();
		if(dat=='yes'){
			var empId = this.getValues().id;
			app.employeeService.givingAcess(empId, this.onGivingAcess, this);
		}
	},
	
	onRevokeAccess : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			var user = Ext.create('tz.model.User', data.returnVal);
			this.loadUser(user);
			this.updateMessage('Access removed successfully for this employee');
		}
	},
	
	revokeAccessConfirm : function()
	{
		Ext.Msg.confirm("Revoke Access", "Do you want to revoke access to this employee ?", this.revokeAccess, this);
	},
	
	revokeAccess : function(dat){
		this.clearMessage();
		if(dat=='yes'){	
			var empId = this.getValues().id;
			app.employeeService.revokeAccess(empId, this.onRevokeAccess, this);
		}
	},
	
});Ext.define('tz.ui.UserGridPanel', {
    extend: 'Ext.grid.Panel',
    title: 'Users',
    frame:true,
    anchor:'60%',
    height:540,
    width :'100%',
    
    initComponent: function() {
        var me = this;
        me.store = ds.userStore;
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :40,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'View User',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					app.userMgmtPanel.showUser(rowIndex);
    				}
    			}]
            },{
                xtype: 'gridcolumn',
                dataIndex: 'firstName',
                text: 'First Name',
                renderer: userRender,
                width :120,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastName',
                text: 'Last Name',
                renderer: userRender,
                width :120,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'emailId',
                text: 'EmailID',
                renderer: userRender,
                width:200,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'loginId',
                text: 'Login Id',
                renderer: userRender,
                width:200,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'active',
                text: 'Active',
                renderer : userActiveRenderer,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'authority',
                text: 'Authority',
                renderer: userRender,
                width:120,
            }
        ];

        me.viewConfig = {
        		stripeRows: true	
        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        iconCls : 'btn-add',
                        tabIndex:11,
                        handler: function(){
                    		me.addUser();
                    	}
                    },'-',{
                        xtype:'button',
                    	itemId: 'grid-excel-button',
                    	iconCls : 'btn-report-excel',
                        text: 'Export to Excel',
                        tabIndex:12,
                        handler: function(){
                        	var vExportContent = me.getExcelXml();
                            document.location='data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);
                        }
                    },'->',me.showCount
                    ]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        me.callParent(arguments);
    },
    
    addUser : function() {
    	app.userMgmtPanel.showUser();
	},


});

function activeRenderer(val) {
	if (val == true) {
		var val1="Yes";
		return '<span style="color:green;">' + val1 + '</span>';
	} 
	if (val == false) {
		var val1="No";
		return '<span style="color:red;">' + val1 + '</span>';
	}
	return val1;
}

function userRender(value, metadata, record, rowIndex, colIndex, store) {
    var dataIndex = app.userMgmtPanel.userGridPanel.columns[colIndex].dataIndex;
    metadata.tdAttr = 'data-qtip="' + record.get(dataIndex) + '"';
    return value;
}

function userActiveRenderer(val, metadata, record, rowIndex, colIndex, store) {
	if (val == true) {
		metadata.tdAttr = 'data-qtip="' + 'Yes' + '"';
		return '<span style="color:green;">' + 'Yes' + '</span>';
	} 
	if (val == false) {
		metadata.tdAttr = 'data-qtip="' + 'No' + '"';
		return '<span style="color:red;">' + 'No' + '</span>';
	}
	return val;
}
Ext.define('tz.ui.UserMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.userGridPanel = new tz.ui.UserGridPanel({manager:me});
    	me.userSearchPanel = new tz.ui.UserSearchPanel({manager:me});
    	me.userDetailPanel = new tz.ui.UserDetailPanel({manager:me});
        
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [ this.userSearchPanel,
 				        {
		   	        		xtype: 'container',
		   	        		height: 10,
		   	        		border : false
 				        },
 				        this.userGridPanel
 				        ]
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
	            items: [me.userDetailPanel]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen: function(){
		this.getLayout().setActiveItem(0);
		this.userSearchPanel.search();
		ds.userSearchStore.loadByCriteria();
	},
	
	showUser : function(rowIndex) {
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var userRec = ds.userStore.getAt(rowIndex);
		}
		this.userDetailPanel.loadUser(userRec);
	}
	
});
Ext.define('tz.ui.UserSearchPanel', {
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    frame:true,
    layout:'anchor',
    anchor:'60%',
    autoScroll:true,
//creating view in init function
    initComponent: function() {
        var me = this;
        
        me.userCombo = Ext.create('Ext.form.ComboBox', {
            fieldLabel: 'User',
            store: ds.userSearchStore,
            name : 'userId',
            tabIndex:7,
            queryMode: 'local',
            displayField: 'fullName',
            labelAlign: 'right',
            valueField: 'id',
            triggerAction: 'all',
        	emptyText:'Select...',
        	anyMatch:true,
        });

        me.authorityCombo = Ext.create('Ext.form.ComboBox', {
        	fieldLabel:'Authority',
        	name:'authority',
            store: ds.authorityStore,
            tabIndex:2,
            displayField: 'name',
            valueField: 'value',
		    queryMode: 'local',
		    typeAhead: true,
		});

        
        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [me.userCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [me.authorityCombo]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:2,
			},{
				text : 'Reset',
				scope: this,
				handler: this.reset,
				tabIndex:3,
			},{
				text : 'Clear',
				scope: this,
				tabIndex:4,
	             handler: function() {
	            	 this.form.reset();
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['userId','=', values.userId]);
    	params.push(['authority','=', values.authority]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=50;
    	ds.userStore.loadByCriteria(filter);
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
Ext.define('tz.service.SettingsService', {
	extend : 'tz.service.Service',
		
	save : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/save',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    getCategory : function(storeType,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/settings/getCategory',
			params : {
				json : storeType
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },


});


Ext.define('tz.ui.SettingsMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',
    activeItem: 0,
    layout:'card',
    title: '',
    padding: 10,
    border:false,
    frame : false,
 
    initComponent: function() {
    	var me = this;
        
        me.settingsDetailPanel = new tz.ui.SettingsDetailPanel({manager:me});
    	me.settingsSearchPanel = new tz.ui.SettingsSearchPanel({manager:me});
    	me.settingsGridPanel = new tz.ui.SettingsGridPanel({manager:me});
    	
        me.items = [{
					xtype: 'panel',
                    autoScroll:true,
                    border:false,
                    layout:'anchor',
                    items: [
                            me.getMessageComp(),
                            me.settingsSearchPanel,
                            {
                            	xtype: 'container',
                            	height: 10,
                            	border : false
			   	   	 		},
			   	   	 		me.settingsGridPanel,
			   	   	 		{
			   	   	 			xtype: 'container',
			   	   	 			height: 10,
			   	   	 			border : false
			   	   	 		},
			   	   	 		me.settingsDetailPanel
			   	   	 		]
        	}
        ];
    	
        me.callParent(arguments);
    },
    
    initScreen: function(){
    	this.clearMessage();
    	this.getLayout().setActiveItem(0);
    	this.settingsSearchPanel.search();
		ds.settingsStore.loadByCriteria();
    },
    
});
Ext.define('tz.ui.SettingsDetailPanel', {
    extend: 'Ext.form.Panel',
    bodyPadding: 10,
    title: 'Step 3 : Edit or add a new type.',
    anchor:'50%',
    height:270,
    autoScroll:true,
    //id : 'SettingsDetailPanel',
    initComponent: function() {
        var me = this;
        
        me.deleteButton = new Ext.Button({
            xtype: 'button',
            text: 'Delete',
            iconCls : 'btn-delete',
            disabled :true,
            scope : this,
            handler: function(){
    			this.deleteConfirm();
    		}
        });
        
        me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    store: ds.settingsStore,
		    queryMode: 'local',
		    displayField: 'type',
		    valueField: 'type',
		    allowBlank:false,
		    name : 'type',
		    maxLength:50,
			enforceMaxLength:true,
		    tabIndex:1,
		});
        
        
        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        iconCls : 'btn-add',
                        scope : this,
                        handler: function(){
                			this.form.reset();
                		}
                    },'-',{
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        scope: this,
                        handler: function(){
                			this.saveConfirm();
                		}
                    },'-',{
                        xtype: 'button',
                        text: 'Reset',
                        iconCls : 'btn-refresh',
                        scope : this,
                        handler: function(){
                			this.form.reset();
                			app.settingsMgmtPanel.clearMessage();
                		}
                    }
                ]
            }
        ];
        me.items = [
            {
	        		 xtype:'container',
	        		 layout: 'column',
		   	         title: 'Add a Category',
		   	         items :[{
		   	             xtype: 'container',
		   	             columnWidth:.3,
		   	             items: [me.typeCombo,
		   	             {
				   	        xtype: 'textfield',
				   	        name: 'name',
				   	        fieldLabel: 'Name',
		   	                allowBlank:false,
		   	                maxLength:50,
		   	                width : 255,
							enforceMaxLength:true,
		   	                tabIndex:2
				   	    },{
				   	        xtype: 'textfield',
				   	        name: 'value',
				   	        fieldLabel: 'Value',
		   	                allowBlank:false,
		   	                maxLength:50,
		   	                width : 255,
							enforceMaxLength:true,
		   	                tabIndex:3
				   	    },{
				   	        xtype: 'textarea',
				   	        name: 'notes',
				   	        fieldLabel: 'Notes',
		   	                maxLength:200,
							enforceMaxLength:true,
		   	                width : 255,
		   	                tabIndex:4
				   	    },{
				   	        xtype: 'numberfield',
				   	        hidden:true,
				   	        name: 'id',
				   	        listeners: {
				   	        	change: function(field, value) {
				   	        		if(value == null){
				   	        			me.deleteButton.disable();
				   	        			me.form.findField('name').setReadOnly(false);
				   	        		}
				   	        		else{
				   	        			me.deleteButton.enable();
				   	        			me.form.findField('name').setReadOnly(true);
				   	        		}
				   	        	}
			                }
				   	    }]
        		}]	   	     }
        ];
        me.callParent(arguments);
    },
    
    saveConfirm : function() {
    	if(!this.form.isValid( )){
    		app.settingsMgmtPanel.updateError('Please fix the errors');
    		return;
    	}
    	var selectedRecord = app.settingsMgmtPanel.settingsGridPanel.getSelectionModel().getSelection()[0];
    	if (selectedRecord != null && this.form.findField('id').getValue() != null) {
			if (selectedRecord.data.type != this.typeCombo.getValue()) {
				Ext.Msg.confirm("Type is changed", "Do you want to continue?", this.save, this);
			}else
				this.save('yes');
		}else
			this.save('yes');
	},
    
    save : function(dat) {
    	if (dat =='yes') {
        	if(this.form.isValid( )){
        		var setting = {};
        		var itemslength = this.form.getFields().getCount();
        		var i = 0;
    			while (i < itemslength) {
    				var fieldName = this.form.getFields().getAt(i).name;
    				if(fieldName == 'id') {
    					var idVal = this.form.findField(fieldName).getValue();
    					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0) {
    						setting['id'] = this.form.findField(fieldName).getValue();
    					}
    				}else
    					setting[fieldName] = this.form.findField(fieldName).getValue();
    				i++;
    			}
    			setting.type = setting.type.charAt(0).toUpperCase() + setting.type.slice(1)
    			setting = Ext.JSON.encode(setting);
    			app.settingsService.save(setting,this.onSave,this);
        	}else
        		app.settingsMgmtPanel.updateError('Please fix the errors');
    	}
    },

    onSave : function(data) {
		if (data.success) { 
			var employee = data.returnVal;
			this.form.findField('id').setValue(employee.id);
			this.form.reset();
			ds.settingsStore.loadByCriteria();
			app.settingsMgmtPanel.settingsSearchPanel.search();
			app.settingsMgmtPanel.updateMessage('Saved Successfully');
		}  else {
			app.settingsMgmtPanel.updateError('Save Failed');
		}	
	},
    
    loadSettings : function(settings) {
    	this.record = settings;
    	this.type = settings.data.type;
    	this.form.reset();
    	this.getForm().loadRecord(settings);
	},
    
	deleteConfirm : function() {
		Ext.Msg.confirm("Delete", "Do you want to continue?", this.deleteCategory, this);
	},
	
	deleteCategory : function(dat) {
		if(dat=='yes'){
			var idVal = this.form.findField('id').getValue();
			app.settingsService.deleteCategory(idVal,this.onDelete,this);
		}
	},
	
	onDelete : function(data) {
		if (data.success) {
			app.settingsMgmtPanel.settingsSearchPanel.search();
			this.form.reset();
			app.settingsMgmtPanel.updateMessage('Deleted Successfully');
		}  else {
			app.settingsMgmtPanel.updateError(data.errorMessage);
		}	
	}
});Ext.define('tz.ui.SettingsSearchPanel', {
    extend: 'Ext.form.Panel',
    bodyPadding: 10,
    title: 'Step 1 : Search for a type',
    frame:true,
    anchor:'50% 22%',
    //width:'100%',
    //height:150, 
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},

    initComponent: function() {
        var me = this;
        
       
        me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    store: ds.settingsStore,
		    labelAlign: 'right',
		    queryMode: 'local',
		    displayField: 'type',
		    valueField: 'type',
		    name : 'type',
		    tabIndex:1
		});
        
        me.items = [
            {
   	             xtype: 'container',
   	             layout: 'column',
   	             columnWidth:.5,
   	             items: [me.typeCombo]
	   	     }
        ]; 
        me.buttons = [ 
            {
            	text : 'Search',
            	scope: this,
            	tabIndex:9,
    	        handler: this.search
			}, 
			{
				text : 'Reset',
				scope: this,
				tabIndex:10,
				handler: this.reset
			} 
		];
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	app.settingsMgmtPanel.settingsDetailPanel.getForm().reset();
    	// Set the params
    	var params = new Array();
    	params.push(['type','like', this.typeCombo.getValue()]);

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=100;
    	ds.settingsSearchStore.loadByCriteria(filter);
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
	
});
Ext.define('tz.ui.SettingsGridPanel', {
    extend: 'Ext.grid.Panel',
    title: 'Step 2 : Select any item to edit',
    frame:true,
    anchor:'50%',
    height:250,
    //width :'100%',
    listeners: {
    	itemclick: function(dv, record, item, index, e) {
    		app.settingsMgmtPanel.settingsDetailPanel.loadSettings(record);                                       
    	}
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.settingsSearchStore;
        
        me.columns = [
			{
			    dataIndex: 'name',
			    text: 'Name',
			    sortable : true,
			    width : 130
			},{
                dataIndex: 'value',
                text: 'Value',
                sortable : true,
                width : 130
            },{
                dataIndex: 'type',
                text: 'Type',
                sortable : true,
                width : 130
            },{
                dataIndex: 'notes',
                text: 'Notes',
                sortable : true,
                width : 200
            }
        ];
        
        me.viewConfig = {
        		stripeRows:false 
        }
        
        me.on('scrollershow', function(scroller) {
        	  if (scroller && scroller.scrollEl) {
        	    scroller.clearManagedListeners(); 
        	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
        	  }
        });
        
        me.callParent(arguments);
    },
    
    
});

Ext.define('tz.service.RequirementService', {
	extend : 'tz.service.Service',
		
    saveRequirements : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveRequirements',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },    
    
    deleteRequirement: function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/deleteRequirement/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    saveRequirement : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveRequirement',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    excelExport: function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/excelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },
    
    onSuccess : function(err) {
    	//Opening the Excel Filename returned from Java, This initiates download on the Browser
    	if (err.responseText.search(false) != -1) {
    		app.invoiceService.onFailure(err);
			return;
		}
    	if (err.responseText.search('.pdf') != -1) {
			var i = err.responseText.indexOf('.pdf');
			var w = window.open(err.responseText.slice(29,i+4));
		}else{
	    	var i=err.responseText.indexOf('xls');
	    	window.open(err.responseText.slice(29,i+3),'_self','',true);
		}
    	app.loadGenerateMask.hide();
    	this.onAjaxResponse;
    },
    
    onFailure : function(err) {
    	app.loadGenerateMask.hide();
    	if (err.responseText.search('.pdf') != -1) {
            Ext.MessageBox.show({
                title: 'Error',
                msg: 'Pdf Generation failed',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
		}else{
	        Ext.MessageBox.show({
	            title: 'Error',
	            msg: 'Excel export failed',
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
        this.onAjaxResponse;
    },

    contactExcelExport: function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/contactExcelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },
    
    clientExcelExport : function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/clientExcelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },
    
    placementsExcelExport : function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/placementLeads/placementsExcelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },
    
    removeCandidate : function(idVals,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/removeCandidate',
			params : {
				json : idVals
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    removeVendor : function(idVals,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/removeVendor',
			params : {
				json : idVals
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    saveTask : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveTask',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },    

    deleteTask: function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/deleteTask/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    saveSubRequirements : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveSubRequirements',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },    

    saveSubVendors : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveSubVendors',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },    
    
});
Ext.define('tz.ui.RequirementMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',
    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;
        
		// Vendor Details Panel
    	me.requirementsGrid = new tz.ui.RequirementsGrid({manager:me});
    	me.requirementsSearchPanel = new tz.ui.RequirementsSearchPanel({manager:me});
    	me.requirementDetailPanel = new tz.ui.RequirementDetailPanel({manager:me});
		
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,    	                 
            	border:false,  
            	items: [me.requirementsSearchPanel,
            	        {
		   	        		xtype: 'container',
		   	        		height: 10,
            	        },
            	        me.requirementsGrid]
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
	            items: [me.requirementDetailPanel]
            }
        ];
        me.callParent(arguments);
    },


	initScreen: function(){
		//this.getLayout().setActiveItem(0);
		ds.clientTypeStore.loadAll();
		ds.immigrationTypeStore .loadAll();
		ds.employmentTypeStore.loadAll();
		ds.employerStore.loadAll();
		ds.researchStatusStore.loadAll();
		ds.marketingStatusStore.loadAll();
		ds.qualificationStore.loadAll();
		ds.hotnessStore.loadAll();
		ds.jobOpeningStatusStore.loadAll();
		ds.candidateSourceStore.loadAll();
		
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	if(app.candidateMgmtPanel != null || ds.candidateSearchStore.getCount() == 0)
    		ds.candidateSearchStore.loadByCriteria(filter);
    	ds.contactSearchStore.loadByCriteria(filter);

    	var params = new Array();
    	params.push(['type','=', 'Client']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.clientSearchStore.loadByCriteria(filter);

    	var params = new Array();
    	params.push(['type','=', 'Vendor']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.vendorStore.loadByCriteria(filter);
    	
    	if (ds.requirementStore.getCount() ==0) 
    		this.requirementsSearchPanel.search();	
	},
	
	showRequirement : function(rowIndex) {
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var record = ds.requirementStore.getAt(rowIndex);
		}
		this.requirementDetailPanel.loadForm(record);
	},
	
	closeForm : function() {
		this.getLayout().setActiveItem(0);
		this.requirementsSearchPanel.search();
	}
	
});
Ext.define('tz.ui.RequirementsGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Requirements',
    frame:true,
    anchor:'100%',
    height:550,

    width :'100%',    
    listeners: {
    	edit: function(editor, event){
    		var record = event.record;
            if(editor.context.field == 'phoneNo'){
            	record.data.phoneNo = record.data.phoneNo.replace(/[^0-9]/g, "");
            	record.data.phoneNo = record.data.phoneNo.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
            	this.getView().refresh();
            }
            if (editor.context.field == 'vendor') {
            	var vendor = ds.vendorStore.getAt(ds.vendorStore.find('name',record.data.vendor));
            	for ( var i = 0; i < ds.contactSearchStore.getCount() ; i++) {
					if (vendor.data.id == ds.contactSearchStore.data.items[i].data.clientId) {
						var contact = ds.contactSearchStore.data.items[i];
						break;
					}
				}
            	if (contact != null) {
            		record.data.contactPerson = contact.data.fullName;
            		this.modifiedIds.push(record.data.id);
				}else
					record.data.contactPerson ='';
            	this.getView().refresh();
			}
            if (editor.context.field == 'requirement') {
            	while (record.data.requirement.indexOf('"') != -1) {
            		record.data.requirement = record.data.requirement.replace('"','');	
				}
                if(event.originalValue != record.data.requirement){
        			this.modifiedIds.push(record.data.id);
        		}
                this.getView().refresh();
			}
            if(event.originalValue != event.value){
    			this.modifiedIds.push(record.data.id);
    		}
        	
        },
        
    },
     
    initComponent: function() {
        var me = this;
        me.store = ds.requirementStore;
        me.modifiedIds = new Array();
        me.modifiedCandidateIds = new Array();
        me.removeRecord = "";
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :40,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'View Requirement',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					app.requirementMgmtPanel.showRequirement(rowIndex);
    				}
    			}]
            },{
		        xtype: 'gridcolumn',
		        dataIndex: 'hotness',
		        text: 'Hotness',
		        width :  45,
                editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                forceSelection:true,
	                displayField: 'value',
	                valueField: 'name',
	                emptyText:'Select...',
	    		    store : ds.hotnessStore,
	                lazyRender: true,
	                listClass: 'x-combo-list-small',
	            },
                renderer:function(value, metadata, record){
                	if (value == 0) {
                		metadata.style = "color:#009AD0;";
					}
                	var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.value+ '"';
                		return rec.data.value;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;	
                	}
	            }
		    },{
				text : 'Unique Id',
				dataIndex : 'id',
				width :  45,
		        renderer: renderValue,
			},{
    			text : 'Job Opening Status',
    			dataIndex : 'jobOpeningStatus',
    			width :  60,
                editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    forceSelection:true,
                    displayField: 'value',
        		    valueField: 'name',
                    store: ds.jobOpeningStatusStore,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                },
                renderer: renderValue,
    		},{
    			xtype: 'datecolumn',
				text : 'Posting Date',
				dataIndex : 'postingDate',
				width :  60,
				renderer: dateRender,
				editor: {
	                xtype: 'datefield',
	                format: 'm/d/y'
	            }
			},{
		        xtype: 'gridcolumn',
		        dataIndex: 'postingTitle',
		        text: 'Posting Title',
		        renderer: renderValue,
		        editor: {
	                xtype: 'textfield',
                    maxLength:100,
   	                enforceMaxLength:true,
	            }
		    },{
		        xtype: 'gridcolumn',
		        dataIndex: 'location',
		        text: 'Location',
		        renderer: renderValue,
		        editor: {
	                xtype: 'textfield',
                    maxLength:50,
   	                enforceMaxLength:true,
	            }
		    },{
				header : 'Client',
				dataIndex : 'client',
	            width :  120,
		        editor: {
	                xtype: 'combobox',
	                forceSelection:true,
	                queryMode: 'local',
	                triggerAction: 'all',
	                displayField: 'name',
	                valueField: 'name',
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.clientSearchStore,
	            },
	            renderer: renderValue,
			},{
				header : 'Vendor',
				dataIndex : 'vendorId',
	            width :  120,
		        /*editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                triggerAction: 'all',
	                displayField: 'name',
	                valueField: 'name',
	                forceSelection:true,
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.vendorStore,
	            },*/
                renderer:function(value, metadata, record){
					var ids = value.toString().split(',');
					var names ="";
					for ( var i = 0; i < ids.length; i++) {
						var rec = ds.vendorStore.getById(parseInt(ids[i]));
						if (rec)
							names += rec.data.name +", ";	
					}
					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
					return names.substring(0,names.length-2);
                }
			},{
				header : 'Contact Person',
				dataIndex : 'contactId',
	            width :  120,
		        /*editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                triggerAction: 'all',
	                displayField: 'fullName',
	                valueField: 'fullName',
	                forceSelection:true,
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.contactSearchStore,
	            },*/
                renderer:function(value, metadata, record){
					var ids = value.toString().split(',');
					var names ="";
					for ( var i = 0; i < ids.length; i++) {
						var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
						if (rec)
							names += rec.data.fullName +", ";	
					}
					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
					return names.substring(0,names.length-2);
                }
			},{
				text : 'Roles and Responsibilities',
				dataIndex : 'requirement',
				width :  150,
		        editor: {
	                xtype: 'textarea',
	                height : 60,
                    maxLength:4000,
   	                enforceMaxLength:true,
	            },
	            renderer: renderValue
			},{
				header : 'Candidate',
				dataIndex : 'candidateId',
	            width :  120,
                renderer:function(value, metadata, record){
					var ids = value.toString().split(',');
					var names ="";
					for ( var i = 0; i < ids.length; i++) {
						var rec = ds.candidateSearchStore.getById(parseInt(ids[i]));
						if (rec)
							names += rec.data.firstName+' '+rec.data.lastName +", ";	
					}
					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
					return names.substring(0,names.length-2);
                }
			},{
				xtype: 'gridcolumn',
				text : 'Submitted Date',
				dataIndex : 'submittedDate',
				width :  60,
                renderer:function(value, metadata, record){
                	if (value != null && value != "") {
                		var req_submittedDate = value.split(',');
                		var dates = new Array();
                		for ( var i = 0; i < req_submittedDate.length; i++) {
                			var id_dates = req_submittedDate[i].split('-');
       						var rec = ds.candidateSearchStore.getById(parseInt(id_dates[0]));
       						if (rec)
       							dates.push(rec.data.firstName+'-'+id_dates[1]);	
    					}
    					metadata.tdAttr = 'data-qtip="' + dates.toString() + '"';
    					return dates.toString();
					}
                	return value;
                }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'module',
                text: 'Module',
                width :  60,
                renderer: renderValue,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
				text : 'Employer',
				dataIndex : 'employer',
				width :  40,
				renderer: employerRenderer,
		        editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                forceSelection:true,
	                displayField: 'value',
	                valueField: 'name',
	                emptyText:'Select...',
	    		    store : ds.employerStore,
	                lazyRender: true,
	                listClass: 'x-combo-list-small'
	            }
			},{
				text : "Alert",
				dataIndex : 'addInfo_Todos',
				width :  80,
                renderer: alertRender,
		        editor: {
	                xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
   	                fieldStyle: "color: red;",
	            }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                renderer: renderValue,
                width:150,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:300,
   	                enforceMaxLength:true,
                }
            },{
  				text : 'Public Link',
  				dataIndex : 'publicLink',
  				renderer: linkRenderer,
  				width :  80,
  				editor: {
  	                xtype: 'textfield',
  	                maxLength:200,
 	                enforceMaxLength:true,
 	                vtype:'url',
  				},
  			},{
				text : 'Source',
				dataIndex : 'source',
				renderer: renderValue,
		        editor: {
	                xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
	            }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'rateSubmitted',
                text: 'Rate Submitted',
                renderer: renderValue,
                editor: {
                    xtype: 'textfield',
                    maxLength:75,
   	                enforceMaxLength:true,
                }
            },{
				text : 'Phone No',
				dataIndex : 'phoneNo',
				width :  80,
                renderer: renderValue,
				editor: {
                    xtype: 'textfield',
                    maxLength:15,
                 	maskRe :  /[0-9]/,
   	                enforceMaxLength:true,
                },
			},{
				text : 'Last Updated User',
				dataIndex : 'lastUpdatedUser',
				width:80,
				renderer:function(value, metadata, record){
               		metadata.tdAttr = 'data-qtip="' + value + '"';
                	return value;
                }
			},{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:100,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:100,
            }
        ];
        
        me.viewConfig = {
        		stripeRows: true,
            	getRowClass: function (record, rowIndex, rowParams, store) {
            		
            		var ids = record.data.candidateId.toString().split(',');
					var dates = new Array();
					for ( var i = 0; i < ids.length; i++) {
						var rec = ds.candidateSearchStore.getById(parseInt(ids[i]));
						if (rec && rec.data.submittedDate != null)
							dates.push(rec.data.submittedDate);
					}
					if (record.data.candidateId.toString() == '' && record.data.postingDate != null) {
                		if (Ext.Date.format(new Date(),'m/d/y') == Ext.Date.format(record.data.postingDate,'m/d/y')) {
                			return 'back-lightGreen1';
    					}else if (Ext.Date.format(new Date((new Date()).setDate((new Date()).getDate()-1)),'m/d/y') == Ext.Date.format(record.data.postingDate,'m/d/y')) {
    						return 'back-lightGreen2';
    					}else if (record.data.postingDate.getTime() > (new Date()).setDate((new Date()).getDate()-7)) {
    						return 'back-lightGreen3';
    					}else
    						return 'row-lightgray';
					}else{
						return 'row-font12';
					}
            	}

        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
                    text: 'Add New',
                    iconCls : 'btn-add',
                    tabIndex:12,
                    handler: function(){
                    	me.addRequirement()
                    }
            	},'-',{
            		xtype: 'button',
            		text: 'Save',
            		iconCls : 'btn-save',
            		tabIndex:13,
            		handler: function(){
            			me.saveRequirements()
            		}
            	},'-',{
                    xtype: 'button',
                    text: 'Delete',
                    iconCls : 'btn-delete',
                    tabIndex:14,
                    handler: function(){
                		me.deleteConfirm();
                	}
                },'-',{
            		xtype:'button',
                  	itemId: 'grid-excel-button',
                  	iconCls : 'btn-report-excel',
                  	text: 'Export to Excel',
                  	tabIndex:15,
                  	handler: function(){
                  		me.getExcelExport();
                  		/*var vExportContent = me.getExcelXml();
                  		document.location='data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);*/
                  	}
            	},'-',{
            		xtype: 'button',
                    text: 'Add Client',
                    iconCls : 'btn-add',
                    tabIndex:16,
                    handler: function(){
                    	me.addClient('Client');
                    }
            	},'-',{
            		xtype: 'button',
                    text: 'Add Vendor',
                    iconCls : 'btn-add',
                    tabIndex:17,
                    handler: function(){
                    	me.addClient('Vendor');
                    }
            	},'-',{
            		xtype: 'button',
                    text: 'Add Candidate',
                    iconCls : 'btn-add',
                    tabIndex:18,
                    handler: function(){
                    	me.addCandidate();
                    }
            	},'-',{
            		xtype: 'button',
                    text: 'Link Candidates',
                    iconCls : 'btn-link',
                    tabIndex:19,
                    //hidden : true,
                    handler: function(){
                    	me.linkCandidate();
                    }
            	},'-',{
        			text : 'Help',
         			iconCls : 'icon_info',
         			scope: this,
         			tabIndex:20,
         			handler: function(){
         		        var legendWin = Ext.create("Ext.window.Window", {
         					title: 'Help',
         					modal: true,
         					height      : 300,
         					width       : 450,
         					bodyStyle   : 'padding: 1px;background:#ffffff;',
         					html: '<B><U>Legend :</U></B>'
         						+ '<table cellspacing="10" cellpadding="5" noOfCols="2" width="450"> '
         						+ '<tr>'
         						+ '<td width="20%" style="background-color:#C2EE9A;"></td>'
         						+ '<td width="50%">Job Openings which are posted today</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="20%" style="background-color:#C2DFBD;"></td>'
         						+ '<td width="50%">Job Openings which are posted yesterday</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="20%" style="background-color:#C2C9A9;"></td>'
         						+ '<td width="50%">Job Openings which are posted in this week and older than yesterday</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="20%" style="background-color:#E5E5E5;"></td>'
         						+ '<td width="50%">Job Openings which are older than one week</td>'
         						+ '</tr>'
         						+ '</table>'
         				});

         				legendWin.show();
         		    }
            	},'->',me.showCount]
          }
      ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        me.store.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);

        ds.candidateSearchStore.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);

        ds.clientSearchStore.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);

        ds.contactSearchStore.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);
        
        ds.vendorStore.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);
        
        ds.hotnessStore.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);
        
        me.callParent(arguments);
    },

    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],

    saveCandidates : function() {
    	var records = new Array();
    	for ( var i = 0; i < ds.subRequirementStore.data.length ; i++) {
			var record = ds.subRequirementStore.getAt(i);
			
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				if (record.data.candidateId == 0 || record.data.candidateId == null){
					Ext.Msg.alert('Error','Please select a candidate');
					return false;
				}
				if (record.data.vendorId == 0 || record.data.vendorId == null){
					Ext.Msg.alert('Error','Please select the vendor for the candidate');
					return false;
				}
				for ( var j = 0; j < ds.subRequirementStore.getCount(); j++) {
					var oldRec = ds.subRequirementStore.getAt(j);
					if (oldRec != record && oldRec.data.vendorId == record.data.vendorId && oldRec.data.candidateId == record.data.candidateId) {
						Ext.Msg.alert('Error','Candidate already submitted through given vendor.');
						return false;
					}
				}
	    		if (record.data.contactId == 0 || record.data.contactId == null)
					delete record.data.contactId;
	    		if (record.data.id == 0 || record.data.id == null)
					delete record.data.id;
				records.push(record.data);
			}
		}
    	if (records.length == 0) {
			Ext.Msg.alert('Error','There are no changes in candidates to submit');
		}else{
        	records = Ext.JSON.encode(records);
        	app.requirementService.saveSubRequirements(records,this.onSaveCandidates,this);
		}
	},
	
	onSaveCandidates : function(data) {
		Ext.getCmp('submittedCandidatesWindow').hide();
		if (data.success) {
			this.modifiedCandidateIds = new Array();
			app.requirementMgmtPanel.requirementsSearchPanel.search();
			Ext.Msg.alert('Success','Candidates Saved Successfully');
		}else {
			Ext.Msg.alert('Success','Save Failed');
			this.body.scrollTo('top', 0);
		}	
	},

    
    linkCandidate : function() {
    	var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
    	app.requirementMgmtPanel.requirementDetailPanel.loadSubRequirments(selectedRecord.data.id);
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Requirement to link.');
		}else {
			var vendorStore = Ext.create('Ext.data.Store', {
	            fields: ['id', 'name'],
	        });
			var vendorIds = selectedRecord.data.vendorId.split(',');
			var vendorData= new Array();
			for ( var i = 0; i < vendorIds.length; i++) {
				var vendorObj = ds.vendorStore.getById(parseInt(vendorIds[i]));
				if(vendorObj != null)
					vendorData.push({"id":vendorObj.data.id,"name":vendorObj.data.name});
			}
        	if(vendorData.length >0 )
        		vendorStore.add(vendorData);

	        var submittedCandidates = Ext.create('Ext.grid.Panel', {
	            title: '',
	            frame:true,
	            anchor:'98%',
	            height:300,
	            width:'100%',
	            store: ds.subRequirementStore,
	            plugins: [{
	            	ptype : 'cellediting',
	            	clicksToEdit: 2
	            }],
	            listeners: {
	                edit: function(editor, event){
	                	var record = event.record;
	            		if(event.originalValue != event.value){
	            			app.requirementMgmtPanel.requirementsGrid.modifiedCandidateIds.push(record.data.id);
	            		}
	                },
	            },
	            dockedItems : [{
	            	xtype: 'toolbar',
	            	dock: 'top',
	            	items: [{
	            		xtype: 'button',
	            		text: 'Add Existing Candidate',
	            		iconCls : 'btn-add',
	            		scope: this,
	            		handler : function(grid, rowIndex, colIndex) {
	            			var requirementId = selectedRecord.data.id;
	            	    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
	            	   	 		id : null,
	            	   	 		requirementId : requirementId,
	            	   	 		submittedDate : new Date()
	            	   	 	});
	            	   	 	ds.subRequirementStore.insert(0, rec);
	            	        this.getView().focusRow(0);
	            		} 
	            	},'-',{
	            		xtype: 'button',
	            		text: 'Save',
	            		iconCls : 'btn-save',
	            		scope: this,
	            		handler: this.saveCandidates
	            	},'-',{
	            		xtype: 'button',
	            		text: 'Reset',
	            		scope: this,
	            		handler : function(grid, rowIndex, colIndex) {
	            			this.reloadCandidates();
	            			
	            		} 
	            	}]
	            }],

	            columns: [
	                      {
	                		xtype : 'actioncolumn',
	              			hideable : false,
	              			width : 35,
	              			items : [ {
	              				icon : 'images/icon_delete.gif',
	              				tooltip : 'Remove Candidate from Jop Opening',
	              				padding : 10,
	              				scope : this,
	              				handler : function(grid, rowIndex, colIndex) {
	                 				this.removeCandidateConfirm(ds.subRequirementStore.getAt(rowIndex));
	              				} 
	              			} ]
	              		},{
	            			header : 'Candidate',
	          				dataIndex : 'candidateId',
	          	            width :  150,
	          		        editor: {
	          	                xtype: 'combobox',
	          	                queryMode: 'local',
	          	                triggerAction: 'all',
	          	                displayField: 'fullName',
	          	                valueField: 'id',
	          	                emptyText:'Select...',
	          	                listClass: 'x-combo-list-small',
	          	                store: ds.candidateSearchStore,
	          	            },
	                          renderer:function(value, metadata, record){
	                          	var candidate = ds.candidateSearchStore.getById(value);
	                          	if(candidate){
	                          		return candidate.data.fullName;
	                          	}else{
	                          		return null;
	                          	}
	                          }
	          			},{
	          				text : 'Submitted Date',
	          				dataIndex : 'submittedDate',
	          				width :  100,
	          				renderer: dateRender,
	          				editor: {
	          	                xtype: 'datefield',
	          	                format: 'm/d/y'
	          	            }
	          			},{
	            			header : 'Vendor',
	          				dataIndex : 'vendorId',
	          	            width :  150,
	          		        editor: {
	          	                xtype: 'combobox',
	          	                queryMode: 'local',
	          	                triggerAction: 'all',
	          	                displayField: 'name',
	          	                valueField: 'id',
	          	                emptyText:'Select...',
	          	                listClass: 'x-combo-list-small',
	          	                store: vendorStore,
	          	            },
	                          renderer:function(value, metadata, record){
	                          	var candidate = ds.vendorStore.getById(value);
	                          	if(candidate)
	                          		return candidate.data.name;
	                          	else
	                          		return null;
	                          }
	          			}
	                  ],
	        });

	        if (Ext.getCmp('submittedCandidatesWindow') == null) {
				var myWin = Ext.create("Ext.window.Window", {
					title: 'Submitted Candidates',
					id : 'submittedCandidatesWindow',
					modal: true,
					height      : 350,
					width       : 600,
					bodyStyle   : 'padding: 1px',
					layout      : 'form',
					labelWidth  : 50,
					defaultType : 'field',
					items       : [{
									xtype:'fieldset',
				    	  			layout: 'column',
				    	  			border:false,
				    	  			items:[{
				    	  				xtype:'container',
				    	  				columnWidth:.99,
				    	  				items :[submittedCandidates]
				    	  			}]
			                     }]
				});
			}
			    
	        Ext.getCmp('submittedCandidatesWindow').show();
		}	
	},

	reloadCandidates : function() {
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord.data.id != null && selectedRecord.data.id != ""){
        	var params = new Array();
        	params.push(['requirementId','=', selectedRecord.data.id]);
        	var filter = getFilter(params);
        	ds.subRequirementStore.loadByCriteria(filter);
    	}else{
    		ds.subRequirementStore.removeAll();
    	}
	},
	
	removeCandidate : function(dat) {
		if (dat=='yes') {
			var record= this.removeRecord;
			app.requirementService.removeCandidate(record.data.id,this.onRemoveCandidate,this);
		}
	},

	removeCandidateConfirm : function(record) {
		this.removeRecord = record;
		if (record.data.id == 0 || record.data.id == null)
			ds.subRequirementStore.remove(record);
		else
			Ext.Msg.confirm("Confirm.", "Do you want to remove selected candidate from the Job Opening?", this.removeCandidate, this);
	},
	
	onRemoveCandidate : function(data) {
		if (data.success) {
	    	var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
	    	app.requirementMgmtPanel.requirementDetailPanel.loadSubRequirments(selectedRecord.data.id);
		}else {
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
	},

    getExcelExport : function() {
    	var values = app.requirementMgmtPanel.requirementsSearchPanel.getValues();
    	var params = new Array();

    	params.push(['candidateId','=', values.candidate]);
    	params.push(['search','=', values.search]);
    	
    	params.push(['jobOpeningStatus','=', values.jobOpeningStatus]);
    	params.push(['module','=', values.module]);

    	var hotness = values.hotJobs.toString();
    	if (hotness != '')
    		hotness = "'"+hotness.replace(/,/g, "','")+"'";
    	params.push(['hotness','=', hotness]);

    	var openings = values.jobOpeningStatus.toString();
    	if (openings != '')
    		openings = "'"+openings.replace(/,/g, "','")+"'";
    	params.push(['jobOpeningStatus','=', openings]);
    	var id = values.id;
    	id = id.replace(',,',",");
    	while (id.length >0) {
        	if (id.substr(id.length-1,id.length) == ",")
        		id = id.substr(0,id.length-1);
			else
				break;
		}
    	if (id.search(',') != -1) 
    		id = "'"+id.replace(/,/g, "','")+"'";

    	params.push(['id','=', id]);
    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;") {
				columns.push(this.columns[i].text);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.requirementService.excelExport(Ext.encode(filter));
	},
	
    addRequirement : function() {
	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.Requirement',{
   	 		id : null,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});

   	 	ds.requirementStore.insert(0, rec);
        this.getView().focusRow(0);
	},
    
    saveRequirements : function() {
    	var records = new Array();
    	for ( var i = 0; i < ds.requirementStore.data.length ; i++) {
			var record = ds.requirementStore.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}
    		if (record.data.candidateId == 0 || record.data.candidateId == null)
				delete record.data.candidateId;
    		
    		if (this.modifiedIds.indexOf(record.data.id) != -1) {
    			records.push(record.data);
			}
		}

    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Requirements to save');
    		return;
		}

    	records = Ext.JSON.encode(records);
    	app.requirementService.saveRequirements(records,this.onSave,this);

	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error',data.errorMessage);
		this.modifiedIds =  new Array();
		app.requirementMgmtPanel.requirementsSearchPanel.search();
	},
	
	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Requirement to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Requirement","Do you to delete selected Requirement?", this.deleteRequirement, this);
		}
	},
	
	deleteRequirement: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.requirementService.deleteRequirement(selectedRecord.data.id, this.onDeleteRequirement, this);
		}
	},
	
	onDeleteRequirement : function(data){
		if (!data.success) { 
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.requirementMgmtPanel.requirementsSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	},

	addClient : function(type) {
		if(app.clientPanel == null){
			app.clientPanel  = new tz.ui.ClientPanel({itemId:'clientPanel'});
			app.centerLayout.add(app.clientPanel);
		}
		app.centerLayout.getLayout().setActiveItem('clientPanel');
		app.clientPanel.clearMessage();
		app.clientPanel.form.reset();
		app.clientPanel.deleteButton.disable();
		app.clientPanel.openedFrom = "Recruit";
		app.clientPanel.vendorTypeCombo.setValue(type);
		d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
		app.clientPanel.form.findField('created').setValue(nd);
	},

	addCandidate : function(type) {
		if(app.candidatePanel == null){
			app.candidatePanel  = new tz.ui.CandidatePanel({itemId:'candidatePanel'});
			app.centerLayout.add(app.candidatePanel);
		}
		app.centerLayout.getLayout().setActiveItem('candidatePanel');
		app.candidatePanel.clearMessage();
		app.candidatePanel.form.reset();
		app.candidatePanel.deleteButton.disable();
		app.candidatePanel.openedFrom = "Recruit";
		d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
		app.candidatePanel.form.findField('created').setValue(nd);
		app.candidatePanel.candidate ="";
		ds.candidate_requirementStore.removeAll();
	},

    getRecruitmentTab : function() {
		if(app.requirementMgmtPanel == null){
			app.requirementMgmtPanel = new tz.ui.RequirementMgmtPanel({itemId:'requirementMgmtPanel'});
			app.centerLayout.add(this.requirementMgmtPanel);
		}    		
		app.centerLayout.getLayout().setActiveItem('requirementMgmtPanel');
		
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.candidateSearchStore.loadByCriteria(filter);

    	params = new Array();
    	params.push(['type','=', 'Client']);
    	filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.clientSearchStore.loadByCriteria(filter);

    	params = new Array();
    	params.push(['type','=', 'Vendor']);
    	filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.vendorStore.loadByCriteria(filter);
		
	},


});

function renderValue(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null || value=="") 
		return 'N/A';
    return value;
}

function employerRenderer(value, metadata, record, rowIndex, colIndex, store) {
	metadata.tdAttr = 'data-qtip="' + value + '"';
	if (value == "TCS") {
		return '<span style="color:green;">' + value + '</span>';
	}
	if (value == "CPS") {
        return '<span style="color:blue;">' + value + '</span>';
    }  
    return 'N/A';
}
function alertRender(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null || value=="") 
    	return 'N/A' ;
    return '<span style="color:red;">' + value + '</span>';
}
Ext.define('tz.ui.RequirementsSearchPanel', {
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    frame:true,
    anchor:'100%',
    autoScroll:true,
//creating view in init function
    initComponent: function() {
        var me = this;

		me.candidateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Candidate',
		    store: ds.candidateSearchStore,
		    queryMode: 'local',
		    //hidden : true,
		    displayField: 'fullName',
		    valueField: 'id',
		    name : 'candidate',
		    tabIndex:6,
		    labelWidth :105,
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});

		me.hotJobsCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Show by hotness',
		    store : ds.hotnessStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'hotJobs',
		    tabIndex:2,
		    labelWidth :105,
		});

		var defaultOpenings = ['01.In-progress','02.Resume Ready','03.Waiting for Approval','04.Hard to Fill'];
		
		me.jobOpeningStatusCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Job Opening Status',
		    store: ds.jobOpeningStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'jobOpeningStatus',
		    tabIndex:3,
		    labelWidth :135,
		    value : defaultOpenings,
		    listConfig: {
		        cls: 'grouped-list'
		      },
		      tpl: Ext.create('Ext.XTemplate',
		        '{[this.currentKey = null]}' +
		        '<tpl for=".">',
		          '<tpl if="this.shouldShowHeader(notes)">' +
		            '<div class="group-header">{[this.showHeader(values.notes)]}</div>' +
		          '</tpl>' +
		          '<div class="x-boundlist-item">{name}</div>',
		        '</tpl>',
		        {
		          shouldShowHeader: function(name){
		            return this.currentKey != name;
		          },
		          showHeader: function(name){
		            this.currentKey = name;
		            switch (name) {
		              case 'Active': return 'Active';
		              case 'Inactive': return 'Inactive';
		            }
		            return 'Other';
		          }
		        }
		      )
		});

        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Search',
	   	                 name: 'search',
	   	                 tabIndex:1
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Module',
	   	                 name: 'module',
	   	                 tabIndex:5
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [me.hotJobsCombo,me.candidateCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [me.jobOpeningStatusCombo,
                     {
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Submitted Date From',
	   	                 name: 'submittedDateFrom',
	   	                 tabIndex:7,
	   	                 labelWidth :135,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Unique Ids (EX:1,2)',
	   	                 name: 'id',
	   	                 labelWidth :115,
	   	                 tabIndex:4
	   	             },{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Submitted Date To',
	   	                 name: 'submittedDateTo',
	   	                 tabIndex:8,
	   	                 labelWidth :115,
	   	             }]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:9,
			},{
				text : 'Reset',
				scope: this,
				handler: this.reset,
				tabIndex:10,
			},{
				text : 'Clear',
				scope: this,
				tabIndex:11,
	             handler: function() {
	            	 this.form.reset();
	            	 me.jobOpeningStatusCombo.setValue(null);
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['candidateId','=', this.candidateCombo.getValue()]);
    	params.push(['search','=', values.search]);
    	
    	var hotness = values.hotJobs.toString();
    	if (hotness != '')
    		hotness = "'"+hotness.replace(/,/g, "','")+"'";
    	params.push(['hotness','=', hotness]);
    	
    	var openings = values.jobOpeningStatus.toString();
    	if (openings != '')
    		openings = "'"+openings.replace(/,/g, "','")+"'";
    	
    	if (values.submittedDateFrom != undefined && values.submittedDateFrom != '') {
    		var submittedDateFrom = new Date(values.submittedDateFrom);
        	params.push(['submittedDateFrom','=',submittedDateFrom]);
    	}

    	if (values.submittedDateTo != undefined && values.submittedDateTo != '') {
    		var submittedDateTo = new Date(values.submittedDateTo);
        	params.push(['submittedDateTo','=',submittedDateTo]);
    	}

    	params.push(['jobOpeningStatus','=', openings]);
    	params.push(['module','=', values.module]);
    	var id = values.id;
    	id = id.replace(',,',",");
    	while (id.length >0) {
        	if (id.substr(id.length-1,id.length) == ",")
        		id = id.substr(0,id.length-1);
			else
				break;
		}
    	if (id.search(',') != -1) 
    		id = "'"+id.replace(/,/g, "','")+"'";

    	params.push(['id','=', id]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=200;
    	ds.requirementStore.loadByCriteria(filter);
    	app.requirementMgmtPanel.requirementsGrid.modifiedIds =  new Array();
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
Ext.define('tz.ui.RequirementDetailPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 10,
    title: '',
    anchor:'100% 95%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'left',labelWidth :100},
    
    initComponent: function() {
        var me = this;
        me.newRequirement = false;
		me.copyRequirement = false;
		me.modified = false;
		me.closeButton = false;
        me.removeRecord = "";
        me.modifiedCandidateIds = new Array();
        me.modifiedResearchIds = new Array();
        me.modifiedVendorIds = new Array();
        me.requirement ="";

        me.vendorStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
        });

        me.hotnessCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Hotness',
		    store: ds.hotnessStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'hotness',
		    typeAhead: true,
		    forceSelection : true,
		    tabIndex:4,
		});
        
		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client',
		    store: ds.clientSearchStore,
		    forceSelection:true,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'name',
		    name : 'client',
		    tabIndex:6,
		});

		me.jobOpeningStatusCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Job Opening Status',
		    store: ds.jobOpeningStatusStore,
		    forceSelection:true,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'jobOpeningStatus',
		    tabIndex:15,
		});

		me.employerCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Employer',
		    store: ds.employerStore,
		    forceSelection:true,		   
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
		    name : 'employer',
		    tabIndex:14,
		});

		me.immigrationCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Immigration Status",
			name: 'immigrationStatus',
			tabIndex:27,
			queryMode: 'local',
			displayField: 'name',
			valueField: 'value',
			store: ds.immigrationTypeStore,
			forceSelection:true,
			allowBlank:true,
		});

		me.qualificationCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Highest Qualification Held',
		    store: ds.qualificationStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    name : 'highestQualification',
		    tabIndex:43,
		});
		
		me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    store: ds.candidateSourceStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    name : 'type',
		    tabIndex:37,
		});
		
		me.currentJobCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Current Job Title",
            name: 'currentJobTitle',
		    store: ds.currentJobStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:6,
		});

		me.marketingStatusCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Marketing Status",
            name: 'marketingStatus',
            store: ds.marketingStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:44,
		});

		me.relocateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Will Relocate",
            name: 'relocate',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:23,
		});

		me.travelCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Trvl?",
            name: 'travel',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:24,
		});

		me.followUpCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Follow Up",
            name: 'followUp',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:38,
		});

		me.openToCTHCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Open To CTH",
            name: 'openToCTH',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:25,
		});

		me.employmentTypeCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
			fieldLabel: "Employment Type",
            name: 'employmentType',
	        tabIndex:28,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.employmentTypeStore,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					this.form.findField('selectedEmploymentType').setValue(combo.rawValue);
				}
		    }
		});
		
		me.deleteButton = new Ext.Button({
            text: 'Delete',
            iconCls : 'btn-delete',
            scope : this,
            tabIndex:20,
            handler: function(){
    			this.deleteConfirm();
    		}
        });

        me.saveAndNewButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & New',
            iconCls : 'btn-save',
            scope: this,
	        handler: function() {
				this.saveAndNewRequirement();
			}
        });

        me.saveAndCopyButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & Copy',
            iconCls : 'btn-save',
            scope: this,
	        handler: function() {
				this.saveAndCopyRequirement();	
			}
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        scope: this,
            	        handler: this.save
                    },
                    '-',me.saveAndNewButton,'-',me.saveAndCopyButton,'-',me.deleteButton,'-',
                    {
                        xtype: 'button',
                        text: 'Close',
                        iconCls : 'btn-close',
                        scope : this,
                        handler: function(){
                        	this.closeConfirm();
                        }
                    }
                ]
            }
        ];

		me.addCandidateButton = new Ext.Button({
    		text: 'Add Existing Candidate',
    		iconCls : 'btn-add',
    		scope: this,
    		handler: this.addCandidate
        });

        me.submittedCandidates = Ext.create('Ext.grid.Panel', {
            title: '',
            frame:true,
            anchor:'98%',
            height:180,
            width:'100%',
            store: ds.subRequirementStore,
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],
            listeners: {
                edit: function(editor, event){
                	var record = event.record;
            		if(event.originalValue != event.value){
            			me.modifiedCandidateIds.push(record.data.id);
            		}
                },
            },
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [me.addCandidateButton,'-',{
            		xtype: 'button',
            		text: 'Save',
            		iconCls : 'btn-save',
            		scope: this,
            		handler: this.saveCandidates
            	},'-',{
            		xtype: 'button',
            		text: 'Reset',
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            				me.modifiedCandidateIds = new Array();
        					me.loadSubRequirments(me.requirementPanel.form.findField('id').getValue());
        				} 
            	}]
            }],

            columns: [
                  {
          			xtype : 'actioncolumn',
        			hideable : false,
        			width : 35,
        			items : [ {
        				icon : 'images/icon_delete.gif',
        				tooltip : 'Remove Candidate from Jop Opening',
        				padding : 10,
        				scope : this,
        				handler : function(grid, rowIndex, colIndex) {
           					this.removeCandidateConfirm(ds.subRequirementStore.getAt(rowIndex));
        				} 
        			} ]
        		},{
                    xtype : 'actioncolumn',
                    hideable : false,
                    width : 35,
                    items : [{
        				icon : 'images/icon_edit.gif',
        				tooltip : 'View Candidate',
        				padding: 50,
        				scope: this,
        				handler : function(grid, rowIndex, colIndex) {
        					if(ds.subRequirementStore.getAt(rowIndex).data.candidateId != null && ds.subRequirementStore.getAt(rowIndex).data.candidateId != 0)
        						me.viewCandidate(ds.subRequirementStore.getAt(rowIndex).data.candidateId);
        				}
        			}]
                },{
      				header : 'Candidate',
    				dataIndex : 'candidateId',
    	            width :  130,
    		        editor: {
    	                xtype: 'combobox',
    	                queryMode: 'local',
    	                triggerAction: 'all',
    	                displayField: 'fullName',
    	                valueField: 'id',
    	                emptyText:'Select...',
    	                listClass: 'x-combo-list-small',
    	                store: ds.candidateSearchStore,
    	            },
                    renderer:function(value, metadata, record){
                    	var candidate = ds.candidateSearchStore.getById(value);
                    	if(candidate){
                    		return candidate.data.fullName;
                    	}else{
                    		return null;
                    	}
                    }
    			},{
    				text : 'Submitted Date',
    				dataIndex : 'submittedDate',
    				width :  80,
    				renderer: dateRender,
    				editor: {
    	                xtype: 'datefield',
    	                format: 'm/d/y'
    	            }
    			},{
      				header : 'Vendor',
    				dataIndex : 'vendorId',
    	            width :  130,
    		        editor: {
    	                xtype: 'combobox',
    	                queryMode: 'local',
    	                triggerAction: 'all',
    	                displayField: 'name',
    	                valueField: 'id',
    	                emptyText:'Select...',
    	                listClass: 'x-combo-list-small',
    	                store: me.vendorStore,
    	            },
                    renderer:function(value, metadata, record){
                    	var candidate = ds.vendorStore.getById(value);
                    	if(candidate)
                    		return candidate.data.name;
                    	else
                    		return null;
                    }
    			},{
                    dataIndex: 'candidateId',
                    text: 'Expected Rate',
                    width :  80,
                    renderer:function(value, metadata, record){
                    	var candidate = ds.candidateSearchStore.getById(value);
                    	if(candidate){
                    		return candidate.data.expectedRate;
                    	}else{
                    		return null;
                    	}
                    }
                },{
                    dataIndex: 'candidateId',
                    text: 'Homeloc',
                    renderer:function(value, metadata, record){
                    	var candidate = ds.candidateSearchStore.getById(value);
                    	if(candidate){
                    		return candidate.data.cityAndState;
                    	}else{
                    		return null;
                    	}
                    }
                },{
                    dataIndex: 'candidateId',
                    text: 'Will Relocate',
                    width :  80,
                    renderer:function(value, metadata, record){
                    	var candidate = ds.candidateSearchStore.getById(value);
                    	if(candidate){
                    		if (candidate.data.relocate == "YES") {
                    			return '<span style="color:#87E320;">' + candidate.data.relocate + '</span>';
                    		} 
                    		if (candidate.data.relocate == "NO") {
                    			return '<span style="color:red;">' + candidate.data.relocate + '</span>';
                    		}
                    	}else{
                    		return null;
                    	}
                    },
                },{
                    dataIndex: 'candidateId',
                    text: 'Trvl?',
                    width :  80,
                    renderer:function(value, metadata, record){
                    	var candidate = ds.candidateSearchStore.getById(value);
                    	if(candidate){
                    		if (candidate.data.travel == "YES") {
                    			return '<span style="color:#87E320;">' + candidate.data.travel + '</span>';
                    		} 
                    		if (candidate.data.travel == "NO") {
                    			return '<span style="color:red;">' + candidate.data.travel + '</span>';
                    		}
                    	}else{
                    		return null;
                    	}
                    }
                },{
                    dataIndex: 'candidateId',
                    text: 'Availability',
                    width :  80,
                    renderer:function(value, metadata, record){
                    	var candidate = ds.candidateSearchStore.getById(value);
                    	if(candidate){
                    		return Ext.Date.format(candidate.data.availability,'m/d/Y');
                    	}else{
                    		return null;
                    	}
                    }
                }
            ],
        });

        me.submittedVendors = Ext.create('Ext.grid.Panel', {
            title: '',
            frame:true,
            anchor:'98%',
            height:180,
            width:'100%',
            store: ds.subVendorStore,
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],

            listeners: {
                edit: function(editor, event){
                	var record = event.record;
            		if(event.originalValue != event.value){
            			me.modifiedVendorIds.push(record.data.id);
            		}
                    if (editor.context.field == 'contactId') {
                    	var contact = ds.contactSearchStore.getById(record.data.contactId);
                    	if (contact != null && contact.data.clientId != record.data.vendorId)
                    		record.data.contactId = null;
                    	ds.contactSearchStore.clearFilter(true);
                    }else if (editor.context.field == 'vendorId') {
                    	record.data.contactId = null;
					}
                    me.submittedVendors.getView().refresh();
                },
                beforeedit: function(editor, event) {
              		var record = event.record;
              		if (event.column.dataIndex == "vendorId") {
              			if (record.data.vendorId != '' && ds.subRequirementStore.findExact('vendorId',record.data.vendorId) != -1) {
              				me.updateError('Candidate associated to this vendor. You can not update vendor');
              				return false;
						}
              		}
              		if (event.column.dataIndex == "contactId") {
              			ds.contactSearchStore.clearFilter(true);
              			//ds.contactSearchStore.filter(new Ext.util.Filter({property:"clientId", value:record.data.vendorId}));
              			//ds.contactSearchStore.filter([{filterFn: function(item) { return item.get('clientId') == record.data.vendorId; }}]);
              			ds.contactSearchStore.filterBy(function(rec) {
              			    return rec.get('clientId') === record.data.vendorId;
              			});
                    	me.submittedVendors.getView().refresh();
              			return true;
  					}
                }
            },
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Add Vendor',
            		iconCls : 'btn-add',
            		scope: this,
            		handler: this.addVendor
            	},'-',{
            		xtype: 'button',
            		text: 'Save',
            		iconCls : 'btn-save',
            		scope: this,
            		handler: this.saveVendors
            	},'-',{
            		xtype: 'button',
            		text: 'Reset',
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            				me.modifiedVendorIds = new Array();
        					me.loadSubVendors(me.requirementPanel.form.findField('id').getValue());
        				} 
            	}]
            }],
            columns: [
                  {
          			xtype : 'actioncolumn',
        			hideable : false,
        			width : 35,
        			items : [ {
        				icon : 'images/icon_delete.gif',
        				tooltip : 'Remove Vendor from Jop Opening',
        				padding : 10,
        				scope : this,
        				handler : function(grid, rowIndex, colIndex) {
        					this.removeVendorConfirm(ds.subVendorStore.getAt(rowIndex));
        				} 
        			} ]
        		},{
      				header : 'Vendor',
    				dataIndex : 'vendorId',
    	            width :  150,
    		        editor: {
    	                xtype: 'combobox',
    	                queryMode: 'local',
    	                triggerAction: 'all',
    	                displayField: 'name',
    	                valueField: 'id',
    	                emptyText:'Select...',
    	                listClass: 'x-combo-list-small',
    	                store: ds.vendorStore,
    	            },
                    renderer:function(value, metadata, record){
                    	var candidate = ds.vendorStore.getById(value);
                    	if(candidate)
                    		return candidate.data.name;
                    	else
                    		return null;
                    }
    			},{
    				header : 'Contact Person',
    				dataIndex : 'contactId',
    	            width :  150,
    		        editor: {
    	                xtype: 'combobox',
    	                queryMode: 'local',
    	                triggerAction: 'all',
    	                displayField: 'fullName',
    	                valueField: 'id',
    	                editable: false,
    	                emptyText:'Select...',
    	                listClass: 'x-combo-list-small',
    	                store: ds.contactSearchStore,
    	            },
                    renderer:function(value, metadata, record){
                    	var contact = ds.contactSearchStore.getById(value);
                    	if(contact)
                    		return contact.data.fullName;
                    	else
                    		return null;
                    }
    			}
            ],
        });
        
        me.researchGridPanel = Ext.create('Ext.grid.Panel', {
            title: '',
            frame:true,
            anchor:'98%',
            height:250,
            width:'100%',
            store: ds.researchStore,
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],
            listeners: {
                edit: function(editor, event){
                	var record = event.record;
            		if(event.originalValue != event.value){
            			me.modifiedResearchIds.push(record.data.id);
            		}
                },
            },

            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Add New Task',
            		iconCls : 'btn-add',
            		scope: this,
            		handler: this.addTask
            	},'-',{
            		xtype: 'button',
            		text: 'Delete Task',
            		iconCls : 'btn-delete',
            		scope: this,
            		handler: this.deleteTaskConfirm
            	}]
            }],
            
            columns: [
                  {
                    xtype: 'gridcolumn',
                    dataIndex: 'task',
                    text: 'Task',
                    renderer: renderValue,
                    width:150,
                    editor: {
                        xtype: 'textarea',
                        maxLength:300,
                        height : 60,
       	                enforceMaxLength:true,
                    }
                },{
    				text : 'Status',
    				dataIndex : 'status',
    				renderer: renderValue,
    				width:70,
                    editor: {
    	                xtype: 'combobox',
    	                queryMode: 'local',
    	                typeAhead: true,
    	                triggerAction: 'all',
    	                selectOnTab: true,
    	                forceSelection:true,
    	                displayField: 'value',
    	                valueField: 'name',
    	                emptyText:'Select...',
    	    		    store : ds.researchStatusStore,
    	                lazyRender: true,
    	                listClass: 'x-combo-list-small'
    	            }
    			},{
    				text : 'Date',
    				dataIndex : 'date',
    				width :  50,
    				renderer: dateRender,
    				editor: {
    	                xtype: 'datefield',
    	                format: 'm/d/y'
    	            }
    			},{
	                dataIndex: 'comments',
	                text: 'Comments',
	                renderer: renderValue,
	                width:250,
	                editor: {
	                    xtype: 'textarea',
	                    height : 60,
	                    maxLength:300,
	   	                enforceMaxLength:true,
	                }
	            },{
	                dataIndex: 'potentialCandidates',
	                text: 'Potential Candidates',
	                renderer: renderValue,
	                width:100,
	                editor: {
	                    xtype: 'textfield',
	                    maxLength:150,
	   	                enforceMaxLength:true,
	                }
	            },{
					text : 'Last Updated User',
					dataIndex : 'lastUpdatedUser',
					width:90,
					renderer:function(value, metadata, record){
	               		metadata.tdAttr = 'data-qtip="' + value + '"';
	                	return value;
	                }
				},{
	                xtype: 'datecolumn',
	                dataIndex: 'created',
	                text: 'Created',
	                format: "m/d/Y H:i:s A",
	                width:100,
	                hidden : true
	            },{
	                xtype: 'datecolumn',
	                dataIndex: 'lastUpdated',
	                format: "m/d/Y H:i:s A",
	                text: 'Last Updated',
	                width:100,
	                hidden : true
	            }
            ],
        });
        
		me.requirementPanel = Ext.create('Ext.form.Panel', {
        	bodyPadding: 10,
        	border : 0,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 100},
        	xtype: 'container',
	        
	         items:[
	                {
	   	   	    	 xtype:'fieldset',
	   	   	    	 title : 'Job Opening Details',
		   	         collapsible: false,
		   	         layout: 'column',
		   	         items :[{
		   	             xtype: 'container',
		   	             columnWidth:.9,
		   	             items: [
		   	                     {
		   	                xtype:'displayfield',
		   	    	        fieldLabel: 'Job Id',
		   	    	        readOnly :true,	
		   	    	        name: 'id'
		   	    	    },{
		   	                 xtype:'datefield',
		   	                 fieldLabel: 'Posting Date',
		   	                 name: 'postingDate',
		   	                 tabIndex:1,
		   	             },{
		   	                 xtype:'textfield',
		   	                 fieldLabel: 'Posting Title',
		   	                 name: 'postingTitle',
		   	                 tabIndex:2,
		   	                 maxLength:100,
		   	                 enforceMaxLength:true,
		   	             },{
		   	                 xtype:'textfield',
		   	                 fieldLabel: 'Location',
		   	                 name: 'location',
		   	                 tabIndex:3,
		   	                 maxLength:100,
		   	                 enforceMaxLength:true,
		   	             }
		   	             ,me.hotnessCombo,
		   	             {
		   	                 xtype:'textfield',
		   	                 fieldLabel: 'Module',
		   	                 name: 'module',
		   	                 tabIndex:5,
		   	                 maxLength:50,
		   	                 enforceMaxLength:true,
		   	             },
		   	             me.clientCombo,
		   	             {
		   	                 xtype:'textfield',
		   	                 fieldLabel: 'Phone No',
		   	                 name: 'phoneNo',
		   	                 maskRe :  /[0-9]/,
		   	                 tabIndex:9,
		   	                 maxLength:15,
		   	                 enforceMaxLength:true,
		   	                 listeners:{
		   	                	 change : function(field,newValue,oldValue){
		   	                		 var value = me.form.findField('phoneNo').getValue();
		   	                		 if (value != "" ) {
		   	                			 value = value.replace(/[^0-9]/g, "");
			   	                		 value = value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
			   	                		 me.form.findField('phoneNo').setValue(value);
		   	                		 }
							    }
	 			            }
		   	             },{
		   	                 xtype:'textfield',
		   	                 fieldLabel: "Alert",
		   	                 name: 'addInfo_Todos',
		   	                 fieldStyle: "color: red;",
		   	                 tabIndex:11,
		   	                 maxLength:45,
		   	                 enforceMaxLength:true,
		   	             },{
		   	                 xtype:'textfield',
		   	                 fieldLabel: 'Source',
		   	                 name: 'source',
		   	                 tabIndex:12,
		   	                 maxLength:45,
		   	                 enforceMaxLength:true,
		   	             },{
		   	                 xtype:'textfield',
		   	                 fieldLabel: 'Rate Submitted',
		   	                 name: 'rateSubmitted',
		   	                 tabIndex:13,
		   	                 maxLength:75,
		   	                 enforceMaxLength:true,
		   	                 
		   	             },
		   	             me.employerCombo,me.jobOpeningStatusCombo,
		   	             {
		   	            	 xtype:'textfield',
		   	                 fieldLabel: 'Public Link',
		   	                 name: 'publicLink',
		   	                 vtype:'url',
		   	                 tabIndex:16,
		   	                 maxLength:200,
		   	                 enforceMaxLength:true,
		   	             },{
		   	                 xtype:'textarea',
		   	                 fieldLabel: 'Roles and Responsibilities',
		   	                 name: 'requirement',
		   	                 tabIndex:17,
		   	                 maxLength:4000,
		   	                 width:600,
		   	                 height:170,
		   	                 enforceMaxLength:true,
		   	                 listeners:{
		   	                	 change:function(field) {
		   	                		 while (field.value.indexOf('"') != -1) {
		   	                			field.value = field.value.replace('"','');	
		   	                		 }
		   	                		 field.setValue(field.value);
		   	                	 }		    
		   	                 }
		   	             },{
		   	                 xtype:'textarea',
		   	                 fieldLabel: 'Comments',
		   	                 name: 'comments',
		   	                 tabIndex:18,
		   	                 maxLength:300,
		   	                 width:400,
		   	                 height:70,
		   	                 enforceMaxLength:true,
		   	             },{
		   	     	        xtype: 'datefield',
		   	    	        hidden:true,
		   	    	        name: 'created'
		   	    	    },{
		   	                 xtype:'textarea',
		   	                 name: 'researchComments',
		   	                 hidden:true,
		   	             }
		   	             ]
		   	         }]
		   	     }]
	     });
		
        me.candidatePanel = Ext.create('Ext.form.Panel', {
        	title : 'Add Candidate',
        	bodyPadding: 10,
        	border : 1,
        	frame:false,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 160},
        	xtype: 'container',
        	dockedItems :  [{
	        	 xtype: 'toolbar',
	        	 dock: 'top',
	        	 items: [{
	        		 xtype: 'button',
	        		 text: 'Save Candidate',
	        		 iconCls : 'btn-save',
	        		 scope: this,
	        		 handler: this.saveCandidate
	        	 },'-',{
	        		 xtype: 'button',
	        		 text: 'Reset',
	        		 scope: this,
	        		 handler: function(){
                     	this.candidatePanel.form.reset();
                     }
	        	 }]
            }],
        	
	         items:[
	                {
				xtype : 'fieldset',
				title : 'Important Information',
				collapsible : true,
				defaultType : 'textfield',
				layout : 'column',
				items : [
				         {
	             xtype: 'container',
	             columnWidth:.9,
	             items: [{
   	                 xtype:'textfield',
   	                 fieldLabel: 'First Name',
   	                 name: 'firstName',
   	                 allowBlank:false,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	                 tabIndex:19,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: 'Last Name',
   	                 name: 'lastName',
   	                 allowBlank:false,
   	                 tabIndex:20,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "City & State",
   	                 name: 'cityAndState',
   	                 tabIndex:21,
   	                 maxLength:90,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "Expected Rate",
   	                 name: 'expectedRate',
   	                 tabIndex:22,
   	                 maxLength:100,
   	                 enforceMaxLength:true,
   	             },
   	             me.relocateCombo,me.travelCombo,me.openToCTHCombo,
   	             {
   	                 xtype:'datefield',
   	                 fieldLabel: 'Availability',
   	                 name: 'availability',
   	                 tabIndex:26,
   	             },
   	             me.immigrationCombo,me.employmentTypeCombo,
   	             {
   	                 xtype:'textarea',
   	                 fieldLabel: "Selected Employment Type",
   	                 name: 'selectedEmploymentType',
   	                 readOnly : true,
   	                 width : 500,
   	                 height : 40,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: "About Partner",
   	                 name: 'aboutPartner',
   	                 tabIndex:29,
   	                 grow : true,
   	                 maxLength:200,
   	                 enforceMaxLength:true,
   	                 width : 500,
   	                 height : 50,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: "Personality",
   	                 name: 'personality',
   	                 tabIndex:30,
   	                 maxLength:300,
   	                 enforceMaxLength:true,
   	                 width : 500,
   	                 height : 50,
   	             }]
	         }]
	        },{
				xtype : 'fieldset',
				title : 'Other Details',
				collapsible : true,
				collapsed : true,
				defaultType : 'textfield',
				layout : 'column',
				items : [
				         {
	             xtype: 'container',
	             columnWidth:.9,
	             items: [{
   	                 xtype:'textfield',
   	                 fieldLabel: 'Email',
   	                 name: 'emailId',
   	                 vtype:'email',
   	                 tabIndex:31,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: 'Contact Number',
   	                 name: 'contactNumber',
   	                 maskRe :  /[0-9]/,
   	                 tabIndex:32,
   	                 maxLength:15,
   	                 enforceMaxLength:true,
   	                 listeners:{
   	                	 change : function(field,newValue,oldValue){
   	                		 var value = me.form.findField('contactNumber').getValue();
   	                		 if (value != "" ) {
   	                			 value = value.replace(/[^0-9]/g, "");
	   	                		 value = value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
	   	                		 me.form.findField('contactNumber').setValue(value);
   	                		 }
					    }
		            }
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: 'Contact Address',
   	                 name: 'contactAddress',
   	                 tabIndex:33,
   	                 maxLength:200,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "Current Job Title",
   	                 name: 'currentJobTitle',
   	                 tabIndex:34,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'datefield',
   	                 fieldLabel: 'StartDate',
   	                 name: 'startDate',
   	                 tabIndex:35,
   	             },{
   	                 xtype:'datefield',
   	                 fieldLabel: 'Submitted Date',
   	                 name: 'submittedDate',
   	                 hidden :true,
   	                 tabIndex:36,
   	             },
   	             me.typeCombo,me.followUpCombo,
   	             {
   	                 xtype:'textfield',
   	                 fieldLabel: "Referral",
   	                 name: 'referral',
   	                 tabIndex:39,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'numberfield',
   	                 fieldLabel: 'Priority',
   	                 name: 'priority',
   	                 tabIndex:40,
   	                 value :0,
   	                 minValue: 0,
   	                 maxValue: 100
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "Source",
   	                 name: 'source',
   	                 tabIndex:41,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "Employer",
   	                 name: 'employer',
   	                 tabIndex:42,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },
   	             me.qualificationCombo,me.marketingStatusCombo,
   	             {
   	                 xtype:'textfield',
   	                 fieldLabel: "Education",
   	                 name: 'education',
   	                 tabIndex:45,
   	                 maxLength:150,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: "Skill Set",
   	                 name: 'skillSet',
   	                 tabIndex:46,
   	                 maxLength:2500,
   	                 enforceMaxLength:true,
   	                 width : 600,
	                 height : 120,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: "Comments",
   	                 name: 'comments',
   	                 tabIndex:47,
   	                 maxLength:1000,
   	                 enforceMaxLength:true,
   	                 width : 600,
   	                 height : 120,
   	             },{
 					xtype : 'datefield',
	            	 name : 'created',
	            	 hidden : true
				}]
	         }]
	        }]
	     });

        me.items = [this.getMessageComp(), this.getHeadingComp(),
            {
	   	    	 xtype:'fieldset',
	   	         collapsible: false,
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [me.requirementPanel]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [{
	   	            	 xtype: 'fieldset',
	   	            	 title:'Submitted Candidates',
	   	            	 id:'Submitted_Candidates',
	   	            	 //collapsed : true,
	   	            	 collapsible: true,
	   	            	 columnWidth:.99,
	   	            	 items: [{
	   	            		 xtype: 'container',
	   	            		 columnWidth:1,
	   	            		 items: [me.submittedCandidates]
	   	            	 }]
	   	             },{
	   	            	 xtype: 'fieldset',
	   	            	 title:'Submitted Vendors',
	   	            	 //collapsed : true,
	   	            	 collapsible: true,
	   	            	 columnWidth:.99,
	   	            	 items: [{
	   	            		 xtype: 'container',
	   	            		 columnWidth:1,
	   	            		 items: [me.submittedVendors]
	   	            	 }]
	   	             },{
	   	            	 xtype: 'fieldset',
	   	            	 title:'Add Candidate',
	   	            	 collapsible: true,
	   	            	 collapsed : true,
	   	            	 columnWidth:.99,
	   	            	 items: [{
	   	            		 xtype: 'container',
	   	            		 columnWidth:1,
	   	            		 items: [me.candidatePanel]
	   	            	 }]
	   	             },{
	   	            	 xtype: 'fieldset',
	   	            	 title:'Add Research Tasks',
	   	            	 collapsible: true,
	   	            	 columnWidth:.99,
	   	            	 items: [{
	   	            		 xtype: 'container',
	   	            		 columnWidth:1,
	   	            		 items: [{
			   	                 xtype:'textarea',
			   	                 fieldLabel: 'Overall Research Comments',
			   	                 name: 'overallResearchComments',
			   	                 tabIndex:18,
			   	                 maxLength:300,
			   	                 width:400,
			   	                 height:70,
			   	                 enforceMaxLength:true,
			   	                 listeners:{
			   	                	 change:function(field) {
			   	                		 me.requirementPanel.form.findField('researchComments').setValue(field.value);
			   	                	 }		    
			   	                 }
			   	             },
			   	             me.researchGridPanel
			   	             ]
	   	            	 }]
	   	             }]
	   	         }]
	   	     }
        ];
        me.callParent(arguments);
    },

    addVendor : function() {
    	var requirementId = this.requirementPanel.form.findField('id').getValue();
    	if (requirementId == null || requirementId == '') {
    		this.updateError("Please save Job openinig");
    		return false;
		}
    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId
   	 	});
   	 	ds.subVendorStore.insert(0, rec);
        this.submittedVendors.getView().focusRow(0);
	},
	
	addCandidate : function() {
    	var requirementId = this.requirementPanel.form.findField('id').getValue();
    	if (requirementId == null || requirementId == '') {
    		this.updateError("Please save Job openinig");
    		return false;
		}
    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId,
   	 		submittedDate : new Date()
   	 	});
   	 	ds.subRequirementStore.insert(0, rec);
        this.submittedCandidates.getView().focusRow(0);
	},
    
	saveVendors : function() {
		for ( var i = 0; i < ds.subVendorStore.data.length; i++) {
			var vendor1 = ds.subVendorStore.getAt(i);
			for ( var j = 0; j < ds.subVendorStore.data.length; j++) {
				var vendor2 = ds.subVendorStore.getAt(j);
				if (vendor1 != vendor2 && vendor1.data.vendorId == vendor2.data.vendorId) {
					this.updateError("Vendor already exists");
					return false;
				}
			}
		}
		
    	var records = new Array();
    	
    	for ( var i = 0; i < ds.subVendorStore.data.length ; i++) {
			var record = ds.subVendorStore.getAt(i);
			
			if (record.data.id == 0 || record.data.id == null || this.modifiedVendorIds.indexOf(record.data.id) != -1) {
	    		if (record.data.id == 0 || record.data.id == null)
	    			delete record.data.id;
	    		if (record.data.vendorId == 0 || record.data.vendorId == null || record.data.vendorId == ''){
					this.updateError("Please select the vendor");
					return false;
				}
	    		if (record.data.contactId == 0 || record.data.contactId == null || record.data.contactId == '')
					delete record.data.contactId;
	    		
    			records.push(record.data);
			}
		}
    	if (records.length == 0) {
    		this.updateError("There are no changes to submit");
		}else{
	    	records = Ext.JSON.encode(records);
	    	app.requirementService.saveSubVendors(records,this.onSaveVendors,this);
		}
	},
	
	onSaveVendors : function(data) {
		this.clearMessage();
		if (data.success) {
			this.modifiedVendorIds = new Array();
			this.loadSubVendors(this.requirementPanel.form.findField('id').getValue());
			this.updateMessage('Vendors Submitted Successfully');
		}else {
			this.updateError('Save Failed');
			this.body.scrollTo('top', 0);
		}	
	},
	
    saveCandidates : function() {
    	var records = new Array();
    	for ( var i = 0; i < ds.subRequirementStore.data.length ; i++) {
			var record = ds.subRequirementStore.getAt(i);
			
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				if (record.data.candidateId == 0 || record.data.candidateId == null || record.data.candidateId == ''){
					this.updateError("Please select a candidate");
					return false;
				}
				if (record.data.vendorId == 0 || record.data.vendorId == null || record.data.vendorId == ''){
					this.updateError("Please select the vendor for the candidate");
					return false;
				}
				for ( var j = 0; j < ds.subRequirementStore.getCount(); j++) {
					var oldRec = ds.subRequirementStore.getAt(j);
					if (oldRec != record && oldRec.data.vendorId == record.data.vendorId && oldRec.data.candidateId == record.data.candidateId) {
						this.updateError("Candidate already submitted through given vendor.");
						return false;
					}
				}
	    		if (record.data.id == 0 || record.data.id == null || record.data.id == '')
					delete record.data.id;
				records.push(record.data);
			}
		}
    	if (records.length == 0) {
    		this.updateError("There are no changes in candidates to submit");
		}else{
        	records = Ext.JSON.encode(records);
        	app.requirementService.saveSubRequirements(records,this.onSaveCandidates,this);
		}
	},
	
	onSaveCandidates : function(data) {
		this.clearMessage();
		if (data.success) {
			this.modifiedCandidateIds = new Array();
			this.loadSubRequirments(this.requirementPanel.form.findField('id').getValue());
			this.updateMessage('Candidates Saved Successfully');
		}else {
			this.updateError('Save Failed');
			this.body.scrollTo('top', 0);
		}	
	},

	deleteTaskConfirm : function()
	{
		var selectedRecord = this.researchGridPanel.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Task to delete.');
		} else if (selectedRecord.data.id == null){
			this.researchGridPanel.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Task","Do you delete selected Task?", this.deleteTask, this);
		}
	},
	
	deleteTask: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.researchGridPanel.getView().getSelectionModel().getSelection()[0];
			app.requirementService.deleteTask(selectedRecord.data.id, this.onDeleteTask, this);
		}
	},
	
	onDeleteTask : function(data){
		this.clearMessage();
		if (!data.success) { 
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			this.loadTasks(this.requirementPanel.form.findField('id').getValue());
			Ext.Msg.alert('Success','Task Deleted Successfully');
		}	
	},
	
    saveTask : function() {
    	var records = new Array();
    	
    	for ( var i = 0; i < ds.researchStore.data.length ; i++) {
			var record = ds.researchStore.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}
    		if (record.data.requirementId == 0 || record.data.requirementId == null)
				delete record.data.requirementId;
    		
    		if (this.modifiedResearchIds.indexOf(record.data.id) != -1) {
    			records.push(record.data);
			}
		}

    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Tasks to save');
    		return;
		}

    	records = Ext.JSON.encode(records);
    	app.requirementService.saveTask(records,this.onSaveTask,this);
    	
	},
    
	onSaveTask : function(data) {
		this.clearMessage();
		if (data.success) {
			this.loadTasks(this.requirementPanel.form.findField('id').getValue());
			this.updateMessage('Saved Successfully');
		}else {
			this.updateError('Save Failed');
			this.body.scrollTo('top', 0);
		}	
	},
	
    addTask : function() {
		if (this.requirementPanel.form.findField('id').getValue() == null) {
			this.updateError('Please Save the Job opening before linking.');
			this.body.scrollTo('top', 0);
			return false;
		}

	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
	    var reqId= this.requirementPanel.form.findField('id').getValue();
   	 	var rec = Ext.create( 'tz.model.Research',{
   	 		id : null,
   	 		date : nd,
   	 		requirementId: reqId,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});

        this.researchGridPanel.store.insert(0, rec);
        this.researchGridPanel.getView().focusRow(0);
	},

	removeVendorConfirm : function(record) {
		this.removeRecord = record;
		if (record.data.id == 0 || record.data.id == null){
			ds.subVendorStore.remove(record);
		}else{
			for ( var i = 0; i < ds.subRequirementStore.getCount(); i++) {
				var rec = ds.subRequirementStore.getAt(i);
				if (rec.data.vendorId == record.data.vendorId && record.data.candidateId != 0) {
					this.updateError('Candidates associated to selected Vendor.');
					return false;
				}
			}
			Ext.Msg.confirm("Confirm.", "Do you want to remove selected vendor from the Job Opening?", this.removeVendor, this);
		}
	},

	removeVendor : function(dat) {
		if (dat=='yes') {
			var record= this.removeRecord;
			var isVal  = record.data.id;
			app.requirementService.removeVendor(isVal,this.onRemoveVendor,this);
		}
	},
	
	onRemoveVendor : function(data) {
		this.clearMessage();
		if (data.success) {
			this.modified = true;
			this.loadSubVendors(this.requirementPanel.form.findField('id').getValue());
		}else {
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
	},
	
	removeCandidate : function(dat) {
		if (dat=='yes') {
			var record= this.removeRecord;
			app.requirementService.removeCandidate(record.data.id,this.onRemoveCandidate,this);
		}
	},

	removeCandidateConfirm : function(record) {
		this.removeRecord = record;
		if (record.data.id == 0 || record.data.id == null)
			ds.subRequirementStore.remove(record);
		else
			Ext.Msg.confirm("Confirm.", "Do you want to remove selected candidate from the Job Opening?", this.removeCandidate, this);
	},
	
	onRemoveCandidate : function(data) {
		this.clearMessage();
		if (data.success) {
			this.modified = true;
			this.loadSubRequirments(this.requirementPanel.form.findField('id').getValue());
		}else {
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
	},
	
    saveCandidate : function() {
		this.clearMessage();
		if(! this.candidatePanel.form.isValid()){
			this.updateError('Please fix the errors.');
			return false;
		}
		var itemslength = this.candidatePanel.form.getFields().getCount();
   		var i = 0;    		
   		var candidate = {};
   		
   		while (i < itemslength) {
   			var fieldName = this.candidatePanel.form.getFields().getAt(i).name;
			candidate[fieldName] = this.candidatePanel.form.findField(fieldName).getValue();
   			i = i + 1;
   		}
   		candidate['employmentType'] = this.employmentTypeCombo.getValue().toString();
   		delete candidate.selectedEmploymentType;
		var candidateObj = Ext.JSON.encode(candidate);
		app.candidateService.saveCandidate(candidateObj, this.onSaveCandidate, this);
	},

	onSaveCandidate: function(data){
		this.clearMessage();
		if(!data.success){
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}else{
			this.candidatePanel.form.reset();
	    	var params = new Array();
	    	var filter = getFilter(params);
	    	filter.pageSize=1000;
	    	ds.candidateSearchStore.loadByCriteria(filter);
		}
		this.body.scrollTo('top', 0);
	},

    loadForm : function(record) {
    	this.modified = false;
    	this.closeButton = false;
    	this.modifiedCandidateIds = new Array();
    	this.requirement = record;
    	this.clearMessage();
    	this.form.reset();
		this.loadRecord(record);
		this.form.findField('overallResearchComments').setValue(record.data.researchComments);

		/*if (ds.req_candidateStore.getCount() >0)
			Ext.getCmp('Submitted_Candidates').expand();
		else
			Ext.getCmp('Submitted_Candidates').collapse();*/
		this.deleteButton.enable();
		this.loadTasks(record.data.id);
		this.loadSubRequirments(record.data.id);
		this.loadSubVendors(record.data.id);
	},
    
	loadSubVendors : function(requirementId) {
    	if (requirementId != null && requirementId != ""){
        	var params = new Array();
        	params.push(['requirementId','=', requirementId]);
        	var filter = getFilter(params);
       		this.vendorStore.removeAll();
        	ds.subVendorStore.loadByCriteria(filter);
    	}else{
    		ds.subVendorStore.removeAll();
       		this.vendorStore.removeAll();
    	}
    	
    	ds.subVendorStore.on('load', function(store, records, options) {
    		var vendorData= new Array();
        	for ( var i = 0; i < records.length; i++) {
				var rec = records[i];
				var vendorName = (ds.vendorStore.getById(rec.data.vendorId)).data.name;
				if(this.vendorStore.getById(rec.data.vendorId)==null)
					vendorData.push({"id":rec.data.vendorId,"name":vendorName});
			}
        	if(vendorData.length >0 ){
        		this.vendorStore.add(vendorData);
        	}        		
    	}, this);
	},
	
	loadSubRequirments : function(requirementId) {
    	if (requirementId != null && requirementId != ""){
        	var params = new Array();
        	params.push(['requirementId','=', requirementId]);
        	var filter = getFilter(params);
        	ds.subRequirementStore.loadByCriteria(filter);
    	}else{
    		ds.subRequirementStore.removeAll();
    	}
	},
	
	loadTasks : function(requirementId) {
    	if (requirementId != null && requirementId != ""){
        	var params = new Array();
        	params.push(['requirementId','=', requirementId]);
        	var filter = getFilter(params);
        	ds.researchStore.loadByCriteria(filter);
    	}else
    		ds.researchStore.removeAll();
	},
	
	checkForDirty : function() {
		if (this.requirement == "")
			this.closeForm();

		var formDirty = false;
		var itemslength = this.requirementPanel.form.getFields().getCount();
		var i = 0;
		while (i < itemslength) {
			var fieldName = this.requirementPanel.form.getFields().getAt(i).name;
			value = this.requirementPanel.form.findField(fieldName).getValue();
			
			if (fieldName == 'created' || fieldName == 'candidateId' ) {
			}else if (fieldName == 'postingDate') {
				if (Ext.Date.format(new Date(this.requirement.data[fieldName]),'m/d/y') != Ext.Date.format(value,'m/d/y')) {
					formDirty = true;
				}
			}else{
				if (value == null || this.requirement.data[fieldName] == null) {
					if (value == null && this.requirement.data[fieldName] != null && this.requirement.data[fieldName] == "") {
						//if value is null and previous value is not null previous value must be empty string
					}else if (this.requirement.data[fieldName] == null && value != null && value == "") {
						//if previous value is null and value is not null then value must be empty string
					}else if (value == null && this.requirement.data[fieldName] == null) {
						// if both are null no issue
					}else{
						formDirty = true;
					}
				}else if (this.requirement.data[fieldName].toString() != value.toString()) {
					formDirty = true;
				}
			}
			
			i = i+1;
		}

    	var tasks = new Array();
    	for ( var i = 0; i < ds.researchStore.data.length ; i++) {
			var record = ds.researchStore.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			tasks.push(record.data);
    		}
    		if (record.data.requirementId == 0 || record.data.requirementId == null)
				delete record.data.requirementId;
    		
    		if (this.modifiedResearchIds.indexOf(record.data.id) != -1) {
    			tasks.push(record.data);
			}
		}
    	if (tasks.length >0)
    		formDirty = true;

		if (formDirty) {
			Ext.MessageBox.show({
				title:'Confirm',
	            msg: "Do you want to save Job opening changes?",
	            buttonText: {                        
	                yes: 'Save and Close',
	                no: 'Close without Saving'
	            },
	            icon: Ext.MessageBox.INFO,
	            fn: function(btn) {
	            	if (btn == 'yes') {
	            		app.requirementMgmtPanel.requirementDetailPanel.save();
	            	}else if (btn == 'no') {
	            		app.requirementMgmtPanel.requirementDetailPanel.closeForm();
					}
	            }
	        });
		}else
			this.closeForm();
	},
	
	closeConfirm : function(){
		
		if (this.candidatePanel.form.findField('firstName').getValue() != '') {
			Ext.Msg.alert('Warning','Some Candidate information is updated, please save them before closing.');
			return false;
		}
    	var vendors = new Array();
    	for ( var i = 0; i < ds.subVendorStore.data.length ; i++) {
			var record = ds.subVendorStore.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				vendors.push(record.data);
			}
		}
    	if (vendors.length >0){
			Ext.Msg.alert('Warning','Please save or reset submitted vendors before closing.');
			return false;
		}
	
    	var candidates = new Array();
    	for ( var i = 0; i < ds.subRequirementStore.data.length ; i++) {
			var record = ds.subRequirementStore.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				candidates.push(record.data);
			}
		}
    	if (candidates.length >0){
			Ext.Msg.alert('Warning','Please save or reset submitted candidates before closing.');
			return false;
		}
    	this.closeButton = true;
		this.checkForDirty();
	},
	
	closeForm: function(){
		if (this.modified) {
			app.requirementMgmtPanel.closeForm();	
		}else
			app.requirementMgmtPanel.getLayout().setActiveItem(0);
	},

	saveAndNewRequirement : function() {
		this.newRequirement = true;
		this.save();
	},
	
	saveAndCopyRequirement : function() {
		this.copyRequirement = true;
		this.save();
	},
	
    save : function(){
    	if(this.requirementPanel.form.isValid( )){
			var otherFieldObj = {};
			var candidateObj = {};
			
			var itemslength = this.requirementPanel.form.getFields().getCount();
			var i = 0;
			while (i < itemslength) {
				var fieldName = this.requirementPanel.form.getFields().getAt(i).name;
				if (fieldName == 'id' ) {
					var idVal = this.requirementPanel.form.findField(fieldName).getValue();
					if(typeof(idVal) != 'undefined' && idVal != null && idVal != "")
						otherFieldObj['id'] = this.requirementPanel.form.findField(fieldName).getValue();
				}else 
					otherFieldObj[fieldName] = this.requirementPanel.form.findField(fieldName).getValue();

				i = i+1;
			}
			
	    	var records = new Array();
	    	for ( var i = 0; i < ds.researchStore.data.length ; i++) {
				var record = ds.researchStore.getAt(i);
	    		if (record.data.id == 0 || record.data.id == null){
	    			delete record.data.id;
	    			records.push(record.data);
	    		}
	    		if (record.data.requirementId == 0 || record.data.requirementId == null)
					delete record.data.requirementId;
	    		
	    		if (this.modifiedResearchIds.indexOf(record.data.id) != -1) {
	    			records.push(record.data);
				}
			}

	    	records = Ext.JSON.encode(records);
			otherFieldObj = Ext.JSON.encode(otherFieldObj);
			
			otherFieldObj =  '{"researchs":{"com.recruit.research.service.Research":' + records +'},'+ otherFieldObj.substring(1,otherFieldObj.length);
			app.requirementService.saveRequirement(otherFieldObj,this.onSave,this);
    	} else {
    		this.successMessage =false;
    		this.updateError('Please fix the errors');
    	}
	},
	
	onSave : function(data){
		this.clearMessage();
		this.modified = true;
		if (data.success) { 
			var rec = data.returnVal;
			this.requirementPanel.form.findField('id').setValue(rec.id)
			this.loadTasks(rec.id);
			this.deleteButton.enable();
			this.requirement.data = rec;
			this.updateMessage('Saved Successfully');
			if(this.newRequirement == true || this.copyRequirement == 'true'){
				this.clearMessage();
				this.newRequirement = false;
				this.updateMessage('Saved successfully');
				this.getForm().reset();
				this.deleteButton.disable();
				ds.subVendorStore.removeAll();
	       		this.vendorStore.removeAll();
	       		ds.subRequirementStore.removeAll();
	       		ds.researchStore.removeAll();
			}
			if(this.copyRequirement == true || this.copyRequirement == 'true'){
				this.clearMessage();
				this.copyRequirement = false; 
				this.updateMessage('Saved previous transaction successfully');
				this.form.findField('id').setValue(null);
				this.deleteButton.disable();
				ds.subVendorStore.removeAll();
	       		this.vendorStore.removeAll();
	       		ds.subRequirementStore.removeAll();
	       		ds.researchStore.removeAll();
			}
			if (this.closeButton) 
				this.closeForm();
		}else {
			this.updateError('Save Failed');
		}	
	},	
	
	onDelete : function(data){
		this.clearMessage();
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			app.requirementMgmtPanel.getLayout().setActiveItem(0);
			app.requirementMgmtPanel.requirementsSearchPanel.search();
		}
	},
	
	deleteConfirm : function()
	{
		Ext.Msg.confirm("Confirm","Do you want to delete this Requirement?", this.deleteRequirement, this);
	},
	
	deleteRequirement : function(dat){
		if(dat=='yes'){
			var idVal = this.requirementPanel.form.findField('id').getValue();
			app.requirementService.deleteRequirement(idVal, this.onDelete, this);
		}
	},

	viewCandidate : function(candidateId) {
		if(app.candidatePanel == null){
			app.candidatePanel  = new tz.ui.CandidatePanel({itemId:'candidatePanel'});
			app.centerLayout.add(app.candidatePanel);
		}
		app.centerLayout.getLayout().setActiveItem('candidatePanel');
		app.candidatePanel.clearMessage();
		if (typeof (candidateId) != 'undefined' && candidateId != null && candidateId >= 0) {
			var record = ds.candidateSearchStore.getById(candidateId);
		}
		app.candidatePanel.loadForm(record);
		app.candidatePanel.openedFrom = "Job Opening";
	},
	
	loadFromCandidates : function() {
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.candidateSearchStore.loadByCriteria(filter);
        ds.candidateSearchStore.on('load', function(store, records, options) {
        	this.submittedCandidates.getView().refresh();
       	}, this);

	}
});Ext.define('tz.service.CandidateService', {
	extend : 'tz.service.Service',
		
	saveCandidates : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/saveCandidates',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    deleteCandidate : function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/deleteCandidate/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    saveCandidate : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/saveCandidate',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    excelExport: function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/candidate/excelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },
    
    onSuccess : function(err) {
    	//Opening the Excel Filename returned from Java, This initiates download on the Browser
    	if (err.responseText.search(false) != -1) {
    		app.invoiceService.onFailure(err);
			return;
		}
    	if (err.responseText.search('.pdf') != -1) {
			var i = err.responseText.indexOf('.pdf');
			var w = window.open(err.responseText.slice(29,i+4));
		}else{
	    	var i=err.responseText.indexOf('xls');
	    	window.open(err.responseText.slice(29,i+3),'_self','',true);
		}
    	app.loadGenerateMask.hide();
    	this.onAjaxResponse;
    },
    
    onFailure : function(err) {
    	app.loadGenerateMask.hide();
    	if (err.responseText.search('.pdf') != -1) {
            Ext.MessageBox.show({
                title: 'Error',
                msg: 'Pdf Generation failed',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
		}else{
	        Ext.MessageBox.show({
	            title: 'Error',
	            msg: 'Excel export failed',
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
        this.onAjaxResponse;
    },

});

Ext.define('tz.ui.CandidateMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.candidatesGridPanel = new tz.ui.CandidatesGridPanel({manager:me});
    	me.candidatesSearchPanel = new tz.ui.CandidatesSearchPanel({manager:me});
    	me.candidatePanel = new tz.ui.CandidatePanel({manager:me});
        
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [ this.candidatesSearchPanel,
 				        {
		   	        		xtype: 'container',
		   	        		height: 10,
		   	        		border : false
 				        },
 				        this.candidatesGridPanel
 				        ]
            },{
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [this.candidatePanel]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen: function(){
		this.getLayout().setActiveItem(0);
		ds.marketingStatusStore.loadAll();
		ds.qualificationStore.loadAll();
    	ds.candidateSourceStore.loadAll();
    	ds.immigrationTypeStore.loadAll();
    	ds.employmentTypeStore.loadAll();
		if (ds.candidateStore.getCount() ==0) 
    		this.candidatesSearchPanel.search();	
	},
	
	showCandidate:function(rowIndex){
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var record = ds.candidateStore.getAt(rowIndex);
		}
		this.candidatePanel.loadForm(record);
	}
	
	
});
Ext.define('tz.ui.CandidatesGridPanel', {
    extend: 'Ext.grid.Panel',
    title: 'Candidates',
    //frame:true,
    anchor:'100%',
    height:560,
    width :'100%',
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value){
    			this.modifiedIds.push(record.data.id);
    		}
    		if(editor.context.field == 'contactNumber'){
    			record.data.contactNumber = record.data.contactNumber.replace(/[^0-9]/g, "");
            	record.data.contactNumber = record.data.contactNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
            	this.getView().refresh();
            }
    		
        },
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.candidateStore;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :40,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'View Candidate',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					app.candidateMgmtPanel.showCandidate(rowIndex);
    				}
    			}]
            },{
                xtype: 'gridcolumn',
                dataIndex: 'priority',
                text: 'Priority',
                width : 70,
                renderer: candidateRender,
				editor: {
                    xtype: 'numberfield',
                    minValue: 0,
                    maxValue: 100
                }
            },{
				text : 'Unique Id',
				dataIndex : 'id',
				width :  70,
		        renderer: candidateRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'firstName',
                text: 'First Name',
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastName',
                text: 'Last Name',
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'marketingStatus',
                text: 'Marketing Status',
                renderer: candidateRender,
                width:120,
                editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    displayField: 'value',
        		    valueField: 'name',
                    store: ds.marketingStatusStore,
                    forceSelection : true,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                }
            },{
				text : 'Availability',
				dataIndex : 'availability',
				width :  100,
				renderer: endDateRenderer,
				editor: {
	                xtype: 'datefield',
	                format: 'm/d/y'
	            }
			},{
				text : 'StartDate',
				dataIndex : 'startDate',
				width :  100,
				renderer: dateRender,
				editor: {
	                xtype: 'datefield',
	                format: 'm/d/y'
	            }
			},{
				text : 'Submitted Date',
				dataIndex : 'submittedDate',
				width :  100,
				hidden :true,
				renderer: dateRender,
				editor: {
	                xtype: 'datefield',
	                format: 'm/d/y'
	            }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:250,
                renderer: candidateRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:1000,
   	                enforceMaxLength:true,
                }
            },{
				text : 'Will Relocate',
				dataIndex : 'relocate',
				width :  60,
				renderer: activeRenderer,
				editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    displayField: 'name',
        		    valueField: 'value',
                    store: ds.yesNoStore,
                    forceSelection : true,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'cityAndState',
                text: 'City & State',
                width:100,
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:90,
   	                enforceMaxLength:true,
                }
            },{
				text : 'Trvl?',
				dataIndex : 'travel',
				width :  60,
				renderer: activeRenderer,
				editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    displayField: 'name',
        		    valueField: 'value',
                    store: ds.yesNoStore,
                    forceSelection : true,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                }
			},{
				text : 'Follow Up',
				dataIndex : 'followUp',
				width :  80,
				renderer: followUpRenderer,
				editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    displayField: 'name',
        		    valueField: 'value',
                    store: ds.yesNoStore,
                    forceSelection : true,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                }
			},{
				text : 'Open To CTH',
				dataIndex : 'openToCTH',
				renderer: candidateRender,
				editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    displayField: 'name',
        		    valueField: 'value',
                    store: ds.yesNoStore,
                    forceSelection : true,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                }
			},{
				text : 'Employment Type',
				dataIndex : 'employmentType',
				renderer: candidateRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'referral',
                text: 'Referral',
                width:80,
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'expectedRate',
                text: 'Expected Rate',
                width:100,
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:100,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'aboutPartner',
                text: 'About Partner',
                width:120,
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:100,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'personality',
                text: 'Personality',
                width:120,
                renderer: candidateRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:100,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'skillSet',
                text: 'Skill Set',
                width:250,
                renderer: candidateRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:2500,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'emailId',
                text: 'EmailID',
                width:120,
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
   	                vtype:'email',	
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'contactNumber',
                text: 'Contact Number',
                width:120,
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:15,
                 	maskRe :  /[0-9]/,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'education',
                text: 'Education',
                width:150,
                renderer: candidateRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:150,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'immigrationStatus',
                text: 'Immigration Status',
                renderer: candidateRender,
                editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    displayField: 'value',
        		    valueField: 'name',
        		    forceSelection : true,
                    store: ds.immigrationTypeStore,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'type',
                text: 'Type',
                width:100,
                renderer: candidateRender,
                editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    displayField: 'value',
        		    valueField: 'name',
        		    forceSelection : true,
                    store: ds.candidateSourceStore,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'currentJobTitle',
                text: 'Current Job Title',
                width:120,
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'employer',
                text: 'Employer',
                width:80,
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'highestQualification',
                text: 'Highest Qualification Held',
                width:120,
                renderer: candidateRender,
                editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    displayField: 'value',
        		    valueField: 'name',
                    store: ds.qualificationStore,
                    forceSelection : true,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'contactAddress',
                text: 'Contact Address',
                width:120,
                renderer: candidateRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:200,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'source',
                text: 'Source',
                width:150,
                renderer: candidateRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:200,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: candidateRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:150,
            }
        ];
        me.viewConfig = {
        	stripeRows: true,
        	
        	getRowClass: function (record, rowIndex, rowParams, store) {
        		return record.get('followUp').match('YES') ? 'back-lightblue' : '';
        	}
        };
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        iconCls : 'btn-add',
                        tabIndex:11,
                        handler: function(){
                    		me.addCandidate();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        tabIndex:11,
                        handler: function(){
                    		me.saveCandidates();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Delete',
                        iconCls : 'btn-delete',
                        tabIndex:11,
                        handler: function(){
                    		me.deleteConfirm();
                    	}
                    },'-',{
                        xtype:'button',
                    	itemId: 'grid-excel-button',
                    	iconCls : 'btn-report-excel',
                        text: 'Export to Excel',
                        tabIndex:12,
                        handler: function(){
                        	me.getExcelExport();
                        }
                    },'->',me.showCount
                    ]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        me.callParent(arguments);
    },
    
    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],

    getExcelExport : function() {
    	var values = app.candidateMgmtPanel.candidatesSearchPanel.getValues();
    	var params = new Array();
    	params.push(['type','=', values.type]);
    	params.push(['marketingStatus','=', values.marketingStatus]);
    	params.push(['experience','=', values.experience]);
   		params.push(['availability','=', values.availability]);
   		params.push(['relocate','=', values.relocate]);
   		params.push(['travel','=', values.travel]);
    	params.push(['search','=', values.search]);
    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;") {
				columns.push(this.columns[i].text);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.candidateService.excelExport(Ext.encode(filter));
	},

    addCandidate : function() {
	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.Candidate',{
   	 		id : null,
   	 		experience : 0,
   	 		travel : 'YES',
   	 		followUp : null, 
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});
   	 	
        this.store.insert(0, rec);
	},
    
    saveCandidates : function() {
    	var records = new Array();
    	for ( var i = 0; i < ds.candidateStore.data.length ; i++) {
			var record = ds.candidateStore.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}
    		if (record.data.employeeId == 0 || record.data.employeeId == null)
				delete record.data.employeeId;

    		if (this.modifiedIds.indexOf(record.data.id) != -1)
    			records.push(record.data);	
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Candidates to save');
    		return;
		}
    	records = Ext.JSON.encode(records);
    	app.candidateService.saveCandidates(records,this.onSave,this);
	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error','Saved Successfully');
		this.modifiedIds =  new Array();
		app.candidateMgmtPanel.candidatesSearchPanel.search();
	},
	
	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Candidate to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Candidate","Do you delete selected Candidate?", this.deleteCandidate, this);
		}
	},
	
	deleteCandidate: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.candidateService.deleteCandidate(selectedRecord.data.id, this.onDeleteCandidate, this);
		}
	},
	
	onDeleteCandidate : function(data){
		if (!data.success) {
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.candidateMgmtPanel.candidatesSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	}


});

function activeRenderer(val, metadata, record, rowIndex, colIndex, store) {
	metadata.tdAttr = 'data-qtip="' + val + '"';
	if (val == "YES") {
		return '<span style="color:#87E320;">' + val + '</span>';
	} 
	if (val == "NO") {
		return '<span style="color:red;">' + val + '</span>';
	}
	return val;
}

function followUpRenderer(val, metadata, record, rowIndex, colIndex, store) {
	metadata.tdAttr = 'data-qtip="' + val + '"';
	if (val == "YES") {
		return '<span style="color:red;">' + val + '</span>';
	} 
	if (val == "NO") {
		return '<span style="color:#87E320;">' + val + '</span>';
	}
	return val;
}

function candidateRender(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null ) 
		return 'N/A';
    return value;
}

function endDateRenderer(value, metadata, record) {
	if (value != null) {
		metadata.tdAttr = 'data-qtip="' + Ext.Date.format(value,'m/d/Y') + '"';
		var date1 = new Date(value);
		var date2 = new Date();
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		//value = value.getMonth()+1 + "/" + value.getDate() + "/" + value.getFullYear();
		if (diffDays <30 && diffDays > 1) {
			return '<span style="color:#E46D0A">' + Ext.Date.format(value,'m/d/Y') + '</span>';
		}else
			return Ext.Date.format(value,'m/d/Y');
	}
}
Ext.define('tz.ui.CandidatesSearchPanel', {
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    frame:true,
    layout:'anchor',
    anchor:'100%',
    autoScroll:true,
//creating view in init function
    initComponent: function() {
        var me = this;
        
		me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    store: ds.candidateSourceStore,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
		    name : 'type',
		    tabIndex:5,
		    labelWidth :105,
		});

		me.marketingStatusCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Marketing Status",
            name: 'marketingStatus',
            store: ds.marketingStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
		    tabIndex:6,
		    labelWidth :105,
		});

		me.relocateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Relocate",
            name: 'relocate',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
		    store: ds.yesNoStore,
		    tabIndex:4,
		});

		me.travelCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Travel",
            name: 'travel',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
		    store: ds.yesNoStore,
		    value : 'YES',
		    tabIndex:3,
		});

        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Search',
	   	                 name: 'search',
	   	                 tabIndex:1,
	   	             },{
	   	                 xtype:'numberfield',
	   	                 fieldLabel: 'Priority',
	   	                 name: 'priority',
	   	                 tabIndex:2,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [me.travelCombo,me.relocateCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [me.typeCombo,me.marketingStatusCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.25,
	   	             items: [{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Availability Date From',
	   	                 name: 'availabilityDateFrom',
	   	                 tabIndex:26,
	   	                 labelWidth :120,
	   	             },{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Availability Date To',
	   	                 name: 'availabilityDateTo',
	   	                 tabIndex:26,
	   	                 labelWidth :120,
	   	             }]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:8,
			},{
				text : 'Reset',
				scope: this,
				handler: this.reset,
				tabIndex:9,
			},{
				text : 'Clear',
				scope: this,
				tabIndex:10,
	             handler: function() {
	            	 this.form.reset();
	            	 this.travelCombo.setValue(null);
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['type','=', values.type]);
    	params.push(['marketingStatus','=', values.marketingStatus]);
    	params.push(['priority','=', values.priority]);
   		params.push(['relocate','=', values.relocate]);
   		params.push(['travel','=', values.travel]);
    	params.push(['search','=', values.search]);
    	if (values.availabilityDateFrom != undefined && values.availabilityDateFrom != '') {
    		var availabilityDateFrom = new Date(values.availabilityDateFrom);
        	params.push(['availabilityDateFrom','=',availabilityDateFrom]);
    	}
    	if (values.availabilityDateTo != undefined && values.availabilityDateTo != '') {
    		var availabilityDateTo = new Date(values.availabilityDateTo);
        	params.push(['availabilityDateTo','=',availabilityDateTo]);
    	}

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=50;
    	ds.candidateStore.loadByCriteria(filter);
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
Ext.define('tz.ui.CandidatePanel', {
	extend : 'tz.ui.BaseFormPanel',
	
	bodyPadding: 10,
    title: '',
    anchor:'100% 95%',
    
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 160},
	
	initComponent : function() {
		var me = this;
		me.modified = false;
		me.closeButton = false;
		me.openedFrom ="";
		me.candidate = "";
		 
		me.immigrationCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Immigration Status",
			name: 'immigrationStatus',
			tabIndex:6,
			queryMode: 'local',
			displayField: 'name',
			valueField: 'value',
			store: ds.immigrationTypeStore,
			forceSelection:true,
			allowBlank:true,
		});

		me.qualificationCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Highest Qualification Held',
		    store: ds.qualificationStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    name : 'highestQualification',
		    tabIndex:16,
		});
		
		me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    store: ds.candidateSourceStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    name : 'type',
		    tabIndex:15,
		});
		
		me.currentJobCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Current Job Title",
            name: 'currentJobTitle',
		    store: ds.currentJobStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:6,
		});

		me.marketingStatusCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Marketing Status",
            name: 'marketingStatus',
            store: ds.marketingStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:18,
		});

		me.relocateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Will Relocate",
            name: 'relocate',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:17,
		});

		me.travelCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Trvl?",
            name: 'travel',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:19,
		});

		me.followUpCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Follow Up",
            name: 'followUp',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:21,
		});

		me.openToCTHCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Open To CTH",
            name: 'openToCTH',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:23,
		});

		me.employmentTypeCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
			fieldLabel: "Employment Type",
            name: 'employmentType',
	        tabIndex:24,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.employmentTypeStore,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					this.form.findField('selectedEmploymentType').setValue(combo.rawValue);
				}
		    }
		});

        me.jobOpeningsGrid = Ext.create('Ext.grid.Panel', {
            title: 'Submitted Jobs',
            frame:true,
            anchor:'98%',
            height:250,
            width:'100%',
            store: ds.candidate_requirementStore,

            columns: [
                      {
                          xtype: 'actioncolumn',
                          width :40,
                          items : [{
              				icon : 'images/icon_edit.gif',
              				tooltip : 'View Requirement',
              				padding: 50,
              				scope: this,
              				handler : function(grid, rowIndex, colIndex) {
              					me.getRequirmentTab(rowIndex);
              				}
              			}]
                      },{
          		        xtype: 'gridcolumn',
          		        dataIndex: 'hotness',
          		        text: 'Hotness',
          		        width :  45,
                          editor: {},
                          renderer:function(value, metadata, record){
                          	if (value == 0) {
                          		metadata.style = "color:#009AD0;";
          					}
                          	var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
                          	if(rec){
                          		metadata.tdAttr = 'data-qtip="' + rec.data.value+ '"';
                          		return rec.data.value;
                          	}else{
                          		metadata.tdAttr = 'data-qtip="' + value + '"';
                          		return value;	
                          	}
          	            }
          		    },{
          				text : 'Unique Id',
          				dataIndex : 'id',
          				width :  45,
          		        renderer: renderValue,
          			},{
              			text : 'Job Opening Status',
              			dataIndex : 'jobOpeningStatus',
              			width :  60,
                        renderer: renderValue,
              		},{
              			xtype: 'datecolumn',
          				text : 'Posting Date',
          				dataIndex : 'postingDate',
          				width :  60,
          				renderer: dateRender,
          			},{
          		        xtype: 'gridcolumn',
          		        dataIndex: 'postingTitle',
          		        text: 'Posting Title',
          		        renderer: renderValue,
          		    },{
          		        xtype: 'gridcolumn',
          		        dataIndex: 'location',
          		        text: 'Location',
          		        renderer: renderValue,
          		    },{
          				header : 'Client',
          				dataIndex : 'client',
          	            width :  120,
          	            renderer: renderValue,
          			},{
          				header : 'Vendor',
          				dataIndex : 'vendor',
          	            width :  120,
          	            renderer: renderValue,
          			},{
          				header : 'Contact Person',
          				dataIndex : 'contactPerson',
          	            width :  120,
          	            renderer: renderValue,
          			},{
          				text : 'Roles and Responsibilities',
          				dataIndex : 'requirement',
          				width :  150,
          	            renderer: renderValue
          			},{
          				header : 'Candidate',
          				dataIndex : 'candidateId',
          	            width :  120,
                          renderer:function(value, metadata, record){
          					var ids = value.toString().split(',');
          					var names ="";
          					for ( var i = 0; i < ids.length; i++) {
          						var rec = ds.candidateSearchStore.getById(parseInt(ids[i]));
          						if (rec)
          							names += rec.data.firstName+' '+rec.data.lastName +", ";	
          					}
          					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
          					return names.substring(0,names.length-2);
                          }
          			},{
        				xtype: 'gridcolumn',
        				text : 'Submitted Date',
        				dataIndex : 'submissionDate',
        				width :  60,
                        renderer:function(value, metadata, record){
                        	if (value != "") {
                        		var req_submittedDate = value.split(',');
                        		var dates = new Array();
                        		for ( var i = 0; i < req_submittedDate.length; i++) {
                        			var id_dates = req_submittedDate[i].split('-');
                        			if (id_dates[1] != "null")
                        				dates.push(id_dates[1]);
            					}
            					metadata.tdAttr = 'data-qtip="' + dates.toString() + '"';
            					return dates.toString();
        					}
                        	return value;
                        }
        			},{
          				xtype: 'gridcolumn',
                        dataIndex: 'module',
                        text: 'Module',
                        width :  60,
                        renderer: renderValue,
          			},{
          				text : 'Employer',
          				dataIndex : 'employer',
          				width :  40,
          				renderer: employerRenderer,
          			},{
                        xtype: 'gridcolumn',
                        dataIndex: 'comments',
                        text: 'Comments',
                        renderer: renderValue,
                        width:150,
                    },{
          				text : 'Public Link',
           				dataIndex : 'publicLink',
           				renderer: linkRenderer,
           				width :  80,
                    },{
          				text : 'Source',
          				dataIndex : 'source',
          				renderer: renderValue,
          			}
                  ],
        });


		
		me.items = [ me.getMessageComp(), me.getHeadingComp(), {
	         xtype: 'container',
	         items:[
			         {xtype: 'hidden', name: 'id'},
	                {
				xtype : 'fieldset',
				title : 'Candidate Information',
				collapsible : true,
				defaultType : 'textfield',
				layout : 'column',
				items : [
				         {
	             xtype: 'container',
	             columnWidth:.4,
	             items: [{
   	                 xtype:'textfield',
   	                 fieldLabel: 'First Name',
   	                 name: 'firstName',
   	                 allowBlank:false,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	                 tabIndex:1,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: 'Email',
   	                 name: 'emailId',
   	                 allowBlank:false,
   	                 vtype:'email',
   	                 tabIndex:3,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: 'Contact Number',
   	                 name: 'contactNumber',
   	                 maskRe :  /[0-9]/,
   	                 tabIndex:5,
   	                 maxLength:15,
   	                 enforceMaxLength:true,
   	                 listeners:{
   	                	 change : function(field,newValue,oldValue){
   	                		 var value = me.form.findField('contactNumber').getValue();
   	                		 if (value != "" ) {
   	                			 value = value.replace(/[^0-9]/g, "");
	   	                		 value = value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
	   	                		 me.form.findField('contactNumber').setValue(value);
   	                		 }
					    }
		            }
   	             },me.immigrationCombo]
	         },{
	             xtype: 'container',
	             columnWidth:.5,
	             items: [{
   	                 xtype:'textfield',
   	                 fieldLabel: 'Last Name',
   	                 name: 'lastName',
   	                 allowBlank:false,
   	                 tabIndex:2,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: 'Contact Address',
   	                 name: 'contactAddress',
   	                 tabIndex:4,
   	                 maxLength:200,
   	                 enforceMaxLength:true,
   	             }]
	         }]
	        },{
				xtype : 'fieldset',
				title : 'Professional Information',
				collapsible : true,
				defaultType : 'textfield',
				layout : 'column',
				items : [
				         {
	             xtype: 'container',
	             columnWidth:.4,
	             items: [{
   	                 xtype:'textfield',
   	                 fieldLabel: "Current Job Title",
   	                 name: 'currentJobTitle',
   	                 tabIndex:7,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'datefield',
   	                 fieldLabel: 'Availability',
   	                 name: 'availability',
   	                 tabIndex:9,
   	             },{
   	                 xtype:'datefield',
   	                 fieldLabel: 'StartDate',
   	                 name: 'startDate',
   	                 tabIndex:11,
   	             },{
   	                 xtype:'datefield',
   	                 fieldLabel: 'Submitted Date',
   	                 name: 'submittedDate',
   	                 tabIndex:13,
   	                 hidden :true,
   	             },
   	             me.typeCombo,me.relocateCombo,me.travelCombo,me.followUpCombo,me.openToCTHCombo,
   	             {
   	                 xtype:'textfield',
   	                 fieldLabel: "Referral",
   	                 name: 'referral',
   	                 tabIndex:25,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: "About Partner",
   	                 name: 'aboutPartner',
   	                 tabIndex:26,
   	                 grow : true,
   	                 maxLength:200,
   	                 enforceMaxLength:true,
   	                 width : 500,
   	                 height : 50,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: "Skill Set",
   	                 name: 'skillSet',
   	                 tabIndex:28,
   	                 maxLength:2500,
   	                 enforceMaxLength:true,
   	                 width : 600,
	                 height : 120,
   	             }]
	         },{
	             xtype: 'container',
	             columnWidth:.5,
	             items: [{
   	                 xtype:'numberfield',
   	                 fieldLabel: 'Priority',
   	                 name: 'priority',
   	                 tabIndex:8,
   	                 value :0,
   	                 minValue: 0,
   	                 maxValue: 100
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "Source",
   	                 name: 'source',
   	                 tabIndex:10,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "City & State",
   	                 name: 'cityAndState',
   	                 tabIndex:12,
   	                 maxLength:90,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "Employer",
   	                 name: 'employer',
   	                 tabIndex:14,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },
   	             me.qualificationCombo,me.marketingStatusCombo,
   	             {
   	                 xtype:'textfield',
   	                 fieldLabel: "Education",
   	                 name: 'education',
   	                 tabIndex:20,
   	                 maxLength:150,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "Expected Rate",
   	                 name: 'expectedRate',
   	                 tabIndex:22,
   	                 maxLength:100,
   	                 enforceMaxLength:true,
   	             },
   	             me.employmentTypeCombo,
   	             {
   	                 xtype:'textarea',
   	                 fieldLabel: "Selected Employment Type",
   	                 name: 'selectedEmploymentType',
   	                 width : 500,
   	                 height : 40,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: "Personality",
   	                 name: 'personality',
   	                 tabIndex:27,
   	                 maxLength:300,
   	                 enforceMaxLength:true,
   	                 width : 500,
   	                 height : 50,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: "Comments",
   	                 name: 'comments',
   	                 tabIndex:29,
   	                 maxLength:1000,
   	                 enforceMaxLength:true,
   	                 width : 600,
   	                 height : 120,
   	             },{
 					xtype : 'datefield',
	            	 name : 'created',
	            	 hidden : true
				}]
	         }]
	        },{
				xtype : 'fieldset',
				title : 'Job Openings',
				collapsible : true,
				defaultType : 'textfield',
				layout : 'column',
				items : [
				         {
	             xtype: 'container',
	             columnWidth:1,
	             items: [me.jobOpeningsGrid]
	         }]
	        }]
	     },
	     
	   	 ];
		
        me.saveButton = new Ext.Button({
			text : 'Save',
			iconCls : 'btn-save',
			tabIndex:30,
			scope: this,
	        handler: this.saveCandidate
        });

        me.deleteButton = new Ext.Button({
			text : 'Delete',
			iconCls : 'btn-delete',
			tabIndex:31,
			scope: this,
	        handler: this.deleteConfirm
        });

        me.closeButton = new Ext.Button({
			text : 'Close',
			iconCls : 'btn-close',
			tabIndex:32,
			scope: this,
	        handler: this.checkForDirty
        });

		me.tbar =  [ this.saveButton, '-', this.deleteButton , '-', this.closeButton ];

		this.callParent(arguments);
	},
	
	checkForDirty : function() {
		if (this.candidate =="")
			this.closeFormDirty();
		this.closeButton = true;
		var formDirty = false;
		var itemslength = this.form.getFields().getCount();
		var i = 0;
		while (i < itemslength) {
			var fieldName = this.form.getFields().getAt(i).name;
			value = this.form.findField(fieldName).getValue();
			
			if (fieldName == 'created' || fieldName == 'selectedEmploymentType') {
			}else{
				if (value == null || this.candidate.data[fieldName] == null) {
					if (value == null && this.candidate.data[fieldName] != null && this.candidate.data[fieldName] == "") {
						//if value is null and previous value is not null previous value must be empty string
					}else if (this.candidate.data[fieldName] == null && value != null && value == "") {
						//if previous value is null and value is not null then value must be empty string
					}else if (value == null && this.candidate.data[fieldName] == null) {
						// if both are null no issue
					}else{
						formDirty = true;
					}
				}else if (fieldName == 'availability' || fieldName == 'startDate' || fieldName == 'submittedDate' ) {
					if (Ext.Date.format(new Date(this.candidate.data[fieldName]),'m/d/y') != Ext.Date.format(value,'m/d/y'))
						formDirty = true;
				}else if (this.candidate.data[fieldName].toString() != value.toString()) {
					formDirty = true;
				}
			}
			
			i = i+1;
		}

		if (formDirty) {
			Ext.MessageBox.show({
				title:'Confirm',
	            msg: "Do you want to save Candidate changes?",
	            buttonText: {                        
	                yes: 'Save and Close',
	                no: 'Close without Saving'
	            },
	            icon: Ext.MessageBox.INFO,
	            fn: function(btn) {
	            	if (btn == 'yes') {
	            		app.candidateMgmtPanel.candidatePanel.saveCandidate();
	            	}else if (btn == 'no') {
	            		if(app.candidatePanel != null)
	            			app.candidatePanel.closeFormDirty();
	            		else
	            			app.candidateMgmtPanel.candidatePanel.closeFormDirty();
					}
	            }
	        });
		}else
			this.closeFormDirty();
	},
	
	closeFormDirty: function(){
		if (this.openedFrom == "Recruit" || this.openedFrom == "Job Opening") {
			app.centerLayout.getLayout().setActiveItem('requirementMgmtPanel');
			app.requirementMgmtPanel.requirementDetailPanel.loadFromCandidates();
			
		}else{
			if(this.modified){
				app.candidateMgmtPanel.getLayout().setActiveItem(0);
				app.candidateMgmtPanel.candidatesSearchPanel.search();
			}else
				this.manager.initScreen();
		}
	},
	
	
    loadForm : function(record) {
    	this.modified = false;
    	this.closeButton = false;
    	this.clearMessage();
    	this.form.reset();
		this.loadRecord(record);
		this.employmentTypeCombo.setValue(record.data.employmentType.split(','));
		this.openedFrom = "Candidate";
		this.candidate = record;
		if (record.data.id != null) {
	    	var params = new Array();
	    	params.push(['candidateId','=', record.data.id]);
	    	var filter = getFilter(params);
	    	filter.pageSize=1000;
	    	ds.candidate_requirementStore.loadByCriteria(filter);
		}else
			ds.candidate_requirementStore.removeAll();
	},

	saveCandidate: function(){
		this.clearMessage();
		if(! this.form.isValid()){
			this.updateError('Please fix the errors.');
			return false;
		}
		var itemslength = this.form.getFields().getCount();
   		var i = 0;    		
   		var candidate = {};
   		
   		while (i < itemslength) {
   			var fieldName = this.form.getFields().getAt(i).name;
   				if(fieldName == 'id') {
   					var idVal = this.form.findField(fieldName).getValue();
   					if(typeof(idVal) != 'undefined' && idVal > 0) {
   						candidate['id'] = this.form.findField(fieldName).getValue();
   					}
   				} else {
   					candidate[fieldName] = this.form.findField(fieldName).getValue();
   				}
   			i = i + 1;
   		}
   		candidate['employmentType'] = this.employmentTypeCombo.getValue().toString();
   		delete candidate.selectedEmploymentType;
		var candidateObj = Ext.JSON.encode(candidate);
		app.candidateService.saveCandidate(candidateObj, this.onSaveCandidate, this);
		
	},
	
	onSaveCandidate: function(data){
		this.modified = true;
		if(!data.success){
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
			
		}else{
			if (this.openedFrom == "Recruit") {
				app.requirementMgmtPanel.requirementsGrid.getRecruitmentTab();
			}else{
				this.getForm().findField('id').setValue(data.returnVal.id);
				this.getForm().findField('comments').setValue(data.returnVal.comments);
				this.candidate.data = data.returnVal;
				this.updateMessage('Saved successfully');
				if (this.closeButton) 
					this.closeFormDirty();
			}
		}
		this.body.scrollTo('top', 0);
	},
	
	deleteConfirm : function()
	{
		Ext.Msg.confirm("This will delete the Candidate", "Do you want to continue?", this.deleteCandidate, this);
	},
	
	onDelete : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			app.candidateMgmtPanel.getLayout().setActiveItem(0);
			app.candidateMgmtPanel.candidatesSearchPanel.search();
		}
		this.body.scrollTo('top', 0);
	},
	
	deleteCandidate: function(dat){
		if(dat=='yes'){
			var candidate = this.form.getValues();
			app.candidateService.deleteCandidate(candidate.id, this.onDelete, this);
		}
	},

	getRequirmentTab : function(rowIndex) {
		app.centerLayout.getLayout().setActiveItem('requirementMgmtPanel');
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var record = ds.candidate_requirementStore.getAt(rowIndex);
		}
		app.requirementMgmtPanel.requirementDetailPanel.loadForm(record);

	}
	
});
Ext.define('tz.service.PlacementService', {
	extend : 'tz.service.Service',
		
    savePlacements : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/placementLeads/savePlacementLeads',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    deletePlacement: function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/placementLeads/deletePlacement/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    savePlacement : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/placementLeads/savePlacement',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

});

Ext.define('tz.ui.PlacementMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.placementSearchPanel = new tz.ui.PlacementSearchPanel({manager:me});
    	me.placementsGrid = new tz.ui.PlacementsGrid({manager:me});
        me.placementsDetailPanel = new tz.ui.PlacementsDetailPanel({manager:me});
        
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,    	                 
            	centered:true,
            	border:false,  
            	items: [ this.placementSearchPanel,
 					    {
		   	        		xtype: 'container',
		   	        		height: 15,
		   	        		border : false
 					    },
 					    this.placementsGrid
 					    ]
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
	            items: [me.placementsDetailPanel]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen: function(){
		this.getLayout().setActiveItem(0);
    	if (ds.placementLeadsStore.getCount() ==0) 
    		this.placementSearchPanel.search();	
	},
	
	showPlacements : function(rowIndex) {
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var record = ds.placementLeadsStore.getAt(rowIndex);
		}
		this.placementsDetailPanel.loadForm(record);
	},
	
});
Ext.define('tz.ui.PlacementsDetailPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 10,
    title: '',
    anchor:'100% 95%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'left'},
    
    initComponent: function() {
        var me = this;
        me.newPlacement = false;
		me.copyPlacement = false;
        
		me.informationTypeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type of Information',
		    store: ds.informationTypeStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
            valueField: 'name',
		    name : 'typeOfInformation',
		    labelWidth :125,
		    tabIndex:5,
		    labelWidth :125,
		});
		
		me.deleteButton = new Ext.Button({
            text: 'Delete',
            iconCls : 'btn-delete',
            scope : this,
            tabIndex:18,
            handler: function(){
    			this.deleteConfirm();
    		}
        });

        me.saveAndNewButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & New',
            iconCls : 'btn-save',
            tabIndex:16,
            scope: this,
	        handler: function() {
				this.saveAndNewPlacement();
			}
        });
        me.saveAndCopyButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & Copy',
            iconCls : 'btn-save',
            tabIndex:17,
            scope: this,
	        handler: function() {
				this.saveAndCopyPlacement();	
			}
        });

		
        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        scope: this,
                        tabIndex:15,
            	        handler: this.save
                    },
                    '-',me.saveAndNewButton,'-',me.saveAndCopyButton,'-',me.deleteButton,'-',
                    {
                        xtype: 'button',
                        text: 'Close',
                        iconCls : 'btn-close',
                        tabIndex:19,
                        scope : this,
                        handler: function(){
                        	this.closeForm();
                        }
                    }
                ]
            }
        ];
    	
        me.items = [this.getMessageComp(), this.getHeadingComp(),
            {
	   	    	 xtype:'fieldset',
	   	         collapsible: false,
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.3,
	   	             items: [{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Date',
	   	                 name: 'date',
	   	                 tabIndex:1,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Person',
	   	                 name: 'person',
	   	                 tabIndex:2,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Source of Information',
	   	                 name: 'sourceOfInformation',
	   	                 tabIndex:3,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Source Sublevel',
	   	                 name: 'source_Sublevel',
	   	                 tabIndex:4,
	   	                 labelWidth :125,
	   	             },
	   	             me.informationTypeCombo,
	   	             {
	   	            	 xtype: 'numberfield',
	   	            	 name: 'priority',
	   	            	 fieldLabel: 'Priority',
	   	            	 minValue: 0,
	   	            	 maxValue: 100,
	   	            	 labelWidth :125,
	   	            	 tabIndex:6,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Hospital / Entity',
	   	                 name: 'hospital_Entity',
	   	                 tabIndex:7,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Location',
	   	                 name: 'location',
	   	                 tabIndex:8,
	   	                 maxLength:100,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Go Live Date',
	   	                 name: 'liveDate',
	   	                 maxLength:50,
	   	                 enforceMaxLength:true,
	   	                 tabIndex:9,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Link 1',
	   	                 name: 'link1',
	   	                 vtype:'url',
	   	                 tabIndex:10,
	   	                 maxLength:200,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Link 2',
	   	                 name: 'link2',
	   	                 vtype:'url',
	   	                 tabIndex:11,
	   	                 maxLength:200,
	   	                 enforceMaxLength:true,
	   	                 labelWidth :125,
	   	             },{
	   	                 xtype:'textarea',
	   	                 fieldLabel: 'Comments',
	   	                 name: 'comments',
	   	                 tabIndex:12,
	   	                 labelWidth :125,
	   	                 maxLength:300,
	   	                 width:400,
	   	                 height:70,
	   	                 enforceMaxLength:true,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.6,
	   	             items: [{
	   	                 xtype:'textarea',
	   	                 fieldLabel: 'Information',
	   	                 name: 'information',
	   	                 tabIndex:13,
	   	                 maxLength:1000,
	   	                 width:750,
	   	                 height:170,
	   	                 enforceMaxLength:true,
	   	             },{
	   	                 xtype:'textarea',
	   	                 fieldLabel: 'Outreach',
	   	                 name: 'outreach',
	   	                 tabIndex:14,
	   	                 maxLength:1000,
	   	                 width:750,
	   	                 height:150,
	   	                 enforceMaxLength:true,
	   	             },{
	   	     	        xtype: 'numberfield',
	   	    	        hidden:true,
	   	    	        name: 'id'
	   	    	    },{
	   	     	        xtype: 'datefield',
	   	    	        hidden:true,
	   	    	        name: 'created'
	   	    	    }]
	   	         }]
	   	     }
        ];
        me.callParent(arguments);
    },

    loadForm : function(record) {
    	this.clearMessage();
    	this.form.reset();
		this.loadRecord(record);
		this.deleteButton.enable();
	},
    
	closeForm: function(){
		app.placementMgmtPanel.initScreen();
	},

	saveAndNewPlacement : function() {
		this.newPlacement = true;
		this.save();
	},
	
	saveAndCopyPlacement : function() {
		this.copyPlacement = true;
		this.save();
	},
	
    save : function(){
    	if(this.form.isValid( )){
			var otherFieldObj = {};
			var itemslength = this.form.getFields().getCount();
			var i = 0;
			while (i < itemslength) {
				var fieldName = this.form.getFields().getAt(i).name;
				if (fieldName == 'id' ) {
					var idVal = this.form.findField(fieldName).getValue();
					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0)
						otherFieldObj['id'] = this.form.findField(fieldName).getValue();
				}else 
					otherFieldObj[fieldName] = this.form.findField(fieldName).getValue();
				i = i+1;
			}
			
			otherFieldObj = Ext.JSON.encode(otherFieldObj);
			app.placementService.savePlacement(otherFieldObj,this.onSave,this);
    	} else {
    		this.updateError('Please fix the errors');
    	}
	},
	
	onSave : function(data){
		if (data.success) { 
			var rec = data.returnVal;
			this.form.findField('id').setValue(rec.id)
			this.deleteButton.enable();
			this.updateMessage('Saved Successfully');
			if(this.newPlacement == true || this.copyPlacement == 'true'){
				this.clearMessage();
				this.newPlacement = false;
				this.updateMessage('Saved successfully');
				this.getForm().reset();
				this.deleteButton.disable();
			}
			if(this.copyPlacement == true || this.copyPlacement == 'true'){
				this.clearMessage();
				this.copyPlacement = false; 
				this.updateMessage('Saved previous Placement successfully');
				this.form.findField('id').setValue(null);
				this.deleteButton.disable();
			}
		}else {
			this.updateError('Save Failed');
		}	
	},	
	
	onDelete : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			this.closeForm();
		}
	},
	
	deleteConfirm : function(){
		Ext.Msg.confirm("Confirm","Do you want to delete this Placement Lead?", this.deletePlacement, this);
	},
	
	deletePlacement : function(dat){
		if(dat=='yes'){
			var idVal = this.getValues().id;
			app.placementService.deletePlacement(idVal, this.onDelete, this);
		}
	},

});Ext.define('tz.ui.PlacementSearchPanel', {
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    frame:true,
    anchor:'100%',
    autoScroll:true,

    initComponent: function() {
        var me = this;
        
        me.informationTypeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type of Information',
		    store: ds.informationTypeStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'typeOfInformation',
		    labelWidth :125,
		    tabIndex:3,
		    labelWidth :125,
		});
        
        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.3,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Search',
	   	                 name: 'search',
	   	                 tabIndex:1,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.3,
	   	             items: [{
	   	                 xtype:'numberfield',
	   	                 fieldLabel: 'Priority',
	   	                 name: 'priority',
	   	                 tabIndex:2,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.3,
	   	             items: [me.informationTypeCombo]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:2,
			},{
				text : 'Reset',
				scope: this,
				handler: this.reset,
				tabIndex:3,
			},{
				text : 'Clear',
				scope: this,
				tabIndex:4,
	             handler: function() {
	            	 this.form.reset();
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.form.getFieldValues();
    	var params = new Array();
    	params.push(['typeOfInformation','=', values.typeOfInformation]);
    	if (values.priority != null)
    		params.push(['priority','=', values.priority.toString()]);
    	params.push(['search','=', values.search]);

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=200;
    	ds.placementLeadsStore.loadByCriteria(filter);
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
Ext.define('tz.ui.PlacementsGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Placement Leads',
    frame:true,
    anchor:'100%',
    height:550,
    width :'100%',
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value){
    			this.modifiedIds.push(record.data.id);
    		}
        }
    },

    initComponent: function() {
        var me = this;
        me.store = ds.placementLeadsStore;
        me.modifiedIds = new Array();
        
        me.columns = [
                      {
                    	  xtype: 'actioncolumn',
                    	  width :40,
                    	  items : [{
                    		  icon : 'images/icon_edit.gif',
                    		  tooltip : 'View Placement',
                    		  padding: 50,
                    		  scope: this,
                    		  handler : function(grid, rowIndex, colIndex) {
                    			  app.placementMgmtPanel.showPlacements(rowIndex);
                    		  }
                    	  }]
                      },{
                    	xtype: 'datecolumn',
                    	text : 'Date',
          				dataIndex : 'date',
          				width :  100,
          				renderer: dateRender,
          				editor: {
          	                xtype: 'datefield',
          	                format: 'm/d/y'
          	            }
          			},{
          				text : 'Source of Information',
          				dataIndex : 'sourceOfInformation',
          				width :  80,
          		        renderer: placementsRender,
          		        editor: {
          	                xtype: 'textfield',
          	                maxLength:45,
         	                enforceMaxLength:true,
          	            }
          			},{
          				text : 'Source Sublevel',
          				dataIndex : 'source_Sublevel',
          				renderer: sourceSublevelRender,
          				xtype: 'datecolumn',
          				editor: {
          	                xtype: 'datefield',
          	                format: 'm/d/y'
          	            }
          			},{
                        xtype: 'gridcolumn',
                        dataIndex: 'priority',
                        text: 'Priority',
                        width : 80,
          		        renderer: placementsRender,
        				editor: {
                            xtype: 'numberfield',
                            minValue: 0,
                            maxValue: 100
                        }
                    },{
          		        xtype: 'gridcolumn',
          		        dataIndex: 'typeOfInformation',
          		        text: 'Type of Information',
          		        renderer: placementsRender,
          		        editor: {
          		        	xtype: 'combobox',
          	                queryMode: 'local',
          	                typeAhead: true,
          	                forceSelection:true,
          	                triggerAction: 'all',
          	                selectOnTab: true,
          	                displayField: 'value',
          	                valueField: 'name',
          	                emptyText:'Select...',
          	    		    store : ds.informationTypeStore,
          	                lazyRender: true,
          	                listClass: 'x-combo-list-small'
          	            }
          		    },{
          				text : 'Information',
          				dataIndex : 'information',
          				width :  120,
          		        renderer: placementsRender,
          		        editor: {
          	                xtype: 'textarea',
          	                height : 60,
          	                maxLength:1000,
         	                enforceMaxLength:true,
          	            }
          			},{
          				header : 'Hospital / Entity',
          				dataIndex : 'hospital_Entity',
          	            width :  80,
          		        renderer: placementsRender,
          		        editor: {
          	                xtype: 'textfield',
          	                maxLength:45,
         	                enforceMaxLength:true,
          	            }
          			},{
          				text : 'Location',
          				dataIndex : 'location',
          				width :  100,
          		        renderer: placementsRender,
          		        editor: {
          	                xtype: 'textfield',
          	                maxLength:150,
         	                enforceMaxLength:true,
          	            }
          			},{
          				text : 'Go Live Date',
          				dataIndex : 'liveDate',
          				renderer: placementsRender,
          				width :  80,
          				editor: {
          	                xtype: 'textfield',
          	                maxLength:50,
         	                enforceMaxLength:true,
          	            }
          			},{
          				text : 'Link 1',
          				dataIndex : 'link1',
          				renderer: linkRenderer,
          				width :  150,
          				editor: {
          	                xtype: 'textfield',
          	                maxLength:200,
         	                enforceMaxLength:true,
         	                vtype:'url',
          				},
          			},{
          				text : 'Link 2',
          				dataIndex : 'link2',
          				renderer: linkRenderer,
          				width :  80,
          				editor: {
          	                xtype: 'textfield',
          	                maxLength:200,
         	                enforceMaxLength:true,
         	                vtype:'url',
          				},
          			},{
          				xtype: 'gridcolumn',
		                dataIndex: 'outreach',
		                text: 'Outreach',
		                width:150,
          		        renderer: placementsRender,
		                editor: {
		                    xtype: 'textarea',
		                    height : 60,
		                    maxLength:1000,
		   	                enforceMaxLength:true,
		                }
		            },{
          				xtype: 'gridcolumn',
		                dataIndex: 'comments',
		                text: 'Comments',
		                width:150,
          		        renderer: placementsRender,
		                editor: {
		                    xtype: 'textarea',
		                    height : 60,
		                    maxLength:300,
		   	                enforceMaxLength:true,
		                }
		            },{
          		        xtype: 'gridcolumn',
          		        dataIndex: 'person',
          		        text: 'Person',
          		        renderer: placementsRender,
          		        editor: {
          	                xtype: 'textfield',
          	                maxLength:45,
         	                enforceMaxLength:true,
          	            }
          		    },{
          				text : 'Last Updated User',
          				dataIndex : 'lastUpdatedUser',
          		        renderer: placementsRender,
          			},{
                        xtype: 'datecolumn',
                        dataIndex: 'created',
                        text: 'Created',
                        format: "m/d/Y H:i:s A",
                        width:100,
                    },{
                        xtype: 'datecolumn',
                        dataIndex: 'lastUpdated',
                        format: "m/d/Y H:i:s A",
                        text: 'Last Updated',
                        width:100,
                    }
                  ];
        
        
        me.viewConfig = {
        		forceFit:true,
                stripeRows: true,
                listeners: {
                    dblClick: function (view, cell, cellIndex, record, row, rowIndex, e) {
                    	var colHeading = view.panel.headerCt.getHeaderAtIndex(cellIndex).text;
                    	if (colHeading =="Link 1" ||colHeading=="Link 2") {
                    		url = cell.textContent;
                    		//window.open(url,"_blank");
    					}
                    },
                }
        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Add New',
            		iconCls : 'btn-add',
            		tabIndex:11,
            		handler: function(){
            			me.addPlacement();
            		}
            	},'-',{
            		xtype: 'button',
            		text: 'Save',
            		iconCls : 'btn-save',
            		tabIndex:11,
            		handler: function(){
            			me.savePlacements();
            		}
            	},'-',{
                    xtype: 'button',
                    text: 'Delete',
                    iconCls : 'btn-delete',
                    tabIndex:11,
                    handler: function(){
                		me.deleteConfirm();
                	}
                },'-',{
            		xtype:'button',
                	itemId: 'grid-excel-button',
                	iconCls : 'btn-report-excel',
                	text: 'Export to Excel',
                	tabIndex:12,
                	handler: function(){
                		me.getExcelExport();
                		/*var vExportContent = me.getExcelXml();
                		document.location='data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);*/
                	}
            	},'->',me.showCount]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        me.callParent(arguments);
    },
    
    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],

    getExcelExport : function() {
    	var values = app.placementMgmtPanel.placementSearchPanel.getValues();
    	var params = new Array();
    	params.push(['date','=', values.date]);
    	params.push(['person','=', values.person]);
    	params.push(['search','=', values.search]);
    	
    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;") {
				columns.push(this.columns[i].text);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.requirementService.placementsExcelExport(Ext.encode(filter));
	},

    addPlacement : function() {
	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.PlacementLeads',{
   	 		id : null,
   	 		hospital_Entity :'N/A',
   	 		location :'N/A',
   	 		liveDate :'N/A',
   	 		outreach :'N/A',
   	 		comments :'N/A',
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});

        this.store.insert(0, rec);
	},
    
    savePlacements : function() {
    	var records = new Array();

    	for ( var i = 0; i < ds.placementLeadsStore.data.length ; i++) {
			var record = ds.placementLeadsStore.getAt(i);
    		if (record.data.id == 0 || record.data.id == null) {
    			delete record.data.id;
    			records.push(record.data);
			}
    		if (this.modifiedIds.indexOf(record.data.id) != -1) {
    			records.push(record.data);	
			}
		}
    	
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Placement Leads to save');
    		return;
		}

    	records = Ext.JSON.encode(records);
    	app.placementService.savePlacements(records,this.onSave,this);

	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error','Saved Successfully');
		this.modifiedIds =  new Array();
		app.placementMgmtPanel.placementSearchPanel.search();
	},

	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Placement Lead to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Placement Lead","Do you delete selected Placement Lead?", this.deletePlacement, this);
		}
	},
	
	deletePlacement: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.placementService.deletePlacement(selectedRecord.data.id, this.onDeletePlacement, this);
		}
	},
	
	onDeletePlacement : function(data){
		if (!data.success) { 
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.placementMgmtPanel.placementSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	}

});

function linkRenderer(value, metadata, record, rowIndex, colIndex, store) {
	metadata.tdAttr = 'data-qtip="' + value + '"';
	return '<a href='+value+' target="_blank">'+ value +'</a>'
}

function placementsRender(value, metadata, record, rowIndex, colIndex, store) {
    var dataIndex = app.placementMgmtPanel.placementsGrid.columns[colIndex].dataIndex;
    metadata.tdAttr = 'data-qtip="' + record.get(dataIndex) + '"';
    return value;
}
function sourceSublevelRender(value, metadata, record, rowIndex, colIndex, store) {
	metadata.tdAttr = 'data-qtip="' + Ext.Date.format(value,'m/d/Y') + '"';
	return Ext.Date.format(value,'m/d/Y');
}
Ext.define('tz.service.ClientService', {
	extend : 'tz.service.Service',
		
	saveClients : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/saveClients',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    deleteClient : function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/deleteClient/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    saveclient : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/saveclient',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
});

Ext.define('tz.ui.ClientGridPanel', {
    extend: 'Ext.grid.Panel',
    title: 'Clients',
    frame:true,
    anchor:'100%',
    height:560,
    width :'100%',
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value){
    			this.modifiedIds.push(record.data.id);
    		}
    		if(editor.context.field == 'contactNumber'){
    			record.data.contactNumber = record.data.contactNumber.replace(/[^0-9]/g, "");
            	record.data.contactNumber = record.data.contactNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
            	this.modifiedIds.push(record.data.id);
            	this.getView().refresh();
            }
            
        }
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.clientStore;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :40,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'View client',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					app.clientMgmtPanel.showClient(rowIndex);
    				}
    			}]
            },{
				text : 'Unique Id',
				dataIndex : 'id',
				width :  70,
		        renderer: clientRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'name',
                text: 'Name',
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:50,
   	                enforceMaxLength:true,
                }
            },{
    			text : 'Type',
    			dataIndex : 'type',
                renderer: clientRender,
                editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    forceSelection:true,
                    displayField: 'value',
        		    valueField: 'name',
                    store: ds.clientTypeStore,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                },
    		},{
                xtype: 'gridcolumn',
                dataIndex: 'score',
                text: 'Score',
                width:80,
                renderer: clientRender,
                editor: {
                    xtype: 'numberfield',
                    minValue: 0,
                    maxValue: 100
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'cityAndState',
                text: 'City & State',
                width:100,
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:90,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'address',
                text: 'Address',
                width:140,
                renderer: clientRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:200,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:140,
                renderer: clientRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:250,
   	                enforceMaxLength:true,
                }
            },{
				header : 'Contractors',
				dataIndex : 'contractorId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != "") {
    					var ids = value.split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.candidateSearchStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.firstName+' '+rec.data.lastName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return '('+ids.length+') '+names.substring(0,names.length-2);
					}else
						return value;
                }
			},{
				header : 'Full Time',
				dataIndex : 'fullTimeId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != "") {
    					var ids = value.split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.candidateSearchStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.firstName+' '+rec.data.lastName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return '('+ids.length+') '+names.substring(0,names.length-2);
					}else
						return value;
                }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'contactNumber',
                text: 'Contact Number',
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:15,
                 	maskRe :  /[0-9]/,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'fax',
                text: 'Fax',
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:50,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'email',
                text: 'Email',
                width:120,
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:100,
   	                enforceMaxLength:true,
   	                //vtype:'email',
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'website',
                text: 'Website',
                width:120,
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:100,
   	                enforceMaxLength:true,
   	                //vtype:'url',
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'industry',
                text: 'Industry',
                renderer: clientRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:50,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'about',
                text: 'About',
                width:140,
                renderer: clientRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:100,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: clientRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:150,
            }
        ];
        me.viewConfig = {
        		stripeRows: true	
        };
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        iconCls : 'btn-add',
                        tabIndex:11,
                        handler: function(){
                    		me.addClient();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        tabIndex:11,
                        handler: function(){
                    		me.saveClients();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Delete',
                        iconCls : 'btn-delete',
                        tabIndex:11,
                        handler: function(){
                    		me.deleteConfirm();
                    	}
                    },'-',{
                        xtype:'button',
                    	itemId: 'grid-excel-button',
                    	iconCls : 'btn-report-excel',
                        text: 'Export to Excel',
                        tabIndex:12,
                        handler: function(){
                        	me.getExcelExport();
                        	/*var vExportContent = me.getExcelXml();
                            document.location='data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);*/
                        }
                    },'->',me.showCount
                    ]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        me.callParent(arguments);
    },
    
    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],
    
    getExcelExport : function() {
    	var values = app.clientMgmtPanel.clientSearchPanel.getValues();
    	var params = new Array();
    	params.push(['type','=', app.clientMgmtPanel.clientSearchPanel.typeCombo.getValue()]);
    	params.push(['id','=', app.clientMgmtPanel.clientSearchPanel.clientCombo.getValue()]);
    	params.push(['fullTime','=', app.clientMgmtPanel.clientSearchPanel.fullTimeCombo.getValue()]);
    	params.push(['contractor','=', app.clientMgmtPanel.clientSearchPanel.contractorCombo.getValue()]);
    	params.push(['score','=', values.score]);
    	params.push(['search','=', values.search]);
    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;") {
				columns.push(this.columns[i].text);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.requirementService.clientExcelExport(Ext.encode(filter));
	},

    addClient : function() {
	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.Client',{
   	 		id : null,
   	 		comments :'N/A',
   	 		email :'N/A',
   	 		website :'N/A',
   	 		contactNumber :'N/A',
   	 		fax :'N/A',
   	 		industry :'N/A',
   	 		about :'N/A',
   	 		address :'N/A',
   	 		cityAndState :'N/A',
   	 		lastUpdatedUser :null,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});
   	 	
        this.store.insert(0, rec);
	},
    
	validateClient : function() {
    	for ( var i = 0; i < ds.clientStore.data.length ; i++) {
			var record = ds.clientStore.getAt(i);
    		if (record.data.name == null || record.data.name == ""){
    			Ext.Msg.alert('Error','Client name should not be blank');
    			return false;
    		}
		}
    	return true;
	},
	
    saveClients : function() {
    	if (this.validateClient()) {
        	var records = new Array();
        	for ( var i = 0; i < ds.clientStore.data.length ; i++) {
    			var record = ds.clientStore.getAt(i);
        		if (record.data.id == 0 || record.data.id == null){
        			delete record.data.id;
        			records.push(record.data);
        		}

        		if (this.modifiedIds.indexOf(record.data.id) != -1)
        			records.push(record.data);	
    		}
        	if (records.length == 0) {
        		Ext.Msg.alert('Error','There are no updated Clients to save');
        		return;
    		}
        	records = Ext.JSON.encode(records);
        	app.clientService.saveClients(records,this.onSave,this);
		}
	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error','Saved Successfully');
		this.modifiedIds =  new Array();
		app.clientMgmtPanel.clientSearchPanel.search();
	},
	
	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Client to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Client","Do you delete selected Client?", this.deleteClient, this);
		}
	},
	
	deleteClient: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.clientService.deleteClient(selectedRecord.data.id, this.onDeleteClient, this);
		}
	},
	
	onDeleteClient : function(data){
		if (!data.success) {
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.clientMgmtPanel.clientSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	}


});

function clientRender(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null || value=="") 
		return 'N/A';
    return value;
}

function dateRender(value, metadata, record, rowIndex, colIndex, store) {
    metadata.tdAttr = 'data-qtip="' + Ext.Date.format(value,'m/d/Y') + '"';
    return Ext.Date.format(value,'m/d/Y');
}
Ext.define('tz.ui.ClientMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.clientGridPanel = new tz.ui.ClientGridPanel({manager:me});
    	me.clientSearchPanel = new tz.ui.ClientSearchPanel({manager:me});
    	me.clientPanel = new tz.ui.ClientPanel({manager:me});
    	
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [ this.clientSearchPanel,
 				        {
		   	        		xtype: 'container',
		   	        		height: 10,
		   	        		border : false
 				        },
 				        this.clientGridPanel
 				        ]
            },{
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [this.clientPanel]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen : function(){
		this.getLayout().setActiveItem(0);
		ds.clientTypeStore.loadAll();
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
		ds.clientSearchStore.loadByCriteria(filter);
		if(app.candidateMgmtPanel != null || ds.candidateSearchStore.getCount() == 0){
    		ds.candidateSearchStore.loadByCriteria(filter);
		}
		if (ds.clientStore.getCount() ==0) 
    		this.clientSearchPanel.search();	
	},
	
	showClient : function(rowIndex) {
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var record = ds.clientStore.getAt(rowIndex);
		}
		this.clientPanel.loadForm(record);
	} 
	
	
});
Ext.define('tz.ui.ClientSearchPanel', {
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    frame:true,
    layout:'anchor',
    anchor:'100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth: 80},
//creating view in init function
    initComponent: function() {
        var me = this;
        
		me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    forceSelection:true,
		    queryMode: 'local',
            displayField: 'value',
		    valueField: 'name',
            store: ds.clientTypeStore,
		    tabIndex:2,
		});

		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client/Vendor',
		    queryMode: 'local',
            displayField: 'name',
            valueField: 'id',
            emptyText:'Select...',
            listClass: 'x-combo-list-small',
            store: ds.clientSearchStore,
		    tabIndex:2,
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});

		me.contractorCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Contractors',
		    store: ds.candidateSearchStore,
		    queryMode: 'local',
            displayField: 'fullName',
            valueField: 'id',
		    name : 'contractorId',
		    tabIndex:7,
		});
		
		me.fullTimeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Full Time',
		    store: ds.candidateSearchStore,
		    queryMode: 'local',
            displayField: 'fullName',
            valueField: 'id',
		    name : 'fullTimeId',
		    tabIndex:7,
		});

        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.33,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Search',
	   	                 name: 'search',
	   	                 tabIndex:1,
	   	             },{
	   	                 xtype:'numberfield',
	   	                 fieldLabel: 'Score',
	   	                 name: 'score',
	   	                 tabIndex:2,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.33,
	   	             items: [me.clientCombo,me.typeCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.33,
	   	             items: [me.contractorCombo,me.fullTimeCombo]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:2,
			},{
				text : 'Reset',
				scope: this,
				handler: this.reset,
				tabIndex:3,
			},{
				text : 'Clear',
				scope: this,
				tabIndex:4,
	             handler: function() {
	            	 this.form.reset();
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['type','=', this.typeCombo.getValue()]);
    	params.push(['id','=', this.clientCombo.getValue()]);
    	params.push(['fullTime','=', this.fullTimeCombo.getValue()]);
    	params.push(['contractor','=', this.contractorCombo.getValue()]);
    	params.push(['score','=', values.score]);
    	params.push(['search','=', values.search]);
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=200;
    	ds.clientStore.loadByCriteria(filter);
    	app.clientMgmtPanel.clientGridPanel.modifiedIds = new Array();
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
Ext.define('tz.ui.ClientPanel', {
	extend : 'tz.ui.BaseFormPanel',
	
	bodyPadding: 10,
    title: '',
    anchor:'100% 95%',
    
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},
	
	initComponent : function() {
		 var me = this;
		 me.openedFrom ="";
		 
		me.vendorTypeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    store: ds.clientTypeStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:false,
		    valueField: 'name',
		    name : 'type',
		    tabIndex:2,
		});
		
		me.candidateCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Contractors',
		    store: ds.candidateSearchStore,
		    queryMode: 'local',
		    forceSelection:true,
            displayField: 'fullName',
            valueField: 'id',
		    name : 'contractorId',
		    tabIndex:10,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					this.form.findField('contractors').setValue(combo.rawValue);
				}
		    }
		});
		
		me.fullTimeCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Full Time',
		    store: ds.candidateSearchStore,
		    queryMode: 'local',
		    forceSelection:true,
            displayField: 'fullName',
            valueField: 'id',
		    name : 'fullTimeId',
		    tabIndex:11,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					this.form.findField('fullTime').setValue(combo.rawValue);
				}
		    }
		});

		me.items = [ me.getMessageComp(), me.getHeadingComp(), {
	         xtype: 'container',
	         items:[
			         {xtype: 'hidden', name: 'id'},
	                {
				xtype : 'fieldset',
				title : 'Client/Vendor Information',
				collapsible : true,
				defaultType : 'textfield',
				layout : 'column',
				items : [
				         {
	             xtype: 'container',
	             columnWidth:.4,
	             items: [{
	                 xtype:'textfield',
	                 fieldLabel: 'Name',
	                 name: 'name',
	                 allowBlank:false,
	                 tabIndex:1,
	                 maxLength : 50,
	                 enforceMaxLength:true,
	             },
	             this.vendorTypeCombo,
	             {
	                 xtype:'textfield',
	                 fieldLabel: 'Contact Number',
	                 name: 'contactNumber',
	                 maxLength : 15,
	                 maskRe :  /[0-9]/,
	                 tabIndex:3,
	                 enforceMaxLength:true,
   	                 listeners:{
   	                	 change : function(field,newValue,oldValue){
   	                		 var value = me.form.findField('contactNumber').getValue();
   	                		 if (value != "" ) {
   	                			 value = value.replace(/[^0-9]/g, "");
	   	                		 value = value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
	   	                		 me.form.findField('contactNumber').setValue(value);
   	                		 }
					    }
   	                 }
	             },{
	                 xtype:'numberfield',
	                 fieldLabel: 'Score',
	                 name: 'score',
	                 maxLength : 12,
	                 tabIndex:4,
	                 enforceMaxLength:true,
	             },{
	                 xtype:'textfield',
	                 fieldLabel: 'Fax',
	                 name: 'fax',
	                 maxLength : 50,
	                 tabIndex:5,
	                 enforceMaxLength:true,
	             },{
	                 xtype:'textfield',
	                 fieldLabel: 'Email',
	                 name: 'email',
   	                 //vtype:'email',
   	                 tabIndex:6,
	                 maxLength : 100,
	                 enforceMaxLength:true,
	             },{
	                 xtype:'textfield',
	                 fieldLabel: 'Website',
	                 name: 'website',
   	                 //vtype:'url',
   	                 tabIndex:7,
	                 maxLength : 100,
	                 enforceMaxLength:true,
	             },{
	                 xtype:'textfield',
	                 fieldLabel: 'Industry',
	                 name: 'industry',
	                 tabIndex:8,
	                 maxLength : 50,
	                 enforceMaxLength:true,
	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "City & State",
   	                 name: 'cityAndState',
   	                 tabIndex:9,
   	                 maxLength:90,
   	                 enforceMaxLength:true,
   	             },
	             me.candidateCombo,me.fullTimeCombo,
	             ]
	         },{
	             xtype: 'container',
	             columnWidth:.5,
	             items: [{
	            	 xtype : 'textarea',
	            	 fieldLabel : 'Comments',
	            	 name : 'comments',
	            	 width:400,
	            	 maxLength : 250,
	            	 enforceMaxLength:true,
	            	 tabIndex:12,
				},{
	                 xtype:'textareafield',
	                 fieldLabel: 'Address',
	                 name: 'address',
	                 width:400,
	                 maxLength : 200,
	                 enforceMaxLength:true,
	                 tabIndex:13,
	             },{
	            	 xtype : 'textarea',
	            	 fieldLabel : 'About',
	            	 name : 'about',
	            	 width:400,
	            	 maxLength : 100,
	            	 enforceMaxLength:true,
	            	 tabIndex:14,
				},{
   	                 xtype:'textarea',
   	                 fieldLabel: 'Selected Contractors',
   	                 name: 'contractors',
   	                 readOnly:true,
   	                 width:400,
   	                 height:70,
   	             },{
   	                 xtype:'textarea',
   	                 fieldLabel: 'Selected Full Time',
   	                 name: 'fullTime',
   	                 readOnly:true,
   	                 width:400,
   	                 height:70,
   	             },{
					xtype : 'datefield',
	            	 name : 'created',
	            	 hidden : true
				}]
	         }]
	        }]
	     }
	   	 ];
		
		
        me.saveButton = new Ext.Button({
			text : 'Save',
			iconCls : 'btn-save',
			tabIndex:17,
			scope: this,
	        handler: this.saveClient
        });

        me.deleteButton = new Ext.Button({
			text : 'Delete',
			iconCls : 'btn-delete',
			tabIndex:18,
			scope: this,
	        handler: this.deleteConfirm
        });

        me.closeButton = new Ext.Button({
			text : 'Close',
			iconCls : 'btn-close',
			tabIndex:19,
			scope: this,
	        handler: this.closeFormDirty
        });

		me.tbar =  [ this.saveButton, '-', this.deleteButton , '-', this.closeButton ];

		this.callParent(arguments);
	},
	
	closeFormDirty: function(){
		if (this.openedFrom == "Recruit") {
			if(app.requirementMgmtPanel == null){
				app.requirementMgmtPanel = new tz.ui.RequirementMgmtPanel({itemId:'requirementMgmtPanel'});
				app.centerLayout.add(this.requirementMgmtPanel);
			}    		
			app.centerLayout.getLayout().setActiveItem('requirementMgmtPanel');
		}else{
			app.clientMgmtPanel.initScreen();
			app.clientMgmtPanel.clientSearchPanel.search();
		}
	},
	
	
    loadForm : function(record) {
    	this.clearMessage();
    	this.form.reset();
		this.loadRecord(record);
		this.openedFrom = "Client";
		var contractorIds = record.data.contractorId.split(',');
		contractorIds = contractorIds.map(Number);
		this.candidateCombo.setValue(contractorIds);
		var fullTimeIds = record.data.fullTimeId.split(',');
		candidateCombo = fullTimeIds.map(Number);
		this.fullTimeCombo.setValue(candidateCombo);
		
	},

	saveClient: function(){
		this.clearMessage();
		if(! this.form.isValid()){
			this.updateError('Please fix the errors.');
			return false;
		}
		var itemslength = this.form.getFields().getCount();
   		var i = 0;    		
   		var client = {};
   		
   		while (i < itemslength) {
   			var fieldName = this.form.getFields().getAt(i).name;
   				if(fieldName == 'id') {
   					var idVal = this.form.findField(fieldName).getValue();
   					if(typeof(idVal) != 'undefined' && idVal > 0) {
   						client['id'] = this.form.findField(fieldName).getValue();
   					}
   				}else if (fieldName == 'score') {
   					var score = this.form.findField(fieldName).getValue();
   					if(typeof(score) != 'undefined' && score > 0) {
   						client['score'] = this.form.findField(fieldName).getValue();
   					}
				}else if (fieldName == 'contractorId' || fieldName == 'fullTimeId' ) {
					var list = this.form.findField(fieldName).getValue();
					if (list != null)
						client[fieldName] = this.form.findField(fieldName).getValue().toString();
   				}else {
   					client[fieldName] = this.form.findField(fieldName).getValue();
   				}
   			i = i + 1;
   		}
		
		var clientObj = Ext.JSON.encode(client);
		app.clientService.saveclient(clientObj, this.onSaveClient, this);
		
	},
	
	onSaveClient: function(data){
		if(!data.success){
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
			
		}else{
			if (this.openedFrom == "Recruit") {
				app.requirementMgmtPanel.requirementsGrid.getRecruitmentTab();
			}else{
				this.getForm().findField('id').setValue(data.returnVal.id);
				this.updateMessage('Saved successfully');
			}
		}
		this.body.scrollTo('top', 0);
	},
	
	deleteConfirm : function()
	{
		Ext.Msg.confirm("This will delete the Client", "Do you want to continue?", this.deleteClient, this);
	},
	
	onDelete : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			app.clientMgmtPanel.initScreen();
		}
		this.body.scrollTo('top', 0);
	},
	
	deleteClient: function(dat){
		if(dat=='yes'){
			var vendor = this.form.getValues();
			app.clientService.deleteClient(vendor.id, this.onDelete, this);
		}
	},
	
	
});
Ext.define('tz.service.ContactService', {
	extend : 'tz.service.Service',
		
	saveContacts : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/saveContacts',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    deleteContact : function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/deleteContact/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

});

Ext.define('tz.ui.ContactMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.contactGridPanel = new tz.ui.ContactGridPanel({manager:me});
    	me.contactSearchPanel = new tz.ui.ContactSearchPanel({manager:me});
        
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [ this.contactSearchPanel,
 				        {
		   	        		xtype: 'container',
		   	        		height: 10,
		   	        		border : false
 				        },
 				        this.contactGridPanel
 				        ]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen: function(){
		this.getLayout().setActiveItem(0);
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.clientSearchStore.loadByCriteria(filter);
    	if (ds.contactStore.getCount() ==0) 
    		this.contactSearchPanel.search();	
	},
	
	
});
Ext.define('tz.ui.ContactGridPanel', {
    extend: 'Ext.grid.Panel',
    title: 'Contacts',
    frame:true,
    anchor:'100%',
    height:560,
    width :'100%',
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value){
    			this.modifiedIds.push(record.data.id);
    		}
    		if(editor.context.field == 'cellPhone'){
    			record.data.cellPhone = record.data.cellPhone.replace(/[^0-9]/g, "");
            	record.data.cellPhone = record.data.cellPhone.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
            	this.getView().refresh();
            }
    		if(editor.context.field == 'clientId'){
            	var client = ds.clientSearchStore.getAt(ds.clientSearchStore.find('id',record.data.clientId));
            	record.data.type = client.data.type;
            	this.modifiedIds.push(record.data.id);
            	this.getView().refresh();
            }
        }
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.contactStore;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
				text : 'Unique Id',
				dataIndex : 'id',
				width :  70,
		        renderer: contactRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'firstName',
                text: 'First Name',
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastName',
                text: 'Last Name',
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
				header : 'Client/Vendor',
				dataIndex : 'clientId',
	            width :  120,
		        editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                forceSelection:true,
	                triggerAction: 'all',
	                displayField: 'name',
	                valueField: 'id',
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.clientSearchStore,
	            },
                renderer:function(value, metadata, record){
                	var rec = ds.clientSearchStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.name + '"';
                		return rec.data.name;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'type',
                text: 'Type',
                renderer: contactRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'score',
                text: 'Score',
                width:80,
                renderer: contactRender,
                editor: {
                    xtype: 'numberfield',
                    minValue: 0,
                    maxValue: 100
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'email',
                text: 'Email',
                width:120,
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
   	                vtype:'email',
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'jobTitle',
                text: 'Job Title',
                width:120,
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'internetLink',
                text: 'Internet Link',
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:50,
   	                enforceMaxLength:true,
   	                vtype:'url',
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'cellPhone',
                text: 'CellPhone',
                renderer: contactRender,
                editor: {
                    xtype: 'textfield',
                    maxLength:15,
                 	maskRe :  /[0-9]/,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'extension',
                text: 'Extension',
                editor: {
                    xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:140,
                renderer: contactRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:50,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'interviews',
                text: 'Interviewer',
                renderer:function(value, p, record){
                	if (value > 0) 
						return 'Yes';
                	else
                		return 'No';
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: contactRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:150,
            }
        ];
        me.viewConfig = {
        		stripeRows: true	
        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        iconCls : 'btn-add',
                        tabIndex:11,
                        handler: function(){
                    		me.addContact();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        tabIndex:11,
                        handler: function(){
                    		me.saveContacts();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Delete',
                        iconCls : 'btn-delete',
                        tabIndex:11,
                        handler: function(){
                    		me.deleteConfirm();
                    	}
                    },'-',{
                        xtype:'button',
                    	itemId: 'grid-excel-button',
                    	iconCls : 'btn-report-excel',
                        text: 'Export to Excel',
                        tabIndex:12,
                        handler: function(){
                        	me.getExcelExport();
                        	/*var vExportContent = me.getExcelXml();
                            document.location='data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);*/
                        }
                    },'->',me.showCount
                    ]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        ds.clientSearchStore.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);

        me.callParent(arguments);
    },
    
    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],
    
    addContact : function() {
	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.Contact',{
   	 		comments :'N/A',
   	 		email :'N/A',
   	 		jobTitle :'N/A',
   	 		internetLink :'N/A',
   	 		cellPhone :'N/A',
   	 		extension :'N/A',
   	 		lastUpdatedUser :null,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});
   	 	
        this.store.insert(0, rec);
	},
    
    getExcelExport : function() {
    	var values = app.contactMgmtPanel.contactSearchPanel.getValues();
    	var params = new Array();
    	params.push(['type','=', app.contactMgmtPanel.contactSearchPanel.typeCombo.getValue()]);
    	params.push(['client','=', app.contactMgmtPanel.contactSearchPanel.clientCombo.getValue()]);
    	params.push(['score','=', values.score]);
    	params.push(['search','=', values.search]);
    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;") {
				columns.push(this.columns[i].text);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.requirementService.contactExcelExport(Ext.encode(filter));
	},

	saveContacts : function() {
    	var records = new Array();
    	for ( var i = 0; i < ds.contactStore.data.length ; i++) {
			var record = ds.contactStore.getAt(i);
    		if (record.data.clientId == 0 || record.data.clientId == null)
				delete record.data.clientId;
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}else if (this.modifiedIds.indexOf(record.data.id) != -1)
    			records.push(record.data);	
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Contacts to save');
    		return;
		}
    	records = Ext.JSON.encode(records);
    	app.contactService.saveContacts(records,this.onSave,this);
	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error','Saved Successfully');
		this.modifiedIds =  new Array();
		app.contactMgmtPanel.contactSearchPanel.search();
	},
	
	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Contact to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Contact","Do you delete selected Contact?", this.deleteContact, this);
		}
	},
	
	deleteContact: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.contactService.deleteContact(selectedRecord.data.id, this.onDeleteContact, this);
		}
	},
	
	onDeleteContact : function(data){
		if (!data.success) {
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.contactMgmtPanel.contactSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	}


});

function contactRender(value, metadata, record, rowIndex, colIndex, store) {
    var dataIndex = app.contactMgmtPanel.contactGridPanel.columns[colIndex].dataIndex;
    metadata.tdAttr = 'data-qtip="' + record.get(dataIndex) + '"';
    return value;
}
Ext.define('tz.ui.ContactSearchPanel', {
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    frame:true,
    layout:'anchor',
    anchor:'100%',
    autoScroll:true,
//creating view in init function
    initComponent: function() {
        var me = this;
        
		me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    forceSelection:true,
		    queryMode: 'local',
            displayField: 'value',
		    valueField: 'name',
            store: ds.clientTypeStore,
		    tabIndex:3,
		});

		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client/Vendor',
		    queryMode: 'local',
            displayField: 'name',
            valueField: 'id',
            emptyText:'Select...',
            listClass: 'x-combo-list-small',
            store: ds.clientSearchStore,
		    tabIndex:2,
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});

        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.33,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Search',
	   	                 name: 'search',
	   	                 tabIndex:33,
	   	             },{
	   	                 xtype:'numberfield',
	   	                 fieldLabel: 'Score',
	   	                 name: 'score',
	   	                 tabIndex:2,
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.33,
	   	             items: [me.clientCombo,me.typeCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.33,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Unique Ids(EX:1,2)',
	   	                 name: 'id',
	   	                 labelWidth :115,
	   	                 tabIndex:3
	   	             }]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:4,
			},{
				text : 'Reset',
				scope: this,
				handler: this.reset,
				tabIndex:5,
			},{
				text : 'Clear',
				scope: this,
				tabIndex:6,
	             handler: function() {
	            	 this.form.reset();
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['type','=', this.typeCombo.getValue()]);
    	params.push(['client','=', this.clientCombo.getValue()]);
    	params.push(['score','=', values.score]);
    	params.push(['search','=', values.search]);
    	var id = values.id;
    	id = id.replace(',,',",");
    	while (id.length >0) {
        	if (id.substr(id.length-1,id.length) == ",")
        		id = id.substr(0,id.length-1);
			else
				break;
		}
    	if (id.search(',') != -1) 
    		id = "'"+id.replace(/,/g, "','")+"'";

    	params.push(['id','=', id]);

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=200;
    	ds.contactStore.loadByCriteria(filter);
    	app.contactMgmtPanel.contactGridPanel.modifiedIds = new Array();
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
Ext.define('tz.service.InterviewService', {
	extend : 'tz.service.Service',
		
	saveInterviews : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/interview/saveInterviews',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    deleteInterview : function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/interview/deleteInterview/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

});

Ext.define('tz.ui.InterviewGridPanel', {
    extend: 'Ext.grid.Panel',
    title: 'Interviews',
    frame:true,
    anchor:'100%',
    height:560,
    width :'100%',
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value){
    			this.modifiedIds.push(record.data.id);
    		}
    		if (editor.context.field == 'time') {
    			var value = record.data.time;
    			if (value.toString().length > 8) {
    				var hour    = value.getHours();
    			    var minute  = value.getMinutes();
    			    var second  = value.getSeconds(); 
    			    if(hour.toString().length == 1) 
    			        var hour = '0'+hour;
    			    if(minute.toString().length == 1) 
    			        var minute = '0'+minute;
    			    if(second.toString().length == 1) 
    			        var second = '0'+second;
    			    
    			    var dateTime = hour+':'+minute+':'+second; 
    			    record.data.time = dateTime;
    			}
    			this.getView().refresh();
    		}
        }
    },
    
    initComponent: function() {
        var me = this;
        me.store = ds.interviewStore;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
				header : 'Candidate',
				dataIndex : 'candidateId',
	            width :  120,
		        editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                forceSelection:true,
	                triggerAction: 'all',
	                displayField: 'fullName',
	                valueField: 'id',
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.candidateSearchStore,
	            },
                renderer:function(value, metadata, record){
                	var rec = ds.candidateSearchStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.firstName+' '+rec.data.lastName + '"';
                		return rec.data.firstName+' '+rec.data.lastName;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
				header : 'Client',
				dataIndex : 'clientId',
	            width :  120,
		        editor: {
	                xtype: 'combobox',
	                forceSelection:true,
	                queryMode: 'local',
	                triggerAction: 'all',
	                displayField: 'name',
	                valueField: 'id',
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.clientSearchStore,
	            },
	            renderer:function(value, metadata, record){
                	var rec = ds.clientSearchStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.name + '"';
                		return rec.data.name;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
				header : 'Vendor',
				dataIndex : 'vendorId',
	            width :  120,
		        editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                triggerAction: 'all',
	                displayField: 'name',
	                valueField: 'id',
	                forceSelection:true,
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.vendorStore,
	            },
	            renderer:function(value, metadata, record){
                	var rec = ds.vendorStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.name + '"';
                		return rec.data.name;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
                dataIndex: 'contactId',
                text: 'Interviewer',
                width:120,
                editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                triggerAction: 'all',
	                displayField: 'fullName',
	                valueField: 'id',
	                forceSelection:true,
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.contactSearchStore,
	            },
	            renderer:function(value, metadata, record){
                	var rec = ds.contactSearchStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.fullName + '"';
                		return rec.data.fullName;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
            },{
                dataIndex: 'interviewDate',
                text: 'Interview Date',
                width:120,
                renderer: interviewDateRender,
                editor: {
                    xtype: 'datefield',
                    format: 'm/d/y'
                }
            },{
                dataIndex: 'time',
                text: 'Time',
                width:120,
                renderer: interviewRender,
                editor: {
                    xtype: 'timefield',
                    increment: 1,
                    anchor: '100%'
                },
            },{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:200,
                renderer: interviewRender,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:500,
   	                enforceMaxLength:true,
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: interviewRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                //format: "m/d/Y H:i:s A",
                renderer: lastUpdatedRender,
                text: 'Last Updated',
                width:150,
            }
        ];
        me.viewConfig = {
        		stripeRows: true	
        };
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        iconCls : 'btn-add',
                        tabIndex:11,
                        handler: function(){
                    		me.addInterview();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        tabIndex:11,
                        handler: function(){
                    		me.saveInterviews();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Delete',
                        iconCls : 'btn-delete',
                        tabIndex:11,
                        handler: function(){
                    		me.deleteConfirm();
                    	}
                    },'-',{
                        xtype:'button',
                    	itemId: 'grid-excel-button',
                    	iconCls : 'btn-report-excel',
                        text: 'Export to Excel',
                        tabIndex:12,
                        handler: function(){
                        	var vExportContent = me.getExcelXml();
                            document.location='data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);
                        }
                    },'->',me.showCount
                    ]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        ds.candidateSearchStore.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);

        ds.clientSearchStore.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);

        ds.vendorStore.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);

        ds.contactSearchStore.on('load', function(store, records, options) {
        	this.getView().refresh();
       	}, me);

        me.callParent(arguments);
    },
    
    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],
    
    addInterview : function() {
	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.Interview',{
   	 		id : null,
   	 		comments :'',
   	 		lastUpdatedUser :null,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});
   	 	
        this.store.insert(0, rec);
	},
    
    saveInterviews : function() {
    	var records = new Array();
    	for ( var i = 0; i < this.store.data.length ; i++) {
			var record = this.store.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}
    		if (record.data.clientId == '' || record.data.clientId == null)
				delete record.data.clientId;
    		if (record.data.vendorId == '' || record.data.vendorId == null)
				delete record.data.vendorId;
    		if (record.data.candidateId == '' || record.data.candidateId == null)
				delete record.data.candidateId;
    		if (record.data.contactId == '' || record.data.contactId == null)
				delete record.data.contactId;
    		
    		if (this.modifiedIds.indexOf(record.data.id) != -1)
    			records.push(record.data);	
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Interviews to save');
    		return;
		}
    	records = Ext.JSON.encode(records);
    	app.interviewService.saveInterviews(records,this.onSave,this);
	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error','Saved Successfully');
		this.modifiedIds =  new Array();
		app.interviewMgmtPanel.interviewSearchPanel.search();
	},
	
	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select an Interview to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Interview","Do you delete selected Interview?", this.deleteInterview, this);
		}
	},
	
	deleteInterview: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.interviewService.deleteInterview(selectedRecord.data.id, this.onDeleteInterview, this);
		}
	},
	
	onDeleteInterview : function(data){
		if (!data.success) {
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.interviewMgmtPanel.interviewSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	}


});

function interviewRender(value, metadata, record) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    return value;
}

function interviewDateRender(value, metadata, record) {
	if (value != null && value != "") {
	    metadata.tdAttr = 'data-qtip="' + Ext.Date.format(new Date(value),'m/d/Y') + '"';
	    return Ext.Date.format(new Date(value),'m/d/Y');
	}
}

function lastUpdatedRender(value, metadata, record) {
    metadata.tdAttr = 'data-qtip="' + Ext.Date.format(new Date(value),'m/d/Y H:i:s A') + '"';
    return Ext.Date.format(new Date(value),'m/d/Y H:i:s A');
}
Ext.define('tz.ui.InterviewMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.interviewGridPanel = new tz.ui.InterviewGridPanel({manager:me});
    	me.interviewSearchPanel = new tz.ui.InterviewSearchPanel({manager:me});
    	
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [ this.interviewSearchPanel,
 				        {
		   	        		xtype: 'container',
		   	        		height: 10,
		   	        		border : false
 				        },
 				        this.interviewGridPanel
 				        ]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen : function(){
		this.getLayout().setActiveItem(0);
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
   		ds.candidateSearchStore.loadByCriteria(filter);
    	ds.contactSearchStore.loadByCriteria(filter);

    	params = new Array();
    	params.push(['type','=', 'Client']);
    	filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.clientSearchStore.loadByCriteria(filter);

    	params = new Array();
    	params.push(['type','=', 'Vendor']);
    	filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.vendorStore.loadByCriteria(filter);

    	if (ds.interviewStore.getCount() ==0) 
    		this.interviewSearchPanel.search();	
	},
	
	showClient : function(rowIndex) {
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var record = ds.clientStore.getAt(rowIndex);
		}
		this.clientPanel.loadForm(record);
	} 
	
	
});
Ext.define('tz.ui.InterviewSearchPanel', {
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    frame:true,
    layout:'anchor',
    anchor:'100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth: 80},
//creating view in init function
    initComponent: function() {
        var me = this;
        
		me.candidateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Candidate',
		    store: ds.candidateSearchStore,
		    queryMode: 'local',
		    displayField: 'fullName',
		    valueField: 'id',
		    name : 'candidate',
		    tabIndex:1,
		});

		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client',
            store: ds.clientSearchStore,
		    queryMode: 'local',
            displayField: 'name',
            valueField: 'id',
		    name : 'client',
		    tabIndex:2,
		});

		me.vendorCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Vendor',
            store: ds.vendorStore,
		    queryMode: 'local',
            displayField: 'name',
            valueField: 'id',
		    name : 'vendor',
		    tabIndex:3,
		});

		me.contactCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Interviewer',
            store: ds.contactSearchStore,
		    queryMode: 'local',
            displayField: 'fullName',
            valueField: 'id',
		    name : 'interviewer',
		    tabIndex:4,
		});

		me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.3,
	   	             items: [me.candidateCombo,me.clientCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.3,
	   	             items: [me.vendorCombo,me.contactCombo]
	   	         },{
	   	        	 xtype: 'container',
	   	             columnWidth:.3,
	   	             items: [{
	   	            	 xtype:'datefield',
	   	            	 fieldLabel: 'Interview Date From',
	   	            	 name: 'interviewDateFrom',
	   	            	 labelWidth :132,
	   	            	 tabIndex:5
	   	             },{
	   	            	 xtype:'datefield',
		   	        	 fieldLabel: 'Interview Date To',
		   	        	 name: 'interviewDateTo',
		   	        	 labelWidth :132,
		   	        	 tabIndex:6
		   	         }]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:7,
			},{
				text : 'Reset',
				scope: this,
				handler: this.reset,
				tabIndex:8,
			},{
				text : 'Clear',
				scope: this,
				tabIndex:9,
	             handler: function() {
	            	 this.form.reset();
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['candidate','=', values.candidate]);
    	params.push(['client','=', values.client]);
    	params.push(['vendor','=', values.vendor]);
    	params.push(['interviewer','=', values.interviewer]);
    	if (values.interviewDateFrom != undefined && values.interviewDateFrom != '') {
    		var interviewDateFrom = new Date(values.interviewDateFrom);
        	params.push(['interviewDateFrom','=',interviewDateFrom]);
    	}
    	if (values.interviewDateTo != undefined && values.interviewDateTo != '') {
    		var interviewDateTo = new Date(values.interviewDateTo);
        	params.push(['interviewDateTo','=',interviewDateTo]);
    	}

    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=50;
    	ds.interviewStore.loadByCriteria(filter);
    	app.interviewMgmtPanel.interviewGridPanel.modifiedIds = new Array();
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
Ext.define('tz.service.ReportsService', {
	extend : 'tz.service.Service',
		
    
    getResult : function(query,  cb, scope){
	    this.onAjaxRawResponse = Ext.bind(this.onAjaxRawResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/getResult',
			params : {
				json : (query)
			},
			success: this.onAjaxRawResponse,
			failure: this.onAjaxRawResponse
		});
		app.loadMask.show();
    },

    getComboValues : function(query,  cb, scope){
	    this.onAjaxRawResponse = Ext.bind(this.onAjaxRawResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/getComboValues',
			params : {
				json : (query)
			},
			success: this.onAjaxRawResponse,
			failure: this.onAjaxRawResponse
		});
		app.loadMask.show();
    },

    
});
Ext.define('tz.ui.ReportsMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',
    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;
        
    	me.reportsDetailPanel = new tz.ui.ReportsDetailPanel({manager:me});
		
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,    	                 
            	border:false,  
            	items: [me.reportsDetailPanel]
            }
        ];
        me.callParent(arguments);
    },


	initScreen: function(){
		this.getLayout().setActiveItem(0);
		this.reportsDetailPanel.readReportsFile();
	},
	
});
Ext.define('tz.ui.ReportsDetailPanel',{
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    anchor:'100%',
    autoScroll:true,
//creating view in init function
    initComponent: function() {
        var me = this;
        
	    Ext.define('dataview_model', {
	        extend    : 'Ext.data.Model',
	        fields  : [
	            {name: 'count',       type: 'string'},
	            {name: 'maxcolumns',  type: 'string'}
	        ]
	    });

	    Ext.create('Ext.data.Store', {
	    	storeId : 'viewStore',
            model    : 'dataview_model',
            data    : [
                {count: '7', maxcolumns: '10'}
            ],
        });

        var tpl = new Ext.XTemplate(
            "<table width='100%' border='1'>" +
            "</table>"
        );

        me.dataView = Ext.create('Ext.view.View', {
        	    store: Ext.data.StoreManager.lookup('viewStore'),
        	    tpl: tpl,
        	    itemSelector: 'div.thumb-wrap',
        	    emptyText: 'No images available',
        	    renderTo: Ext.getBody()
        	});
        
        me.searchPanel = Ext.create('Ext.form.Panel', {
        	region: 'north',
        	collapsible: true,
        	bodyPadding: 10,
        	border : 1,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
            frame:true,

            buttons : [
                  {
                  	text : 'Search',
                  	scope: this,
      				handler: function() {
      					this.showReport();	
					},
          	        tabIndex:6,
      			},{
      				text : 'Reset',
      				scope: this,
      				handler: function() {
      					this.reset();	
					},
      				tabIndex:7,
      			},{
      				text : 'Clear',
      				scope: this,
      				tabIndex:8,
      	             handler: function() {
      	            	 this.searchPanel.form.reset();
      	             }
      			}
      		],
        });
        
        me.reportsStore = Ext.create('Ext.data.Store', {
        	fields: ['label', 'value'],
        });
        
        me.reportsGrid = Ext.create('Ext.grid.Panel', {
        	title : 'Reports',
        	region: 'west', 
        	collapsible: true,
        	width: 250,
        	xtype: 'panel',
        	store : me.reportsStore,
        	viewConfig: {
        		style: {overflow:'auto', overflowX: 'hidden'}
        	},
        	columns: [{ dataIndex: 'label', flex:1,
        				renderer:function(value, metadata, record){
        					return '<a href=#>'+value+'</a>'; 
        				}
        	}],
            listeners : {
                cellclick: function( grid, td, cellIndex, record, tr, rowIndex, e, eOpts ){
                	app.reportsMgmtPanel.reportsDetailPanel.showReport();
                }
            }

        }), 
        
        me.myPanel = Ext.create('Ext.panel.Panel', {
            width: '100%',
            height: 600,
            title: '',
            layout: 'border',
            items: [
                    me.reportsGrid,
                    {
                    	xtype: 'panel',
  	            		split: true,
  	            		autoScroll:true,
  	            		region: 'center',
  	            		items: [me.searchPanel,
  	            		        {
  	            					xtype: 'container',
  	            					height : 10
  	            		        },
  	            		        me.dataView
  	            		        ]
                    }],
                    renderTo: Ext.getBody()
        });

        me.items = [me.myPanel]; 
        
        me.listeners = {
                afterRender: function(thisForm, options){
                    this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                        enter: this.showReport,
                        scope: this
                    });
                }
            } 
        
        me.callParent(arguments);
    },

    showReport : function(report) {
    	if (this.reportsGrid.getView().getSelectionModel().getSelection()[0] != null ) {
        	report = this.reportsGrid.getView().getSelectionModel().getSelection()[0].data.label;
		}else{
			report = 'Research Tasks';
		}
    	if (this.searchPanel.title != report)
    		this.createFilters(report);
    	switch (report) {
		case 'Research Tasks':
			this.searchTasks();
			break;
		case 'Available Candidates':
			this.searchCandidates();
			break;

		default:
			break;
		}
	},
    
    readReportsFile : function() {
    	var xmlDoc= this.loadXMLDoc("reports.xml");
    	var reports = new Array();
    	this.reportsStore.removeAll();
    	
    	for ( var i = 0; xmlDoc.getElementsByTagName("report") [i] != null ; i++) {
    		var report=xmlDoc.getElementsByTagName("report") [i];
        	var name = report.getElementsByTagName('name')[0].textContent;
        	reports.push({"label":name,"value":name});
		}
    	this.reportsStore.add(reports);
    	this.showReport();
	},
    
	loadXMLDoc : function(filename) {
		if (window.XMLHttpRequest){
			xhttp=new XMLHttpRequest();
		}else{
			// code for IE5 and IE6
			xhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xhttp.open("GET",filename,false);
		xhttp.send();
		return xhttp.responseXML;
	},
	
	createFilters : function(selectedReport) {
		var xmlDoc= this.loadXMLDoc("reports.xml");
		var items = [];
		this.searchPanel.removeAll();
		var container = new Ext.container.Container({
			layout: {
		        type: 'table',
		        columns: 3,
		        tableAttrs: {
		            style: {
		                width: '100%',
		                labelWidth : 120
		            }
		        }
		    },
		    items: []
		});
		
		for ( var i = 0; xmlDoc.getElementsByTagName("report") [i] != null ; i++) {
    		var report=xmlDoc.getElementsByTagName("report") [i];
        	var name = report.getElementsByTagName('name')[0].textContent;
        	if (selectedReport == name) {
        		this.searchPanel.setTitle(name);
        		for ( var j = 0; report.getElementsByTagName('filter')[j] != null; j++) {
    				var filter = report.getElementsByTagName('filter')[j];
    				var filterLabel = filter.getAttribute('label');
    				var filterName = filter.getAttribute('name');
    				var type  = filter.getAttribute('type');
    				switch (type.toLowerCase()) {
					case 'textfield':
				        var field = new Ext.form.field.Text({
				        	fieldLabel: filterLabel,
				        	name : filterName,
				        	labelWidth : 120
				        });
				        items.push(field);
						break;
					case 'datefield':
				        var field = new Ext.form.field.Date({
				        	fieldLabel: filterLabel,
				        	name : filterName,
				        	labelWidth : 120
				        });
				        items.push(field);
						break;
					case 'combobox':
				        var comboStore = Ext.create('Ext.data.Store', {
				        	fields: ['label'],
				        	loadAll : function(query){
				           		app.reportsService.getComboValues(query, this.onLoadAll, this);
				            },
				            onLoadAll: function(data){
				            	data = Ext.decode(data);
				        		if (data.success  && data.returnVal.rows) {
									var comboArray = new Array();
				        			for ( var m = 0; m < data.returnVal.rows.length; m++) {
										comboArray.push({"label":data.returnVal.rows[m]});
									}
				        			this.loadData(comboArray);
				        		} else {
				        			console.log('Unable to load the data');
				        		}
				            }
				        });
				        
				        if (filter.getAttribute('values') == null || filter.getAttribute('values') =="") {
				        	var query = filter.getAttribute('query');
							comboStore.loadAll(query);
						}else{
							var comboArray = new Array();
					        comboArray = filter.getAttribute('values').split(',');
					        for ( var k = 0; k < comboArray.length; k++) {
					        	comboArray[k]= {"label":comboArray[k]};
							}
					        comboStore.removeAll();
					        comboStore.add(comboArray);
						}
				        
						var field = Ext.create('Ext.form.ComboBox', {
						    fieldLabel: filterLabel,
				            name: filterName,
						    queryMode: 'local',
						    displayField: 'label',
						    valueField: 'label',
						    store: comboStore,
						    labelWidth :120,
						    tabIndex:3,
						});

				        items.push(field);
						break;
					default:
						break;
					}
				}
			}
		}
		container.items.add(items);
		this.searchPanel.add(container);
	},
	
	searchTasks : function() {
    	var values = this.searchPanel.getValues();
    	var query = "SELECT requirementId 'Job Id',Task,Status,date_format(date,'%m/%d/%Y') Date,Comments,potentialCandidates 'Potential Candidates' FROM researchtasks where 1=1 "
    	if (values.jobId != null && values.jobId != "" )
    		query += " and requirementId ="+values.jobId;
    	if (values.task != null && values.task != "" )
    		query += " and task like '%"+values.task+"%'";
    	if (values.status != null && values.status != "" )
    		query += " and status like '"+values.status+"'";
    	if (values.dateFrom != undefined && values.dateFrom != '') {
    		var dateFrom = Ext.Date.format(new Date(values.dateFrom) , 'Y-m-d');
    		query += " and date(date) >= '"+dateFrom+"'";
    	}
    	if (values.dateTo != undefined && values.dateTo != '') {
    		var dateTo = Ext.Date.format(new Date(values.dateTo) , 'Y-m-d');
    		query += " and date(date) <= '"+dateTo+"'";
    	}
    	query += " order by date desc";
    	app.reportsService.getResult(query,this.onGetResult,this);
	},
	
    searchCandidates : function() {
    	var values = this.searchPanel.getValues();
    	var query = "SELECT id 'Candidate Id',priority Priority,firstName FirstName,lastName LastName,marketingStatus 'Marketing Status',date_format(availability,'%m/%d/%Y') Availability," +
    			"relocate 'Will Relocate',travel 'Trvl?',followUp 'Follow Up',cityAndState 'City & State' FROM candidate where 1=1 "
    	if (values.marketingStatus != null && values.marketingStatus != "" )
    		query += " and marketingStatus like '"+values.marketingStatus+"'";
    	if (values.priority != null && values.priority != "" )
    		query += " and priority like '"+values.priority+"'";
    	if (values.relocate != null && values.relocate != "" )
    		query += " and relocate like '"+values.relocate+"'";
    	if (values.travel != null && values.travel != "" )
    		query += " and travel like '"+values.travel+"'";
    	
    	if (values.availabilityDateFrom != undefined && values.availabilityDateFrom != '') {
    		var availabilityDateFrom = Ext.Date.format(new Date(values.availabilityDateFrom) , 'Y-m-d');
    		query += " and date(availability) >= '"+availabilityDateFrom+"'";
    	}

    	if (values.availabilityDateTo != undefined && values.availabilityDateTo != '') {
    		var availabilityDateTo = Ext.Date.format(new Date(values.availabilityDateTo) , 'Y-m-d');
    		query += " and date(availability) <= '"+availabilityDateTo+"'";
    	}
    	
    	if ((values.availabilityDateFrom == undefined || values.availabilityDateFrom == '') && (values.availabilityDateTo == undefined || values.availabilityDateTo == '')) {
			query += " and availability between curdate() and date_add(curdate(),interval 10 day)"
		}

    	query += " order by availability desc";
    	
    	app.reportsService.getResult(query,this.onGetResult,this);
	},

	onGetResult : function(data) {
		if (data.search('"success":false') != -1) {
			data = Ext.decode(data);
			this.showError(data.errorMessage);
		}else{
			var table =data.substring(29,data.search('</table>')+8);
			var tpl = new Ext.XTemplate(
	    		table
	        );
	      	this.dataView.tpl = tpl;
		}
      	this.dataView.refresh()
	},
		
	reset:function(){
		this.form.reset();
		var tpl = new Ext.XTemplate();
		this.dataView.tpl = tpl;
		this.dataView.refresh()
		this.showReport();
	},
	
	showError : function(error) {
		var tpl = new Ext.XTemplate(
				"<table>" +
				"<tr><td style='color:red;'><font size='3'>"+error+"</font></td></tr>" +
                "</table>"
        );
		this.dataView.tpl = tpl;
		this.dataView.refresh()
	}
});
