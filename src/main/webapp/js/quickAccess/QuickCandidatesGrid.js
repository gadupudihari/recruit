Ext.define('tz.ui.QuickCandidatesGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Candidates',
    frame:true,
    anchor:'100%',
    width :'98%',
    listeners: {
		afterrender: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
    },
    
    initComponent: function() {
        var me = this;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :30,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'View Candidate',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					me.showCandidate(me.store.getAt(rowIndex));
    				}
    			}]
            },{
                xtype: 'gridcolumn',
                dataIndex: 'priority',
                text: 'Priority',
                width : 70,
                renderer : numberRender,
            },{
				text : 'Unique Id',
				dataIndex : 'id',
				width :  70,
		        renderer: candidateRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'firstName',
                text: 'First Name',
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastName',
                text: 'Last Name',
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'emailId',
                text: 'Email Id',
                width:120,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'contactNumber',
                text: 'Contact No',
                width:80,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'marketingStatus',
                text: 'Marketing Status',
                width:120,
                renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + value + '"';
                	if (value != '1. Active' && value != '2. Yet to Start' )
                		metadata.style = 'background-color:lightgray;'
            		return value;
                }
            },{
				text : 'Availability',
				dataIndex : 'availability',
				width :  68,
				renderer: endDateRenderer,
			},{
				text : 'Start Date',
				dataIndex : 'startDate',
				width :  68,
				renderer: dateRender,
			},{
				text : 'Submitted Date',
				dataIndex : 'submittedDate',
				width :  100,
				hidden :true,
				renderer: dateRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:250,
                renderer: candidateRender,
            },{
				text : 'Will Relocate',
				dataIndex : 'relocate',
				width :  60,
				renderer: activeRenderer,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'cityAndState',
                text: 'City & State',
                width:100,
                renderer: candidateRender,
            },{
				text : 'Will Travel',
				dataIndex : 'travel',
				width :  60,
				renderer: activeRenderer,
			},{
				text : 'Follow Up',
				dataIndex : 'followUp',
				width :  60,
				renderer: followUpRenderer,
			},{
				text : 'Open To CTH',
				dataIndex : 'openToCTH',
				renderer: candidateRender,
			},{
				text : 'Employment Type',
				dataIndex : 'employmentType',
				renderer: candidateRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'referral',
                text: 'Referral',
                width:80,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'expectedRate',
                text: 'Expected Rate',
                width:100,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'aboutPartner',
                text: 'About Partner',
                width:120,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'personality',
                text: 'Personality',
                width:120,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'skillSet',
                text: 'Skill Set',
                width:250,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'education',
                text: 'Education',
                width:150,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'immigrationStatus',
                text: 'Immigration Status',
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'type',
                text: 'Type',
                width:100,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'currentJobTitle',
                text: 'Current Job Title',
                width:120,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'employer',
                text: 'Employer',
                width:80,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'highestQualification',
                text: 'Highest Qualification Held',
                width:120,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'contactAddress',
                text: 'Contact Address',
                width:120,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'source',
                text: 'Source',
                width:150,
                renderer: candidateRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: candidateRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:150,
            }
        ];
        me.viewConfig = {
        	stripeRows: true,
        	getRowClass: function (record, rowIndex, rowParams, store) {
        		return record.get('followUp').match('YES') ? 'back-lightblue' : '';
        	}
        };
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: ['->',me.showCount]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        me.callParent(arguments);
    },

    showCandidate : function(record) {
    	app.setActiveTab(0,'li_candidates');
		app.candidateMgmtPanel.getLayout().setActiveItem(1);
		app.candidateMgmtPanel.candidatePanel.loadForm(record);
		app.candidateMgmtPanel.candidatePanel.openedFrom = 'Candidate';
	},
	
    search : function() {
    	var params = new Array();
    	params.push(['table','=', "submissions"]);
    	if (this.hotJobsCombo.getValue() != null) {
        	var hotness = this.hotJobsCombo.getValue().toString();
        	if (hotness != '')
        		hotness = "'"+hotness.replace(/,/g, "','")+"'";
        	params.push(['hotness','=', hotness]);
		}
    	if (this.submissionStatusCombo.getValue() != null) {
        	var openings = this.submissionStatusCombo.getValue().toString();
        	if (openings != '')
        		openings = "'"+openings.replace(/,/g, "','")+"'";
        	params.push(['submissionStatus','=', openings]);
    	}
    	
    	if (this.submissionCombo.getValue() != null && this.submissionCombo.getValue() != '') {
   		 	var date = new Date();
   		 	params.push(['submittedDateTo','=',date]);
   		 	if (this.submissionCombo.getValue() == 'One Week')
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.DAY, -6)]);
   		 	else if (this.submissionCombo.getValue() == 'Two Weeks')
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.DAY, -13)]);
   		 	else if (this.submissionCombo.getValue() == 'One Month') 
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.MONTH, -1)]);
   		 	else if (this.submissionCombo.getValue() == 'Three Months') 
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.MONTH, -3)]);
    	}
    	params.push(['vendorId','=',this.vendorCombo.getValue()]);
    	var filter = getFilter(params);
    	filter.pageSize=100;
    	ds.homeRequirementStore2.loadByCriteria(filter);
	},
});

