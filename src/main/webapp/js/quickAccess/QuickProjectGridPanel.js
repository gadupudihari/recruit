//Define a project grid
Ext.define('tz.ui.QuickProjectGridPanel', {
	extend : 'Ext.grid.Panel',
	title: 'Projects',
    height:250,
    anchor:'100%',
    width :'98%',
	frame:true,
	
	initComponent : function() {
		
		
		this.columns = [{
			text : 'Project Name',
			width : 130,
			dataIndex : 'projectName'
		},{
			text : 'Project Code',
			width : 200,
			dataIndex : 'projectCode'
		},{
			text : 'Vendor Name',
			width:100,
			dataIndex : 'vendorName'
		},{
			text : 'Project Status',
			width : 100,
			dataIndex : 'projectStatus',
			renderer: statusRenderer
		} ,{
			text : 'Proj StartDate',
			width : 100,
			xtype: 'datecolumn',   
			format:'m/d/Y',
			dataIndex : 'projectStartDate'
		},{
			text : 'Proj EndDate',
			width : 100,
			xtype: 'datecolumn',   
			format:'m/d/Y',
			dataIndex : 'projectEndDate'
		},{
			text : 'Appr. EndDate',
			width : 100,
			xtype: 'datecolumn',   
			format:'m/d/Y',
			dataIndex : 'approxEndDate',
		},{
			text : 'Employer',
			width : 70,
			dataIndex : 'employer',
			renderer: employerRenderer,
		},{
			text : 'Invoicing Terms',
			width : 90,
			dataIndex : 'invoicingTerms',
			renderer: weeksRenderer,
		},{
			text : 'Payment Terms',
			width : 90,
			dataIndex : 'paymentTerms',
			renderer: weeksRenderer,
		},{
			text : 'Project Comments',
			width : 175,
			dataIndex : 'projectComments',
		}];

		Ext.apply(this, {
			columns : this.columns,
			viewConfig : {
				stripeRows : true
			}
		});

		this.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

		this.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: ['->',this.showCount]
        }];

        this.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

		this.callParent(arguments);
        
	},

});

