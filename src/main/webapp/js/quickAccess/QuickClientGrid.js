Ext.define('tz.ui.QuickClientGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Clients',
    frame:true,
    anchor:'100%',
    width :'98%',
    listeners: {
		afterrender: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
    },
    
    initComponent: function() {
        var me = this;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :40,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'View client',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					me.showClient(me.store.getAt(rowIndex));
    				}
    			}]
            },{
				text : 'Unique Id',
				dataIndex : 'id',
				width :  70,
		        renderer: clientRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'name',
                text: 'Name',
                renderer: clientRender,
            },{
    			text : 'Type',
    			dataIndex : 'type',
                renderer: clientRender,
    		},{
                xtype: 'gridcolumn',
                dataIndex: 'score',
                text: 'Priority',
                width:80,
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'city',
                text: 'City',
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'state',
                text: 'State',
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'address',
                text: 'Address',
                width:140,
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:140,
                renderer: clientRender,
            },{
				header : 'Contractors',
				dataIndex : 'contractorId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != "") {
    					var ids = value.split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contractorsStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.firstName+' '+rec.data.lastName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return '('+ids.length+') '+names.substring(0,names.length-2);
					}else
						return value;
                }
			},{
				header : 'Full Time',
				dataIndex : 'fullTimeId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != "") {
    					var ids = value.split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contractorsStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.firstName+' '+rec.data.lastName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return '('+ids.length+') '+names.substring(0,names.length-2);
					}else
						return value;
                }
			},{
				header : 'Related Clients/Vendors',
				dataIndex : 'referenceId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != "") {
    					var ids = value.split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.client_vendorStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.name +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return '('+ids.length+') '+names.substring(0,names.length-2);
					}else
						return value;
                }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'contactNumber',
                text: 'Contact Number',
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'fax',
                text: 'Fax',
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'email',
                text: 'Email',
                width:120,
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'website',
                text: 'Website',
                width:120,
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'industry',
                text: 'Industry',
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'about',
                text: 'About',
                width:140,
                renderer: clientRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: clientRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:150,
            }
        ];
        me.viewConfig = {
        		stripeRows: true	
        };
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: ['->',me.showCount]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        me.callParent(arguments);
    },

    showClient : function(record) {
    	app.setActiveTab(0,'li_clients');
    	app.clientMgmtPanel.getLayout().setActiveItem(1);
    	app.clientMgmtPanel.clientPanel.loadForm(record);
    	app.clientMgmtPanel.clientPanel.openedFrom = 'QuickAccess';
	}

});

