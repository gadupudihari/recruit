Ext.define('tz.ui.QuickContactGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Contacts',
    frame:true,
    anchor:'100%',
    width :'98%',
    listeners: {
		afterrender: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
    },
    
    initComponent: function() {
        var me = this;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
            	xtype : 'actioncolumn',
            	width : 30,
            	items : [{
            		icon : 'images/icon_edit.gif',
            		tooltip : 'Edit/View Contact',
            		padding : 10,
            		scope : this,
            		handler : function(grid, rowIndex, colIndex) {
            			me.showContactPanel(me.store.getAt(rowIndex));
            		} 
            	}]
            },{
				text : 'Unique Id',
				dataIndex : 'id',
				width :  70,
		        renderer: contactRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'firstName',
                text: 'First Name',
                renderer: contactRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastName',
                text: 'Last Name',
                renderer: contactRender,
            },{
				header : 'Client/Vendor',
				dataIndex : 'clientId',
	            width :  120,
                renderer:function(value, metadata, record){
                	var rec = ds.clientSearchStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.name + '"';
                		return rec.data.name;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
                xtype: 'gridcolumn',
                text: 'Type',
				dataIndex : 'clientId',
                renderer:function(value, metadata, record){
                	var rec = ds.clientSearchStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.type + '"';
                		return rec.data.type;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'score',
                text: 'Score',
                width:80,
                renderer: contactRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'email',
                text: 'Email',
                width:120,
                renderer: contactRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'jobTitle',
                text: 'Job Title',
                width:120,
                renderer: contactRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'internetLink',
                text: 'Internet Link',
                renderer: contactRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'cellPhone',
                text: 'CellPhone',
                renderer: contactRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'extension',
                text: 'Extension',
            },{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:140,
                renderer: contactRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'interviews',
                text: 'Interviewer',
                renderer:function(value, p, record){
                	if (value > 0) 
						return 'Yes';
                	else
                		return 'No';
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: contactRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:150,
            }
        ];
        me.viewConfig = {
        		stripeRows: true	
        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: ['->',me.showCount]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        ds.clientSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        me.callParent(arguments);
    },
    
    showContactPanel : function(record) {
    	app.setActiveTab(0,'li_contacts');
    	app.contactMgmtPanel.getLayout().setActiveItem(1);
    	app.contactMgmtPanel.contactDetailPanel.loadForm(record);
    	app.contactMgmtPanel.contactDetailPanel.openedFrom = 'Contact';
	}
});

