Ext.define('tz.ui.QuickRequirementsGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Submissions',
    frame:true,
    anchor:'100%',
    width :'98%',    
    listeners: {
		afterrender: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
    },
     
    initComponent: function() {
        var me = this;
        
        me.columns = [{
            xtype : 'actioncolumn',
            hideable : false,
            width : 30,
            items : [{
				icon : 'images/icon_edit.gif',
				tooltip : 'View Requirement',
				padding: 50,
				scope: this,
				handler : function(grid, rowIndex, colIndex) {
						app.homeMgmtPanel.editRequirement(me.store.getAt(rowIndex).data.requirementId);
				}
			}]
        },{
      	  xtype: 'gridcolumn',
      	  dataIndex: 'hotness',
      	  text: 'Hotness',
      	  width :  45,
      	  renderer:function(value, metadata, record){
      		  if (Number(value) == 0) {
      			  metadata.style = "color:#009AD0;";
      		  }
      		  var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
      		  if(rec){
      			  metadata.tdAttr = 'data-qtip="' + rec.data.value.substring(rec.data.value.indexOf('.')+1)+ '"';
      			  return rec.data.value.substring(rec.data.value.indexOf('.')+1);
      		  }else{
      			  metadata.tdAttr = 'data-qtip="' + value + '"';
      			  return value;
      		  }
      	  }
        },{
			xtype: 'datecolumn',
			text : 'Submitted Date',
			dataIndex : 'submittedDate',
			width :  60,
			format: "m/d/Y",
		},{
			header : 'Submission Status',
			dataIndex : 'submissionStatus',
			width :  70,
			renderer:function(value, metadata, record){
				metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
				if (value == "I-Scheduled")
					metadata.tdAttr = 'data-qtip="' + 'Interview date : '+ record.data.interviewDate + '"';
				else if (value == "C-Accepted")
					metadata.tdAttr = 'data-qtip="' + 'Project start date: ' +Ext.Date.format(record.data.projectStartDate,'m/d/Y') + '"';
				else
					metadata.tdAttr = 'data-qtip="' + value + '"';
				return value;
			}
		},{
			header : 'Candidate',
			dataIndex : 'candidateId',
			width :  110,
			renderer:function(value, metadata, record){
				metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
				if(record){
					metadata.tdAttr = 'data-qtip="'+record.data.candidate+'"';
					return record.data.candidate;
				}else{
					return null;
				}
			}
		},{
			header : 'Vendor',
			dataIndex : 'vendor',
            width :  80,
			renderer:function(value, metadata, record){
				metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
				if (value != ''){
					metadata.tdAttr = 'data-qtip="'+value+'<br><b>Open Snapshot</b>"';
					return '<a href=# style="color: #000000"> '+value+'</a>';
				}
			}
		},{
	        xtype: 'gridcolumn',
	        dataIndex: 'reqCityAndState',
	        text: 'City & State',
	        renderer: renderValue,
			renderer:function(value, metadata, record){
				metadata.tdAttr = 'data-qtip="' + value + '"';
				metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
				return value;
			}
	    },{
			header : 'Client',
			dataIndex : 'client',
            width :  80,
			renderer:function(value, metadata, record){
				metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
				if (value != ''){
					metadata.tdAttr = 'data-qtip="'+value+'<br><b>Open Snapshot</b>"';
					return '<a href=# style="color: #000000"> '+value+'</a>';
				}
			}
		},{
			text : "Alert",
			dataIndex : 'addInfoTodos',
			width :  70,
			renderer:function(value, metadata, record){
				metadata.tdAttr = 'data-qtip="' + value + '"';
				metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
			    if (value == null || value=="") 
			    	return 'N/A' ;
			    return '<span style="color:red;">' + value + '</span>';
			}
		},{
			xtype: 'gridcolumn',
			dataIndex: 'postingTitle',
			text: 'Posting Title',
			renderer:function(value, metadata, record){
				metadata.tdAttr = 'data-qtip="' + value + '"';
				return value;
			}
		},{
			xtype: 'gridcolumn',
			dataIndex: 'module',
			text: 'Module',
			renderer:function(value, metadata, record){
				metadata.tdAttr = 'data-qtip="' + value + '"';
				return value;
			}
		},{
			xtype: 'datecolumn',
			text : 'Posting Date',
			dataIndex : 'postingDate',
			width :  60,
			format: "m/d/Y H:i:s A",
		},{
			header : 'Submitted Rate-1',
			dataIndex : 'submittedRate1',
		},{
			header : 'Submitted Rate-2',
			dataIndex : 'submittedRate2',
			width :  60,
		},{
			dataIndex: 'candidateExpectedRate',
			text: 'Expected Rate',
			width :  50,
		},{
			dataIndex: 'statusUpdated',
			text: 'Status Updated Date',
			width :  60,
			xtype: 'datecolumn',
			format:'m/d/Y',
		},{
			dataIndex: 'cityAndState',
			text: 'Homeloc',
			width :  80,
		},{
			dataIndex: 'relocate',
			text: 'Will Relocate',
			width :  50,
			renderer: activeRenderer,
		},{
			dataIndex: 'travel',
			text: 'Will Travel',
			width :  50,
			renderer: activeRenderer,
		},{
			dataIndex: 'availability',
			text: 'Availability',
			width :  80,
			xtype: 'datecolumn',
			format:'m/d/Y',
		},{
			dataIndex: 'comments',
			text: 'Comments',
		}
		];
        
        me.viewConfig = {
        		stripeRows: true,
            	getRowClass: function (record, rowIndex, rowParams, store) {
            		return 'row-font12';
            	}
        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

		me.hotJobsCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Hotness',
		    store : ds.hotnessStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'hotJobs',
		    value : ['00','01'],
		    tabIndex:11,
		    labelWidth :50,
		    width :150,
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
		});

		me.submissionStatusCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Status',
		    store: ds.submissionStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'submissionStatus',
		    tabIndex:13,
		    labelWidth :40,
		    width :140,
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
		});


        me.submissionCombo = Ext.create('Ext.form.ComboBox', {
        	matchFieldWidth: false,
            fieldLabel: 'Submitted Date',
            store: ds.monthStore,
            name : 'payroll',
            queryMode: 'local',
            displayField: 'label',
            valueField: 'value',
            labelAlign: 'right',
            tabIndex :14,
            labelWidth :80,
            width :150,
            //value : 'One Week',
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
        });

        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [me.hotJobsCombo,me.submissionStatusCombo,me.submissionCombo,
            	        '-',{
            		text : '',
            		tabIndex:15,
            		iconCls : 'btn-Search',
            		handler: function(){
            			me.search();
            		}
            	},'-',{
            		text : 'Reset',
            		width : 40,
            		tabIndex:15,
            		handler: function(){
            			me.hotJobsCombo.reset();
            			me.submissionStatusCombo.reset();
            			me.submissionCombo.reset();
            			me.search();
            		}
            	},'-',{
        			text : '',
         			iconCls : 'icon_info',
         			scope: this,
         			tabIndex:16,
         			handler: function(){
         		        var legendWin = Ext.create("Ext.window.Window", {
         					title: 'Legend',
         					modal: true,
         					height      : 400,
         					width       : 550,
         					bodyStyle   : 'padding: 1px;background:#ffffff;',
         					html: '<table cellspacing="10" cellpadding="5" noOfCols="2" width="550"> '
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFCBC;"></td>'
         						+ '<td width="50%">Reqs which are opened today</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FAFAD0;"></td>'
         						+ '<td width="50%">Reqs which are opened since yesterday</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFFE0;"></td>'
         						+ '<td width="50%">Reqs which are opened since a week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFFEE;"></td>'
         						+ '<td width="50%">Reqs that are opened for more than one week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#C3D5F6;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made today</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#CFDCF5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made yesterday</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#D9E3F5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made with in one week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#E5EBF5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made more than one week back</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#A7FDA1;"></td>'
         						+ '<td width="50%">Reqs for which  placement was made</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFD3A7;"></td>'
         						+ '<td width="50%">Reqs which reached atleast interview stage</td>'
         						+ '</tr>'
         						+ '</table>'
         				});
         				legendWin.show();
         		    }
            	},'->',me.showCount]
          }
      ];
        
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        ds.contractorsStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.clientSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.contactSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        ds.vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        ds.hotnessStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        me.callParent(arguments);
    },

    showRequirement : function(record) {
    	app.setActiveTab(0,'li_requirements');
    	app.requirementMgmtPanel.getLayout().setActiveItem(1);
    	app.requirementMgmtPanel.requirementDetailPanel.loadForm(record);
    	app.requirementMgmtPanel.requirementDetailPanel.openedFrom = 'Recruit';

	},
	
    search : function() {
    	var params = new Array();
		var recId = this.manager.quickAccessCombo.getValue();
		var record = ds.quickAccessStore.getById(recId);
		if (record.data.type== 'Candidate') 
	    	params.push(['candidateId','=', record.data.referenceId]);
		else if (record.data.type== 'Vendor')
	    	params.push(['vendorId','=', record.data.referenceId]);
		else
			params.push(['clientId','=', record.data.referenceId]);
    	
    	params.push(['table','=', "submissions"]);
    	if (this.hotJobsCombo.getValue() != null) {
        	var hotness = this.hotJobsCombo.getValue().toString();
        	if (hotness != '')
        		hotness = "'"+hotness.replace(/,/g, "','")+"'";
        	params.push(['hotness','=', hotness]);
		}
    	if (this.submissionStatusCombo.getValue() != null) {
        	var openings = this.submissionStatusCombo.getValue().toString();
        	if (openings != '')
        		openings = "'"+openings.replace(/,/g, "','")+"'";
        	params.push(['submissionStatus','=', openings]);
    	}
    	
    	if (this.submissionCombo.getValue() != null && this.submissionCombo.getValue() != '') {
   		 	var date = new Date();
   		 	params.push(['submittedDateTo','=',date]);
   		 	if (this.submissionCombo.getValue() == 'One Week')
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.DAY, -6)]);
   		 	else if (this.submissionCombo.getValue() == 'Two Weeks')
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.DAY, -13)]);
   		 	else if (this.submissionCombo.getValue() == 'One Month') 
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.MONTH, -1)]);
   		 	else if (this.submissionCombo.getValue() == 'Three Months') 
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.MONTH, -3)]);
    	}
    	params.push(['deleted','=', 'false']);
    	var filter = getFilter(params);
    	filter.pageSize=100;
    	this.store.loadByCriteria(filter);
	},
    
});
