Ext.define('tz.ui.QuickAccessPanel',{
    extend: 'Ext.form.Panel',    
    bodyPadding: 0,
    title: '',
    anchor:'100%',
    autoScroll:true,
//creating view in init function
    initComponent: function() {
        var me = this;
        me.selection = '';
        
        me.quickAccessCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Search',
		    store: ds.quickAccessStore,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'id',
		    name : 'name',
		    anyMatch:true,
		    labelWidth :55,
		    width :250,
   			listeners: {
   				scope:me,
		    	select:function(combo, records, eOpts) {
		    		ds.quickAccessStore.clearFilter();
		    		me.show_hideGrids(records[0].data.type);
		    		me.search();
		    	}
			},
			listConfig: {
		        cls: 'grouped-list'
			},
			tpl: Ext.create('Ext.XTemplate',
					'{[this.currentKey = null]}' +
					'<tpl for=".">',
					'<tpl if="this.shouldShowHeader(type)">' +
		            '<div class="group-header">{[this.showHeader(values.type)]}</div>' +
		            '</tpl>' +
		            '<div class="x-boundlist-item">{name}</div>',
		            '</tpl>',
		            {
		          	shouldShowHeader: function(name){
		          		return this.currentKey != name;
		          	},
		          	showHeader: function(name){
		          		this.currentKey = name;
		          		return name;
		          	}
		            }
			)
		});
        
        me.searchPanel = Ext.create('Ext.form.Panel', {
        	title: 'Search',
        	region: 'north',
        	width: '99%',
        	collapsible: true,
        	bodyPadding: 10,
        	border : 1,
        	buttonAlign:'left',
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
            frame:true,

            items : [{
            	xtype:'container',
   	   	        layout: 'column',
   	   	        items :[{
   	   	            xtype: 'container',
   	   	            columnWidth:.99,
   	   	            items: [me.quickAccessCombo]
   	   	        }]
            }],
                        
            buttons : [{
                  	text : 'Search',
                  	scope: this,
      				handler: function() {
      					this.search();
					},
          	        tabIndex:6,
      			},{
      				text : 'Reset',
      				scope: this,
      				handler: function() {
      					this.reset();
      			    	this.searchPanel.form.reset();
					},
      				tabIndex:7,
      			},{
      				text : 'Clear',
      				scope: this,
      				tabIndex:8,
      	             handler: function() {
      	            	 this.searchPanel.form.reset();
      	             }
      			}
      		],
        });
        
        me.alertsInfoPanel = Ext.create('Ext.form.Panel', {
        	title: 'Messages',
        	region: 'north',
        	width: '99%',
        	hidden : true,
        	collapsible: true,
        	collapsed : false,
        	bodyPadding: 10,
        	border : 1,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
            frame:false,
        	listeners: {
        		afterrender: function(panel) {
        			me.alertsInfoPanel.header.el.on('click', function() {
        				if (me.alertsInfoPanel.collapsed) {me.alertsInfoPanel.expand();}
        				else {me.alertsInfoPanel.collapse();}
        			});
        		}
        	},
            items : [{
            	xtype:'container',
   	   	        layout: 'column',
   	   	        items :[{
   	   	            xtype: 'container',
   	   	            columnWidth:.95,
   	   	            items: [{
   	   	            	xtype:'displayfield',
   	   	            	padding : '0 0 0 20',
   	   	            	fieldLabel: '',
   	   	            	name: 'messagesInfo',
   	   	            }]
   	   	        }]
            }],
        });

        me.quickInfoPanel = Ext.create('Ext.form.Panel', {
        	title: 'Quick Information',
        	region: 'north',
        	width: '99%',
        	collapsible: true,
        	collapsed : false,
        	autoScroll:true,
        	hidden : true,
        	bodyPadding: 10,
        	border : 1,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
            frame:false,
        	listeners: {
        		afterrender: function(panel) {
        			panel.header.el.on('click', function() {
        				if (panel.collapsed) {panel.expand();}
        				else {panel.collapse();}
        			});
        		}
        	},

            items : [{
            	xtype:'container',
   	   	        layout: 'column',
   	   	        items :[{
   	   	            xtype: 'container',
   	   	            columnWidth:.99,
   	   	            items: [{
   	   	            	xtype:'displayfield',
      	                padding : '0 0 0 20',
       	                fieldLabel: '',
       	                name: 'quickInfo',
   	   	            },{
   	   	            	xtype:'displayfield',
      	                padding : '0 0 0 20',
       	                fieldLabel: '',
       	                name: 'submissions',
   	   	            },{
   	   	            	xtype:'displayfield',
      	                padding : '0 0 0 20',
       	                fieldLabel: '',
       	                name: 'project',
   	   	            },{
   	   	            	xtype:'displayfield',
   	   	            	padding : '0 0 0 20',
   	   	            	fieldLabel: '',
   	   	            	name: 'certifications',
   	   	            }]
   	   	        }]
            }],
        });

        me.reqQuickInfoPanel = Ext.create('Ext.form.Panel', {
        	title: 'Quick Information',
        	region: 'north',
        	width: '99%',
        	autoScroll:true,
        	bodyPadding: 10,
        	border : 1,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
            frame:false,

            items : [{
            	xtype:'container',
   	   	        layout: 'column',
   	   	        items :[{
   	   	            xtype: 'container',
   	   	            columnWidth:.6,
   	   	            items: [{
   	   	            	xtype:'displayfield',
      	                padding : '0 0 0 20',
       	                fieldLabel: '',
       	                name: 'quickInfo',
   	   	            }]
   	   	        },{
   	   	            xtype: 'container',
   	   	            columnWidth:.4,
   	   	            items: [{
   	   	            	xtype:'displayfield',
      	                padding : '0 0 0 20',
       	                fieldLabel: '',
       	                name: 'shortlist',
   	   	            },{
   	   	            	xtype:'displayfield',
      	                padding : '0 0 0 20',
       	                fieldLabel: '',
       	                name: 'submissions',
   	   	            },{
   	   	            	xtype:'displayfield',
   	   	            	padding : '0 0 0 20',
   	   	            	fieldLabel: '',
   	   	            	name: 'suggested',
   	   	            }]
   	   	        }]
            }],
        });

        me.candidatesGrid = new tz.ui.QuickCandidatesGrid({
        	manager:me,
        	padding: 5,
        	region: 'north', 
        	collapsible: true,
        	collapsed : true,
        	hidden : true,
        	width: '99%',
        	height : 300,
        });

        me.vendorGrid = new tz.ui.QuickVendorGrid({
        	manager:me,
        	padding: 5,
        	region: 'north', 
        	collapsible: true,
        	collapsed : true,
        	hidden : true,
        	width: '99%',
        	height : 300,
        });

        me.clientGrid = new tz.ui.QuickClientGrid({
        	manager:me,
        	padding: 5,
        	region: 'north', 
        	collapsible: true,
        	collapsed : true,
        	hidden : true,
        	width: '99%',
        	height : 300,
        });

        me.contactGrid = new tz.ui.QuickContactGrid({
        	manager:me,
        	padding: 5,
        	region: 'north', 
        	collapsible: true,
        	collapsed : true,
        	hidden : true,
        	width: '99%',
        	height : 300,
        });

        me.interviewGrid = new tz.ui.QuickInterviewGrid({
        	manager:me,
        	padding: 5,
        	region: 'north', 
        	collapsible: true,
        	collapsed : true,
        	hidden : true,
        	width: '99%',
        	height : 300,
        });

        me.requirementsGrid = new tz.ui.QuickRequirementsGrid({
        	manager:me,
        	padding: 5,
        	region: 'north', 
        	collapsible: true,
        	collapsed : true,
        	hidden : true,
        	width: '99%',
        	height : 400,
        });

        me.certificationGrid = new tz.ui.QuickCertificationGrid({
        	store : ds.quickCertificationStore,
        	padding: 5,
        	region: 'north', 
        	collapsible: true,
        	hidden : true,
        	collapsed : true,
        	height : 250,
        	width: '99%',
        	listeners: {
        		afterrender: function(panel) {
        			panel.header.el.on('click', function() {
        				if (panel.collapsed) {panel.expand();}
        				else {panel.collapse();}
        			});
        		}
        	}
        });

        me.projectGrid = new tz.ui.QuickProjectGridPanel({
        	store : ds.quickProjectStore,
        	padding: 5,
        	region: 'north', 
        	collapsible: true,
        	hidden : true,
        	collapsed : true,
        	height : 250,
        	width: '99%',
        	listeners: {
        		afterrender: function(panel) {
        			panel.header.el.on('click', function() {
        				if (panel.collapsed) {panel.expand();}
        				else {panel.collapse();}
        			});
        		}
        	}
        });

        me.myPanel = Ext.create('Ext.panel.Panel', {
            width: '100%',
            title: '',
            layout: 'border',
            items: [{
            	xtype: 'panel',
        		split: true,
        		autoScroll:true,
        		region: 'center',
        		items: [me.searchPanel,
        		        {
        					xtype: 'container',
        					height: 10,
        					border : false,
        		        },{
		   	    	    	xtype: 'container',
		   	    	    	layout :'table',
		   	    	    	items: [{
	                            xtype:'button',
	                            margin: '0 0 0 10',
	                            text: 'Collapse All',
	                            scope:me,
	                            handler: function(){
	                            	this.collapseAll();
	                            }
				            },{
	                            xtype:'button',
	                            margin: '0 0 0 10',
	                            text: 'Expand All',
	                            scope:me,
	                            handler: function(){
	                            	this.expandAll();
	                            }
				            },{
				            	xtype:'displayfield',
				            	fieldLabel: '',
				            	name: 'employeeHyperlink',
				            	margin: '0 0 0 10',
				            	height : 18,
				            	listeners: {
				            		afterrender: function(component) {
				            			component.getEl().on('click', function() { 
				            				me.editRecord('client');
				            			});  	
				            		}
				            	}
				            },{
				            	xtype:'displayfield',
				            	fieldLabel: '',
				            	name: 'resumeHyperlink',
				            	margin: '0 0 0 10',
				            	height : 18,
				            }]
		   	    	    },{
        					xtype: 'container',
        					height: 10,
        					border : false,
        		        },
        		        me.alertsInfoPanel,
			            me.quickInfoPanel,
        		        me.candidatesGrid,
        		        me.requirementsGrid,
        		        me.interviewGrid,
        		        me.contactGrid,
        		        //me.certificationGrid,
        		        me.projectGrid,
        		        me.vendorGrid,
        		        me.clientGrid,
        		        ]
            }],
        });

		var browserHeight = app.mainViewport.height;
		var headerHt = app.mainViewport.items.items[0].getHeight();
		if (me.itemId == 'snapshot')
			me.myPanel.setHeight(browserHeight - headerHt);
		else
			me.myPanel.setHeight(browserHeight - headerHt - 50);
        
        me.items = [me.myPanel]; 
        
        me.listeners = {
                afterRender: function(thisForm, options){
                    this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {
                        enter: this.search,
                        scope: this
                    });
                }
        } 
        
        me.callParent(arguments);
    },
    

    reset : function() {
    	this.alertsInfoPanel.hide();
    	this.quickInfoPanel.hide();
    	this.vendorGrid.hide();
    	this.clientGrid.hide();
    	this.candidatesGrid.hide();
    	this.requirementsGrid.hide();
    	this.contactGrid.hide();
    	this.interviewGrid.hide();
    	this.projectGrid.hide();
    	/*Ext.getCmp('collapse').hide();
    	Ext.getCmp('expand').hide();*/
	},
    
    show_hideGrids : function(type) {
    	this.quickInfoPanel.show();
    	this.requirementsGrid.show();
    	this.interviewGrid.show();
		if (type == "Candidate") {
			this.candidatesGrid.hide();
			this.contactGrid.hide();
			this.vendorGrid.show();
			this.projectGrid.show();
			this.clientGrid.show();
			this.alertsInfoPanel.show();
			this.certificationGrid.show();
		}else if (type == "Vendor") {
			this.candidatesGrid.show();
			this.contactGrid.show();
			this.vendorGrid.hide();
			this.projectGrid.show();
			this.clientGrid.show();
			this.certificationGrid.hide();
		}else if (type == "Client") {
			this.candidatesGrid.show();
			this.contactGrid.show();
			this.vendorGrid.show();
			this.projectGrid.show();
			this.clientGrid.hide();
			this.certificationGrid.hide();
		}
	},
    
    collapseAll : function(itemId) {
/*    	Ext.getCmp('collapse').hide();
    	Ext.getCmp('expand').show();
*/    	this.quickInfoPanel.collapse();
		this.requirementsGrid.collapse();
		this.candidatesGrid.collapse();
		this.vendorGrid.collapse();
		this.clientGrid.collapse();
		this.contactGrid.collapse();
		this.interviewGrid.collapse();
		this.projectGrid.collapse();
		this.alertsInfoPanel.collapse();
		this.selection='';
	},
    
	expandAll : function(itemId) {
/*		Ext.getCmp('collapse').show();
		Ext.getCmp('expand').hide();
*/		this.quickInfoPanel.expand();
		this.requirementsGrid.expand();
		this.candidatesGrid.expand();
		this.vendorGrid.expand();
		this.clientGrid.expand();
		this.contactGrid.expand();
		this.interviewGrid.expand();
		this.quickInfoPanel.expand();
		this.projectGrid.expand();
		this.alertsInfoPanel.expand();
	},
	
	search : function() {
		var recId = this.quickAccessCombo.getValue();
		var record = ds.quickAccessStore.getById(recId);
		this.quickInfoPanel.form.reset();
		this.form.findField('employeeHyperlink').setValue("<a title='Edit' ><img align='top' src='images/icon_edit.gif'>&nbsp"+record.data.name+"</a>");
		if(record.data.type == "Candidate") {
			this.getEmployeeInfo(record.data.referenceId);
			this.getSubmissions(record.data.referenceId);
			this.loadCertifications();
		}else{
			this.getVendorInfo(record.data.referenceId);
		}
/*		if (Ext.getCmp('collapse').hidden && Ext.getCmp('collapse').hidden)
			Ext.getCmp('expand').show();
*/
		if (recId != null) {
			this.loadVendors(recId);
			this.loadRequirements(recId);
			this.loadCandidates(recId);
			this.loadContacts(recId);
			this.loadInterviews(recId);
			this.loadProjects(recId);
			this.loadClients(recId);
		}
		
		//remove email alerts in Alerts panel
		for ( var i = 0; ; i++) {
			if (Ext.getCmp('alert'+i) != null)
				this.alertsInfoPanel.remove(Ext.getCmp('messages_container'+i),true);
			else
				break;
		}
		
		var employee = ds.quickAccessStore.getById(recId);
		ds.messagesStore.clearFilter();
		ds.messagesStore.filterBy(function(rec) {
		    return rec.get('refId') === Number(employee.data.referenceId);
		});
		
		//Removing duplicates 
		var messages = ds.messagesStore.collect('message');
		
		var newItems = new Array();
		var table = '';
		for ( var i = 0; i < messages.length; i++) {
			var record = ds.messagesStore.getAt(ds.messagesStore.findExact('message',messages[i])).data;
			table += '<tr>';
			table += '<td style="border:1px solid black;">'+Ext.Date.format(record.alertDate,'m/d/Y')+'</td>';
			table += '<td style="border:1px solid black;"><a onclick="javascript:app.quickAccessMgmtPanel.quickAccessPanel.showMessages('+record.id+')" style="color:black;">'+record.subject+'</a></td>';
			//table += '<td style="border:1px solid black;">'+record.status+'</td>';
			table += '</tr>';
		}
		
		//Add email alerts in Alerts panel
		//this.alertsInfoPanel.add(newItems);
		if (table != ''){
			table = '<tr>'+
					'<th style="border:1px solid black;">Alert Date</th>'+
					'<th style="border:1px solid black;">Subject</th>'+
					//'<th style="border:1px solid black;">Status</th>'+
					'</tr>'+ table;
			var messagesTable = '<table cellpadding="5" style="border:1px solid black;border-collapse:collapse;">'+table+'</table><br>';
			this.alertsInfoPanel.form.findField('messagesInfo').setValue(messagesTable);
		}else
			this.alertsInfoPanel.form.findField('messagesInfo').setValue('');
		
		ds.messagesStore.clearFilter();
		this.alertsInfoPanel.doLayout();

	},

    showMessages : function(itemId) {
		app.centerLayout.getLayout().setActiveItem('homeMgmtPanel');
		document.getElementById('li_quickAccess').className = ""; //"gbzt";
		document.getElementById('li_quickAccess1').style.backgroundColor = "#157fcc";
		document.getElementById('li_home').className = "activeNavigationBar";
		app.prevId = 'li_home';
		app.homeMgmtPanel.homeDetailPanel.expandMessage(itemId);
	},

	loadProjects : function(recId) {
		if (recId == null)
			recId = this.quickAccessCombo.getValue();
		var record = ds.quickAccessStore.getById(recId);
		if (record.data.type== 'Candidate') {
	    	var params = new Array();
	    	params.push(['candidateId','=', record.data.referenceId]);
	    	var filter = getFilter(params);
	    	filter.pageSize=100;
	    	this.projectGrid.store.loadByCriteria(filter);
		}else if (record.data.type== 'Vendor') {
	    	var params = new Array();
	    	params.push(['vendorId','=', record.data.referenceId]);
	    	var filter = getFilter(params);
	    	filter.pageSize=100;
	    	this.projectGrid.store.loadByCriteria(filter);
		}else if (record.data.type== 'Client') {
	    	var params = new Array();
	    	params.push(['clientId','=', record.data.referenceId]);
	    	var filter = getFilter(params);
	    	filter.pageSize=100;
	    	this.projectGrid.store.loadByCriteria(filter);
		}
		
    	this.projectGrid.store.on('load', function(store, records, options) {
        	this.projectGrid.showCount.setValue('Displaying '+store.getCount()+' projects');
    		store.filterBy(function(rec) {
    		    return rec.get('projectStatus') === 'Active';
    		});
			if (store.getCount() == 0)
				info = '<u><B>Current Projects :</B></u> '+ 'N/A' +'<br><br>';
			else{
				info = '<u><B>Current Projects :</B></u> <br><br>';
				var table = '<tr>'+
				'<th style="border:1px solid black;">Project Name</th>'+
				'<th style="border:1px solid black;">Project Code</th>'+
				'<th style="border:1px solid black;">Vendor</th>'+
				'<th style="border:1px solid black;">Start Date</th>'+
				'<th style="border:1px solid black;">End Date</th>'+
				'<th style="border:1px solid black;">Appr. End Date</th>'+
				'<th style="border:1px solid black;">Employer</th>'+
				'<th style="border:1px solid black;">Comments</th>'+
				'</tr>';
				for ( var i = 0; i < store.getCount(); i++) {
					var record = store.getAt(i);
					table += '<tr>';
					table += '<td style="border:1px solid black;">'+record.data.projectName+'</td>';
					table += '<td style="border:1px solid black;">'+record.data.projectCode+'</td>';
					table += '<td style="border:1px solid black;">'+record.data.vendorName+'</td>';
					table += '<td style="border:1px solid black;">'+Ext.Date.format(record.data.projectStartDate,'m/d/Y')+'</td>';
					table += '<td style="border:1px solid black;">'+Ext.Date.format(record.data.projectEndDate,'m/d/Y')+'</td>';
					table += '<td style="border:1px solid black;">'+Ext.Date.format(record.data.approxEndDate,'m/d/Y')+'</td>';
					table += '<td style="border:1px solid black;">'+record.data.employer+'</td>';
					table += '<td style="border:1px solid black;">'+record.data.projectComments+'</td>';
					table += '</td>';
				}
				info += '<table cellpadding="5" style="border:1px solid black;border-collapse:collapse;">'+table+'</table>';
			}
			this.quickInfoPanel.form.findField('project').setValue(info);
			this.quickInfoPanel.expand();
			store.clearFilter();
       	}, this);
	},

	loadCertifications : function(recId) {
		if (recId == null)
			recId = this.quickAccessCombo.getValue();
		var record = ds.quickAccessStore.getById(recId);
		if (record.data.type== 'Candidate') {
	    	var params = new Array();
	    	params.push(['candidateId','=', record.data.referenceId]);
	    	var filter = getFilter(params);
	    	filter.pageSize=100;
	    	this.certificationGrid.store.loadByCriteria(filter);
	    	
	    	this.certificationGrid.store.on('load', function(store, records, options) {
	        	this.certificationGrid.showCount.setValue('Displaying '+store.getCount()+' certifications');
				if (records.length == 0)
					info = '<u><B>Certifications :</B></u> '+ 'N/A' +'<br><br>';
				else{
					info = '<u><B>Certifications :</B></u> <br><br>';
					var table = '<tr>'+
					'<th style="border:1px solid black;">Certification Name</th>'+
					'<th style="border:1px solid black;">Date Achieved</th>'+
					'<th style="border:1px solid black;">Status</th>'+
					'<th style="border:1px solid black;">Version</th>'+
					'</tr>';

			    	for ( var i = 0; i < records.length; i++) {
						table += '<tr>';
						table += '<td style="border:1px solid black;">'+records[i].data.name+'</td>';
						table += '<td style="border:1px solid black;">'+Ext.Date.format(records[i].data.dateAchieved,'m/d/Y')+'</td>';
						table += '<td style="border:1px solid black;">'+records[i].data.status+'</td>';
						table += '<td style="border:1px solid black;">'+records[i].data.version+'</td>';
						table += '</td>';
						
					}
			    	info += '<table cellpadding="5" style="border:1px solid black;border-collapse:collapse;">'+table+'</table>';
				}
				this.quickInfoPanel.form.findField('certifications').setValue(info);
				this.quickInfoPanel.expand();
	       	}, this);
		}
	},

	loadVendors : function(recId) {
		if (recId == null)
			recId = this.quickAccessCombo.getValue();
    	var params = new Array();
		var record = ds.quickAccessStore.getById(recId);
		if (record.data.type== 'Candidate'){ 
			params.push(['vendor_candidateId','=', record.data.referenceId]);
	    	var filter = getFilter(params);
	    	filter.pageSize=100;
	    	this.vendorGrid.store.loadByCriteria(filter);
		}else if (record.data.type== 'Client') {
			if (record.data.name.toLowerCase() == 'n/a'){
				this.vendorGrid.store.removeAll();
			}else {
				params.push(['relatedVendors','=', record.data.referenceId]);
		    	var filter = getFilter(params);
		    	filter.pageSize=100;
		    	this.vendorGrid.store.loadByCriteria(filter);
			}
		}
    	this.vendorGrid.store.on('load', function(store, records, options) {
        	this.vendorGrid.showCount.setValue('Displaying '+records.length+' vendors');
    	}, this);

	},

	loadClients : function(recId) {
		if (recId == null)
			recId = this.quickAccessCombo.getValue();
    	var params = new Array();
		var record = ds.quickAccessStore.getById(recId);
		if (record.data.type== 'Candidate'){ 
			params.push(['client_candidateId','=', record.data.referenceId]);
	    	var filter = getFilter(params);
	    	filter.pageSize=100;
	    	this.clientGrid.store.loadByCriteria(filter);
		}else if (record.data.type== 'Vendor') {
			params.push(['relatedClients','=', record.data.referenceId]);
	    	var filter = getFilter(params);
	    	filter.pageSize=100;
	    	this.clientGrid.store.loadByCriteria(filter);
		}
    	this.clientGrid.store.on('load', function(store, records, options) {
        	this.clientGrid.showCount.setValue('Displaying '+records.length+' clients');
    	}, this);
	},

	loadCandidates : function(recId) {
		if (recId == null)
			recId = this.quickAccessCombo.getValue();
    	var params = new Array();
		var record = ds.quickAccessStore.getById(recId);
		if (record.data.type== 'Vendor'){ 
			params.push(['vendorId','=', record.data.referenceId]);	
	    	var filter = getFilter(params);
	    	filter.pageSize=100;
	    	this.candidatesGrid.store.loadByCriteria(filter);
		}else if (record.data.type== 'Client') {
			params.push(['clientId','=', record.data.referenceId]);	
	    	var filter = getFilter(params);
	    	filter.pageSize=100;
	    	this.candidatesGrid.store.loadByCriteria(filter);
		}
    	this.candidatesGrid.store.on('load', function(store, records, options) {
        	this.candidatesGrid.showCount.setValue('Displaying '+records.length+' candidates');
    	}, this);
	},

	loadRequirements : function(recId) {
    	this.requirementsGrid.search();

    	this.requirementsGrid.store.on('load', function(store, records, options) {
        	this.requirementsGrid.showCount.setValue('Displaying '+store.getCount()+' submissions');
    	}, this);
	},

	loadContacts : function(recId) {
		if (recId == null)
			recId = this.quickAccessCombo.getValue();
    	var params = new Array();
		var record = ds.quickAccessStore.getById(recId);
		if (record.data.type== 'Vendor' || record.data.type== 'Client') {
			params.push(['client','=', record.data.referenceId]);
	    	var filter = getFilter(params);
	    	filter.pageSize=100;
	    	this.contactGrid.store.loadByCriteria(filter);
		}
    	this.contactGrid.store.on('load', function(store, records, options) {
        	this.contactGrid.showCount.setValue('Displaying '+store.getCount()+' contacts');
    	}, this);
	},

	loadInterviews : function(recId) {
		if (recId == null)
			recId = this.quickAccessCombo.getValue();
    	var params = new Array();
		var record = ds.quickAccessStore.getById(recId);
		if (record.data.type== 'Candidate') 
			params.push(['candidate','=', record.data.referenceId]);
		else if (record.data.type== 'Vendor')
			params.push(['vendor','=', record.data.referenceId]);
		else
			params.push(['client','=', record.data.referenceId]);
    	var filter = getFilter(params);
    	filter.pageSize=100;
    	this.interviewGrid.store.loadByCriteria(filter);

    	this.interviewGrid.store.on('load', function(store, records, options) {
        	this.interviewGrid.showCount.setValue('Displaying '+store.getCount()+' interviews');
    	}, this);
	},

	getEmployeeInfo : function(employeeId) {
    	var params = new Array();
    	params.push(['id','=', employeeId]);
    	var filter = getFilter(params);
    	filter.pageSize=50;
    	app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidates,this);
	},
	
	onGetCandidates : function(data) {
		if (data.rows.length > 0) {
			var candidate = data.rows[0];
			var info= "";
			var table = '<tr style="background-color:#ffffff;font-weight:bold;">'+
							'<th rowspan="2" >Contact Number</th>'+
							'<th rowspan="2" >Email</th>'+
							'<th colspan="2" >Immigration</th>'+
							'<th rowspan="2" >Location</th>'+
							'<th rowspan="2" >City & State</th>'+
							'<th rowspan="2" >Alert</th>'+
							'<th rowspan="2" >Expected Rate</th>'+
							'<th colspan="2" >Rate Range</th>'+
						'</tr>'+
						'<tr style="background-color:#ffffff;font-weight:bold;"> <th >Status</th> <th >Verified</th>'+
						'<th >From</th> <th >To</th></tr>';
			if (candidate.contactNumber == null) 
				candidate.contactNumber ='';
			if (candidate.emailId == null)
				candidate.emailId ='';
			if (candidate.immigrationStatus == null)
				candidate.immigrationStatus ='';
			if (candidate.location == null)
				candidate.location ='';
			if (candidate.cityAndState == null)
				candidate.cityAndState ='';
			if (candidate.currentJobTitle == null)
				candidate.currentJobTitle ='';
			if (candidate.availability == null || candidate.availability == '')
				candidate.availability ='';
			else
				candidate.availability = Ext.Date.format(new Date(candidate.availability),'m/d/Y')
			if (candidate.highestQualification == null)
				candidate.highestQualification ='';
			if (candidate.employmentType == null)
				candidate.employmentType ='';
			if (candidate.skillSet == null)
				candidate.skillSet ='';
			if (candidate.immigrationVerified == null || candidate.immigrationVerified == '')
				candidate.immigrationVerified ='N/A';
			if (candidate.allCertifications == null)
				candidate.allCertifications ='';
			if (candidate.currentRoles == null)
				candidate.currentRoles ='';
			if (candidate.targetSkillSet == null)
				candidate.targetSkillSet ='';
			if (candidate.targetRoles == null)
				candidate.targetRoles ='';
			if (candidate.rateFrom == null)
				candidate.rateFrom ='';
			if (candidate.rateTo == null)
				candidate.rateTo ='';
			
			candidate.roleSet = '';
			if (candidate.roleSetIds != null && candidate.roleSetIds != ''){
				var roleSetIds = candidate.roleSetIds.toString().split(',');
				for ( var i = 0; i < roleSetIds.length; i++) {
					candidate.roleSet += ds.targetRolesStore.getById(Number(roleSetIds[i])).data.value+',';
				}
				candidate.roleSet = candidate.roleSet.substring(0,candidate.roleSet.length-1);
			}
			
			table += '<tr style="background-color:#ffffff;">'+
				 		'<td >'+candidate.contactNumber+'</td>'+
				 		'<td ><a href="mailto:'+candidate.emailId+'" target="_blank">'+candidate.emailId+'</a></td>'+
				 		'<td >'+candidate.immigrationStatus+'</td>'+
				 		'<td >'+candidate.immigrationVerified+'</td>'+
				 		'<td >'+candidate.location+'</td>'+
				 		'<td >'+candidate.cityAndState+'</td>'+
				 		'<td style="color:red">'+candidate.alert+'</td>'+
				 		'<td >'+candidate.expectedRate+'</td>'+
				 		'<td >'+candidate.rateFrom+'</td>'+
				 		'<td >'+candidate.rateTo+'</td>'+
			 		'</tr>';
	 
			info = '<table cellpadding="5" bgcolor="#575252">'+table+'</table><br>';
			
			info += '<table cellpadding="5" bgcolor="#575252">'+
				'<tr style="background-color:#ffffff;font-weight:bold;">'+
				'<th>Marketing Status</th>'+
				'<th>Current Job</th>'+
				'<th>Availability</th>'+
				'<th>Qualification</th>'+
				'<th>Emplment type</th>'+
				'<th>Communication</th>'+
				'<th>Experience</th>'+
				'</tr>';

			info += '<tr style="background-color:#ffffff;">'+
				'<td>'+candidate.marketingStatus+'</td>'+
	 			'<td>'+candidate.currentJobTitle+'</td>'+
	 			'<td>'+candidate.availability+'</a></td>'+
	 			'<td>'+candidate.highestQualification+'</td>'+
	 			'<td>'+candidate.employmentType+'</td>'+
	 			'<td>'+candidate.communication+'</td>'+
	 			'<td>'+candidate.experience+'</td>'+
	 			'</tr>'+'</table><br>';
			
			table = '<tr style="background-color:#ffffff;"><th width="100px">Certifications</th>'+'<td>'+candidate.allCertifications.replace(/ -- /g, '<br />')+'</td></tr>'+
					'<tr style="background-color:#ffffff;"><th>Skill Set</th>'+'<td>'+candidate.skillSet+'</td></tr>'+
					'<tr style="background-color:#ffffff;"><th>Role Set</th>'+'<td>'+candidate.roleSet+'</td></tr>'+
					'<tr style="background-color:#ffffff;"><th>Target Skill set</th>'+'<td>'+candidate.targetSkillSet+'</td></tr>'+
					'<tr style="background-color:#ffffff;"><th>Target Roles</th>'+'<td>'+candidate.targetRoles+'</td></tr>'+
					'<tr style="background-color:#ffffff;"><th>P1 Comments</th>'+'<td>'+candidate.p1comments.replace(/\n/g, '<br />')+'</td></tr>'+
					'<tr style="background-color:#ffffff;"><th>P2 Comments</th>'+'<td>'+candidate.p2comments.replace(/\n/g, '<br />')+'</td></tr>';
			
			info += '<table cellpadding="5" bgcolor="#575252">'+table+'</table><br>';
			
			this.quickInfoPanel.form.findField('quickInfo').setValue(info);
			this.quickInfoPanel.expand();
			
			if (candidate.resumeLink != '' && candidate.resumeLink != null)
				this.form.findField('resumeHyperlink').setValue("<a href="+candidate.resumeLink+" target='_blank'><img align='top' src='images/icon_edit.gif'>&nbsp;View Resume</a>");

		}
	},
	
	getSubmissions : function(candidateId) {
		var params = new Array();
    	params.push(['candidateId','=', candidateId]);
    	params.push(['table','=', "submissions"]);
		params.push(['order','=', 'submittedDate']);
		params.push(['deleted','=', 'false']);
		params.push(['orderby','=', 'DESC']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.requirementService.getSubRequirements(Ext.JSON.encode(filter),this.onGetSubRequirements,this);
	},
	
	onGetSubRequirements : function(data) {
		if (data.rows.length > 0) {
			var submissionsStore = new tz.store.Sub_RequirementStore();
			submissionsStore.add(data.rows);
	    	var headings = "<tr style='background-color:#dddddd;font-weight:bold;'>" +
	    					"<td>Job Id</td>" +
	    					"<td>Vendor</td>" +
	    					"<td>Client</td>" +
	    					"<td>Submitted Date</td>" +
	    					"<td>Submission Status</td>" +
	    					"<td>Submitted Rate-1</td>" +
	    					"<td>Submitted Rate-2</td>" +
	    					"<td>Posting Date</td>" +
	    					"<td>Posting Title</td>" +
	    					"<td>Module</td>" +
	    					"<td>Location</td>" +
	    					"<th>Resume</th></tr>";
	    	var table ="";
	    	for ( var i = 0; i < submissionsStore.getCount() && i < 5; i++) {
				var rec = submissionsStore.getAt(i).data;
				if (rec.vendor == null)
					rec.vendor = 'N/A';
				if (rec.client == null)
					rec.client = 'N/A';
				var background = getSubmissionBackGround(submissionsStore.getAt(i));
				table += "<tr bgcolor='#"+background+"'>" +
							"<td><a onclick='javascript:app.homeMgmtPanel.homeDetailPanel.requirementsGrid1.editRequirement(\""+rec.requirementId+"\")' >"+rec.requirementId+"</td>"+
							"<td>"+rec.vendor+"</td>" +
							"<td>"+rec.client+"</td>" +
							"<td>"+Ext.Date.format(new Date(rec.submittedDate),'m/d/Y')+"</td>" +
							"<td>"+rec.submissionStatus+"</td>" +
							"<td>"+rec.submittedRate1+"</td>" +
							"<td>"+rec.submittedRate2+"</td>" +
							"<td>"+Ext.Date.format(new Date(rec.postingDate),'m/d/Y')+"</td>" +
							"<td>"+rec.postingTitle+"</td>" +
							"<td>"+rec.module+"</td>" +
							"<td>"+rec.location+"</td>" ;
				if (rec.resumeLink != null && rec.resumeLink != '')
					table += "<td><a href="+rec.resumeLink+" target='_blank'>View</a></td></tr>" ;
				else
					table += "<td>n/a</td></tr>" ;
			}
			if (table != '')
				table = "<table cellpadding='5' bgcolor='#575252'>"+headings+table+'</table>';
			else 
				table = 'There are no submissions';
			this.quickInfoPanel.form.findField('submissions').setValue('<b>Last 5 Submissions : </b>'+table);

		}
	},
	
	getVendorInfo : function(vendorId) {
		recId = this.quickAccessCombo.getValue();
		var record = ds.quickAccessStore.getById(recId);
		var params = new Array();
		if (record.data.type== 'Vendor')
			params.push(['vendorId','=', vendorId]);
		else
			params.push(['clientId','=', vendorId]);
		params.push(['type','=', record.data.type]);
		params.push(['table','=', "submissions"]);
		params.push(['order','=', 'submittedDate']);
		params.push(['orderby','=', 'DESC']);
    	var filter = getFilter(params);
    	filter.pageSize=5;
    	app.requirementService.getSubRequirements(Ext.JSON.encode(filter),this.onGetVendorInfo,this);
    	app.quickAccessService.getStatistics(Ext.JSON.encode(filter),this.onGetStatistics,this);
	},
	
	onGetVendorInfo : function(data) {
		var recId = this.quickAccessCombo.getValue();
		var record = ds.quickAccessStore.getById(recId);
		if (record.data.type== 'Vendor')
			var vendor  = ds.vendorStore.getById(parseInt(record.data.referenceId));
		else
			var vendor  = ds.clientSearchStore.getById(parseInt(record.data.referenceId));

		var table = "<b>Contact Information : </b>"+
					"<table cellpadding='5' bgcolor='#575252'> " +
					"<tr bgcolor='#ffffff'><td>Contact Number</td><td>Fax</td><td>Email Id</td><td>City</td><td>State</td><td>Ok to train Candidate</td>" +
					"<tr bgcolor='#ffffff'><td>"+vendor.data.contactNumber+"</td><td>"+vendor.data.fax+"</td>" +
					"<td><a href='mailto:"+vendor.data.email+"' target='_top'>"+vendor.data.email+"</a></td>" +
					"<td>"+vendor.data.city+"</td><td>"+vendor.data.state+"</td><td>"+vendor.data.helpCandidate+"</td></tr> " +
					"</table>";
		this.quickInfoPanel.form.findField('quickInfo').setValue(table);
		
		if (data.rows.length > 0) {
			var submissionsStore = new tz.store.Sub_RequirementStore();
			submissionsStore.add(data.rows);

			var table = "<b>Last 5 Submissions : </b>" +
					"<table cellpadding='5' bgcolor='#575252'> <tr bgcolor='#ffffff'><th>Job Id</th><th>Candidate</th>" ;
			if (vendor.data.type== 'Vendor')
				table += "<th>Client</th>";
			else
				table += "<th>Vendor</th>";
			table += "<th>Submitted Date</th><th>Submission Status</th><th>Submitted Rate-1</th><th>Submitted Rate-2</th>" +
					"<th>Posting Date</th><th>Posting Title</th><th>Module</th><th>Resume</th></tr>" ;
	    	for ( var i = 0; i < submissionsStore.getCount() && i < 5; i++) {
				var record = submissionsStore.getAt(i).data;
				var background = getSubmissionBackGround(submissionsStore.getAt(i));
				table += "<tr bgcolor='#"+background+"'>" +
						"<td><a onclick='javascript:app.homeMgmtPanel.homeDetailPanel.requirementsGrid1.editRequirement(\""+record.requirementId+"\")' >"+record.requirementId+"</td>"+
						"<td>"+record.candidate+"</td>";
				if (vendor.data.type== 'Vendor')
					table += "<td>"+record.client+"</td>";
				else
					table += "<td>"+record.vendor+"</td>";
				table += "<td>"+Ext.Date.format(new Date(record.submittedDate),'m/d/Y')+"</td>";
				table += "<td>"+record.submissionStatus+"</td>";
				table += "<td>"+record.submittedRate1+"</td>";
				table += "<td>"+record.submittedRate2+"</td>";
				table += "<td>"+Ext.Date.format(new Date(record.postingDate),'m/d/Y')+"</td>";
				table += "<td>"+record.postingTitle+"</td>";
				table += "<td>"+record.module+"</td>";
				if (record.resumeLink != null && record.resumeLink != '')
					table += "<td><a href="+record.resumeLink+" target='_blank'>View</a></td></tr>" ;
				else
					table += "<td>n/a</td></tr>" ;

			}
			table += "</table>";
			this.quickInfoPanel.form.findField('submissions').setValue(table);
			
		}

		this.quickInfoPanel.expand();
	},

	onGetStatistics : function(data) {
		if (data.rows.length > 0) {
			var stats = "<table cellpadding='5' bgcolor='#575252'><tr bgcolor='#F2F2F2'>" +
						"<td >Reqs</td>"+
						"<td >Reqs submitted to</td>"+
						"<td style='background:#DBE5F1'>%</td>"+
						"<td >Submissions</td>" +
						"<td >Interview ? </td>" +
						"<td style='background:#DBE5F1'>%</td>"+
						"<td >Accepted ? </td>" +
						"<td style='background:#DBE5F1'>%</td></tr>";
			for ( var i = 0; i < data.rows.length; i++) {
				var record = data.rows[i];
				var subsPercentage = 0, interviewPercentage = 0 ,placementsPercentage = 0;
				
				if (record.reqs == 0)
					subsPercentage = record.subReqs*100/1;
				else
					subsPercentage = record.subReqs*100/record.reqs;
				if (record.subs == 0){
					interviewPercentage = record.interviews*100/1;
					placementsPercentage = record.placements*100/1;
				}else{
					interviewPercentage = record.interviews*100/record.subs;
					placementsPercentage = record.placements*100/record.subs;
				}
				
				stats += "<tr bgcolor='#FFFFFF' style='text-align:center;'>" +
						"<td >"+record.reqs+"</td>"+
						"<td >"+record.subReqs+"</td>" +
						"<td style='background:#DBE5F1'>"+subsPercentage.toFixed(2)+"</td>" +
						"<td >"+record.subs+"</td>" +
						"<td >"+record.interviews+"</td>" +
						"<td style='background:#DBE5F1'>"+interviewPercentage.toFixed(2)+"</td>" +
						"<td >"+record.placements+"</td>" +
						"<td style='background:#DBE5F1'>"+placementsPercentage.toFixed(2)+"</td>" +
						"</tr>";
			}
			stats += "</table>";
			this.quickInfoPanel.form.findField('certifications').setValue('<b>Submission Stats: (Full) </b>'+stats);
		}
	},
	
	showCandidate : function(candidateId) {
		var recId = this.quickAccessCombo.getValue();
		var record = ds.quickAccessStore.getById(recId);
		var candidateId = record.data.referenceId;
		var params = new Array();
		params.push(['id','=', candidateId]);
		var filter = getFilter(params);
		filter.pageSize=50;
		app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidate,this);
	},
	
	onGetCandidate : function(data) {
		if (data.rows.length > 0) {
			app.setActiveTab(0,'li_candidates');
			app.candidateMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			app.candidateMgmtPanel.candidatePanel.loadForm(record);
			app.candidateMgmtPanel.candidatePanel.openedFrom = 'Candidate';
		}
	},

	showVendor : function(vendorId) {
		var recId = this.quickAccessCombo.getValue();
		var record = ds.quickAccessStore.getById(recId);
		var vendorId = record.data.referenceId;
		var params = new Array();
		params.push(['id','=', vendorId]);
		var filter = getFilter(params);
		filter.pageSize=50;
		app.clientService.getClients(Ext.JSON.encode(filter),this.onGetVendor,this);
	},
	
	onGetVendor : function(data) {
		if (data.rows.length > 0) {
	    	app.setActiveTab(0,'li_clients');
	    	app.clientMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Client', data.rows[0]);
	    	app.clientMgmtPanel.clientPanel.loadForm(record);
	    	app.clientMgmtPanel.clientPanel.openedFrom = 'Client';
		}
	},

	editRecord : function() {
		var recId = this.quickAccessCombo.getValue();
		var record = ds.quickAccessStore.getById(recId);
		if (record.data.type == 'Candidate') {
			this.showCandidate(record.data.referenceId);
		}else{
			this.showVendor(record.data.referenceId);
		}
	},
	
	showReqSnapshot : function(requirementId) {
		this.reqQuickInfoPanel.form.reset();
		var params = new Array();
    	params.push(['id','=', requirementId]);
    	var filter = getFilter(params);
    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onGetRequirements,this);
    	
	},
	
	onGetRequirements : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = Ext.create('tz.model.Requirement', data.rows[0]).data;
			
			if (record.clientId != null && record.clientId != '') {
				var client = ds.clientSearchStore.getById(Number(record.clientId)).data.name;
			}else
				var client = '';
			
			var vendor ="";
        	if (record.vendorId != null && record.vendorId != '') {
				var ids = record.vendorId.toString().split(',');
				for ( var i = 0; i < ids.length; i++) {
					var rec = ds.vendorStore.getById(parseInt(ids[i]));
					if (rec)
						vendor += rec.data.name +", ";	
				}
				if (vendor.length > 1)
					vendor = vendor.substring(0,vendor.length-2);
			}
        	
        	var hotness ='';
        	var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',record.hotness));
        	if(rec)
        		hotness = rec.data.value.substring(rec.data.value.indexOf('.')+1);

        	if (record.resume == null || record.resume == 'null')
        		record.resume = 'N/A';
        	
			var detailsTable = "<table width ='100%' border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>" +
								"<col width='20%'><col width='30%'><col width='20%'><col width='30%'>"+
								"<tr style='text-align:left;'><th>Job Id</th><td colspan='3'>" +
									"<a onclick='javascript:app.homeMgmtPanel.homeDetailPanel.requirementsGrid1.editRequirement(\""+record.id+"\")' >" +
									"<img align='top' src='images/icon_edit.gif'>&nbsp;"+record.id+"</td></tr>"+
								"<tr style='text-align:left;'><th>Posting Date </th><td colspan='3'>"+Ext.Date.format(new Date(record.postingDate),'m/d/Y h:i A')+"</td></tr>"+
								"<tr style='text-align:left;'><th>Posting Title</th><td>" +record.postingTitle+"</td>" +
										"<th>Alert</th><td style='color:red;'>" +record.addInfo_Todos+"</td></tr>"+
								"<tr style='text-align:left;'><th>Hotness</th><td>" +hotness+"</td>" +
										"<th>Job Opening Status</th><td>" +record.jobOpeningStatus+"</td></tr>"+
								"<tr style='text-align:left;'><th>City & State</th><td>" +record.cityAndState+"</td>" +
										"<th>Location</th><td>" +record.location+"</td></tr>"+
								"<tr style='text-align:left;'><th>Module</th><td colspan='3'>" +record.module+"</td></tr>"+
								"<tr style='text-align:left;'><th>Employer</th><td>" +record.employer+"</td>" +
										"<th>Resume</th><td>" +record.resume+"</td></tr>"+
								"<tr style='text-align:left;'><th>Max Bill Rate</th><td>" +record.rateSubmitted+"</td>" +
										"<th>Source</th><td>" +record.source+"</td></tr>";
										
					if(record.remote)
						detailsTable += "<tr style='text-align:left;'><th>Remote</th><td colspan='3'>Yes</td></tr>";
					else
						detailsTable += "<tr style='text-align:left;'><th>Remote</th><td colspan='3'>No</td></tr>" ;
					
					if(record.relocate)
						detailsTable += "<tr style='text-align:left;'><th>Relocation Needed</th><td colspan='3'>Yes</td></tr>" ;
					else
						detailsTable += "<tr style='text-align:left;'><th>Relocation Needed</th><td colspan='3'>No</td></tr>" ;
					
					if(record.travel)
						detailsTable += "<tr style='text-align:left;'><th>Travel Needed</th><td colspan='3'>Yes</td></tr></tr>" ;
					else
						detailsTable += "<tr style='text-align:left;'><th>Travel Needed</th><td colspan='3'>No</td></tr>" ;
					
					
					detailsTable += "<tr style='text-align:left;'><th>Client</th><td>" +client+"</td>" +
										"<th>Vendor</th><td>" +vendor+"</td></tr>"+
									"<tr style='text-align:left;'><th>Ok to train Candidate</th><td colspan='3'>" + record.helpCandidate +"</td></tr>"+
									"<tr style='text-align:left;'><th>Skill Set</th><td colspan='3'>" +record.skillSet+"</td></tr>"+
									"<tr style='text-align:left;'><th>Communication Required</th><td>" +record.communication+"</td>" +
										"<th>Experience Required</th><td>" +record.experience+"</td></tr>" +
									"<tr style='text-align:left;'><th>Role Set</th><td colspan='3'>" +record.targetRoles+"</td></tr>"+
									"<tr style='text-align:left;'><th>Mandatory Certifications</th><td colspan='3'>" +record.certifications +"</td></tr>" +
									"<tr style='text-align:left;'><th>Roles and Responsibilities</th><td colspan='3'><br>" +record.requirement.replace(/(?:\r\n|\r|\n)/g, '<br>')+"</td></tr>"+
							'</table>';
			
			this.reqQuickInfoPanel.form.findField('quickInfo').setValue(detailsTable);
			
	    	var browserHeight = app.mainViewport.height;
	    	var browserWidth = app.mainViewport.width;

	    	if (app.windowXPosition+200 > browserWidth ){
	    		app.windowXPosition = 0;
	    		app.windowYPosition = -60;
	    	}
	    	var windowXPosition = app.windowXPosition;
	    	app.windowXPosition += 150;
	    	var windowYPosition = app.windowYPosition;
	    	app.windowId += 1;
			var snapshotWindow = Ext.create('Ext.window.Window', {
				title: record.id,
				height : browserHeight-100,
				width: browserWidth*70/100,
				id : 'window'+app.windowId,
				modal: false,
				minimizable: true,
				layout: 'fit',
				border : 0,
				listeners: {
					"minimize": function (window, opts) {
						window.collapse();
						window.setWidth(150);
						window.alignTo(Ext.getBody(), 'bl-bl',[windowXPosition,windowYPosition]);
					},
					"close": function (window, opts) {
						app.arrangeWindows();
					}
				},
				tools: [{
					type: 'prev',
					handler: function (evt, toolEl, owner, tool) {
						var window = owner.up('window');
						window.alignTo(Ext.getBody(), 'tl-tl',[0,90],true);
					}
				},{
					type: 'next',
					handler: function (evt, toolEl, owner, tool) {
						var window = owner.up('window');
						window.alignTo(Ext.getBody(), 'tr-tr',[0,90],true);                
					}
				},{
					type: 'restore',
					handler: function (evt, toolEl, owner, tool) {
						var window = owner.up('window');
						window.setWidth(browserWidth*70/100);
						window.expand('', false);
						window.center();
					}
				}],
				items: [this.reqQuickInfoPanel],
			});
			snapshotWindow.show();
		}
		
    	var params = new Array();
    	params.push(['requirementId','=', record.id]);
    	var filter = getFilter(params);
    	ds.subRequirementStore.loadByCriteria(filter);
    	app.requirementService.getSubRequirements(Ext.JSON.encode(filter),this.onGetSubmissions,this);

	},
	
	onGetSubmissions : function(data) {
		if (data.rows.length > 0) {
			var headings = "<table width ='100%' border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>" +
					"<tr bgcolor='#CAF0CA'>" +
					"<th>Candidate</th>" +
					"<th>Vendor</th>" +
					"<th>Submitted Date</th>" +
					"<th>Submission Status</th>" +
					"<th>Submitted Rate-1</th>" +
					"<th>Submitted Rate-2</th>" +
					"<th>Resume</th>" +
					"</tr>" ;
			var shortHeadings = "<table width ='100%' border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>" +
					"<tr bgcolor ='#FFF753'><th>Candidate</th><th>Expected Rate</th><th>Job help</th><th>Home Loc</th></tr>" ;
			
			var table ='', shortTable ='';
			
			for ( var i = 0; i < data.rows.length; i++) {
				var record = data.rows[i];
				if (record.submissionStatus == 'Shortlisted') {
					shortTable += "<tr>" +
							"<td>"+record.candidate+"</td>" +
							"<td>"+record.candidateExpectedRate+"</td>" +
							"<td>"+record.jobHelp+"</td>" +
							"<td>"+record.location+"</td></tr>" ;
				}else{
					table += "<tr>" +
							"<td>"+record.candidate+"</td>" +
							"<td>"+record.vendor+"</td>" +
							"<td>"+Ext.Date.format(new Date(record.submittedDate),'m/d/Y')+"</td>" +
							"<td>"+record.submissionStatus+"</td>" +
							"<td>"+record.submittedRate1+"</td>" +
							"<td>"+record.submittedRate2+"</td>" ;
					if (record.resumeLink != null && record.resumeLink != '')
						table += "<td><a href="+record.resumeLink+" target='_blank'>View</a></td></tr>" ;
					else
						table += "<td>n/a</td></tr>" ;
				}
			}
			if (table.length > 0)
				table = "<b>Submitted Candidates :</b>"+headings+table+"</table><br>";
			if (shortTable.length > 0)
				shortTable = "<b>Shortlisted Candidates :</b>"+shortHeadings+shortTable+"</table><br>";
		
			
			this.reqQuickInfoPanel.form.findField('shortlist').setValue(shortTable);
			this.reqQuickInfoPanel.form.findField('submissions').setValue(table);
			
			//loading suggested vendors
			if (record.clientId != null && record.clientId != ''){
				var params = new Array();
				params.push(['relatedVendors','=', record.clientId]);
				params.push(['deleted','=', 'false']);
				var filter = getFilter(params);
				app.clientService.getClients(Ext.JSON.encode(filter),this.onGetSuggestedVendors,this);
			}

		}
	},
	
	onGetSuggestedVendors : function(data) {
		if (data.success && data.rows.length > 0) {
			var table = "<table width ='100%' border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>" +
						"<tr>" +
						"<th>Name</th>" +
						"<th>Score</th>" +
						"<th>Address</th>" +
						"<th>Contact Number</th>" +
						"<th>Fax</th>" +
						"<th>Email</th>" +
						"<th>Website</th>" +
						"<th>Industry</th>" +
						"</tr>" ;

			for ( var i = 0; i < data.rows.length; i++) {
				var record = Ext.create('tz.model.Client', data.rows[i]).data;
				table += "<tr>" +
						"<td>"+record.name+"</td>" +
						"<td>"+record.score+"</td>" +
						"<td>"+record.address+"</td>" +
						"<td>"+record.contactNumber+"</td>" +
						"<td>"+record.fax+"</td>" +
						"<td>"+record.email+"</td>" +
						"<td>"+record.website+"</td>" +
						"<td>"+record.industry+"</td>" ;
			}
			table = "<b>Suggested Vendors :</b>"+table+"</table><br>";
			this.reqQuickInfoPanel.form.findField('suggested').setValue(table);
		}
	}

});
