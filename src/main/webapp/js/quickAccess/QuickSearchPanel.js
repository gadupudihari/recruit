Ext.define('tz.ui.QuickSearchPanel', {
    extend: 'Ext.form.Panel',    
    title: '',
    frame:true,
    anchor:'100%',
    autoScroll:true,
//creating view in init function
    initComponent: function() {
        var me = this;
        me.openedFrom ='';

        me.quickAccessCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Quick Access Search',
		    store: ds.quickAccessStore,
		    padding:'0 0 0 0',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'id',
		    name : 'name',
		    forceSelection : true,
		    labelAlign : 'right',
		    anyMatch:true,
		    width :350,
		    labelWidth :120,
			listConfig: {
		        cls: 'grouped-list'
		      },
		      tpl: Ext.create('Ext.XTemplate',
		        '{[this.currentKey = null]}' +
		        '<tpl for=".">',
		          '<tpl if="this.shouldShowHeader(type)">' +
		            '<div class="group-header">{[this.showHeader(values.type)]}</div>' +
		          '</tpl>' +
		          '<div class="x-boundlist-item">{name}</div>',
		        '</tpl>',
		        {
		          shouldShowHeader: function(name){
		            return this.currentKey != name;
		          },
		          showHeader: function(name){
		            this.currentKey = name;
		            return name;
		          }
		        }
		      )
		});

        me.items = [{
        	xtype:'container',
	   	        layout: 'table',
	   	        items :[{
	   	            xtype: 'container',
	   	            layout: 'table',
	   	            items: [me.quickAccessCombo,
	   	                    {
                        xtype:'button',
                        margin: '0 0 0 20',
                        text: 'Search',
                        scope:me,
                        handler: function(){
                        	me.manager.showQuickAccess();
                        }
                    },{
                        xtype:'button',
                        margin: '0 0 0 10',
                        text: 'Clear',
                        scope:me,
                        handler: function(){
                        	me.form.reset();
                        }
                    }]
	   	        }]
        }]; 
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

});
