Ext.define('tz.ui.QuickInterviewGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Interviews',
    frame:true,
    anchor:'100%',
    width :'98%',
    listeners: {
		afterrender: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
    },
    
    initComponent: function() {
        var me = this;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
            	xtype : 'actioncolumn',
            	width : 30,
            	items : [{
            		icon : 'images/icon_edit.gif',
            		tooltip : 'Edit/View Interview',
            		padding : 10,
            		scope : this,
            		handler : function(grid, rowIndex, colIndex) {
            			me.editInterview(me.store.getAt(rowIndex));
            		} 
            	}]
            },{
				header : 'Candidate',
				dataIndex : 'candidate',
	            width :  150,
	            renderer: interviewRender,
			},{
				header : 'Client',
				dataIndex : 'clientId',
	            width :  120,
	            renderer:function(value, metadata, record){
                	var rec = ds.clientSearchStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.name + '"';
                		return rec.data.name;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
				header : 'Vendor',
				dataIndex : 'vendorId',
	            width :  120,
	            renderer:function(value, metadata, record){
                	var rec = ds.vendorStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.name + '"';
                		return rec.data.name;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
                dataIndex: 'contactId',
                text: 'Interviewer',
                width:120,
	            renderer:function(value, metadata, record){
                	if (value != null && value != '') {
    					var ids = value.toString().split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.fullName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return names.substring(0,names.length-2);
                	}
                	return 'N/A';
                }
            },{
                dataIndex: 'interviewDate',
                text: 'Interview Date',
                width:120,
                renderer: interviewDateRender,
            },{
                dataIndex: 'time',
                text: 'Time',
                width:120,
                renderer: interviewRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                width:200,
                renderer: interviewRender,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastUpdatedUser',
                text: 'Last Updated User',
                renderer: interviewRender,
            },{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:150,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                //format: "m/d/Y H:i:s A",
                renderer: lastUpdatedRender,
                text: 'Last Updated',
                width:150,
            }
        ];
        me.viewConfig = {
        		stripeRows: true	
        };
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: ['->',me.showCount]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        ds.contractorsStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.clientSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.contactSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        me.callParent(arguments);
    },
    
    editInterview : function(record) {
    	app.setActiveTab(0,'li_interviews');
    	app.interviewMgmtPanel.getLayout().setActiveItem(1);
    	app.interviewMgmtPanel.interviewGridPanel.setStores();
    	app.interviewMgmtPanel.interviewDetailPanel.form.reset();
    	app.interviewMgmtPanel.interviewDetailPanel.loadForm(record);
    	app.interviewMgmtPanel.interviewDetailPanel.openedFrom ='Interview';
    },
        
});

