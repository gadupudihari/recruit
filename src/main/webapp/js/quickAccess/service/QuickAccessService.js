Ext.define('tz.service.QuickAccessService', {
	extend : 'tz.service.Service',
		
	getEmployeeInfo: function(employeeId,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/quickAccess/getEmployeeInfo',
			params : {
				json : employeeId
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    getVendorInfo: function(vendorId,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/quickAccess/getVendorInfo',
			params : {
				json : vendorId
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    getStatistics : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/quickAccess/getStatistics',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

});
