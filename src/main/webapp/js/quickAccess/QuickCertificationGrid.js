Ext.define('tz.ui.QuickCertificationGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Certifications',
    frame:true,
    anchor:'100%',
    width :'100%',    
     
    initComponent: function() {
        var me = this;
        
        me.columns = [
          			{
        				xtype: 'datecolumn',
        				header : 'Date Achieved',
        				dataIndex : 'dateAchieved',
        				width:80,
        	            renderer: Ext.util.Format.dateRenderer('m/d/Y'),
        			},{
        				text : 'Certification Name',
        				dataIndex : 'name',
        				width :  120,
        			},{
        				text : 'Status',
        				dataIndex : 'status',
        				width:80,
                        renderer:function(value, metadata, record){
                        	var rec = ds.certificationStatusStore.getAt(ds.certificationStatusStore.findExact('name',value));
                        	if(rec){
                        	    metadata.tdAttr = 'data-qtip="' + value + '"';
                        	    if (rec.data.value == 'Approval Process') 
                        	    	rec.data.value.style = "background-color:#d9ead3;border-color: #d9ead3;";
                        	    else if (value == 'Yet to Start') 
                        	    	metadata.style = "background-color:#fff2cc;border-color: #fff2cc;";
                        	    else if (rec.data.value == 'Certification in progress') 
                        	    	metadata.style = "background-color:#cfe2f3;border-color: #cfe2f3;";
                        	    else if (rec.data.value == 'Certified') 
                        	    	metadata.style = "background-color:#ead1dc;border-color: #ead1dc;";
                        	    else if (rec.data.value == 'Issue') 
                        	    	metadata.style = "background-color:#ff9900;border-color: #ff9900;";
                        	    
                        	    return rec.data.value;
                        	}else{
                        		metadata.tdAttr = 'data-qtip="' + value + '"';
                        		return value;	
                        	}
        	            }
        			},{
        				text : 'Sponsored By',
        				dataIndex : 'sponsoredBy',
        				width :  120,
        				renderer : sponsoredRenderer,
        			},{
        				text : 'Reimbursement Status',
        				dataIndex : 'reimbursementStatus',
        				width :  120,
        			},{
        				text : 'Version',
        				dataIndex : 'version',
        				width:80,
        			},{
                        dataIndex: 'certificationComments',
                        text: 'Certification Comments',
                        labelAlign:'center',
            			align : 'left',
                        width:150,
                    },{
        				text : 'Epic User Web',
        				dataIndex : 'epicUserWeb',
        			},{
        				text : 'Latest NVT',
        				dataIndex : 'latestNVT',
        				width:80,
        			},{
        				text : 'Expenses Paid or not',
        				dataIndex : 'expensesStatus',
        				renderer : greenTooltipRenderer,
        			},{
        				text : 'CPS Payment to Epic',
        				dataIndex : 'cpsPaymentToEpic',
        				renderer : Ext.util.Format.usMoney,
        				width:90,
        			},{
        				text : 'Payment to Trainer',
        				dataIndex : 'paymentDetails',
        				renderer : greenTooltipRenderer,
        			},{
        				text : 'Epic Invoice Amt',
        				dataIndex : 'epicInvoiceAmount',
        				renderer : function(value){
        					return '<b>'+Ext.util.Format.usMoney(value)+'</b>';
        				},
        				width:90,
        			},{
        				text : 'Epic Invoice No',
        				dataIndex : 'epicInvoiceNo',
        				renderer : greenTooltipRenderer,
        				width:90,
        			},{
        				text : 'Epic Invoice Comments',
        				dataIndex : 'epicInvoiceComments',
        				width:120,
        			},{
        				text : 'Verified',
        				dataIndex : 'verified',
        				width:90,
        			},{
        				text : 'Expenses Incurred',
        				dataIndex : 'expensesIncurred',
        				renderer : Ext.util.Format.usMoney,
        				width:90,
        			},{
        				text : 'Expenses Paid',
        				dataIndex : 'expensesPaid',
        				renderer : function(value){
        					return '<b>'+Ext.util.Format.usMoney(value)+'</b>';
        				},
        				width:90,
        			},{
        				text : 'Expense Comments',
        				dataIndex : 'expenseComments',
        				flex : 1,
        			},{
        				xtype: 'gridcolumn',
        				dataIndex: 'createdUser',
        				text: 'Created User',
        				width:100,
        			},{
        				xtype: 'gridcolumn',
        				dataIndex: 'lastUpdatedUser',
        				text: 'Last Updated User',
        				width:100,
        			},{
        				xtype: 'datecolumn',
        				dataIndex: 'created',
        				format: "m/d/Y H:i:s A",
        				text: 'Created',
        				hidden:true,
        				width:150,
        			},{
        				xtype: 'datecolumn',
        				dataIndex: 'lastUpdated',
        				format: "m/d/Y H:i:s A",
        				text: 'Last Updated',
        				hidden:true,
        				width:150,
                     }
                ];
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: ['->',me.showCount]
        }];
   
        me.viewConfig = {
        		stripeRows: true,
        };

        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        me.callParent(arguments);
    }, 
	
});
