Ext.define('tz.ui.QuickAccessMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;
        me.quickAccessPanel = new tz.ui.QuickAccessPanel({manager:me});
        
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	centered:true,
            	border:false,
            	height : '100%',
            	items: [me.quickAccessPanel]
            }
        ];
        me.callParent(arguments);
    },
	
	initScreen: function(){
		this.getLayout().setActiveItem(0);
		ds.quickAccessStore.loadByCriteria();
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.clientSearchStore.loadByCriteria(filter);
    	ds.contactSearchStore.loadByCriteria(filter);
    	ds.vendorStore.loadByCriteria(filter);
    	ds.contractorsStore.loadByCriteria(filter);
    	ds.candidateSearchStore.loadByCriteria(filter);

	},
	
});
 