Ext.define('tz.ui.CandidatesEmailGrid', {
    extend: 'Ext.grid.Panel',
    title: '',
    frame:false,
    width :'100%',
    height : 200,
    
    listeners: {
        edit: function(editor, event){
        	var record = event.record;
    		if(editor.context.field == 'contactNumber'){
    			record.data.contactNumber = record.data.contactNumber.replace(/[^0-9]/g, "");
            	record.data.contactNumber = record.data.contactNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
            	this.getView().refresh();
            }else if (editor.context.field == 'certifications') {
            	while (record.data.certifications.indexOf('"') != -1) {
            		record.data.certifications = record.data.certifications.replace('"','');	
				}
                this.getView().refresh();
            }
            var scrollPosition = this.getEl().down('.x-grid-view').getScroll();
        	this.getView().refresh();
        	//And then after the record is refreshed scroll to the same position
        	this.getEl().down('.x-grid-view').scrollTo('top', scrollPosition.top, false);
        },
    },
    initComponent: function() {
        var me = this;
        me.store = ds.candidateEmailStore;
        
        me.columns = [
                      {
                          xtype: 'actioncolumn',
                          width :30,
                          items : [{
              				icon : 'images/icon_delete.gif',
              				tooltip : 'View Candidate',
              				padding: 50,
              				scope: this,
              				handler : function(grid, rowIndex, colIndex) {
              					me.store.remove(me.store.getAt(rowIndex));
              					me.getView().refresh();
              				}
              			}]
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'priority',
                          text: 'Priority',
                          width : 40,
                          renderer : numberRender,
          				editor: {
                              xtype: 'numberfield',
                              minValue: 0,
                              maxValue: 100
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'marketingStatus',
                          text: 'Marketing Status',
                          renderer: candidateRender,
                          width:55,
                          editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'value',
                  		    valueField: 'name',
                              store: ds.marketingStatusStore,
                              forceSelection : true,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
                      },{
          				text : 'Unique Id',
          				dataIndex : 'id',
          				width :  40,
          		        renderer: candidateRender,
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'firstName',
                          text: 'First Name',
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:45,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'lastName',
                          text: 'Last Name',
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:45,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'certifications',
                          text: 'Certifications',
                          width:120,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textarea',
                              height : 60,
                              maxLength:500,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'skillSet',
                          text: 'Skill Set',
                          width:120,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textarea',
                              height : 60,
                              maxLength:2000,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'alert',
                          text: 'Alert',
                          width:70,
                          hidden : true,
                          renderer: candidateAlertRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:45,
             	                enforceMaxLength:true,
             	                fieldStyle: "color: red;",
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'emailId',
                          text: 'Email Id',
                          width:60,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:45,
             	                enforceMaxLength:true,
             	                vtype:'email',	
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'contactNumber',
                          text: 'Contact No',
                          width:80,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:15,
                           	maskRe :  /[0-9]/,
             	                enforceMaxLength:true,
                          }
                      },{
          				text : 'Availability',
          				dataIndex : 'availability',
          				xtype: 'datecolumn',
          				width :  68,
          				editor: {
          	                xtype: 'datefield',
          	                format: 'm/d/y'
          	            }
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'p1comments',
                          text: 'P1 Comments',
                          width:90,
                          renderer: candidateRender,
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'p2comments',
                          text: 'P2 Comments',
                          width:90,
                          hidden : true,
                          renderer: candidateRender,
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'source',
                          text: 'Source',
                          width:100,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'value',
                  		    valueField: 'name',
                              store: ds.candidateSourceStore,
                              forceSelection : true,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'referral',
                          text: 'Referral',
                          width:80,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:45,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'expectedRate',
                          text: 'Expected Rate',
                          width:75,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:100,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'rateFrom',
                          text: 'Rate From',
                          width:75,
                          renderer: numberRender,
                          editor: {
                              xtype: 'numberfield',
                              minValue : 0,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'rateTo',
                          text: 'Rate To',
                          width:75,
                          renderer: numberRender,
                          editor: {
                              xtype: 'numberfield',
                              minValue : 0,
                          }
                      },{
          				text : 'Will Relocate',
          				dataIndex : 'relocate',
          				width :  55,
          				renderer: activeRenderer,
          				editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'name',
                              valueField: 'value',
                              store: ds.yesNoStore,
                              forceSelection : true,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
          			},{
          				text : 'Will Travel',
          				dataIndex : 'travel',
          				width :  55,
          				renderer: activeRenderer,
          				editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'name',
                  		    valueField: 'value',
                              store: ds.yesNoStore,
                              forceSelection : true,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
          			},{
          				text : 'Open To CTH',
          				dataIndex : 'openToCTH',
          				width :  75,
          				renderer: candidateRender,
          				editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'name',
                  		    valueField: 'value',
                              store: ds.yesNoStore,
                              forceSelection : true,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'cityAndState',
                          text: 'City & State',
                          width:90,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:90,
             	                enforceMaxLength:true,
                          }
                      },{
          				text : 'Location',
          				dataIndex : 'location',
          				width :  80,
          				renderer: candidateRender,
          				editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'name',
                  		    valueField: 'value',
                              store: ds.candidateLocationStore,
                              forceSelection : true,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
          			},{
          				text : 'Follow Up',
          				dataIndex : 'followUp',
          				width :  60,
          				renderer: followUpRenderer,
          				editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'name',
                  		    valueField: 'value',
                              store: ds.yesNoStore,
                              forceSelection : true,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
          			},{
          				text : 'Employment Type',
          				width:70,
          				dataIndex : 'employmentType',
          				renderer: candidateRender,
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'aboutPartner',
                          text: 'About Partner',
                          width:65,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:100,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'personality',
                          text: 'Personality',
                          width:65,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textarea',
                              height : 60,
                              maxLength:100,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'contactAddress',
                          text: 'Contact Address',
                          width:100,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textarea',
                              height : 60,
                              maxLength:200,
             	                enforceMaxLength:true,
                          }
                      },{
          				text : 'Start Date',
          				dataIndex : 'startDate',
          				width :  68,
          				xtype: 'datecolumn',
          				editor: {
          	                xtype: 'datefield',
          	                format: 'm/d/y'
          	            }
          			},{
          				text : 'Submitted Date',
          				dataIndex : 'submittedDate',
          				width :  100,
          				hidden :true,
          				xtype: 'datecolumn',
          				editor: {
          	                xtype: 'datefield',
          	                format: 'm/d/y'
          	            }
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'education',
                          text: 'Education',
                          width:150,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textarea',
                              height : 60,
                              maxLength:150,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'immigrationStatus',
                          text: 'Immigration Status',
                          renderer: candidateRender,
                          editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'value',
                  		    valueField: 'name',
                  		    forceSelection : true,
                              store: ds.immigrationTypeStore,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
                      },{
          				text : 'Immigration Verified',
          				dataIndex : 'immigrationVerified',
          				width :  100,
          				hidden : true,
          				renderer: candidateRender,
          				editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'name',
                  		    valueField: 'value',
                              store: ds.verifiedStore,
                              forceSelection : true,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'type',
                          text: 'Type',
                          width:100,
                          hidden : true,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'value',
                  		    valueField: 'name',
                  		    forceSelection : true,
                              store: ds.candidateTypeStore,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'currentJobTitle',
                          text: 'Current Job Title',
                          width:120,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:45,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'employer',
                          text: 'Employer',
                          width:80,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'textfield',
                              maxLength:45,
             	                enforceMaxLength:true,
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'highestQualification',
                          text: 'Highest Qualification Held',
                          width:120,
                          renderer: candidateRender,
                          editor: {
                              xtype: 'combobox',
                              queryMode: 'local',
                              typeAhead: true,
                              triggerAction: 'all',
                              selectOnTab: true,
                              displayField: 'value',
                  		    valueField: 'name',
                              store: ds.qualificationStore,
                              forceSelection : true,
                              lazyRender: true,
                              listClass: 'x-combo-list-small'
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'lastUpdatedUser',
                          text: 'Last Updated User',
                          renderer: candidateRender,
                          hidden : true,
                      },{
                          xtype: 'datecolumn',
                          dataIndex: 'created',
                          text: 'Created',
                          format: "m/d/Y H:i:s A",
                          width:150,
                          hidden : true,
                      },{
                          xtype: 'datecolumn',
                          dataIndex: 'lastUpdated',
                          format: "m/d/Y H:i:s A",
                          text: 'Last Updated',
                          width:150,
                          hidden : true,
                      }
                  ];
        me.viewConfig = {
        	stripeRows: true,
        	
        	getRowClass: function (record, rowIndex, rowParams, store) {
        		return record.get('followUp').match('YES') ? 'back-lightblue' : '';
        	}
        };
        
        me.priority = new Ext.form.field.Number({
        	fieldLabel: 'Priority Upto',
        	value : 0,
        	minValue : 0,
        	maxValue : 100,
        });

	    var priority = [];
	    y = 0;
	    while (y<=10){
	    	priority.push([y]);
	         y++;
	    }
	    this.priorityStore = new Ext.data.SimpleStore
	    ({
	          fields : ['priority'],
	          data : priority
	    });

		me.priorityCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Priority',
		    store : this.priorityStore,
		    queryMode: 'local',
		    displayField: 'priority',
            valueField: 'priority',
		    name : 'priority',
		    allowDecimals: false,
		    value : 0,
		    labelWidth :45,
		    width :170,
		});
		
        me.searchField = new Ext.form.field.Text({
			fieldLabel: 'Search',
			name: 'search',
			labelWidth :45,
			width :170,
        });
        
        me.sourceField = new Ext.form.field.Text({
        	fieldLabel: 'Source',
        	name: 'source',
        	labelWidth :45,
        	width :170,
        });
		
        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [me.searchField,me.sourceField,me.priorityCombo,
        	        '-',{
        				text : 'Search',
        				iconCls : 'btn-Search',
        				handler: function(){
        					me.search();
        				}
        	        }
        	        ]
        }];

        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });

        me.callParent(arguments);
        
        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);
        
    },

    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],

    search : function() {
    	var params = new Array();
    	params.push(['priority','=',this.priorityCombo.getValue().toString()]);
    	params.push(['marketingStatus','=', "1. Active"]);
    	params.push(['source','=', this.sourceField.getValue()]);
    	params.push(['search','=', this.searchField.getValue()]);
    	var filter = getFilter(params);
    	ds.candidateEmailStore.loadByCriteria(filter);
	},

});
