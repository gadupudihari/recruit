Ext.define('tz.ui.HomeRequirementsGrid1', {
    extend: 'Ext.grid.Panel',
    title: 'Requirements',
    frame:true,
    width :'100%',
    //split: true,
	//region: 'west', 
	//collapsible: true,
	listeners: {
		afterrender: function(panel) {
			panel.header.el.on('dblclick', function() {
		    	var browserHeight = app.mainViewport.height;
				var headerHt = app.mainViewport.items.items[0].getHeight();
				var grid2 = app.homeMgmtPanel.homeDetailPanel.requirementsGrid2;
				if (panel.collapsed != true && grid2.collapsed != true) {
					// both are expanded
					grid2.collapse();
					grid2.collapsed = true;
					panel.setHeight((browserHeight - headerHt - 75));
				}else {
					//grid1 expanded and grid2 collapsed
					panel.expand();
					grid2.expand();
					panel.setHeight((browserHeight - headerHt - 50)/2);
					grid2.setHeight((browserHeight - headerHt - 50)/2);
				}
			});
		}
	},
	plugins: [{
        ptype: "headericons",
        headerButtons : [
            {
                xtype: 'button',
                tooltip : 'Maximize/Restore',  
                icon : 'images/arrow_out.png',
                scope: this,
                handler : function(panel) {
                	var panel = app.homeMgmtPanel.homeDetailPanel.requirementsGrid1;
    		    	var browserHeight = app.mainViewport.height;
    				var headerHt = app.mainViewport.items.items[0].getHeight();
    				var grid2 = app.homeMgmtPanel.homeDetailPanel.requirementsGrid2;
    				if (panel.collapsed != true && grid2.collapsed != true) {
    					// both are expanded
    					grid2.collapse();
    					grid2.collapsed = true;
    					panel.setHeight((browserHeight - headerHt - 75));
    				}else {
    					//grid1 expanded and grid2 collapsed
    					panel.expand();
    					grid2.expand();
    					panel.setHeight((browserHeight - headerHt - 50)/2);
    					grid2.setHeight((browserHeight - headerHt - 50)/2);
    				}
    			}
            }
        ]
    }],
    initComponent: function() {
        var me = this;
        me.store = ds.homeRequirementStore1;
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :35,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'Edit Job Opening',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					me.editRequirement(me.store.getAt(rowIndex).data.id);
    				}
    			}],
                renderer:function(value, metadata, record){
                	me.columns[0].items[0].tooltip = 'Edit job '+record.data.id ;
	            }
            },{
                xtype: 'actioncolumn',
                width :35,
                items : [{
                	icon : 'images/icon_reschedule.gif',
                	tooltip : 'View Job Opening',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					//this.showSuggested(this.store.getAt(rowIndex).data.id);
    					me.shareRequirement(me.store.getAt(rowIndex));
    				}
    			}]
            },{
		        xtype: 'gridcolumn',
		        dataIndex: 'hotness',
		        text: 'Hotness',
		        width :  45,
                renderer:function(value, metadata, record){
                	if (value == 0) {
                		metadata.style = "color:blue;";
					}
                	var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.value.substring(rec.data.value.indexOf('.')+1)+ '"';
                		return rec.data.value.substring(rec.data.value.indexOf('.')+1);
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
	            }
		    },{
				text : 'Posting Date',
				dataIndex : 'postingDate',
				width :  60,
				renderer:function(value, metadata, record){
					return '<span style="color:blue;">' + Ext.Date.format(value,'m/d/Y h:i:s A') + '</span>';
				}
			},{
    			text : 'Job Opening Status',
    			dataIndex : 'jobOpeningStatus',
    			width :  60,
    			renderer:function(value, metadata, record){
    				metadata.style = 'background-color:#'+getBackGround(record)+';'
    				if (value == '01.In-progress' && record.data.submittedDate != null && record.data.submittedDate != ""){
    					var count = record.data.submittedDate.split(',').length;
    					metadata.tdAttr = 'data-qtip="' + value.substring(value.indexOf('.')+1)+'<br>'+Ext.String.format('{0} Submission{1} made', count, count !== 1 ? 's' : '')+ '"';
    				}else
    					metadata.tdAttr = 'data-qtip="' + value.substring(value.indexOf('.')+1)+ '"';
            		return value.substring(value.indexOf('.')+1);
    			}
    		},{
		        xtype: 'gridcolumn',
		        dataIndex: 'postingTitle',
		        text: 'Posting Title',
                renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + value + '<br><b>Double click to open Detailed Job opening</b>"';
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
            		return '<a href=# style="color: #000000"> ' + value + '</a>';
                }
		    },{
                xtype: 'gridcolumn',
                dataIndex: 'module',
                text: 'Module',
                width :  60,
                renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + value + '"';
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
            		return value;
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'shortlistedCandidates',
                text: 'Short',
                width :  40,
                align : 'center',
                renderer:function(value, metadata, record){
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
                	if (value != null && value != ''){
                		var tasks = value.split('####');
                		var tooltip ="<table cellpadding='5' bgcolor='gray'>" +
                				"<tr bgcolor='#ffffff'>" +
                				"<th>S.No</th><th>Candidate</th><th>Expected Rate</th><th>Job help</th><th>Home Loc</th><th>Source</th></tr>";
                		for ( var i = 0; i < tasks.length; i++) {
                			var values = tasks[i].split('::');
							tooltip += "<tr bgcolor='#ffffff'>"+
										"<td>"+(i+1)+"</td><td>"+values[0]+"</td><td>"+values[1]+"</td><td>"+values[2]+"</td><td>"+values[3]+"</td><td>"+values[4]+"</td></tr>";
						}
                		tooltip += '</table>';
       					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
       					return '<span style="color:blue;">' + tasks.length + '</span>';
                	}
            		return 0;
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'pendingTasks',
                text: 'P. Tasks',
                width :  40,
                align : 'center',
                renderer:function(value, metadata, record){
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
                	if (value != null && value != ''){
                		var tasks = value.split('####');
                		var tooltip ='';
                		for ( var i = 0; i < tasks.length; i++) {
							tooltip += (i+1) +'. '+ tasks[i] +'<br>';
						}
       					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
       					return '<span style="color:red;">' + tasks.length + '</span>';
                	}
            		return 0;
                }
            },{
				header : 'Client',
				dataIndex : 'clientId',
	            width :  120,
	            renderer:function(value, metadata, record){
                	var rec = ds.clientSearchStore.getById(value);
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
                	if(rec){
                		metadata.tdAttr = 'data-qtip="'+rec.data.name+'<br><b>Open Snapshot</b>"';
                		return '<a href=# style="color: #000000"> ' + rec.data.name + '</a>';
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
				header : 'Vendor',
				dataIndex : 'vendorId',
	            width :  120,
	            renderer:function(value, metadata, record){
					metadata.style = 'background-color:#'+getBackGround(record)+';'
	            	if (value != null && value != '') {
						var ids = value.toString().split(',');
						var names ="";
						for ( var i = 0; i < ids.length; i++) {
							var rec = ds.vendorStore.getById(parseInt(ids[i]));
							if (rec)
								names += rec.data.name +", ";	
						}
                		metadata.tdAttr = 'data-qtip="'+names.substring(0,names.length-2)+'<br><b>Open Snapshot</b>"';
                		if (ids.length > 1)
                			return '<a href=# style="color: #000000"> ' + '('+ids.length+') '+ names.substring(0,names.length-2) + '</a>';
                		return '<a href=# style="color: #000000"> ' + names.substring(0,names.length-2) + '</a>';
					}
	            	return 'N/A';
                }
			},{
				text : "Alert",
				dataIndex : 'alert',
				width :  80,
                renderer: alertRender,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'rateSubmitted',
                text: 'Max Bill Rate',
                renderer: renderValue,
            },{
				text : 'Location',
				dataIndex : 'location',
				width :  80,
				renderer: renderValue,
			},{
		        xtype: 'gridcolumn',
		        dataIndex: 'cityAndState',
		        text: 'City & State',
		        renderer: renderValue,
		    },{
				text : 'Roles and Responsibilities',
				dataIndex : 'requirement',
				width :  150,
	            renderer: requirementRenderer
			},{
                xtype: 'gridcolumn',
                dataIndex: 'skillSet',
                text: 'Skill Set',
                width:250,
                renderer: reqSkillsetRender,
            },{
				header : 'Candidate',
				dataIndex : 'candidateId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != null && value != '') {
                		var ids = value.toString().split(',');
    					var names ="",tooltip='';
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contractorsStore.getById(parseInt(ids[i]));
    						if (rec){
    							names += rec.data.fullName+", ";	
    							tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
    						}
    					}
    					metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
    					return '('+ids.length+') '+ names.substring(0,names.length-2);
                	}
                	return 'N/A';
                }
			},{
				xtype: 'gridcolumn',
				text : 'Submitted Date',
				dataIndex : 'submittedDate',
				width :  60,
                renderer:function(value, metadata, record){
                	if (value != null && value != "") {
                		var req_submittedDate = value.split(',');
                		var dates = new Array();
                		for ( var i = 0; i < req_submittedDate.length; i++) {
                			var id_dates = req_submittedDate[i].split('-');
       						var rec = ds.contractorsStore.getById(parseInt(id_dates[0]));
       						if (rec){
       							var letters = rec.data.fullName.match(/\b(\w)/g);
       							var joinName = letters.join('');
       							dates.push(id_dates[1]+'-'+joinName);
       						}
    					}
                		var tooltip = dates.toString();
                		tooltip = tooltip.replace(/,/g, "<br>")
    					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
    					return dates.toString();
					}
                	return value;
                }
			},{
    			text : 'Resume',
    			dataIndex : 'resume',
    			width :  60,
                renderer:function(value, metadata, record){
                	if (value != null) {
    					metadata.tdAttr = 'data-qtip="' + value + '%"';
    					return value+'%';
					}
                	return 'N/A';
                }
    		},{
				text : 'Employer',
				dataIndex : 'employer',
				width :  40,
				renderer: employerRenderer,
			},{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                renderer: renderValue,
                width:150,
            },{
				text : 'Communication Required',
				dataIndex : 'communication',
				width :  80,
				renderer: renderValue,
			},{
  				text : 'Public Link',
  				dataIndex : 'publicLink',
  				renderer: linkRenderer,
  				width :  80,
  			},{
				text : 'Source',
				dataIndex : 'source',
				renderer: renderValue,
			},{
				header : 'Contact Person',
				dataIndex : 'contactId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != null) {
    					var ids = value.toString().split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.fullName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return names.substring(0,names.length-2);
                	}
                }
			},{
				text : 'Last Updated User',
				dataIndex : 'lastUpdatedUser',
				hidden :true,
				width:80,
				renderer:function(value, metadata, record){
               		metadata.tdAttr = 'data-qtip="' + value + '"';
                	return value;
                }
			},{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:100,
                hidden :true,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:100,
                hidden :true,
            }
        ];
        
        me.viewConfig = {
        		stripeRows: false,
        		style: {overflow:'auto', overflowX: 'hidden'},
        		
            	getRowClass: function (record, rowIndex, rowParams, store) {
					return 'row-font12';
            	},
                listeners: {
                	celldblclick: function (view, cell, cellIndex, record, row, rowIndex, e) {
                    	var colHeading = view.panel.headerCt.getHeaderAtIndex(cellIndex).text;
                    	if(colHeading == "Client"){
                    		var clientId = record.data.clientId;
                    		if (clientId != null && clientId != '')
                    			me.manager.showClient_Vendor(clientId, 'Client');
                    	}else if (colHeading == "Vendor") {
                    		var vendorId = record.data.vendorId;
                    		if (vendorId != null && vendorId != '')
                    			me.manager.showClient_Vendor(vendorId.split(',')[0], 'Vendor');
						}else if (colHeading == "Posting Title") {
							me.shareRequirement(record);
						}
                    },
                }

        };

		me.hotJobsCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Hotness',
		    store : ds.hotnessStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'hotJobs',
		    value : ['00','01'],
		    tabIndex:1,
		    labelWidth :50,
		    width :220,
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
		});

		me.jobOpeningStatusCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Status',
		    store: ds.jobOpeningStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'jobOpeningStatus',
		    tabIndex:2,
		    labelWidth :40,
		    width :140,
		    value : '01.In-progress',
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			},//listeners
		    listConfig: {
		        cls: 'grouped-list'
		      },
		      tpl: Ext.create('Ext.XTemplate',
		        '{[this.currentKey = null]}' +
		        '<tpl for=".">',
		          '<tpl if="this.shouldShowHeader(notes)">' +
		            '<div class="group-header">{[this.showHeader(values.notes)]}</div>' +
		          '</tpl>' +
		          '<div class="x-boundlist-item">{name}</div>',
		        '</tpl>',
		        {
		          shouldShowHeader: function(name){
		            return this.currentKey != name;
		          },
		          showHeader: function(name){
		            this.currentKey = name;
		            switch (name) {
		              case 'Active': return 'Active';
		              case 'Inactive': return 'Inactive';
		            }
		            return 'Other';
		          }
		        }
		      )
		});

        me.postingCombo = Ext.create('Ext.form.ComboBox', {
            fieldLabel: 'Posting Date',
            store: ds.monthStore,
            name : 'payroll',
            queryMode: 'local',
            displayField: 'label',
            valueField: 'value',
            labelAlign: 'right',
            tabIndex :3,
            labelWidth :80,
            width :180,
            value : 'One Week',
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
        });

        me.dockedItems = [
            {
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [me.hotJobsCombo,me.jobOpeningStatusCombo,me.postingCombo,
            	        '-',{
            		text : 'Search',
            		iconCls : 'btn-Search',
            		tabIndex:4,
            		handler: function(){
            			me.search();
            		}
            	},'-',{
            		text : 'Reset',
            		tabIndex:5,
            		handler: function(){
            			me.hotJobsCombo.reset();
            			me.jobOpeningStatusCombo.reset();
            			me.postingCombo.reset();
            			me.search();
            		}
            	},'-',{
        			text : '',
         			iconCls : 'icon_info',
         			tooltip :'Default : Displaying Job openings with hotness 0.Highest Priority, 1.Strong Req and Job opening status 01.In-progress and posted in last week',
         			scope: this,
         			tabIndex:5,
         			handler: function(){
         		        var legendWin = Ext.create("Ext.window.Window", {
         					title: 'Legend',
         					modal: true,
         					height      : 400,
         					width       : 550,
         					bodyStyle   : 'padding: 1px;background:#ffffff;',
         					html: '<table cellspacing="10" cellpadding="5" noOfCols="2" width="550"> '
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFCBC;"></td>'
         						+ '<td width="50%">Reqs which are opened today</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FAFAD0;"></td>'
         						+ '<td width="50%">Reqs which are opened since yesterday</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFFE0;"></td>'
         						+ '<td width="50%">Reqs which are opened since a week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFFEE;"></td>'
         						+ '<td width="50%">Reqs that are opened for more than one week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#C3D5F6;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made today</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#CFDCF5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made yesterday</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#D9E3F5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made with in one week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#E5EBF5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made more than one week back</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#A7FDA1;"></td>'
         						+ '<td width="50%">Reqs for which  placement was made</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFD3A7;"></td>'
         						+ '<td width="50%">Reqs which reached atleast interview stage</td>'
         						+ '</tr>'
         						+ '</table>'
         				});
         				legendWin.show();
         		    }
            	},'-',{
        			text : 'Assign',
        			hidden : true,
         			icon : 'images/icon_lookup.gif',
         			scope: this,
         			tabIndex:5,
         			handler: function(){
         				me.addWorkingList();
         		    }
            	}]
          }
      ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        ds.candidateSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.clientSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.contactSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        ds.vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        ds.hotnessStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        me.callParent(arguments);
        
        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);

    },
    
    search : function() {
    	var params = new Array();
    	if (this.hotJobsCombo.getValue() != null){
        	var hotness = this.hotJobsCombo.getValue().toString();
        	if (hotness != '')
        		hotness = "'"+hotness.replace(/,/g, "','")+"'";
        	params.push(['hotness','=', hotness]);
    	}
    	if (this.jobOpeningStatusCombo.getValue() != null) {
        	var openings = this.jobOpeningStatusCombo.getValue().toString();
        	if (openings != '')
        		openings = "'"+openings.replace(/,/g, "','")+"'";
        	params.push(['jobOpeningStatus','=', openings]);
		}

    	if (this.postingCombo.getValue() != null && this.postingCombo.getValue() != '') {
   		 	var date = new Date();
   		 	params.push(['postingDateTo','=',date]);
   		 	if (this.postingCombo.getValue() == 'One Week')
   		 		params.push(['postingDateFrom','=',Ext.Date.add(date, Ext.Date.DAY, -6)]);
   		 	else if (this.postingCombo.getValue() == 'Two Weeks')
   		 		params.push(['postingDateFrom','=',Ext.Date.add(date, Ext.Date.DAY, -13)]);
   		 	else if (this.postingCombo.getValue() == 'One Month') 
   		 		params.push(['postingDateFrom','=',Ext.Date.add(date, Ext.Date.MONTH, -1)]);
   		 	else if (this.postingCombo.getValue() == 'Three Months') 
   		 		params.push(['postingDateFrom','=',Ext.Date.add(date, Ext.Date.MONTH, -3)]);
    	}
    	
    	var filter = getFilter(params);
    	ds.homeRequirementStore1.loadByCriteria(filter);
	},
	
	editRequirement : function(requirementId) {
		var params = new Array();
    	params.push(['id','=', requirementId]);
    	var filter = getFilter(params);
    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onGetRequirements,this);
	},
	
	onGetRequirements : function(data) {
		if (data.success && data.rows.length > 0) {
			app.setActiveTab(0,'li_requirements');
			app.requirementMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Requirement', data.rows[0]);
			app.requirementMgmtPanel.requirementDetailPanel.loadForm(record);
			app.requirementMgmtPanel.requirementDetailPanel.openedFrom = 'Home';
		}
	},
	
	showRequirement : function(rowIndex) {
		var record = this.store.getAt(rowIndex);
		app.setActiveTab(0,'li_requirements');
		app.requirementMgmtPanel.getLayout().setActiveItem(1);
		app.requirementMgmtPanel.requirementDetailPanel.loadForm(record);
		app.requirementMgmtPanel.requirementDetailPanel.openedFrom = 'Home';
	},

	showSuggested : function(requirementId) {
		var params = new Array();
    	params.push(['id','=', requirementId]);
    	var filter = getFilter(params);
    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onShowSuggested,this);
	},

	onShowSuggested : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = Ext.create('tz.model.Requirement', data.rows[0]);
			if (app.checkWindowCount() >= 10){
				Ext.Msg.alert('Error','You have exceeded maximum no of windows(10).Please close one of them.');
				return ;
			}
			var browserHeight = app.mainViewport.height;
	    	var browserWidth = app.mainViewport.width;
	    	var potentialCandidatesPanel = new tz.ui.PotentialCandidatesPanel({itemId:'snapshot'});
	    	var subRequirementStore = new tz.store.Sub_RequirementStore();
	    	var potentialCandidatesStore = new tz.store.CandidateSearchStore();
	    	var shortlistedCandidatesStore = new tz.store.Sub_RequirementStore();

	    	potentialCandidatesPanel.submittedCandidates.bindStore(subRequirementStore);
	    	potentialCandidatesPanel.potentialCandidates.bindStore(potentialCandidatesStore);
	    	potentialCandidatesPanel.shortlistedCandidates.bindStore(shortlistedCandidatesStore);
	    	potentialCandidatesPanel.loadRecords(record);

	    	if (record.data.vendorId != null && record.data.vendorId != '') {
	            var vendorStore = Ext.create('Ext.data.Store', {
	                fields: ['id', 'name'],
	            });
	    		var vendorData= new Array();
	    		var vendorIds = record.data.vendorId.split(',');
	        	for ( var i = 0; i < vendorIds.length; i++) {
	    			
	    			var vendorName = (ds.vendorStore.getById(Number(vendorIds[i]))).data.name;
	    			if(vendorStore.getById(Number(vendorIds[i]))==null)
	    				vendorData.push({"id":Number(vendorIds[i]),"name":vendorName});
	    		}
	        	if(vendorData.length >0 ){
	        		vendorStore.add(vendorData);
	        	}        		
	    		potentialCandidatesPanel.vendorCombo.setValue('');
	    		potentialCandidatesPanel.vendorCombo.setDisabled(false);
	    		potentialCandidatesPanel.vendorCombo.bindStore(vendorStore);
			}
	    	
	    	if (app.windowXPosition+200 > browserWidth ){
	    		app.windowXPosition = 0;
	    		app.windowYPosition = -60;
	    	}
	    	var windowXPosition = app.windowXPosition;
	    	app.windowXPosition += 150;
	    	var windowYPosition = app.windowYPosition;
	    	app.windowId += 1;
			var snapshotWindow = Ext.create('Ext.window.Window', {
				title: record.data.id+' - '+record.data.postingTitle,
				height : browserHeight - 90,
				width: (browserWidth/2),
				id : 'window'+app.windowId,
				modal: false,
				minimizable: true,
				layout: 'fit',
				border : 0,
				listeners: {
					"minimize": function (window, opts) {
						window.collapse();
						window.setWidth(150);
						window.alignTo(Ext.getBody(), 'bl-bl',[windowXPosition,windowYPosition],true);
					},
					"close": function (window, opts) {
						app.arrangeWindows();
					}
				},
				tools: [{
					type: 'prev',
					handler: function (evt, toolEl, owner, tool) {
						var window = owner.up('window');
						window.alignTo(Ext.getBody(), 'tl-tl',[0,90],true);
					}
				},{
					type: 'next',
					handler: function (evt, toolEl, owner, tool) {
						var window = owner.up('window');
						window.alignTo(Ext.getBody(), 'tr-tr',[0,90],true);                
					}
				},{
					type: 'restore',
					handler: function (evt, toolEl, owner, tool) {
						var window = owner.up('window');
						window.setWidth(browserWidth/2);
						window.expand('', false);
						window.alignTo(Ext.getBody(), 'tr-tr',[0,90],true);
					}
				}],
				items: [potentialCandidatesPanel],
			});
			snapshotWindow.show();
			snapshotWindow.alignTo(Ext.getBody(), 'tr-tr',[0,90]);
		}
	},
	
	
	addWorkingList : function() {
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
    	if (selectedRecord == null) {
    		Ext.Msg.alert('Error','Please select a record.');
    		return false;
		}
    	
    	var loginId = document.getElementById('loginIdVal').innerText;
    	var user = ds.userStore.getAt(ds.userStore.findExact('loginId',loginId));
    	selectedRecord.data.userId = user.data.id;
    	selectedRecord.data.assignedUserId = user.data.id;
    	selectedRecord.data.requirementId = selectedRecord.data.id;
    	delete selectedRecord.data.id;

    	app.homeMgmtPanel.workListGrid.store.add(selectedRecord.data);
    	app.homeMgmtPanel.workListGrid.getView().refresh();
	},
	
	shareRequirement : function(record) {
		var record = record.data;
		var params = new Array();
    	params.push(['id','=', record.id]);
    	var filter = getFilter(params);
    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onShareRequirements,this);
	},
	
	onShareRequirements : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = Ext.create('tz.model.Requirement', data.rows[0]);
			record = record.data;
			app.homeMgmtPanel.requirementSharePanel.data = record;
			if (record.clientId != null && record.clientId != '') {
				var client = ds.clientSearchStore.getById(Number(record.clientId)).data.name;
			}else
				var client = '';
			
			var ids = record.vendorId.split(',').map(Number);
			var names ="";
			for ( var i = 0; i < ids.length; i++) {
				var rec = ds.vendorStore.getById(parseInt(ids[i]));
				if (rec)
					names += rec.data.name +", ";	
			}
			var vendor = names.substring(0,names.length-2);
			
			var detailsTable = "<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>"+
					"<tr><th>Posting Date </th><td>"+Ext.Date.format(new Date(record.postingDate),'m/d/Y')+'</td></tr>'+
					"<tr><th>Posting Title</th><td>" +record.postingTitle+'</td></tr>'+
					"<tr><th>City & State</th><td>" +record.cityAndState+'</td></tr>'+
					"<tr><th>Client</th><td>" +client+'</td></tr>'+
					"<tr><th>Vendor</th><td>" +vendor+'</td></tr>'+
					"<tr><th>Alert</th><td>" +record.addInfo_Todos+'</td></tr>'+
					"<tr><th>Source</th><td>" +record.source+'</td></tr>'+
					"<tr><th>Module</th><td>" +record.module+'</td></tr>'+
					"<tr><th>Skill Set</th><td>" +record.skillSet+'</td></tr>'+
					"<tr><th>Role Set</th><td>" +record.targetRoles+'</td></tr>'+
					"<tr><th>Mandatory Certifications</th><td>" +record.certifications +'</td></tr>';
			
			if(record.remote)
				detailsTable += "<tr><th>Remote</th><td>Yes</td></tr>";
			else
				detailsTable += "<tr><th>Remote</th><td>No</td></tr>" ;

			if(record.relocate)
				detailsTable += "<tr><th>Relocation Needed</th><td>Yes</td></tr>" ;
			else
				detailsTable += "<tr><th>Relocation Needed</th><td>No</td></tr>" ;

			if(record.travel)
				detailsTable += "<tr><th>Travel Needed</th><td>Yes</td></tr>" ;
			else
				detailsTable += "<tr><th>Travel Needed</th><td>No</td></tr>" ;

			detailsTable += "<tr><th>Ok to train Candidate</th><td>" + record.helpCandidate +'</td></tr>'+
							"<tr><th>Roles and Responsibilities</th><td><br>" +record.requirement.replace(/(?:\r\n|\r|\n)/g, '<br>')+'</td></tr>'+
							"<tr><th>Last updated by</th><td><br>" +record.lastUpdatedUser+'</td></tr>'+
							"<tr><th>Last updated on</th><td><br>" +Ext.Date.format(new Date(record.lastUpdated),'m/d/Y')+'</td></tr>'+
							'</table>';

			app.homeMgmtPanel.showSharePanel();
			app.homeMgmtPanel.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').setValue(detailsTable);
			app.homeMgmtPanel.requirementSharePanel.emailShortlisted.disable();
			app.homeMgmtPanel.requirementSharePanel.emailConfidential.enable();
			app.homeMgmtPanel.requirementSharePanel.fileName = record.id;
			app.homeMgmtPanel.requirementSharePanel.openedFrom = 'Home';

		}
	}
	
});
