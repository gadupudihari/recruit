Ext.define('tz.ui.HomeSmeGridPanel', {
    extend: 'Ext.grid.Panel',
    title: '',
    frame:true,
    anchor:'100%',
    //height:560,
    width :'100%',
    
    initComponent: function() {
        var me = this;

        me.columns = [{
        	xtype : 'actioncolumn',
        	width : 30,
        	items : [{
        		icon : 'images/icon_edit.gif',
        		tooltip : 'Edit/View Contact',
        		padding : 10,
        		scope : this,
        		handler : function(grid, rowIndex, colIndex) {
        			var record = me.store.getAt(rowIndex);
        			me.viewContact(record.data.id);
        		} 
        	}]
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'priority',
        	text: 'Priority',
        	width:50,
        	renderer: numberRender,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'contactName',
        	text: 'Contact Name',
        	width :  150,
        	renderer: contactRender,
        },{
        	header : 'Client/Vendor Name',
        	dataIndex : 'clientId',
        	width :  150,
        	renderer:function(value, metadata, record){
        		var tooltip ='';
    			if (record.data.clientType != null && record.data.clientType != '')
    				tooltip = record.data.clientType +' : '
        		var rec = ds.client_vendorStore.getById(value);
        		if(rec){
        			tooltip += rec.data.name;
        			metadata.tdAttr = 'data-qtip="'+tooltip+'"';
        			return rec.data.name ;
        		}else{
        			metadata.tdAttr = 'data-qtip="' + 'n/a' + '"';
        			return 'n/a';
        		}
        	}
        },{
            xtype: 'gridcolumn',
            dataIndex: 'skillSetGroupIds',
            text: 'Skill Set Group',
            width:150,
            renderer:function(value, metadata, record){
            	var tooltip = "";
            	var skillsets = new Array();
        		if (record.data.skillSetGroupIds != null  && record.data.skillSetGroupIds != '') {
					var skillSetGroupIds = record.data.skillSetGroupIds.split(',');
					for ( var i = 0; i < skillSetGroupIds.length; i++) {
						tooltip += ds.skillsetGroupStore.getById(Number(skillSetGroupIds[i])).data.name +"<br>";
						skillsets.push(ds.skillsetGroupStore.getById(Number(skillSetGroupIds[i])).data.name);
					}
					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
				}else
					return 'N/A' ;
                return skillsets.toString() ;
            }
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'module',
        	text: 'Skill Set',
        	width:150,
        	renderer:function(value, metadata, record){
        		metadata.tdAttr = 'data-qtip="' + value.replace(/,/g, '<br>') + '"';
        		return value;
        	}
        },{
            xtype: 'gridcolumn',
            dataIndex: 'roleSetIds',
            text: 'Role Set',
            width:150,
            renderer:function(value, metadata, record){
            	var tooltip = "";
        		if (record.data.roleSetIds != null  && record.data.roleSetIds != '') {
					var roleSet = record.data.roleSetIds.split(',');
					for ( var i = 1; i <= roleSet.length; i++) {
						tooltip += ds.targetRolesStore.getById(Number(roleSet[i-1])).data.value +"<br>";
					}
					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
				}else
					return 'N/A' ;
                return tooltip.replace(/<br>/g, ', ') ;
            }
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'SMEComments',
        	text: 'SME Comments',
        	width:200,
        	renderer: contactRender,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'email',
        	text: 'Email',
        	width:120,
        	renderer: contactRender,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'cellPhone',
        	text: 'CellPhone',
        	renderer: contactRender,
        	hidden : true,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'score',
        	text: 'Score',
        	width:70,
        	hidden : true,
        	renderer: numberRender,
        }];

		me.searchField = Ext.create('Ext.form.TextField', {
		    labelWidth: 55,
			fieldLabel: 'Search',
			name: 'search',
        	listeners: {
        		specialkey: function(f,k){  
        			if(k.getKey()==k.ENTER){
        				me.search();
              		}
              	}
        	},
		});

        me.searchButton = new Ext.Button({
    		iconCls : 'btn-Search',
            text: 'Search',
            tooltip:'Search',
			scope: this,
	        handler: function() {
	        	this.search();
	        }
        });

        me.addButton = new Ext.Button({
    		iconCls : 'btn-add',
            text: 'Add SME',
            tooltip:'Add new SME',
			scope: this,
	        handler: function() {
	        	this.addSME();
	        }
        });

        me.resetButton = new Ext.Button({
            text: 'Reset',
            tooltip:'Reset',
			scope: this,
	        handler: function() {
	        	this.searchField.reset();
	        	this.search();
	        }
        });

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
            width :200,
        });

        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [ me.searchField,'-',me.searchButton,'-',me.resetButton,'-',me.addButton,'->',me.showCount]
        }];

        me.viewConfig = {
        		stripeRows: false,
        		style: {overflow:'auto', overflowX: 'hidden'},
        };

        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			this.showCount.setValue(msg);
			store.sort('priority','ASC');
       	}, me);

        ds.client_vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        me.callParent(arguments);

        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);

    },
    
    search : function() {
		var params = new Array();
    	params.push(['contactType','=', 'SME']);
    	params.push(['search','=', this.searchField.getValue()]);
    	var module = this.manager
    	var filter = getFilter(params);
		this.store.loadByCriteria(filter);
	},
	
	viewContact : function(contactId) {
    	var params = new Array();
    	params.push(['id','=', contactId]);
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	app.contactService.getContacts(Ext.JSON.encode(filter),this.onGetContact,this);
	},
	
	onGetContact : function(data) {
		if (data.success && data.rows.length > 0) {
	    	app.setActiveTab(0,'li_contacts');
	    	app.contactMgmtPanel.getLayout().setActiveItem(1);
	    	app.contactMgmtPanel.contactDetailPanel.opendFrom='Home';
			var contact = Ext.create('tz.model.Contact', data.rows[0]);
			app.contactMgmtPanel.contactDetailPanel.loadForm(contact);
		}
	},
    
	addSME : function() {
   	 	var contact = Ext.create( 'tz.model.Contact',{
   	 		type : 'SME'
   	 	});
		app.setActiveTab(0,'li_contacts');
		app.contactMgmtPanel.getLayout().setActiveItem(1);
		app.contactMgmtPanel.contactDetailPanel.loadForm(contact);
		app.contactMgmtPanel.contactDetailPanel.opendFrom = 'Home';
	}
	
});
