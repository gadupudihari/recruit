Ext.define('tz.ui.MessagesGrid', {
    extend: 'Ext.grid.Panel',
    title: '',
    frame:true,
    width :'100%',
	features: [
               Ext.create('feature.groupingsummarytotal', {
                   id: 'groupSummary',
                   groupHeaderTpl: 
                	   "{name} <button id=\"sweepButtonPop_{name}\" type='button' style='height:22px;color: #3764a0;background: white;' onclick='app.homeMgmtPanel.messagesGrid.sweepConfirm(\"{name}\")'>"+
                   					"<img style='height: 22px;' align='top' src='images/icon_sweep_gray.png'>Sweep</button>",
                   onGroupClick: function() {}
               })        
           ],
           
    initComponent: function() {
        var me = this;
        me.store = ds.messagesPopupStore;
        
        me.columns = [{
        	xtype: 'actioncolumn',
        	width : 25,
        	items: [{
        		handler: function (grid, rowIndex, colIndex) {
        			var record = me.store.getAt(rowIndex);
        			me.completeMessage(record.data.id);
        		}
        	}],
        	renderer : function(value, metadata, record) {
        		if (record.data.completedDate == null) {
        			me.columns[0].items[0].icon = 'images/acceptblack.png';
        			me.columns[0].items[0].tooltip = 'Click to Mark it as Completed';
        		}else{
        			me.columns[0].items[0].icon = '';
        			me.columns[0].items[0].tooltip = '';
        		}
        	},
        },{
        	xtype: 'actioncolumn',
        	width : 25,
        	items: [{
        		handler: function (grid, rowIndex, colIndex) {
        			var record = me.store.getAt(rowIndex);
            		if (record.data.pinned )
            			me.unPinMessage(record.data.id);
            		else
            			me.pinMessage(record.data.id);
        		}
        	}],
        	renderer : function(value, metadata, record) {
        		if (record.data.pinned == null || record.data.pinned == false) {
        			me.columns[1].items[0].icon = 'images/icon-pinned_gray.png';
        			me.columns[1].items[0].tooltip = 'Pin to the Inbox';
        		}else {
        			me.columns[1].items[0].icon = 'images/icon-pinned_clr.png';
        			me.columns[1].items[0].tooltip = 'Unpin';
        		}
        	},
        },{
        	text: "Priority", dataIndex: 'priority', align : 'center', sortable : false, width : 40 ,
        	renderer:function(value, metadata, record){
        		if (value == 1)
        			return '<span style="color:red;">' + value + '</span>';
        		else if (value == 2)
        			return '<span style="color:blue;">' + value + '</span>';
        		else if (value == 3)
        			return '<span style="color:black;">' + value + '</span>';
        	}
        },{
        	xtype: 'datecolumn',text: "Alert Date", dataIndex: 'alertDate',format:'m/d/Y', sortable : false, width : 70
        },{
        	text: "Subject", flex: 1, dataIndex: 'subject',
        	sortable : false,
        	renderer:function(value, metadata, record){
        		metadata.tdAttr = 'data-qtip="' + 'Alert Date : ' + Ext.Date.format(record.data.alertDate,'m/d/Y') +'<br>'+ record.data.message + '"';
        		return  '<a href=#><b>'+value+'</b></a>' ;
        	}
        }];
        
        me.viewConfig = {
    		stripeRows: true,
        };

        me.dockedItems = [{
	    	xtype: 'toolbar',
	    	dock: 'top',
	    	items: [{
	    		xtype:'button',
	    		iconCls : 'btn-email',
                text: 'Email',
                tooltip:'Email selected record',
	    		handler: function(){
	    			me.showEmail();
	    		}
	    	},'-',{
	    		xtype:'button',
	    		icon : 'images/icon-pinned_clr.png',
                text: 'Pinned',
                tooltip:'Show pinned messages only',
	    		handler: function(){
	    			if (this.text == 'Pinned') {
	    				this.setIconCls('btn-pinned30'),
	    				this.setWidth(80);
	    				this.setText('Show All');
	    				this.setTooltip('Showing pinned messages');
	    				me.filterPinned(true);
					}else{
						this.setIconCls('btn-pinned'),
						this.setWidth(60);
						this.setText('Pinned');
						this.setTooltip('Showing all messages');
						me.filterPinned(false);
					}
	    		}
	    	},'-',{
	    		xtype:'button',
	    		iconCls : 'btn-go',
                text: 'Open Selected Record',
                width : 150,
                tooltip:'',
	    		handler: function(){
	    			me.showCandidate();
	    		}
	    	}]
	    }];
        
        me.callParent(arguments);
        
    },
    
    plugins: [{
        ptype: 'rowexpander',
        id:'rowExpander1',
        padding : '0 0 0 2500',
        rowBodyTpl : new Ext.XTemplate(
            '<div style="margin-left:160px"><p style="font-size: 13px;line-height: 1.5;">{message}</p></div>',
        {
        })
    }],
    
	completeMessage : function(id) {
		this.store.remove(this.store.getById(id));
		this.store.sort();
		this.getView().refresh();
    	app.reportsService.completeMessage(id);
	},
	
	onCompleteMessage : function(data) {
		if (data.success) {
			this.store.loadByCriteria();
		}
	},
	
	pinMessage : function(id) {
		this.store.getById(id).data.pinned = true;
		this.getView().refresh();
		app.reportsService.pinMessage(id);
	},
	
	unPinMessage : function(id) {
		this.store.getById(id).data.pinned = false;
		this.getView().refresh();
		app.reportsService.unPinMessage(id);
	},
	
	sweepConfirm : function(group) {
		this.group = group;
		Ext.Msg.confirm("Sweep All", "Do you want to clear all unpinned items in this group ?", this.sweepAllEmail, this);
	},
	
	sweepAllEmail : function(dat) {
		if (dat=='yes') {
			app.reportsService.sweepAllEmail(this.group,this.onSweepAll,this);
		}
	},
	
	onSweepAll : function(data) {
		if (data.success) {
	    	var params = new Array();
	    	var filter = getFilter(params);
	    	this.store.loadByCriteria(filter);
		}
	},
	
	showEmail : function() {
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a record.');
		} else{
			if (Ext.getCmp('emailWindow') == null) {
				var emailWindow = Ext.create('Ext.window.Window', {
				    title: 'Email',
				    id : 'emailWindow',
				    constrain: true,
				    waitMsgTarget: true,
				    loadMask: true,
				    width: 600,
				    modal: true,
				    bodyPadding: 10,
				    layout: 'fit',
				    items: [{
				        xtype: 'form',       
				            items: [{
			                    xtype: 'fieldset',
			                    layout: 'anchor',
			                    //height: 200,
			                    items: [{
			                        xtype: 'container',
			                        layout: 'anchor',                        
			                        defaults: {anchor: '100%'},
			                        items: [{
			                            xtype: 'textfield',
			                            name: 'emailId',
			                            labelAlign: 'top',
			                            fieldLabel: 'To:',
			                            emptyText: 'Type in email Id ex: abc@gmail.com,def@gmail.com',
			                            allowBlank: false
			                        },{
			                            xtype: 'textfield',
			                            name: 'subject',
			                            labelAlign: 'top',
			                            fieldLabel: 'Subject:',
			                            emptyText: 'Enter email Subject',
			                            allowBlank: false,
			                            value : selectedRecord.data.subject
			                        },{
			                        	xtype: 'htmleditor',
			                            height:180,
			                            name: 'message',  
			                            labelAlign: 'top',
			                            fieldLabel: 'Body',
			                            emptyText: 'Enter email Body',
			                            allowBlank: false,
			                            value : selectedRecord.data.message +'<br><br>'+'Regards'+'<br>'+'Team'
			                        }]
			                    }]
			                }]
				        
				    }],
				    buttons: [{
				    	text: 'Send',
			            handler: function(){
			            	var params = new Array();
			            	for ( var i = 0; i < 3; i++ ) {
			            		var component = Ext.getCmp('emailWindow').items.items[0].items.items[0].items.items[0].items.items[i];
			            		if(! component.isValid()){
			            			Ext.Msg.alert('Error','Please fix the errors.');
			            			return false;
			            		}else{
			            			params.push([component.name,'=', component.value]);
			            		}
							}
			            	var filter = getFilter(params);
			            	app.homeMgmtPanel.homeDetailPanel.messagesGrid.sendEmail(filter);
			            }
			        }]
				});
			}
			Ext.getCmp('emailWindow').show();
			
		}
	},
	
	sendEmail : function(filter) {
		app.reportsService.sendHomeEmail(Ext.encode(filter),this.onSendEmail,this);
	},
	
	onSendEmail : function(data) {
		if (data.success) {
			Ext.getCmp('emailWindow').close();
			Ext.Msg.alert('Success','Email sent successfully.');
		}
	},

	filterPinned : function(pinned) {
		if (pinned) {
			this.store.filterBy(function(rec) {
			    return rec.get('pinned') === true;
			});
			
			var groups = this.store.collect('group');
			for ( var i = 0; i < groups.length; i++) {
				document.getElementById('sweepButtonPop_'+groups[i]).style.visibility = 'hidden';	
			}
		}else{
			this.store.clearFilter();
			var groups = this.store.collect('group');
			for ( var i = 0; i < groups.length; i++) {
				document.getElementById('sweepButtonPop_'+groups[i]).style.visibility = 'visible';	
			}
		}
	},

	showWindow : function() {
		try {
			ds.messagesPopupStore.add(ds.messagesStore.data.items);	
		} catch (e) {
			ds.messagesPopupStore.add(ds.messagesStore.data.items);
		}
		
		try {
			var messagesGrid = new tz.ui.MessagesGrid({manager:app.homeMgmtPanel,store:ds.messagesPopupStore});
		} catch (e) {
			var messagesGrid = new tz.ui.MessagesGrid({manager:app.homeMgmtPanel,store:ds.messagesPopupStore});
		}

        var myWin = Ext.create("Ext.window.Window", {
        	title : 'Messages',
        	modal : false,
        	height : 600,
        	width : 1000,
        	bodyStyle : 'padding: 1px',
        	layout : 'form',
        	autoScroll:true,
        	items : [{
        		xtype:'fieldset',
        		layout: 'column',
        		autoScroll:true,
        		border:false,
        		items:[{
        			xtype:'container',
        			columnWidth:.99,
        			items :[messagesGrid]
        		}]
        	}]
        });
		    
		myWin.show();
	},
	
	showCandidate : function() {
		var record = this.getView().getSelectionModel().getSelection()[0];
		if (record == undefined) {
			Ext.Msg.alert('Error','Please select a record.');
		} else{
        	if (record.data.alertName == 'Job Submission' ) {
        		var candidateId = record.data.refId;
    	    	var params = new Array();
    	    	params.push(['id','=', candidateId]);
    	    	var filter = getFilter(params);
    	    	filter.pageSize=50;
    	    	app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidates,this);
			}
		}
	},
	
	onGetCandidates : function(data) {
		if (data.rows.length > 0) {
			app.setActiveTab(0,'li_candidates');
			app.candidateMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			app.candidateMgmtPanel.candidatePanel.loadForm(record);
			app.candidateMgmtPanel.candidatePanel.openedFrom = 'Home';
		}
	}

});

function html2text(html) {
    var tag = document.createElement('div');
    tag.innerHTML = html;
    
    return tag.innerText;
}
