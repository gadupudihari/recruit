Ext.define('tz.ui.HomeDetailPanel', {
    extend: 'tz.ui.BaseFormPanel',
    padding : '10 10 0 10',
    border:0,
    title: '',
    anchor:'100% 100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},

    initComponent: function() {
        var me = this;

        me.requirementsGrid1 = new tz.ui.HomeRequirementsGrid1({manager:me});
        me.requirementsGrid2 = new tz.ui.HomeRequirementsGrid2({manager:me});
        me.candidatesGrid = new tz.ui.HomeCandidatesGrid1({manager:me});
        me.contactGridPanel = new tz.ui.HomeContactGridPanel({manager:me});
        var contactStore = new tz.store.ContactStore();
        
        me.smeGridPanel = new tz.ui.HomeSmeGridPanel({manager:me,store : contactStore});

        me.candidatesSearchPanel = new tz.ui.HomeCandidatesSearchPanel({manager:me});
        me.candidatesGrid2 = new tz.ui.HomeCandidatesGrid2({manager:me});

    	this.messageTabsStore = Ext.create('Ext.data.Store', {
    	    fields: ['name'],
    	    data : [
	    	        {"name":"Inbox"},{"name":"Pinned"},{"name":"Completed"},{"name":"Deleted"},
	    	    ]
    	});

        me.messageTabsGrid = Ext.create('Ext.grid.Panel', {
        	title : '',
        	width: 150,
        	xtype: 'panel',
        	bodyStyle: {
        	    background: '#eaeaea',
        	},
        	store : me.messageTabsStore,
        	columns: [{ dataIndex: 'name', flex:1,
        				renderer:function(value, metadata, record){
        					return '<a href=#><b>'+value+'</b></a>'; 
        				}
        	}],
            listeners : {
                cellclick: function( grid, td, cellIndex, record, tr, rowIndex, e, eOpts ){
                	app.homeMgmtPanel.homeDetailPanel.searchMessages();
                }
            }
        }), 

        me.pinnedButton = new Ext.Button({
    		iconCls : 'btn-pinned',
            text: 'Pinned',
            tooltip:'Showing all messages',
    		handler: function(){
    			if (this.text == 'Pinned') {
    				this.setIconCls('btn-pinned30'),
    				this.setWidth(80);
    				this.setText('Show All');
    				this.setTooltip('Showing pinned messages');
    				me.filterPinned(true);
				}else{
					this.setIconCls('btn-pinned'),
					this.setWidth(60);
					this.setText('Pinned');
					this.setTooltip('Showing all messages');
					me.filterPinned(false);
				}
    		}
    	});

        me.messagesGrid = Ext.create('Ext.grid.Panel', {
            frame:false,
            width :'100%',
        	store : ds.messagesStore,
        	features: [
                       Ext.create('feature.groupingsummarytotal', {
                           id: 'groupSummary',
                           groupHeaderTpl: 
                        	   "{name} <button id=\"sweepButton_{name}\" type='button' style='height:22px;color: #3764a0;background: white;' onclick='app.homeMgmtPanel.homeDetailPanel.sweepConfirm(\"{name}\")'>"+
                           					"<img style='height: 22px;' align='top' src='images/icon_sweep_gray.png'>Sweep</button>",
                           onGroupClick: function() {}
                       })        
                   ],       
            columns: [{
            	xtype: 'actioncolumn',
            	width : 25,
            	items: [{
            		handler: function (grid, rowIndex, colIndex) {
            			var record = me.messagesGrid.store.getAt(rowIndex);
            			if (record.data.completedDate == null ) {
            				me.completeMessage(record.data.id);	
            			}else if (record.data.completedDate != null && record.data.deletedDate == null) {
							me.activeMessage(record.data.id);
						}
            		}
            	}],
            	renderer : function(value, metadata, record) {
            		if (record.data.completedDate == null && record.data.deletedDate == null) {
            			me.messagesGrid.columns[0].items[0].icon = 'images/acceptblack.png';
            			me.messagesGrid.columns[0].items[0].tooltip = 'Click to Mark it as Completed';
            		}else if (record.data.completedDate != null && record.data.deletedDate == null) {
            			me.messagesGrid.columns[0].items[0].icon = 'images/accept.png';
            			me.messagesGrid.columns[0].items[0].tooltip = 'Click to Mark it as Active';
					}else{
            			me.messagesGrid.columns[0].items[0].icon = '';
            			me.messagesGrid.columns[0].items[0].tooltip = '';
            		}
            	},
            },{
            	xtype: 'actioncolumn',
            	width : 25,
            	items: [{
            		handler: function (grid, rowIndex, colIndex) {
            			var record = me.messagesGrid.store.getAt(rowIndex);
                		if (record.data.pinned )
                			me.unPinMessage(record.data.id);
                		else
                			me.pinMessage(record.data.id);
            		}
            	}],
            	renderer : function(value, metadata, record) {
            		if (record.data.pinned == null || record.data.pinned == false) {
            			me.messagesGrid.columns[1].items[0].icon = 'images/icon-pinned_gray.png';
            			me.messagesGrid.columns[1].items[0].tooltip = 'Pin to the Inbox';
            		}else {
            			me.messagesGrid.columns[1].items[0].icon = 'images/icon-pinned_clr.png';
            			me.messagesGrid.columns[1].items[0].tooltip = 'Unpin';
            		}
            	},
            },{
            	text: "Priority", dataIndex: 'priority', align : 'center', sortable : false, width : 40 ,
            	renderer:function(value, metadata, record){
            		if (value == 1)
            			return '<span style="color:red;">' + value + '</span>';
            		else if (value == 2)
            			return '<span style="color:blue;">' + value + '</span>';
            		else if (value == 3)
            			return '<span style="color:black;">' + value + '</span>';
            	}
            },{
            	xtype: 'datecolumn',text: "Alert Date", dataIndex: 'alertDate',format:'m/d/Y', sortable : false, width : 70
            },{
            	text: "Subject", flex: 1, dataIndex: 'subject',
            	sortable : false,
            	renderer:function(value, metadata, record){
            		metadata.tdAttr = 'data-qtip="' + 'Alert Date : ' + Ext.Date.format(record.data.alertDate,'m/d/Y') +'<br>'+ record.data.message + '"';
            		return  '<a href=#><b>'+value+'</b></a>' ;
            	}
            }],
    	    dockedItems : [{
		    	xtype: 'toolbar',
		    	dock: 'top',	
		    	items: [{
		    		xtype:'button',
		    		iconCls : 'btn-email',
	                text: 'Email',
	                tooltip:'Email selected record',
		    		handler: function(){
		    			me.showEmail();
		    		}
		    	},'-',{
		    		xtype:'button',
		    		icon : 'images/arrow_refresh.png',
	                text: 'Refresh',
	                tooltip:'Refresh',
		    		handler: function(){
		    			me.searchMessages();
		    		}
		    	},'-',me.pinnedButton,'-',{
		    		xtype:'button',
		    		icon : 'images/icon-full-screen.png',
	                text: 'Pop out',
	                tooltip:'Show in New window',
		    		handler: function(){
		    			me.showWindow();
		    		}
		    	}]
		    }],
    	                
            viewConfig :{
        		forceFit:true,
            },
            
            plugins: [{
                ptype: 'rowexpander',
                id:'rowExpander1',
                padding : '0 0 0 2500',
                rowBodyTpl : new Ext.XTemplate(
                    "<div style='margin-left:160px'><p style='font-size: 13px;line-height: 1.5;'>{message} <br>{hyperlink}</p></div>",
                {
                })
            }],
            animCollapse: false,
        });

        me.alertsTab = Ext.create('Ext.tab.Panel', {
        	activeTab : 1,
			plain: true,
        	border:0,
        	layout   : 'card',
        	defaults: {
	        	listeners: {
	        		//Reset monthlyexpenses grid when clicked
	        		activate: function(tab, eOpts) {
	        			me.updateActiveTab(tab);
	        		}
	        	}
        	},
			items : [{
				title : 'Messages',
				items : [{
					xtype : 'container',
					layout: {
				        type: 'hbox',
				        align: 'stretch'
				    },
					items : [me.messageTabsGrid,me.messagesGrid]
				}]
			},{
				title : 'Job Openings & Candidates',
				items : [{
					xtype : 'container',
					layout : 'column',
					items : [{
						xtype : 'container',
						columnWidth : .5,
						items : [me.requirementsGrid1,me.requirementsGrid2]
					},{
						xtype : 'container',
						columnWidth : .5,
						items : [me.candidatesGrid]
					}]
				}]
			},{
				title : "SME's",
				items : [me.smeGridPanel]
			},{
				title : 'Vendors',
				items : [me.contactGridPanel]
			},{
				title : 'Candidates',
				items : [me.candidatesSearchPanel,
				         {
   	        				height: 10,
   	        				border : false
				         },
				         this.candidatesGrid2
			        ]
			}]
		});

       me.items = [me.alertsTab];

        me.callParent(arguments);
    },
    
    expandMessage : function(recordId) {
    	this.alertsTab.setActiveTab(0);    	
		var expander = this.messagesGrid.plugins[0];				
		var rowNode = this.messagesGrid.getView().getNode(1);		
		var row = Ext.fly(rowNode, '_rowExpander');
		var isCollapsed = row.hasCls(expander.rowCollapsedCls);
		//Finding if the row is already collapsed and expanding
		//getting row index and record using id
		//if(isCollapsed)
		if(!expander.recordsExpanded[ds.messagesStore.getById(recordId).internalId])
			expander.toggleRow(this.messagesGrid.getView().getPosition(ds.messagesStore.getById(recordId)).row,ds.messagesStore.getById(recordId));
	},

    updateActiveTab : function(tab) {
    	var browserHeight = app.mainViewport.height;
		var headerHt = app.mainViewport.items.items[0].getHeight();

    	if (tab.title == 'Job Openings & Candidates') {
    		this.requirementsGrid1.hotJobsCombo.reset();
    		this.requirementsGrid2.hotJobsCombo.reset();
    		this.requirementsGrid1.search();
    		this.requirementsGrid2.search();
    		this.candidatesGrid.search();
		}else if (tab.title == 'Vendors') {
			this.contactGridPanel.search();
		}else if (tab.title == "SME's") {
			this.smeGridPanel.search();
		}else if (tab.title == "Candidates") {
			app.settingsService.getCustomColumns('',this.onGetCustomColumns,this);
			this.candidatesSearchPanel.checkForDirty();
			var searchPanelHeight = this.candidatesSearchPanel.getHeight();
			this.candidatesGrid2.setHeight(browserHeight - searchPanelHeight - headerHt -100);
		}
	},
    
	onGetCustomColumns : function(data) {
		if (data.success && data.returnVal.rows.length > 0) {
			var columnNames = data.returnVal.rows[0].columns.split(',');
			var columns = this.candidatesGrid2.columns;
			
			for ( var i = 0; i < columns.length; i++) {
				if (columnNames.indexOf(columns[i].text) != -1 ) {
					this.candidatesGrid2.columns[i].show();
					this.candidatesGrid2.columns[i].hidden =false;
				}else{
					this.candidatesGrid2.columns[i].hide();
					this.candidatesGrid2.columns[i].hidden =true;
				}
			}
		}
	},
	
    showQuickAccess : function(candidateId) {
    	ds.quickAccessStore.clearFilter();
    	ds.quickAccessStore.filterBy(function(rec) {
			return rec.get('type') == 'Candidate';
		});
		var employee = ds.quickAccessStore.getAt(ds.quickAccessStore.findExact('referenceId',candidateId.toString()));
    	ds.quickAccessStore.clearFilter();
		if (employee == null) {
			return false;
		}
		app.showSnapshot(employee);
	},

	searchMessages : function() {
    	if (this.messageTabsGrid.getView().getSelectionModel().getSelection()[0] != null ) 
        	report = this.messageTabsGrid.getView().getSelectionModel().getSelection()[0].data.name;
		else{
			var view = this.messageTabsGrid.getView();
			var record = this.messageTabsGrid.store.getAt(0);
			view.select(record);
			report = 'Inbox';
		}
			
    	var params = new Array();
    	params.push(['report','=',report]);
    	var filter = getFilter(params);
    	ds.messagesStore.loadByCriteria(filter);

    	ds.messagesStore.on('load', function(store, records, options) {
    		store.clearFilter();
    		store.filterBy(function(rec) {
    			return rec.get('group') == 'Today' && rec.get('pinned') != true;
    		});
    		if (store.getCount() > 0) 
    			app.homeMgmtPanel.homeDetailPanel.alertsTab.items.items[0].setTitle('Messages <span style="color:red;">'+'('+store.getCount()+') </span>');
    		else
    			app.homeMgmtPanel.homeDetailPanel.alertsTab.items.items[0].setTitle('Messages ('+store.getCount()+')');
    		
    		store.clearFilter();

    		var groups = this.messagesGrid.store.collect('group');
        	if (report == 'Completed' || report == 'Deleted'){
        		for ( var i = 0; i < groups.length; i++) {
        			if (document.getElementById('sweepButton_'+groups[i]) != null)
        				document.getElementById('sweepButton_'+groups[i]).style.visibility = 'hidden';	
        		}
        	}else{
        		for ( var i = 0; i < groups.length; i++) {
        			if (document.getElementById('sweepButton_'+groups[i]) != null)
        				document.getElementById('sweepButton_'+groups[i]).style.visibility = 'visible';	
        		}
        	}

    		for ( var i = 0; i < store.getCount(); i++) {
    			var rec = store.getAt(i).data;
				if (rec.subject == 'Submissions turnaround time' || rec.subject == 'Daily requirement submissions summary' ) {
	        		var date = '"'+Ext.Date.format(rec.alertDate,'m-d-Y')+'"';
					rec.hyperlink = "<a onclick='javascript:app.homeMgmtPanel.homeDetailPanel.loadSubmissionReport("+date+")' href=# title='Open Report' >Open Report</a>";
				}else if (rec.subject == 'Pending research tasks') {
	        		var date = '"'+Ext.Date.format(rec.alertDate,'m-d-Y')+'"';
					rec.hyperlink = "<a onclick='javascript:app.homeMgmtPanel.homeDetailPanel.loadResearchReport("+date+")' href=# title='Open Report' >Open Report</a>";
				}else{
					rec.hyperlink = rec.employeeName + "&nbsp&nbsp<a onclick='javascript:app.homeMgmtPanel.homeDetailPanel.showCandidate("+rec.refId+")' href=# title='Edit' >" +
					"<img align='top' src='images/edit.png'></a>&nbsp&nbsp" +
					"<a onclick='javascript:app.homeMgmtPanel.homeDetailPanel.showQuickAccess("+rec.refId+")' href=# title='View' ><img align='top' src='images/Popout.ico'></a>" 
				}
			}
        	this.messagesGrid.getView().refresh();
       	}, this);
    	
		this.pinnedButton.setIconCls('btn-pinned'),
		this.pinnedButton.setWidth(60);
		this.pinnedButton.setText('Pinned');
		this.pinnedButton.setTooltip('Showing all messages');
		this.filterPinned(false);

	},
	
	activeMessage : function name(id) {
		this.messagesGrid.store.remove(this.messagesGrid.store.getById(id));
		this.messagesGrid.store.sort();
		this.messagesGrid.getView().refresh();
		app.reportsService.activeMessage(id);
	},

	completeMessage : function(id) {
		this.messagesGrid.store.remove(this.messagesGrid.store.getById(id));
		this.messagesGrid.store.sort();
		this.messagesGrid.getView().refresh();
    	app.reportsService.completeMessage(id);
	},
	
	onCompleteMessage : function(data) {
		if (data.success) {
			this.messagesGrid.store.loadByCriteria();
		}
	},
	
	pinMessage : function(id) {
		this.messagesGrid.store.getById(id).data.pinned = true;
		this.messagesGrid.getView().refresh();
		app.reportsService.pinMessage(id);
	},
	
	unPinMessage : function(id) {
		this.messagesGrid.store.getById(id).data.pinned = false;
		this.messagesGrid.getView().refresh();
		app.reportsService.unPinMessage(id);
	},
	
	sweepConfirm : function(group) {
		this.group = group;
		Ext.Msg.confirm("Sweep All", "Do you want to clear all unpinned items in this group ?", this.sweepAllEmail, this);
	},
	
	sweepAllEmail : function(dat) {
		if (dat=='yes') {
			app.reportsService.sweepAllEmail(this.group,this.onSweepAll,this);
		}
	},
	
	onSweepAll : function(data) {
		if (data.success) {
			this.searchMessages();
		}
	},
	
	showEmail : function() {
		var selectedRecord = this.messagesGrid.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a record.');
		} else{
			if (Ext.getCmp('emailWindow') == null) {
				var emailWindow = Ext.create('Ext.window.Window', {
				    title: 'Email',
				    id : 'emailWindow',
				    constrain: true,
				    waitMsgTarget: true,
				    loadMask: true,
				    width: 600,
				    modal: true,
				    bodyPadding: 10,
				    layout: 'fit',
				    items: [{
				        xtype: 'form',       
				            items: [{
			                    xtype: 'fieldset',
			                    layout: 'anchor',
			                    //height: 200,
			                    items: [{
			                        xtype: 'container',
			                        layout: 'anchor',                        
			                        defaults: {anchor: '100%'},
			                        items: [{
			                            xtype: 'textfield',
			                            name: 'emailId',
			                            labelAlign: 'top',
			                            fieldLabel: 'To:',
			                            emptyText: 'Type in email Id ex: abc@gmail.com,def@gmail.com',
			                            allowBlank: false
			                        },{
			                            xtype: 'textfield',
			                            name: 'subject',
			                            labelAlign: 'top',
			                            fieldLabel: 'Subject:',
			                            emptyText: 'Enter email Subject',
			                            allowBlank: false,
			                            value : selectedRecord.data.subject
			                        },{
			                        	xtype: 'htmleditor',
			                            height:180,
			                            name: 'message',  
			                            labelAlign: 'top',
			                            fieldLabel: 'Body',
			                            emptyText: 'Enter email Body',
			                            allowBlank: false,
			                            value : selectedRecord.data.message +'<br><br>'+'Regards'+'<br>'+'Team'
			                        }]
			                    }]
			                }]
				        
				    }],
				    buttons: [{
				    	text: 'Send',
			            handler: function(){
			            	var params = new Array();
			            	for ( var i = 0; i < 3; i++ ) {
			            		var component = Ext.getCmp('emailWindow').items.items[0].items.items[0].items.items[0].items.items[i];
			            		if(! component.isValid()){
			            			Ext.Msg.alert('Error','Please fix the errors.');
			            			return false;
			            		}else{
			            			params.push([component.name,'=', component.value]);
			            		}
							}
			            	var filter = getFilter(params);
			            	app.homeMgmtPanel.homeDetailPanel.sendEmail(filter);
			            }
			        }]
				});
			}
			Ext.getCmp('emailWindow').show();
			
		}
	},
	
	sendEmail : function(filter) {
		app.reportsService.sendEmail(Ext.encode(filter),this.onSendEmail,this);
	},
	
	onSendEmail : function(data) {
		if (data.success) {
			Ext.getCmp('emailWindow').close();
			Ext.Msg.alert('Success','Email sent successfully.');
		}
	},

	filterPinned : function(pinned) {
		if (pinned) {
			this.messagesGrid.store.filterBy(function(rec) {
			    return rec.get('pinned') === true;
			});
			var groups = this.messagesGrid.store.collect('group');
			for ( var i = 0; i < groups.length; i++) {
				if (document.getElementById('sweepButton_'+groups[i]) != null)
					document.getElementById('sweepButton_'+groups[i]).style.visibility = 'hidden';	
			}
		}else{
			this.messagesGrid.store.clearFilter();
			var groups = this.messagesGrid.store.collect('group');
			for ( var i = 0; i < groups.length; i++) {
				if (document.getElementById('sweepButton_'+groups[i]) != null)
					document.getElementById('sweepButton_'+groups[i]).style.visibility = 'visible';	
			}
		}
	},

	showWindow : function() {
		try {
			ds.messagesPopupStore.add(ds.messagesStore.data.items);	
		} catch (e) {
			ds.messagesPopupStore.add(ds.messagesStore.data.items);
		}
		try {
			var messagesGrid = new tz.ui.MessagesGrid({manager:app.homeMgmtPanel,store:ds.messagesPopupStore});
		} catch (e) {
			var messagesGrid = new tz.ui.MessagesGrid({manager:app.homeMgmtPanel,store:ds.messagesPopupStore});
		}

        var myWin = Ext.create("Ext.window.Window", {
        	title : 'Messages',
        	modal : false,
        	height : 600,
        	width : 700,
			minimizable: true,
        	autoScroll:true,
        	listeners: {
                "minimize": function (window, opts) {
                    window.collapse();
                    window.setWidth(200);
                    window.alignTo(Ext.getBody(), 'bl-bl')
                }
            },
            tools: [{
                type: 'restore',
                handler: function (evt, toolEl, owner, tool) {
                    var window = owner.up('window');
                    window.setWidth(700);
                    window.expand('', false);
                    window.center();
                }
            }],
        	items : [{
        		xtype:'fieldset',
        		layout: 'column',
        		autoScroll:true,
        		border:false,
        		items:[{
        			xtype:'container',
        			columnWidth:.99,
        			items :[messagesGrid]
        		}]
        	}]
        }).show();
		    
		Ext.create('Ext.tip.ToolTip', {
			target: myWin.header.items.get(1).el,
			html: 'Click to restore window.'
		});
		Ext.create('Ext.tip.ToolTip', {
			target: myWin.header.items.get(2).el,
			html: 'Click to minimize window.'
		});
		Ext.create('Ext.tip.ToolTip', {
			target: myWin.header.items.get(3).el,
			html: 'Click to close window.'
		});

	},
	
	showCandidate : function(candidateId) {
		var params = new Array();
		params.push(['id','=', candidateId]);
		var filter = getFilter(params);
		filter.pageSize=50;
		app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidates,this);
		ds.marketingStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Marketing Status'])));
		ds.qualificationStore.loadByCriteria(getFilter(new Array(['type','=', 'Qualification'])));
    	ds.candidateTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Type'])));
    	ds.immigrationTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Immigration'])));
    	ds.employmentTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Employment Type'])));
    	ds.certificationStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Status'])));
    	ds.certificationSponsoredStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Sponsored'])));
    	ds.candidateSourceStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Source'])));
    	ds.communicationStore.loadByCriteria(getFilter(new Array(['type','=', 'Communication'])));
    	ds.personalityStore.loadByCriteria(getFilter(new Array(['type','=', 'Personality'])));
    	ds.experienceStore.loadByCriteria(getFilter(new Array(['type','=', 'Experience'])));
    	ds.targetRolesStore.loadByCriteria(getFilter(new Array(['type','=', 'Target Roles'])));
    	ds.certificationsStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Name'])));
	},
	
	onGetCandidates : function(data) {
		if (data.rows.length > 0) {
			app.setActiveTab(0,'li_candidates');
			app.candidateMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			app.candidateMgmtPanel.candidatePanel.loadForm(record);
			app.candidateMgmtPanel.candidatePanel.openedFrom = 'Home';
		}
	},

	showClient_Vendor : function(id, type) {
		app.loadMask.show();
		ds.quickAccessStore.clearFilter();
        ds.quickAccessStore.filterBy(function(rec) {
        	return rec.get('type') === type; 
        });
        var record = ds.quickAccessStore.getAt(ds.quickAccessStore.findExact('referenceId',id.toString()));
        app.showSnapshot(record);
	},
	
	loadQuickAccessPanel : function() {
		if(app.quickAccessMgmtPanel == null){
			app.quickAccessMgmtPanel = new tz.ui.QuickAccessMgmtPanel({itemId:'quickAccessMgmtPanel'});
			app.centerLayout.add(app.quickAccessMgmtPanel);
		}    		
		app.centerLayout.getLayout().setActiveItem('quickAccessMgmtPanel');
		document.getElementById('li_home').className = ""; //"gbzt";
		document.getElementById('li_home1').style.backgroundColor = "#157fcc";
		document.getElementById('li_quickAccess').className = "activeNavigationBar";
		app.prevId = 'li_quickAccess';
	},
	
	loadSubmissionReport : function(date) {
		app.reportsService.getSubmissionReport("Submissions-"+date+".html");
	},
	
	loadResearchReport : function(date) {
		app.reportsService.getSubmissionReport("PendingTasks-"+date+".html");
	}

});
