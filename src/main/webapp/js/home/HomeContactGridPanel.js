Ext.define('tz.ui.HomeContactGridPanel', {
    extend: 'Ext.grid.Panel',
    title: '',
    frame:true,
    anchor:'100%',
    //height:560,
    width :'100%',
    
    initComponent: function() {
        var me = this;
        me.store = ds.highContactsStore;
        
        me.columns = [
            {
            	xtype : 'actioncolumn',
            	width : 30,
            	items : [{
            		icon : 'images/icon_edit.gif',
            		tooltip : 'Edit/View Contact',
            		padding : 10,
            		scope : this,
            		handler : function(grid, rowIndex, colIndex) {
            			var record = ds.highContactsStore.getAt(rowIndex);
            			me.viewContact(record.data.contactId);
            		} 
            	}]
            },{
                xtype: 'gridcolumn',
                dataIndex: 'contact',
                text: 'Contact Name',
                renderer: contactRender,
                width :  120,
            },{
                xtype: 'gridcolumn',
                text: 'Vendor',
				dataIndex : 'vendor',
				renderer: contactRender,
				width :  120,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'score',
                text: 'Score',
                width:50,
                renderer: numberRender,
            },{
            	text: "Recent Interview Date", 
            	dataIndex: 'statusUpdated',
            	format:'m/d/Y', 
            	width :  120,
            	renderer:function(value, metadata, record){
                	var tooltip ="<table cellpadding='5' bgcolor='gray'>" +
                				"<tr bgcolor='#ffffff'><th>Candidate</th><td>"+record.data.candidate+"</td></tr>" +
                				"<tr bgcolor='#ffffff'><th>Vendor</th><td>"+record.data.vendor+"</td></tr>" +
                				"<tr bgcolor='#ffffff'><th>Client</th><td>"+record.data.client+"</td></tr>" +
                				"<tr bgcolor='#ffffff'><th>Interview Date</th><td>"+Ext.Date.format(value,'m/d/Y')+"</td></tr>" +
                				"<tr bgcolor='#ffffff'><th>Submission status</th><td>"+record.data.submissionStatus+"</td></tr>" +
                				"<tr bgcolor='#ffffff'><th>Skill Set</th><td>"+record.data.module+"</td></tr>" ;
                		tooltip += '</table>';
       					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
       					return Ext.Date.format(value,'m/d/Y') ;
                }
            },{
				header : 'Job Id',
				dataIndex : 'requirementId',
	            width :  100,
                renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + value + '<br><b>Double click to edit Job opening</b>"';
            		return '<a href=# style="color: #000000"> ' + value + '</a>';
                }
			},{
                xtype: 'gridcolumn',
                text: 'Client',
				dataIndex : 'client',
				renderer: contactRender,
				width :  120,
            },{
                xtype: 'gridcolumn',
                text: 'Candidate',
				dataIndex : 'candidate',
				renderer: contactRender,
				width :  120,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'module',
                text: 'Skill Set',
                width:120,
                renderer: contactRender,
                flex : 1
            }
        ];

        me.viewConfig = {
        		stripeRows: false,
        		style: {overflow:'auto', overflowX: 'hidden'},
        		
                listeners: {
                	celldblclick: function (view, cell, cellIndex, record, row, rowIndex, e) {
                    	var colHeading = view.panel.headerCt.getHeaderAtIndex(cellIndex).text;
                    	if(colHeading == "Client"){
                    		var clientId = record.data.clientId;
                    		if (clientId != null && clientId != '')
                    			me.manager.showClient_Vendor(clientId, 'Client');
                    	}else if (colHeading == "Vendor") {
                    		var vendorId = record.data.vendorId;
                    		if (vendorId != null && vendorId != '')
                    			me.manager.showClient_Vendor(vendorId.split(',')[0], 'Vendor');
						}else if (colHeading == "Job Id") {
							app.homeMgmtPanel.homeDetailPanel.requirementsGrid1.editRequirement(record.data.requirementId);
						}
                    },
                }

        };

        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        ds.client_vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        me.callParent(arguments);

        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);

    },
    
    search : function() {
		var params = new Array();
    	var filter = getFilter(params);
		ds.highContactsStore.loadByCriteria(filter);
	},
	
	viewContact : function(contactId) {
    	var params = new Array();
    	params.push(['id','=', contactId]);
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	app.contactService.getContacts(Ext.JSON.encode(filter),this.onGetContact,this);
	},
	
	onGetContact : function(data) {
		if (data.success && data.rows.length > 0) {
	    	app.setActiveTab(0,'li_contacts');
	    	app.contactMgmtPanel.getLayout().setActiveItem(1);
	    	app.contactMgmtPanel.contactDetailPanel.opendFrom='Home';
			var contact = Ext.create('tz.model.Contact', data.rows[0]);
			app.contactMgmtPanel.contactDetailPanel.loadForm(contact);
		}
	},

    
});
