Ext.define('tz.ui.HomeCandidatesGrid1', {
    extend: 'Ext.grid.Panel',
    title: 'Candidates',
    frame:true,
    width :'100%',
	//region: 'center', 
	//collapsible: false,

    initComponent: function() {
        var me = this;
        me.store = ds.homeCandidateStore;
        me.emailFilter ;
        me.candidates = new Array();
        me.emailIds = new Array();
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :30,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'Edit Candidate',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					this.showCandidate(rowIndex);
    				}
    			}]
            },{
                xtype: 'actioncolumn',
                width :30,
                items : [{
                	icon : 'images/Popout.ico',
    				tooltip : 'View Snapshot',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					var candidateId = me.store.getAt(rowIndex).data.id;
    					this.showQuickAccess(candidateId);
    				}
    			}]
            },{
            	xtype: 'actioncolumn',
            	width :35,
            	items : [{
            		icon : 'images/icon_reschedule.gif',
            		tooltip : 'View Candidate',
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			this.shareRequirement(this.store.getAt(rowIndex));
            		}
            	}]
            },{
                xtype: 'gridcolumn',
                dataIndex: 'priority',
                text: 'Priority',
                align : 'center',
                width : 40,
                renderer:function(value, metadata, record){
                	if (value == 0 || value == 1)
                		return '<span style="color:blue;">' + value + '</span>';
                	return value;
                }
            },{
                xtype: 'gridcolumn',
                dataIndex: 'experience',
                text: 'Experience',
                align : 'center',
                width : 40,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'communication',
                text: 'Communication',
                align : 'center',
                width : 40,
                renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + value + '"';
                	if (value == 'Excellent')
                		return 9;
                	else if (value == 'Good')
                		return 8;
                	else if (value == 'Average')
                		return 5;
                	else if (value == 'Not Relavant')
                		return 0;
                }
            },
            {text: 'Candidate',dataIndex: 'fullName',
                renderer:function(value, metadata, record){
            		metadata.style = 'background-color:#'+getCandidateBackGround(record)+';'
            		tooltip = "Contact No : "+ record.data.contactNumber+"<br>City & State : "+ record.data.cityAndState+"<br>Gender : "+ record.data.gender+
            		"<br>Immigration Status : "+ record.data.immigrationStatus+"<br>Immigration Verified : "+ record.data.immigrationVerified+
            		"<br>Expected Rate : "+ record.data.expectedRate+"<br>Alert : <span style='color:red;'>" + record.data.alert + "</span>";
                	metadata.tdAttr = 'data-qtip="' + tooltip + '"';
                	return value;
                }
            },
            {text : 'Resume Help',width:80,dataIndex : 'resumeHelp',
                renderer:function(value, metadata, record){
                	metadata.style = 'background-color:#'+getCandidateBackGround(record)+';'
                	metadata.tdAttr = 'data-qtip="' + value + '"';
                	return value;
                }
            },
          	{text : 'Marketing Status',dataIndex: 'marketingStatus',width:55,
            	renderer:function(value, metadata, record){
                	metadata.style = 'background-color:#'+getCandidateBackGround(record)+';'
                	metadata.tdAttr = 'data-qtip="' + record.data.submissions + '"';
                	return value;
                }
            },
          	{text : 'Alert',dataIndex: 'alert',width:70,
            	renderer:function(value, metadata, record){	
                	metadata.style = 'background-color:#'+getCandidateBackGround(record)+';'
                	metadata.tdAttr = 'data-qtip="' + value + '"';
                    if (value == null || value=="") 
                    	return 'N/A' ;
                    return '<span style="color:red;">' + value + '</span>';
                }
            },
          	{text : 'Source',dataIndex: 'source',width:100,
            	renderer:function(value, metadata, record){
                	metadata.style = 'background-color:#'+getCandidateBackGround(record)+';'
                	metadata.tdAttr = 'data-qtip="' + "<b>Source : </b>" +value +"<br><b>Referral : </b>"+record.data.referral+ '"';
                	if (value == 'Referral' || value == 'Employee Referral')
                		return 'R-'+record.data.referral;
                	return value;
                }
          	},
          	{text : 'Certifications',dataIndex: 'allCertifications',width:120,
          		renderer:function(value, metadata, record){
          			var tooltip = "";
                	if(value != null && value != ''){
                		tooltip = "<b>Certifications :</b><br>"+value.replace(/ -- /g, '<br />')+'<br><br>';
                	}
                	if (record.data.skillSetIds != null  && record.data.skillSetIds != '') {
    					var skillSet = record.data.skillSetIds.split(',');
    					tooltip += "<b>Skill Set: </b><br>"
    					for ( var i = 1; i <= skillSet.length; i++) {
    						tooltip += ds.skillSetStore.getById(Number(skillSet[i-1])).data.value+',';
    						if (i%3 == 0)
    							tooltip += "<br>";
    					}
    					tooltip += "<br><br>";
    				}
                	if (record.data.roleSetIds != null  && record.data.roleSetIds != '') {
    					var roleSet = record.data.roleSetIds.split(',');
    					tooltip += "<b>Roleset: </b><br>";
    					for ( var i = 1; i <= roleSet.length; i++) {
    						tooltip += ds.targetRolesStore.getById(Number(roleSet[i-1])).data.value+',';
    						if (i%3 == 0)
    							tooltip += "<br>";
    					}
    					tooltip += "<br><br>";
    				}
                	if (record.data.targetSkillSetIds != null  && record.data.targetSkillSetIds != '') {
    					var targetSkillSet = record.data.targetSkillSetIds.split(',');
    					tooltip += "<b>Target Skill Set: </b><br>";
    					for ( var i = 1; i <= targetSkillSet.length; i++) {
    						tooltip += ds.skillSetStore.getById(Number(targetSkillSet[i-1])).data.value+',';
    						if (i%3 == 0)
    							tooltip += "<br>";
    					}
    					tooltip += "<br><br>";
    				}
            		if (record.data.targetRoles != null  && record.data.targetRoles != '') {
    					var targetRoles = record.data.targetRoles.split(',');
    					tooltip += "<b>Target Roles: </b><br>";
    					for ( var i = 1; i <= targetRoles.length; i++) {
    						tooltip += targetRoles[i-1]+',';
    						if (i%3 == 0)
    							tooltip += "<br>";
    					}
    				}
            	    metadata.tdAttr = 'data-qtip="' + tooltip + '"';
                    return value.replace(/ -- /g, ', ') ;
                }
          	},
          	{text : 'Skill Set',dataIndex: 'skillSet',width:120,renderer: candidateRender,hidden : true},
          	{text : 'Availability',dataIndex : 'availability',width :  68,renderer: endDateRenderer,},
          	{text : 'Email Id',dataIndex: 'emailId',width:60,renderer: candidateRender,hidden : true},
          	{text : 'Contact No',dataIndex: 'contactNumber',width:80,renderer: candidateRender,},
          	{text : 'P1 Comments',dataIndex: 'p1comments',width:90,renderer: candidateRender,},
          	{text : 'Expected Rate',dataIndex: 'expectedRate',width:75,renderer: candidateRender,},
          	{text : 'Rate From',dataIndex: 'rateFrom',width:75,renderer: numberRender,},
          	{text : 'Rate To',dataIndex: 'rateTo',width:75,renderer: numberRender,},
          	{text : 'Will Relocate',dataIndex : 'relocate',width :  55,renderer: activeRenderer,},
          	{text : 'Will Travel',dataIndex : 'travel',width :  55,renderer: activeRenderer,},
          	{text : 'Open To CTH',dataIndex : 'openToCTH',width :  75,renderer: candidateRender,},
          	{text : 'City & State',dataIndex : 'cityAndState',width :  90,renderer: candidateRender,},
          	{text : 'Location',dataIndex : 'location',width :  75,renderer: candidateRender,},
          	{text : 'Follow Up',dataIndex : 'followUp',width :  75,renderer: followUpRenderer,},
          	{text : 'Employment Type',dataIndex : 'employmentType',width :  75,renderer: candidateRender,},
          	{text : 'About Partner',dataIndex : 'aboutPartner',width :  75,renderer: candidateRender,},
          	{text : 'Personality',dataIndex : 'personality',width :  75,renderer: candidateRender,},
          	{text : 'Contact Address',dataIndex : 'contactAddress',width :  75,renderer: candidateRender,},
          	{text : 'Start Date',dataIndex : 'startDate',width :  75,renderer: candidateRender,},
          	{text : 'Education',dataIndex : 'education',width :  75,renderer: candidateRender,},
          	{text : 'Immigration Status',dataIndex : 'immigrationStatus',width :  75,renderer: candidateRender,},
          	{text : 'Immigration Verified',dataIndex : 'immigrationVerified',width :  75,renderer: candidateRender,},
          	{text : 'Type',dataIndex : 'type',width :  75,renderer: candidateRender,},
          	{text : 'Employer',dataIndex : 'employer',width :  75,renderer: candidateRender,},
          	{text : 'Highest Qualification Held',dataIndex : 'highestQualification',width :  75,renderer: candidateRender,},
          	{text : 'Roleset',dataIndex : 'currentRoles',width :  75,renderer: candidateRender,hidden : true},
          	{text : 'Target Roles',dataIndex : 'targetRoles',width :  75,renderer: candidateRender,hidden : true},
          	{text : 'Target Skill Set',dataIndex : 'targetSkillSet',width :  75,renderer: candidateRender,hidden : true},
          	{text : 'Other SkillSet' , dataIndex: 'otherSkillSet',width:120,renderer: candidateRender,},
          	{text : 'Interview Help',dataIndex : 'interviewHelp',width :  75,renderer: candidateRender,},
          	{text : 'Job Help',dataIndex : 'jobHelp',width :  75,renderer: candidateRender,},
          	{text : 'Current Job Title',dataIndex : 'currentJobTitle',width :  75,renderer: candidateRender,},
        ];
        me.viewConfig = {
        	stripeRows: true,
        };
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
		    listeners: {
                render: function(c) {
                    new Ext.ToolTip({
                        target: c.getEl(),
                        maxWidth: 1000,
                        dismissDelay: 60000,
                        html: 'Default : Displaying P0, P1 candidates with Marketing status "Active"' 
                    });
                }
            }
        });

        me.priority = new Ext.form.field.Number({
        	fieldLabel: 'Priority Upto',
        	value : 0,
        	minValue : 0,
        	maxValue : 100,
        });

	    var priority = [];
	    y = 0;
	    while (y<=10){
	    	priority.push([y]);
	         y++;
	    }
	    this.priorityStore = new Ext.data.SimpleStore
	    ({
	          fields : ['priority'],
	          data : priority
	    });

		me.priorityCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Priority',
		    store : this.priorityStore,
		    queryMode: 'local',
		    displayField: 'priority',
            valueField: 'priority',
		    name : 'priority',
		    allowDecimals: false,
		    value : [0,1],
		    labelWidth :45,
		    width :170,
		    tabIndex:23,
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
		});
		
        me.searchField = new Ext.form.field.Text({
			fieldLabel: 'Search',
			name: 'search',
			labelWidth :45,
			width :170,
			tabIndex:21,
 			listeners: {
 				render: function(c) {
 					new Ext.ToolTip({
 						target: c.getEl(),
 						dismissDelay: 60000,
 						html: 'Search Active candidates'
 					});
 				},
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
        });
        
        me.sourceField = new Ext.form.field.Text({
        	fieldLabel: 'Source',
        	name: 'source',
        	labelWidth :45,
        	width :170,
        	tabIndex:22,
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				},//specialkey
 			}//listeners
        });
		
        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [me.searchField,me.sourceField,me.priorityCombo,
        	        '-',{
        				text : 'Search',
        				iconCls : 'btn-Search',
        				tabIndex:24,
        				handler: function(){
        					me.search();
        				}
        	        },'-',{
        	        	xtype:'button',
        	        	iconCls : 'btn-email',
        	        	text: 'Email',
        	        	tooltip:'Email all candidates',
        	        	tabIndex:25,
        	        	handler: function(){
        	        		me.showEmail(me.store);
        	        	}
        	        },'->',me.showCount
        	        ]
        }];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });

        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			this.showCount.setValue(msg);
			this.loadSubmissions();
       	}, me);

        me.callParent(arguments);
        
        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);
        
    },
    
    search : function() {
    	var params = new Array();
    	if (this.priorityCombo.getValue() != null)
    		params.push(['priority','=',this.priorityCombo.getValue().toString()]);
    	params.push(['marketingStatus','=', "1. Active"]);
    	params.push(['source','=', this.sourceField.getValue()]);
    	params.push(['search','=', this.searchField.getValue()]);
    	var filter = getFilter(params);
    	ds.homeCandidateStore.loadByCriteria(filter);
	},
	
	loadSubmissions : function() {
    	var params = new Array();
    	var candidateIds = this.store.collect('id');
    	params.push(['table','=', "submissions"]);
    	params.push(['candidateId','=', candidateIds.toString()]);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.subRequirementHomeStore.loadByCriteria(filter);
    	var headings = "<tr style='background-color:#dddddd;font-weight:bold;'>" +
						"<td>Vendor</td>" +
						"<td>Client</td>" +
						"<td>Submitted Date</td>" +
						"<td>Submitted Rate-1</td>" +
						"<td>Submitted Rate-2</td>" +
						"<td>Posting Date</td>" +
						"<td>Posting Title</td>" +
						"<td>Location</td></tr>";
    	
    	ds.subRequirementHomeStore.on('load', function(store, records, options) {
    		var candidateIds = this.store.collect('id');
    		for ( var i = 0; i < candidateIds.length; i++) {
    			ds.subRequirementHomeStore.clearFilter();
    			ds.subRequirementHomeStore.filterBy(function(rec) {
    			    return rec.get('candidateId') === Number(candidateIds[i]);
    			});
    			var table ="";
    			for ( var j = 0; j < ds.subRequirementHomeStore.getCount() && j< 10; j++) {
    				var rec = ds.subRequirementHomeStore.getAt(j);
					table += "<tr bgcolor='#ffffff'><td>"+rec.data.vendor+"</td>" +
							"<td>"+rec.data.client+"</td>" +
							"<td>"+Ext.Date.format(rec.data.submittedDate,'m/d/Y')+"</td>" +
							"<td>"+rec.data.submittedRate1+"</td>" +
							"<td>"+rec.data.submittedRate2+"</td>" +
							"<td>"+Ext.Date.format(rec.data.postingDate,'m/d/Y')+"</td>" +
							"<td>"+rec.data.postingTitle+"</td>" +
							"<td>"+rec.data.location+"</td></tr>" ;
				}
    			if (table != ''){
    				table = "<table cellpadding='5' bgcolor='#575252'>"+headings+table+'</table>';
    				var vendors = ds.subRequirementHomeStore.collect('vendor');
    				vendors.sort();
    				var clients = ds.subRequirementHomeStore.collect('client');
    				clients.sort();
    				var vendorsList = '';
    				for ( var j = 0; j < vendors.length; j++) {
    	    			ds.subRequirementHomeStore.filterBy(function(rec) {
    	    			    return rec.get('candidateId') === Number(candidateIds[i]) && rec.get('vendor') === vendors[j];
    	    			});
    	    			vendorsList += vendors[j]+' - '+ds.subRequirementHomeStore.getCount()+', ';
					}
    				vendorsList = vendorsList.substring(0,vendorsList.length-2);
    				table += '<br><big><b>Submitted Vendors :</b> '+vendorsList+'</big>';
    				table += '<br><br><big><b>Submitted Clients :</b> '+clients.toString()+'</big>';
    			}
    			else
    				table = "No submissions";
    			var employeeName = this.store.getById(candidateIds[i]).data.fullName;
    			this.store.getById(Number(candidateIds[i])).data.submissions = '<b>'+employeeName+' previous submissions : </b>'+table;
			}
    		this.getView().refresh();
    	}, this);
		
	},
	
	showCandidate : function(rowIndex) {
		var record = this.store.getAt(rowIndex);
		app.setActiveTab(0,'li_candidates');
		app.candidateMgmtPanel.getLayout().setActiveItem(1);
		app.candidateMgmtPanel.candidatePanel.loadForm(record);
		app.candidateMgmtPanel.candidatePanel.openedFrom = 'Home';

	},

	showQuickAccess : function(candidateId) {
    	ds.quickAccessStore.clearFilter();
        ds.quickAccessStore.filterBy(function(rec) {
        	return rec.get('type') === 'Candidate'; 
        });
        
        var employee = ds.quickAccessStore.getAt(ds.quickAccessStore.findExact('referenceId',candidateId.toString()));
        app.showSnapshot(employee);
	},
	
	showEmail : function(store) {
		var emailWindowPanel = new tz.ui.EmailWindowPanel();
		this.candidates = store.collect('fullName');
		this.emailIds = store.collect('emailId');

		emailWindowPanel.candidates = this.candidates;
		emailWindowPanel.emailIds = this.emailIds;

		var container = new Ext.container.Container({
			layout: {
		        type: 'table',
		        columns: 4,
		    },
		    items: []
		});
		var items = [];
		for ( var i = 0; i < this.candidates.length; i++) {
	        var field = new Ext.form.Label({
	        	id:this.emailIds[i],
	            html:this.candidates[i]+'&nbsp <a onclick="javascript:app.homeMgmtPanel.homeDetailPanel.candidatesGrid.removeEmailId(\''+this.emailIds[i]+'\');" href="#"></a>',
	            cls:'email-labels',  
	            style: {
	            	color: '#FFFFFF',
	            	backgroundColor:'#4C4C4C'
	            },
	        });
	        items.push(field);
		}

		container.items.add(items);
		emailWindowPanel.candidatesPanel.add(container);

		var addEmailButton = new Ext.Button({
			id : 'addEmailButton_seperate',
            text: '',
            iconCls : 'btn-add',
            handler: function(){
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').reset();
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').reset();
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').show();
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').show();
            	Ext.getCmp('addEmailButton_seperate').hide();
            	Ext.getCmp('saveEmailButton_seperate').show();
            	emailWindow.items.items[0].candidatesPanel.doLayout();
    		}
        });

		var saveEmailButton = new Ext.Button({
			margin : '0 0 0 10',
            id : 'saveEmailButton_seperate',
            icon : 'images/accept.png',
            hidden : true,
            handler: function(){
            	if (! emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').isValid() || 
            			! emailWindow.items.items[0].candidatesPanel.form.findField('emailId').isValid() ) {
            		Ext.Msg.alert('Error','Please fix the errors.');
            		return false;
				}

            	var candidateName = emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').getValue();
            	var emailId = emailWindow.items.items[0].candidatesPanel.form.findField('emailId').getValue();
            	var field = new Ext.form.Label({
    	        	id: emailId,
    	            html: candidateName+'&nbsp <a onclick="javascript:app.homeMgmtPanel.homeDetailPanel.candidatesGrid.removeEmailId(\''+emailId+'\');" href="#"></a>',
    	            cls:'email-labels',  
    	            style: {
    	            	color: '#FFFFFF',
    	            	backgroundColor:'#4C4C4C'
    	            },
    	        });
            	emailWindow.items.items[0].candidatesPanel.items.items[0].items.add(field);
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').hide();
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').hide();
            	Ext.getCmp('addEmailButton_seperate').show();
            	Ext.getCmp('saveEmailButton_seperate').hide();
            	emailWindow.items.items[0].candidatesPanel.doLayout();
            	app.homeMgmtPanel.homeDetailPanel.candidatesGrid.emailIds.push(emailId);
				app.homeMgmtPanel.homeDetailPanel.candidatesGrid.candidates.push(candidateName);
    		}
        });

		var container2 = new Ext.container.Container({
			fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth :50},
			layout: {
		        type: 'table',
		        columns: 4,
		    },
		    items: [addEmailButton,
		            {
		    	xtype: 'textfield',
				name: 'candidateName',
				fieldLabel: 'Name',
				allowBlank: false,
				hidden : true,
				labelWidth :50
			},{
				xtype: 'textfield',
				name: 'emailId',
				fieldLabel: 'Email Id',
				allowBlank: false,
				hidden : true,
				vtype: 'email',
				labelWidth :60
			},
			saveEmailButton
			]
		});
		emailWindowPanel.candidatesPanel.add(container2);
		
		if (Ext.getCmp('emailHomeCandidateWindow') == null) {
			var emailWindow = Ext.create('Ext.window.Window', {
			    title: 'Email',
			    id : 'emailHomeCandidateWindow',
			    width: 1000,	
			    height : app.mainViewport.height-90,
			    modal: true,
			    autoScroll:true,
			    items: [emailWindowPanel],
			    buttons: [{
			    	text: 'Send All',
		            handler: function(){
		            	if (emailWindow.items.items[0].form.isValid()) {
		            		var params = new Array();
		            		params.push(['from','=', emailWindow.items.items[0].fromEmailCombo.getValue()]);
		            		params.push(['to','=', app.homeMgmtPanel.homeDetailPanel.candidatesGrid.emailIds.toString()]);
		            		params.push(['candidates','=', app.homeMgmtPanel.homeDetailPanel.candidatesGrid.candidates.toString()]);
		            		params.push(['subject','=', emailWindow.items.items[0].form.findField('subject').getValue()]);
		            		params.push(['message','=', emailWindow.items.items[0].form.findField('message').getValue()]);
			            	var filter = getFilter(params);
			            	app.homeMgmtPanel.homeDetailPanel.candidatesGrid.sendEmailConform(filter);
						}//if
		            }//handler
		        }]//buttons
			});
		}
		Ext.getCmp('emailHomeCandidateWindow').show();
	},

	sendEmailConform : function(filter) {
		this.emailFilter = filter;
		Ext.Msg.confirm("Confirm","Do you want to send email to "+this.emailIds.length+" candidates?", this.sendEmail, this);
	},

	sendEmail : function(dat) {
		if (dat=='yes') {
			var filter = this.emailFilter;
			app.reportsService.sendHomeEmail(Ext.encode(filter),this.onSendEmail,this);	
		}
	},
	
	onSendEmail : function(data) {
		if (data.success) {
			Ext.getCmp('emailHomeCandidateWindow').close();
			Ext.Msg.alert('Success','Email sent successfully.');
			var filter = this.emailFilter;
			app.reportsService.sendSummaryEmail(Ext.encode(filter));
		}else
			Ext.Msg.alert('Error',data.errorMessage);
	},
	
	removeEmailId : function(emailId) {
		//Ext.getCmp('jobEmailPanel').remove(Ext.getCmp(id),true);
		Ext.getCmp(emailId).hide();
		this.emailIds.splice(this.emailIds.indexOf(emailId), 1);
		this.candidates.splice(this.emailIds.indexOf(emailId), 1);
	},
	
	shareRequirement : function(record) {
		if (record != null) {
			record = record.data;
			var detailsTable = "<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>"+
						"<tr><th>Name</th><td>"+record.firstName+' '+record.lastName+'</td></tr>'+
						"<tr><th>Email id</th><td>" +record.emailId+'</td></tr>'+
						"<tr><th>Contact number</th><td>" +record.contactNumber+'</td></tr>'+
						"<tr><th>City & State</th><td>" +record.cityAndState+'</td></tr>'+
						"<tr><th>Immigration Status</th><td>" +record.immigrationStatus+'</td></tr>'+
						"<tr><th>Contact Address</th><td>" +record.contactAddress+'</td></tr>'+
						"<tr><th>Current Job title</th><td>" +record.currentJobTitle+'</td></tr>';
			
			if (record.availability != null && record.availability != '')
				detailsTable += "<tr><th>Availability</th><td>" +Ext.Date.format(new Date(record.availability),'m/d/Y')+'</td></tr>';
			else
				detailsTable += "<tr><th>Availability</th><td>N/A" +'</td></tr>';
			if (record.startDate != null && record.startDate != '')
				detailsTable += "<tr><th>Start Date</th><td>" +Ext.Date.format(new Date(record.startDate),'m/d/Y')+'</td></tr>';
			else
				detailsTable += "<tr><th>Start Date</th><td>N/A" +'</td></tr>';
			
			detailsTable += "<tr><th>Experience</th><td>" +record.experience+'</td></tr>'+
						"<tr><th>Marketing Status</th><td>" +record.marketingStatus+'</td></tr>'+
						"<tr><th>Communication</th><td>" +record.communication+'</td></tr>'+
						"<tr><th>Personality</th><td>" +record.personality+'</td></tr>'+
						"<tr><th>Expected Rate</th><td>" +record.expectedRate+ " From : </b>"+record.rateFrom +" To : </b>"+record.rateTo +'</td></tr>'+
						"<tr><th>Education</th><td>" +record.education+'</td></tr>'+
						"<tr><th>Highest Qualification Held</th><td>" +record.highestQualification+'</td></tr>'+
						"<tr><th>Employment Type</th><td>" +record.employmentType+'</td></tr>'+
						"<tr><th>Skill Set</th><td>" +record.skillSet+'</td></tr>'+
						"<tr><th>Role Set</th><td>" +record.currentRoles+'</td></tr>'+
						"<tr><th>Target Skill Set</th><td>" +record.targetSkillSet+'</td></tr>'+
						"<tr><th>Target Roles</th><td>" +record.targetRoles+'</td></tr>'+
						"<tr><th>Certifications</th><td>" +record.allCertifications.replace(/ -- /g, ' , ')+'</td></tr></table>';

			app.homeMgmtPanel.showSharePanel();
			app.homeMgmtPanel.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').setValue(detailsTable);
			app.homeMgmtPanel.requirementSharePanel.fileName = record.firstName+' '+record.lastName;
			app.homeMgmtPanel.requirementSharePanel.data = '';
			app.homeMgmtPanel.requirementSharePanel.openedFrom = 'Home';
		}
		app.homeMgmtPanel.requirementSharePanel.emailShortlisted.disable();
		app.homeMgmtPanel.requirementSharePanel.emailConfidential.disable();
	}

});

function getCandidateBackGround(record) {
	if (record.data.marketingStatus == '1. Active' ||  record.data.marketingStatus == '2. Yet to Start') {
		if (record.data.priority == 0)
			return 'C3D5F6';
		else if (record.data.priority == 1)
			return 'CFDCF5';
		else if (record.data.priority == 2)
			return 'D9E3F5';
		else if (record.data.priority == 3)
			return 'E5EBF5';
	}
	return null;
}