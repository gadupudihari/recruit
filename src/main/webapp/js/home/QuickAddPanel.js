Ext.define('tz.ui.QuickAddPanel', {
    extend: 'tz.ui.BaseFormPanel',
    border:0,
    title: '',
    anchor:'100% 100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},

    initComponent: function() {
        var me = this;
        me.addMore = false;

		me.sourceCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Source",
            name: 'source',
            store: ds.candidateSourceStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:106,
		});

		me.marketingStatusCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Marketing Status",
            name: 'marketingStatus',
            store: ds.marketingStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    valueField: 'name',
		    tabIndex:103,
		});

		me.relocateCheckbox = Ext.create('Ext.form.Checkbox', {
        	boxLabel : 'Will Relocate',
            name : 'relocate',
            inputValue: '0',
            boxLabelAlign : 'before',
            labelAlign :'right',
            itemId : 'relocate',
            tabIndex:15,
            padding : '0 0 0 32',
        });

		me.travelCheckbox = Ext.create('Ext.form.Checkbox', {
        	boxLabel : 'Will Travel',
            name : 'travel',
            inputValue: '0',
            boxLabelAlign : 'before',
            labelAlign :'right',
            itemId : 'travel',
            tabIndex:16,
            padding : '0 0 0 45',
        });

        me.candidatePanel = Ext.create('Ext.form.Panel', {
        	title : '',
        	bodyPadding: 10,
        	border : 1,
        	frame:false,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 100},
        	xtype: 'container',
        	dockedItems :  [{
	        	 xtype: 'toolbar',
	        	 dock: 'top',
	        	 items: [{
	        		 xtype: 'button',
	        		 text: 'Save',
	        		 tooltip: 'Save Candidate',
	        		 tabIndex : 111,
	        		 iconCls : 'btn-save',
	        		 scope: this,
	        		 handler: this.saveCandidate
	        	 },'-',{
	        		 xtype: 'button',
	        		 text: 'Reset',
	        		 tooltip: 'Reset Candidate',
	        		 tabIndex : 112,
	        		 scope: this,
	        		 handler: function(){
                     	me.candidatePanel.form.reset();
                     }
	        	 }]
            }],
        	
	         items:[{
	        	 xtype : 'fieldset',
	        	 defaultType : 'textfield',
	        	 layout : 'column',
	        	 items : [{
	        		 xtype: 'container',
	        		 columnWidth:.9,
	        		 items: [{
	        			 xtype:'textfield',
	        			 fieldLabel: 'First Name *',
	        			 name: 'firstName',
	        			 allowBlank:false,
	        			 maxLength:45,
	        			 enforceMaxLength:true,
	        			 tabIndex:101,
	        		 },{
	        			 xtype:'textfield',
	        			 fieldLabel: 'Last Name *',
	        			 name: 'lastName',
	        			 allowBlank:false,
	        			 tabIndex:102,
	        			 maxLength:45,
	        			 enforceMaxLength:true,
	        		 },
	        		 me.marketingStatusCombo,
	        		 {
	        			 xtype:'textfield',
	        			 fieldLabel: "Resume Link",
	        			 name: 'resumeLink',
	        			 tabIndex:104,
	        			 maxLength:250,
	        			 enforceMaxLength:true,
	        		 },{
	        			 xtype:'textfield',
	        			 fieldLabel: "Referral/More info",
	        			 name: 'referral',
	        			 tabIndex:105,
	        			 maxLength:45,
	        			 enforceMaxLength:true,
	        		 },
	        		 me.sourceCombo
	        		 ]
	        	 }]
	         },{
	        	 xtype : 'fieldset',
	        	 title : 'More',
	        	 collapsible: true,
	        	 collapsed : true,
	        	 defaultType : 'textfield',
	        	 layout : 'column',
	        	 items : [{
	        		 xtype: 'container',
	        		 columnWidth:.9,
	        		 items: [{
	        			 xtype:'textfield',
	        			 fieldLabel: "City & State",
	        			 name: 'cityAndState',
	        			 tabIndex:107,
	        			 maxLength:90,
	        			 enforceMaxLength:true,
	        		 },{
	        			 xtype:'textfield',
	        			 fieldLabel: 'Email',
	        			 name: 'emailId',
	        			 vtype:'email',
	        			 tabIndex:108,
	        			 maxLength:45,
	        			 enforceMaxLength:true,
	        		 },{
	        			 xtype:'textfield',
	        			 fieldLabel: 'Contact Number',
	        			 name: 'contactNumber',
	        			 maskRe :  /[0-9]/,
	        			 tabIndex:109,
	        			 maxLength:12,
	        			 enforceMaxLength:true,
	        			 listeners:{
	        				 change : function(field,newValue,oldValue){
	        					 var value = me.form.findField('contactNumber').getValue();
	        					 if (value != "" ) {
	        						 value = value.replace(/[^0-9]/g, "");
	        						 value = value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
	        						 me.candidatePanel.form.findField('contactNumber').setValue(value);
	        					 }
	        				 }
	        			 }
	        		 },
	        		 me.relocateCheckbox,me.travelCheckbox,
	        		 ]
	        	 }]	
	         },{
	        	 xtype:'button',
	        	 text: 'Save and Add More',
	        	 iconCls : 'btn-save',
	        	 tabIndex : 113,
	        	 margin: '0 0 0 0',
	        	 height : 18,
	        	 scope: this,
	        	 handler: function(){
	        		 me.saveAndMoreCandidate();
	        	 }
	         }]
        });

		me.roleCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Contact Type",
            name: 'role',
	        tabIndex:205,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.contactRoleStore,
            multiSelect : true
		});
		
		me.typeCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Type',
		    queryMode: 'local',
		    forceSelection:true,
            displayField: 'value',
            valueField: 'name',
		    name : 'type',
		    store: ds.contactTypeStore,
		    tabIndex:206,
		    multiSelect: true,
		});

        me.contactPanel = Ext.create('Ext.form.Panel', {
        	title : '',
        	bodyPadding: 10,
        	border : 1,
        	frame:false,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 100},
        	xtype: 'container',
        	dockedItems :  [{
	        	 xtype: 'toolbar',
	        	 dock: 'top',
	        	 items: [{
	        		 xtype: 'button',
	        		 text: 'Save',
	        		 tooltip: 'Save Contact',
	        		 tabIndex : 211,
	        		 iconCls : 'btn-save',
	        		 scope: this,
	        		 handler: this.saveContact
	        	 },'-',{
	        		 xtype: 'button',
	        		 text: 'Reset',
	        		 tooltip: 'Reset Contact',
	        		 tabIndex : 212,
	        		 scope: this,
	        		 handler: function(){
                     	me.contactPanel.form.reset();
                     }
	        	 }]
            }],
        	
	         items:[{
	        	 xtype : 'fieldset',
	        	 defaultType : 'textfield',
	        	 layout : 'column',
	        	 items : [{
	        		 xtype: 'container',
	        		 columnWidth:.9,
	        		 items: [{
	        			 xtype:'textfield',
	        			 fieldLabel: 'First Name *',
	        			 name: 'firstName',
	        			 allowBlank:false,
	        			 maxLength:45,
	        			 enforceMaxLength:true,
	        			 tabIndex:201,
	        		 },{
	        			 xtype:'textfield',
	        			 fieldLabel: 'Last Name *',
	        			 name: 'lastName',
	        			 allowBlank:false,
	        			 tabIndex:202,
	        			 maxLength:45,
	        			 enforceMaxLength:true,
	        		 },{
	        			 xtype:'textfield',
	        			 fieldLabel: 'Email',
	        			 name: 'email',
	        			 vtype:'email',
	        			 tabIndex:203,
	        			 maxLength:45,
	        			 enforceMaxLength:true,
	        		 },{
	        			 xtype:'textfield',
	        			 fieldLabel: 'Cell Phone',
	        			 name: 'cellPhone',
	        			 maskRe :  /[0-9]/,
	        			 tabIndex:204,
	        			 maxLength:12,
	        			 enforceMaxLength:true,
	        			 listeners:{
	        				 change : function(field,newValue,oldValue){
	        					 var value = me.form.findField('cellPhone').getValue();
	        					 if (value != "" && value != null) {
	        						 value = value.replace(/[^0-9]/g, "");
	        						 value = value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
	        						 me.contactPanel.form.findField('cellPhone').setValue(value);
	        					 }
	        				 }
	        			 }
	        		 },
	        		 me.typeCombo
	        		 ]
	        	 }]
	         },{
	        	 xtype:'button',
	        	 text: 'Save and Add More',
	        	 iconCls : 'btn-save',
	        	 tabIndex : 113,
	        	 margin: '0 0 0 0',
	        	 height : 18,
	        	 scope: this,
	        	 handler: function(){
	        		 me.saveAndMoreContact();
	        	 }
	         }]
        });
        
        me.alertsTab = Ext.create('Ext.tab.Panel', {
			plain: true,
        	border:0,
        	layout   : 'card',
        	defaults: {
	        	listeners: {
	        		//Reset monthlyexpenses grid when clicked
	        		activate: function(tab, eOpts) {
	        			me.updateActiveTab(tab);
	        		}
	        	}
        	},
			items : [{
				title : 'Candidate',
				items : [me.candidatePanel]
			},{
				title : 'Contact',
				items : [me.contactPanel]
			}]
		});

        me.items = [me.alertsTab];
        me.callParent(arguments);
    },
    
    updateActiveTab : function(tab) {
		if (tab.title == "Candidate") {
			this.candidatePanel.form.findField('firstName').focus(true, 10);
		}else if (tab.title == "Contact") {
			this.contactPanel.form.findField('firstName').focus(true, 10);
		}
	},
    
	saveCandidate: function(){
		if(! this.candidatePanel.form.isValid()){
			Ext.Msg.alert('Error','Please fix the errors');
			return false;
		}
		var itemslength = this.candidatePanel.form.getFields().getCount();
   		var i = 0;    		
   		var candidate = {};
   		while (i < itemslength) {
   			var fieldName = this.candidatePanel.form.getFields().getAt(i).name;
   			candidate[fieldName] = this.candidatePanel.form.findField(fieldName).getValue();
   			i = i + 1;
   		}
   		candidate.version = 0;
   		delete candidate.candidateHyperlink;
   		
    	var candidateObj = Ext.JSON.encode(candidate);
		app.candidateService.saveCandidate(candidateObj, this.onSaveCandidate, this);
	},
	
	onSaveCandidate: function(data){
		if(!data.success){
	    	Ext.Msg.alert('Success','Saved Successfully');
		}else{
			ds.contractorsStore.loadByCriteria();
			this.candidatePanel.form.reset();
			Ext.Msg.alert('Success','Saved Successfully');
			if(this.addMore){
				this.addMore = false;
				this.viewCandidate(data.returnVal.id);
				Ext.getCmp('quickAddWindow').close();
			}
		}
	},

	viewCandidate : function(candidateId) {
		if (typeof (candidateId) != 'undefined' && candidateId != null && candidateId >= 0){
			var params = new Array();
			params.push(['id','=', candidateId]);
			var filter = getFilter(params);
			filter.pageSize=50;
			app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidate,this);
		}
	},

	onGetCandidate : function(data) {
		if (data.rows.length > 0) {
			app.setActiveTab(0,'li_candidates');
			app.candidateMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			app.candidateMgmtPanel.candidatePanel.loadForm(record);
		}
	},

    saveContact : function(){
    	if(this.contactPanel.form.isValid( )){
			var otherFieldObj = {};
			var itemslength = this.contactPanel.form.getFields().getCount();
			var i = 0;
			while (i < itemslength) {
				var fieldName = this.contactPanel.form.getFields().getAt(i).name;
				otherFieldObj[fieldName] = this.contactPanel.form.findField(fieldName).getValue();
				i = i+1;
			}
			otherFieldObj.confidentiality = 5;
			delete otherFieldObj.contactHyperlink;
			if (this.typeCombo.getValue() != null)
				otherFieldObj['type'] = this.typeCombo.getValue().toString();
			if (this.roleCombo.getValue() != null)
				otherFieldObj['role'] = this.roleCombo.getValue().toString();
			otherFieldObj = Ext.JSON.encode(otherFieldObj);
			app.contactService.saveContact(otherFieldObj,this.onSaveContact,this);
    	} else {
    		Ext.Msg.alert('Error','Please fix the errors');
    	}
	},
	
	onSaveContact : function(data){
		this.modified = true;
		if (data.success) { 
	    	Ext.Msg.alert('Success','Saved Successfully');
	    	this.contactPanel.form.reset();
	    	var params = new Array();
	    	var filter = getFilter(params);
	    	filter.pageSize=1000;
	    	ds.contactSearchStore.loadByCriteria(filter);
			if(this.addMore){
				this.addMore = false;
				this.viewContact(data.returnVal.id);
				Ext.getCmp('quickAddWindow').close();
			}
		}else {
			Ext.Msg.alert('Error',data.errorMessage);
		}	
	},	

	viewContact : function(contactId) {
    	var params = new Array();
    	params.push(['id','=', contactId]);
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	app.contactService.getContacts(Ext.JSON.encode(filter),this.onGetContact,this);
	},
	
	onGetContact : function(data) {
		if (data.success && data.rows.length > 0) {
	    	app.setActiveTab(0,'li_contacts');
	    	app.contactMgmtPanel.getLayout().setActiveItem(1);
	    	app.contactMgmtPanel.contactDetailPanel.opendFrom='Home';
			var contact = Ext.create('tz.model.Contact', data.rows[0]);
			app.contactMgmtPanel.contactDetailPanel.loadForm(contact);
		}
	},

	saveAndMoreCandidate : function() {
		this.addMore = true;
		this.saveCandidate();
	},
	
	saveAndMoreContact : function() {
		this.addMore = true;
		this.saveContact();
	},
    
});
