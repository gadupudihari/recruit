Ext.define('tz.ui.HomeRequirementsGrid2', {
    extend: 'Ext.grid.Panel',
    title: 'Submissions',
    frame:true,
    width :'100%',
    //split: true,
	//region: 'west', 
	//collapsible: true,
	listeners: {
		afterrender: function(panel) {
			panel.header.el.on('dblclick', function() {
		    	var browserHeight = app.mainViewport.height;
				var headerHt = app.mainViewport.items.items[0].getHeight();
				var grid2 = app.homeMgmtPanel.homeDetailPanel.requirementsGrid1;
				if (panel.collapsed != true && grid2.collapsed != true) {
					// both are expanded
					grid2.collapse();
					grid2.collapsed = true;
					panel.setHeight((browserHeight - headerHt - 75));
				}else {
					//grid1 expanded and grid2 collapsed
					panel.expand();
					grid2.expand();
					panel.setHeight((browserHeight - headerHt - 50)/2);
					grid2.setHeight((browserHeight - headerHt - 50)/2);
				}
			});
		}
	},
	plugins: [{
        ptype: "headericons",
        headerButtons : [
            {
                xtype: 'button',
                tooltip : 'Maximize/Restore',  
                icon : 'images/arrow_out.png',
                scope: this,
                handler : function(panel) {
                	var panel = app.homeMgmtPanel.homeDetailPanel.requirementsGrid2;
    		    	var browserHeight = app.mainViewport.height;
    				var headerHt = app.mainViewport.items.items[0].getHeight();
    				var grid2 = app.homeMgmtPanel.homeDetailPanel.requirementsGrid1;
    				if (panel.collapsed != true && grid2.collapsed != true) {
    					// both are expanded
    					grid2.collapse();
    					grid2.collapsed = true;
    					panel.setHeight((browserHeight - headerHt - 75));
    				}else {
    					//grid1 expanded and grid2 collapsed
    					panel.expand();
    					grid2.expand();
    					panel.setHeight((browserHeight - headerHt - 50)/2);
    					grid2.setHeight((browserHeight - headerHt - 50)/2);
    				}
    			}
            }
        ]
    }],
    
    initComponent: function() {
        var me = this;
        me.store = ds.homeRequirementStore2;
        me.emailFilter = '';
        
        me.columns = [{
                          xtype : 'actioncolumn',
                          hideable : false,
                          width : 30,
                          items : [{
              				icon : 'images/icon_edit.gif',
              				tooltip : 'Edit Job Opening',
              				padding: 50,
              				scope: this,
              				handler : function(grid, rowIndex, colIndex) {
           						app.homeMgmtPanel.homeDetailPanel.requirementsGrid1.editRequirement(me.store.getAt(rowIndex).data.requirementId);
              				}
              			}]
                      },{
                    	  xtype: 'actioncolumn',
                    	  width :35,
                    	  items : [{
                    		  icon : 'images/icon_reschedule.gif',
                    		  tooltip : 'View Job Opening',
                    		  padding: 50,
                    		  scope: this,
                    		  handler : function(grid, rowIndex, colIndex) {
                    			  //app.homeMgmtPanel.homeDetailPanel.requirementsGrid1.showSuggested(me.store.getAt(rowIndex).data.requirementId);
                    			  me.shareRequirement(me.store.getAt(rowIndex));
                    		  }
                    	  }]
                      },{
                    	  xtype: 'gridcolumn',
                    	  dataIndex: 'hotness',
                    	  text: 'Hotness',
                    	  width :  45,
                    	  renderer:function(value, metadata, record){
                    		  if (Number(value) == 0) {
                    			  metadata.style = "color:blue;";
                    		  }
                    		  var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
                    		  if(rec){
                    			  metadata.tdAttr = 'data-qtip="' + rec.data.value.substring(rec.data.value.indexOf('.')+1)+ '"';
                    			  return rec.data.value.substring(rec.data.value.indexOf('.')+1);
                    		  }else{
                    			  metadata.tdAttr = 'data-qtip="' + value + '"';
                    			  return value;
                    		  }
                    	  }
                      },{
          				xtype: 'datecolumn',
          				text : 'Submitted Date',
          				dataIndex : 'submittedDate',
          				width :  60,
          				format: "m/d/Y",
          			},{
          				header : 'Submission Status',
          				dataIndex : 'submissionStatus',
          				width :  70,
          				renderer:function(value, metadata, record){
          					metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
          					metadata.tdAttr = 'data-qtip="' + value + '"';
          					if (value == "I-Scheduled")
          						metadata.tdAttr = 'data-qtip="' + 'Interview date : '+ record.data.interviewDate + '<br><b>Double click to open Interview</b>"';
          					else if (value == "C-Accepted")
          						metadata.tdAttr = 'data-qtip="' + 'Project start date: ' +Ext.Date.format(record.data.projectStartDate,'m/d/Y') + '"';
          					else if (value == "I-Succeeded" || value == "I-Failed")
          						metadata.tdAttr = 'data-qtip="' + value + '<br><b>Double click to open Interview</b>"';
          					return value;
          				}
          			},{
                    	  header : 'Candidate',
                    	  dataIndex : 'candidateId',
                    	  width :  110,
                          renderer:function(value, metadata, record){
                        	  metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
                        	  if(record){
                        		  metadata.tdAttr = 'data-qtip="'+record.data.candidate+'"';
                        		  return record.data.candidate;
                        	  }else{
                        		  return null;
                        	  }
                          }
                      },{
          				header : 'Vendor',
          				dataIndex : 'vendor',
          	            width :  80,
          				renderer:function(value, metadata, record){
          					metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
          					if (value != ''){
              					metadata.tdAttr = 'data-qtip="'+value+'<br><b>Open Snapshot</b>"';
              					return '<a href=# style="color: #000000"> '+value+'</a>';
          					}
          				}
          			},{
        		        xtype: 'gridcolumn',
        		        dataIndex: 'reqCityAndState',
        		        text: 'City & State',
        		        renderer: renderValue,
          				renderer:function(value, metadata, record){
          					metadata.tdAttr = 'data-qtip="' + value + '"';
          					metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
          					return value;
          				}
        		    },{
          				header : 'Client',
          				dataIndex : 'client',
          	            width :  80,
          				renderer:function(value, metadata, record){
      						metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
          					if (value != ''){
          						metadata.tdAttr = 'data-qtip="'+value+'<br><b>Open Snapshot</b>"';
          						return '<a href=# style="color: #000000"> '+value+'</a>';
          					}
          				}
          			},{
          				text : "Alert",
          				dataIndex : 'addInfoTodos',
          				width :  70,
          				renderer:function(value, metadata, record){
          					metadata.tdAttr = 'data-qtip="' + value + '"';
          					metadata.style = 'background-color:#'+getSubmissionBackGround(record)+';'
          				    if (value == null || value=="") 
          				    	return 'N/A' ;
          				    return '<span style="color:red;">' + value + '</span>';
          				}
          			},{
          				xtype: 'gridcolumn',
          				dataIndex: 'postingTitle',
          				text: 'Posting Title',
          				renderer:function(value, metadata, record){
                        	metadata.tdAttr = 'data-qtip="' + value + '<br><b>Double click to open Detailed Job Openinng</b>"';
                    		return '<a href=# style="color: #000000"> ' + value + '</a>';
          				}
          			},{
          				xtype: 'gridcolumn',
          				dataIndex: 'module',
          				text: 'Module',
          				renderer:function(value, metadata, record){
          					metadata.tdAttr = 'data-qtip="' + value + '"';
          					return value;
          				}
          			},{
          				xtype: 'datecolumn',
          				text : 'Posting Date',
          				dataIndex : 'postingDate',
          				width :  60,
          				format: "m/d/Y H:i:s A",
          			},{
          				header : 'Submitted Rate-1',
          				dataIndex : 'submittedRate1',
          			},{
          				header : 'Submitted Rate-2',
          				dataIndex : 'submittedRate2',
          				width :  60,
          			},{
          				dataIndex: 'candidateExpectedRate',
          				text: 'Expected Rate',
          				width :  50,
          			},{
          				dataIndex: 'statusUpdated',
          				text: 'Status Updated Date',
          				width :  60,
          				xtype: 'datecolumn',
          				format:'m/d/Y',
          			},{
          				dataIndex: 'cityAndState',
          				text: 'Homeloc',
          				width :  80,
          			},{
          				dataIndex: 'relocate',
          				text: 'Will Relocate',
          				width :  50,
          				renderer: activeRenderer,
          			},{
          				dataIndex: 'travel',
          				text: 'Will Travel',
          				width :  50,
          				renderer: activeRenderer,
          			},{
          				dataIndex: 'availability',
          				text: 'Availability',
          				width :  80,
          				xtype: 'datecolumn',
          				format:'m/d/Y',
          			},{
          				dataIndex: 'comments',
          				text: 'Comments',
          			}
          			];
        
        me.viewConfig = {
        		stripeRows: false,
        		style: {overflow:'auto', overflowX: 'hidden'},
            	getRowClass: function (record, rowIndex, rowParams, store) {
					return 'row-font12';
            	},
            	
                listeners: {
                	celldblclick: function (view, cell, cellIndex, record, row, rowIndex, e) {
                    	var colHeading = view.panel.headerCt.getHeaderAtIndex(cellIndex).text;
                    	if(colHeading == "Client"){
                    		var clientId = record.data.clientId;
                    		if (clientId != null && clientId != '')
                    			me.manager.showClient_Vendor(clientId, 'Client');
                    	}else if (colHeading == "Vendor") {
                    		var vendorId = record.data.vendorId;
                    		me.manager.showClient_Vendor(vendorId, 'Vendor');
						}else if (colHeading == "Posting Title") {
							me.shareRequirement(record);
						}else if (colHeading == "Submission Status" && ['I-Scheduled','I-Succeeded','I-Failed'].indexOf(record.data.submissionStatus) != -1) {
							me.getInterview(record);
						}
                    },
                }

        };

		me.hotJobsCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Hotness',
		    store : ds.hotnessStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'hotJobs',
		    value : ['00','01'],
		    tabIndex:11,
		    labelWidth :50,
		    width :150,
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
		});

		me.vendorCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Vendor',
		    queryMode: 'local',
		    name : 'vendor',
            displayField: 'name',
            valueField: 'id',
            store: ds.vendorStore,
		    tabIndex:12,
		    labelWidth :45,
		    width :150,
		    anyMatch:true,
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            },
			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
		});

		me.submissionStatusCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Status',
		    store: ds.submissionStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'submissionStatus',
		    tabIndex:13,
		    labelWidth :40,
		    width :140,
		    //value : ['Submitted','Withdrawn','I-Scheduled','I-Succeeded','I-Failed','C-Accepted','C-Declined'],
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
		});


        me.submissionCombo = Ext.create('Ext.form.ComboBox', {
        	matchFieldWidth: false,
            fieldLabel: 'Submitted Date',
            store: ds.monthStore,
            name : 'payroll',
            queryMode: 'local',
            displayField: 'label',
            valueField: 'value',
            labelAlign: 'right',
            tabIndex :14,
            labelWidth :80,
            width :150,
            value : 'One Week',
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
        });

        me.dockedItems = [
            {
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [me.hotJobsCombo,me.vendorCombo,me.submissionStatusCombo,me.submissionCombo,
            	        '-',{
            		text : '',
            		tabIndex:15,
            		iconCls : 'btn-Search',
            		handler: function(){
            			me.search();
            		}
            	},'-',{
            		text : 'Reset',
            		width : 40,
            		tabIndex:15,
            		handler: function(){
            			me.hotJobsCombo.reset();
            			me.submissionStatusCombo.reset();
            			me.submissionCombo.reset();
            			me.vendorCombo.reset();
            			me.search();
            		}
            	},'-',{
                    xtype: 'button',
                    icon : 'images/icon_share.png',
                    tabIndex:34,
                    scope : this,
                    handler: function(){
                    	this.shareAllRequirement();
                    }
                },'-',{
        			text : '',
         			iconCls : 'icon_info',
         			scope: this,
         			tooltip :'Default : Displaying submissions with hotness 0.Highest Priority, 1.Strong Req and submitted in last week',
         			tabIndex:16,
         			handler: function(){
         		        var legendWin = Ext.create("Ext.window.Window", {
         					title: 'Legend',
         					modal: true,
         					height      : 400,
         					width       : 550,
         					bodyStyle   : 'padding: 1px;background:#ffffff;',
         					html: '<table cellspacing="10" cellpadding="5" noOfCols="2" width="550"> '
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFCBC;"></td>'
         						+ '<td width="50%">Reqs which are opened today</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FAFAD0;"></td>'
         						+ '<td width="50%">Reqs which are opened since yesterday</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFFE0;"></td>'
         						+ '<td width="50%">Reqs which are opened since a week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFFEE;"></td>'
         						+ '<td width="50%">Reqs that are opened for more than one week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#C3D5F6;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made today</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#CFDCF5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made yesterday</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#D9E3F5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made with in one week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#E5EBF5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made more than one week back</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#A7FDA1;"></td>'
         						+ '<td width="50%">Reqs for which  placement was made</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFD3A7;"></td>'
         						+ '<td width="50%">Reqs which reached atleast interview stage</td>'
         						+ '</tr>'
         						+ '</table>'
         				});
         				legendWin.show();
         		    }
            	}]
          }
      ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        ds.contractorsStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.clientSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.contactSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        ds.vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        ds.hotnessStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        me.callParent(arguments);
        
        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);

    },
    
    search : function() {
    	var params = new Array();
    	params.push(['table','=', "submissions"]);
    	if (this.hotJobsCombo.getValue() != null) {
        	var hotness = this.hotJobsCombo.getValue().toString();
        	if (hotness != '')
        		hotness = "'"+hotness.replace(/,/g, "','")+"'";
        	params.push(['hotness','=', hotness]);
		}
    	if (this.submissionStatusCombo.getValue() != null) {
        	var openings = this.submissionStatusCombo.getValue().toString();
        	if (openings != '')
        		openings = "'"+openings.replace(/,/g, "','")+"'";
        	params.push(['submissionStatus','=', openings]);
    	}
    	
    	if (this.submissionCombo.getValue() != null && this.submissionCombo.getValue() != '') {
   		 	var date = new Date();
   		 	params.push(['submittedDateTo','=',date]);
   		 	if (this.submissionCombo.getValue() == 'One Week')
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.DAY, -6)]);
   		 	else if (this.submissionCombo.getValue() == 'Two Weeks')
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.DAY, -13)]);
   		 	else if (this.submissionCombo.getValue() == 'One Month') 
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.MONTH, -1)]);
   		 	else if (this.submissionCombo.getValue() == 'Three Months') 
   		 		params.push(['submittedDateFrom','=',Ext.Date.add(date, Ext.Date.MONTH, -3)]);
    	}
    	params.push(['vendorId','=',this.vendorCombo.getValue()]);
    	params.push(['deleted','=', 'false']);
    	var filter = getFilter(params);
    	filter.pageSize=100;
    	ds.homeRequirementStore2.loadByCriteria(filter);
	},
	
	showRequirement : function(rowIndex) {
		var record = this.store.getAt(rowIndex);
		app.setActiveTab(0,'li_requirements');
		app.requirementMgmtPanel.getLayout().setActiveItem(1);
		app.requirementMgmtPanel.requirementDetailPanel.loadForm(record);
		app.requirementMgmtPanel.requirementDetailPanel.openedFrom = 'Home';
	},

	viewCandidate : function(candidateId) {
		if (typeof (candidateId) != 'undefined' && candidateId != null && candidateId >= 0){
			var params = new Array();
			params.push(['id','=', candidateId]);
			var filter = getFilter(params);
			filter.pageSize=50;
			app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidate,this);
		}
	},

	onGetCandidate : function(data) {
		if (data.rows.length > 0) {
			app.setActiveTab(0,'li_candidates');
			app.candidateMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			app.candidateMgmtPanel.candidatePanel.loadForm(record);
			app.candidateMgmtPanel.candidatePanel.openedFrom = 'Candidate';
		}
	},
	
	showSuggested : function(requirementId) {
		var params = new Array();
    	params.push(['id','=', requirementId]);
    	var filter = getFilter(params);
    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onGetRequirements,this);
	},
	
	onGetRequirements : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = Ext.create('tz.model.Requirement', data.rows[0]);
			app.homeMgmtPanel.homeDetailPanel.requirementsGrid1.showSuggested(record);
		}
	},
	
	shareAllRequirement : function() {
		var vendors = this.store.collect('vendor');
		if (vendors.length > 1) {
			Ext.Msg.alert('Error','Multiple vendor submissions found, please select a vendor.');
			return false;
		}else if (vendors.length > 1) {
			Ext.Msg.alert('Error','Submissions are not found.');
			return false;
		}

		var contactIds = this.store.collect('contactId').toString();
		var emailIds = new Array();
		var contacts = new Array();
		if (contactIds != '') {
			contactIds = contactIds.split(',');
			for ( var i = 0; i < contactIds.length; i++) {
				var contact = ds.contactSearchStore.getById(Number(contactIds[i]));
				if (contact != null && contact.data.email != ''){
					emailIds.push(contact.data.email);
					contacts.push(contact.data.fullName);
				}
			}
		}
		
		var table = "<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>" +
					"<tr>" +
					"<th style='font-weight:bold;'>Status</th>"+
					"<th style='font-weight:bold;'>Submitted Date</th>"+
					"<th style='font-weight:bold;'>Candidate</th>"+
					"<th style='font-weight:bold;'>City & State</th>"+
					"<th style='font-weight:bold;'>Client</th>"+
					"<th style='font-weight:bold;'>Posting Title</th>"+
					"<th style='font-weight:bold;'>Posting Date</th>"+
					"<th style='font-weight:bold;'>Module</th>"+
					"<th style='font-weight:bold;'>Submitted Rate</th>" +
					"</tr>";

		for ( var i = 0; i < this.store.getCount(); i++) {
			var backGround = "FFFFFF";
			if (i%2 == 0)
				backGround = "E3FFE1";

			var record = this.store.getAt(i).data;
			table += "<tr>" +
					"<td bgcolor='#"+backGround+"'></td>" +
					"<td bgcolor='#"+backGround+"'>"+Ext.Date.format(record.submittedDate,'m/d/Y')+"</td>" +
					"<td bgcolor='#"+backGround+"'>"+record.candidate+"</td>" +
					"<td bgcolor='#"+backGround+"'>"+record.reqCityAndState+"</td>" +
					"<td bgcolor='#"+backGround+"'>"+record.client+"</td>" +
					"<td bgcolor='#"+backGround+"'>"+record.postingTitle+"</td>" +
					"<td bgcolor='#"+backGround+"'>"+Ext.Date.format(record.postingDate,'m/d/Y')+"</td>" +
					"<td bgcolor='#"+backGround+"'>"+record.module+"</td>" +
					"<td bgcolor='#"+backGround+"'>"+record.submittedRate1+'<br>'+record.submittedRate2+"</td></tr>";
		}
		table += "</table>";
		table = "Hi "+contacts+",<br><br>"+table;
		
		app.homeMgmtPanel.showSharePanel();
		app.homeMgmtPanel.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').setValue(table);
		app.homeMgmtPanel.requirementSharePanel.emailShortlisted.disable();
		app.homeMgmtPanel.requirementSharePanel.emailConfidential.disable();
		app.homeMgmtPanel.requirementSharePanel.candidates = contacts;
		app.homeMgmtPanel.requirementSharePanel.emailIds = emailIds;
		app.homeMgmtPanel.requirementSharePanel.fileName = vendors+" Submissions";
		app.homeMgmtPanel.requirementSharePanel.data = '';
		app.homeMgmtPanel.requirementSharePanel.openedFrom = 'Home';
	},
	
	shareRequirement : function(record) {
		var record = record.data;
		var params = new Array();
    	params.push(['id','=', record.requirementId]);
    	var filter = getFilter(params);
    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onShareRequirements,this);
	},
	
	onShareRequirements : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = Ext.create('tz.model.Requirement', data.rows[0]);
			record = record.data;
			app.homeMgmtPanel.requirementSharePanel.data = record;
			if (record.clientId != null && record.clientId != '') {
				var client = ds.clientSearchStore.getById(Number(record.clientId)).data.name;
			}else
				var client = '';
			
			var ids = record.vendorId.split(',').map(Number);
			var names ="";
			for ( var i = 0; i < ids.length; i++) {
				var rec = ds.vendorStore.getById(parseInt(ids[i]));
				if (rec)
					names += rec.data.name +", ";	
			}
			var vendor = names.substring(0,names.length-2);
			
			var detailsTable = "<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>"+
								"<tr><th>Posting Date </th><td>"+Ext.Date.format(new Date(record.postingDate),'m/d/Y')+'</td></tr>'+
								"<tr><th>Posting Title</th><td>" +record.postingTitle+'</td></tr>'+
								"<tr><th>City & State</th><td>" +record.cityAndState+'</td></tr>'+
								"<tr><th>Client</th><td>" +client+'</td></tr>'+
								"<tr><th>Vendor</th><td>" +vendor+'</td></tr>'+
								"<tr><th>Alert</th><td>" +record.addInfo_Todos+'</td></tr>'+
								"<tr><th>Source</th><td>" +record.source+'</td></tr>'+
								"<tr><th>Module</th><td>" +record.module+'</td></tr>'+
								"<tr><th>Skill Set</th><td>" +record.skillSet+'</td></tr>'+
								"<tr><th>Role Set</th><td>" +record.targetRoles+'</td></tr>'+
								"<tr><th>Mandatory Certifications</th><td>" +record.certifications +'</td></tr>';
	
			if(record.remote)
				detailsTable += "<tr><th>Remote</th><td>Yes</td></tr>";
			else
				detailsTable += "<tr><th>Remote</th><td>No</td></tr>" ;
		
			if(record.relocate)
				detailsTable += "<tr><th>Relocation Needed</th><td>Yes</td></tr>" ;
			else
				detailsTable += "<tr><th>Relocation Needed</th><td>No</td></tr>" ;
		
			if(record.travel)
				detailsTable += "<tr><th>Travel Needed</th><td>Yes</td></tr>" ;
			else
				detailsTable += "<tr><th>Travel Needed</th><td>No</td></tr>" ;
		
			detailsTable += "<tr><th>Ok to train Candidate</th><td>" + record.helpCandidate +'</td></tr>'+
							"<tr><th>Roles and Responsibilities</th><td><br>" +record.requirement.replace(/(?:\r\n|\r|\n)/g, '<br>')+'</td></tr>'+
							"<tr><th>Last updated by</th><td><br>" +record.lastUpdatedUser+'</td></tr>'+
							"<tr><th>Last updated on</th><td><br>" +Ext.Date.format(new Date(record.lastUpdated),'m/d/Y')+'</td></tr>'+
							'</table>';

			app.homeMgmtPanel.showSharePanel();
			app.homeMgmtPanel.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').setValue(detailsTable);
			app.homeMgmtPanel.requirementSharePanel.emailShortlisted.disable();
			app.homeMgmtPanel.requirementSharePanel.emailConfidential.enable();
			app.homeMgmtPanel.requirementSharePanel.fileName = record.id;
			app.homeMgmtPanel.requirementSharePanel.openedFrom = 'Home';
		
		}
	},
	
	getInterview : function(record) {
		var params = new Array();
    	params.push(['candidate','=', record.data.candidateId]);
    	params.push(['client','=', record.data.clientId]);
    	params.push(['vendor','=', record.data.vendorId]);
    	params.push(['requirementId','=', record.data.requirementId]);
    	var filter = getFilter(params);
    	app.interviewService.getInterviews(Ext.JSON.encode(filter),this.onGetInterview,this);
	},
	
	onGetInterview : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = Ext.create('tz.model.Interview', data.rows[0]);
			app.setActiveTab(0,'li_interviews');
			app.interviewMgmtPanel.interviewGridPanel.editInterview(record);
			app.interviewMgmtPanel.interviewDetailPanel.opendFrom = 'Home';
		}
		
	}

});

function getSubmissionBackGround(record) {
	//placements green(a7fda1), Interviews orange(ffd3a7)
	var subReq = record.data;
	if (subReq.submissionStatus == 'C-Accepted')
		return 'A7FDA1';
	if (subReq.submissionStatus == 'I-Scheduled' || subReq.submissionStatus == 'I-Succeeded' || subReq.submissionStatus == 'I-Failed' 
		|| subReq.submissionStatus == 'C-Accepted' || subReq.submissionStatus == 'C-Declined' || subReq.submissionStatus == 'I-Withdrawn' )
		return 'FFD3A7';

	//submissions in blue
	if (Ext.Date.format(subReq.submittedDate,'m/d/Y') == Ext.Date.format(new Date(),'m/d/Y')) 
		return 'C3D5F6';	
	if (Ext.Date.format(subReq.submittedDate,'m/d/Y') == Ext.Date.format(new Date(new Date().setDate(new Date().getDate() - 1)),'m/d/Y')) 
		return 'CFDCF5';	
	if (new Date(subReq.submittedDate).getTime() >= (new Date()).setDate((new Date()).getDate()-7)) 
		return 'D9E3F5';	
	if (new Date(subReq.submittedDate).getTime() >= (new Date()).setMonth((new Date()).getMonth()-1)) 
		return 'E5EBF5';	
				
	//Postings in yellow
	if (subReq.postingDate != null) {
		if (Ext.Date.format(new Date(),'m/d/y') == Ext.Date.format(subReq.postingDate,'m/d/y')) {
			return 'FFFCBC';
		}else if (Ext.Date.format(new Date((new Date()).setDate((new Date()).getDate()-1)),'m/d/y') == Ext.Date.format(subReq.postingDate,'m/d/y')) {
			return 'FAFAD0';
		}else if (subReq.postingDate.getTime() > (new Date()).setDate((new Date()).getDate()-7)) {
			return 'FFFFE0';
		}else if (subReq.postingDate.getTime() > (new Date()).setMonth((new Date()).getMonth()-1)) {
			return 'FFFFEE';
		}
	}
	return 'FFFFFF';
}