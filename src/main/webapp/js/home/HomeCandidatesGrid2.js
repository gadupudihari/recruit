Ext.define('tz.ui.HomeCandidatesGrid2', {
    extend: 'Ext.grid.Panel',
    title: 'Candidates',
    frame:true,
    width :'100%',
    listeners: {
    	edit: function(editor, event){
            var record = event.record;
			this.editFlag = true;
        	this.getView().refresh();
        },
    },

    initComponent: function() {
        var me = this;
        me.store = ds.homeCandidateStore2;
        me.editFlag = false;
        me.emailFilter ;
        me.candidates = new Array();
        me.emailIds = new Array();
        me.vendorId = null;        
        me.selModel = Ext.create('Ext.selection.CheckboxModel');
        
        me.columns = [{
          				text : 'Id',
          				dataIndex : 'id',
          				width :  40,
          		        hidden : true
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'priority',
                          text: 'Priority',
                          width : 40,
                          editor: {
                              xtype: 'textfield',
                          },
                   },{
          				text : 'Experience',
          				dataIndex : 'experience',
          				width :  50,
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
          				text : 'Communication',
          				dataIndex : 'communication',
          				width :  70,
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'fullName',
                          text: 'Candidate',
                          editor: {
                              xtype: 'textfield',
                          },
                          renderer:function(value, metadata, record){
                          	record.data.fullName = record.data.firstName+' '+record.data.lastName;
                          	return record.data.fullName;
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'initials',
                          text: 'Initials',
                          editor: {
                              xtype: 'textfield',
                          },
                          renderer:function(value, metadata, record){
                          	record.data.initials = record.data.firstName.charAt(0)+record.data.lastName.charAt(0);
                          	return record.data.initials;
                          }
                      },{
          				text : 'Resume Help',
          				width:70,
          				dataIndex : 'resumeHelp',
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'marketingStatus',
                          text: 'Marketing Status',
                          width:55,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'alert',
                          text: 'Alert',
                          width:70,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'source',
                          text: 'Source',
                          width:80,
                          editor: {
                              xtype: 'textfield',
                          },
                      	renderer:function(value, metadata, record){
                          	metadata.tdAttr = 'data-qtip="' + "<b>Source : </b>" +value +"<br><b>Referral : </b>"+record.data.referral+ '"';
                          	if (value == 'Referral' || value == 'Employee Referral')
                          		return 'R-'+record.data.referral;
                          	return value;
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'skillSet',
                          text: 'Skill Set',
                          width:120,
                          editor: {
                              xtype: 'textfield',
                          },
                          renderer:function(value, metadata, record){
                        	  var skillset = new Array();
                        	  if (record.data.skillSetIds != null  && record.data.skillSetIds != '') {
                        		  var skillSet = record.data.skillSetIds.split(',');
                        		  for ( var i = 0; i < skillSet.length; i++) {
                        			  skillset.push(ds.skillSetStore.getById(Number(skillSet[i])).data.value);
                        		  }
                        	  }
                        	  return skillset.toString() ;
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'skillSetGroup',
                          text: 'Skill Set Group',
                          width:120,
                          editor: {
                              xtype: 'textfield',
                          },
                          renderer:function(value, metadata, record){
                        	  var skillsetGroup = new Array();
                        	  if (record.data.skillSetGroupIds != null  && record.data.skillSetGroupIds != '') {
                        		  var skillSet = record.data.skillSetGroupIds.split(',');
                        		  for ( var i = 0; i < skillSet.length; i++) {
                        			  skillsetGroup.push(ds.skillsetGroupStore.getById(Number(skillSet[i])).data.name);
                        		  }
                        	  }
                        	  record.data.skillSetGroup = skillsetGroup.toString();
                        	  return skillsetGroup.toString() ;
                          }
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'allCertifications',
                          text: 'Certifications',
                          width:120,
                          editor: {
                              xtype: 'textfield',
                          },
                          renderer:function(value, metadata, record){
                            	record.data.allCertifications = value.replace(/ -- /g, ', ');
                            	return value.replace(/ -- /g, ', ');
                          }
                      },{
                    	  xtype: 'datecolumn',
          				text : 'Availability',
          				dataIndex : 'availability',
          				width :  68,
          				format:'m/d/Y',
                        editor: {
                            xtype: 'datefield',
                        },
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'emailId',
                          text: 'Email Id',
                          width:60,
                          hidden : true,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'contactNumber',
                          text: 'Contact No',
                          width:80,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'gender',
                          text: 'Gender',
                          width:55,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'p1comments',
                          text: 'P1 Comments',
                          width:90,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'p2comments',
                          text: 'P2 Comments',
                          width:90,
                          hidden : true,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'expectedRate',
                          text: 'Expected Rate',
                          width:75,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'rateFrom',
                          text: 'Rate From',
                          width:75,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'rateTo',
                          text: 'Rate To',
                          width:75,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'technicalScore',
                          text: 'Technical Score',
                          width : 40,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
          				text : 'Will Relocate',
          				dataIndex : 'relocate',
          				width :  55,
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
          				text : 'Will Travel',
          				dataIndex : 'travel',
          				width :  55,
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
          				text : 'Good Candidate for Remote',
          				dataIndex : 'goodCandidateForRemote',
          				width :  80,
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
          				text : 'Open To CTH',
          				dataIndex : 'openToCTH',
          				width :  75,
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'cityAndState',
                          text: 'City & State',
                          width:90,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
          				text : 'Location',
          				dataIndex : 'location',
          				width :  80,
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
          				text : 'Follow Up',
          				dataIndex : 'followUp',
          				width :  60,
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'personality',
                          text: 'Personality',
                          width:65,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'contactAddress',
                          text: 'Contact Address',
                          width:100,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                    	  xtype: 'datecolumn',
          				text : 'Initial Contact Date',
          				dataIndex : 'contactDate',
          				width :  68,
          				format:'m/d/Y',
                        editor: {
                            xtype: 'datefield',
                        },
          			},{
          				xtype: 'datecolumn',
          				text : 'Start Date',
          				dataIndex : 'startDate',
          				width :  68,
          				format:'m/d/Y',
                        editor: {
                            xtype: 'datefield',
                        },
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'education',
                          text: 'Education',
                          width:150,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                      	header : 'Resume Link',
                      	dataIndex : 'resumeLink',
                        editor: {
                            xtype: 'textfield',
                        },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'immigrationStatus',
                          text: 'Immigration Status',
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
          				text : 'Immigration Verified',
          				dataIndex : 'immigrationVerified',
          				width :  100,
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'type',
                          text: 'Type',
                          width:100,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'employer',
                          text: 'Employer',
                          width:80,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'highestQualification',
                          text: 'Highest Qualification Held',
                          width:120,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
          				text : 'Employment Type',
          				width:70,
          				dataIndex : 'employmentType',
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
          				text : 'Current Roles',
          				width:70,
          				dataIndex : 'currentRoles',
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
          				text : 'Target Roles',
          				width:70,
          				dataIndex : 'targetRoles',
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
          				text : 'Target Skill Set',
          				width:80,
          				dataIndex : 'targetSkillSet',
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
          				text : 'Interview Help',
          				width:80,
          				dataIndex : 'interviewHelp',
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
          				text : 'Job Help',
          				width:80,
          				dataIndex : 'jobHelp',
                        editor: {
                            xtype: 'textfield',
                        },
          			},{
                          xtype: 'gridcolumn',
                          dataIndex: 'aboutPartner',
                          text: 'About Partner',
                          width:65,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'currentJobTitle',
                          text: 'Current Job Title',
                          width:120,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'billingRange',
                          text: 'Billing Range',
                          width:120,
                          editor: {
                              xtype: 'textfield',
                          },
                      },{
                          xtype: 'gridcolumn',
                          dataIndex: 'confidentiality',
                          text: 'Confidentiality',
                          width : 45,
                          editor: {
                              xtype: 'textfield',
                          },
                      }
                  ];
        me.viewConfig = {
        	stripeRows: true,
        };
        
        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
		    listeners: {
                render: function(c) {
                    new Ext.ToolTip({
                        target: c.getEl(),
                        maxWidth: 1000,
                        dismissDelay: 60000,
                        html: 'Default : Displaying P0, P1 candidates with Marketing status "Active"' 
                    });
                }
            }
        });

        
        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [{
        		xtype:'button',
        		iconCls : 'btn-email',
        		text: 'Email',
        		tooltip:'Email all candidates',
        		tabIndex:25,
        		handler: function(){
        			me.getColumns();
        		}
        	},'-',{
        		xtype:'button',
        		iconCls : 'btn-report-excel',
        		text: 'Export to Excel',
        		tooltip: 'Export to Excel',
        		tabIndex:28,
        		handler: function(){
                	var vExportContent = me.getExcelXml();
                    document.location='data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);
                }
        	},'-',{
        		xtype:'button',
        		text: 'Mark as Sent',
        		tooltip:'Market selected candidates',
        		tabIndex:25,
        		handler: function(){
        			me.marketCandidatesConfirm();
        		}
        	},'->',me.showCount
        	]
        }];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });

        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			this.showCount.setValue(msg);
       	}, me);

        me.callParent(arguments);
        
        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);
        
    },

    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],

	getColumns : function() {
		var candidates = this.getSelectionModel().getSelection();
		if (candidates.length == 0) {
			Ext.Msg.alert('Warning','Please select atleast one candidate.');
			return false;
		}

        var hiddenColumns = new Array();
        var shownColumns = new Array();
        for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;" && this.columns[i].text != '' ) {
				if (this.columns[i].hidden )
					hiddenColumns.push({"text":this.columns[i].text,"dataIndex":this.columns[i].dataIndex});
				else
					shownColumns.push({"text":this.columns[i].text,"dataIndex":this.columns[i].dataIndex});
			}
		}
        var shownItems = [];
        var hiddenItems = [];
        var unCheckColumns = ['Candidate'];
        
        for (var i = 0; i < shownColumns.length; i++) {
        	var checked = true;
        	if (unCheckColumns.indexOf(shownColumns[i].text) != -1)
        		checked = false;
        	shownItems.push({
                boxLabel : shownColumns[i].text,
                name : shownColumns[i].dataIndex,
                checked : checked,
                id :'candidateShownCheckbox'+i
            });
		}
        for ( var i = 0; i < hiddenColumns.length; i++) {
        	hiddenItems.push({
                boxLabel : hiddenColumns[i].text,
                name : hiddenColumns[i].dataIndex,
                id :'candidateHiddenCheckbox'+i
            });
		}
        var formPanel = new Ext.form.FormPanel({
        	border : 1,
        	autoScroll : true,
        	frame : false,
            items : [{
	            padding: 20,
                xtype : 'checkboxgroup',
                fieldLabel : 'Check/Uncheck the desired columns',
                columns : 1,
                labelAlign : 'top',
                items : shownItems
            },{
	   	    	 xtype:'fieldset',
	   	    	 title : 'More',
	   	         collapsible: true,
	   	         collapsed: true,
	   	         layout: 'fit',
	   	         items :[{
	   	                xtype : 'checkboxgroup',
	   	                fieldLabel : '',
	   	                columns : 3,
	   	                labelAlign : 'top',
	   	                items : hiddenItems
	   	         }]
            }],
            buttons: [{
            	xtype : 'button',
            	text: 'Select All',
            	icon : 'images/icon_checkmark_dark.gif',
            	handler: function() {
            		for ( var i = 0; Ext.getCmp('candidateHiddenCheckbox'+i) != null ; i++) {
            			Ext.getCmp('candidateHiddenCheckbox'+i).setValue(true);
					}
            		for ( var i = 0; Ext.getCmp('candidateShownCheckbox'+i) != null ; i++) {
            			Ext.getCmp('candidateShownCheckbox'+i).setValue(true);
					}
            	}
            },'-',{
            	text: 'Deselect All',
            	icon : 'images/icon_delete.gif',
            	handler: function() {
            		for ( var i = 0; Ext.getCmp('candidateHiddenCheckbox'+i) != null ; i++) {
            			Ext.getCmp('candidateHiddenCheckbox'+i).setValue(false);
					}
            		for ( var i = 0; Ext.getCmp('candidateShownCheckbox'+i) != null ; i++) {
            			Ext.getCmp('candidateShownCheckbox'+i).setValue(false);
					}
            	}
            },'-',{
            	xtype : 'button',
            	text: 'Continue',
            	iconCls : 'btn-payment',
            	scope: this,
            	handler: this.showEmail
            }]
        });

        if (Ext.getCmp('candidateColumnsWindow') != null)
        	Ext.getCmp('candidateColumnsWindow').close();
			var myWin = Ext.create("Ext.window.Window", {
				title: 'Select Columns',
				id : 'candidateColumnsWindow',
				modal: true,
				height : 450,
				width : 800,
				layout: 'fit',
				items : [formPanel]
			});
        Ext.getCmp('candidateColumnsWindow').show();
	},

    showEmail : function() {

		var candidates = this.getSelectionModel().getSelection();
		var store = new tz.store.CandidateStore();
		store.add(candidates);
		var candidateIds = store.collect('id');
		var checkedColumns = new Array();
		for ( var i = 0; Ext.getCmp('candidateHiddenCheckbox'+i) != null ; i++) {
			if (Ext.getCmp('candidateHiddenCheckbox'+i).checked) {
				checkedColumns.push(Ext.getCmp('candidateHiddenCheckbox'+i).name);
			}
		}
		for ( var i = 0; Ext.getCmp('candidateShownCheckbox'+i) != null ; i++) {
			if (Ext.getCmp('candidateShownCheckbox'+i).checked) {
				checkedColumns.push(Ext.getCmp('candidateShownCheckbox'+i).name);
			}
		}
		if (checkedColumns.length ==0 ) {
			Ext.Msg.alert('Error',' Please select atleast one column');
			return false;
		}
        if (Ext.getCmp('candidateColumnsWindow') != null)
        	Ext.getCmp('candidateColumnsWindow').close();

		var table ="<br><br><table cellpadding='5' bgcolor='gray'>" +
			"<tr bgcolor='#ffffff'>" ;
		for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;" && this.columns[i].text != '' 
				&& checkedColumns.indexOf(this.columns[i].dataIndex) != -1 ) {
				table += "<th width='"+this.columns[i].width+"'>"+this.columns[i].text+"</th>";
			}
		}
		table += "</tr>";
		for ( var i = 0; i < candidates.length; i++) {
			var record = candidates[i];
			table += "<tr bgcolor='#ffffff'>";
			for ( var j = 0; j < this.columns.length; j++) {
				if (this.columns[j].xtype != 'actioncolumn' && this.columns[j].text != "&#160;" && this.columns[j].text != '' 
					&& checkedColumns.indexOf(this.columns[j].dataIndex) != -1 ) {
					var value = record.data[this.columns[j].dataIndex];
					if (value == null)
						table += "<td></td>";
					else if (this.columns[j].xtype == 'datecolumn')
						table += "<td>"+Ext.Date.format(value,'m/d/Y')+"</td>";
					else
						table += "<td>"+value+"</td>";
				}
			}
			table += "</tr>";
		}
	
		
        var fromEmailCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: '<a onclick="javascript:app.homeMgmtPanel.addUserEmail()" href=# title="Send Email from this email id">From</a>',
		    store: ds.userEmailStore,
		    queryMode: 'local',
		    displayField: 'emailId',
            valueField: 'emailId',
		    name : 'from',
		    editable: false,
		    forceSelection : true,
		    allowBlank: false,
		    labelAlign: 'left',
		    labelWidth : 50,
		});

        if (ds.userEmailStore.getCount() > 0)
        	fromEmailCombo.setValue(ds.userEmailStore.getAt(0).data.emailId);
        if (this.vendorId != null && ds.vendorStore.getById(this.vendorId) != null)
        	var emailTo = ds.vendorStore.getById(this.vendorId).data.email;
        
		var vendorCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Vendor',
		    queryMode: 'local',
		    name : 'vendor',
            displayField: 'name',
            valueField: 'id',
            store: ds.vendorStore,
            value : this.vendorId,
		    tabIndex:12,
		    anyMatch:true,
			listeners: {
				select: function(combo,value){
					var emailIds = new Array();
					vendorEmail = combo.store.getById(combo.getValue()).data.email;
					if (vendorEmail != null && vendorEmail != '' && vendorEmail.toLowerCase() != 'n/a'){
						var allEmailIds = emailWindow.items.items[0].items.items[0].items.items[1].items.items[0].getValue();
						if (allEmailIds != null && allEmailIds != '' ){
							allEmailIds = allEmailIds.split(',');
							emailIds.push(allEmailIds);	
						}
						emailIds.push(combo.store.getById(combo.getValue()).data.email);
						emailWindow.items.items[0].items.items[0].items.items[1].items.items[0].setValue(emailIds.toString());
						emailWindow.items.items[0].items.items[0].items.items[1].items.items[4].setValue(combo.getValue());
					}
				}
 			}//listeners
		});

        var candidatesPanel = Ext.create('Ext.form.Panel', {
        	border : 1,
        	bodyPadding: 5,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
            frame:false,
            items : [{
            	xtype:'fieldset',
            	border : 0,
            	collapsible: false,
            	layout: 'column',
            	items :[{
            		xtype: 'container',
            		layout : 'table',
            		columnWidth:1,
            		items: [fromEmailCombo,vendorCombo,
            		        {
            			xtype: 'button',
            			text : 'Clear',
                    	tooltip : 'Clear To email ids',
                    	margin: '0 0 0 10',
                    	scope: this,
                    	handler: function(){
                    		emailWindow.items.items[0].items.items[0].items.items[1].items.items[0].reset();
                    	}
                    }]
            	},{
            		xtype: 'container',
            		layout : 'fit',
            		columnWidth:1,
            		items: [{
            			xtype: 'textfield',
            			name: 'to',
            			labelAlign: 'top',
            			fieldLabel: 'To:',
            			emptyText: 'Enter email ids',
            			allowBlank: false,
            			value : emailTo
            		},{
            			xtype: 'textfield',
            			name: 'subject',
            			labelAlign: 'top',
            			fieldLabel: 'Subject:',
            			emptyText: 'Enter email Subject',
            			allowBlank: false,
            			value : 'List of Current Available Candidates'
            		},{
            			xtype: 'htmleditor',
            			height:350,
            			name: 'message',  
            			labelAlign: 'top',
            			fieldLabel: 'Body',
            			emptyText: 'Enter email Body',
            			allowBlank: false,
            			value : table
            		},{
            			xtype: 'textfield',
            			name: 'candidateIds',
            			value : candidateIds,
            			hidden: true
            		},{
            			xtype: 'textfield',
            			name: 'vendorId',
            			hidden: true,
            			value : this.vendorId
            		}]
            	}]
            }]
        });

        var panel = this;
		if (Ext.getCmp('emailRequirementWindow') == null) {
			var emailWindow = Ext.create('Ext.window.Window', {
			    title: 'Email',
			    id : 'emailRequirementWindow',
			    width: 1000,	
			    layout: 'fit',
			    modal: true,
			    autoScroll:true,
			    items: [candidatesPanel],
			    buttons: [{
			    	text: 'Send All',
		            handler: function(){
		            	if (emailWindow.items.items[0].form.isValid()) {
		            		var params = new Array();
		            		params.push(['from','=', emailWindow.items.items[0].items.items[0].items.items[0].items.items[0].getValue()]);
		            		params.push(['to','=', emailWindow.items.items[0].items.items[0].items.items[1].items.items[0].getValue()]);
		            		params.push(['subject','=', emailWindow.items.items[0].items.items[0].items.items[1].items.items[1].getValue()]);
		            		params.push(['message','=', emailWindow.items.items[0].items.items[0].items.items[1].items.items[2].getValue()]);
		            		params.push(['candidateIds','=', emailWindow.items.items[0].items.items[0].items.items[1].items.items[3].getValue()]);
		            		params.push(['vendorId','=', emailWindow.items.items[0].items.items[0].items.items[1].items.items[4].getValue()]);
			            	var filter = getFilter(params);
			            	panel.sendEmailConform(filter);
						}//if
		            }//handler
		        }]//buttons
			});
		}
		Ext.getCmp('emailRequirementWindow').show();
	},
	
	sendEmailConform : function(filter) {
		this.emailFilter = filter;
		Ext.Msg.confirm("Confirm","Do you want to send the email ?", this.sendEmail, this);
	},

	sendEmail : function(dat) {
		if (dat=='yes') {
			var filter = this.emailFilter;
			app.candidateService.sendMarketingEmail(Ext.encode(filter),this.onSendEmail,this);	
		}
	},
	
	onSendEmail : function(data) {
		if (data.success) {
			Ext.getCmp('emailRequirementWindow').close();
			Ext.Msg.alert('Success','Email sent successfully.');
			this.emailFilter = null;
		}else
			Ext.Msg.alert('Error',data.errorMessage);
	},

    getExcelExport : function() {
    	var values = app.homeMgmtPanel.homeDetailPanel.candidatesSearchPanel.getValues();
    	var params = new Array();
    	var marketingStatus = values.marketingStatus.toString();
    	while (marketingStatus.length >0) {
        	if (marketingStatus.substr(marketingStatus.length-1,marketingStatus.length) == ",")
        		marketingStatus = marketingStatus.substr(0,marketingStatus.length-1);
			else
				break;
		}
    	if (marketingStatus.search(',') != -1) 
    		marketingStatus = "'"+marketingStatus.replace(/,/g, "','")+"'";
    	
    	params.push(['marketingStatus','=', marketingStatus]);

    	params.push(['priority','=', values.priority]);
   		params.push(['relocate','=', values.relocate]);
   		params.push(['travel','=', values.travel]);
    	params.push(['search','=', values.searchCandidates]);
    	params.push(['confidentiality','=', values.confidentiality]);
    	params.push(['referral','=', values.referral]);
    	params.push(['source','=', values.source]);
    	params.push(['certifications','=', values.certifications]);
    	params.push(['cityAndState','=', values.cityAndState]);
    	params.push(['skillSetId','=', values.skillSetId]);
    	params.push(['skillSetGroupId','=', values.skillSetGroupId]);

    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;" && this.columns[i].text != '' && !this.columns[i].hidden && this.columns[i].text != "Confidentiality") {
				columns.push(this.columns[i].text + ':'+this.columns[i].width);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.candidateService.excelExport(Ext.encode(filter));
	},

	marketCandidatesConfirm : function() {
		var panel = this;
		var candidates = this.getSelectionModel().getSelection();
		if (candidates.length == 0) {
			Ext.Msg.alert('Warning','Please select atleast one candidate.');
			return false;
		}
        if (ds.userEmailStore.getCount() == 0){
        	Ext.MessageBox.show({
	            title: 'Error',
	            msg: 'Please enter your email credentials',
				buttons: Ext.Msg.OK,
				closable :true,
	            fn: function(btn) {
					if (btn == 'ok') 
						app.homeMgmtPanel.addUserEmail();
				}
	        });
        	return false;
        }
        
		var vendorCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Please select Vendor',
		    queryMode: 'local',
		    name : 'vendor',
            displayField: 'name',
            valueField: 'id',
            store: ds.vendorStore,
            value : this.vendorId,
		    tabIndex:12,
		    anyMatch:true,
		});

        var vendorPanel = Ext.create('Ext.form.Panel', {
        	bodyPadding: 5,
        	border : 0,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
            items : [{
            	xtype:'container',
   	         	layout: 'column',
   	         	items :[vendorCombo,
   	                    {
                    xtype:'button',
                    margin: '0 0 0 20',
                    text: 'OK',
                    handler: function(){
                    	panel.marketCandidates(vendorCombo.getValue());
                    }
                }]
            }]
        });

		var vendorSearchWindow = Ext.create('Ext.window.Window', {
		    title: '',
		    id : 'vendorSearchWindow',
		    modal: true,
		    bodyPadding: 0,
		    layout: 'fit',
		    items: [vendorPanel],
		});
		Ext.getCmp('vendorSearchWindow').show();
	},
	
	marketCandidates : function(vendorId) {
    	if (vendorId == null || vendorId == '') {
			Ext.Msg.alert('Error','Please select vendor.');
			return false;
		}
        if (Ext.getCmp('vendorSearchWindow') != null) 
        	Ext.getCmp('vendorSearchWindow').close();
    	var emailTo = ds.vendorStore.getById(Number(vendorId)).data.email;		
    	if (emailTo == null || emailTo == '') {
			Ext.Msg.alert('Error','Email id does not exist for the selected vendor.');
			return false;
		}
		var candidates = this.getSelectionModel().getSelection();
		var store = new tz.store.CandidateStore();
		store.add(candidates);
		var candidateIds = store.collect('id');

		var params = new Array();
		params.push(['from','=', ds.userEmailStore.getAt(0).data.emailId]);
		params.push(['to','=', emailTo]);
		params.push(['candidateIds','=', candidateIds.toString()]);
		params.push(['vendorId','=', vendorId]);
    	var filter = getFilter(params);
    	app.candidateService.marketCandidates(Ext.encode(filter),this.onMarketCandidates,this);
	},
	
	onMarketCandidates : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Marked selected candidates successfully.');
		}else
			Ext.Msg.alert('Error',data.errorMessage);
	}
	
});

