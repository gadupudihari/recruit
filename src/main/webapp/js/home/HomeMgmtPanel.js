//source https://gist.github.com/aghuddleston/2770422#file-panelheaderextraicons-js
Ext.define('Ext.ux.panel.header.ExtraIcons', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.headericons',
	alternateClassName: 'Ext.ux.PanelHeaderExtraIcons',
 
	iconCls: '',
	index: undefined,
 
	headerButtons: [],
 
	init: function(panel) {
		this.panel = panel;
		this.callParent();
		panel.on('render', this.onAddIcons, this, {single: true});
	},
 
	onAddIcons :function () {
		if (this.panel.getHeader) {
			this.header = this.panel.getHeader();
		} else if (this.panel.getOwnerHeaderCt) {
			this.header = this.panel.getOwnerHeaderCt();
		}
		this.header.insert(this.index || this.header.items.length, this.headerButtons);
	}
});

Ext.define('WFP.view.component.GroupingSummaryWithTotal', {
	  extend: 'Ext.grid.feature.GroupingSummary',
	  alias: 'feature.groupingsummarytotal',
	  getTableFragments: function () {
	    return {
	      closeRows: this.closeRows
	    };
	  },
	  closeRows: function () {
	    return '</tpl>{[this.recursiveCall ? "" : this.printTotalRow()]}';
	  }, 
	  getFragmentTpl: function () {
	    var me = this,
	      fragments = me.callParent();
	    me.totalData = this.generateTotalData();
	    Ext.apply(fragments, {
	      printTotalRow: Ext.bind(this.printTotalRow, this)
	    });
	    Ext.apply(fragments, {
	      recurse: function (values, reference) {
	        this.recursiveCall = true;
	        var returnValue = this.apply(reference ? values[reference] : values);
	        this.recursiveCall = false;
	        return returnValue;
	      }
	    });
	    return fragments;
	  },
	  printTotalRow: function () {
	    var inner = this.view.getTableChunker().metaRowTpl.join(''),
	      prefix = Ext.baseCSSPrefix;
	    inner = inner.replace(prefix + 'grid-row', prefix + 'grid-row-summary');
	    inner = inner.replace('{{id}}', '{gridSummaryValue}');
	    inner = inner.replace(this.nestedIdRe, '{id$1}');
	    inner = inner.replace('{[this.embedRowCls()]}', '{rowCls}');
	    inner = inner.replace('{[this.embedRowAttr()]}', '{rowAttr}');
	    inner = Ext.create('Ext.XTemplate', inner, {
	      firstOrLastCls: Ext.view.TableChunker.firstOrLastCls
	    });
	    return inner.applyTemplate({
	      columns: this.getTotalData()
	    });
	  },
	  getTotalData: function () {
	    var me = this,
	      columns = me.view.headerCt.getColumnsForTpl(),
	      i = 0,
	      length = columns.length,
	      data = [],
	      active = me.totalData,
	      column;
	    for (; i < length; ++i) {
	      column = columns[i];
	      column.gridSummaryValue = this.getColumnValue(column, active);
	      data.push(column);
	    }
	    return data;
	  },
	  generateTotalData: function () {
	    var me = this,
	      data = {},
	      store = me.view.store,
	      columns = me.view.headerCt.getColumnsForTpl(),
	      i = 0,
	      length = columns.length,
	      fieldData,
	      key,
	      comp;
	    for (i = 0, length = columns.length; i < length; ++i) {
	      comp = Ext.getCmp(columns[i].id);
	      if(comp.summaryType=='header'){
	    	  data[comp.id] ="Net Profit";
	      }else{
	    	  data[comp.id] = me.getSummary(store, comp.summaryType, comp.dataIndex, false);
	      }
	    }
	    return data;
	  },
      collapseAll: function() {
    	  var self = this, groups = this.view.el.query('.x-grid-group-body'); 
    	  Ext.Array.forEach(groups, function (group) {         
    		  self.collapse(Ext.get(group.id));     
    	  }); 
      },

      expandAll: function() {
    	  var self = this, groups = this.view.el.query('.x-grid-group-body');    
    	  Ext.Array.forEach(groups, function (group) {     
    		  self.expand(Ext.get(group.id));     
    	  }); 
      },
     
	});

Ext.define('tz.ui.HomeMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    border:false,
    //height:'590',
 
    initComponent: function() {
        var me = this;
        me.setHeight = true;
        me.homeDetailPanel = new tz.ui.HomeDetailPanel({manager:me});
        me.messagesGrid = new tz.ui.MessagesGrid({manager:me});
        me.requirementSharePanel = new tz.ui.RequirementSharePanel({manager:me});
        me.workListGrid = new tz.ui.WorkListGrid({manager:me});
        me.quickAddPanel = new tz.ui.QuickAddPanel({manager:me});
        
        //me.showWorkingList();
        
        me.items = [{
		       	 xtype: 'panel',
		    	 layout:'anchor',
		    	 height : '100%',
		    	 autoScroll:true,
		    	 border:0,
		    	 items:[me.homeDetailPanel]
			},{
		       	 xtype: 'panel',
		    	 layout:'anchor',
		    	 height : '100%',
		    	 autoScroll:true,
		    	 border:0,
		    	 items:[me.requirementSharePanel]
			}];
        me.callParent(arguments);
    },
    
    initScreen: function(){
    	this.getLayout().setActiveItem(0);
		this.homeDetailPanel.searchMessages();

    	ds.quickAccessStore.clearFilter();
    	ds.quickAccessStore.loadByCriteria();

    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.contractorsStore.loadByCriteria(filter);
    	ds.contactSearchStore.loadByCriteria(filter);
    	ds.client_vendorStore.loadByCriteria(filter);
    	ds.userEmailStore.loadByCriteria(filter);
		ds.skillsetGroupStore.loadByCriteria();
    	
    	if(ds.candidateSearchStore.getCount() == 0)
    		ds.candidateSearchStore.loadByCriteria(filter);
    	
    	var params = new Array();
    	params.push(['type','=', 'Client']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.clientSearchStore.loadByCriteria(filter);

    	var params = new Array();
    	params.push(['type','=', 'Vendor']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.vendorStore.loadByCriteria(filter);
    	
    	ds.userStore.loadByCriteria(filter);
    	var params = new Array();
    	params.push(['authority','=', 'RECRUITER']);
    	var filter = getFilter(params);
    	ds.recruiterStore.loadByCriteria(filter);

    	//from job openings tab
		ds.clientTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Client Type'])));
    	ds.employerStore.loadByCriteria(getFilter(new Array(['type','=', 'Employer'])));
		ds.researchStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Research Status'])));
		ds.hotnessStore.loadByCriteria(getFilter(new Array(['type','=', 'Hotness'])));
		ds.jobOpeningStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Job Opening Status'])));
		ds.submissionStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Submission Status'])));
    	ds.contactTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Contact Type'])));
    	ds.smeTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'SME Type'])));
    	ds.contactRoleStore.loadByCriteria(getFilter(new Array(['type','=', 'Contact Role'])));

    	//in candidates tab
		ds.marketingStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Marketing Status'])));
		ds.qualificationStore.loadByCriteria(getFilter(new Array(['type','=', 'Qualification'])));
    	ds.candidateTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Type'])));
    	ds.immigrationTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Immigration'])));
    	ds.employmentTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Employment Type'])));
    	ds.certificationStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Status'])));
    	ds.certificationSponsoredStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Sponsored'])));
    	ds.candidateSourceStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Source'])));
    	ds.communicationStore.loadByCriteria(getFilter(new Array(['type','=', 'Communication'])));
    	ds.personalityStore.loadByCriteria(getFilter(new Array(['type','=', 'Personality'])));
    	ds.experienceStore.loadByCriteria(getFilter(new Array(['type','=', 'Experience'])));
    	ds.targetRolesStore.loadByCriteria(getFilter(new Array(['type','=', 'Target Roles'])));
    	ds.certificationsStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Name'])));
    	
    	var params = new Array();
    	params.push(['type','=', 'Skill Set/Target Skill Set']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.skillSetStore.loadByCriteria(filter);
	},
	
	setHeights : function() {
    	this.getLayout().setActiveItem(0);
    	var browserHeight = app.mainViewport.height;
		var headerHt = app.mainViewport.items.items[0].getHeight();
		this.homeDetailPanel.messagesGrid.setHeight(browserHeight - headerHt - 50);
		this.homeDetailPanel.messageTabsGrid.setHeight(browserHeight - headerHt - 50);
		this.homeDetailPanel.requirementsGrid1.setHeight((browserHeight - headerHt - 50)/2);
		this.homeDetailPanel.requirementsGrid2.setHeight((browserHeight - headerHt - 50)/2);
		this.homeDetailPanel.candidatesGrid.setHeight(browserHeight - headerHt - 50);
		this.homeDetailPanel.contactGridPanel.setHeight(browserHeight - headerHt - 50);
		this.homeDetailPanel.smeGridPanel.setHeight(browserHeight - headerHt - 50);
	},
	
	editRequirement : function(requirementId) {
		var params = new Array();
    	params.push(['id','=', requirementId]);
    	var filter = getFilter(params);
    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onGetRequirements,this);
	},
	
	onGetRequirements : function(data) {
		if (data.success && data.rows.length > 0) {
			app.setActiveTab(0,'li_requirements');
			app.requirementMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Requirement', data.rows[0]);
			app.requirementMgmtPanel.requirementDetailPanel.loadForm(record);
		}
	},
	
	addUserEmail : function() {
		
        var smtp = new Ext.form.field.Text({
			 name: 'smtp',
			 fieldLabel: 'SMTP',
			 readOnly : true,
			 allowBlank: false,
        });
        var portNo = new Ext.form.field.Number({
			 name: 'portNo',
			 fieldLabel: 'Port No',
			 readOnly : true,
			 allowBlank: false,
       });
        var domineCombo = Ext.create('Ext.form.ComboBox', {
        	fieldLabel: 'Mail server',
        	store: ds.domineStore,
        	queryMode: 'local',
        	displayField: 'value',
        	valueField: 'name',
        	name : 'domain',
        	editable: false,
        	allowBlank: false,
        	forceSelection : true,
        	listeners: {
        		change: function(combo,value){
        			emailPanel.form.findField('smtp').setReadOnly(true);
        			emailPanel.form.findField('portNo').setReadOnly(true);
        			if (combo.value == 'Gmail'){
        				emailPanel.form.findField('smtp').setValue('smtp.gmail.com');
        				emailPanel.form.findField('portNo').setValue(465);
        			}else if (combo.value == 'Yahoo'){
        				emailPanel.form.findField('smtp').setValue('smtp.mail.yahoo.com');
        				emailPanel.form.findField('portNo').setValue(465);
        			}else if (combo.value == 'Outlook'){
        				emailPanel.form.findField('smtp').setValue('smtp.live.com');
        				emailPanel.form.findField('portNo').setValue(587);
        			}else if (combo.value == 'Other'){
        				emailPanel.form.findField('smtp').reset();
        				emailPanel.form.findField('portNo').reset();
        				emailPanel.form.findField('smtp').setReadOnly(false);
        				emailPanel.form.findField('portNo').setReadOnly(false);
        			}
        			emailPanel.doLayout();
        		}
        	}
        });
		
		
        var emailPanel = Ext.create('Ext.form.Panel', {
        	title : '',
        	bodyPadding: 10,
        	border : 0,
        	frame:false,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 70},
        	xtype: 'container',
        	dockedItems :  [{
	        	 xtype: 'toolbar',
	        	 dock: 'top',
	        	 items: [{
	        		 xtype: 'button',
	        		 text: 'Save',
	        		 iconCls : 'btn-save',
	        		 scope: this,
	        		 handler: function(){
	        			 if (emailPanel.form.isValid()) {
		        			 var itemslength = emailPanel.form.getFields().getCount();
		        			 var emailObj = {};
		        			 for ( var i = 0; i < itemslength; i++) {
		        				 var fieldName = emailPanel.form.getFields().getAt(i).name;
		        				 emailObj[fieldName] = emailPanel.form.findField(fieldName).getValue();
		        			 }
		        			 this.saveEmailDetails(emailObj);
						}
	        		 }
	        	 },'-',{
	        		 xtype: 'button',
	        		 text: 'Reset',
	        		 scope: this,
	        		 handler: function(){
	        			 emailPanel.form.reset();
                     }
	        	 }]
            }],
            items:[{
            	xtype : 'fieldset',
            	border : 0,
            	layout : 'column',
            	items : [{
            		xtype: 'container',
            		columnWidth:.9,
            		items: [{
            			xtype: 'textfield',
            			name: 'emailId',
            			fieldLabel: 'Email Id',
            			emptyText: 'Ex: abc@gmail.com',
            			allowBlank: false,
            			vtype: 'email'
            		},{
            			xtype:'textfield',
            			inputType : 'password',
            			fieldLabel: 'Password *',
            			name: 'password',
            			allowBlank:false,
            		},
            		domineCombo,smtp,portNo
            		]
            	}]
            }]
        });

        var emailWindow = Ext.create('Ext.window.Window', {
        	title: 'Email',
        	id : 'emailUserWindow',
        	width: 300,
        	modal: true,
        	layout: 'fit',
        	items: [{
        		xtype: 'form',
        		items: [emailPanel]
        	}],
        });
		Ext.getCmp('emailUserWindow').show();
	},
	
	saveEmailDetails : function(emailObj) {
		app.settingsService.saveEmailDetails(Ext.JSON.encode(emailObj),this.onSaveEmailDetails,this);
	},
	
	onSaveEmailDetails : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved successfully.');
			Ext.getCmp('emailUserWindow').hide();
			ds.userEmailStore.loadByCriteria();
		}else
			Ext.Msg.alert('Success',data.errorMessage);
		
	},
	
	showSnapShot : function(refId, type) {
        ds.quickAccessStore.filterBy(function(rec) {
        	return rec.get('type') === type; 
        });

		var record = ds.quickAccessStore.getAt(ds.quickAccessStore.findExact('referenceId',refId.toString()));
		
		app.showSnapshot(record);
	},
	
	removeEmailId : function(emailId) {
		//Ext.getCmp('jobEmailPanel').remove(Ext.getCmp(id),true);
		Ext.getCmp(emailId).hide();
		this.emailIds.splice(this.emailIds.indexOf(emailId), 1);
		this.candidates.splice(this.emailIds.indexOf(emailId), 1);
	},
	
	showSharePanel : function() {
		this.getLayout().setActiveItem(1);
		this.requirementSharePanel.form.reset();
	},
	
	showWorkingList : function() {
    	var browserHeight = app.mainViewport.height;
    	var browserWidth = app.mainViewport.width;
    	this.workListGrid.setHeight(browserHeight-150);
    	var windowXPosition = app.windowXPosition;
    	app.windowXPosition += 150;
    	var windowYPosition = app.windowYPosition;
        var emailWindow = Ext.create('Ext.window.Window', {
        	title: 'Working List',
			height : browserHeight-100,
			width: browserWidth/2,
			modal: false,
			minimizable: true,
        	layout: 'fit',
			listeners: {
				"minimize": function (window, opts) {
					window.collapse();
					window.setWidth(150);
					window.alignTo(Ext.getBody(), 'bl-bl',[windowXPosition,windowYPosition]);
				},
				"close": function (window, opts) {
					app.arrangeWindows();
				}
			},
			tools: [{
				type: 'prev',
				handler: function (evt, toolEl, owner, tool) {
					var window = owner.up('window');
					window.alignTo(Ext.getBody(), 'tl-tl',[0,90],true);
				}
			},{
				type: 'next',
				handler: function (evt, toolEl, owner, tool) {
					var window = owner.up('window');
					window.alignTo(Ext.getBody(), 'tr-tr',[0,90],true);                
				}
			},{
				type: 'restore',
				handler: function (evt, toolEl, owner, tool) {
					var window = owner.up('window');
					window.setWidth(browserWidth/2);
					window.expand('', false);
					window.center();
				}
			}],
        	items: [{
        		xtype: 'form',
        		items: [this.workListGrid]
        	}],
        });
        
        emailWindow.show();
	},
	
	viewCandidate : function(candidateId) {
		if (typeof (candidateId) != 'undefined' && candidateId != null && candidateId >= 0){
			var params = new Array();
			params.push(['id','=', candidateId]);
			var filter = getFilter(params);
			filter.pageSize=50;
			app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidate,this);
		}
	},

	onGetCandidate : function(data) {
		if (data.rows.length > 0) {
			app.setActiveTab(0,'li_candidates');
			app.candidateMgmtPanel.getLayout().setActiveItem(1);
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			app.candidateMgmtPanel.candidatePanel.loadForm(record);
			app.candidateMgmtPanel.candidatePanel.openedFrom = 'Candidate';
		}
	},
	
	addContact : function(type) {
		var manager = this;
		if (Ext.getCmp('addContact') == null) {
			var contactWindow = Ext.create('Ext.window.Window', {
			    title: 'Add Contact',
			    id : 'addContact',
			    width: 320,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
		   	                 	xtype:'textfield',
		   	                 	fieldLabel: 'First Name *',
		   	                 	name: 'firstName',
		   	                 	allowBlank:false,
		   	                 	maxLength:45,
		   	                 	enforceMaxLength:true,
		   	                 	tabIndex:501,
		   	             	},{
		   	             		xtype:'textfield',
		   	             		fieldLabel: 'Last Name *',
		   	             		name: 'lastName',
								allowBlank:false,
								tabIndex:502,
								maxLength:45,
								enforceMaxLength:true,
		   	             	},{
		   	             		xtype:'textfield',
		   	             		fieldLabel: 'Email',
		   	             		name: 'email',
		   	             		//allowBlank:false,
		   	             		vtype:'email',
		   	             		tabIndex:503,
		   	             		maxLength:45,
		   	             		enforceMaxLength:true,
		   	             	},{
		   		                xtype: 'combobox',
		   		                fieldLabel: 'Client/Vendor',
		   		                name : 'clientId',
		   		                queryMode: 'local',
		   		                anyMatch:true,
		   		                triggerAction: 'all',
		   		                displayField: 'name',
		   		                valueField: 'id',
		   		                emptyText:'Select...',
		   		                listClass: 'x-combo-list-small',
		   		                store: ds.client_vendorStore,
		   		                tabIndex:504,
		   		            },{
		   	             		xtype:'textfield',
		   	             		fieldLabel: 'Job Title',
		   	             		name: 'jobTitle',
		   	             		maxLength:45,
		   	             		enforceMaxLength:true,
		   	             		tabIndex:505,
		   	             	},{
		   	             		xtype:'displayfield',
		   	             		fieldLabel: 'Type',
		   	             		name: 'type',
		   	             		value : type
		   	             	}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
			    	tooltip:'Save Contact',
			    	tabIndex:506,
		            handler: function(){
		            	var contact = {};
		            	for ( var i = 0; i < 6; i++ ) {
		            		var component = Ext.getCmp('addContact').items.items[0].items.items[0].items.items[0].items.items[i];
		            		if(! component.isValid()){
		            			Ext.Msg.alert('Error','Please fix the errors.');
		            			return false;
		            		}else{
		            			contact[component.name] = component.value;
		            		}
						}
		    			var clientObj = {};
		    			var client = '';

		            	if (contact.clientId != null && contact.clientId != '' && contact.clientId != 0) {
		    				clientObj['id'] = contact.clientId;
		    				clientObj = Ext.JSON.encode(clientObj);
		    				client = ',"client":' + clientObj;
						}
		            	delete contact.clientId;
		            	contact = Ext.JSON.encode(contact);
		            	contact = contact.substring(1,contact.length-1);
		            	contact = '{' + contact + client + '}';
		            	app.contactService.saveContact(contact,manager.onAddContact,manager);
		            }
		        }]
			});
		}
		Ext.getCmp('addContact').show();
	},
	
	onAddContact : function(data) {
		Ext.getCmp('addContact').close();
    	params = new Array();
    	filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.contactSearchStore.loadByCriteria(filter);
	},
	
	showQuickAdd : function() {
		var quickAddPanel = new tz.ui.QuickAddPanel();
		if (Ext.getCmp('quickAddWindow') == null) {
			var snapshotWindow = Ext.create('Ext.window.Window', {
				width : 500,
				id : 'quickAddWindow',
				modal: true,
				layout: 'fit',
				border : 1,
				items: [quickAddPanel],
			});
		}
		snapshotWindow.show();
		quickAddPanel.candidatePanel.form.findField('firstName').focus('', 10);
	}
	
});

