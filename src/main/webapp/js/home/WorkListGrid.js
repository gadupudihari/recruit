Ext.define('tz.ui.WorkListGrid', {
    extend: 'Ext.grid.Panel',
    title: '',
    frame:false,
    width :'100%',
    //split: true,
	//region: 'west', 
	//collapsible: true,
    initComponent: function() {
        var me = this;
        me.store = ds.workListStore;
        me.modifiedIds = new Array();
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :35,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'View Requirement',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					this.showRequirement(rowIndex);
    				}
    			}],
                renderer:function(value, metadata, record){
                	me.columns[0].items[0].tooltip = 'View job '+record.data.requirementId ;
	            }
            },{
		        dataIndex: 'userId',
		        text: 'User',
                editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                forceSelection:true,
	                displayField: 'fullName',
	                valueField: 'id',
	                emptyText:'Select...',
	    		    store : ds.userStore,
	                lazyRender: true,
	            },
	            renderer:function(value, metadata, record){
                	var rec = ds.userStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="'+rec.data.fullName+'"';
                		return '<a href=# style="color: #000000"> ' + rec.data.fullName + '</a>';
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
		    },{
		        xtype: 'gridcolumn',
		        dataIndex: 'assignedUserId',
		        text: 'Assigned to',
                editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                forceSelection:true,
	                displayField: 'fullName',
	                valueField: 'id',
	                emptyText:'Select...',
	    		    store : ds.userStore,
	                lazyRender: true,
	            },
	            renderer:function(value, metadata, record){
                	var rec = ds.userStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="'+rec.data.fullName+'"';
                		return '<a href=# style="color: #000000"> ' + rec.data.fullName + '</a>';
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
		    },{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                renderer: renderValue,
                width:150,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:250,
   	                enforceMaxLength:true,
                }
            },{
				text : 'Job Id',
				dataIndex : 'requirementId',
				width :  60,
			},{
		        xtype: 'gridcolumn',
		        dataIndex: 'hotness',
		        text: 'Hotness',
		        width :  45,
                renderer:function(value, metadata, record){
                	if (value == 0) {
                		metadata.style = "color:#009AD0;";
					}
                	var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.value.substring(rec.data.value.indexOf('.')+1)+ '"';
                		return rec.data.value.substring(rec.data.value.indexOf('.')+1);
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
	            }
		    },{
    			xtype: 'datecolumn',
				text : 'Posting Date',
				dataIndex : 'postingDate',
				width :  60,
				format: "m/d/Y h:i:s A",
			},{
    			text : 'Job Opening Status',
    			dataIndex : 'jobOpeningStatus',
    			renderer: renderValue,
    			width :  60,
    		},{
		        xtype: 'gridcolumn',
		        dataIndex: 'postingTitle',
		        text: 'Posting Title',
		        renderer: renderValue,
		    },{
                xtype: 'gridcolumn',
                dataIndex: 'module',
                text: 'Module',
                renderer: renderValue,
                width :  60,
            },{
				header : 'Client',
				dataIndex : 'clientId',
	            width :  120,
	            renderer:function(value, metadata, record){
                	var rec = ds.clientSearchStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="'+rec.data.name+'"';
                		return '<a href=# style="color: #000000"> ' + rec.data.name + '</a>';
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
				header : 'Vendor',
				dataIndex : 'vendorId',
	            width :  120,
	            renderer:function(value, metadata, record){
	            	if (value != null && value != '') {
						var ids = value.toString().split(',');
						var names ="";
						for ( var i = 0; i < ids.length; i++) {
							var rec = ds.vendorStore.getById(parseInt(ids[i]));
							if (rec)
								names += rec.data.name +", ";	
						}
                		metadata.tdAttr = 'data-qtip="'+names.substring(0,names.length-2)+'"';
                		if (ids.length > 1)
                			return '<a href=# style="color: #000000"> ' + '('+ids.length+') '+ names.substring(0,names.length-2) + '</a>';
                		return '<a href=# style="color: #000000"> ' + names.substring(0,names.length-2) + '</a>';
					}
	            	return 'N/A';
                }
			},{
				text : "Alert",
				dataIndex : 'addInfoTodos',
				width :  80,
  				renderer:function(value, metadata, record){
  					metadata.tdAttr = 'data-qtip="' + value + '"';
  				    if (value == null || value=="") 
  				    	return 'N/A' ;
  				    return '<span style="color:red;">' + value + '</span>';
  				}
			},{
				text : 'Location',
				dataIndex : 'location',
				width :  80,
				renderer: renderValue,
			},{
		        xtype: 'gridcolumn',
		        dataIndex: 'cityAndState',
		        text: 'City & State',
		        renderer: renderValue,
		    },{
                xtype: 'gridcolumn',
                dataIndex: 'skillSet',
                text: 'Skill Set',
                width:250,
                renderer: renderValue,
            },{
				header : 'Candidate',
				dataIndex : 'candidateId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != null && value != '') {
                		var ids = value.toString().split(',');
    					var names ="",tooltip='';
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contractorsStore.getById(parseInt(ids[i]));
    						if (rec){
    							names += rec.data.fullName+", ";	
    							tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
    						}
    					}
    					metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
    					return '('+ids.length+') '+ names.substring(0,names.length-2);
                	}
                	return 'N/A';
                }
			},{
    			text : 'Resume',
    			dataIndex : 'resume',
    			width :  60,
                renderer:function(value, metadata, record){
                	if (value != null) {
    					metadata.tdAttr = 'data-qtip="' + value + '%"';
    					return value+'%';
					}
                	return 'N/A';
                }
    		},{
				text : 'Created User',
				dataIndex : 'createdUser',
				hidden :true,
				width:80,
				renderer:function(value, metadata, record){
               		metadata.tdAttr = 'data-qtip="' + value + '"';
                	return value;
                }
			},{
				text : 'Last Updated User',
				dataIndex : 'lastUpdatedUser',
				hidden :true,
				width:80,
				renderer:function(value, metadata, record){
               		metadata.tdAttr = 'data-qtip="' + value + '"';
                	return value;
                }
			},{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created On',
                format: "m/d/Y H:i:s A",
                width:100,
                hidden :true,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated On',
                width:100,
                hidden :true,
            }
        ];
        
        me.viewConfig = {
        		stripeRows: false,
        		style: {overflow:'auto', overflowX: 'hidden'},
        		
            	getRowClass: function (record, rowIndex, rowParams, store) {
					return 'row-font12';
            	},
        };

		me.hotJobsCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Hotness',
		    store : ds.hotnessStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'hotJobs',
		    value : ['00','01'],
		    tabIndex:1,
		    labelWidth :50,
		    width :220,
 			listeners: {
 				specialkey: function(f,e){
 					if(e.getKey()==e.ENTER){
 						me.search();
 					}
 				}//specialkey
 			}//listeners
		});

        me.dockedItems = [
            {
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Save',
            		iconCls : 'btn-save',
            		tabIndex:15,
            		handler: function(){
            			me.saveWorkingList()
            		}
            	},'-',{
            		text : 'Search',
            		iconCls : 'btn-Search',
            		tabIndex:4,
            		handler: function(){
            			me.search();
            		}
            	},'-',{
            		text : 'Reset',
            		tabIndex:5,
            		handler: function(){
            			me.hotJobsCombo.reset();
            			me.search();
            		}
            	}]
          }
      ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        ds.contractorsStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.clientSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        ds.hotnessStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        me.callParent(arguments);
        
        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);

    },
    
    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],
    
    search : function() {
    	var params = new Array();
    	var filter = getFilter(params);
    	this.store.loadByCriteria(filter);
	},
	
	showRequirement : function(rowIndex) {
		var record = this.store.getAt(rowIndex);
		app.setActiveTab(0,'li_requirements');
		app.requirementMgmtPanel.getLayout().setActiveItem(1);
		app.requirementMgmtPanel.requirementDetailPanel.loadForm(record);
		app.requirementMgmtPanel.requirementDetailPanel.openedFrom = 'Home';
	},

	saveWorkingList : function() {
    	var records = new Array();
    	for ( var i = 0; i < this.store.data.length ; i++) {
			var record = this.store.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}
    		if (this.modifiedIds.indexOf(record.data.id) != -1)
    			records.push(record.data);	
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated records to save');
    		return;
		}else{
			for ( var i = 0; i < records.length; i++) {
				var record = records[i];
	    		if (record.userId == null || record.userId == ""){
	    			Ext.Msg.alert('Error','Pealse select user');
	    			return false;
	    		}else if (record.assignedUserId == null || record.assignedUserId == "") {
	    			Ext.Msg.alert('Error','Please select Assigned to user');
	    			return false;
				}else if (record.requirementId == null || record.requirementId == "") {
	    			Ext.Msg.alert('Error','Please select a job');
	    			return false;
				}
			}
	    	records = Ext.JSON.encode(records);
	    	app.requirementService.saveWorkList(records,this.onSave,this);
		}
	}
	
});
