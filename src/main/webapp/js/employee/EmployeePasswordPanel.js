Ext.define('tz.ui.EmployeePasswordPanel', {
    extend: 'tz.ui.BaseFormPanel',
    //frame:true,
    bodyPadding: 10,
    title: 'Change Password',
    anchor:'10%',
    width:'10%',
    height :'10%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},    
    
    
    initComponent: function() {
        var me = this;
     
        me.items = [this.getMessageComp(),this.getHeadingComp(),
            {
        		xtype:'fieldset',
        		title: '',
  	        	collapsible: false,
  	        	layout: 'column',
  	        	items :[{
	   	             xtype: 'container',
	   	             columnWidth:.5,
                items: [{
		   	            	xtype:'textfield',
		  	                 inputType : 'password',
		  	                 fieldLabel: 'Current Password *',
		  	                 name: 'oldPassword',
		  	                 allowBlank:false,
		  	                 tabIndex:1
		  	             },{
		   	                 xtype:'textfield',
		   	                 inputType : 'password',
		   	                 fieldLabel: 'New Password *',
		   	                 name: 'newPassword',
		   	                 allowBlank:false,
		   	                 minLength : 6,
		   	                 maxLength : 25,
		   	                 tabIndex:2
		   	             },{
		   	            	xtype:'textfield',
		   	                 inputType : 'password',
		   	                 fieldLabel: 'Confirm Password *',
		   	                 name: 'confirmPassword',
		   	                 minLength : 6,
		   	                 maxLength     : 25,
		   	                 allowBlank:false,
		   	                 tabIndex:3,
		   	                 validator: function() {
		   	                	 var pass1 = me.form.findField('newPassword').getValue();
		   	                	 var pass2 = me.form.findField('confirmPassword').getValue();

			                     if (pass1 == pass2)
			                    	 return true;
			                     else 
			                    	 return "Passwords do not match!";
			               }
		   	             },
                ]
  	        	}]
            }
            
        ];

        this.tbar =  [ {
            xtype: 'button',
            text: 'Save',
            iconCls : 'btn-save',
            tabIndex:4,
            scope: this,
            handler: this.savePassword
        }, '-', {
            xtype: 'button',
            text: 'Reset',
            iconCls : 'btn-refresh',
            tabIndex:5,
            scope: this,
            handler: this.reset
        } ];
        

        me.callParent(arguments);
    },
    
    reset: function(){
    	this.clearMessage();
    	this.form.findField('oldPassword').reset();
    	this.form.findField('newPassword').reset();
    	this.form.findField('confirmPassword').reset();  	
	},
	
	initScreen: function(){
		// events to register
        this.addEvents ({
            'iconlogoutclicked'      : true   // simple sample of a logout event
        });
	},
	
	savePassword: function(){
		this.clearMessage();
		var userFieldObj = {};
		var newPass = this.form.findField('newPassword').getValue();
		var confirmPass =this.form.findField('confirmPassword').getValue();
		var oldPass = this.form.findField('oldPassword').getValue();
		
		if(! this.form.isValid()){
				this.updateError('Required fields cannot be blank');
				return false;
		}
		if(oldPass == newPass ){
			this.updateError('Current Password and New Password should not be same');
					return false;
		}
		if(newPass.length<6 || confirmPass.length<6){
				this.updateError('Must have atleast 6 characters');
				return false;
		}
		
		if(newPass != confirmPass ){
			this.updateError('Passwords do not match');
					return false;
		}
		
		
		userFieldObj['password'] = newPass;
		userFieldObj['oldPassword'] = oldPass;
		userFieldObj = Ext.JSON.encode(userFieldObj);			
		var employee = userFieldObj ;
		app.employeeService.savePassword(employee, this.onSavePassword, this);
			
	},		
	
	onSavePassword: function(data){
		if(!data.success){
			this.updateError("The Current Password you gave is incorrect.");
			this.form.findField('oldPassword').setValue('');
		}else{
			//this.getForm().findField('password').setValue(data.returnVal.id);
			this.updateMessage('Password Changed Successfully');
	        this.accessLogout();
		}
	},
	
	 accessLogout : function ( ) {
	        // show dialog with confirmation
		 Ext.Msg.show({
	            title     : 'Logout',
	            msg       : 'Password Changed successfully, please login with your new password, Click Ok to Logout.',
	            buttons   : Ext.Msg.OK,
	            fn        : this.processLogout,
	            icon      : Ext.MessageBox.INFORMATION,
	            closable:false
	        });  
	    },
	 
	    processLogout : function ( btn) {
	    	if (btn == 'ok' ) self.location = '/recruit/j_spring_security_logout';  // put here your own redirect, this is going to the root page
	    	self.location = '/recruit/j_spring_security_logout'; 
	    }
	
});


 