Ext.define('tz.ui.UserGridPanel', {
    extend: 'Ext.grid.Panel',
    title: 'Users',
    frame:true,
    anchor:'70%',
    height:540,
    width :'100%',
    
    initComponent: function() {
        var me = this;
        me.store = ds.userStore;
        
        me.columns = [
            {
                xtype: 'actioncolumn',
                width :40,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'View User',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					app.userMgmtPanel.showUser(rowIndex);
    				}
    			}]
            },{
                xtype: 'gridcolumn',
                dataIndex: 'firstName',
                text: 'First Name',
                renderer: userRender,
                width :120,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'lastName',
                text: 'Last Name',
                renderer: userRender,
                width :120,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'emailId',
                text: 'EmailID',
                renderer: userRender,
                width:200,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'loginId',
                text: 'Login Id',
                renderer: userRender,
                width:200,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'active',
                text: 'Active',
                renderer : userActiveRenderer,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'authority',
                text: 'Authority',
                renderer: userRender,
                width:120,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'excludeConfidential',
                text: 'Exclude Confidential',
                renderer: userRender,
                width:120,
            }
        ];

        me.viewConfig = {
        		stripeRows: true	
        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add New',
                        iconCls : 'btn-add',
                        tabIndex:6,
                        handler: function(){
                    		me.addUser();
                    	}
                    },'-',{
                        xtype: 'button',
                        text: 'Delete',
                        iconCls : 'btn-delete',
                        tabIndex:7,
                        handler: function(){
                    		me.deleteConfirm();
                    	}
                    },'-',{
                        xtype:'button',
                    	itemId: 'grid-excel-button',
                    	iconCls : 'btn-report-excel',
                        text: 'Export to Excel',
                        tabIndex:8,
                        handler: function(){
                        	var vExportContent = me.getExcelXml();
                            document.location='data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);
                        }
                    },'->',me.showCount
                    ]
            }
        ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });

        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			this.showCount.setValue(msg);
       	}, me);

        me.callParent(arguments);
    },
    
    addUser : function() {
    	app.userMgmtPanel.showUser();
	},

	deleteConfirm : function(){
		Ext.Msg.confirm("This will delete the User", "Do you want to continue?", this.deleteEmployee, this);
	},
	
	deleteEmployee : function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.employeeService.deleteUser(selectedRecord.data.id, this.onDelete, this);
		}
	},

	onDelete : function(data){
		if(!data.success){
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.userMgmtPanel.userSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}
	},

});

function activeRenderer(val) {
	if (val == true) {
		var val1="Yes";
		return '<span style="color:green;">' + val1 + '</span>';
	} 
	if (val == false) {
		var val1="No";
		return '<span style="color:red;">' + val1 + '</span>';
	}
	return val1;
}

function userRender(value, metadata, record) {
    metadata.tdAttr = 'data-qtip="' + value + '"';
    return value;
}

function userActiveRenderer(val, metadata, record, rowIndex, colIndex, store) {
	if (val == true) {
		metadata.tdAttr = 'data-qtip="' + 'Yes' + '"';
		return '<span style="color:green;">' + 'Yes' + '</span>';
	} 
	if (val == false) {
		metadata.tdAttr = 'data-qtip="' + 'No' + '"';
		return '<span style="color:red;">' + 'No' + '</span>';
	}
	return val;
}
