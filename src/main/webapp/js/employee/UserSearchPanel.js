Ext.define('tz.ui.UserSearchPanel', {
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    frame:true,
    layout:'anchor',
    anchor:'70%',
    autoScroll:true,
    buttonAlign:'left',
//creating view in init function
    initComponent: function() {
        var me = this;
        
        me.userCombo = Ext.create('Ext.form.ComboBox', {
            fieldLabel: 'User',
            store: ds.userSearchStore,
            name : 'userId',
            tabIndex:1,
            queryMode: 'local',
            displayField: 'fullName',
            labelAlign: 'right',
            valueField: 'id',
            triggerAction: 'all',
        	emptyText:'Select...',
        	anyMatch:true,
        });

        me.authorityCombo = Ext.create('Ext.form.ComboBox', {
        	fieldLabel:'Authority',
        	name:'authority',
            store: ds.authorityStore,
            tabIndex:2,
            displayField: 'name',
            valueField: 'value',
		    queryMode: 'local',
		    typeAhead: true,
		});

        
        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [me.userCombo]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [me.authorityCombo]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:3,
			},{
				text : 'Reset',
				scope: this,
				handler: this.reset,
				tabIndex:4,
			},{
				text : 'Clear',
				scope: this,
				tabIndex:5,
	             handler: function() {
	            	 this.form.reset();
	             }
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['userId','=', values.userId]);
    	params.push(['authority','=', values.authority]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=50;
    	ds.userStore.loadByCriteria(filter);
	},
	
	reset:function(){
		this.form.reset();
		this.search();
	},
	
});
