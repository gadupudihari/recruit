Ext.define('tz.ui.UserMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',

    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;

    	me.userGridPanel = new tz.ui.UserGridPanel({manager:me});
    	me.userSearchPanel = new tz.ui.UserSearchPanel({manager:me});
    	me.userDetailPanel = new tz.ui.UserDetailPanel({manager:me});
        
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,
            	border:false,
            	items: [ this.userSearchPanel,
 				        {
		   	        		height: 10,
		   	        		border : false
 				        },
 				        this.userGridPanel
 				        ]
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
	            items: [me.userDetailPanel]
            }
        ];
        me.callParent(arguments);
    },
    
	initScreen: function(){
		this.getLayout().setActiveItem(0);
		this.userSearchPanel.search();
		ds.userSearchStore.loadByCriteria();
	},
	
	showUser : function(rowIndex) {
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var userRec = ds.userStore.getAt(rowIndex);
		}
		this.userDetailPanel.loadUser(userRec);
	}
	
});
