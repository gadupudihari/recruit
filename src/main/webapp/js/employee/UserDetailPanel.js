Ext.define('tz.ui.UserDetailPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 10,
    title: '',
    anchor:'60% 50%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right', labelWidth : 120},
    
    initComponent: function() {
        var me = this;
        me.user ="";

        me.authorityCombo = Ext.create('Ext.form.ComboBox', {
        	fieldLabel:'Authority',
        	name:'authority',
        	forceSelection:true,
            store: ds.authorityStore,
            tabIndex:6,
            displayField: 'name',
            valueField: 'value',
		    queryMode: 'local',
		    typeAhead: true,
		});

        me.deleteButton = new Ext.Button({
            text: 'Delete',
            iconCls : 'btn-delete',
            scope : this,
            tabIndex:9,
            handler: function(){
    			this.deleteConfirm();
    		}
        });

        me.resetPasswordButton = new Ext.Button({
            text: 'Reset Password',
            iconCls : 'btn-reset',
            tabIndex:10,
            scope : this,
            handler: function(){
            	this.resetPasswordConfirm();
            }
        });
        
        me.givingAcessButton = new Ext.Button({
            text: 'Give Access',
            iconCls : 'btn-access',
            tabIndex:11,
            scope : this,
            handler: function(){
            	this.givingAcessConfirm();
            }
        });

        me.revokeAccessButton = new Ext.Button({
            text: 'Revoke Access',
            iconCls : 'btn-revoke',
            tabIndex:12,
            scope : this,
            handler: function(){
            	this.revokeAccessConfirm();
            }
        });

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'Save',
                        iconCls : 'btn-save',
                        scope: this,
                        tabIndex:8,
            	        handler: this.saveUser
                    },'-',
                    me.deleteButton,'-',me.resetPasswordButton,'-',me.givingAcessButton,'-',me.revokeAccessButton,'-',
                    {
                        xtype: 'button',
                        text: 'Close',
                        iconCls : 'btn-close',
                        tabIndex:13,
                        scope : this,
                        handler: function(){
                        	app.userMgmtPanel.initScreen();
                        }
                    }
                ]
            }
        ];
    	
        me.items = [this.getMessageComp(), this.getHeadingComp(),
            {
	   	    	 xtype:'fieldset',
	   	         title: 'User Information',
	   	         collapsible: true,
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'First Name *',
	   	                 name: 'firstName',
	   	                 allowBlank:false,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	                 tabIndex:1,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Email *',
	   	                 name: 'emailId',
	   	                 allowBlank:false,
	   	                 vtype:'email',
	   	                 tabIndex:3,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	             },{
	   	            	 xtype: 'textfield',
	   	            	 fieldLabel: 'Login Id',
	   	            	 name: 'loginId',
	   	            	 tabIndex:5,
	   	            	 readOnly : true
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Exclude Confidential',
	   	                 name: 'excludeConfidential',
	   	                 tabIndex:7,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	                 maskRe :  /[0-9,]/,
	   	                 listeners:{
	   	                	 change : function(field,newValue,oldValue){
	   	                		 this.setValue(newValue.replace(/,,/g, ","));
	   	                	 }
	   	                 },
	   	                 validator: function(value){
	   	                	 if (value != null && value != '') {
		   	                	 var values = value.split(',');
		   	                	 for ( var i = 0; i < values.length; i++) {
									if (values[i] < 1)
										return "Minimum value for Confidentiality is 1.";
									else if (values[i] > 9) 
										return "Maximum value for Confidentiality is 9.";
								}
							}
							return true;
	   	                 }
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Last Name *',
	   	                 name: 'lastName',
	   	                 allowBlank:false,
	   	                 tabIndex:2,
	   	                 maxLength:45,
	   	                 enforceMaxLength:true,
	   	             },	new tz.ui.ActiveCombo({
		   	  			name : 'active',
		   				store : ds.boolStore,
		   				forceSelection:true,
		   				value : true,
	   	                tabIndex:4,
			   		 }),
			   		 me.authorityCombo
	   	             ]
	   	         }]
	   	     },
   	     {
   	        xtype: 'numberfield',
   	        hidden:true,
   	        name: 'id'
   	    },{
   	        xtype: 'textfield',
   	        hidden:true,
   	        name: 'password'
   	    }
        ];
        me.callParent(arguments);
    },

    saveUser: function(){
    	if(this.form.isValid( )){
			var otherFieldObj = {};
			var itemslength = this.form.getFields().getCount();
			var i = 0;
			while (i < itemslength) {
				var fieldName = this.form.getFields().getAt(i).name;
				if(fieldName == 'id') {
					var idVal = this.form.findField(fieldName).getValue();
					if(typeof(idVal) != 'undefined' && idVal != null && idVal >= 0) {
						otherFieldObj['id'] = this.form.findField(fieldName).getValue();
					}
				} else {
					otherFieldObj[fieldName] = this.form.findField(fieldName).getValue();
				}
				i = i+1;
			}
			
			otherFieldObj = Ext.JSON.encode(otherFieldObj);
			app.employeeService.saveUser(otherFieldObj, this.onSaveUser, this);
    	} else {
    		this.updateError('Please fix the errors');
    	}
	},
	
	onSaveUser: function(data){
		if (data.success) { 
			var user = Ext.create('tz.model.User', data.returnVal);
			this.loadUser(user);
			this.updateMessage('Saved Successfully');
		}else {
			this.updateError('Save Failed');
		}	
	},

	onDelete : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			app.userMgmtPanel.initScreen();
		}
	},
	
	deleteConfirm : function(){
		Ext.Msg.confirm("This will delete the User", "Do you want to continue?", this.deleteEmployee, this);
	},
	
	deleteEmployee : function(dat){
		if(dat=='yes'){
			var idVal = this.getValues().id;
			app.employeeService.deleteUser(idVal, this.onDelete, this);
		}
	},

	onResetPassword : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			this.updateMessage('Password Reset Successful. The new password is '+data.successMessage);
		}
		
	},
	
	resetPasswordConfirm : function()
	{
		Ext.Msg.confirm("Reset Password", "Do you want to reset the password ?", this.resetPassword, this);
	},
	
	resetPassword : function(dat){
		this.clearMessage();
		if(dat=='yes'){	
			var empId = this.getValues().id;
			app.employeeService.resetPassword(empId, this.onResetPassword, this);
		}
	},

	loadUser: function(user){
		this.clearMessage();
		this.user = user;
		if(user){
			this.loadRecord(user);
			var loginId=user.data['loginId'];
			var empId=user.data['id'];
			this.deleteButton.enable();
			if(!loginId){
				this.resetPasswordButton.disable();
				this.givingAcessButton.enable();
				this.revokeAccessButton.disable();
				this.authorityCombo.setReadOnly(true);
			}
			else{
				this.resetPasswordButton.enable();
				this.givingAcessButton.disable();
				this.revokeAccessButton.enable();
				this.authorityCombo.setReadOnly(false);
			}
			var params = new Array();
		} else {
			this.getForm().reset();
			this.deleteButton.disable();
			this.givingAcessButton.disable();
			this.resetPasswordButton.disable();
			this.revokeAccessButton.disable();
			this.authorityCombo.setReadOnly(true);
		}
	},
	
	onGivingAcess : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			var user = Ext.create('tz.model.User', data.returnVal);
			this.loadUser(user);
			this.updateMessage('Access Granted to this employee, Default Loginid is the email Id, Password is '+data.successMessage);
		}
	},
	
	givingAcessConfirm : function()
	{
		if (this.user.data.active) {
			Ext.Msg.confirm("Grant Access", "Do you want to give access to this employee ?", this.givingAcess, this);	
		}else{
			this.updateError("Employee must be active inorder to give access");
		}
	},
	
	givingAcess : function(dat){
		this.clearMessage();
		if(dat=='yes'){
			var empId = this.getValues().id;
			app.employeeService.givingAcess(empId, this.onGivingAcess, this);
		}
	},
	
	onRevokeAccess : function(data){
		if(!data.success){
			this.updateError(data.errorMessage);
		}else{
			var user = Ext.create('tz.model.User', data.returnVal);
			this.loadUser(user);
			this.updateMessage('Access removed successfully for this employee');
		}
	},
	
	revokeAccessConfirm : function()
	{
		Ext.Msg.confirm("Revoke Access", "Do you want to revoke access to this employee ?", this.revokeAccess, this);
	},
	
	revokeAccess : function(dat){
		this.clearMessage();
		if(dat=='yes'){	
			var empId = this.getValues().id;
			app.employeeService.revokeAccess(empId, this.onRevokeAccess, this);
		}
	},
	
});