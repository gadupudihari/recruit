Ext.define('tz.ui.ExpeseCombo', {
	extend : 'Ext.form.ComboBox',
	queryMode : 'local',
	triggerAction : 'all',
	displayField : 'name',
	valueField : 'value',
	forceSelection : true,
	matchFieldWidth : true,
});


Ext.define('tz.ui.EmployeeRateCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel : 'Project',
	queryMode : 'local',
	triggerAction : 'all',
	displayField : 'label',
	valueField : 'value',
	name : 'employeeBillingRatesId',
	forceSelection : true,
	allowBlank : false,
	matchFieldWidth : false,
	listConfig : {
		width : 200
	}
});

Ext.define('tz.ui.WithHoldingCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel : 'WH State',
	queryMode : 'local',
	triggerAction : 'all',
	displayField : 'withHolding',
	valueField : 'withHolding',
	name : 'withHolding'
});


Ext.define('tz.ui.EmployeeCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Employee',
    queryMode: 'local',
    displayField: 'fullName',
    valueField: 'id',
    name : 'employeeId',
    forceSelection:true,
    allowBlank:false,
	typeAhead: true,
	triggerAction: 'all',
	emptyText:'Select...',
	queryDelay:100,
	anyMatch:true,
	listConfig:{width:600},
	typeAheadDelay:30000
});

Ext.define('tz.ui.ProjectCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Project',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'name',
    name : 'projectName',
	typeAhead: true,
	triggerAction: 'all',
	emptyText:'Select...',
	queryDelay:100,
	anyMatch:true,
	listConfig:{width:600},
	typeAheadDelay:30000
});

Ext.define('tz.ui.AuthorityCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Authority',
    queryMode: 'local',
    displayField: 'authority',
    valueField: 'authority',
    name : 'authority',
	typeAhead: true,
	triggerAction: 'all',
	emptyText:'Select...',
	queryDelay:100,
	anyMatch:true,
	listConfig:{width:600},
	typeAheadDelay:30000
});

Ext.define('tz.ui.TNEStatusCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Status',
    queryMode: 'local',
    labelAlign: 'right',
    displayField: 'value',
    valueField: 'name',
    name : 'status'
});


Ext.define('tz.ui.WeekCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Week',
    queryMode: 'local',
    displayField: 'weekEnding',
    valueField: 'weekEnding',
    name : 'status',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.PaymentGeneratedCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Payment Generated',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    name : 'paymentGenerated',
    forceSelection:true,
    allowBlank:false,
    value: false,
    disabled: true
});

Ext.define('tz.ui.InvoiceGeneratedCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Invoice Generated',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    name : 'invoiceGenerated',
    forceSelection:true,
    allowBlank:false,
    value: false,
    disabled: true
});

Ext.define('tz.ui.ExpenseTypeCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Expense Type',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    name : 'type',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.SortingCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Sort By',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    name : 'sortingCombo',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.TimesheetTypeCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Timesheet Type',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    name : 'type',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.PeriodCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Choose Period',
    queryMode: 'local',
    displayField: 'label',
    valueField: 'value',
    name : 'period',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.LayoutCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Choose Layout',
    queryMode: 'local',
    displayField: 'label',
    valueField: 'value',
    name : 'layout',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.MonthCombo', {
	extend : 'Ext.form.ComboBox',
    fieldLabel: 'Month',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    name : 'monthType',
    forceSelection:true,
    allowBlank:false
});

Ext.define('tz.ui.ActiveCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Active',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    name : 'status',
    value : true
});


Ext.define('tz.ui.EmpTypeCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Type of Employee',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    value:'EMPLOYEE',
    name : 'empType'
});

Ext.define('tz.ui.ImmigrationCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Immigration Status',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    name : 'immistatus'
});

Ext.define('tz.ui.DepositStatusCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Deposit Status',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    name : 'depositStatus'
});

Ext.define('tz.ui.DepositTypeCombo', {
	extend : 'Ext.form.ComboBox',
	fieldLabel: 'Deposit Type',
    queryMode: 'local',
    displayField: 'value',
    valueField: 'name',
    name : 'depositType'
});