Ext.define('tz.service.RequirementService', {
	extend : 'tz.service.Service',
		
    saveRequirements : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveRequirements',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },    
    
    deleteRequirement: function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/deleteRequirement/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    saveRequirement : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveRequirement',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    excelExport: function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/excelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },
    
    onSuccess : function(err) {
    	//Opening the Excel Filename returned from Java, This initiates download on the Browser
    	if (err.responseText.search(false) != -1) {
    		app.invoiceService.onFailure(err);
			return;
		}
    	if (err.responseText.search('.pdf') != -1) {
			var i = err.responseText.indexOf('.pdf');
			var w = window.open(err.responseText.slice(29,i+4));
		}else{
	    	var i=err.responseText.indexOf('xls');
	    	window.open(err.responseText.slice(29,i+3),'_self','',true);
		}
    	app.loadGenerateMask.hide();
    	this.onAjaxResponse;
    },
    
    onFailure : function(err) {
    	app.loadGenerateMask.hide();
    	if (err.responseText.search('.pdf') != -1) {
            Ext.MessageBox.show({
                title: 'Error',
                msg: 'Pdf Generation failed',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
		}else{
	        Ext.MessageBox.show({
	            title: 'Error',
	            msg: 'Excel export failed',
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
        this.onAjaxResponse;
    },

    contactExcelExport: function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/contactExcelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },
    
    clientExcelExport : function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/client/clientExcelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },
    
    placementsExcelExport : function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/placementLeads/placementsExcelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },

    interviewExcelExport : function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/interview/interviewExcelExport',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },

    removeCandidate : function(idVals,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/removeCandidate',
			params : {
				json : idVals
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },

    removeVendor : function(idVals,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/removeVendor',
			params : {
				json : idVals
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    saveTask : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveTask',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },    

    deleteTask: function(id,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/deleteTask/'+id,
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    saveSubRequirements : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveSubRequirements',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },    

    saveSubVendors : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveSubVendors',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },    

    getPendingJobs : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/getPendingJobs',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    exportAvailCandidates : function(filter,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/reports/exportAvailCandidates',
			params : {
				json : filter
			},
			success: this.onSuccess,
			failure: this.onFailure
		});
		app.loadGenerateMask.show();
    },
    
    getRequirements : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/getRequirements',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    immigrationVerifyEmails : function(values) {
		Ext.Ajax.request({
			url : 'remote/candidate/immigrationVerifyEmails',
			params : {
				json : values
			},
		});
    },
    
    getSubRequirements : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/getSubRequirements',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    saveShortlisted : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveShortlisted',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.saveMask.show();
    },
    
    removeShortlist : function(idVals,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/removeShortlist',
			params : {
				json : idVals
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    removeShortlistedJob : function(idVals,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/removeShortlistedJob',
			params : {
				json : idVals
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    getSuggestedCriteria : function(idVals,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/getSuggestedCriteria',
			params : {
				json : idVals
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    getPotentialCriteria : function(idVals,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			async: true,
			url : 'remote/requirement/getPotentialCriteria',
			params : {
				json : idVals
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },

    saveWorkList : function(values,  cb, scope){
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/saveWorkList',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    },
    
    getRequirementNo : function(values,  cb, scope) {
	    this.onAjaxResponse = Ext.bind(this.onAjaxResponse, scope || window, [cb, scope], true);
		Ext.Ajax.request({
			url : 'remote/requirement/getRequirementNo',
			params : {
				json : values
			},
			success: this.onAjaxResponse,
			failure: this.onAjaxResponse
		});
		app.loadMask.show();
    }
    
});
