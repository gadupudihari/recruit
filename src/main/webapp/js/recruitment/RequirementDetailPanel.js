Ext.define('tz.ui.RequirementDetailPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 0,
    title: '',
    anchor:'100% 100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'left',labelWidth :100},
    
    initComponent: function() {
        var me = this;
        me.refresh = false;
        me.newRequirement = false;
		me.copyRequirement = false;
		me.modified = false;
		me.closeButton = false;
        me.removeRecord = "";
        me.modifiedCandidateIds = new Array();
        me.modifiedResearchIds = new Array();
        me.modifiedVendorIds = new Array();
        me.verifyShortlistIds = new Array();
        me.verifyCandidateIds = new Array();
        me.modifiedShortlistIds = new Array();
        me.requirement ="";
        me.openedFrom ="";
        me.emailFilter ;
        me.CAcceptedIds = new Array();
        me.ISucceededIds = new Array();
        me.candidates = new Array();
        me.emailIds = new Array();
        me.specialCriteria = new Array();
        
        me.potentialCandidates = new tz.ui.SuggestedCandidates({manager:me});
        me.reqSmeGridPanel = new tz.ui.ReqSmeGridPanel({manager:me});

        me.vendorStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
        });

        me.hotnessCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Hotness',
		    store: ds.hotnessStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'hotness',
		    typeAhead: true,
		    forceSelection : true,
		    tabIndex:7,
		});
        
		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client',
		    store: ds.clientSearchStore,
		    forceSelection:true,
		    anyMatch:true,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'id',
		    name : 'clientId',
		    tabIndex:22,
		});

		me.jobOpeningStatusCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Job Opening Status',
		    store: ds.jobOpeningStatusStore,
		    forceSelection:true,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'jobOpeningStatus',
		    tabIndex:8,
		    labelWidth : 120,
		});

		me.employerCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Employer',
		    store: ds.employerStore,
		    editable: false,		   
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
		    name : 'employer',
		    tabIndex:12,
		});

		me.communicationCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Communication Required",
            name: 'communication',
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.communicationStore,
            editable: false,
		    allowBlank:true,
		    //width : 220,
		    tabIndex:28,
		});

		me.experienceCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Experience Required",
            name: 'experience',
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.experienceStore,
            editable: false,
		    allowBlank:true,
		    tabIndex:29,
		    labelWidth : 120,
		});

		me.helpCandidateCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Ok to train Candidate",
            name: 'helpCandidate',
	        tabIndex:25,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.helpCandidateStore,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					if (combo.value == 'Other')
						this.requirementPanel.form.findField('helpCandidateComments').show();
					else{
						this.requirementPanel.form.findField('helpCandidateComments').hide();
						this.requirementPanel.form.findField('helpCandidateComments').reset();
					}
					this.requirementPanel.doLayout();
				}
		    }
		});
		
		me.skillSetCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Skill Set",
            name: 'selectedSkillSet',
	        tabIndex:27,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'id',
            store: ds.skillSetStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    enableKeyEvents : true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var skillSetIds = this.requirementPanel.form.findField('skillSetIds').getValue();
					if (skillSetIds == null || skillSetIds == '' )
						skillSetIds = new Array();
					else
						skillSetIds = skillSetIds.split(',');
					if (skillSetIds.indexOf(combo.value.toString()) == -1) {
						skillSetIds.push(combo.value.toString());
					}else{
						skillSetIds.splice(skillSetIds.indexOf(combo.value.toString()), 1);
					}
					var skillsets = new Array();
					for ( var i = 0; i < skillSetIds.length; i++) {
						skillsets.push(ds.skillSetStore.getById(Number(skillSetIds[i])).data.name);
					}
					me.skillSetCombo.reset();
					this.requirementPanel.form.findField('skillSetIds').setValue(skillSetIds.toString());
					this.requirementPanel.form.findField('skillSet').setValue(skillsets.toString());
				},
		    }
		});

		me.moduleCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Module",
            name: 'selectedModule',
	        tabIndex:11,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.skillSetStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    enableKeyEvents : true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var module = this.requirementPanel.form.findField('module').getValue();
					if (module == null || module == '' )
						module = new Array();
					else
						module = module.split(',');
					if (module.indexOf(combo.value) == -1) {
						module.push(combo.value);
					}else{
						module.splice(module.indexOf(combo.value), 1);
					}
					me.moduleCombo.reset();
					this.requirementPanel.form.findField('module').setValue(module.toString());
				},
		    }
		});

		me.secondaryModuleCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Secondary Module",
            name: 'selectedSecondaryModule',
	        tabIndex:11,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.skillSetStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    enableKeyEvents : true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var module = this.requirementPanel.form.findField('secondaryModule').getValue();
					if (module == null || module == '' )
						module = new Array();
					else
						module = module.split(',');
					if (module.indexOf(combo.value) == -1) {
						module.push(combo.value);
					}else{
						module.splice(module.indexOf(combo.value), 1);
					}
					me.secondaryModuleCombo.reset();
					this.requirementPanel.form.findField('secondaryModule').setValue(module.toString());
				},
		    }
		});

		me.certificationsCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Mandatory Certifications",
            name: 'selectedCertifications',
	        tabIndex:31,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'id',
            store: ds.certificationsStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    enableKeyEvents : true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var certIds = this.requirementPanel.form.findField('certificationIds').getValue();
					if (certIds == null || certIds == '' )
						certIds = new Array();
					else
						certIds = certIds.split(',');
					if (certIds.indexOf(combo.value.toString()) == -1) {
						certIds.push(combo.value.toString());
					}else{
						certIds.splice(certIds.indexOf(combo.value.toString()), 1);
					}
					var certifications = new Array();
					for ( var i = 0; i < certIds.length; i++) {
						certifications.push(ds.certificationsStore.getById(Number(certIds[i])).data.name);
					}
					me.certificationsCombo.reset();
					this.requirementPanel.form.findField('certificationIds').setValue(certIds.toString());
					this.requirementPanel.form.findField('certifications').setValue(certifications.toString());
				},
		    }
		});

		me.targetRolesCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Role set",
            name: 'selectedTargetRoles',
	        tabIndex:30,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.targetRolesStore,
            anyMatch:true,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				select: function(combo,value){
					var targetRoles = this.requirementPanel.form.findField('targetRoles').getValue();
					if (targetRoles == null || targetRoles == '' )
						targetRoles = new Array();
					else
						targetRoles = targetRoles.split(',');
					if (targetRoles.indexOf(combo.value) == -1) {
						targetRoles.push(combo.value);
					}else{
						targetRoles.splice(targetRoles.indexOf(combo.value), 1);
					}
					me.targetRolesCombo.reset();
					this.requirementPanel.form.findField('targetRoles').setValue(targetRoles.toString());
				},
		    }
		});

		me.immigrationCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Immigration Status",
			name: 'immigrationStatus',
			tabIndex:96,
			queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
			store: ds.immigrationTypeStore,
			forceSelection:true,
			allowBlank:true,
		});

		me.sourceCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Source",
            name: 'source',
            store: ds.candidateSourceStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:42,
		});

		me.marketingStatusCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Marketing Status",
            name: 'marketingStatus',
            store: ds.marketingStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    tabIndex:97,
		});

		me.aboutPartnerCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "About Partner",
            name: 'aboutPartner',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:99,
		});

		me.openToCTHCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Open To CTH",
            name: 'openToCTH',
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'value',
            store: ds.yesNoStore,
		    forceSelection:true,
		    allowBlank:true,
		    tabIndex:94,
		});

		me.employmentTypeCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
			fieldLabel: "Employment Type",
            name: 'employmentType',
	        tabIndex:98,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.employmentTypeStore,
		    forceSelection:true,
		    allowBlank:true,
		    listeners: {
				scope:this,
				change: function(combo,value){ 
					this.form.findField('selectedEmploymentType').setValue(combo.rawValue);
				}
		    }
		});

		me.personalityCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Personality",
            name: 'personality',
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.personalityStore,
            editable: false,
		    allowBlank:true,
		    //width : 220,
		    tabIndex:100,
		});
		
		me.resumeCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "Resume",
            name: 'resume',
	        tabIndex:13,
	        labelWidth : 120,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.jobResumeStore,
            editable: false,
		    allowBlank:true,
		    tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item">{value}</div>','</tpl>'),
		});

		me.locationCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Location",
            name: 'location',
            store: ds.candidateLocationStore,
		    queryMode: 'local',
		    displayField: 'value',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'name',
		    labelWidth : 120,
		    tabIndex:10,
		});

		me.recruiterCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: "Recruiter",
            name: 'recruiter',
            store: ds.recruiterStore,
		    queryMode: 'local',
		    displayField: 'fullName',
		    forceSelection:true,
		    allowBlank:true,
		    valueField: 'id',
		    tabIndex:16,
		});

		me.remoteCheckbox = Ext.create('Ext.form.Checkbox', {
        	boxLabel : 'Remote',
            name : 'remote',
            inputValue: '0',
            checked :false,
            labelAlign :'right',
            itemId : 'remote',
            width : 150,
            tabIndex:19,
        });

		me.relocateCheckbox = Ext.create('Ext.form.Checkbox', {
        	boxLabel : 'Relocation Needed',
            name : 'relocate',
            inputValue: '0',
            //hecked :true,
            labelAlign :'right',
            itemId : 'relocate',
            width : 150,
            tabIndex:17,
        });

		me.travelCheckbox = Ext.create('Ext.form.Checkbox', {
        	boxLabel : 'Travel Needed',
            name : 'travel',
            inputValue: '0',
            //checked :true,
            labelAlign :'right',
            itemId : 'travel',
            width : 150,
            tabIndex:18,
            listeners: {
		    	change:function(checkbox,value) {
		    		if (value){
		    			me.tAndEPaidCheckbox.show();
		    			me.tAndENotPaidCheckbox.show();
		    		}else{
		    			me.tAndEPaidCheckbox.hide();
		    			me.tAndENotPaidCheckbox.hide();
		    			me.tAndEPaidCheckbox.setValue(false);
		    			me.tAndENotPaidCheckbox.setValue(false);
		    		}
		    		me.requirementPanel.doLayout();
		    	}
			} 
        });

		me.tAndEPaidCheckbox = Ext.create('Ext.form.Checkbox', {
        	boxLabel : 'T and E Paid',
            name : 'tAndEPaid',
            inputValue: '0',
            checked :false,
            labelAlign :'right',
            itemId : 'tAndEPaid',
            width : 150,
            tabIndex:20,
        });

		me.tAndENotPaidCheckbox = Ext.create('Ext.form.Checkbox', {
        	boxLabel : 'T and E not paid',
            name : 'tAndENotPaid',
            inputValue: '0',
            checked :false,
            labelAlign :'right',
            itemId : 'tAndENotPaid',
            width : 150,
            tabIndex:21,
        });

		me.deleteButton = new Ext.Button({
            text: 'Delete',
            iconCls : 'btn-delete',
            tooltip : 'Delete Job Opening',
            scope : this,
            tabIndex:44,
            handler: function(){
    			this.deleteConfirm();
    		}
        });

        me.saveAndRefreshButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & Refresh',
            tooltip : 'Save and Refresh',
            iconCls : 'btn-save',
            tabIndex:41,
            scope: this,
	        handler: function() {
				this.saveAndRefresh();
			}
        });

        me.saveAndNewButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & New',
            iconCls : 'btn-save',
            tooltip : 'Save and Add new job opening',
            tabIndex:42,
            scope: this,
	        handler: function() {
				this.saveAndNewRequirement();
			}
        });

        me.saveAndCopyButton = new Ext.Button({
        	xtype: 'button',
            text: 'Save & Copy',
            tooltip : 'Save & Copy',
            iconCls : 'btn-save',
            tabIndex:43,
            scope: this,
	        handler: function() {
				this.saveAndCopyRequirement();	
			}
        });

        me.dockedItems = [{
        	xtype: 'toolbar',
            dock: 'top',
            items: [{
            	xtype: 'button',
                text: 'Save',
                iconCls : 'btn-save',
                tooltip : 'Save',
                scope: this,
                tabIndex:40,
    	        handler: this.saveConfirm
            },
            '-',me.saveAndRefreshButton,'-',me.saveAndNewButton,'-',me.saveAndCopyButton,'-',me.deleteButton,'-',
            {
                xtype: 'button',
                text: 'Share',
                tooltip : 'View Job Opening',
                icon : 'images/icon_share.png',
                tabIndex:45,
                scope : this,
                handler: function(){
                	this.shareRequirement();
                }
            },'-',{
                xtype: 'button',
                text: 'Refresh',
                tooltip : 'Refresh Form',
                icon : 'images/arrow_refresh.png',
                tabIndex:46,
                scope : this,
                handler: function(){
                	app.requirementMgmtPanel.loadStores();
                	var id = me.requirementPanel.form.findField('id').getValue();
                	me.requirementPanel.form.reset();
                	me.researchGridPanel.store.removeAll();
                	me.getRequirement(id);
                }
            },'-',,{
                xtype: 'button',
                text: 'Next',
                tooltip : 'Edit Next Job opening',
                icon : 'images/icon_lookup.gif',
                tabIndex:48,
                scope : this,
                hidden : true,
                handler: function(){
                	this.nextRequirement();
                }
            },{
                xtype: 'button',
                text: 'Close',
                tooltip : 'Close form',
                iconCls : 'btn-close',
                tabIndex:48,
                scope : this,
                handler: function(){
                	this.closeConfirm();
                }
            }]
        }];

		me.addCandidateButton = new Ext.Button({
    		text: 'Add',
    		tooltip: 'Add Existing Candidate',
    		iconCls : 'btn-add',
    		tabIndex : 81,
    		scope: this,
    		handler: this.addCandidate
        });

        me.addInterviewButton = new Ext.Button({
        	xtype: 'button',
            text: 'Add Interview',
            tooltip: 'Add Interview',
            tabIndex : 83,
            iconCls : 'btn-add',
            scope: this,
	        handler: function() {
				this.addInterview();	
			}
        });

        me.submittedCandidates = Ext.create('Ext.grid.Panel', {
        	title: 'Submitted Candidates',
        	cls : 'submitted-green',
            frame:false,
            anchor:'98%',
            height:180,
            width:'100%',
            store: ds.subRequirementStore,
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],
            listeners: {
                edit: function(editor, event){
                	var record = event.record;
            		if(event.originalValue != event.value){
            			me.modifiedCandidateIds.push(record.data.id);
            			me.verifyCandidateIds.push(record.data.candidateId);
            		}
            		if (editor.context.field == 'submissionStatus'  && event.originalValue != event.value) {
            			// if submission status updated then update statusUpdated to current date
            			record.data.statusUpdated = new Date();
					}
            		if (event.column.dataIndex == "candidateId" && record.data.candidateId != null && record.data.candidateId != '' ) {
            			var candidate  = ds.contractorsStore.getById(record.data.candidateId);
            				record.data.candidate = candidate.data.fullName;
            			var candidate  = ds.candidateSearchStore.getById(record.data.candidateId);
            			if (candidate != null) {
            				record.data.candidateExpectedRate = candidate.data.expectedRate;
            				record.data.cityAndState = candidate.data.cityAndState;
            				record.data.relocate = candidate.data.relocate;
            				record.data.travel = candidate.data.travel;
            				record.data.availability = candidate.data.availability;
            				me.submittedCandidates.getView().refresh();	
            			}
					}else if (editor.context.field == 'submittedTime') {
		    			var time = record.data.submittedTime;
		    			if (time != null && time.toString().length > 8) {
		    			    record.data.submittedTime = timeToString(time);
		    			    this.getView().refresh();
		    			}
		    		}else if (editor.context.field == 'submissionStatus' && record.data.id != null && event.originalValue != event.value 
		    				&& event.value == 'C-Accepted' && record.data.projectInserted != true) {
						me.CAcceptedIds.push(record.data.id);
					}else if (editor.context.field == 'submissionStatus' && record.data.id != null && event.originalValue != event.value && event.value == 'I-Succeeded') {
						me.ISucceededIds.push(record.data.id);
					}
                },
                beforeedit: function(editor, event) {
              		var record = event.record;
            		if (record.get('deleted')==true )
						return false;
              		if (event.column.dataIndex == "candidateId" && record.data.candidateId != null && record.data.candidateId != '' ) {
              			var candidate  = ds.candidateSearchStore.getById(record.data.candidateId);
              			if (candidate == null)
							return false;
              		}
              		return true;
                },
            	itemclick: function(dv, record, item, index, e) {
            		if (record.data.deleted == true)
            			me.submittedCandidates.getSelectionModel().deselectAll();
            	}
            },
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [me.addCandidateButton,'-',{
            		xtype: 'button',
            		text: '',
            		tooltip : 'Save Submitted Candidates',
            		tabIndex : 82,
            		iconCls : 'btn-save',
            		scope: this,
            		handler: this.validatesCandidates
            	},'-',me.addInterviewButton,'-',{
            		xtype: 'button',
            		text: 'Reset',
            		tooltip : 'Reset Submitted Candidates',
            		tabIndex : 84,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            				me.modifiedCandidateIds = new Array();
        					me.loadSubRequirments(me.requirementPanel.form.findField('id').getValue());
        				} 
            	},'-',{
		    		xtype:'button',
		    		iconCls : 'btn-add',
	                text: 'Add Guide',
	                tooltip:'Add Guide',
	                tabIndex : 85,
		    		handler: function(){
		    			me.addGuide('Submitted');
		    		}
		    	},'-',{
		    		xtype:'button',
		    		iconCls : 'btn-add',
	                text: 'Add Reference',
	                tooltip:'Add Reference',
	                tabIndex : 86,
		    		handler: function(){
		    			me.addReference();
		    		}
		    	},{
		    		xtype:'button',
		    		iconCls : 'btn-add',
	                text: 'Show Interview',
	                tooltip:'Show Interview',
	                tabIndex : 87,
		    		handler: function(){
		    			var record = me.submittedCandidates.getView().getSelectionModel().getSelection()[0];
		    			if (record == null){
		    	    		Ext.Msg.alert('Error','Please select a record.');
		    	    		return false;
		    			}
		    			me.getInterview(record);
		    		}
		    	}]
            }],

            columns: [
                  {
          			xtype : 'actioncolumn',
        			hideable : false,
        			width : 30,
        			items : [ {
        				padding : 10,
        				scope : this,
        				handler : function(grid, rowIndex, colIndex) {
        					var record = ds.subRequirementStore.getAt(rowIndex);
        					if (record.data.deleted != true) {
        						this.removeCandidateConfirm(record);	
        					}
        				} 
        			} ],
        			renderer : function(value, metadata, record) {
                		if (record.data.deleted == true) {
                			me.submittedCandidates.columns[0].items[0].icon = '';
                			me.submittedCandidates.columns[0].items[0].tooltip = '';
                		}else{
                			me.submittedCandidates.columns[0].items[0].icon = 'images/icon_delete.gif';
                			me.submittedCandidates.columns[0].items[0].tooltip = 'Remove Candidate from Jop Opening';
                		}
                	},
        		},{
                    xtype : 'actioncolumn',
                    hideable : false,
                    width : 30,
                    items : [{
        				icon : 'images/icon_edit.gif',
        				tooltip : 'View Candidate',
        				padding: 50,
        				scope: this,
        				handler : function(grid, rowIndex, colIndex) {
        					if(ds.subRequirementStore.getAt(rowIndex).data.candidateId != null && ds.subRequirementStore.getAt(rowIndex).data.candidateId != 0)
        						me.viewCandidate(ds.subRequirementStore.getAt(rowIndex).data.candidateId);
        				}
        			}]
                },{
      				header : 'Candidate',
    				dataIndex : 'candidateId',
    	            width :  130,
    		        editor: {
    		        	matchFieldWidth: false,
    		        	anyMatch:true,
    	                xtype: 'combobox',
    	                queryMode: 'local',
    	                triggerAction: 'all',
    	                displayField: 'fullName',
    	                valueField: 'id',
    	                emptyText:'Select...',
    	                listClass: 'x-combo-list-small',
    	                store: ds.contractorsStore,
    	                anyMatch:true,
    	                width :  130,
    	            },
                    renderer:function(value, metadata, record){
                    	if(record){
                    		return record.data.candidate;
                    	}else{
                    		return null;
                    	}
                    }
    			},{
      				header : 'Submission Status',
    				dataIndex : 'submissionStatus',
    	            width :  90,
    		        editor: {
    		        	matchFieldWidth: false,
    	                xtype: 'combobox',
    	                queryMode: 'local',
    	                triggerAction: 'all',
    	                displayField: 'value',
    	                valueField: 'name',
    	                emptyText:'Select...',
    	                listClass: 'x-combo-list-small',
    	                store: ds.submissionStatusStore,
    	                width :  130,
    	            },
    			},{
    				text : 'Submitted Date',
    				dataIndex : 'submittedDate',
    				width :  80,
    				renderer: dateRender,
    				editor: {
    	                xtype: 'datefield',
    	                format: 'm/d/y'
    	            }
    			},{
    				text : 'Submitted Time',
    				dataIndex : 'submittedTime',
    				width :  80,
    				editor: {
    					xtype: 'timefield',
                        increment: 1,
                        anchor: '100%'
    	            }
    			},{
                    dataIndex: 'statusUpdated',
                    text: 'Status Updated Date',
                    width :  80,
                    xtype: 'datecolumn',
                    format:'m/d/Y',
    				editor: {
    	                xtype: 'datefield',
    	                format: 'm/d/y'
    	            }
                },{
      				header : 'Vendor',
    				dataIndex : 'vendorId',
    	            width :  110,
    		        editor: {
    		        	matchFieldWidth: false,
    		        	anyMatch:true,
    	                xtype: 'combobox',
    	                queryMode: 'local',
    	                triggerAction: 'all',
    	                displayField: 'name',
    	                valueField: 'id',
    	                emptyText:'Select...',
    	                listClass: 'x-combo-list-small',
    	                store: me.vendorStore,
    	                width :  130,
    	            },
                    renderer:function(value, metadata, record){
                    	var vendor = ds.vendorStore.getById(value);
                    	if(vendor)
                    		return vendor.data.name;
                    	else
                    		return null;
                    }
    			},{
      				header : 'Resume Link',
    				dataIndex : 'resumeLink',
    				renderer: renderValue,
    		        editor: {
    	                xtype: 'textfield',
                        maxLength:250,
       	                enforceMaxLength:true,
       	                vtype:'url',
    	            },
    			},{
      				header : 'View Resume',
    				dataIndex : 'resumeLink',
    				width :  50,
                    renderer:function(value, metadata, record){
                    	if (value != null && value != ''){
                        	metadata.tdAttr = 'data-qtip="' + value + '"';
                        	return '<a href='+value+' target="_blank">'+ 'Go' +'</a>';
                    	}
                    },
    			},{
      				header : 'Submitted Rate-1',
    				dataIndex : 'submittedRate1',
    				renderer: renderValue,
    		        editor: {
    	                xtype: 'textfield',
                        maxLength:100,
       	                enforceMaxLength:true,
    	            },
    			},{
      				header : 'Submitted Rate-2',
    				dataIndex : 'submittedRate2',
    				renderer: renderValue,
    		        editor: {
    	                xtype: 'textfield',
                        maxLength:100,
       	                enforceMaxLength:true,
    	            },
    			},{
      				header : 'Comments',
    				dataIndex : 'comments',
    				width : 120,
    				renderer: renderValue,
    		        editor: {
    	                xtype: 'textarea',
    	                height : 40,
                        maxLength:250,
       	                enforceMaxLength:true,
    	            },
    			},{
      				header : 'Guide',
    				dataIndex : 'guide',
                    renderer:function(value, metadata, record){
                    	if (value != null && value != '') {
        					var ids = value.toString().split(',');
        					var names ="",tooltip='';
        					for ( var i = 0; i < ids.length; i++) {
        						var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
        						if (rec){
        							names += rec.data.fullName+", ";	
        							tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
        						}
        					}
        					metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
        					return '('+ids.length+') '+ names.substring(0,names.length-2);
    					}
                    	return 'N/A';
                    }
    			},{
      				header : 'References',
    				dataIndex : 'reference',
                    renderer:function(value, metadata, record){
                    	if (value != null && value != '') {
        					var ids = value.toString().split(',');
        					var names ="",tooltip='';
        					for ( var i = 0; i < ids.length; i++) {
        						var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
        						if (rec){
        							names += rec.data.fullName+", ";	
        							tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
        						}
        					}
        					metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
        					return '('+ids.length+') '+ names.substring(0,names.length-2);
    					}
                    	return 'N/A';
                    }
    			},{
    				text: 'Resume Help',
    				dataIndex: 'resumeHelp',
                },{
                	text: 'Interview Help',
                    dataIndex: 'interviewHelp',
                },{
                	text: 'Job Help',
                	dataIndex: 'jobHelp',
                },{
                    dataIndex: 'candidateExpectedRate',
                    text: 'Expected Rate',
                },{
                    dataIndex: 'cityAndState',
                    text: 'Homeloc',
                },{
                    dataIndex: 'relocate',
                    text: 'Will Relocate',
                    width :  80,
                    renderer: activeRenderer
                },{
                    dataIndex: 'travel',
                    text: 'Will Travel',
                    width :  80,
                    renderer: activeRenderer
                },{
                	dataIndex: 'alert',
                	text: 'Alert',
                	width:70,
                	renderer:function(value, metadata, record){	
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		if (value == null || value=="") 
                			return 'N/A' ;
                		return '<span style="color:red;">' + value + '</span>';
                	}
                },{
                    dataIndex: 'availability',
                    text: 'Availability',
                    width :  80,
                    xtype: 'datecolumn',
                    format:'m/d/Y',
                }
            ],
            viewConfig : {
            	stripeRows:false ,
            	getRowClass: function (record, rowIndex, rowParams, store) {
            		return record.get('deleted')==true ? 'gray-row' : '';
            	}
            }
        });

        me.contactsCombo = Ext.create('Ext.form.ComboBox', {
        	id : 'submittedContactCombo',
        	name : 'contactId',
        	displayField: 'fullName',
            valueField: 'id',
            listClass: 'x-combo-list-small',
            store: ds.contactSearchStore,
		    queryMode: 'local',
		    typeAhead: true,
		    anyMatch:true,
		});

        me.submittedVendorsCount = new Ext.form.field.Display({
        	name : 'submittedVendorsCount',
        	fieldLabel: '',
        });

        me.submittedVendors = Ext.create('Ext.grid.Panel', {
        	title:'Vendors',
            frame:false,
            anchor:'98%',
            height:150,
            width:'100%',
            store: ds.subVendorStore,
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],

            listeners: {
                edit: function(editor, event){
                	var record = event.record;
            		if(event.originalValue != event.value){
            			me.modifiedVendorIds.push(record.data.id);
            		}
                    if (editor.context.field == 'contactId') {
                    	var contact = ds.contactSearchStore.getById(record.data.contactId);
                    	if (contact != null && contact.data.clientId != record.data.vendorId)
                    		record.data.contactId = null;
                    	else{
                    		record.data.email = contact.data.email;
                    		record.data.cellPhone = contact.data.cellPhone;
                    	}
                    	ds.contactSearchStore.clearFilter(true);
                    }else if (editor.context.field == 'vendorId') {
                    	record.data.contactId = null;
					}
                    me.submittedVendors.getView().refresh();
                },
                beforeedit: function(editor, event) {
              		var record = event.record;
              		if (event.column.dataIndex == "vendorId") {
              			if (record.data.vendorId != '' && ds.subRequirementStore.findExact('vendorId',record.data.vendorId) != -1) {
              				me.updateError('Candidate associated to this vendor. You can not update vendor');
              				return false;
						}
              		}
              		if (event.column.dataIndex == "contactId") {
    					var contactCombo = Ext.getCmp('submittedContactCombo');
    					contactCombo.setValue('');
    					contactCombo.setDisabled(false);
                        var contacts = new Array();
                        ds.contactSearchStore.clearFilter(true);
              			ds.contactSearchStore.filterBy(function(rec) {
              			    return rec.get('clientId') === record.data.vendorId;
              			});
              			for ( var i = 0; i < ds.contactSearchStore.getCount(); i++) {
							contacts.push(ds.contactSearchStore.getAt(i));
						}
              			var newContactsStore = new tz.store.ContactSearchStore({data: contacts}); 
              			contactCombo.bindStore(newContactsStore);
              			me.submittedVendors.getView().refresh();
              			return true;
  					}
                }
            },
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Add Vendor',
            		iconCls : 'btn-add',
            		tooltip : 'Add Vendor',
            		tabIndex:51,
            		scope: this,
            		handler: this.addVendor
            	},'-',{
            		xtype: 'button',
            		text: 'Save',
            		iconCls : 'btn-save',
            		tooltip : 'Save Vendor',
            		tabIndex:52,
            		scope: this,
            		handler: this.saveVendors
            	},'-',{
            		xtype: 'button',
            		text: 'Reset',
            		tooltip : 'Reload Vendors',
            		tabIndex:53,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            				me.modifiedVendorIds = new Array();
        					me.loadSubVendors(me.requirementPanel.form.findField('id').getValue());
        				} 
            	},'->',me.submittedVendorsCount]
            }],
            columns: [
                  {
          			xtype : 'actioncolumn',
        			hideable : false,
        			width : 35,
        			items : [ {
        				icon : 'images/icon_delete.gif',
        				tooltip : 'Remove Vendor from Jop Opening',
        				padding : 10,
        				scope : this,
        				handler : function(grid, rowIndex, colIndex) {
        					this.removeVendorConfirm(ds.subVendorStore.getAt(rowIndex));
        				} 
        			} ]
        		},{
        			xtype: 'actioncolumn',
        			width :35,
        			items : [{
        				icon : 'images/Popout.ico',
        				tooltip : 'View Snapshot',
        				padding: 50,
        				scope: this,
        				handler : function(grid, rowIndex, colIndex) {
        					var vendorId = me.submittedVendors.store.getAt(rowIndex).data.vendorId;
        					app.homeMgmtPanel.showSnapShot(vendorId, 'Vendor');
        				}
        			}]
        		},{
        			header : 'Id',
        			dataIndex : 'id',
        			width : 0
        		},{
      				header : 'Vendor',
    				dataIndex : 'vendorId',
    	            width :  150,
    		        editor: {
    		        	matchFieldWidth: false,
    		        	anyMatch:true,
    	                xtype: 'combobox',
    	                id : 'submittedVendorCombo',
    	                queryMode: 'local',
    	                triggerAction: 'all',
    	                displayField: 'name',
    	                valueField: 'id',
    	                emptyText:'Select...',
    	                listClass: 'x-combo-list-small',
    	                store: ds.vendorStore,
    	            },
                    renderer:function(value, metadata, record){
                    	var vendor = ds.vendorStore.getById(value);
                    	if(vendor){
                    		metadata.tdAttr = 'data-qtip="' + vendor.data.name + '"';
                    		return vendor.data.name;
                    	}else
                    		return null;
                    }
    			},{
    				header : 'Contact Person',
    				dataIndex : 'contactId',
    	            width :  150,
    		        editor: me.contactsCombo, 
                    renderer:function(value, metadata, record){
                    	var contact = ds.contactSearchStore.getById(value);
                    	if(contact)
                    		return contact.data.fullName;
                    	else
                    		return null;
                    }
    			},{
    				header : 'Contact Email',
    				dataIndex : 'email',
    	            width :  150,
    			},{
    				header : 'Contact Cell Phone',
    				dataIndex : 'cellPhone',
    	            width :  150,
    			},{
                    xtype : 'actioncolumn',
                    hideable : false,
                    width : 30,
                    items : [{
        				icon : 'images/icon_edit.gif',
        				tooltip : 'View Contact',
        				padding: 50,
        				scope: this,
        				handler : function(grid, rowIndex, colIndex) {
        					if(ds.subVendorStore.getAt(rowIndex).data.contactId != null && ds.subVendorStore.getAt(rowIndex).data.contactId != 0)
        						me.viewContact(ds.subVendorStore.getAt(rowIndex).data.contactId);
        				}
        			}]
                }
            ],
        });

        me.suggestedVendorsCount = new Ext.form.field.Display({
        	name : 'suggestedVendorsCount',
        	fieldLabel: '',
        });

        me.suggstedVendors = Ext.create('Ext.grid.Panel', {
        	title:'Suggested Vendors',
            frame:false,
            anchor:'98%',
            height:180,
            width:'100%',
            store: ds.suggstedVendorStore,

            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Add to Vendors',
            		icon : 'images/icon_lookup_up.gif',
            		tooltip : 'Add selected vendor into Vendors list',
            		tabIndex : 55,
            		scope: this,
            		handler: function() {
            			this.addFromSuggested();
					}
            	},'-',{
            		xtype: 'button',
            		text: 'Reset',
            		tooltip : 'Reload Suggested vendors',
            		tabIndex : 56,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			me.loadSuggestedVendors();
            		} 
            	},'->',me.suggestedVendorsCount]
            }],
            columns: [{
                          xtype: 'actioncolumn',
                          width :40,
                          items : [{
              				icon : 'images/icon_edit.gif',
              				tooltip : 'View Vendor',
              				padding: 50,
              				scope: this,
              				handler : function(grid, rowIndex, colIndex) {
              					me.showVendor(me.suggstedVendors.store.getAt(rowIndex));
              				}
              			}]
                      },
                      {dataIndex: 'id',text : 'Unique Id',width :  50,renderer: clientRender},
                      {dataIndex: 'name',text: 'Name',renderer: clientRender},
                      {dataIndex: 'score',text: 'Score',width:50,renderer: clientRender},
                      {dataIndex: 'city',text: 'City',width:60,renderer: clientRender},
                      {dataIndex: 'state',text: 'State',width:60,renderer: clientRender},
                      {dataIndex: 'address',text: 'Address',width:120,renderer: clientRender},
                      {dataIndex: 'comments',text: 'Comments',width:100,renderer: clientRender},
                      {dataIndex: 'contactNumber',text: 'Contact Number',width:90,renderer: clientRender},
                      {dataIndex: 'fax',text: 'Fax',width:90,renderer: clientRender},
                      {dataIndex: 'email',text: 'Email',width:120,renderer: clientRender},
                      {dataIndex: 'website',text: 'Website',width:120,renderer: clientRender},
                      {dataIndex: 'industry',text: 'Industry',renderer: clientRender}
                  ],
        });

        me.shortlistedCandidates = Ext.create('Ext.grid.Panel', {
        	title: 'Shortlisted Candidates',
        	cls : 'shortlisted-yellow',
            frame:false,
            anchor:'98%',
            height:200,
            width:'100%',
            store: ds.shortlistedCandidatesStore,
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],
            listeners: {
                edit: function(editor, event){
                	var record = event.record;
            		if(event.originalValue != event.value){
            			me.modifiedShortlistIds.push(record.data.id);
            			me.verifyShortlistIds.push(record.data.candidateId);
            		}
            		if (event.column.dataIndex == "candidateId" && record.data.candidateId != null && record.data.candidateId != '' ) {
            			var candidate  = ds.candidateSearchStore.getById(record.data.candidateId);
            			if (candidate != null) {
            				record.data.candidate = candidate.data.fullName;
            				record.data.candidateExpectedRate = candidate.data.expectedRate;
            				record.data.cityAndState = candidate.data.cityAndState;
            				record.data.relocate = candidate.data.relocate;
            				record.data.travel = candidate.data.travel;
            				record.data.availability = candidate.data.availability;
            				me.shortlistedCandidates.getView().refresh();	
            			}
					}
                },
                beforeedit: function(editor, event) {
              		var record = event.record;
            		if (record.get('deleted')==true )
						return false;
              		if (event.column.dataIndex == "candidateId" && record.data.candidateId != null && record.data.candidateId != '' ) {
              			var candidate  = ds.candidateSearchStore.getById(record.data.candidateId);
              			if (candidate == null)
							return false;
              		}
              		return true;
                },
            	itemclick: function(dv, record, item, index, e) {
            		if (record.data.deleted == true)
            			me.shortlistedCandidates.getSelectionModel().deselectAll();
            	}
            },
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Add',
            		tooltip: 'Add Existing Candidate',
            		iconCls : 'btn-add',
            		tabIndex:71,
            		scope: this,
            		handler: this.addShortlist
            	},'-',{
            		xtype: 'button',
            		text: 'Save',
            		tooltip: 'Save',
            		iconCls : 'btn-save',
            		tabIndex:72,
            		scope: this,
            		handler: this.validatesShortlist
            	},'-',{
    	        	xtype:'button',
    	        	icon : 'images/icon_lookup_down.gif',
    	        	text: 'Submit',
    	        	tooltip:'Mark candidate as submitted',
    	        	tabIndex:73,
    	        	handler: function(){
    	        		var record = me.shortlistedCandidates.getView().getSelectionModel().getSelection()[0];
    	        		if (record == null) {
    	            		me.updateError('Please select a candidate.');
    	            		return false;
    	        		}
    	        		me.submitCandidate(record.data.candidateId);
    	        	}
    	        },'-',{
            		xtype: 'button',
            		text: 'Reset',
            		tooltip: 'Reset Shortlisted Candidates',
            		tabIndex:74,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            				me.modifiedShortlistIds = new Array();
        					me.loadShortlisted(me.requirementPanel.form.findField('id').getValue());
        				} 
            	},'-',{
		    		xtype:'button',
		    		iconCls : 'btn-add',
	                text: 'Add Guide',
	                tooltip:'Add Guide',
	                tabIndex:75,
		    		handler: function(){
		    			me.addGuide('Shortlisted');
		    		}
		    	}]
            }],

            columns: [
                  {
          			xtype : 'actioncolumn',
        			hideable : false,
        			width : 30,
        			items : [ {
        				padding : 10,
        				scope : this,
        				handler : function(grid, rowIndex, colIndex) {
        					var record = me.shortlistedCandidates.store.getAt(rowIndex);
        					if (record.data.deleted != true) {
        						this.removeShortlistConfirm(record);	
        					}
        				} 
        			} ],
        			renderer : function(value, metadata, record) {
                		if (record.data.deleted == true) {
                			me.shortlistedCandidates.columns[0].items[0].icon = '';
                			me.shortlistedCandidates.columns[0].items[0].tooltip = '';
                		}else{
                			me.shortlistedCandidates.columns[0].items[0].icon = 'images/icon_delete.gif';
                			me.shortlistedCandidates.columns[0].items[0].tooltip = 'Remove';
                		}
                	},
        		},{
                    xtype : 'actioncolumn',
                    hideable : false,
                    width : 30,
                    items : [{
        				icon : 'images/icon_edit.gif',
        				tooltip : 'View Candidate',
        				padding: 50,
        				scope: this,
        				handler : function(grid, rowIndex, colIndex) {
        					if(me.shortlistedCandidates.store.getAt(rowIndex).data.candidateId != null && me.shortlistedCandidates.store.getAt(rowIndex).data.candidateId != 0)
        						me.viewCandidate(me.shortlistedCandidates.store.getAt(rowIndex).data.candidateId);
        				}
        			}]
                },{
      				header : 'Candidate',
    				dataIndex : 'candidateId',
    	            width :  130,
    		        editor: {
    		        	matchFieldWidth: false,
    		        	anyMatch:true,
    	                xtype: 'combobox',
    	                queryMode: 'local',
    	                triggerAction: 'all',
    	                displayField: 'fullName',
    	                valueField: 'id',
    	                emptyText:'Select...',
    	                listClass: 'x-combo-list-small',
    	                store: ds.contractorsStore,
    	                anyMatch:true,
    	                width :  130,
    	            },
                    renderer:function(value, metadata, record){
                    	if(record){
                    		metadata.tdAttr = 'data-qtip="' + record.data.candidate + '"';
                    		return record.data.candidate;
                    	}else{
                    		return null;
                    	}
                    }
    			},{
                    dataIndex: 'candidateExpectedRate',
                    text: 'Expected Rate',
                },{
      				header : 'Comments',
    				dataIndex : 'comments',
    				width : 120,
    				renderer: renderValue,
    		        editor: {
    	                xtype: 'textarea',
    	                height : 40,
                        maxLength:250,
       	                enforceMaxLength:true,
    	            },
    			},{
      				header : 'Guide',
    				dataIndex : 'guide',
                    renderer:function(value, metadata, record){
                    	if (value != null && value != '') {
        					var ids = value.toString().split(',');
        					var names ="",tooltip='';
        					for ( var i = 0; i < ids.length; i++) {
        						var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
        						if (rec){
        							names += rec.data.fullName+", ";	
        							tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
        						}
        					}
        					metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
        					return '('+ids.length+') '+ names.substring(0,names.length-2);
    					}
                    	return 'N/A';
                    }
    			},{
                	text : 'Resume Help',
                	dataIndex : 'resumeHelp',
                },{
                    dataIndex: 'cityAndState',
                    text: 'Homeloc',
                },{
                    dataIndex: 'relocate',
                    text: 'Will Relocate',
                    width :  80,
                    renderer:activeRenderer
                },{
                    dataIndex: 'travel',
                    text: 'Will Travel',
                    width :  80,
                    renderer:activeRenderer
                },{
                	dataIndex: 'alert',
                	text: 'Alert',
                	width:70,
                	renderer:function(value, metadata, record){	
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		if (value == null || value=="") 
                			return 'N/A' ;
                		return '<span style="color:red;">' + value + '</span>';
                	}
                },{
                    dataIndex: 'availability',
                    text: 'Availability',
                    width :  80,
                    xtype: 'datecolumn',
                    format:'m/d/Y',
                }
            ],
            viewConfig : {
            	stripeRows:false ,
            	getRowClass: function (record, rowIndex, rowParams, store) {
            		return record.get('deleted')==true ? 'gray-row' : '';
            	}
            }
        });
        
        me.researchGridPanel = Ext.create('Ext.grid.Panel', {
            title: 'Tasks',
            header : false,
            frame:false,
            anchor:'98%',
            height:200,
            width:'100%',
            store: ds.researchStore,
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],
            listeners: {
                edit: function(editor, event){
                	var record = event.record;
            		if(event.originalValue != event.value){
            			me.modifiedResearchIds.push(record.data.id);
            		}
                },
            },

            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Add New Task',
            		iconCls : 'btn-add',
            		tooltip : 'Add new Task',
            		tabIndex:61,
            		scope: this,
            		handler: this.addTask
            	},'-',{
            		xtype: 'button',
            		text: 'Save',
            		tooltip : 'Save Tasks',
            		iconCls : 'btn-save',
            		scope: this,
            		tabIndex:62,
            		handler: function() {
            			if (me.researchGridPanel.store.getCount() ==0 ) {
            	    		this.body.scrollTo('top', 0);
            	    		this.updateError("There are no updates in Research tasks.");
            	    		return false;
						}
            			me.saveTask();
					}
            	},'-',{
            		xtype: 'button',
            		text: 'Delete Task',
            		tooltip : 'Delete Tasks',
            		iconCls : 'btn-delete',
            		tabIndex:63,
            		scope: this,
            		handler: this.deleteTaskConfirm
            	}]
            }],
            
            columns: [
                  {
                    xtype: 'gridcolumn',
                    dataIndex: 'serialNo',
                    text: 'S.No',
                    width:50,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'task',
                    text: 'Task',
                    renderer: renderValue,
                    width : 200,
                    editor: {
                        xtype: 'textarea',
                        maxLength:300,
                        height : 60,
       	                enforceMaxLength:true,
       	                listeners:{
       	                	change : function(field,newValue,oldValue){
       	                		this.setValue(newValue.replace(/"/g, "'"));
       	                	}
 			            }
                    }
                },{
    				text : 'Status',
    				dataIndex : 'status',
    				width:70,
                    editor: {
                    	matchFieldWidth: false,
    	                xtype: 'combobox',
    	                queryMode: 'local',
    	                typeAhead: true,
    	                triggerAction: 'all',
    	                selectOnTab: true,
    	                forceSelection:true,
    	                displayField: 'value',
    	                valueField: 'name',
    	                emptyText:'Select...',
    	    		    store : ds.researchStatusStore,
    	                lazyRender: true,
    	                listClass: 'x-combo-list-small'
    	            },
                    renderer:function(value, metadata, record){
                    	metadata.tdAttr = 'data-qtip="' + value + '"';
                    	if (value == 'Yet to Start')
                    		metadata.style = 'background-color:#FFD3A7;';//orange
                    	else if (value == 'In Progress')
                    		metadata.style = 'background-color:#A7FDA1;';//green
                    	else if (value == 'Next in Line')
                    		metadata.style = 'background-color:#ffe9ce;';//light orange
                    	else if (value == 'Completed')
                    		metadata.style = 'background-color:#d2fdcf;';//light green
                		return value;
                    }
    			},{
	                dataIndex: 'comments',
	                text: 'Comments',
	                renderer: renderValue,
	                width : 200,
	                editor: {
	                    xtype: 'textarea',
	                    height : 60,
	                    maxLength:300,
	   	                enforceMaxLength:true,
       	                listeners:{
       	                	change : function(field,newValue,oldValue){
       	                		this.setValue(newValue.replace(/"/g, "'"));
       	                	}
 			            }
	                }
	            },{
					text : 'Last Updated User',
					dataIndex : 'lastUpdatedUser',
					width:90,
					renderer:function(value, metadata, record){
	               		metadata.tdAttr = 'data-qtip="' + value + '"';
	                	return value;
	                }
				},{
	                xtype: 'datecolumn',
	                dataIndex: 'created',
	                text: 'Created',
	                format: "m/d/Y H:i:s A",
	                width:120,
	                hidden : true
	            },{
	                xtype: 'datecolumn',
	                dataIndex: 'lastUpdated',
	                format: "m/d/Y H:i:s A",
	                text: 'Last Updated',
	                width:120,
	                //hidden : true
	            }
            ],
        });
        
		me.requirementPanel = Ext.create('Ext.form.Panel', {
        	bodyPadding: 0,
        	border : 0,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 100},
        	xtype: 'container',
	        
	         items:[
	                {
	   	   	    	 xtype:'fieldset',
	   	   	    	 title : 'Job Opening Details',
		   	         collapsible: false,
		   	         layout: 'column',
		   	         items :[{
		   	             xtype: 'container',
		   	             columnWidth:.9,
		   	             items: [{
		   	            	 xtype: 'container',
		   	            	layout :'table',
		   	            	 items: [{
		   	            		 xtype:'displayfield',
		   	            		 fieldLabel: 'Job Id',
		   	            		 readOnly :true,
		   	            		 name: 'id',
		   	            		 width : 255,
		   	            		tabIndex:1,
		   	            	 },{
		   	            		 xtype:'displayfield',
		   	            		 fieldLabel: 'Requirement No',
		   	            		 readOnly :true,
		   	            		 name: 'requirementNo',
		   	            		 labelWidth : 120,
		   	            		 width : 300,
		   	            		tabIndex:2,
		   	            	 }]
		   	             },{
		   	    	    	xtype: 'container',
		   	    	    	layout :'table',
		   	    	    	items: [{
		   	    	    		xtype:'datefield',
		   	    	    		fieldLabel: 'Posting Date',
		   	    	    		name: 'postingDate',
		   	    	    		tabIndex:3,
		   	    	    	},{
		   	    	    		xtype: 'timefield',
		   	    	    		labelWidth : 120,
		   	    	    		increment: 1,
		   	    	    		anchor: '100%',
		   	    	    		fieldLabel: 'Time',
		   	    	    		name: 'postingTime',
		   	    	    		tabIndex:4,
		   	    	    	}]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout :'table',
		   	    	    	items: [{
			   	                 xtype:'textfield',
			   	                 fieldLabel: 'Posting Title',
			   	                 name: 'postingTitle',
			   	                 tabIndex:5,
			   	                 maxLength:100,
			   	                 enforceMaxLength:true,
			   	             },{
			   	                 xtype:'textfield',
			   	                 labelWidth : 120,
			   	                 fieldLabel: "Alert",
			   	                 name: 'addInfo_Todos',
			   	                 fieldStyle: "color: red;",
			   	                 tabIndex:6,
			   	                 maxLength:45,
			   	                 enforceMaxLength:true,
			   	             }]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout :'table',
		   	    	    	items: [me.hotnessCombo,me.jobOpeningStatusCombo]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout :'table',
		   	    	    	items: [{
			   	                 xtype:'textfield',
			   	                 fieldLabel: 'City & State',
			   	                 name: 'cityAndState',
			   	                 tabIndex:9,
			   	                 maxLength:100,
			   	                 enforceMaxLength:true,
			   	             },
			   	             me.locationCombo]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout :'table',
		   	    	    	items: [me.moduleCombo,
		   	    	    	        {
			   	                 xtype:'textfield',
			   	                 fieldLabel: 'Selected Module',
			   	                 name: 'module',
			   	                 width : 420,
			   	                 labelWidth : 120,
			   	                 maxLength:300,
			   	                 enforceMaxLength:true,
			   	                 readOnly : true,
			   	             }]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout :'table',
		   	    	    	items: [me.secondaryModuleCombo,
		   	    	    	        {
			   	                 xtype:'textfield',
			   	                 fieldLabel: 'Selected Secondary Module',
			   	                 name: 'secondaryModule',
			   	                 width : 420,
			   	                 labelWidth : 120,
			   	                 maxLength:200,
			   	                 enforceMaxLength:true,
			   	                 readOnly : true,
			   	             }]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout :'table',
		   	    	    	items: [me.employerCombo,me.resumeCombo]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout :'table',
		   	    	    	items: [{
			   	                 xtype:'textfield',
			   	                 fieldLabel: 'Max Bill Rate',
			   	                 name: 'rateSubmitted',
			   	                 tabIndex:14,
			   	                 maxLength:75,
			   	                 enforceMaxLength:true,
			   	             },{
			   	                 xtype:'textfield',
			   	                 fieldLabel: 'Source',
			   	                 name: 'source',
			   	                 tabIndex:15,
			   	                 maxLength:45,
			   	                 labelWidth : 120,
			   	                 enforceMaxLength:true,
			   	             }]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	layout :'table',
		   	    	    	items: [me.recruiterCombo]
		   	    	    }
		   	             ]
		   	         }]
		   	     },{
	   	   	    	 xtype:'fieldset',
	   	   	    	 title : 'Travel & Relocation Needed',
		   	         collapsible: false,
		   	         layout: 'column',
		   	         items :[{
		   	    	    	xtype: 'container',
		   	    	    	padding : '10 0 10 100',
		   	    	    	layout :'table',
		   	    	    	items: [me.relocateCheckbox,me.travelCheckbox,me.remoteCheckbox,]
		   	    	    },{
		   	    	    	xtype: 'container',
		   	    	    	padding : '0 0 10 100',
		   	    	    	layout :'table',
		   	    	    	items: [me.tAndEPaidCheckbox,me.tAndENotPaidCheckbox]
		   	    	    }]
		   	     },{
	   	   	    	 xtype:'fieldset',
	   	   	    	 title : 'Job Description',
		   	         collapsible: false,
		   	         layout: 'column',
		   	         items :[{
		   	             xtype: 'container',
		   	             columnWidth:1,
		   	             items: [{
		   	            	 xtype: 'container',
		   	            	 padding : '0 0 10 0',
		   	            	 layout :'table',
		   	            	 items: [me.clientCombo,
		   	            	 {
		   	            		 xtype: 'button',
		   	            		 margin : '0 10 5 10',
		   	            		 icon : 'images/Popout.ico',
		   	            		 tooltip : 'View Snapshot',
		   	            		 tabIndex:23,
		   	            		 scope: this,
		   	            		 handler : function(grid, rowIndex, colIndex) {
		   	            			 var clientId = me.clientCombo.getValue();
		   	            			 if (clientId != null && clientId != '') {
		   	            				 app.homeMgmtPanel.showSnapShot(clientId, 'Client');
		   	            			 }
		   	            		 } 
		   	            	 },{
		   	            		 xtype:'textfield',
		   	            		 fieldLabel: 'Public Link',
		   	            		 name: 'publicLink',
		   	            		 vtype:'url',
		   	            		 tabIndex:24,
		   	            		 maxLength:200,
		   	            		 labelWidth : 120,
		   	            		 enforceMaxLength:true,
		   	            	  }
		   	            	  ]
		   	             },
		   	             me.submittedVendors,
           		         {
		   	            	 border : false,
		   	            	 height: 5,
           		         },
           		         me.suggstedVendors,
		   	             {
		   	            	 xtype: 'container',
		   	            	padding : '10 0 0 0',
		   	            	 layout :'table',
		   	            	 items: [me.helpCandidateCombo,
		   	            	         {
		   	            		 xtype: 'textarea',
		   	            		 fieldLabel: 'Comments',
		   	            		 name: 'helpCandidateComments',
		   	            		 hidden : true,
		   	            		 tabIndex:26,
		   	            		 width : 420,
		   	            		 height : 35,
		   	            		 maxLength:100,
		   	            		 labelWidth : 120,
		   	            		 enforceMaxLength:true,
		   	            	  }
		   	            	  ]
		   	             },{
		   	            	 xtype: 'container',
		   	            	 layout :'table',
		   	            	 items: [me.skillSetCombo,
		   	            	         {
		    	             		xtype: 'textarea',
		       	             		fieldLabel: 'Selected Skill Set',
		       	             		name: 'skillSet',
		       	             		readOnly : true,
		       	             		maxLength:2000,
		       	             		width : 420,
		       	             		labelWidth : 120,
		       	             		height : 40,
		       	             	},{
		       	             		margin : '0 0 0 10',
		       	             		xtype: 'button',
		       	             		icon : 'images/icon_lookup_down.gif',
		       	             		tooltip : 'Copy to certifications',
		       	             		tabIndex : 45,
		       	             		hidden : true,
		       	             		handler: function() {
		       	             			me.copyToCertifications();
		       	             		}
		       	             	}
		   	            	  ]
		   	             },{
		   	            	 xtype: 'container',
		   	            	 layout :'table',
		   	            	 padding : '0 0 10 0',
		   	            	 items: [{
		   	            		 xtype: 'textfield',
		   	            		 fieldLabel: 'Skillset Ids',
		   	            		 name: 'skillSetIds',
		   	            		 readOnly : true,
		   	            		 hidden : true,
		   	            	 }]
		   	             },{
		   	            	 xtype: 'container',
		   	            	 layout :'table',
		   	            	 items: [me.communicationCombo,me.experienceCombo]
		   	             },{
		   	            	 xtype: 'container',
		   	            	 layout :'table',
		   	            	 items: [me.targetRolesCombo,
		   	            	         {
	   	            		 		xtype: 'textarea',
	   	            		 		fieldLabel: 'Selected Role set',
	   	            		 		name: 'targetRoles',
	   	            		 		readOnly : true,
	   	            		 		maxLength:1000,
	   	            		 		width : 420,
	   	            		 		labelWidth : 120,
	   	            		 		height : 40,
		   	            	    }]
		   	             },{
		   	            	 xtype: 'container',
		   	            	 layout :'table',
		   	            	 padding : '0 0 10 0',
		   	            	 items: [me.certificationsCombo,
		   	            	         {
		    	             		xtype: 'textarea',
		       	             		fieldLabel: 'Selected Certifications',
		       	             		name: 'certifications',
		       	             		readOnly : true,
		       	             		maxLength:1000,
		       	             		width : 420,
		       	             		labelWidth : 120,
		       	             		height : 40,
		       	             	},{
		       	             		margin : '0 0 0 10',
		       	             		xtype: 'button',
		       	             		icon : 'images/icon_lookup_up.gif',
		       	             		tooltip : 'Copy to Skillset',
		       	             		tabIndex : 45,
		       	             		hidden : true,
		       	             		handler: function() {
		       	             			me.copyToSkillset();
		       	             		}
		       	             	},{
		       	             		xtype: 'textfield',
		       	             		fieldLabel: 'Cert Ids',
		       	             		name: 'certificationIds',
		       	             		readOnly : true,
		       	             		hidden : true,
		       	             	}]
		   	             },{
		   	                 xtype:'textarea',
		   	                 fieldLabel: 'Roles and Responsibilities',
		   	                 name: 'requirement',
		   	                 tabIndex:32,
		   	                 maxLength:4000,
		   	                 width:675,
		   	                 maxWidth:675,
		   	                 height:230,
		   	                 enforceMaxLength:true,
		   	                 listeners:{
		   	                	 change:function(field) {
		   	                		 while (field.value.indexOf('"') != -1) {
		   	                			field.value = field.value.replace('"','');	
		   	                		 }
		   	                		 field.setValue(field.value);
		   	                	 }		    
		   	                 }
		   	             },{
		   	                 xtype:'textarea',
		   	                 fieldLabel: 'Comments',
		   	                 name: 'comments',
		   	                 tabIndex:33,
		   	                 maxLength:1200,
		   	                 width : 675,
			                 height : 80,
		   	                 enforceMaxLength:true,
		   	             },{
		   	     	        xtype: 'datefield',
		   	    	        hidden:true,
		   	    	        name: 'created'
		   	             },{
		   	                 xtype:'textarea',
		   	                 name: 'researchComments',
		   	                 hidden:true,
		   	             },{
		   	            	 xtype:'numberfield',
		   	            	allowDecimals: false,
		   	            	 hidden : true,
		   	            	 name: 'version',
		   	            	 value: 0
		   	             }]
		   	         }]
		   	     }]
	     });
		
        me.candidatePanel = Ext.create('Ext.form.Panel', {
        	title : 'Add Candidate',
        	bodyPadding: 10,
        	border : 1,
        	frame:false,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 160},
        	xtype: 'container',
        	dockedItems :  [{
	        	 xtype: 'toolbar',
	        	 dock: 'top',
	        	 items: [{
	        		 xtype: 'button',
	        		 text: 'Save Candidate',
	        		 tooltip: 'Save Candidate',
	        		 tabIndex : 101,
	        		 iconCls : 'btn-save',
	        		 scope: this,
	        		 handler: this.saveCandidate
	        	 },'-',{
	        		 xtype: 'button',
	        		 text: 'Reset',
	        		 tooltip: 'Reset Candidate',
	        		 tabIndex : 102,
	        		 scope: this,
	        		 handler: function(){
                     	this.candidatePanel.form.reset();
                     }
	        	 }]
            }],
        	
	         items:[
	                {
				xtype : 'fieldset',
				border : 0,
				title : '',
				defaultType : 'textfield',
				layout : 'column',
				items : [
				         {
	             xtype: 'container',
	             columnWidth:.9,
	             items: [{
   	                 xtype:'textfield',
   	                 fieldLabel: 'First Name *',
   	                 name: 'firstName',
   	                 allowBlank:false,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	                 tabIndex:90,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: 'Last Name *',
   	                 name: 'lastName',
   	                 allowBlank:false,
   	                 tabIndex:91,
   	                 maxLength:45,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "City & State",
   	                 name: 'cityAndState',
   	                 tabIndex:92,
   	                 maxLength:90,
   	                 enforceMaxLength:true,
   	             },{
   	                 xtype:'textfield',
   	                 fieldLabel: "Expected Rate",
   	                 name: 'expectedRate',
   	                 tabIndex:93,
   	                 maxLength:100,
   	                 enforceMaxLength:true,
   	             },
   	             me.openToCTHCombo,
   	             {
   	                 xtype:'datefield',
   	                 fieldLabel: 'Availability',
   	                 name: 'availability',
   	                 tabIndex:95,
   	             },
   	             me.immigrationCombo,me.marketingStatusCombo,me.employmentTypeCombo,
   	             {
   	                 xtype:'textarea',
   	                 fieldLabel: "Selected Employment Type",
   	                 name: 'selectedEmploymentType',
   	                 readOnly : true,
   	                 width : 500,
   	                 height : 40,
   	             },
   	             me.aboutPartnerCombo,me.personalityCombo
   	             ]
	         }]
	        }]
	     });

        me.criteriaPanel = Ext.create('Ext.form.Panel', {
        	frame:false,
        	xtype: 'container',
        	bodyPadding: 10,
        	border : 1,
        	items:[{
        		xtype : 'fieldset',
				border : 0,
				layout : 'column',
				items : [{
	   	        	 xtype: 'container',
	   	        	 columnWidth:1,
	   	             items: [{
		 					xtype:'displayfield',
							fieldLabel: '',
							height : 10,
							name: 'criteriaMask',
							value : "Loading..." 
						},{
							xtype:'displayfield',
							fieldLabel: '',
							height : 10,
							name: 'client',
							value : "<b>Special criteria : </b><br><table cellpadding='2'>" +
									"<tr><td><div id='criteria_client'/></td></tr>" +
									"<tr><td><div id='criteria_clientAll'/></td></tr>" +
									"<tr><td><div id='criteria_location'/></td></tr>" +
									"<tr><td><div id='criteria_certification'/></td></tr>" +
									"<tr><td><div id='criteria_vendor'/></td></tr>" +
									"<tr><td><div id='criteria_project'/></td></tr>" +
									"</table>",
						}]
				}]
        	}]
	     });

        me.items = [this.getMessageComp(), this.getHeadingComp(),
                    {
        				border : false,
        				height: 10,
                    },
            {
	   	    	 xtype:'fieldset',
	   	    	 border : 0,
	   	         collapsible: false,
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.5,
	   	             padding : '0 10 0 0',
	   	             items: [me.requirementPanel]
	   	         },{
	   	        	 xtype: 'container',
	   	        	 columnWidth:.5,
	   	             items: [{
	            			 xtype: 'container',
	            			 padding : '0 10 0 10',
   	            			 layout : 'fit',
   	            			 items: [{
   	            				 xtype:'textarea',
   	            				 fieldLabel: 'Overall Research Comments',
   	            				 name: 'overallResearchComments',
   	            				 tabIndex:60,
   	            				 maxLength:300,
   	            				 labelWidth:90,
   	            				 width:400,
   	            				 height:40,
   	            				 enforceMaxLength:true,
   	            				 listeners:{
   	            					 change:function(field) {
   	            						 me.requirementPanel.form.findField('researchComments').setValue(field.value);
   	            					 }		    
   	            				 }
   	            			 }]
	   	             },{
	   	            	 border : false,
	   	            	 height : 5,
	   	             },{
	   	            	 xtype:'fieldset',
	   	            	 bodyPadding: 0,
	   	            	 border : 0,
	   	            	 collapsible: false,
	   	            	 layout: 'column',
	   	            	 items :[{
	   	            		 xtype: 'container',
	   	            		 columnWidth:.5,
	   	            		 padding : '0 5 0 0',
	   	            		 items: [me.researchGridPanel]
	   	            	 },{
	   	            		 xtype: 'container',
	   	            		 columnWidth:.5,
	   	            		 items: [me.reqSmeGridPanel,]
	   	            	 }]
	   	             },
    		         me.potentialCandidates,
    		         {
    			 		border : false,
    			 		height : 5,
    		         },
	   	             me.criteriaPanel,
    		         {
    		        	 border : false,
    		        	 height : 5,
    		         },
    		         me.shortlistedCandidates,
    		         {
     			 		border : false,
     			 		height : 5,
     		         },
	   	             me.submittedCandidates,
	   	             {
	   	            	 xtype: 'fieldset',
	   	            	 title:'Add Candidate',
	   	            	 collapsible: true,
	   	            	 collapsed : true,
	   	            	 columnWidth:.99,
	   	            	 items: [{
	   	            		 xtype: 'container',
	   	            		 columnWidth:1,
	   	            		 items: [me.candidatePanel]
	   	            	 }]
	   	             }]
	   	         }]
	   	     }
        ];

        me.callParent(arguments);
    },

    shortlistCandidate : function(dat) {
    	var selectedRecord = this.potentialCandidates.getView().getSelectionModel().getSelection()[0];
    	if (selectedRecord == null) {
    		this.body.scrollTo('top', 0);
    		this.updateError('Please select a candidate.');
    		return false;
		}
		var selectedRecord = this.potentialCandidates.getView().getSelectionModel().getSelection()[0];
		var requirementId = this.requirementPanel.form.findField('id').getValue();
		var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId,
   	 		candidateId : selectedRecord.data.id,
   	 		submissionStatus : 'Shortlisted',
   	 	});
   	 	this.shortlistedCandidates.store.insert(0, rec);
        this.shortlistedCandidates.getView().focusRow(0);
        
        this.shortlistedCandidates.store.filterBy(function(rec) {
        	return rec.get('deleted') != true;
        });

		for ( var i = 0; i < this.shortlistedCandidates.store.data.length; i++) {
			var candidate1 = this.shortlistedCandidates.store.getAt(i);
			for ( var j = 0; j < this.shortlistedCandidates.store.data.length; j++) {
				var candidate2 = this.shortlistedCandidates.store.getAt(j);
				if (candidate1 != candidate2 && candidate1.data.candidateId == candidate2.data.candidateId) {
					this.body.scrollTo('top', 0);
					this.updateError("Candidate already shortlisted");
					this.loadShortlisted(requirementId);
					return false;
				}
			}
		}
		this.shortlistedCandidates.store.clearFilter();
        this.validatesShortlist();
	},
    
	submitCandidate : function(candidateId) {
		var requirementId = this.requirementPanel.form.findField('id').getValue();
		var vendorId = null;
		if (this.submittedVendors.store.getCount() == 1) {
			vendorId = this.submittedVendors.store.getAt(0).data.vendorId;
		}
		var candidate  = ds.candidateSearchStore.getById(candidateId);
    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId,
   	 		vendorId : vendorId,
   	 		candidateId : candidateId,
   	 		submittedDate : new Date(),
			candidate : candidate.data.fullName,
			candidateExpectedRate : candidate.data.expectedRate,
			cityAndState : candidate.data.cityAndState,
			relocate : candidate.data.relocate,
			travel : candidate.data.travel,
			availability : candidate.data.availability,
			vendor : candidate.data.vendor
   	 	});
   	 	ds.subRequirementStore.insert(0, rec);
        this.submittedCandidates.getView().focusRow(0);
        this.submittedCandidates.getView().refresh();
        
        ds.subRequirementStore.filterBy(function(rec) {
        	return rec.get('deleted') != true;
        });

    	for ( var i = 0; i < ds.subRequirementStore.data.length ; i++) {
			var record = ds.subRequirementStore.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				for ( var j = 0; j < ds.subRequirementStore.getCount(); j++) {
					var oldRec = ds.subRequirementStore.getAt(j);
					if (oldRec != record && oldRec.data.vendorId == record.data.vendorId && oldRec.data.candidateId == record.data.candidateId) {
						this.body.scrollTo('top', 0);
						this.updateError("Candidate already submitted through given vendor.");
						this.loadSubRequirments(requirementId);
						return false;
					}
				}
			}
		}
    	ds.subRequirementStore.clearFilter();
    	this.validatesCandidates();
    	this.verifyCandidateIds.push(candidateId);
	},
	
    addShortlist : function() {
    	var requirementId = this.requirementPanel.form.findField('id').getValue();
    	if (requirementId == null || requirementId == '') {
    		this.body.scrollTo('top', 0);
    		this.updateError("Please save Job opening");
    		return false;
		}
    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId,
   	 		submissionStatus : 'Shortlisted',
   	 	});
   	 	this.shortlistedCandidates.store.insert(0, rec);
        this.shortlistedCandidates.getView().focusRow(0);
	},
 
	validatesShortlist : function() {
		var immigratioVerified = true;
        this.shortlistedCandidates.store.filterBy(function(rec) {
        	return rec.get('deleted') != true;
        });

		for ( var i = 0; i < this.shortlistedCandidates.store.data.length; i++) {
			var candidate1 = this.shortlistedCandidates.store.getAt(i);
			for ( var j = 0; j < this.shortlistedCandidates.store.data.length; j++) {
				var candidate2 = this.shortlistedCandidates.store.getAt(j);
				if (candidate1 != candidate2 && candidate1.data.candidateId == candidate2.data.candidateId) {
					this.body.scrollTo('top', 0);
					this.updateError("Candidate already shortlisted");
					return false;
				}
			}
		}
		this.shortlistedCandidates.store.clearFilter();
		
    	var records = new Array();
    	for ( var i = 0; i < this.shortlistedCandidates.store.data.length ; i++) {
			var record = this.shortlistedCandidates.store.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.modifiedShortlistIds.indexOf(record.data.id) != -1) {
	    		if (record.data.candidateId == 0 || record.data.candidateId == null || record.data.candidateId == ''){
					this.body.scrollTo('top', 0);
					this.updateError("Please select the candidate");
					return false;
				}
				//check for immigration verified
				var candidate = ds.candidateSearchStore.getById(record.data.candidateId);
				if (candidate.data.immigrationVerified != 'YES'  && candidate.data.immigrationVerified != 'Not Needed' )
					immigratioVerified = false;

    			records.push(record.data);
			}
		}
    	if (records.length == 0) {
    		this.body.scrollTo('top', 0);
    		this.updateError("There are no changes in shortlists");
		}else if (immigratioVerified == false) {
			var panel = this;
			Ext.MessageBox.show({
				title:'Confirm',
	            msg: 'You are trying to shortlist a candidate whose immigration is not verified.<br>Do you want to shortlist Candidates?',
	            buttonText: {yes: 'No',no: 'Yes'},
	            icon: Ext.MessageBox.QUESTION,
	            fn: function(btn) {
	            	if (btn == 'no') {
	            		panel.saveShortlist();
	            	}else if (btn == 'yes') {
	            		panel.removeVerified(panel.shortlistedCandidates.store,panel.verifyShortlistIds);
	            		panel.shortlistedCandidates.getView().refresh();
					}
	            }
	        });
		}else{
			this.saveShortlist();
		}
	},
	
	saveShortlist : function() {
    	var records = new Array();
    	for ( var i = 0; i < this.shortlistedCandidates.store.data.length ; i++) {
			var record = this.shortlistedCandidates.store.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.modifiedShortlistIds.indexOf(record.data.id) != -1) {
	    		if (record.data.id == 0 || record.data.id == null)
	    			delete record.data.id;
	    		delete record.data.vendorId;
    			records.push(record.data);
			}
		}
    	if (records.length == 0) {
    		this.body.scrollTo('top', 0);
    		this.updateError("There are no changes in shortlists");
		}else{
	    	records = Ext.JSON.encode(records);
	    	app.requirementService.saveShortlisted(records,this.onSaveShortlisted,this);
		}
	},
	
	onSaveShortlisted : function(data) {
		this.clearMessage();
		if (data.success) {
			if (this.verifyShortlistIds.length > 0) {
	        	var params = new Array();
	        	params.push(['candidateId','=', this.verifyShortlistIds.toString()]);
	        	params.push(['jobId','=', this.requirementPanel.form.findField('id').getValue()]);
	        	var filter = getFilter(params);
				app.requirementService.immigrationVerifyEmails(Ext.JSON.encode(filter));	
			}
			this.verifyShortlistIds = new Array();
			this.modifiedShortlistIds = new Array();
			this.loadShortlisted(this.requirementPanel.form.findField('id').getValue());
			this.updateMessage('Candidate Shortlisted Successfully');
			if (Ext.getCmp('guideWindow') != null)
				Ext.getCmp('guideWindow').close();
		}else {
			this.updateError(data.errorMessage);
			this.loadShortlisted(this.requirementPanel.form.findField('id').getValue());
			this.body.scrollTo('top', 0);
		}
		this.shortlistedCandidates.getSelectionModel().deselectAll();
	},

	removeShortlistConfirm : function(record) {
		this.removeRecord = record;
		if (record.data.id == 0 || record.data.id == null){
			this.shortlistedCandidates.store.remove(record);
		}else{
			this.removeShortlist("yes");
			//Ext.Msg.confirm("Confirm.", "Do you want to remove selected Candidate from the Job Opening?", this.removeShortlist, this);
		}
	},

	removeShortlist : function(dat) {
		if (dat=='yes') {
			var record= this.removeRecord;
			var idVal  = record.data.id;
			app.requirementService.removeShortlist(idVal,this.onRemoveShortlist,this);
		}
	},
	
	onRemoveShortlist : function(data) {
		this.clearMessage();
		if (data.success) {
			this.modified = true;
			this.loadShortlisted(this.requirementPanel.form.findField('id').getValue());
		}else {
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
	},
	
    addVendor : function() {
    	var requirementId = this.requirementPanel.form.findField('id').getValue();
    	/*if (requirementId == null || requirementId == '') {
    		this.body.scrollTo('top', 0);
    		this.updateError("Please save Job opening");
    		return false;
		}*/
    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId
   	 	});
   	 	ds.subVendorStore.insert(0, rec);
        this.submittedVendors.getView().focusRow(0);
	},
	
	addCandidate : function() {
    	var requirementId = this.requirementPanel.form.findField('id').getValue();
    	if (requirementId == null || requirementId == '') {
    		this.body.scrollTo('top', 0);
    		this.updateError("Please save Job opening");
    		return false;
		}
    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId,
   	 		submittedDate : new Date()
   	 	});
   	 	ds.subRequirementStore.insert(0, rec);
        this.submittedCandidates.getView().focusRow(0);
	},
    
	saveVendors : function() {
    	var requirementId = this.requirementPanel.form.findField('id').getValue();
    	if (requirementId == null || requirementId == '') {
    		this.saveConfirm();
    		return false;
    	}

		for ( var i = 0; i < ds.subVendorStore.data.length; i++) {
			var vendor1 = ds.subVendorStore.getAt(i);
			for ( var j = 0; j < ds.subVendorStore.data.length; j++) {
				var vendor2 = ds.subVendorStore.getAt(j);
				if (vendor1 != vendor2 && vendor1.data.vendorId == vendor2.data.vendorId) {
					this.body.scrollTo('top', 0);
					this.updateError("Vendor already exists");
					return false;
				}
			}
		}
		for ( var i = 0; i < ds.subVendorStore.data.length; i++) {
			var record = ds.subVendorStore.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.modifiedVendorIds.indexOf(record.data.id) != -1) {
				var vendor = ds.vendorStore.getById(record.data.vendorId);
				var helpCandidate = this.helpCandidateCombo.getValue();
				if (record.data.requirementId == null || record.data.requirementId == 0 || record.data.requirementId == '')
					record.data.requirementId = requirementId;
				if (vendor.data.helpCandidate!=null && vendor.data.helpCandidate!='' && helpCandidate != vendor.data.helpCandidate){
					this.helpCandidateCombo.setValue(vendor.data.helpCandidate);
					Ext.Msg.alert('Warning','Ok to train candidate is overwritten by vendor '+vendor.data.name+', Please verify.');
					break;
				}
			}
		}

		var records = new Array();
    	
    	for ( var i = 0; i < ds.subVendorStore.data.length ; i++) {
			var record = ds.subVendorStore.getAt(i);
			
			if (record.data.id == 0 || record.data.id == null || this.modifiedVendorIds.indexOf(record.data.id) != -1) {
	    		if (record.data.id == 0 || record.data.id == null)
	    			delete record.data.id;
	    		if (record.data.vendorId == 0 || record.data.vendorId == null || record.data.vendorId == ''){
					this.body.scrollTo('top', 0);
					this.updateError("Please select the vendor");
					return false;
				}
	    		if (record.data.contactId == 0 || record.data.contactId == null || record.data.contactId == '')
					delete record.data.contactId;
	    		
    			records.push(record.data);
			}
		}
    	if (records.length == 0) {
    		this.body.scrollTo('top', 0);
    		this.updateError("There are no changes to submit");
		}else{
	    	records = Ext.JSON.encode(records);
	    	app.requirementService.saveSubVendors(records,this.onSaveVendors,this);
		}
	},
	
	onSaveVendors : function(data) {
		this.clearMessage();
		if (data.success) {
			this.loadSubVendors(this.requirementPanel.form.findField('id').getValue());
			this.updateMessage('Vendors Submitted Successfully');
			this.modifiedVendorIds = new Array();
		}else {
			this.body.scrollTo('top', 0);
			this.updateError('Save Failed');
		}	
	},
	
	addFromSuggested : function() {
    	var selectedRecord = this.suggstedVendors.getView().getSelectionModel().getSelection()[0];
    	if (selectedRecord == null) {
    		this.body.scrollTo('top', 0);
    		this.updateError('Please select a vendor.');
    		return false;
		}
		var requirementId = this.requirementPanel.form.findField('id').getValue();
		var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		vendorId : selectedRecord.data.id,
   	 		requirementId : requirementId
   	 	});
   	 	this.submittedVendors.store.insert(0, rec);
        this.submittedVendors.getView().focusRow(0);
        this.modifiedVendorIds.push(null);
        
		for ( var i = 0; i < this.submittedVendors.store.data.length; i++) {
			var vendor1 = this.submittedVendors.store.getAt(i);
			for ( var j = 0; j < this.submittedVendors.store.data.length; j++) {
				var vendor2 = ds.subVendorStore.getAt(j);
				if (vendor1 != vendor2 && vendor1.data.vendorId == vendor2.data.vendorId) {
					this.body.scrollTo('top', 0);
					this.updateError("Vendor already exists");
					this.loadSubVendors(requirementId);
					return false;
				}
			}
		}        
        this.saveVendors();
	},
	
	validatesCandidates : function() {
		var immigratioVerified = true;
    	var records = new Array();
        ds.subRequirementStore.filterBy(function(rec) {
        	return rec.get('deleted') != true;
        });
        
    	for ( var i = 0; i < ds.subRequirementStore.data.length ; i++) {
			var record = ds.subRequirementStore.getAt(i);
			
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				if (record.data.candidateId == 0 || record.data.candidateId == null || record.data.candidateId == ''){
					this.body.scrollTo('top', 0);
					this.updateError("Please select a candidate");
					return false;
				}
				if (record.data.vendorId == 0 || record.data.vendorId == null || record.data.vendorId == ''){
					this.body.scrollTo('top', 0);
					this.updateError("Please select the vendor for the candidate");
					return false;
				}
				for ( var j = 0; j < ds.subRequirementStore.getCount(); j++) {
					var oldRec = ds.subRequirementStore.getAt(j);
					if (oldRec != record && oldRec.data.vendorId == record.data.vendorId && oldRec.data.candidateId == record.data.candidateId) {
						this.body.scrollTo('top', 0);
						this.updateError("Candidate already submitted through given vendor.");
						return false;
					}
				}
				//check for immigration verified
				var candidate = ds.candidateSearchStore.getById(record.data.candidateId);
				if (candidate.data.immigrationVerified != 'YES'  && candidate.data.immigrationVerified != 'Not Needed' )
					immigratioVerified = false;
				records.push(record.data);
			}
		}
    	ds.subRequirementStore.clearFilter();
    	if (records.length == 0) {
    		this.body.scrollTo('top', 0);
    		this.updateError("There are no changes in candidates to submit");
		}else if (immigratioVerified == false) {
			var panel = this;
			Ext.MessageBox.show({
				title:'Confirm',
	            msg: 'You are trying to submit a candidate whose immigration is not verified.<br>Do you want to submit Candidates?',
	            buttonText: {yes: 'No',no: 'Yes'},
	            icon: Ext.MessageBox.QUESTION,
	            fn: function(btn) {
	            	if (btn == 'no') {
	            		panel.saveCandidates();
	            	}else if (btn == 'yes') {
	            		panel.removeVerified(ds.subRequirementStore,panel.verifyCandidateIds);
	            		panel.submittedCandidates.getView().refresh();
					}
	            }
	        });
		}else {
			this.saveCandidates();
		}
	},
	
	removeVerified : function(store,modifiedIds) {
    	for ( var i = 0; i < store.data.length ; i++) {
			var record = store.getAt(i);
			var candidate = ds.candidateSearchStore.getById(record.data.candidateId);
			if (candidate.data.immigrationVerified != 'YES'  && candidate.data.immigrationVerified != 'Not Needed' 
				&& (record.data.id == null || record.data.id == '' || record.data.id == 0 || modifiedIds.indexOf(record.data.id) != -1))
				store.remove(record);
    	}
	},
	
    saveCandidates : function() {
    	var records = new Array();
    	for ( var i = 0; i < ds.subRequirementStore.data.length ; i++) {
			var record = ds.subRequirementStore.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				//if both vendor and candidate is entered check Ok to train candidate in vendor and resume help in candidate are equal
				var vendor = ds.vendorStore.getById(record.data.vendorId).data;
				var candidate  = ds.candidateSearchStore.getById(record.data.candidateId).data;
				if (vendor.helpCandidate != '' && candidate.resumeHelp != '' && vendor.helpCandidate != candidate.resumeHelp ){
					//if vendor will not allow to help candidate and if candidate needs resume help show warning
					Ext.Msg.alert('Warning','"Ok to train candidate" for '+vendor.name+' and '+candidate.fullName+' are not matching. Ensure this before making submission. '+
							'<br>'+vendor.name+' - "'+vendor.helpCandidate+'",<br> '+candidate.fullName+' - "'+candidate.resumeHelp+'"');
				}
	    		if (record.data.id == 0 || record.data.id == null || record.data.id == '')
					delete record.data.id;
	    		
	    		if (record.data.submittedTime != null && record.data.submittedTime != ''){
	    			var time = record.data.submittedTime.split(':');
	    			record.data.submittedDate = new Date(record.data.submittedDate.setHours(record.data.submittedTime.split(':')[0]));
	    			record.data.submittedDate = new Date(record.data.submittedDate.setMinutes(record.data.submittedTime.split(':')[1]));
	    		}
				records.push(record.data);
			}
		}
    	if (records.length == 0) {
    		this.body.scrollTo('top', 0);
    		this.updateError("There are no changes in candidates to submit");
		}else{
			this.checkForModule(records);
		}
	},
	
	checkForModule : function(records) {
		var msg = "";
		ds.skillSetStore.clearFilter();
		var modules = this.requirementPanel.form.findField('module').getValue();
		if (modules != null && modules != '') {
			modules = modules.split(',');
			for ( var i = 0; i < records.length; i++) {
				var candidate  = ds.candidateSearchStore.getById(records[i].candidateId);
				if (candidate != null && candidate.data.resumeHelp == '100-Yes') {
					var moduleMsg = "";
					for ( var j = 0; j < modules.length; j++) {
						var moduleRecord = ds.skillSetStore.getAt(ds.skillSetStore.findExact('name',modules[j]));
						var contacts = "";
						if (candidate.data.gender == 'Male')
							contacts = moduleRecord.data.inthelp1;
						else if (candidate.data.gender == 'Female')
							contacts = moduleRecord.data.inthelp2;
						
						if (contacts == null || contacts == "") {
							moduleMsg += '- '+modules[j]+'<br>';
						}
					}//for
					
					if (moduleMsg != ""){
						msg += candidate.data.fullName+" needs resume help. Following module(s) do not have interview helper <br>"+moduleMsg+'<br>';
					}
					
				}//if
			}//for
		}//if
		
		var panel = this
		if (msg != "") {
			msg += "Do you want to submit the candidate(s)"
			Ext.MessageBox.show({
				title:'Confirm',
	            msg: msg,
	            buttons: Ext.MessageBox.YESNO,
	            icon: Ext.MessageBox.QUESTION,
	            fn: function(btn) {
	            	if (btn == 'yes') {
	                	records = Ext.JSON.encode(records);
	                	app.requirementService.saveSubRequirements(records,panel.onSaveCandidates,panel);
					}
	            }
	        });
		}else{
        	records = Ext.JSON.encode(records);
        	app.requirementService.saveSubRequirements(records,panel.onSaveCandidates,panel);
		}
		
	},
	
	onSaveCandidates : function(data) {
		if (data.success) {
			var jobId = this.requirementPanel.form.findField('id').getValue();
			this.loadSubRequirments(jobId);
			this.loadShortlisted(jobId);
			this.updateMessage('Candidates Saved Successfully');
			if (this.verifyCandidateIds.length > 0) {
	        	var params = new Array();
	        	params.push(['candidateId','=', this.verifyCandidateIds.toString()]);
	        	params.push(['jobId','=', jobId]);
	        	var filter = getFilter(params);
				app.requirementService.immigrationVerifyEmails(Ext.JSON.encode(filter));	
			}
			this.verifyCandidateIds = new Array();
			this.modifiedCandidateIds = new Array();
			if (this.CAcceptedIds.length > 0) {
				app.interviewService.insertProjects(this.CAcceptedIds);
			}
			if (this.ISucceededIds.length > 0) {
				app.interviewService.interviewSucceedEmail(this.ISucceededIds);
			}
			
			this.ISucceededIds = new Array();
			if (Ext.getCmp('guideWindow') != null)
				Ext.getCmp('guideWindow').close();
			
			if (Ext.getCmp('referenceWindow') != null)
				Ext.getCmp('referenceWindow').close();
		}else {
			if (data.errorMessage == 'Candidate already submitted.Please refresh.'){
				Ext.Msg.alert('Error',data.errorMessage);
				this.loadSubRequirments(this.requirementPanel.form.findField('id').getValue());
			}else{
				Ext.Msg.alert('Error','Save Failed');	
			}
			this.body.scrollTo('top', 0);
		}
		this.submittedCandidates.getSelectionModel().deselectAll();
	},

	deleteTaskConfirm : function()
	{
		var selectedRecord = this.researchGridPanel.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Task to delete.');
		} else if (selectedRecord.data.id == null){
			this.researchGridPanel.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Task","Do you want to delete selected Task?", this.deleteTask, this);
		}
	},
	
	deleteTask: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.researchGridPanel.getView().getSelectionModel().getSelection()[0];
			app.requirementService.deleteTask(selectedRecord.data.id, this.onDeleteTask, this);
		}
	},
	
	onDeleteTask : function(data){
		this.clearMessage();
		if (!data.success) { 
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			this.loadTasks(this.requirementPanel.form.findField('id').getValue());
		}	
	},
	
    saveTask : function() {
		var requirementId = this.requirementPanel.form.findField('id').getValue();
    	if (requirementId == null || requirementId == '') {
    		this.body.scrollTo('top', 0);
    		this.updateError("Please save Job opening");
    		return false;
		}

    	var records = new Array();
    	
    	for ( var i = 0; i < ds.researchStore.data.length ; i++) {
			var record = ds.researchStore.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}
    		if (record.data.requirementId == 0 || record.data.requirementId == null)
				delete record.data.requirementId;
    		
    		if (this.modifiedResearchIds.indexOf(record.data.id) != -1) {
    			records.push(record.data);
			}
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Tasks to save');
    		return;
		}
    	records = Ext.JSON.encode(records);
    	app.requirementService.saveTask(records,this.onSaveTask,this);
    	
	},
    
	onSaveTask : function(data) {
		this.clearMessage();
		if (data.success) {
			this.loadTasks(this.requirementPanel.form.findField('id').getValue());
			this.updateMessage('Saved Successfully');
		}else {
			this.body.scrollTo('top', 0);
			this.updateError('Save Failed');
		}	
	},
	
    addTask : function() {
		if (this.requirementPanel.form.findField('id').getValue() == null) {
			this.body.scrollTo('top', 0);
			this.updateError('Please Save the Job opening before linking.');
			return false;
		}

	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
	    var reqId= this.requirementPanel.form.findField('id').getValue();
	    var serialNo = this.researchGridPanel.store.max('serialNo');
	    if (serialNo == null)
	    	serialNo = 0;
   	 	var rec = Ext.create( 'tz.model.Research',{
   	 		id : null,
   	 		date : nd,
   	 		requirementId: reqId,
   	 		serialNo : serialNo + 1,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});

        this.researchGridPanel.store.insert(0, rec);
        this.researchGridPanel.getView().focusRow(0);
	},

	removeVendorConfirm : function(record) {
		this.removeRecord = record;
		if (record.data.id == 0 || record.data.id == null){
			ds.subVendorStore.remove(record);
		}else{
			for ( var i = 0; i < ds.subRequirementStore.getCount(); i++) {
				var rec = ds.subRequirementStore.getAt(i);
				if (rec.data.vendorId == record.data.vendorId && record.data.candidateId != 0) {
					this.body.scrollTo('top', 0);
					this.updateError('Candidates associated to selected Vendor.');
					return false;
				}
			}
			Ext.Msg.confirm("Confirm.", "Do you want to remove selected vendor from the Job Opening?", this.removeVendor, this);
		}
	},

	removeVendor : function(dat) {
		if (dat=='yes') {
			var record= this.removeRecord;
			var isVal  = record.data.id;
			app.requirementService.removeVendor(isVal,this.onRemoveVendor,this);
		}
	},
	
	onRemoveVendor : function(data) {
		this.clearMessage();
		if (data.success) {
			this.modified = true;
			this.loadSubVendors(this.requirementPanel.form.findField('id').getValue());
		}else {
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
	},
	
	removeCandidateConfirm : function(record) {
		this.removeRecord = record;
		if (record.data.id == 0 || record.data.id == null)
			ds.subRequirementStore.remove(record);
		else
			this.removeCandidate('yes');
			//Ext.Msg.confirm("Confirm.", "Do you want to remove selected candidate from the Job Opening?", this.removeCandidate, this);
	},
	
	removeCandidate : function(dat) {
		if (dat=='yes') {
			var record= this.removeRecord;
			app.requirementService.removeCandidate(record.data.id,this.onRemoveCandidate,this);
		}
	},

	onRemoveCandidate : function(data) {
		this.clearMessage();
		if (data.success) {
			this.modified = true;
			this.loadSubRequirments(this.requirementPanel.form.findField('id').getValue());
		}else {
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
	},
	
    saveCandidate : function() {
		this.clearMessage();
		if(! this.candidatePanel.form.isValid()){
			this.body.scrollTo('top', 0);
			this.updateError('Please fix the errors.');
			return false;
		}
		var itemslength = this.candidatePanel.form.getFields().getCount();
   		var i = 0;    		
   		var candidate = {};
   		
   		while (i < itemslength) {
   			var fieldName = this.candidatePanel.form.getFields().getAt(i).name;
			candidate[fieldName] = this.candidatePanel.form.findField(fieldName).getValue();
   			i = i + 1;
   		}
   		if (candidate.priority == null || candidate.priority == '' )
   			delete candidate.priority;

   		candidate.employmentType = candidate.selectedEmploymentType;
		while (candidate.employmentType.indexOf(', ') != -1) {
			candidate.employmentType = candidate.employmentType.replace(', ',',');
		}
   		delete candidate.selectedEmploymentType;
		var candidateObj = Ext.JSON.encode(candidate);
		app.candidateService.saveCandidate(candidateObj, this.onSaveCandidate, this);
	},

	onSaveCandidate: function(data){
		this.clearMessage();
		if(!data.success){
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}else{
			this.candidatePanel.form.reset();
	    	var params = new Array();
	    	var filter = getFilter(params);
	    	filter.pageSize=1000;
	    	app.loadMask.show();
	    	ds.candidateSearchStore.loadByCriteria(filter);
	    	ds.contractorsStore.loadByCriteria(filter);
	        ds.candidateSearchStore.on('load', function(store, records, options) {
	        	app.loadMask.hide();
	       	}, this);
		}
		this.body.scrollTo('top', 0);
	},

    loadForm : function(record) {
    	this.modified = false;
    	this.closeButton = false;
    	this.modifiedCandidateIds = new Array();
    	this.modifiedShortlistIds = new Array();
    	this.modifiedVendorIds = new Array();
    	this.requirement = record;
    	this.clearMessage();
    	ds.clientSearchStore.clearFilter();
    	this.skillSetCombo.store.clearFilter();
    	this.targetRolesCombo.store.clearFilter();
    	this.certificationsCombo.store.clearFilter();
    	this.potentialCandidates.store.removeAll();
    	this.reqSmeGridPanel.store.removeAll();
    	
		if (Ext.getCmp('certificationContainer') != null)
			this.criteriaPanel.remove(Ext.getCmp('certificationContainer'), true);
		if (Ext.getCmp('moduleContainer') != null)
			this.criteriaPanel.remove(Ext.getCmp('moduleContainer'), true);
		if (Ext.getCmp('skillsetContainer') != null)
			this.criteriaPanel.remove(Ext.getCmp('skillsetContainer'), true);

    	this.form.reset();
		this.loadRecord(record);
		this.form.findField('overallResearchComments').setValue(record.data.researchComments);
		this.form.findField('postingTime').setValue(dateToTime(record.data.postingDate));
		if (record.data.skillSetIds != null && record.data.skillSetIds != '') {
			var skillSetIds = record.data.skillSetIds.split(',');
			var skillSet ='';
			for ( var i = 0; i < skillSetIds.length; i++) {
				skillSet += ds.skillSetStore.getById(Number(skillSetIds[i])).data.value;
				if (skillSetIds.length > (i+1))
					skillSet += ",";
			}
			this.requirementPanel.form.findField('skillSet').setValue(skillSet);
		}

		/*if (ds.req_candidateStore.getCount() >0)
			Ext.getCmp('Submitted_Candidates').expand();
		else
			Ext.getCmp('Submitted_Candidates').collapse();*/
		this.deleteButton.enable();
		this.loadTasks(record.data.id);
		this.loadSubRequirments(record.data.id);
		this.loadSubVendors(record.data.id);
		this.loadShortlisted(record.data.id);
		this.loadSuggestedVendors();
		this.reqSmeGridPanel.search();
		
		this.criteriaPanel.form.findField('client').show();
		this.criteriaPanel.form.findField('criteriaMask').hide();

		if (record.data.id == "" || record.data.id == null) 
			this.requirementPanel.form.findField('version').setValue(0);

		if (record.data.id != "" && record.data.id != null){
			this.potentialCandidates.limit = 10;
			this.loadCandidatesStore();
			this.getSpecialCriteria(record.data.id);
		}
	},

	getSpecialCriteria : function(requirementId) {
		this.criteriaPanel.form.findField('client').hide();
		this.criteriaPanel.form.findField('criteriaMask').show();
		if (requirementId != null && requirementId != '')
			app.requirementService.getSuggestedCriteria(requirementId,this.onGetSpecialCriteria,this);
	},
	
	onGetSpecialCriteria : function(data) {
		var specialCriteria = new Array();

		if (data.success && data.returnVal.rows.length > 0) {
			this.criteriaPanel.form.findField('client').show();
			this.criteriaPanel.form.findField('criteriaMask').hide();
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var rec = data.returnVal.rows[i];
				var type = "'"+rec.type+"'";
				if (rec.type == 'location' ){
					if(rec.value > 0)
						document.getElementById('criteria_location').innerHTML = '<a onclick="javascript:app.requirementMgmtPanel.requirementDetailPanel.filterPotential('+type+')">'
							+rec.value + ' Candidates who are local to requirement (1.Active ,2.Yet to Start)</a>';
					else
						document.getElementById('criteria_location').innerHTML = '';
				}else if (rec.type == 'client'){
					if(rec.value > 0)
						document.getElementById('criteria_client').innerHTML = '<a onclick="javascript:app.requirementMgmtPanel.requirementDetailPanel.filterPotential('+type+')">'
							+rec.value+ ' Candidates with Same Client & Same Module Submissions (1.Active ,2.Yet to Start)</a>';
					else
						document.getElementById('criteria_client').innerHTML = '';
				}else if (rec.type == 'certification'){
					if(rec.value > 0)
						document.getElementById('criteria_certification').innerHTML = '<a onclick="javascript:app.requirementMgmtPanel.requirementDetailPanel.filterPotential('+type+')">'
							+rec.value + ' Candidates having certifications that are required for the Job Opening (All Marketing Status)</a>';
					else
						document.getElementById('criteria_certification').innerHTML = '';
				}else if (rec.type == 'clientAll'){
					if(rec.value > 0)
						document.getElementById('criteria_clientAll').innerHTML = '<a onclick="javascript:app.requirementMgmtPanel.requirementDetailPanel.filterPotential('+type+')">'
							+rec.value + ' Candidates with Same Client & Same Module Submissions (All Marketing Status)</a>';
					else
						document.getElementById('criteria_clientAll').innerHTML = '';
				}else if (rec.type == 'vendor'){
					if(rec.value > 0)
						document.getElementById('criteria_vendor').innerHTML = '<a onclick="javascript:app.requirementMgmtPanel.requirementDetailPanel.filterPotential('+type+')">'+
							+rec.value + ' Candidates with Same Vendor & Same Module Submission ( All Marketing Status )</a>';
					else
						document.getElementById('criteria_vendor').innerHTML = '';
				}else if (rec.type == 'project'){
					if(rec.value > 0)
						document.getElementById('criteria_project').innerHTML = '<a onclick="javascript:app.requirementMgmtPanel.requirementDetailPanel.filterPotential('+type+')">'
							+rec.value + ' Candidates with Same Client & Same Module Project ( All Marketing Status )</a>';
					else
						document.getElementById('criteria_project').innerHTML = '';
				}
			}
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var rec = data.returnVal.rows[i];
				if (rec.value > 0) {
					specialCriteria.push({"type":rec.type,"count":rec.value,"candidates" : rec.ids});
				}
			}
		}
		this.specialCriteria = specialCriteria;
		this.criteriaPanel.doLayout();
	},

	filterPotential : function(type) {
    	var params = new Array();
		for ( var i = 0; i < this.specialCriteria.length; i++) {
			if (this.specialCriteria[i].type == type && this.specialCriteria[i].count != "0") {
				params.push(['candidateIds','=', this.specialCriteria[i].candidates]);
				break;
			}
		}
		if (params.length > 0) {
	    	var filter = getFilter(params);
			filter.pageSize=50;
			app.candidateService.getAllCandidates(Ext.JSON.encode(filter),this.onGetCandidates,this);
		}else
			Ext.Msg.alert('Error','Candidates not found for the selected criteria');	
	},
	
	onGetCandidates : function(data) {
		this.potentialCandidates.store.removeAll();
		if (data.rows.length > 0) {
			this.potentialCandidates.store.add(data.rows);
		}
		var record = this.requirement; 
		var reqSkillSet = record.data.skillSet.split(',');
		var reqModule = record.data.module.split(',');
		var reqTargetRoles = record.data.targetRoles.split(',');
		var reqCertifications = record.data.certifications.split(',');
		
		for ( var i = 0; i < this.potentialCandidates.store.data.getCount(); i++) {
			var record = this.potentialCandidates.store.getAt(i).data;
			record = this.calculateMatching(record);
			var priority = 0;
			if (record.matchScore >= 1 ) {
				//If matchScore>=1, the add priority (for 0 - 6, 1 to 4 give 5-priority)
				if (record.priority != null ) {
					if (record.priority == 0 ){
						priority = 6;
					}else if (record.priority >= 1 && record.priority <= 4 ) {
						priority = 5-record.priority;
					}
				}
				record.matchScore = record.matchScore + priority;
				record.matchScoreDetails += '<br>Priority :' + priority; 
			}
		}
		this.potentialCandidates.store.sort('matchScore', 'DESC');
		this.potentialCandidates.getView().refresh();
		this.potentialCandidates.showPotentialCount.setValue(this.potentialCandidates.store.getCount()+' candidates');
		
		this.getPotentialCriteria();
		this.skillWiseCriteria();
	},

	loadSubVendors : function(requirementId) {
    	if (requirementId != null && requirementId != ""){
        	var params = new Array();
        	params.push(['requirementId','=', requirementId]);
        	var filter = getFilter(params);
       		this.vendorStore.removeAll();
        	ds.subVendorStore.loadByCriteria(filter);
    	}else{
    		ds.subVendorStore.removeAll();
       		this.vendorStore.removeAll();
    	}
    	
    	ds.subVendorStore.on('load', function(store, records, options) {
    		var vendorData= new Array();
        	for ( var i = 0; i < records.length; i++) {
				var rec = records[i];
				var vendorName = (ds.vendorStore.getById(rec.data.vendorId)).data.name;
				if(this.vendorStore.getById(rec.data.vendorId)==null)
					vendorData.push({"id":rec.data.vendorId,"name":vendorName});
			}
        	if(vendorData.length >0 ){
        		this.vendorStore.add(vendorData);
        	}        		
        	this.submittedVendorsCount.setValue(store.getCount()+' Vendors');
    	}, this);
	},
	
	loadSuggestedVendors : function() {
		var clientId = this.clientCombo.getValue();
		if (clientId == null || clientId == ''){
			this.suggstedVendors.store.removeAll();	
		}else{
			if (this.clientCombo.getRawValue().toLowerCase() == 'n/a'){
				this.suggstedVendors.store.removeAll();
	        	this.suggestedVendorsCount.setValue('0 Vendors');
			}else{
				var params = new Array();
				params.push(['relatedVendors','=', clientId]);
				var filter = getFilter(params);
				this.suggstedVendors.store.loadByCriteria(filter);
				
				this.suggstedVendors.store.on('load', function(store, records, options) {
		        	this.suggestedVendorsCount.setValue(store.getCount()+' Vendors');
		    	}, this);
			}
		}
	},
	
	loadSubRequirments : function(requirementId) {
    	if (requirementId != null && requirementId != ""){
        	var params = new Array();
        	params.push(['requirementId','=', requirementId]);
        	params.push(['table','=', "submissions"]);
        	var filter = getFilter(params);
        	ds.subRequirementStore.loadByCriteria(filter);
        	
        	ds.subRequirementStore.on('load', function(store, records, options) {
    			for ( var i = 0; i < this.CAcceptedIds.length; i++) {
    				ds.subRequirementStore.getById(Number(this.CAcceptedIds[i])).data.projectInserted = true;
    			}
    			this.CAcceptedIds = new Array();
        	}, this);
    	}else{
    		ds.subRequirementStore.removeAll();
    	}
	},
	
	loadTasks : function(requirementId) {
    	if (requirementId != null && requirementId != ""){
        	var params = new Array();
        	params.push(['requirementId','=', requirementId]);
        	var filter = getFilter(params);
        	ds.researchStore.loadByCriteria(filter);
    	}else
    		ds.researchStore.removeAll();
	},
	
	loadCandidatesStore : function() {
		var potentialCandidates = new Array();
		var remote = this.requirementPanel.form.findField('remote').getValue();
		var relocate = this.requirementPanel.form.findField('relocate').getValue();
		var travel = this.requirementPanel.form.findField('travel').getValue();
		var requirementId = this.requirementPanel.form.findField('id').getValue();
		
		// if Remote is YES, Don't filter Active and In progress candidates
		// if job relocate or travel any one is true, then any one of them in candidate should be true
		// Both relocate and travel are false the no filter, match all candidates 

    	var params = new Array();

    	if (remote == true && relocate == false && travel == false ){
			// Remote - true  Remote and Travel are flase don't filter anything in Candidates
        	var filter = getFilter(params);
        	filter.pageSize=1000;
	    	if(ds.candidateSearchStore.getCount() == 0){
	    		ds.candidateSearchStore.loadByCriteria(filter);
	    		ds.candidateSearchStore.on('load', function(store, records, options) {
		        	this.loadPotentialCandidates(store);
		       	}, this);
	    	}else{
	    		this.loadPotentialCandidates(ds.candidateSearchStore);
	    	}
	    	return;
    	}
		
		if (remote == true) {
			if (relocate == true || travel == true) {
				// Remote - true , Don't filter Active and In progress & Remote or Travel true filter these in Candidates
				params.push(['RelocateOrTravel','=','true'])
			}
		}else{
			if (relocate == true || travel == true) {
				//Remote - No filter Active and In progress & Remote or Travel true filter these in Candidates
				params.push(['RelocateOrTravel','=','true'])
				params.push(['marketingStatus','=', "'1. Active','2. Yet to Start'"]);
			}else{
				//Remote - No filter Active and In progress & Remote are Travel false, filter these in Candidates
				params.push(['marketingStatus','=', "'1. Active','2. Yet to Start'"]);
			}
		}
		params.push(['gender','=', this.potentialCandidates.genderCombo.getValue()]);
    	if (this.potentialCandidates.ottCheckbox.getValue())
    		params.push(['resumeHelp','=','100-Yes' ]);

		var count =0;
		var filter = getFilter(params);
		filter.pageSize=1000;
		ds.candidateSearchStore2.loadByCriteria(filter);
		ds.candidateSearchStore2.on('load', function(store, records, options) {
			if (count ==0){
				this.loadPotentialCandidates(store);
				count++;
			}
       	}, this);

	},
	
	loadPotentialCandidates : function(store) {
		var potentialCandidates = new Array();
		this.potentialCandidates.store.removeAll();

		for ( var i = 0; i < store.data.getCount(); i++) {
			var record = store.getAt(i).data;
			record = this.calculateMatching(record);
			var priority = 0;
			if (record.matchScore >= 1 ) {
				//If matchScore>=1, then add priority (for 0 - 6, 1 to 4 give 5-priority)
				if (record.priority != null ) {
					if (record.priority == 0 ){
						priority = 6;
					}else if (record.priority >= 1 && record.priority <= 4 ) {
						priority = 5-record.priority;
					}
				}
				record.matchScore = record.matchScore + priority;
				record.matchScoreDetails += '<br>Priority :' + priority; 
				potentialCandidates.push(record);
			}
		}
		if (this.potentialCandidates.limit == 10){
			this.potentialCandidates.store.add(potentialCandidates.slice(0, 10));
			this.potentialCandidates.showPotentialCount.setValue(this.potentialCandidates.store.getCount()+' of '+potentialCandidates.length+' candidates');
		}else{
			this.potentialCandidates.store.add(potentialCandidates);
			this.potentialCandidates.showPotentialCount.setValue(this.potentialCandidates.store.getCount()+' candidates');
		}
		
		this.potentialCandidates.store.sort('matchScore', 'DESC');
		this.potentialCandidates.getView().refresh();
		ds.candidateSearchStore.clearFilter();
		this.getPotentialCriteria();
		this.skillWiseCriteria();
	},
	
	calculateMatching : function(record) {
		var reqSkillSet = this.requirementPanel.form.findField('skillSetIds').getValue().split(',');
		var reqModule = this.requirementPanel.form.findField('module').getValue().split(',');
		var reqSecModule = this.requirementPanel.form.findField('secondaryModule').getValue().split(',');
		var reqTargetRoles = this.requirementPanel.form.findField('targetRoles').getValue().split(',');
		var reqCertifications = this.requirementPanel.form.findField('certifications').getValue().split(',');

		var skillset = 0, module =0, certification =0, targetRole =0, targetSkillset=0, currentRoles = 0 , priority = 0;
		var matchScore = 0;
		//skillset match with skillset, 2 - skillset
		if (record.skillSetIds != null && record.skillSetIds != '' && reqSkillSet != null ) {
			canSkillSet = record.skillSetIds;
			canSkillSet = canSkillSet.split(','); // split with comma 
			for ( var j = 0; j < canSkillSet.length; j++) {
				if (reqSkillSet.indexOf(canSkillSet[j]) != -1) {
					skillset = skillset+2;
				}
			}
		}
		//skillset match with module,  3 - module
		if (record.skillSetIds != null && record.skillSetIds != '' && reqModule != null ) {
			canSkillSet = record.skillSetIds;
			canSkillSet = canSkillSet.split(','); // split with comma 
			for ( var j = 0; j < canSkillSet.length; j++) {
				var skillsetname = ds.skillSetStore.getById(Number(canSkillSet[j])).data.name;
				if(reqModule.indexOf(skillsetname)!=-1 || reqSecModule.indexOf(skillsetname)!=-1 ){
					module = module+3;
				}
			}
		}
		//target skillset match with target skillset, 1 - skillset
		if (record.targetSkillSetIds != null && record.targetSkillSetIds != '' && reqSkillSet != null ) {
			canTargetSkillSet = record.targetSkillSetIds;
			canTargetSkillSet = canTargetSkillSet.split(','); // split with comma 
			for ( var j = 0; j < canTargetSkillSet.length; j++) {
				if (reqSkillSet.indexOf(canTargetSkillSet[j]) != -1) {
					targetSkillset = targetSkillset+1;
				}
			}
		}
		//Roleset(reqTargetRoles) to Target roles, 2 for any no of roles 
		if (record.targetRoles != null && record.targetRoles != '' && reqTargetRoles != null ) {
			canTargetRoles = record.targetRoles;
			canTargetRoles = canTargetRoles.split(','); // split with comma 
			for ( var j = 0; j < canTargetRoles.length; j++) {
				if (reqTargetRoles.indexOf(canTargetRoles[j]) != -1) {
					targetRole = 2;
				}
			}
		}
		//Role set(reqTargetRoles) to role set(roleSetIds), 3 for any no of roles 
		if (record.roleSetIds != null && record.roleSetIds != '' && reqTargetRoles != null ) {
			canRoleSets = record.roleSetIds;
			canRoleSets = canRoleSets.split(','); // split with comma 
			for ( var j = 0; j < canRoleSets.length; j++) {
				var roleSetName = ds.targetRolesStore.getById(Number(canRoleSets[j])).data.name
				if (reqTargetRoles.indexOf(roleSetName) != -1) {
					currentRoles = 3;
				}
			}
		}
		//Certifications match, 5 for each
		//allCertifications contains list of all certifications from grid seperated by ' -- '
		if (record.allCertifications != null && record.allCertifications != '' && reqCertifications != null ) {
			canCertifications = record.allCertifications;
			canCertifications = canCertifications.split(' -- '); // split with seperator 
			for ( var j = 0; j < canCertifications.length; j++) {
				if (reqCertifications.indexOf(canCertifications[j]) != -1) {
					certification = certification + 5;
				}
			}
		}
		
		matchScore = skillset + module + certification + targetRole + targetSkillset + currentRoles ;
		record.matchScore = matchScore;
		record.matchScoreDetails = 'Skill set : '+skillset +'<br>Role set : '+currentRoles+ '<br>Module : '+module+
									'<br>Target Skill set : '+ targetSkillset + '<br>Target Role : '+targetRole+'<br>Certification :' + certification ;
		return record;
	},

	getPotentialCriteria : function() {
		if (this.potentialCandidates.store.getCount() > 0) {
	    	var params = new Array();
	    	var candidateIds = this.potentialCandidates.store.collect('id').toString();
	    	params.push(['candidateIds','=', candidateIds]);
	    	params.push(['requirementId','=', this.requirementPanel.form.findField('id').getValue()]);
	    	var filter = getFilter(params);
			app.requirementService.getPotentialCriteria(Ext.JSON.encode(filter),this.onGetPotentialCriteria,this);
		}
	},
	
	onGetPotentialCriteria : function(data) {
		if (data.success && data.returnVal.rows.length > 0) {
			for ( var i = 0; i < this.potentialCandidates.store.getCount(); i++) {
				this.potentialCandidates.store.getAt(i).data.matchIssue = false;
				this.potentialCandidates.store.getAt(i).data.matchIssueNotes = '';
			}
			for ( var int = 0; int < data.returnVal.rows.length; int++) {
				if (data.returnVal.rows[int].type == 'client' && data.returnVal.rows[int].value != null) {
					var values = data.returnVal.rows[int].value.split(' :: ');
					var client = ds.clientSearchStore.getById(Number(data.returnVal.rows[int].clientId)).data.name;
					for ( var i = 0; i < values.length; i++) {
						var canId = values[i].split(' -- ')[0];	
						var module = values[i].split(' -- ')[1];
						this.potentialCandidates.store.getById(Number(canId)).data.matchIssue = true;
						this.potentialCandidates.store.getById(Number(canId)).data.matchIssueNotes = '-Sub to '+client+' with a different module ('+module+') <br>';
					}
				}else if (data.returnVal.rows[int].type == 'vendor' && data.returnVal.rows[int].value != null) {
					var canIds = data.returnVal.rows[int].value.split(' :: ');
					for ( var i = 0; i < canIds.length; i++) {
						if (canIds[i].split(' -- ')[1] >= 3) {
							var vendor = ds.vendorStore.getById(Number(canIds[i].split(' -- ')[1])).data.name;
							var module = canIds[i].split(' -- ')[2];	
							this.potentialCandidates.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssue = true;
							if(this.potentialCandidates.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssueNotes == null)
								this.potentialCandidates.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssueNotes ='';
							this.potentialCandidates.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssueNotes += '-Sub to '+vendor+' with a different module ('+module+') <br>';
						}
					}
				}else if (data.returnVal.rows[int].type == 'Client_vendor' && data.returnVal.rows[int].value != null) {
					var candidates = data.returnVal.rows[int].value.split(' :: ');
					for ( var i = 0; i < candidates.length; i++) {
						if (candidates[i].split(' -- ')[1] >= 3) {
							var candidateId = candidates[i].split(' -- ')[0];
							var vendor = ds.vendorStore.getById(Number(candidates[i].split(' -- ')[1])).data.name;
							var client = ds.clientSearchStore.getById(Number(candidates[i].split(' -- ')[2])).data.name;
							if(this.potentialCandidates.store.getById(Number(candidateId)).data.matchIssueNotes == null)
								this.potentialCandidates.store.getById(Number(candidateId)).data.matchIssueNotes ='';
							this.potentialCandidates.store.getById(Number(candidateId)).data.matchIssueNotes += '-Sub to '+vendor+' with same '+client+' <br>';
						}
					}
				}else if (data.returnVal.rows[int].type == 'MVC' && data.returnVal.rows[int].value != null) {
					var candidates = data.returnVal.rows[int].value.split(' :: ');
					for ( var i = 0; i < candidates.length; i++) {
						if (this.potentialCandidates.store.getById(Number(candidates[i])).data.matchScoreDetails.search("MVC : 10") == -1){
							this.potentialCandidates.store.getById(Number(candidates[i])).data.matchScore += 10;
							this.potentialCandidates.store.getById(Number(candidates[i])).data.matchScoreDetails += "<br>MVC : 10";
						}
					}
				}else if (data.returnVal.rows[int].type == 'MV' && data.returnVal.rows[int].value != null) {
					var candidates = data.returnVal.rows[int].value.split(' :: ');
					for ( var i = 0; i < candidates.length; i++) {
						if (this.potentialCandidates.store.getById(Number(candidates[i])).data.matchScoreDetails.search("MV : 5") == -1){
							this.potentialCandidates.store.getById(Number(candidates[i])).data.matchScore += 5;
							this.potentialCandidates.store.getById(Number(candidates[i])).data.matchScoreDetails += "<br>MV : 5";
						}
					}
				}else if (data.returnVal.rows[int].type == 'VendorWithdrawn' && data.returnVal.rows[int].value != null) {
					var values = data.returnVal.rows[int].value.split(',');
					for ( var i = 0; i < values.length; i++) {
						var candidateId = values[i].split(' -- ')[0];
						var vendor = ds.vendorStore.getById(Number(values[i].split(' -- ')[1])).data.name;
						this.potentialCandidates.store.getById(Number(candidateId)).data.matchIssue = true;
						if(this.potentialCandidates.store.getById(Number(candidateId)).data.matchIssueNotes == null)
							this.potentialCandidates.store.getById(Number(candidateId)).data.matchIssueNotes ='';
						this.potentialCandidates.store.getById(Number(candidateId)).data.matchIssueNotes += '-Sub to '+vendor+' & Withdrawn status <br>';
					}
				}else if (data.returnVal.rows[int].type == 'CLientWithdrawn' && data.returnVal.rows[int].value != null) {
					var candidates = data.returnVal.rows[int].value.split(',');
					var client = ds.clientSearchStore.getById(Number(data.returnVal.rows[int].clientId)).data.name;
					for ( var i = 0; i < candidates.length; i++) {
						this.potentialCandidates.store.getById(Number(candidates[i])).data.matchIssue = true;
						if(this.potentialCandidates.store.getById(Number(candidates[i])).data.matchIssueNotes == null)
							this.potentialCandidates.store.getById(Number(candidates[i])).data.matchIssueNotes ='';
						this.potentialCandidates.store.getById(Number(candidates[i])).data.matchIssueNotes += '-Sub to '+client+' & Withdrawn status <br>';
						
					}
				}else if (data.returnVal.rows[int].type == 'SameModuleDifferentVendor' && data.returnVal.rows[int].value != null) {
					var candidates = data.returnVal.rows[int].value.split(',');
					for ( var i = 0; i < candidates.length; i++) {
						if(this.potentialCandidates.store.getById(Number(candidates[i])).data.matchIssueNotes == null)
							this.potentialCandidates.store.getById(Number(candidates[i])).data.matchIssueNotes ='';
						
						var vendors = new Array();
						var cand_vendors = data.returnVal.rows[int].module.split(',');
						for ( var k = 0; k < cand_vendors.length; k++) {
							if(candidates[i] == cand_vendors[k].split('-')[0])
								vendors.push(ds.vendorStore.getById(Number(cand_vendors[k].split('-')[1])).data.name);
						}
						this.potentialCandidates.store.getById(Number(candidates[i])).data.matchIssueNotes +='-Sub to same Module, with different vendor ('+vendors.toString()+')<br>';
						this.potentialCandidates.store.getById(Number(candidates[i])).data.matchScore += 2;
						this.potentialCandidates.store.getById(Number(candidates[i])).data.matchScoreDetails += "<br>Same Module, different vendor : 2";
					}
				}
			}
		}
		this.potentialCandidates.store.sort('matchScore', 'DESC');
		this.potentialCandidates.getView().refresh();
	},

	skillWiseCriteria : function() {
		var reqModule = this.requirementPanel.form.findField('module').getValue().split(',');
		var moduleArray = new Array();
		for ( var i = 0; i < reqModule.length; i++) {
			moduleArray.push({"module":reqModule[i],"count":0,"candidates" : ''});
		}
		var reqSkillSet = this.requirementPanel.form.findField('skillSet').getValue().split(',');
		var skillSetArray = new Array();
		for ( var i = 0; i < reqSkillSet.length; i++) {
			skillSetArray.push({"skillSet":reqSkillSet[i],"count":0,"candidates" : ''});
		}
		
		var reqCertification = this.requirementPanel.form.findField('certifications').getValue().split(',');
		var certificationArray = new Array();
		for ( var i = 0; i < reqCertification.length; i++) {
			certificationArray.push({"certification":reqCertification[i],"count":0,"candidates" : ''});
		}
		
		for ( var i = 0; i < this.potentialCandidates.store.getCount(); i++) {
			var record = this.potentialCandidates.store.getAt(i).data;
			//skillset match with module
			if (record.skillSetIds != null && record.skillSetIds != '' && reqModule.length > 0) {
				canSkillSet = record.skillSetIds.split(','); // split with comma
				for ( var j = 0; j < canSkillSet.length; j++) {
					var skillsetname = ds.skillSetStore.getById(Number(canSkillSet[j])).data.name;
					if(reqModule.indexOf(skillsetname)!=-1){
						var index = reqModule.indexOf(skillsetname);
						moduleArray[index].count += 1;
						moduleArray[index].candidates = moduleArray[index].candidates + ',' + record.id;
					}//if
				}//for
			}//if
			
			//skillset match with skillset
			if (record.skillSetIds != null && record.skillSetIds != '' && reqSkillSet.length > 0) {
				canSkillSet = record.skillSetIds.split(','); // split with comma 
				for ( var j = 0; j < canSkillSet.length; j++) {
					var skillsetname = ds.skillSetStore.getById(Number(canSkillSet[j])).data.name;
					if (reqSkillSet.indexOf(skillsetname) != -1) {
						var index = reqSkillSet.indexOf(skillsetname);
						skillSetArray[index].count += 1;
						skillSetArray[index].candidates = skillSetArray[index].candidates + ',' + record.id;
					}//if
				}//for
			}//if
			
			//Certification match with Certification
			if (record.allCertifications != null && record.allCertifications != '' && reqCertification.length > 0) {
				canCertifications = record.allCertifications;
				canCertifications = canCertifications.split(' -- '); // split with comma 
				for ( var j = 0; j < canCertifications.length; j++) {
					if (reqCertification.indexOf(canCertifications[j]) != -1) {
						var index = reqCertification.indexOf(canCertifications[j]);
						certificationArray[index].count += 1;
						certificationArray[index].candidates = certificationArray[index].candidates + ',' + record.id;
					}//if
				}//for
			}//if
			
		}//for
		
		if (Ext.getCmp('certificationContainer') != null)
			this.criteriaPanel.remove(Ext.getCmp('certificationContainer'), true);
		if (Ext.getCmp('moduleContainer') != null)
			this.criteriaPanel.remove(Ext.getCmp('moduleContainer'), true);
		if (Ext.getCmp('skillsetContainer') != null)
			this.criteriaPanel.remove(Ext.getCmp('skillsetContainer'), true);
		
		var certificationContainer = new Ext.container.Container({
			id : 'certificationContainer',
			padding : '0 0 0 10',
			layout: {
		        type: 'table',
		    },
		    items: []
		});
		var items = new Array();
        var field = new Ext.form.field.Display({
        	fieldLabel: 'Cert',
        	labelWidth : 50,
        });
        var panel = this;
        items.push(field);
		for ( var i = 0; i < certificationArray.length; i++) {
			var field = new Ext.form.field.Display({
				value : '<a>'+certificationArray[i].certification + ' (' + certificationArray[i].count+')</a>',
				itemId : certificationArray[i].candidates,
				padding : '0 10 0 0',
				listeners: {
					afterrender: function(component) {
						component.getEl().on('click', function() {
							var candidates = this.itemId;
							//remove comma at starting
							if (candidates.charAt(0) == ',')
								candidates = candidates.substr(1);
							panel.loadIntoPotentials(candidates);
						},this,panel);
					}
				}
			});
			items.push(field);
		}
		certificationContainer.add(items);

		
		var moduleContainer = new Ext.container.Container({
			id : 'moduleContainer',
			padding : '0 0 0 10',
			layout: {
		        type: 'table',
		    },
		    items: []
		});
		var items = new Array();
        var field = new Ext.form.field.Display({
        	fieldLabel: 'Module',
        	labelWidth : 50,
        });
        var panel = this;
        items.push(field);
		for ( var i = 0; i < moduleArray.length; i++) {
			var field = new Ext.form.field.Display({
				value : '<a>'+moduleArray[i].module + ' (' + moduleArray[i].count+')</a>',
				itemId : moduleArray[i].candidates,
				padding : '0 10 0 0',
				listeners: {
					afterrender: function(component) {
						component.getEl().on('click', function() {
							var candidates = this.itemId;
							//remove comma at starting
							if (candidates.charAt(0) == ',')
								candidates = candidates.substr(1);
							panel.loadIntoPotentials(candidates);
						},this,panel);
					}
				}
			});
			items.push(field);
		}
		moduleContainer.add(items);
		
		var skillsetContainer = new Ext.container.Container({
			id : 'skillsetContainer',
			padding : '0 0 0 10',
			layout: {
		        type: 'table',
		    },
		    items: []
		});
		var items = new Array();
        var field = new Ext.form.field.Display({
        	fieldLabel: 'Skill Set',
        	labelWidth : 50,
        });
        items.push(field);

		for ( var i = 0; i < skillSetArray.length; i++) {
			var field = new Ext.form.field.Display({
				value : '<a>'+skillSetArray[i].skillSet + ' (' + skillSetArray[i].count+')</a>',
				itemId : skillSetArray[i].candidates,
				padding : '0 10 0 0',
				listeners: {
					afterrender: function(component) {
						component.getEl().on('click', function() {
							var candidates = this.itemId;
							//remove comma at starting
							if (candidates.charAt(0) == ',')
								candidates = candidates.substr(1);
							panel.loadIntoPotentials(candidates);
						},this,panel);
					}
				}
			});
			items.push(field);
		}
		skillsetContainer.add(items)
		
		this.criteriaPanel.add(certificationContainer);
		this.criteriaPanel.add(moduleContainer);
		this.criteriaPanel.add(skillsetContainer);
		this.criteriaPanel.doLayout();
	},
	
	loadIntoPotentials : function(candidates) {
		this.potentialCandidates.store.removeAll();
		if (candidates != '') {
			var potentialCandidates = new Array();
			var candidateIds = candidates.split(',');
			for ( var i = 0; i < candidateIds.length; i++) {
				var record = ds.candidateSearchStore.getById(Number(candidateIds[i])).data;
				record = this.calculateMatching(record);
				var priority = 0;
				if (record.matchScore >= 1 ) {
					//If matchScore>=1, the add priority (for 0 - 6, 1 to 4 give 5-priority)
					if (record.priority != null ) {
						if (record.priority == 0 ){
							priority = 6;
						}else if (record.priority >= 1 && record.priority <= 4 ) {
							priority = 5-record.priority;
						}
					}
					record.matchScore = record.matchScore + priority;
					record.matchScoreDetails += '<br>Priority :' + priority; 
				}
				potentialCandidates.push(record);
			}
			this.potentialCandidates.store.add(potentialCandidates);
		}
		this.potentialCandidates.store.sort('matchScore', 'DESC');
		this.potentialCandidates.getView().refresh();
		this.potentialCandidates.showPotentialCount.setValue(this.potentialCandidates.store.getCount()+' candidates');
		
		this.getPotentialCriteria();

	},
	
	loadShortlisted : function(requirementId) {
    	if (requirementId != null && requirementId != ""){
        	var params = new Array();
        	params.push(['table','=', "Shortlisted"]);
        	params.push(['requirementId','=', requirementId]);
        	var filter = getFilter(params);
        	this.shortlistedCandidates.store.loadByCriteria(filter);
    	}else{
    		this.shortlistedCandidates.store.removeAll();
    	}
	},
	
	checkForDirty : function() {
		if (this.requirement == "")
			this.closeForm();

		var formDirty = false;
		var itemslength = this.requirementPanel.form.getFields().getCount();
		var i = 0;
		while (i < itemslength) {
			var fieldName = this.requirementPanel.form.getFields().getAt(i).name;
			value = this.requirementPanel.form.findField(fieldName).getValue();
			
			if (fieldName == 'created' || fieldName == 'candidateId' || fieldName == 'postingTime' || fieldName == 'selectedTargetRoles' || fieldName == 'selectedSkillSet'
				|| fieldName == 'selectedCertifications' || fieldName == 'selectedModule' || fieldName == 'vendorId' || fieldName == 'contactId' 
					|| fieldName=='submittedVendorsCount' || fieldName == 'suggestedVendorsCount' || fieldName == 'certificationIds' || fieldName == 'skillSetIds' 
					|| fieldName == 'certifications' || fieldName == 'skillSet') {
			}else if (fieldName == 'postingDate') {
				if (Ext.Date.format(new Date(this.requirement.data[fieldName]),'m/d/y') != Ext.Date.format(value,'m/d/y')) {
					formDirty = true;
				}
			}else{
				if (value == null || this.requirement.data[fieldName] == null) {
					if (value == null && this.requirement.data[fieldName] != null && this.requirement.data[fieldName] == "") {
						//if value is null and previous value is not null previous value must be empty string
					}else if (this.requirement.data[fieldName] == null && value != null && value == "") {
						//if previous value is null and value is not null then value must be empty string
					}else if (value == null && this.requirement.data[fieldName] == null) {
						// if both are null no issue
					}else{
						formDirty = true;
					}
				}else if (this.requirement.data[fieldName].toString() != value.toString()) {
					formDirty = true;
				}
			}
			
			i = i+1;
		}

    	var tasks = new Array();
    	for ( var i = 0; i < ds.researchStore.data.length ; i++) {
			var record = ds.researchStore.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			tasks.push(record.data);
    		}
    		if (record.data.requirementId == 0 || record.data.requirementId == null)
				delete record.data.requirementId;
    		
    		if (this.modifiedResearchIds.indexOf(record.data.id) != -1) {
    			tasks.push(record.data);
			}
		}
    	if (tasks.length >0)
    		formDirty = true;

		if (formDirty) {
			Ext.MessageBox.show({
				title:'Confirm',
	            msg: "Do you want to save Job opening changes?",
	            buttonText: {
	                yes: 'Save and Close',
	                no: 'Close without Saving'
	            },
	            icon: Ext.MessageBox.INFO,
	            fn: function(btn) {
	            	if (btn == 'yes') {
	            		app.requirementMgmtPanel.requirementDetailPanel.closeButton = true;
	            		app.requirementMgmtPanel.requirementDetailPanel.save('yes');
	            	}else if (btn == 'no') {
	            		app.requirementMgmtPanel.requirementDetailPanel.closeForm();
					}
	            }
	        });
		}else
			this.closeForm();
	},
	
	closeConfirm : function(){
		
		if (this.candidatePanel.form.findField('firstName').getValue() != '') {
			Ext.Msg.alert('Warning','Some Candidate information is updated, please save them before closing.');
			return false;
		}
    	var vendors = new Array();
    	for ( var i = 0; i < ds.subVendorStore.data.length ; i++) {
			var record = ds.subVendorStore.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				vendors.push(record.data);
			}
		}
    	if (vendors.length >0){
			Ext.Msg.alert('Warning','Please save or reset submitted vendors before closing.');
			return false;
		}
	
    	var candidates = new Array();
    	for ( var i = 0; i < ds.subRequirementStore.data.length ; i++) {
			var record = ds.subRequirementStore.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				candidates.push(record.data);
			}
		}
    	if (candidates.length >0){
			Ext.Msg.alert('Warning','Please save or reset submitted candidates before closing.');
			return false;
		}
    	this.closeButton = true;
		this.checkForDirty();
	},
	
	closeForm: function(){
		if (this.openedFrom == 'Home') {
			app.setActiveTab(0,'li_home');
			app.requirementMgmtPanel.getLayout().setActiveItem(0);
			if (this.modified) {
				app.homeMgmtPanel.homeDetailPanel.requirementsGrid1.search();
				app.homeMgmtPanel.homeDetailPanel.requirementsGrid2.search();
			}
		}else if (this.openedFrom =='Candidate') {
			this.openedFrom ='';
			app.setActiveTab(0,'li_candidates');
		}else if (this.openedFrom =='Interviews') {
			this.openedFrom ='';
			app.setActiveTab(0,'li_interviews');
		}else{
			if (this.modified) {
				app.requirementMgmtPanel.closeForm();	
			}else
				app.requirementMgmtPanel.getLayout().setActiveItem(0);
		}
	},

	nextRequirement : function() {
		var requirementId = this.requirementPanel.form.findField('id').getValue();
    	if (requirementId == null || requirementId == '') {
    		this.body.scrollTo('top', 0);
    		this.updateError("Please save Job opening");
    		return false;
		}
		var params = new Array();
		params.push(['nextJob','=', requirementId]);
		var filter = getFilter(params);
		filter.pageSize=50;
		app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onGetNextRequirement,this);
	},
	
	onGetNextRequirement : function(data) {
		if (data.success && data.rows.length > 0) {
			app.setActiveTab(0,'li_requirements');
			var record = Ext.create('tz.model.Requirement', data.rows[0]);
			this.loadForm(record);
		}
	},
	
	saveAndRefresh : function() {
		this.refresh = true ;
		this.saveConfirm();
	},

	saveAndNewRequirement : function() {
		this.newRequirement = true;
		this.saveConfirm();
	},

	saveAndCopyRequirement : function() {
		this.copyRequirement = true;
		this.saveConfirm();
	},
	
	saveConfirm : function() {
		var relocate = this.requirementPanel.form.findField('relocate').getValue();
		var travel = this.requirementPanel.form.findField('travel').getValue();
		if (relocate == false && travel == false) {
			Ext.Msg.alert('Warning','Please make sure Relocate/Travel are properly marked, this will afftect the match score.');
		}
		this.save('yes');
	},
	
    save : function(dat){
    	if (dat == 'yes') {
        	if (this.requirementPanel.form.findField("postingTime").getValue() != null && this.requirementPanel.form.findField("postingTime").getValue() != null ){
        		this.requirementPanel.form.findField("postingDate").allowBlank = false;
        	}
        	if (this.requirementPanel.form.findField("postingDate").getValue() != null && this.requirementPanel.form.findField("postingDate").getValue() != null ){
        		this.requirementPanel.form.findField("postingTime").allowBlank = false;
        	}
        	
        	if(this.requirementPanel.form.isValid( )){
    			var otherFieldObj = {};
    			var clientObj = {};
    			var recruiterObj = {};
    			var values = new Array();
    			
    			var itemslength = this.requirementPanel.form.getFields().getCount();
    			var i = 0;
    			while (i < itemslength) {
    				var fieldName = this.requirementPanel.form.getFields().getAt(i).name;
    				if (fieldName == 'id' ) {
    					var idVal = this.requirementPanel.form.findField(fieldName).getValue();
    					if(typeof(idVal) != 'undefined' && idVal != null && idVal != "")
    						otherFieldObj[fieldName] = this.requirementPanel.form.findField(fieldName).getValue();
    				}else 
    					otherFieldObj[fieldName] = this.requirementPanel.form.findField(fieldName).getValue();

    				if (fieldName != 'created' ) {
    					if (this.requirementPanel.form.findField(fieldName).getValue() != null && this.requirementPanel.form.findField(fieldName).getValue() != '')
    						values.push(this.requirementPanel.form.findField(fieldName).getValue());
    				}
    				i = i+1;
    			}
    			if (otherFieldObj.postingDate != null){
    				otherFieldObj.postingDate = new Date(otherFieldObj.postingDate.setHours(otherFieldObj.postingTime.getHours()));
    				otherFieldObj.postingDate = new Date(otherFieldObj.postingDate.setMinutes(otherFieldObj.postingTime.getMinutes()));
    			}
    			delete otherFieldObj.postingTime;
    			delete otherFieldObj.selectedModule;
    	   		delete otherFieldObj.selectedTargetRoles;
    	   		delete otherFieldObj.selectedSkillSet;
    	   		delete otherFieldObj.selectedCertifications;
    	   		delete otherFieldObj.selectedSecondaryModule;
    	   		delete otherFieldObj.submittedVendorsCount;
    	   		delete otherFieldObj.suggestedVendorsCount;
    	   		delete otherFieldObj.clientId;
    	   		delete otherFieldObj.recruiter;
    	   		delete otherFieldObj.certificationIds;
    	   		delete otherFieldObj.skillSetIds;
    	   		
    	   		//fileds from vendor grid
    	   		delete otherFieldObj.vendorId;
    	   		delete otherFieldObj.contactId;
    	   		
    			if (values.length == 0) {
    				this.body.scrollTo('top', 0);
    				this.updateError('Please add some data to save');
    				return false;
    			}
    			if (otherFieldObj.resume == null || otherFieldObj.resume == '')
    				delete otherFieldObj.resume ;
    			if (otherFieldObj.requirementNo == null || otherFieldObj.requirementNo == '')
    				delete otherFieldObj.requirementNo
    	    	var tasks = new Array();
    	    	for ( var i = 0; i < ds.researchStore.data.length ; i++) {
    				var record = ds.researchStore.getAt(i);
    	    		if (record.data.id == 0 || record.data.id == null){
    	    			delete record.data.id;
    	    			tasks.push(record.data);
    	    		}
    				delete record.data.requirementId;
    	    		if (this.modifiedResearchIds.indexOf(record.data.id) != -1) {
    	    			tasks.push(record.data);
    				}
    			}
    	    	
				var certIds = this.requirementPanel.form.findField('certificationIds').getValue();
				if (certIds == null || certIds == '' )
					certIds = new Array();
				else
					certIds = certIds.split(',');
				var sub_Certifications = new Array();
				for ( var i = 0; i < certIds.length; i++) {
					var record = {};
					record.settingsId = Number(certIds[i]);
					sub_Certifications.push(record);
				}

				var skillSetIds = this.requirementPanel.form.findField('skillSetIds').getValue();
				if (skillSetIds == null || skillSetIds == '' )
					skillSetIds = new Array();
				else
					skillSetIds = skillSetIds.split(',');
				var sub_skillSet = new Array();
				for ( var i = 0; i < skillSetIds.length; i++) {
					var record = {};
					record.skillSetId = Number(skillSetIds[i]);
					sub_skillSet.push(record);
				}

				sub_Certifications = Ext.JSON.encode(sub_Certifications);
				sub_skillSet = Ext.JSON.encode(sub_skillSet);
    	    	tasks = Ext.JSON.encode(tasks);
    			otherFieldObj = Ext.JSON.encode(otherFieldObj);
    			
    			var recruiter = '';
    			var idVal = this.requirementPanel.form.findField('recruiter').getValue();
    			if(idVal != '' && idVal != null && idVal >= 0) {
    				recruiterObj['id'] = idVal;
    				recruiterObj = Ext.JSON.encode(recruiterObj);
    				recruiter = ',"recruiterObj":' + recruiterObj;
    			}
    			var client = '';
    			var idVal = this.requirementPanel.form.findField('clientId').getValue();
    			if(idVal != '' && idVal != null && idVal >= 0) {
    				clientObj['id'] = idVal;
    				clientObj = Ext.JSON.encode(clientObj);
    				client = ',"client":' + clientObj;
    			}
    			
   				otherFieldObj =  '{"researchs":{"com.recruit.research.service.Research":' + tasks +'},'+
   									'"sub_Certifications":{"com.recruit.requirement.service.Sub_Certification":'+sub_Certifications+'},'+
   									'"sub_Skillsets":{"com.recruit.requirement.service.Sub_SkillSet":'+sub_skillSet+'}'
   									+ client + recruiter +','+ otherFieldObj.substring(1,otherFieldObj.length);
    			app.requirementService.saveRequirement(otherFieldObj,this.onSave,this);
        	} else {
        		this.body.scrollTo('top', 0);
        		this.updateError('Please fix the errors');
        	}
    	}
    },
	
	onSave : function(data){
		this.clearMessage();
		this.modified = true;
		this.clearMessage();
		if (data.success) { 
			var rec = data.returnVal;
			this.requirementPanel.form.findField('id').setValue(rec.id);
			this.requirementPanel.form.findField('version').setValue(rec.version);
			this.requirementPanel.form.findField('certificationIds').setValue(rec.certificationIds);
			this.requirementPanel.form.findField('skillSetIds').setValue(rec.skillSetIds);
			if (this.modifiedVendorIds.length > 0) {
				this.saveVendors();
			}
			if(this.newRequirement == true || this.newRequirement == 'true'){
				this.newRequirement = false;
				this.updateMessage('Saved successfully');
				this.getForm().reset();
				this.deleteButton.disable();
				ds.subVendorStore.removeAll();
	       		this.vendorStore.removeAll();
	       		ds.subRequirementStore.removeAll();
	       		ds.researchStore.removeAll();
	       		this.potentialCandidates.store.removeAll();
	       		this.shortlistedCandidates.store.removeAll();
	       		this.suggstedVendors.store.removeAll();
	       		this.suggestedVendorsCount.setValue('');
	       		this.submittedVendorsCount.setValue('');
	       		this.criteriaPanel.form.reset();
	       		this.reqSmeGridPanel.store.removeAll();
	       		//get requirement no
	       		app.requirementService.getRequirementNo('',this.onGetRequirementNo,this);
			}else if(this.copyRequirement == true || this.copyRequirement == 'true'){
				this.copyRequirement = false; 
				this.updateMessage('Saved previous Requirement successfully');
				this.form.findField('id').setValue(null);
				this.deleteButton.disable();
				ds.subVendorStore.removeAll();
	       		this.vendorStore.removeAll();
	       		ds.subRequirementStore.removeAll();
	       		ds.researchStore.removeAll();
	       		this.potentialCandidates.store.removeAll();
	       		this.shortlistedCandidates.store.removeAll();
	       		this.suggstedVendors.store.removeAll();
	       		this.suggestedVendorsCount.setValue('');
	       		this.submittedVendorsCount.setValue('');
	       		this.criteriaPanel.form.reset();
	       		this.reqSmeGridPanel.store.removeAll();
	       		//get requirement no
	       		app.requirementService.getRequirementNo('',this.onGetRequirementNo,this);
			}else if (this.refresh == true || this.refresh == 'true') {
				this.refresh = false;
				this.loadTasks(rec.id);
				this.deleteButton.enable();
				this.requirement.data = rec;
				this.updateMessage('Saved Successfully');
				this.loadCandidatesStore();
				this.getSpecialCriteria(rec.id);
			}else{
				var rec = data.returnVal;
				this.loadTasks(rec.id);
				this.deleteButton.enable();
				this.requirement.data = rec;
				this.updateMessage('Saved Successfully');
			}
			if (this.closeButton) 
				this.closeForm();
		}else {
			this.body.scrollTo('top', 0);
			var panel = this;
			if (data.errorMessage == "Version mismatch") {
				Ext.MessageBox.show({
					title:'Confirm',
		            msg: "This record has been changed by another user since you opened it, " +
		            		"do you want to update this record with latest version or do you want to override and change the record with your new updates",
		            buttonText: {                        
		                yes : "Update latest version",
		                no : "Override with new updates"
		            },
		            icon: Ext.MessageBox.INFO,
		            fn: function(btn) {
		            	if (btn == 'yes') {
		            		panel.getRequirement(data.returnVal.id);
		            	}else if (btn == 'no') {
		            		panel.requirementPanel.form.findField('version').setValue(Number(data.returnVal.version) + 1);
		            		panel.save('yes');
						}
		            }
		        });
			}
			else
				this.updateError(data.errorMessage);
		}	
	},	
	
	onGetRequirementNo : function(data) {
		if (data.success) {
	   	 	this.requirementPanel.form.findField('requirementNo').setValue(Number(data.successMessage));
		}
	},
	
	onDelete : function(data){
		this.clearMessage();
		if(!data.success){
			this.body.scrollTo('top', 0);
			this.updateError(data.errorMessage);
		}else{
			app.requirementMgmtPanel.getLayout().setActiveItem(0);
			app.requirementMgmtPanel.requirementsSearchPanel.search();
		}
	},
	
	deleteConfirm : function()
	{
		Ext.Msg.confirm("Confirm","Do you want to delete this Requirement?", this.deleteRequirement, this);
	},
	
	deleteRequirement : function(dat){
		if(dat=='yes'){
			var idVal = this.requirementPanel.form.findField('id').getValue();
			app.requirementService.deleteRequirement(idVal, this.onDelete, this);
		}
	},

	viewCandidate : function(candidateId) {
		if (typeof (candidateId) != 'undefined' && candidateId != null && candidateId >= 0){
			var params = new Array();
			params.push(['id','=', candidateId]);
			var filter = getFilter(params);
			filter.pageSize=50;
			app.candidateService.getCandidates(Ext.JSON.encode(filter),this.onGetCandidate,this);
		}
	},
	
	onGetCandidate : function(data) {
		if (data.rows.length > 0) {
			app.setActiveTab(0,'li_candidates');
			app.candidateMgmtPanel.getLayout().setActiveItem(1);
			app.candidateMgmtPanel.candidatePanel.clearMessage();
			var record = Ext.create('tz.model.Candidate', data.rows[0]);
			app.candidateMgmtPanel.candidatePanel.loadForm(record);
			app.candidateMgmtPanel.candidatePanel.openedFrom = "Job Opening";
		}
	},
	
	loadFromCandidates : function() {
		var requirementId = this.requirementPanel.form.findField('id').getValue();
		this.loadSubRequirments(requirementId);
		this.loadShortlisted(requirementId);
	},

	addInterview : function() {
		var requirementId = this.requirementPanel.form.findField('id').getValue();
    	if (requirementId == null || requirementId == '') {
    		this.body.scrollTo('top', 0);
    		this.updateError("Please save Job opening");
    		return false;
		}
    	this.manager.showInterview();
    	this.manager.interviewDetailPanel.opendFrom='panel';
    	this.manager.interviewDetailPanel.clearMessage();
    	this.manager.interviewDetailPanel.form.reset();
    	this.manager.interviewDetailPanel.form.findField('requirementId').setValue(requirementId);
	
        var vendorStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
        });
        var clientStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
        });
        var candidateStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'fullName'],
        });

        ds.clientSearchStore.clearFilter(true);
        ds.vendorStore.clearFilter(true);
        ds.contractorsStore.clearFilter(true);

        var clientId = this.requirementPanel.form.findField('clientId').getValue();
        if (clientId != null && clientId != '') {
        	var client = ds.clientSearchStore.getById(clientId);
        	var clientData= new Array();
        	clientData.push({"id":client.data.id,"name":client.data.name});
    		clientStore.add(clientData);
		}else
			clientStore.add(ds.clientSearchStore.data.items);
		
        var vendorIds = ds.subVendorStore.collect('vendorId');
        if (vendorIds.length > 0) {
        	var vendorData= new Array();
			for ( var i = 0; i < vendorIds.length; i++) {
				vendorData.push({"id":ds.vendorStore.getById(vendorIds[i]).data.id,"name":ds.vendorStore.getById(vendorIds[i]).data.name});
			}
			vendorStore.add(vendorData);
		}else
			vendorStore.add(ds.vendorStore.data.items);
        
        if (ds.subRequirementStore.getCount() > 0) {
        	var candidateData= new Array();
			for ( var i = 0; i < ds.subRequirementStore.getCount(); i++) {
				if (ds.subRequirementStore.getAt(i).data.deleted != true)
					candidateData.push({"id":ds.subRequirementStore.getAt(i).data.candidateId,"fullName":ds.subRequirementStore.getAt(i).data.candidate});
			}
			candidateStore.add(candidateData);
		}else
			candidateStore.add(ds.contractorsStore.data.items);
        
        this.manager.interviewDetailPanel.vendorCombo.bindStore(vendorStore);
        this.manager.interviewDetailPanel.clientCombo.bindStore(clientStore);	
        this.manager.interviewDetailPanel.candidateCombo.bindStore(candidateStore);
        this.manager.interviewDetailPanel.form.findField('clientId').setValue(this.requirementPanel.form.findField('clientId').getValue());
        this.manager.interviewDetailPanel.form.findField('skillSet').setValue(this.requirementPanel.form.findField('skillSet').getValue());
        this.manager.interviewDetailPanel.form.findField('requirementId').setReadOnly(true);

        if(vendorStore.getCount() == 1)
        	this.manager.interviewDetailPanel.form.findField('vendorId').setValue(vendorStore.getAt(0).data.id);
        if(candidateStore.getCount() == 1)
        	this.manager.interviewDetailPanel.form.findField('candidateId').setValue(candidateStore.getAt(0).data.id);

	},
	
	viewContact : function(contactId) {
    	var params = new Array();
    	params.push(['id','=', contactId]);
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	app.contactService.getContacts(Ext.JSON.encode(filter),this.onGetContact,this);
	},
	
	onGetContact : function(data) {
		if (data.success && data.rows.length > 0) {
	    	app.setActiveTab(0,'li_contacts');
	    	app.contactMgmtPanel.getLayout().setActiveItem(1);
	    	app.contactMgmtPanel.contactDetailPanel.opendFrom='panel';
			var contact = Ext.create('tz.model.Contact', data.rows[0]);
			app.contactMgmtPanel.contactDetailPanel.loadForm(contact);
		}
	},

	onSendEmail : function(data) {
		if (data.success) {
			Ext.getCmp('emailRequirementWindow').close();
			Ext.Msg.alert('Success','Email sent successfully.');
			var filter = this.emailFilter;
			app.reportsService.sendSummaryEmail(Ext.encode(filter));	
		}else
			Ext.Msg.alert('Error',data.errorMessage);
	},

	shareRequirement : function() {
		this.manager.showSharePanel();
		var record = this.requirement.data;
		
		if (record.clientId != null && record.clientId != '') {
			var client = ds.clientSearchStore.getById(Number(record.clientId)).data.name;
		}else
			var client = '';
		
		var ids = this.submittedVendors.store.collect('vendorId');
		var names ="";
		for ( var i = 0; i < ids.length; i++) {
			var rec = ds.vendorStore.getById(parseInt(ids[i]));
			if (rec)
				names += rec.data.name +", ";	
		}
		var vendor = names.substring(0,names.length-2);

		var record = this.requirementPanel.form.getValues();
		this.manager.requirementSharePanel.data = record;
		var detailsTable = "<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>"+
							"<tr><th>Posting Date </th><td>"+Ext.Date.format(new Date(record.postingDate),'m/d/Y')+'</td></tr>'+
							"<tr><th>Posting Title</th><td>" +record.postingTitle+'</td></tr>'+
							"<tr><th>City & State</th><td>" +record.cityAndState+'</td></tr>'+
							"<tr><th>Client</th><td>" +client+'</td></tr>'+
							"<tr><th>Vendor</th><td>" +vendor+'</td></tr>'+
							"<tr><th>Alert</th><td>" +record.addInfo_Todos+'</td></tr>'+
							"<tr><th>Source</th><td>" +record.source+'</td></tr>'+
							"<tr><th>Module</th><td>" +record.module+'</td></tr>'+
							"<tr><th>Skill Set</th><td>" +record.skillSet+'</td></tr>'+
							"<tr><th>Role Set</th><td>" +record.targetRoles+'</td></tr>'+
							"<tr><th>Mandatory Certifications</th><td>" +record.certifications +'</td></tr>';

			if(record.remote)
				detailsTable += "<tr><th>Remote</th><td>Yes</td></tr>";
			else
				detailsTable += "<tr><th>Remote</th><td>No</td></tr>" ;
			
			if(record.relocate)
				detailsTable += "<tr><th>Relocation Needed</th><td>Yes</td></tr>" ;
			else
				detailsTable += "<tr><th>Relocation Needed</th><td>No</td></tr>" ;
			
			if(record.travel)
				detailsTable += "<tr><th>Travel Needed</th><td>Yes</td></tr>" ;
			else
				detailsTable += "<tr><th>Travel Needed</th><td>No</td></tr>" ;
			
			detailsTable += "<tr><th>Ok to train Candidate</th><td>" + record.helpCandidate +'</td></tr>'+
							"<tr><th>Roles and Responsibilities</th><td><br>" +record.requirement.replace(/(?:\r\n|\r|\n)/g, '<br>')+'</td></tr>'+
							'</table>';

		if (this.submittedCandidates.store.getCount() > 0) {
			detailsTable += "<br>Submissions : <table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>" +
					"<tr><th style='font-weight:bold;'>Name</th>" +
					"<th style='font-weight:bold;'>Home Location</th>" +
					"<th style='font-weight:bold;'>Comments</th>" +
					"<th style='font-weight:bold;'>Resume</th></tr>";
			for ( var i = 0; i < this.submittedCandidates.store.getCount(); i++) {
				var record = this.submittedCandidates.store.getAt(i).data;
				detailsTable += "<tr>" +
						"<td>"+record.candidate+"</td>" +
						"<td>"+record.cityAndState+"</td>" +
						"<td>"+record.comments+"</td>" ;
				if (record.resumeLink != null && record.resumeLink != '')
					detailsTable += "<td><a href="+record.resumeLink+" target='_blank'>View</a></td></tr>" ;
				else
					detailsTable += "<td>n/a</td></tr>" ;
			}
			detailsTable += "</table>";
		}

		this.manager.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').setValue(detailsTable);
		this.manager.requirementSharePanel.emailShortlisted.enable();
		this.manager.requirementSharePanel.emailConfidential.enable();
		this.manager.requirementSharePanel.fileName = record.id;
		this.manager.requirementSharePanel.openedFrom = 'Requirement Panel';
	},

	addGuide : function(type) {
		if (type == "Shortlisted")
			var selectedRecord = this.shortlistedCandidates.getView().getSelectionModel().getSelection()[0];
		else
			var selectedRecord = this.submittedCandidates.getView().getSelectionModel().getSelection()[0];
    	if (selectedRecord == null) {
    		Ext.Msg.alert('Error','Please select a record.');
    		return false;
		}
    	var guide = selectedRecord.data.guide;
    	guide = guide.split(',').map(Number);
    	var guides = new Array();
    	for ( var i = 0; i < guide.length; i++) {
			var rec = ds.contactSearchStore.getById(guide[i]);
			if (rec){
				guides.push(rec.data.fullName);
			}    		
    	}
    	guides = guides.toString();
    	
    	var panel = this;
		if (Ext.getCmp('guideWindow') == null) {
			var guideWindow = Ext.create('Ext.window.Window', {
			    title: 'Add guide',
			    id : 'guideWindow',
			    width: 380,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
				                xtype: 'combobox',
				                queryMode: 'local',
				                anyMatch:true,
				                forceSelection : true,
				                displayField: 'fullName',
				    		    valueField: 'id',
				                store: ds.contactSearchStore,
			        			fieldLabel: 'Guide',
			        			tabIndex : 201,
			        			width : 250,
								listeners:{
									select : function(combo,newValue,oldValue){
										var guides = Ext.getCmp('guideWindow').items.items[0].items.items[0].items.items[0].items.items[2].getValue();
										if (guides == null || guides == '' )
											guides = new Array();
										else
											guides = guides.split(',');
										if (guides.indexOf(combo.value.toString()) == -1) {
											guides.push(combo.value.toString());
										}else{
											guides.splice(guides.indexOf(combo.value.toString()), 1);
										}
										combo.reset();
										
								    	var guideNames = new Array();
								    	for ( var i = 0; i < guides.length; i++) {
											var rec = ds.contactSearchStore.getById(Number(guides[i]));
											if (rec){
												guideNames.push(rec.data.fullName);
											}    		
								    	}
										Ext.getCmp('guideWindow').items.items[0].items.items[0].items.items[0].items.items[1].setValue(guideNames.toString());
										Ext.getCmp('guideWindow').items.items[0].items.items[0].items.items[0].items.items[2].setValue(guides.toString());
									}
		   			            }	
			        		},{
			        			xtype: 'textarea',
			        			readOnly :true,
			        			name: 'guides',  
			        			fieldLabel: 'Selected Guides',
			        			tabIndex : 202,
			        			value : guides,
			        			width : 250,
			        		},{
			        			xtype: 'textfield',
			        			hidden :true,
			        			name: 'guide',  
			        			value : selectedRecord.data.guide,
			        		}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
			    	tabIndex : 203,
			    	tooltip : 'Save Guide',
		            handler: function(){
		            	var component = Ext.getCmp('guideWindow').items.items[0].items.items[0].items.items[0].items.items[2];
		            	selectedRecord.data.guide = component.value.toString();
		            	if (type == 'Shortlisted'){
		            		panel.modifiedShortlistIds.push(selectedRecord.data.id);
		            		panel.saveShortlist();
		            	}else{
		            		panel.modifiedCandidateIds.push(selectedRecord.data.id);
		            		panel.saveCandidates();
		            	}
		            }
		        },{
			    	text: 'Clear',
			    	tabIndex : 204,
			    	tooltip : 'Clear all guides',
		            handler: function(){
		            	Ext.getCmp('guideWindow').items.items[0].items.items[0].items.items[0].items.items[0].setValue(null);
		            	Ext.getCmp('guideWindow').items.items[0].items.items[0].items.items[0].items.items[1].setValue(null);
		            	Ext.getCmp('guideWindow').items.items[0].items.items[0].items.items[0].items.items[2].setValue(null);
		            }
		        }]
			});
		}
		Ext.getCmp('guideWindow').show();
	},
	
	addReference : function() {
		var selectedRecord = null;
    	selectedRecord = this.submittedCandidates.getView().getSelectionModel().getSelection()[0];
    	if (selectedRecord == null) {
    		Ext.Msg.alert('Error','Please select a record.');
    		return false;
		}
    	var reference = selectedRecord.data.reference;
    	reference = reference.split(',').map(Number);
    	var references = new Array();
    	var referencesTable = "";
    	for ( var i = 0; i < reference.length; i++) {
			var rec = ds.contactSearchStore.getById(reference[i]);
			if (rec){
				references.push(rec.data.fullName);
				referencesTable += "<tr bgcolor='#ffffff'><td>"+rec.data.fullName+"</td><td>"+rec.data.email+"</td><td>"+rec.data.cellPhone+"</td></tr>";
			}    		
    	}
    	references = references.toString();
    	if (referencesTable != '') {
			referencesTable = "<table cellpadding='5' bgcolor='gray'>"+
								"<tr bgcolor='#ffffff'><th>Reference Name</th><th>Email Id</th><th>Cell Phone</th></tr>"+
								referencesTable+
								"</table>";
		}
    	
    	
    	var panel = this;
		if (Ext.getCmp('referenceWindow') == null) {
			var referenceWindow = Ext.create('Ext.window.Window', {
			    title: 'Add Reference',
			    id : 'referenceWindow',
			    //width: 420,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
				                xtype: 'combobox',
				                queryMode: 'local',
				                anyMatch:true,
				                forceSelection : true,
				                displayField: 'fullName',
				    		    valueField: 'id',
				                store: ds.contactSearchStore,
			        			fieldLabel: 'Reference',
			        			//value : reference,
			        			tabIndex : 301,
			        			width : 250,
								listeners:{
									select : function(combo,newValue,oldValue){
										var references = Ext.getCmp('referenceWindow').items.items[0].items.items[0].items.items[0].items.items[2].getValue();
										if (references == null || references == '' )
											references = new Array();
										else
											references = references.split(',');
										if (references.indexOf(combo.value.toString()) == -1) {
											references.push(combo.value.toString());
										}else{
											references.splice(references.indexOf(combo.value.toString()), 1);
										}
										combo.reset();
										
								    	var referenceNames = new Array();
								    	for ( var i = 0; i < references.length; i++) {
											var rec = ds.contactSearchStore.getById(Number(references[i]));
											if (rec){
												referenceNames.push(rec.data.fullName);
											}    		
								    	}
										Ext.getCmp('referenceWindow').items.items[0].items.items[0].items.items[0].items.items[1].setValue(referenceNames.toString());
										Ext.getCmp('referenceWindow').items.items[0].items.items[0].items.items[0].items.items[2].setValue(references.toString());
									}
		   			            }	
			        		},{
			        			xtype: 'textarea',
			        			name: 'references',
			        			readOnly :true,
			        			fieldLabel: 'Selected References',
			        			value : references,
			        			width : 250,
			        			tabIndex : 302,
			        		},{
			        			xtype: 'textfield',
			        			hidden :true,
			        			name: 'reference',  
			        			value : selectedRecord.data.reference,
			        		},{
			        			xtype: 'displayfield',
			        			name: 'referencesTable',
			        			fieldLabel: '',
			        			value : referencesTable,
			        		}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Add Reference',
			    	iconCls : 'btn-add',
			    	tabIndex : 304,
			    	tooltip : 'Add Reference',
		            handler: function(){
		            	app.homeMgmtPanel.addContact('Reference');
		            }
		        },{
			    	text: 'Save',
			    	tabIndex : 305,
			    	tooltip : 'Save Reference',
		            handler: function(){
		            	var component = Ext.getCmp('referenceWindow').items.items[0].items.items[0].items.items[0].items.items[2];
		            	selectedRecord.data.reference = component.value.toString();
		            	panel.modifiedCandidateIds.push(selectedRecord.data.id);
		            	panel.saveCandidates();
		            }
		        },{
			    	text: 'Clear',
			    	tabIndex : 306,
			    	tooltip : 'Clear all References',
		            handler: function(){
		            	Ext.getCmp('referenceWindow').items.items[0].items.items[0].items.items[0].items.items[0].setValue(null);
		            	Ext.getCmp('referenceWindow').items.items[0].items.items[0].items.items[0].items.items[1].setValue(null);
		            	Ext.getCmp('referenceWindow').items.items[0].items.items[0].items.items[0].items.items[2].setValue(null);
		            }
		        }]
			});
		}
		Ext.getCmp('referenceWindow').show();
	},

	getRequirement : function(requirementId) {
		if (requirementId != null  && requirementId != '') {
			var params = new Array();
	    	params.push(['id','=', requirementId]);
	    	var filter = getFilter(params);
	    	app.requirementService.getRequirements(Ext.JSON.encode(filter),this.onGetRequirement,this);
		}
	},
	
	onGetRequirement : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = Ext.create('tz.model.Requirement', data.rows[0]);
			this.loadForm(record);
		}
	},

    showVendor : function(record) {
    	var params = new Array();
    	params.push(['id','=', record.data.id]);
    	var filter = getFilter(params);
    	app.clientService.getClients(Ext.JSON.encode(filter),this.onGetVendor,this);
	},
	
	onGetVendor : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = Ext.create('tz.model.Client', data.rows[0]);
	    	app.setActiveTab(0,'li_clients');
	    	app.clientMgmtPanel.getLayout().setActiveItem(1);
	    	app.clientMgmtPanel.clientPanel.loadForm(record);
	    	app.clientMgmtPanel.clientPanel.openedFrom = 'Recruit';
		}
	},
	
	loadXMLDoc : function(filename) {
		 var xmlDoc;
		 try {
			 var xmlhttp = new XMLHttpRequest();
			 xmlhttp.open('GET', filename, false);
			 xmlhttp.setRequestHeader('Content-Type', 'text/xml');
			 xmlhttp.send('');
			 xmlDoc = xmlhttp.responseXML;
		 } catch (e) {
			 try {
				 xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			 } catch (e) {
				 console.error(e.message);
			 }
		 }
		 return xmlDoc;
	},
	
	copyToCertifications : function() {
		var certifications = this.requirementPanel.form.findField('certifications').getValue();
		var skillSetList = this.requirementPanel.form.findField('skillSet').getValue();
		certifications = certifications.split(',');
		skillSetList = skillSetList.split(',');
		var skillsetIds = new Array();
		ds.skillSetStore.clearFilter();
		for ( var i = 0; i < skillSetList.length; i++) {
			skillsetIds.push(ds.skillSetStore.getAt(ds.skillSetStore.findExact('name',skillSetList[i])).data.id);
		}
		var xmlDoc= this.loadXMLDoc("Xml files/SkillsetMapping.xml");
		for ( var i = 0; xmlDoc.getElementsByTagName("mapping") [i] != null ; i++) {
			var mapping=xmlDoc.getElementsByTagName("mapping") [i];
			var skillsetTag = mapping.getElementsByTagName('skillset')[0];
			var skillsetId = skillsetTag.getAttribute('id');
			var innerSkillsetIds = skillsetId.split(',');
			for ( var j = 0; j < innerSkillsetIds.length; j++) {
				if (skillsetIds.indexOf(Number(innerSkillsetIds[j])) != -1) {
					var certify = mapping.getElementsByTagName('cerification')[0];
					var certificationNames = certify.getAttribute('name').split(',');
					if (certifications.indexOf(certificationNames[j]) == -1) {
						certifications.push(certificationNames[j]);	
					}//if
				}//if
			}//for
		}//for
		for ( var i = 0; i < certifications.length; i++) {
			if (certifications[i] == ''){
				certifications.splice(i, 1);
				i--;
			}
		}

		if (certifications.length > 0) {
			this.requirementPanel.form.findField('certifications').setValue(certifications.toString());
		}
	},
	
	copyToSkillset : function() {
		var certifications = this.requirementPanel.form.findField('certifications').getValue();
		var skillSetList = this.requirementPanel.form.findField('skillSet').getValue();
		certifications = certifications.split(',');
		skillSetList = skillSetList.split(',');
		var certifyIds = new Array();
		ds.certificationsStore.clearFilter();
		for ( var i = 0; i < certifications.length; i++) {
			if (certifications[i] == ''){
				certifications.splice(i, 1);
				i--;
			}else
				certifyIds.push(ds.certificationsStore.getAt(ds.certificationsStore.findExact('name',certifications[i])).data.id);
		}
		var xmlDoc= this.loadXMLDoc("Xml files/SkillsetMapping.xml");
		for ( var i = 0; xmlDoc.getElementsByTagName("mapping") [i] != null ; i++) {
			var mapping=xmlDoc.getElementsByTagName("mapping") [i];
			var certify = mapping.getElementsByTagName('cerification')[0];
			var certifyId = certify.getAttribute('id');
			if (certifyIds.indexOf(Number(certifyId)) != -1) {
				var skillsetTag = mapping.getElementsByTagName('skillset')[0];
				var skillset = skillsetTag.getAttribute('name').split(',');
				for ( var j = 0; j < skillset.length; j++) {
					if (skillSetList.indexOf(skillset[j]) == -1) {
						skillSetList.push(skillset[j]);	
					}//if
				}//for
			}//if
		}//for
		for ( var i = 0; i < skillSetList.length; i++) {
			if (skillSetList[i] == ''){
				skillSetList.splice(i, 1);
				i--;
			}
		}
		
		if (skillSetList.length > 0) {
			this.requirementPanel.form.findField('skillSet').setValue(skillSetList.toString());
		}
	},
	
	getInterview : function(record,typeOfAccess) {
		if (record.data.id != null && record.data.id != '') {
			var params = new Array();
	    	params.push(['candidate','=', record.data.candidateId]);
	    	params.push(['client','=', record.data.clientId]);
	    	params.push(['vendor','=', record.data.vendorId]);
	    	params.push(['requirementId','=', record.data.requirementId]);
	    	var filter = getFilter(params);
    		app.interviewService.getInterviews(Ext.JSON.encode(filter),this.showInterview,this);
		}
	},
	
	showInterview : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = data.rows[0];
			var interviewer ="";
        	if (record.contactId != null && record.contactId != '') {
				var ids = record.contactId.split(',');
				for ( var i = 0; i < ids.length; i++) {
					var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
					if (rec)
						interviewer += rec.data.fullName +", ";	
				}
				interviewer = interviewer.substring(0,interviewer.length-2);
        	}
			
			var detailsTable = "&nbsp<a onclick='javascript:app.requirementMgmtPanel.requirementDetailPanel.getInterviewById("+record.id+")' href=# title='Edit' >" +
								"<img align='top' src='images/icon_edit.gif'>Edit Interview</a><br><br>"+
								"<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>"+
								"<tr><th>Job Id</th><td>"+ record.requirementId +'</td></tr>'+
								"<tr><th>Candidate</th><td>"+record.candidate+'</td></tr>'+
								"<tr><th>Client</th><td>" +record.client+'</td></tr>'+
								"<tr><th>Vendor</th><td>" +record.vendor+'</td></tr>'+
								"<tr><th>Interviewer</th><td>" +interviewer+'</td></tr>'+
								"<tr><th>Interview Date</th><td>" +record.interviewDate+'</td></tr>'+
								"<tr><th>Local Time</th><td>" +record.currentTime+'</td></tr>'+
								"<tr><th>Timezone</th><td>" +record.timeZone+'</td></tr>'+
								"<tr><th>PST Time</th><td>" +record.time+'</td></tr>'+
								"<tr><th>Interview</th><td><br>" +record.interview+'</td></tr>'+
								"<tr><th>Employee Feedback</th><td><br>" +record.employeeFeedback+'</td></tr>'+
								"<tr><th>Vendor Feedback</th><td><br>" +record.vendorFeedback+'</td></tr></table>';

			if (Ext.getCmp('showInterviewWindow') == null) {
				var legendWin = Ext.create("Ext.window.Window", {
					title: 'Interview',
					id : 'showInterviewWindow',
					autoScroll:true,
					modal: true,
					height: 600,
					width : 800,
					bodyStyle : 'padding: 1px;background:#ffffff;',
					html: detailsTable
				});
				legendWin.show();
			}
		}else
			Ext.Msg.alert('Error','Interview not Exist.');
	},

	getInterviewById : function(id) {
		if (id != null && id != '') {
			if (Ext.getCmp('showInterviewWindow') != null)
				Ext.getCmp('showInterviewWindow').close();

			var params = new Array();
	    	params.push(['id','=', id]);
	    	var filter = getFilter(params);
    		app.interviewService.getInterviews(Ext.JSON.encode(filter),this.editInterview,this);
		}
	},

	editInterview : function(data) {
		if (data.success && data.rows.length > 0) {
			var record = Ext.create('tz.model.Interview', data.rows[0]);
			app.setActiveTab(0,'li_interviews');
			app.interviewMgmtPanel.interviewGridPanel.editInterview(record);
			app.interviewMgmtPanel.interviewDetailPanel.opendFrom = 'Recruit';
		}
	},
	
	showSuggestedPopout : function() {
    	var browserHeight = app.mainViewport.height;
    	var browserWidth = app.mainViewport.width;
    	var actualPotentialCandidates = this.potentialCandidates;
    	
    	var potentialCandidates = new tz.ui.SuggestedCandidates({
    		manager:this,
    		scope : this,
    	});
    	potentialCandidates.on({
    		itemclick:   function(dv, record, item, index, e) {
	    		app.requirementMgmtPanel.requirementDetailPanel.potentialCandidates.getView().select(record);                                
    		},
    	});
    	potentialCandidates.genderCombo.on({
    		select:   function(combo) {
	    		app.requirementMgmtPanel.requirementDetailPanel.potentialCandidates.genderCombo.setValue(combo.value);                                
    		},
    	});
    	potentialCandidates.ottCheckbox.on({
    		change:   function(checkbox,value) {
	    		app.requirementMgmtPanel.requirementDetailPanel.potentialCandidates.ottCheckbox.setValue(value);                                
    		},
    	});
    	//potentialCandidates.dockedItems.items[1].items.items[11].hide();
    	
    	var snapshotWindow = Ext.create('Ext.window.Window', {
			height : browserHeight-100,
			width: browserWidth*80/100,
			modal: false,
			layout: 'fit',
			border : 0,
			items: [potentialCandidates],
		});
		snapshotWindow.show();
	}

});

function timeToString(time) {
	if (time != null && time != '') {
		var hour    = time.getHours();
	    var minute  = time.getMinutes();
	    var second  = time.getSeconds(); 
	    if(hour.toString().length == 1) 
	        var hour = '0'+hour;
	    if(minute.toString().length == 1) 
	        var minute = '0'+minute;
	    if(second.toString().length == 1) 
	        var second = '0'+second;
	    
	    var stringTime = hour+':'+minute+':'+second; 
	    return stringTime;
	}
}

function dateToTime(date) {
	if (date == null || date == '')
		return null;
	if (date.getHours() == 0 && date.getMinutes() == 0)
		return '12:00 AM';
	if (date != null) {
		var hour    = date.getHours();
	    var minute  = date.getMinutes();
	    if (hour ==0)
	    	hour = 12;
	    if (hour > 12){
		    if(minute.toString().length == 1) 
		        minute = '0'+minute;
			time = Number(hour) -12 +':'+minute ;
	    }else{
		    if(minute.toString().length == 1) 
		        minute = '0'+minute;
			time = hour +':'+minute ;
		}
	    if (hour > 11)
	    	time += ' PM';
	    else
	    	time += ' AM';
	    return time;
	}
    return null;
}
