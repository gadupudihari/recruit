Ext.define('tz.ui.RequirementSharePanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 0,
    title: '',
    anchor:'60% 100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'left',labelWidth :100},
    
    initComponent: function() {
        var me = this;
        me.emailFilter ;
        me.candidates = new Array();
        me.emailIds = new Array();
        me.fileName = '';
        me.openedFrom = '';
        me.data = '';
        me.emailConfidentialData = false;
        me.subject = '';
        
        me.emailButton = new Ext.Button({
    		text: 'Email',
    		tooltip: 'Send Email',
    		iconCls : 'btn-email',
    		tabIndex:2,
    		scope: this,
    		handler: function() {
    			me.emailConfirm();
			}
        });

        me.emailShortlisted = new Ext.Button({
    		text: 'Email Shortlisted',
    		tooltip: 'Send Email to Shortlisted candidates',
    		iconCls : 'btn-email',
    		scope: this,
    		tabIndex:3,
    		handler: function() {
    			me.emailShortlistConfirm();
			}
        });

        me.emailConfidential = new Ext.Button({
    		text: 'Email with Client info',
    		tooltip: 'Send Email with Client information',
    		iconCls : 'btn-email',
    		tabIndex:7,
    		scope: this,
    		handler: function() {
    			me.emailClientConfirm();
			}
        });

        me.pdfExportButton = new Ext.Button({
            xtype: 'button',
            text: 'Generate PDF',
            tooltip: 'Generate PDF',
            iconCls : 'btn-report-pdf',
            tabIndex:4,
            scope : this,
            handler:function(){
            	this.generatePDF();
            }
        });
        
        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [me.emailButton,'-',me.emailShortlisted,'-',me.pdfExportButton,'-',
        	        {
                		xtype: 'button',
                		text: 'Print',
                		tooltip: 'Print',
                		icon : 'images/printer.png',
                		tabIndex:5,
                		scope : this,
                		handler: function(){
                			me.print();
                		}
        	        },'-',{
                		xtype: 'button',
                		text: 'Close',
                		tooltip: 'Close',
                		iconCls : 'btn-close',
                		tabIndex:6,
                		scope : this,
                		handler: function(){
                			if (me.openedFrom == "Home")
                				me.manager.getLayout().setActiveItem(0);
                			else if (me.openedFrom == "Requirement Grid" || me.openedFrom == "Candidates Grid" )
                				me.manager.getLayout().setActiveItem(0);
                			else
                				me.manager.getLayout().setActiveItem(1);
                		}
        	        },'-',me.emailConfidential]
        }];

        me.criteriaPanel = Ext.create('Ext.form.Panel', {
        	frame:false,
        	xtype: 'container',
        	border : 0,
        	items:[{
        		xtype : 'fieldset',
				border : 0,
				layout : 'column',
				items : [{
	   	        	 xtype: 'container',
	   	        	 columnWidth:1,
	   	             items: [{
	   	            	 xtype:'displayfield',
	   	                 fieldLabel: '',
	   	                 name: 'jobOpeningDetails',
	   	                 tabIndex:1,
	   	             }]
	   	         }]
        	}]
	     });

        me.items = [this.getMessageComp(), this.getHeadingComp(),
                    {
        				border : false,
        				height: 10,
                    },
                    me.criteriaPanel
        ];

        me.callParent(arguments);
    },
	
	showEmail : function(message,store) {
		var emailWindowPanel = new tz.ui.EmailWindowPanel();
		if (this.manager.itemId != "homeMgmtPanel") {
			if (store != null) {
				this.candidates = store.collect('fullName');
				this.emailIds = store.collect('emailId');
				if (store.getCount() > 0 && store.getAt(0).data.fullName== null)
					this.candidates = store.collect('candidate');
				emailWindowPanel.candidates = this.candidates;
				emailWindowPanel.emailIds = this.emailIds;
			}else{
				this.candidates = new Array();
				this.emailIds = new Array();
			}
		}
		emailWindowPanel.form.findField('subject').setValue(this.subject);
		emailWindowPanel.form.findField('message').setValue(message +'<br><br>'+'Regards'+'<br>'+'Team');
		var manager = this.manager;
		emailWindowPanel.candidatesPanel.doLayout();
		
		var container = new Ext.container.Container({
			id : 'emailCandidatesContainer1',
			layout: {
		        type: 'table',
		        columns: 4,
		    },
		    items: []
		});
		var items = [];
		for ( var i = 0; i < this.candidates.length; i++) {
	        var field = new Ext.form.Label({
	        	id:this.emailIds[i],
	            html:this.candidates[i]+'&nbsp <a onclick="javascript:app.homeMgmtPanel.removeEmailId(\''+this.emailIds[i]+'\');" href="#"></a>',
	            cls:'email-labels',  
	            style: {
	            	color: '#FFFFFF',
	            	backgroundColor:'#4C4C4C'
	            },
	        });
	        items.push(field);
		}
		container.items.add(items);
		emailWindowPanel.candidatesPanel.add(container);
	
		var addEmailButton = new Ext.Button({
			id : 'addEmailButton_seperate',
            text: '',
            iconCls : 'btn-add',
            handler: function(){
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').reset();
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').reset();
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').show();
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').show();
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').allowBlank = true;
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').allowBlank = true;

            	Ext.getCmp('addEmailButton_seperate').hide();
            	Ext.getCmp('saveEmailButton_seperate').show();
            	emailWindow.items.items[0].candidatesPanel.doLayout();
    		}
        });

		var saveEmailButton = new Ext.Button({
			margin : '0 0 0 10',
			tooltip : 'Add Email Id',
            id : 'saveEmailButton_seperate',
            icon : 'images/accept.png',
            hidden : true,
            handler: function(){
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').allowBlank = false;
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').allowBlank = false;

            	if (! emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').isValid() || 
            			! emailWindow.items.items[0].candidatesPanel.form.findField('emailId').isValid() ) {
            		Ext.Msg.alert('Error','Please fix the errors.');
            		return false;
				}

            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').allowBlank = true;
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').allowBlank = true;
            	
            	var candidateName = emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').getValue();
            	var emailId = emailWindow.items.items[0].candidatesPanel.form.findField('emailId').getValue();
            	var field = new Ext.form.Label({
    	        	id: emailId,
    	            html: candidateName+'&nbsp <a onclick="javascript:app.homeMgmtPanel.removeEmailId(\''+emailId+'\');" href="#"></a>',
    	            cls:'email-labels',  
    	            style: {
    	            	color: '#FFFFFF',
    	            	backgroundColor:'#4C4C4C'
    	            },
    	        });
            	emailWindow.items.items[0].candidatesPanel.items.items[0].items.add(field);
            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').hide();
            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').hide();
            	Ext.getCmp('addEmailButton_seperate').show();
            	Ext.getCmp('saveEmailButton_seperate').hide();
            	emailWindow.items.items[0].candidatesPanel.doLayout();
				manager.requirementSharePanel.emailIds.push(emailId);
				manager.requirementSharePanel.candidates.push(candidateName);
    		}
        });

		var container2 = new Ext.container.Container({
			id : 'emailCandidatesContainer2',
			fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth :50},
			layout: {
		        type: 'table',
		        columns: 4,
		    },
		    items: [addEmailButton,
		            {
		    	xtype: 'textfield',
				name: 'candidateName',
				fieldLabel: 'Name',
				hidden : true,
				labelWidth :50
			},{
				xtype: 'textfield',
				name: 'emailId',
				fieldLabel: 'Email Id',
				hidden : true,
				vtype: 'email',
				labelWidth :60
			},
			saveEmailButton
			]
		});
		emailWindowPanel.candidatesPanel.add(container2);

		if (Ext.getCmp('emailRequirementWindow') == null) {
			var emailWindow = Ext.create('Ext.window.Window', {
			    title: 'Email',
			    id : 'emailRequirementWindow',
			    width: 1000,	
			    layout: 'fit',
			    modal: true,
			    autoScroll:true,
			    items: [emailWindowPanel],
			    buttons: [{
			    	text: 'Send All',
		            handler: function(){
		            	emailWindow.items.items[0].candidatesPanel.form.findField('candidateName').allowBlank = true;
		            	emailWindow.items.items[0].candidatesPanel.form.findField('emailId').allowBlank = true;

		            	if (emailWindow.items.items[0].form.isValid()) {
		            		var params = new Array();
		            		params.push(['from','=', emailWindow.items.items[0].fromEmailCombo.getValue()]);
		            		params.push(['to','=', manager.requirementSharePanel.emailIds.toString()]);
		            		params.push(['candidates','=', manager.requirementSharePanel.candidates.toString()]);
		            		params.push(['subject','=', emailWindow.items.items[0].form.findField('subject').getValue()]);
		            		params.push(['message','=', emailWindow.items.items[0].form.findField('message').getValue()]);
			            	var filter = getFilter(params);
			            	manager.requirementSharePanel.sendEmailConform(filter);
						}//if
		            }//handler
		        }]//buttons
			});
		}
		Ext.getCmp('emailRequirementWindow').show();
	},

	sendEmailConform : function(filter) {
		if (this.emailConfidentialData) {
			var panel = this;
			Ext.MessageBox.show({
				title:'Confirm',
	            msg: "You are sending client/vendor and other confidential information",
	            buttonText: {                        
	                yes : "Don't send",
	                no : "Send"
	            },
	            icon: Ext.MessageBox.INFO,
	            fn: function(btn) {
	            	if (btn == 'no') {
	            		panel.emailConfidentialData = false;
	            		panel.sendEmailConform(filter);
					}
	            }
	        });
		}else{
			this.emailFilter = filter;
			Ext.Msg.confirm("Confirm","Do you want to send email to "+this.emailIds.length+" candidates?", this.sendEmail, this);
		}
	},

	sendEmail : function(dat) {
		if (dat=='yes') {
			var filter = this.emailFilter;
			app.reportsService.sendHomeEmail(Ext.encode(filter),this.onSendEmail,this);	
		}
	},
	
	onSendEmail : function(data) {
		if (data.success) {
			Ext.getCmp('emailRequirementWindow').close();
			Ext.Msg.alert('Success','Email sent successfully.');
			var filter = this.emailFilter;
			app.reportsService.sendSummaryEmail(Ext.encode(filter));	
		}else
			Ext.Msg.alert('Error',data.errorMessage);
	},

	print : function() {
        var myWindow = window.open( );
        myWindow.document.write('<html><head>');
        myWindow.document.write('</head><body>');
        myWindow.document.write(this.criteriaPanel.form.findField('jobOpeningDetails').getValue());
        myWindow.document.write('</body></html>');
        myWindow.print();
	},
	
	generatePDF : function() {
		var pdfValue = this.criteriaPanel.form.findField('jobOpeningDetails').getValue();
		pdfValue = "<html><head></head><body>"+pdfValue+"</body></html>";
		var params = new Array();
		params.push(['pdfValue','=', pdfValue]);
		params.push(['fileName','=', this.fileName]);
    	var filter = getFilter(params);
		app.reportsService.generatePDF(Ext.JSON.encode(filter));
	},

	emailConfirm : function() {
		if (this.data == '') {
			var message = this.form.findField('jobOpeningDetails').getValue();
		}else{
			var message = this.generateMessage();
		}
		this.showEmail(message);
		this.emailConfidentialData = false;
	},

	emailShortlistConfirm : function() {
		var message = this.generateMessage();
		this.showEmail(message,this.manager.requirementDetailPanel.shortlistedCandidates.store);
		this.emailConfidentialData = false;
	},

	emailClientConfirm : function() {
		var message = this.form.findField('jobOpeningDetails').getValue();
		this.showEmail(message);
		this.emailConfidentialData = true;
	},

	generateMessage : function() {
		var record = this.data;
		var detailsTable = "<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>"+
							"<tr><th>Posting Date </th><td>"+Ext.Date.format(new Date(record.postingDate),'m/d/Y')+'</td></tr>'+
							"<tr><th>Posting Title</th><td>" +record.postingTitle+'</td></tr>'+
							"<tr><th>City & State</th><td>" +record.cityAndState+'</td></tr>'+
							"<tr><th>Module</th><td>" +record.module+'</td></tr>'+
							"<tr><th>Skill Set</th><td>" +record.skillSet+'</td></tr>'+
							"<tr><th>Role Set</th><td>" +record.targetRoles+'</td></tr>'+
							"<tr><th>Mandatory Certifications</th><td>" +record.certifications +'</td></tr>';

		if(record.remote)
		detailsTable += "<tr><th>Remote</th><td>Yes</td></tr>";
		else
		detailsTable += "<tr><th>Remote</th><td>No</td></tr>" ;
		
		if(record.relocate)
		detailsTable += "<tr><th>Relocation Needed</th><td>Yes</td></tr>" ;
		else
		detailsTable += "<tr><th>Relocation Needed</th><td>No</td></tr>" ;
		
		if(record.travel)
		detailsTable += "<tr><th>Travel Needed</th><td>Yes</td></tr>" ;
		else
		detailsTable += "<tr><th>Travel Needed</th><td>No</td></tr>" ;
		
		detailsTable += "<tr><th>Roles and Responsibilities</th><td><br>" +record.requirement.replace(/(?:\r\n|\r|\n)/g, '<br>')+'</td></tr></table>';

		return detailsTable;
	}
});
