Ext.define('tz.ui.EmailWindowPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 10,
    title: '',
    anchor:'100% 100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'right'},
    
    initComponent: function() {
        var me = this;
        me.requirement ="";
        me.candidates = new Array();
        me.emailIds = new Array();

        me.fromEmailCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: '<a onclick="javascript:app.homeMgmtPanel.addUserEmail()" href=# title="Send Email from this email id">From</a>',
		    store: ds.userEmailStore,
		    queryMode: 'local',
		    displayField: 'emailId',
            valueField: 'emailId',
		    name : 'from',
		    editable: false,
		    forceSelection : true,
		    allowBlank: false,
		    labelAlign: 'left',
		    labelWidth : 50,
		});

        me.candidatesPanel = Ext.create('Ext.form.Panel', {
        	border : 1,
        	bodyPadding: 5,
        	fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth : 120},
            frame:false,
        });

        me.items = [{
        	xtype:'fieldset',
        	border : 0,
        	collapsible: false,
        	layout: 'column',
        	items :[{
        		xtype: 'container',
        		columnWidth:1,
        		items: [
        		        me.fromEmailCombo,
        		        {
        		        	xtype: 'displayfield',
        		        	value: 'Send Individual emails To :',
        		        },
        		        me.candidatesPanel,
        		        {
        		        	xtype: 'textfield',
        		        	name: 'subject',
        		        	labelAlign: 'top',
        		        	fieldLabel: 'Subject:',
        		        	emptyText: 'Enter email Subject',
        		        	allowBlank: false,
        		        	width : 920,
        		        },{
        		        	xtype: 'htmleditor',
        		        	width : 920,
        		        	height:350,
        		        	name: 'message',  
        		        	labelAlign: 'top',
        		        	fieldLabel: 'Body',
        		        	emptyText: 'Enter email Body',
        		        	allowBlank: false,
        		        }]
        	}]
        }];

        me.callParent(arguments);
    },

    addEmail : function() {
    	var emailWindow = Ext.create('Ext.window.Window', {
    		title: 'Add Email id',
    		width: 250,
    		modal: true,
    		bodyPadding: 0,
    		layout: 'fit',
    		items: [{
    			xtype: 'form',
    			fieldDefaults: { msgTarget: 'side',labelAlign: 'right',labelWidth :50},
    			bodyPadding: 10,
    			items: [{
    				xtype: 'fieldset',
    				layout: 'anchor',
    				border : 0,
    				items: [{
    					xtype: 'container',
    					layout: 'anchor',                        
    					defaults: {anchor: '100%'},
    					items: [{
    						xtype: 'textfield',
    						name: 'candidateName',
    						fieldLabel: 'Name',
    						allowBlank: false,
    					},{
    						xtype: 'textfield',
    						name: 'emailId',
    						fieldLabel: 'Email Id',
    						allowBlank: false,
    						vtype: 'email'
    					}]
    				}]
    			}]
    		}],
    		buttons: [{
    			text: 'Add',
    			handler: function(){
    				var valuesObj = {};
    				for ( var i = 0; i < 2; i++ ) {
    					var component = emailWindow.items.items[0].items.items[0].items.items[0].items.items[i];
    					if(! component.isValid()){
    						Ext.Msg.alert('Error','Please fix the errors.');
    						return false;
    					}else{
    						valuesObj[component.name] = component.value;
    					}
    				}
    				app.requirementMgmtPanel.requirementDetailPanel.emailIds.push(valuesObj.emailId);
    				app.requirementMgmtPanel.requirementDetailPanel.candidates.push(valuesObj.candidateName);
    				emailWindow.close();
    			}
    		}]
    	});
    	emailWindow.show();
	},
	
});
