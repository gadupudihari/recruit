Ext.define('tz.ui.RequirementsGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Requirements',
    id : 'reqGridPanel',
    frame:true,
    anchor:'100%',
    //height:550,
    
    width :'100%',    
    listeners: {
    	edit: function(editor, event){
    		var record = event.record;
            if (editor.context.field == 'requirement') {
            	while (record.data.requirement.indexOf('"') != -1) {
            		record.data.requirement = record.data.requirement.replace('"','');	
				}
                this.getView().refresh();
            }else if (editor.context.field == 'vendorId') {
            	record.data.contactId = null;
            	this.getView().refresh();
            }
            if(event.originalValue != event.value && record.data.id != null){
    			this.modifiedIds.push(record.data.id);
    		}
            var scrollPosition = this.getEl().down('.x-grid-view').getScroll();
        	this.getView().refresh();
        	//And then after the record is refreshed scroll to the same position
        	this.getEl().down('.x-grid-view').scrollTo('top', scrollPosition.top, false);
        },
        beforeedit: function(editor, event) {
      		var record = event.record;
      		if (event.column.dataIndex == "vendorId" ) {
      			if (record.data.id != '' && record.data.id != null && record.data.id != 0 ) {
      				return false;
				}
      		}else if (event.column.dataIndex == "contactId") {
      			if (record.data.id != '' && record.data.id != null && record.data.id != 0 )
      				return false;

				var contactCombo = Ext.getCmp('requirmentContactCombo');
				contactCombo.setValue('');
				contactCombo.setDisabled(false);
                var contacts = new Array();
                ds.contactSearchStore.clearFilter(true);
      			ds.contactSearchStore.filterBy(function(rec) {
      			    return rec.get('clientId') === parseInt(record.data.vendorId);
      			});
      			for ( var i = 0; i < ds.contactSearchStore.getCount(); i++) {
					contacts.push(ds.contactSearchStore.getAt(i));
				}
      			var newContactsStore = new tz.store.ContactSearchStore({data: contacts}); 
      			contactCombo.bindStore(newContactsStore);
      			this.getView().refresh();
      			return true;
			}else if (event.column.dataIndex == "module" && record.data.module != null && record.data.module != '') {
				return false;
			}
        }
    },
     
    initComponent: function() {
        var me = this;
        me.store = ds.requirementStore;
        me.modifiedIds = new Array();
        me.modifiedCandidateIds = new Array();
        me.removeRecord = "";
        //me.selModel = Ext.create('Ext.selection.CheckboxModel');
        
        me.contactsCombo = Ext.create('Ext.form.ComboBox', {
        	id : 'requirmentContactCombo',
        	displayField: 'fullName',
            valueField: 'id',
            listClass: 'x-combo-list-small',
            store: ds.contactSearchStore,
		    queryMode: 'local',
		    typeAhead: true,
		    anyMatch:true,
            triggerAction: 'all',
            selectOnTab: true,
            emptyText:'Select...',
            lazyRender: true,
		    tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item"><b>{fullName}</b></div>','</tpl>'),
		});

        me.columns = [
            {
                xtype: 'actioncolumn',
                width :40,
                items : [{
    				icon : 'images/icon_edit.gif',
    				tooltip : 'Edit Requirement',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					app.requirementMgmtPanel.showRequirement(rowIndex);
    				}
    			}]
            },{
                xtype: 'actioncolumn',
                width :35,
                items : [{
    				icon : 'images/icon_reschedule.gif',
    				tooltip : 'View Requirement',
    				padding: 50,
    				scope: this,
    				handler : function(grid, rowIndex, colIndex) {
    					this.shareRequirement(this.store.getAt(rowIndex));
    				}
    			}]
            },{
		        xtype: 'gridcolumn',
		        dataIndex: 'hotness',
		        text: 'Hotness',
		        width :  45,
                editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                forceSelection:true,
	                displayField: 'value',
	                valueField: 'name',
	                emptyText:'Select...',
	    		    store : ds.hotnessStore,
	                lazyRender: true,
	                listClass: 'x-combo-list-small',
	                tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item"><b>{value}</b></div>','</tpl>'),
	            },
                renderer:function(value, metadata, record){
                	if (value == 0) {
                		metadata.style = "color:#009AD0;";
					}
                	var rec = ds.hotnessStore.getAt(ds.hotnessStore.findExact('name',value));
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.value.substring(rec.data.value.indexOf('.')+1)+ '"';
                		return rec.data.value.substring(rec.data.value.indexOf('.')+1);
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;	
                	}
	            }
		    },{
				text : 'Unique Id',
				dataIndex : 'id',
				width :  45,
		        renderer: numberRender,
			},{
				text : 'Req No',
				dataIndex : 'requirementNo',
				width :  45,
		        renderer: numberRender,
			},{
    			text : 'Job Opening Status',
    			dataIndex : 'jobOpeningStatus',
    			width :  60,
                editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    forceSelection:true,
                    displayField: 'value',
        		    valueField: 'name',
                    store: ds.jobOpeningStatusStore,
                    lazyRender: true,
                    listClass: 'x-combo-list-small',
                    tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item"><b>{value}</b></div>','</tpl>'),
                },
    			renderer:function(value, metadata, record){
            		metadata.tdAttr = 'data-qtip="' + value.substring(value.indexOf('.')+1)+ '"';
            		return value.substring(value.indexOf('.')+1);
    			}
    		},{
    			xtype: 'datecolumn',
				text : 'Posting Date',
				dataIndex : 'postingDate',
				width :  60,
				format: "m/d/Y h:i:s A",
				editor: {
	                xtype: 'datefield',
	                format: 'm/d/y'
	            }
			},{
		        xtype: 'gridcolumn',
		        dataIndex: 'postingTitle',
		        text: 'Posting Title',
		        editor: {
	                xtype: 'textfield',
                    maxLength:100,
   	                enforceMaxLength:true,
	            },
                renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + value + '"';
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
            		return value;
                }
		    },{
                xtype: 'gridcolumn',
                dataIndex: 'module',
                text: 'Module',
                width :  60,
                editor: {
                	matchFieldWidth: false,
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                forceSelection:true,
	                displayField: 'value',
	                valueField: 'name',
	                emptyText:'Select...',
	    		    store : ds.skillSetStore,
	                lazyRender: true,
	                listClass: 'x-combo-list-small',
	                tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item"><b>{value}</b></div>','</tpl>'),
	            },
                renderer:function(value, metadata, record){
                	metadata.tdAttr = 'data-qtip="' + 'P-'+value +'<br>O-'+record.data.secondaryModule+ '"';
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
            		return value;
                }
            },{
				header : 'Client',
				dataIndex : 'clientId',
	            width :  120,
		        editor: {
	                xtype: 'combobox',
	                anyMatch:true,
	                forceSelection:true,
	                queryMode: 'local',
	                triggerAction: 'all',
	                displayField: 'name',
	                valueField: 'id',
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.clientSearchStore,
	                tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item"><b>{name}</b></div>','</tpl>'),
	            },
	            renderer:function(value, metadata, record){
                	var rec = ds.clientSearchStore.getById(value);
                	metadata.style = 'background-color:#'+getBackGround(record)+';'
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.name + '"';
                		return rec.data.name;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
				header : 'Vendor',
				dataIndex : 'vendorId',
	            width :  120,
                editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                triggerAction: 'all',
	                displayField: 'name',
	                valueField: 'id',
	                anyMatch:true,
	                forceSelection:true,
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.vendorStore,
	            },
	            renderer:function(value, metadata, record){
					metadata.style = 'background-color:#'+getBackGround(record)+';'
	            	if (value != null && value != '') {
						var ids = value.toString().split(',');
						var names ="";
						for ( var i = 0; i < ids.length; i++) {
							var rec = ds.vendorStore.getById(parseInt(ids[i]));
							if (rec)
								names += rec.data.name +", ";	
						}
						metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
						if (ids.length > 1)
							return '('+ids.length+') '+ names.substring(0,names.length-2);
						return names.substring(0,names.length-2);
					}
	            	return 'N/A';
                }
			},{
				text : "Alert",
				dataIndex : 'addInfo_Todos',
				width :  80,
		        editor: {
	                xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
   	                fieldStyle: "color: red;",
	            },
                renderer:alertRender
			},{
                xtype: 'gridcolumn',
                dataIndex: 'rateSubmitted',
                text: 'Max Bill Rate',
                renderer: renderValue,
                editor: {
                    xtype: 'textfield',
                    maxLength:75,
   	                enforceMaxLength:true,
                }
            },{
				text : 'Location',
				dataIndex : 'location',
				width :  80,
				renderer: renderValue,
				editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    displayField: 'name',
        		    valueField: 'value',
                    store: ds.candidateLocationStore,
                    forceSelection : true,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                }
			},{
		        xtype: 'gridcolumn',
		        dataIndex: 'cityAndState',
		        text: 'City & State',
		        renderer: renderValue,
		        editor: {
	                xtype: 'textfield',
                    maxLength:50,
   	                enforceMaxLength:true,
	            }
		    },{
				header : 'Contact Person',
				dataIndex : 'contactId',
	            width :  120,
	            editor: me.contactsCombo,
                renderer:function(value, metadata, record){
                	if (value != null && value != '') {
    					var ids = value.toString().split(',');
    					var names ="";
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
    						if (rec)
    							names += rec.data.fullName +", ";	
    					}
    					metadata.tdAttr = 'data-qtip="' + names.substring(0,names.length-2) + '"';
    					return names.substring(0,names.length-2);
                	}
                	return 'N/A';
                }
			},{
				text : 'Roles and Responsibilities',
				dataIndex : 'requirement',
				width :  150,
		        editor: {
	                xtype: 'textarea',
	                height : 60,
                    maxLength:4000,
   	                enforceMaxLength:true,
	            },
	            renderer: requirementRenderer
			},{
                xtype: 'gridcolumn',
                dataIndex: 'skillSet',
                text: 'Skill Set',
                width:150,
                renderer: reqSkillsetRender
            },{
				header : 'Candidate',
				dataIndex : 'candidateId',
	            width :  120,
                renderer:function(value, metadata, record){
                	if (value != null && value != '') {
    					var ids = value.toString().split(',');
    					var names ="",tooltip='';
    					for ( var i = 0; i < ids.length; i++) {
    						var rec = ds.contractorsStore.getById(parseInt(ids[i]));
    						if (rec){
    							names += rec.data.fullName+", ";	
    							tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
    						}
    					}
    					metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
    					return '('+ids.length+') '+ names.substring(0,names.length-2);
					}
                	return 'N/A';
                }
			},{
				xtype: 'gridcolumn',
				text : 'Submitted Date',
				dataIndex : 'submittedDate',
				width :  60,
                renderer:function(value, metadata, record){
                	if (value != null && value != "") {
                		var req_submittedDate = value.split(',');
                		var dates = new Array();
                		for ( var i = 0; i < req_submittedDate.length; i++) {
                			var id_dates = req_submittedDate[i].split('-');
       						var rec = ds.contractorsStore.getById(parseInt(id_dates[0]));
       						if (rec){
       							var letters = rec.data.fullName.match(/\b(\w)/g);
       							var joinName = letters.join('');
       							dates.push(id_dates[1]+'-'+joinName);
       						}
       								
    					}
                		var tooltip = dates.toString();
                		tooltip = tooltip.replace(/,/g, "<br>")
    					metadata.tdAttr = 'data-qtip="' + tooltip + '"';
    					return dates.toString();
					}
                	return value;
                }
			},{
				header : 'Recruiter',
				dataIndex : 'recruiter',
	            width :  100,
		        editor: {
	                xtype: 'combobox',
	                forceSelection:true,
	                queryMode: 'local',
	                triggerAction: 'all',
	                displayField: 'fullName',
	                valueField: 'id',
	                emptyText:'Select...',
	                listClass: 'x-combo-list-small',
	                store: ds.recruiterStore,
	                tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item"><b>{fullName}</b></div>','</tpl>'),
	            },
	            renderer:function(value, metadata, record){
                	var rec = ds.recruiterStore.getById(value);
                	if(rec){
                		metadata.tdAttr = 'data-qtip="' + rec.data.fullName + '"';
                		return rec.data.fullName;
                	}else{
                		metadata.tdAttr = 'data-qtip="' + value + '"';
                		return value;
                	}
                }
			},{
    			text : 'Resume',
    			dataIndex : 'resume',
    			width :  60,
                editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    forceSelection:true,
                    displayField: 'value',
        		    valueField: 'name',
                    store: ds.jobResumeStore,
                    lazyRender: true,
                    listClass: 'x-combo-list-small',
                    tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item"><b>{value}</b></div>','</tpl>'),
                },
                renderer:function(value, metadata, record){
                	if (value != null) {
    					metadata.tdAttr = 'data-qtip="' + value + '%"';
    					return value+'%';
					}
                	return 'N/A';
                }
    		},{
				text : 'Experience Required',
				dataIndex : 'experience',
				width :  70,
				editor: {
                    xtype: 'combobox',
                    queryMode: 'local',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    displayField: 'name',
        		    valueField: 'value',
                    store: ds.experienceStore,
                    forceSelection : true,
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                },
                renderer:function(value, metadata, record){
                	if (value != null) {
                		metadata.tdAttr = 'data-qtip="' + value + '"';
    					return value;
					}
                	return 'N/A';
                }
			},{
				text : 'Employer',
				dataIndex : 'employer',
				width :  40,
				renderer: employerRenderer,
		        editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                forceSelection:true,
	                displayField: 'value',
	                valueField: 'name',
	                emptyText:'Select...',
	    		    store : ds.employerStore,
	                lazyRender: true,
	                listClass: 'x-combo-list-small',
                    tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item"><b>{value}</b></div>','</tpl>'),
	            }
			},{
                xtype: 'gridcolumn',
                dataIndex: 'comments',
                text: 'Comments',
                renderer: renderValue,
                width:150,
                editor: {
                    xtype: 'textarea',
                    height : 60,
                    maxLength:1200,
   	                enforceMaxLength:true,
                }
            },{
				text : 'Communication Required',
				dataIndex : 'communication',
				width :  80,
                renderer: renderValue,
				editor: {
	                xtype: 'combobox',
	                queryMode: 'local',
	                typeAhead: true,
	                triggerAction: 'all',
	                selectOnTab: true,
	                forceSelection:true,
	                displayField: 'value',
	                valueField: 'name',
	                emptyText:'Select...',
	    		    store : ds.communicationStore,
	                lazyRender: true,
	                listClass: 'x-combo-list-small',
                    tpl: Ext.create('Ext.XTemplate','<tpl for=".">','<div class="x-boundlist-item"><b>{value}</b></div>','</tpl>'),
	            },
			},{
				dataIndex: 'remote',
				text: 'Remote',
				editor: {
					xtype: 'combobox',
					queryMode: 'local',
					typeAhead: true,
					forceSelection : true,
					triggerAction: 'all',
					selectOnTab: true,
					displayField: 'name',
					valueField: 'value',
					emptyText:'Select...',
					store : ds.boolStore,
					lazyRender: true,
					listClass: 'x-combo-list-small'
				},
				renderer:boolRenderer
			},{
				dataIndex: 'relocate',
				text: 'Relocate',
				editor: {
					xtype: 'combobox',
					queryMode: 'local',
					typeAhead: true,
					forceSelection : true,
					triggerAction: 'all',
					selectOnTab: true,
					displayField: 'name',
					valueField: 'value',
					emptyText:'Select...',
					store : ds.boolStore,
					lazyRender: true,
					listClass: 'x-combo-list-small'
				},
				renderer:boolRenderer
			},{
				dataIndex: 'travel',
				text: 'Travel',
				editor: {
					xtype: 'combobox',
					queryMode: 'local',
					typeAhead: true,
					forceSelection : true,
					triggerAction: 'all',
					selectOnTab: true,
					displayField: 'name',
					valueField: 'value',
					emptyText:'Select...',
					store : ds.boolStore,
					lazyRender: true,
					listClass: 'x-combo-list-small'
				},
				renderer:boolRenderer
			},{
				dataIndex: 'tAndEPaid',
				text: 'T and E Paid',
				editor: {
					xtype: 'combobox',
					queryMode: 'local',
					typeAhead: true,
					forceSelection : true,
					triggerAction: 'all',
					selectOnTab: true,
					displayField: 'name',
					valueField: 'value',
					emptyText:'Select...',
					store : ds.boolStore,
					lazyRender: true,
					listClass: 'x-combo-list-small'
				},
				renderer:boolRenderer
			},{
				dataIndex: 'tAndENotPaid',
				text: 'T and E not paid',
				editor: {
					xtype: 'combobox',
					queryMode: 'local',
					typeAhead: true,
					forceSelection : true,
					triggerAction: 'all',
					selectOnTab: true,
					displayField: 'name',
					valueField: 'value',
					emptyText:'Select...',
					store : ds.boolStore,
					lazyRender: true,
					listClass: 'x-combo-list-small'
				},
				renderer:boolRenderer
			},{
                xtype: 'gridcolumn',
                dataIndex: 'certifications',
                text: 'Certifications',
                width:120,
                renderer: renderValue,
            },{
                xtype: 'gridcolumn',
                dataIndex: 'targetRoles',
                text: 'Target Roles',
                width:120,
                renderer: renderValue,
            },{
  				text : 'Public Link',
  				dataIndex : 'publicLink',
  				renderer: linkRenderer,
  				width :  80,
  				editor: {
  	                xtype: 'textfield',
  	                maxLength:200,
 	                enforceMaxLength:true,
 	                vtype:'url',
  				},
  			},{
				text : 'Source',
				dataIndex : 'source',
				renderer: renderValue,
		        editor: {
	                xtype: 'textfield',
                    maxLength:45,
   	                enforceMaxLength:true,
	            }
			},{
				text : 'Last Updated User',
				dataIndex : 'lastUpdatedUser',
				width:80,
				renderer:function(value, metadata, record){
               		metadata.tdAttr = 'data-qtip="' + value + '"';
                	return value;
                }
			},{
                xtype: 'datecolumn',
                dataIndex: 'created',
                text: 'Created',
                format: "m/d/Y H:i:s A",
                width:100,
            },{
                xtype: 'datecolumn',
                dataIndex: 'lastUpdated',
                format: "m/d/Y H:i:s A",
                text: 'Last Updated',
                width:100,
            }
        ];
        
        me.viewConfig = {
        		stripeRows: false,
            	getRowClass: function (record, rowIndex, rowParams, store) {
					return 'row-font12';
            	}
        };

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
		    listeners: {
                render: function(c) {
                    new Ext.ToolTip({
                        target: c.getEl(),
                        maxWidth: 1000,
                        dismissDelay: 60000,
                        html: 'Default : Displaying Job openings with job opening status 01.In-progress, 02.Resume Ready, 03.Submitted, 04.Hard to Fill' 
                    });
                }
            }
        });

        me.dockedItems = [
            {
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
                    text: 'Add New',
                    tooltip: 'Add New',
                    iconCls : 'btn-add',
                    tabIndex:30,
                    handler: function(){
                    	me.addRequirement()
                    }
            	},'-',{
            		xtype: 'button',
            		text: 'Save',
            		tooltip: 'Save',
            		iconCls : 'btn-save',
            		hidden : true,
            		tabIndex:31,
            		handler: function(){
            			me.saveRequirements()
            		}
            	},{
                    xtype: 'button',
                    text: 'Delete',
                    tooltip: 'Delete',
                    iconCls : 'btn-delete',
                    tabIndex:32,
                    handler: function(){
                		me.deleteConfirm();
                	}
                },'-',{
            		xtype:'button',
                  	itemId: 'grid-excel-button',
                  	iconCls : 'btn-report-excel',
                  	text: 'Export to Excel',
                  	tooltip: 'Export to Excel',
                  	tabIndex:33,
                  	handler: function(){
                  		me.getExcelExport();
                  	}
            	},'-',{
            		xtype: 'button',
                    text: 'Add Client',
                    tooltip: 'Add Client',
                    iconCls : 'btn-add',
                    tabIndex:34,
                    handler: function(){
                    	me.addClient('Client');
                    }
            	},'-',{
            		xtype: 'button',
                    text: 'Add Vendor',
                    tooltip: 'Add Vendor',
                    iconCls : 'btn-add',
                    tabIndex:35,
                    handler: function(){
                    	me.addClient('Vendor');
                    }
            	},'-',{
            		xtype: 'button',
                    text: 'Add Candidate',
                    tooltip: 'Add Candidate',
                    iconCls : 'btn-add',
                    tabIndex:36,
                    handler: function(){
                    	me.addCandidate();
                    }
            	},'-',{
            		xtype: 'button',
                    text: 'Link Candidates',
                    tooltip: 'Link Candidates',
                    iconCls : 'btn-link',
                    tabIndex:37,
                    handler: function(){
                    	me.linkCandidate();
                    }
            	},'-',{
            		xtype: 'button',
                    text: 'Add Interview',
                    tooltip: 'Add Interview',
                    iconCls : 'btn-add',
                    tabIndex:38,
                    handler: function(){
                    	me.addInterview();
                    }
            	},'-',{
            		xtype: 'button',
                    text: 'Add Contact',
                    tooltip: 'Add Contact',
                    iconCls : 'btn-add',
                    tabIndex:39,
                    handler: function(){
                    	me.addCantact();
                    }
            	},'-',{
        			text : 'Legend',
        			tooltip: 'Legend',
         			iconCls : 'icon_info',
         			scope: this,
         			tabIndex:40,
         			handler: function(){
         		        var legendWin = Ext.create("Ext.window.Window", {
         					title: 'Legend',
         					modal: true,
         					height      : 400,
         					width       : 550,
         					bodyStyle   : 'padding: 1px;background:#ffffff;',
         					html: '<table cellspacing="10" cellpadding="5" noOfCols="2" width="550"> '
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFCBC;"></td>'
         						+ '<td width="50%">Reqs which are opened today</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FAFAD0;"></td>'
         						+ '<td width="50%">Reqs which are opened since yesterday</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFFE0;"></td>'
         						+ '<td width="50%">Reqs which are opened since a week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFFFEE;"></td>'
         						+ '<td width="50%">Reqs that are opened for more than one week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#C3D5F6;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made today</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#CFDCF5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made yesterday</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#D9E3F5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made with in one week</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#E5EBF5;"></td>'
         						+ '<td width="50%">Reqs for which submissions were made more than one week back</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#A7FDA1;"></td>'
         						+ '<td width="50%">Reqs for which  placement was made</td>'
         						+ '</tr>'
         						+ '<tr>'
         						+ '<td width="15%" style="background-color:#FFD3A7;"></td>'
         						+ '<td width="50%">Reqs which reached atleast interview stage</td>'
         						+ '</tr>'
         						+ '</table>'
         				});

         				legendWin.show();
         		    }
            	},'-',{
        			text : 'Refresh',
        			tooltip : 'Refresh',
         			icon : 'images/arrow_refresh.png',
         			scope: this,
         			tabIndex:41,
         			handler: function(){
         				app.requirementMgmtPanel.loadStores();
         				app.requirementMgmtPanel.requirementsSearchPanel.reset();
         		    }
            	},'->',me.showCount]
          }
      ];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			this.showCount.setValue(msg);
       	}, me);

        ds.contractorsStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.clientSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        ds.contactSearchStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        ds.vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        ds.hotnessStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);
        
        me.callParent(arguments);
        
        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);

    },

/*    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],
*/
    addInterview : function() {
    	var records = this.getSelectionModel().getSelection();
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','Please select a record');
    		return false;
		}else if (records.length > 1) {
    		Ext.Msg.alert('Error','Please select only one record');
    		return false;
		}
    	var record = records[0];
    	
		var requirementId = record.data.id;
    	if (requirementId == null || requirementId == '') {
    		this.updateError("Please save Job opening");
    		return false;
		}
    	this.manager.showInterview();
    	this.manager.interviewDetailPanel.opendFrom='grid';
    	this.manager.interviewDetailPanel.clearMessage();
    	this.manager.interviewDetailPanel.form.reset();
    	this.manager.interviewDetailPanel.form.findField('requirementId').setValue(requirementId);
	
        var vendorStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
        });
        var clientStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
        });
        var candidateStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'fullName'],
        });

        ds.clientSearchStore.clearFilter(true);
        ds.vendorStore.clearFilter(true);
        ds.contractorsStore.clearFilter(true);

        var clientId = record.data.clientId;
        if (clientId != null && clientId != '') {
        	var client = ds.clientSearchStore.getById(clientId);
        	var clientData= new Array();
        	clientData.push({"id":client.data.id,"name":client.data.name});
    		clientStore.add(clientData);
		}else
			clientStore.add(ds.clientSearchStore.data.items);
		
        var vendorIds = record.data.vendorId.split(',');
        vendorIds = vendorIds.map(Number);
        if (vendorIds.length > 0) {
        	var vendorData= new Array();
			for ( var i = 0; i < vendorIds.length; i++) {
				vendorData.push({"id":ds.vendorStore.getById(vendorIds[i]).data.id,"name":ds.vendorStore.getById(vendorIds[i]).data.name});
			}
			vendorStore.add(vendorData);
		}else
			vendorStore.add(ds.vendorStore.data.items);

        var candidateIds = record.data.candidateId;

        if (candidateIds != null && candidateIds != '') {
        	var candidateIds = record.data.candidateId.split(',');
        	var candidateData= new Array();
			for ( var i = 0; i < candidateIds.length; i++) {
				candidateData.push({"id":ds.contractorsStore.getById(Number(candidateIds[i])).data.id,"fullName":ds.contractorsStore.getById(Number(candidateIds[i])).data.fullName});
			}
			candidateStore.add(candidateData);
		}else
			candidateStore.add(ds.contractorsStore.data.items);
        
        this.manager.interviewDetailPanel.vendorCombo.bindStore(vendorStore);
        this.manager.interviewDetailPanel.clientCombo.bindStore(clientStore);	
        this.manager.interviewDetailPanel.candidateCombo.bindStore(candidateStore);
        this.manager.interviewDetailPanel.form.findField('clientId').setValue(record.data.clientId);
        this.manager.interviewDetailPanel.form.findField('requirementId').setReadOnly(true);
        if(vendorStore.getCount() == 1)
        	this.manager.interviewDetailPanel.form.findField('vendorId').setValue(vendorStore.getAt(0).data.id);
        if(candidateStore.getCount() == 1)
        	this.manager.interviewDetailPanel.form.findField('candidateId').setValue(candidateStore.getAt(0).data.id);
	
	},
	
	addCantact : function() {
    	app.setActiveTab(0,'li_contacts');
    	app.contactMgmtPanel.getLayout().setActiveItem(1);
    	app.contactMgmtPanel.contactDetailPanel.form.reset();
   	 	var record = Ext.create( 'tz.model.Contact',{
   	 		id : null
   	 	});
   	 	app.contactMgmtPanel.contactDetailPanel.loadForm(record);
    	app.contactMgmtPanel.contactDetailPanel.opendFrom='grid';
	},
    
    saveCandidates : function() {
    	var records = new Array();
        ds.subRequirementStore.filterBy(function(rec) {
        	return rec.get('deleted') != true;
        });

    	for ( var i = 0; i < ds.subRequirementStore.data.length ; i++) {
			var record = ds.subRequirementStore.getAt(i);
			
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				if (record.data.candidateId == 0 || record.data.candidateId == null){
					Ext.Msg.alert('Error','Please select a candidate');
					return false;
				}
				if (record.data.vendorId == 0 || record.data.vendorId == null){
					Ext.Msg.alert('Error','Please select the vendor for the candidate');
					return false;
				}
				for ( var j = 0; j < ds.subRequirementStore.getCount(); j++) {
					var oldRec = ds.subRequirementStore.getAt(j);
					if (oldRec != record && oldRec.data.vendorId == record.data.vendorId && oldRec.data.candidateId == record.data.candidateId) {
						Ext.Msg.alert('Error','Candidate already submitted through given vendor.');
						return false;
					}
				}
	    		if (record.data.contactId == 0 || record.data.contactId == null)
					delete record.data.contactId;
	    		if (record.data.id == 0 || record.data.id == null)
					delete record.data.id;
				records.push(record.data);
			}
		}
    	ds.subRequirementStore.clearFilter();
    	if (records.length == 0) {
			Ext.Msg.alert('Error','There are no changes in candidates to submit');
		}else{
        	records = Ext.JSON.encode(records);
        	app.requirementService.saveSubRequirements(records,this.onSaveCandidates,this);
		}
	},
	
	onSaveCandidates : function(data) {
		//Ext.getCmp('submittedCandidatesWindow').hide();
		if (data.success) {
			this.modifiedCandidateIds = new Array();
			this.reloadCandidates();
			app.requirementMgmtPanel.requirementsSearchPanel.search();
			Ext.Msg.alert('Success','Candidates Saved Successfully');
		}else {
			Ext.Msg.alert('Success','Save Failed');
			this.body.scrollTo('top', 0);
		}	
	},
    
    linkCandidate : function() {
    	var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
    	if (selectedRecord == null) {
    		Ext.Msg.alert('Error','Please select a Requirement to link.');
    		return false;
		}
    	app.requirementMgmtPanel.requirementDetailPanel.loadSubRequirments(selectedRecord.data.id);

		var vendorStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
        });
		var vendorIds = selectedRecord.data.vendorId.split(',');
		var vendorData= new Array();
		for ( var i = 0; i < vendorIds.length; i++) {
			var vendorObj = ds.vendorStore.getById(parseInt(vendorIds[i]));
			if(vendorObj != null)
				vendorData.push({"id":vendorObj.data.id,"name":vendorObj.data.name});
		}
    	if(vendorData.length >0 )
    		vendorStore.add(vendorData);

        var submittedCandidates = Ext.create('Ext.grid.Panel', {
            title: '',
            frame:false,
            anchor:'100%',
            height:300,
            width:'100%',
            store: ds.subRequirementStore,
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],
            listeners: {
                edit: function(editor, event){
                	var record = event.record;
            		if(event.originalValue != event.value){
            			app.requirementMgmtPanel.requirementsGrid.modifiedCandidateIds.push(record.data.id);
            		}
                },
                beforeedit: function(editor, event) {
              		var record = event.record;
            		if (record.get('deleted')==true )
						return false;
              		return true;
                },
            	itemclick: function(dv, record, item, index, e) {
            		if (record.data.deleted == true)
            			me.submittedCandidates.getSelectionModel().deselectAll();
            	}
            },
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Add Existing Candidate',
            		tooltip: 'Add Existing Candidate',
            		iconCls : 'btn-add',
            		tabIndex : 101,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			var requirementId = selectedRecord.data.id;
            	    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
            	   	 		id : null,
            	   	 		requirementId : requirementId,
            	   	 		submittedDate : new Date()
            	   	 	});
            	   	 	ds.subRequirementStore.insert(0, rec);
            	        this.getView().focusRow(0);
            		} 
            	},'-',{
            		xtype: 'button',
            		text: 'Save',
            		tooltip: 'Save Candidates',
            		iconCls : 'btn-save',
            		tabIndex : 102,
            		scope: this,
            		handler: this.saveCandidates
            	},'-',{
            		xtype: 'button',
            		text: 'Reset',
            		tooltip: 'Reset',
            		tabIndex : 103,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			this.reloadCandidates();
            			
            		} 
            	}]
            }],

            columns: [
                      {
                		xtype : 'actioncolumn',
              			hideable : false,
              			icon : 'images/icon_delete.gif',
              			width : 35,
              			items : [ {
              				padding : 10,
              				icon : 'images/icon_delete.gif',
              				scope : this,
              				handler : function(grid, rowIndex, colIndex) {
                 				this.removeCandidateConfirm(ds.subRequirementStore.getAt(rowIndex));
              				},
              			} ]
              		},{
            			header : 'Candidate',
          				dataIndex : 'candidateId',
          	            width :  150,
          		        editor: {
          	                xtype: 'combobox',
          	                queryMode: 'local',
          	                triggerAction: 'all',
          	                displayField: 'fullName',
          	                valueField: 'id',
          	                emptyText:'Select...',
          	                listClass: 'x-combo-list-small',
          	                store: ds.contractorsStore,
          	                anyMatch:true,
          	            },
                          renderer:function(value, metadata, record){
                          	var candidate = ds.contractorsStore.getById(value);
                          	if(candidate){
                          		return candidate.data.fullName;
                          	}else{
                          		return null;
                          	}
                          }
          			},{
          				text : 'Submitted Date',
          				dataIndex : 'submittedDate',
          				width :  100,
          				renderer: dateRender,
          				editor: {
          	                xtype: 'datefield',
          	                format: 'm/d/y'
          	            }
          			},{
            			header : 'Vendor',
          				dataIndex : 'vendorId',
          	            width :  150,
          		        editor: {
          	                xtype: 'combobox',
          	                queryMode: 'local',
          	                triggerAction: 'all',
          	                displayField: 'name',
          	                valueField: 'id',
          	                emptyText:'Select...',
          	                listClass: 'x-combo-list-small',
          	                store: vendorStore,
          	                anyMatch:true,
          	            },
                          renderer:function(value, metadata, record){
                          	var candidate = ds.vendorStore.getById(value);
                          	if(candidate)
                          		return candidate.data.name;
                          	else
                          		return null;
                          }
          			}
                  ],
                  viewConfig : {
                  	stripeRows:false ,
                  	getRowClass: function (record, rowIndex, rowParams, store) {
                  		return record.get('deleted')==true ? 'gray-row' : '';
                  	}
                  }
        });

        if (Ext.getCmp('submittedCandidatesWindow') == null) {
			var myWin = Ext.create("Ext.window.Window", {
				title: 'Submitted Candidates',
				id : 'submittedCandidatesWindow',
				modal: true,
				height      : 340,
				width       : 600,
				layout      : 'form',
				items       : [submittedCandidates]
			});
		}
		    
        Ext.getCmp('submittedCandidatesWindow').show();
	},

	reloadCandidates : function() {
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord.data.id != null && selectedRecord.data.id != ""){
        	var params = new Array();
        	params.push(['requirementId','=', selectedRecord.data.id]);
        	params.push(['table','=', "submissions"]);
        	var filter = getFilter(params);
        	ds.subRequirementStore.loadByCriteria(filter);
    	}else{
    		ds.subRequirementStore.removeAll();
    	}
	},
	
	removeCandidate : function(dat) {
		if (dat=='yes') {
			var record= this.removeRecord;
			app.requirementService.removeCandidate(record.data.id,this.onRemoveCandidate,this);
		}
	},

	removeCandidateConfirm : function(record) {
		this.removeRecord = record;
		if (record.data.id == 0 || record.data.id == null)
			ds.subRequirementStore.remove(record);
		else
			Ext.Msg.confirm("Confirm.", "Do you want to remove selected candidate from the Job Opening?", this.removeCandidate, this);
	},
	
	onRemoveCandidate : function(data) {
		if (data.success) {
	    	var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
	    	app.requirementMgmtPanel.requirementDetailPanel.loadSubRequirments(selectedRecord.data.id);
		}else {
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
	},

    getExcelExport : function() {
    	var values = app.requirementMgmtPanel.requirementsSearchPanel.getValues();
    	var params = new Array();

    	params.push(['candidateId','=', values.candidate]);
    	params.push(['search','=', values.searchRequirements]);
    	params.push(['jobOpeningStatus','=', values.jobOpeningStatus]);
    	params.push(['module','=', values.module]);

    	var hotness = values.hotJobs.toString();
    	if (hotness != '')
    		hotness = "'"+hotness.replace(/,/g, "','")+"'";
    	params.push(['hotness','=', hotness]);

    	var openings = values.jobOpeningStatus.toString();
    	if (openings != '')
    		openings = "'"+openings.replace(/,/g, "','")+"'";
    	params.push(['jobOpeningStatus','=', openings]);
    	if (values.submittedDateFrom != undefined && values.submittedDateFrom != '') {
    		var submittedDateFrom = new Date(values.submittedDateFrom);
        	params.push(['submittedDateFrom','=',submittedDateFrom]);
    	}

    	if (values.submittedDateTo != undefined && values.submittedDateTo != '') {
    		var submittedDateTo = new Date(values.submittedDateTo);
        	params.push(['submittedDateTo','=',submittedDateTo]);
    	}
    	params.push(['module','=', values.module]);
    	
    	var id = values.id;
    	id = id.replace(',,',",");
    	while (id.length >0) {
        	if (id.substr(id.length-1,id.length) == ",")
        		id = id.substr(0,id.length-1);
			else
				break;
		}
    	if (id.search(',') != -1) 
    		id = "'"+id.replace(/,/g, "','")+"'";

    	params.push(['id','=', id]);
    	var columns = new Array();
    	for ( var i = 0; i < this.columns.length; i++) {
			if (this.columns[i].xtype != 'actioncolumn' && this.columns[i].text != "&#160;" && this.columns[i].text != '' && !this.columns[i].hidden ) {
				columns.push(this.columns[i].text + ':'+this.columns[i].width);
			}
		}
    	params.push(['columns','=', columns.toString()]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	app.requirementService.excelExport(Ext.encode(filter));
	},
	
    addRequirement : function() {
    	app.requirementService.getRequirementNo('',this.onGetRequirementNo,this);
   	 	
   	 	//ds.requirementStore.insert(0, rec);
        //this.getView().focusRow(0);
	},
    
	onGetRequirementNo : function(data) {
		if (data.success) {

		    d = new Date();
		    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
		    nd = new Date(utc + (3600000*-7));
	   	 	var rec = Ext.create( 'tz.model.Requirement',{
	   	 		id : null,
	   	 		employer : 'CPS',
	   	 		communication : 'Average',
	   	 		helpCandidate : '100-Yes',
	   	 		remote : false,
	   	 		//relocate : true,
	   	 		//travel : true,
	   	 		version : 0,
	   	 		requirementNo : Number(data.successMessage),
	   	 		created : nd,
	   	 		lastUpdated :nd
	   	 	});

	   	 	app.requirementMgmtPanel.getLayout().setActiveItem(1);
	   	 	app.requirementMgmtPanel.requirementDetailPanel.loadForm(rec);
	   	 	app.requirementMgmtPanel.requirementDetailPanel.openedFrom = 'Requirement';
		}
	},
	
    saveRequirements : function() {
    	var records = new Array();
    	for ( var i = 0; i < ds.requirementStore.data.length ; i++) {
			var record = ds.requirementStore.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}else
    			delete record.data.vendorId;
    		if (record.data.vendorId == '' || record.data.vendorId == null || record.data.vendorId == 0 ) 
				delete record.data.vendorId;
    		if (record.data.clientId == '' || record.data.clientId == null || record.data.clientId == 0 ) 
				delete record.data.clientId;
    		if (record.data.candidateId == 0 || record.data.candidateId == null)
				delete record.data.candidateId;
    		if (record.data.contactId == 0 || record.data.contactId == null)
				delete record.data.contactId;
    		if (record.data.resume == null )
				delete record.data.resume;
    		if (record.data.remote == null )
				delete record.data.remote;
    		if (record.data.relocate == null )
				delete record.data.relocate;
    		if (record.data.travel == null )
				delete record.data.travel;
    		if (record.data.tAndEPaid == null )
				delete record.data.tAndEPaid;
    		if (record.data.tAndENotPaid == null )
				delete record.data.tAndENotPaid;
    		if (record.data.recruiter == null )
				delete record.data.recruiter;
    		
    		if (this.modifiedIds.indexOf(record.data.id) != -1) {
    			records.push(record.data);
			}
		}

    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Requirements to save');
    		return;
		}

    	records = Ext.JSON.encode(records);
    	app.requirementService.saveRequirements(records,this.onSave,this);

	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error',data.errorMessage);
		this.modifiedIds =  new Array();
		app.requirementMgmtPanel.requirementsSearchPanel.search();
	},
	
	deleteConfirm : function()
	{
		var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
		if (selectedRecord == undefined) {
			Ext.Msg.alert('Error','Please select a Requirement to delete.');
		} else if (selectedRecord.data.id == null){
			this.store.remove(selectedRecord);
		} else{
			Ext.Msg.confirm("Delete Requirement","Do you to delete selected Requirement?", this.deleteRequirement, this);
		}
	},
	
	deleteRequirement: function(dat){
		if(dat=='yes'){
			var selectedRecord = this.getView().getSelectionModel().getSelection()[0];
			app.requirementService.deleteRequirement(selectedRecord.data.id, this.onDeleteRequirement, this);
		}
	},
	
	onDeleteRequirement : function(data){
		if (!data.success) { 
			Ext.Msg.alert('Error',data.errorMessage);
		}else{
			app.requirementMgmtPanel.requirementsSearchPanel.search();
			Ext.Msg.alert('Success','Deleted Successfully');
		}	
	},

	addClient : function(type) {
		if(app.clientPanel == null){
			app.clientPanel  = new tz.ui.ClientPanel({itemId:'clientPanel'});
			app.centerLayout.add(app.clientPanel);
		}
		app.centerLayout.getLayout().setActiveItem('clientPanel');
		app.clientPanel.deleteButton.disable();
		app.clientPanel.saveAndCloseButton.show();
		app.clientPanel.saveButton.hide();

	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
	    if (type == 'Vendor') {
			var helpCandidate = '100-Yes';
			app.clientPanel.helpCandidateCombo.show();
	    }else{
	    	var helpCandidate = '';
	    	app.clientPanel.helpCandidateCombo.hide();
	    }
	    
   	 	var rec = Ext.create( 'tz.model.Client',{
   	 		id : null,
   	 		type : type,
   	 		helpCandidate : helpCandidate,
   	 		comments :'N/A',
   	 		email :'',
   	 		website :'N/A',
   	 		contactNumber :'N/A',
   	 		fax :'N/A',
   	 		industry :'N/A',
   	 		about :'N/A',
   	 		address :'N/A',
   	 		cityAndState :'N/A',
   	 		lastUpdatedUser :null,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});
   	 	app.clientPanel.loadForm(rec);
		app.clientPanel.openedFrom = "Recruit";
	},

	addCandidate : function(type) {
		app.setActiveTab(0,'li_candidates');
		app.candidateMgmtPanel.getLayout().setActiveItem(1);
		app.candidateMgmtPanel.candidatePanel.clearMessage();
		app.candidateMgmtPanel.candidatePanel.form.reset();
		app.candidateMgmtPanel.candidatePanel.deleteButton.disable();
		app.candidateMgmtPanel.candidatePanel.openedFrom = "Recruit";
		app.candidateMgmtPanel.candidatePanel.saveAndCloseButton.show();
		app.candidateMgmtPanel.candidatePanel.saveButton.hide();

	    d = new Date();
	    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	    nd = new Date(utc + (3600000*-7));
   	 	var rec = Ext.create( 'tz.model.Candidate',{
   	 		id : null,
   	 		experience : 0,
   	 		relocate : true,
   	 		travel : true,
   	 		followUp : null,
   	 		confidentiality : 5,
   	 		version : 0,
   	 		created : nd,
   	 		lastUpdated :nd
   	 	});
   	 	
   	 	app.candidateMgmtPanel.candidatePanel.loadForm(rec);
   	 	app.candidateMgmtPanel.candidatePanel.openedFrom = "Job Opening";
	},

    getRecruitmentTab : function() {
		if(app.requirementMgmtPanel == null){
			app.requirementMgmtPanel = new tz.ui.RequirementMgmtPanel({itemId:'requirementMgmtPanel'});
			app.centerLayout.add(this.requirementMgmtPanel);
		}    		
		app.centerLayout.getLayout().setActiveItem('requirementMgmtPanel');
		
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.candidateSearchStore.loadByCriteria(filter);

    	params = new Array();
    	params.push(['type','=', 'Client']);
    	filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.clientSearchStore.loadByCriteria(filter);

    	params = new Array();
    	params.push(['type','=', 'Vendor']);
    	filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.vendorStore.loadByCriteria(filter);
		
	},
	
	shareRequirement : function(record) {
		var record = record.data;
		this.manager.requirementSharePanel.data = record;
		if (record.clientId != null && record.clientId != '') {
			var client = ds.clientSearchStore.getById(Number(record.clientId)).data.name;
		}else
			var client = '';
		
		var ids = record.vendorId.split(',').map(Number);
		var names ="";
		for ( var i = 0; i < ids.length; i++) {
			var rec = ds.vendorStore.getById(parseInt(ids[i]));
			if (rec)
				names += rec.data.name +", ";	
		}
		var vendor = names.substring(0,names.length-2);
		
		var detailsTable = "<table border='1' cellspacing='0' style='border-collapse:collapse;' cellpadding='5'>"+
							"<tr><th>Posting Date </th><td>"+Ext.Date.format(new Date(record.postingDate),'m/d/Y')+'</td></tr>'+
							"<tr><th>Posting Title</th><td>" +record.postingTitle+'</td></tr>'+
							"<tr><th>City & State</th><td>" +record.cityAndState+'</td></tr>'+
							"<tr><th>Client</th><td>" +client+'</td></tr>'+
							"<tr><th>Vendor</th><td>" +vendor+'</td></tr>'+
							"<tr><th>Alert</th><td>" +record.addInfo_Todos+'</td></tr>'+
							"<tr><th>Source</th><td>" +record.source+'</td></tr>'+
							"<tr><th>Module</th><td>" +record.module+'</td></tr>'+
							"<tr><th>Skill Set</th><td>" +record.skillSet+'</td></tr>'+
							"<tr><th>Role Set</th><td>" +record.targetRoles+'</td></tr>'+
							"<tr><th>Mandatory Certifications</th><td>" +record.certifications +'</td></tr>';

		if(record.remote)
			detailsTable += "<tr><th>Remote</th><td>Yes</td></tr>";
		else
			detailsTable += "<tr><th>Remote</th><td>No</td></tr>" ;
		
		if(record.relocate)
			detailsTable += "<tr><th>Relocation Needed</th><td>Yes</td></tr>" ;
		else
			detailsTable += "<tr><th>Relocation Needed</th><td>No</td></tr>" ;
		
		if(record.travel)
			detailsTable += "<tr><th>Travel Needed</th><td>Yes</td></tr>" ;
		else
			detailsTable += "<tr><th>Travel Needed</th><td>No</td></tr>" ;
		
		detailsTable += "<tr><th>Ok to train Candidate</th><td>" + record.helpCandidate +'</td></tr>'+
						"<tr><th>Roles and Responsibilities</th><td><br>" +record.requirement.replace(/(?:\r\n|\r|\n)/g, '<br>')+'</td></tr>'+
						"<tr><th>Last updated by</th><td><br>" +record.lastUpdatedUser+'</td></tr>'+
						"<tr><th>Last updated on</th><td><br>" +Ext.Date.format(new Date(record.lastUpdated),'m/d/Y')+'</td></tr>'+
						'</table>';

		
		this.manager.showSharePanel();
		this.manager.requirementSharePanel.criteriaPanel.form.findField('jobOpeningDetails').setValue(detailsTable);
		this.manager.requirementSharePanel.emailShortlisted.disable();
		this.manager.requirementSharePanel.emailConfidential.enable();
		this.manager.requirementSharePanel.fileName = record.id;
		this.manager.requirementSharePanel.openedFrom = 'Requirement Grid';
	}
	
});

function boolRenderer(value, metadata, record, rowIndex, colIndex, store) {
	if(value == true){
	    metadata.tdAttr = 'data-qtip="' + 'YES' + '"';
	    return 'YES';
	}else if (value == false) {
	    metadata.tdAttr = 'data-qtip="' + 'NO' + '"';
	    return 'NO';
	}
	return 'N/A';
}

function requirementRenderer(value, metadata, record, rowIndex, colIndex, store) {
	if(value != null){
	    metadata.tdAttr = 'data-qtip="' + value.replace(/(?:\r\n|\r|\n)/g, '<br />') + '"';	
	}
	return value;
}

function renderValue(value, metadata, record, rowIndex, colIndex, store) {
    if (value == '') {
        metadata.tdAttr = 'data-qtip="' + value + '"';	
    	return 'N/A';
	}
    if (value != null ) {
    	metadata.tdAttr = 'data-qtip="' + value.toString().replace(/(?:\r\n|\r|\n)/g, '<br />') + '"';
    	return value;
    }
    metadata.tdAttr = 'data-qtip="' + value + '"';	
	return 'N/A';
}

function employerRenderer(value, metadata, record, rowIndex, colIndex, store) {
	metadata.tdAttr = 'data-qtip="' + value + '"';
	if (value == "TCS") {
		return '<span style="color:green;">' + value + '</span>';
	}
	if (value == "CPS") {
        return '<span style="color:blue;">' + value + '</span>';
    }  
    return 'N/A';
}

function alertRender(value, metadata, record, rowIndex, colIndex, store) {
	metadata.style = 'background-color:#'+getBackGround(record)+';'
    metadata.tdAttr = 'data-qtip="' + value + '"';
    if (value == null || value=="") 
    	return 'N/A' ;
    return '<span style="color:red;">' + value + '</span>';
}

function comboRender(value, metadata, record, rowIndex, colIndex, store) {
	metadata.style = "background-color:#FFFFEB;";
    metadata.tdAttr = 'data-qtip="' + value + '"';
    return value;
}

function createHeaderMenu(menu,header) {
    menu.add([{
        text: 'Show All Columns',
        handler: function () {
        	app.loadMask.show();
            var columnDataIndex = menu.activeHeader.dataIndex;
            Ext.each(header, function (column) {
                column.show();
            });
            app.loadMask.hide();                
        }
    }]);
    menu.add([{
        text: 'Hide All Columns Except This',
        handler: function () {
        	app.loadMask.show();
            var columnDataIndex = menu.activeHeader.dataIndex;
            //alert(columnDataIndex);
            Ext.each(header, function (column) {
            	
                if (column.dataIndex != columnDataIndex) {
                    column.hide();
                }
            });
            app.loadMask.hide();
        }
    }]);
}

function getBackGround(record) {
	//placements green(A7FDA1), Interviews orange(FFD3A7)
	if (record.data.sub_Requirements.length > 0) {
		for ( var i = 0; i < record.data.sub_Requirements.length; i++) {
			var subReq = record.data.sub_Requirements[i];
			if (subReq.submissionStatus == 'C-Accepted')
				return 'A7FDA1'
		}
		for ( var i = 0; i < record.data.sub_Requirements.length; i++) {
			var subReq = record.data.sub_Requirements[i];
			if (subReq.submissionStatus == 'I-Scheduled' || subReq.submissionStatus == 'I-Succeeded' || subReq.submissionStatus == 'I-Failed' 
				|| subReq.submissionStatus == 'C-Accepted' || subReq.submissionStatus == 'C-Declined' || subReq.submissionStatus == 'I-Withdrawn')
				return 'FFD3A7'
		}
	}
	//submissions in blue
	if (record.data.submittedDate != null && record.data.submittedDate != '') {
		var req_submittedDate = record.data.submittedDate.split(',');
		//today
		for ( var i = 0; i < req_submittedDate.length; i++) {
			var id_dates = req_submittedDate[i].split('-');
				if (Ext.Date.format(new Date(id_dates[1]),'m/d/Y') == Ext.Date.format(new Date(),'m/d/Y')) 
					return 'C3D5F6';	
		}
		//yesterday
		for ( var i = 0; i < req_submittedDate.length; i++) {
			var id_dates = req_submittedDate[i].split('-');
				if (Ext.Date.format(new Date(id_dates[1]),'m/d/Y') == Ext.Date.format(new Date(new Date().setDate(new Date().getDate() - 1)),'m/d/Y')) 
					return 'CFDCF5';	
		}
		//last 7 days
		for ( var i = 0; i < req_submittedDate.length; i++) {
			var id_dates = req_submittedDate[i].split('-');
				if (new Date(id_dates[1]).getTime() >= (new Date()).setDate((new Date()).getDate()-7)) 
					return 'D9E3F5';	
		}
		//last month
		for ( var i = 0; i < req_submittedDate.length; i++) {
			var id_dates = req_submittedDate[i].split('-');
				if (new Date(id_dates[1]).getTime() >= (new Date()).setMonth((new Date()).getMonth()-1)) 
					return 'E5EBF5';	
		}
	}
	//Postings in yellow
	if (record.data.postingDate != null) {
		if (Ext.Date.format(new Date(),'m/d/y') == Ext.Date.format(record.data.postingDate,'m/d/y')) {
			return 'FFFCBC';
		}else if (Ext.Date.format(new Date((new Date()).setDate((new Date()).getDate()-1)),'m/d/y') == Ext.Date.format(record.data.postingDate,'m/d/y')) {
			return 'FAFAD0';
		}else if (record.data.postingDate.getTime() > (new Date()).setDate((new Date()).getDate()-7)) {
			return 'FFFFE0';
		}else if (record.data.postingDate.getTime() > (new Date()).setMonth((new Date()).getMonth()-1)) {
			return 'FFFFEE';
		}
	}
	return null;
}

function reqSkillsetRender(value, metadata, record, rowIndex, colIndex, store) {
	if (record.data.skillSetIds != null && record.data.skillSetIds != '') {
		var skillSetIds = record.data.skillSetIds.split(',');
		var skillSet ='';
		for ( var i = 0; i < skillSetIds.length; i++) {
			skillSet += ds.skillSetStore.getById(Number(skillSetIds[i])).data.value;
			if (skillSetIds.length > (i+1))
				skillSet += ",";
		}
		metadata.tdAttr = 'data-qtip="' + skillSet + '"';
		return skillSet;
	}
	return 'N/A';
}
