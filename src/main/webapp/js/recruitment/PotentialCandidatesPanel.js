Ext.define('tz.ui.PotentialCandidatesPanel', {
    extend: 'tz.ui.BaseFormPanel',
    
    bodyPadding: 0,
    title: '',
    anchor:'100% 100%',
    autoScroll:true,
    fieldDefaults: { msgTarget: 'side',labelAlign: 'left',labelWidth :100},
    
    initComponent: function() {
        var me = this;
        me.requirement = '';
        me.removeRecord = "";
        me.modifiedCandidateIds = new Array();
        me.modifiedResearchIds = new Array();
        me.modifiedVendorIds = new Array();
        me.verifyCandidateIds = new Array();
        me.modifiedShortlistIds = new Array();
        me.emailFilter ;
        me.CAcceptedIds = new Array();
        me.ISucceededIds = new Array();
        
        var vendorStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'name'],
        });

		me.addCandidateButton = new Ext.Button({
    		text: 'Add',
    		tooltip: 'Add Existing Candidate',
    		iconCls : 'btn-add',
    		scope: this,
    		handler: this.addCandidate
        });

        me.vendorCombo = Ext.create('Ext.form.ComboBox', {
        	displayField: 'name',
            valueField: 'id',
            listClass: 'x-combo-list-small',
            store: vendorStore,
		    queryMode: 'local',
		    typeAhead: true,
		    anyMatch:true,
		});
		
        me.submittedCandidates = Ext.create('Ext.grid.Panel', {
            title: 'Submitted Candidates',
            cls : 'submitted-green',
            frame:false,
            anchor:'98%',
            height:200,
            width:'100%',
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],
            listeners: {
                edit: function(editor, event){
                	var record = event.record;
            		if(event.originalValue != event.value){
            			me.modifiedCandidateIds.push(record.data.id);
            			me.verifyCandidateIds.push(record.data.candidateId);
            		}
            		if (event.column.dataIndex == "candidateId" && record.data.candidateId != null && record.data.candidateId != '' ) {
            			var candidate  = ds.candidateSearchStore.getById(record.data.candidateId);
            			if (candidate != null) {
            				record.data.candidate = candidate.data.fullName;
            				record.data.candidateExpectedRate = candidate.data.expectedRate;
            				record.data.cityAndState = candidate.data.cityAndState;
            				record.data.relocate = candidate.data.relocate;
            				record.data.travel = candidate.data.travel;
            				record.data.availability = candidate.data.availability;
            				me.submittedCandidates.getView().refresh();	
            			}
					}else if (editor.context.field == 'submittedTime') {
		    			var time = record.data.submittedTime;
		    			if (time != null && time.toString().length > 8) {
		    			    record.data.submittedTime = timeToString(time);
		    			    this.getView().refresh();
		    			}
		    		}else if (editor.context.field == 'submissionStatus' && record.data.id != null && event.originalValue != event.value 
		    				&& event.value == 'C-Accepted' && record.data.projectInserted != true) {
						me.CAcceptedIds.push(record.data.id);
					}else if (editor.context.field == 'submissionStatus' && record.data.id != null && event.originalValue != event.value && event.value == 'I-Succeeded') {
						me.ISucceededIds.push(record.data.id);
					}
                },
                beforeedit: function(editor, event) {
              		var record = event.record;
              		if (event.column.dataIndex == "candidateId" && record.data.candidateId != null && record.data.candidateId != '' ) {
              			var candidate  = ds.candidateSearchStore.getById(record.data.candidateId);
              			if (candidate == null)
							return false;
              		}
              		return true;
                }
            },
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [me.addCandidateButton,'-',{
            		xtype: 'button',
            		text: 'Save',
            		iconCls : 'btn-save',
            		scope: this,
            		handler: this.saveCandidates
            	},'-',{
		    		xtype:'button',
		    		iconCls : 'btn-add',
	                text: 'Add Guide',
	                tooltip:'Add Guide',
		    		handler: function(){
		    			me.addGuide('Submitted');
		    		}
		    	},'-',{
		    		xtype:'button',
		    		iconCls : 'btn-add',
	                text: 'Add Reference',
	                tooltip:'Add Reference',
		    		handler: function(){
		    			me.addReference();
		    		}
		    	},'-',{
            		xtype: 'button',
            		text: 'Reset',
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            				me.modifiedCandidateIds = new Array();
        					me.loadSubRequirments(me.requirement.data.id);
        				} 
            	}]
            }],

            columns: [{
            	xtype : 'actioncolumn',
            	hideable : false,
            	width : 30,
            	items : [ {
            		icon : 'images/icon_delete.gif',
            		tooltip : 'Remove Candidate from Jop Opening',
            		padding : 10,
            		scope : this,
            		handler : function(grid, rowIndex, colIndex) {
            			this.removeCandidateConfirm(me.submittedCandidates.store.getAt(rowIndex));
            		} 
            	} ]
            },{
            	xtype : 'actioncolumn',
            	hideable : false,
            	width : 30,
            	items : [{
            		icon : 'images/icon_edit.gif',
            		tooltip : 'View Candidate',
            		padding: 50,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			if(me.submittedCandidates.store.getAt(rowIndex).data.candidateId != null && me.submittedCandidates.store.getAt(rowIndex).data.candidateId != 0)
            				me.viewCandidate(me.submittedCandidates.store.getAt(rowIndex).data.candidateId);
            		}
            	}]
            },{
            	header : 'Candidate',
            	dataIndex : 'candidateId',
            	width :  130,
            	editor: {
            		matchFieldWidth: false,
            		xtype: 'combobox',
            		queryMode: 'local',
            		triggerAction: 'all',
            		displayField: 'fullName',
            		valueField: 'id',
            		emptyText:'Select...',
            		listClass: 'x-combo-list-small',
            		store: ds.contractorsStore,
            		anyMatch:true,
            		width :  130,
            	},
            	renderer:function(value, metadata, record){
            		if(record){
            			return record.data.candidate;
            		}else{
            			return null;
            		}
            	}
            },{
            	header : 'Submission Status',
            	dataIndex : 'submissionStatus',
            	width :  90,
            	editor: {
            		matchFieldWidth: false,
            		xtype: 'combobox',
            		queryMode: 'local',
            		triggerAction: 'all',
            		displayField: 'value',
            		valueField: 'name',
            		emptyText:'Select...',
            		listClass: 'x-combo-list-small',
            		store: ds.submissionStatusStore,
            		width :  130,
            	},
            },{
            	text : 'Submitted Date',
            	dataIndex : 'submittedDate',
            	width :  80,
            	renderer: dateRender,
            	editor: {
            		xtype: 'datefield',
            		format: 'm/d/y'
            	}
            },{
            	text : 'Submitted Time',
            	dataIndex : 'submittedTime',
            	width :  80,
            	editor: {
            		xtype: 'timefield',
            		increment: 1,
            		anchor: '100%'
            	}
            },{
            	header : 'Vendor',
            	dataIndex : 'vendorId',
            	width :  110,
            	editor: me.vendorCombo,
            	renderer:function(value, metadata, record){
            		var vendor = ds.vendorStore.getById(value);
            		if(vendor)
            			return vendor.data.name;
            		else
            			return null;
            	}
            },{
            	header : 'Resume Link',
            	dataIndex : 'resumeLink',
            	renderer: renderValue,
            	editor: {
            		xtype: 'textfield',
            		maxLength:250,
            		enforceMaxLength:true,
            		vtype:'url',
            	},
            },{
            	header : 'Submitted Rate-1',
            	dataIndex : 'submittedRate1',
            	editor: {
            		xtype: 'textfield',
            		maxLength:100,
            		enforceMaxLength:true,
            	},
            },{
            	header : 'Submitted Rate-2',
            	dataIndex : 'submittedRate2',
            	editor: {
            		xtype: 'textfield',
            		maxLength:100,
            		enforceMaxLength:true,
            	},
            },{
            	header : 'Comments',
            	dataIndex : 'comments',
            	width : 120,
            	editor: {
            		xtype: 'textarea',
            		height : 40,
            		maxLength:250,
            		enforceMaxLength:true,
            	},
            },{
            	header : 'Guide',
            	dataIndex : 'guide',
            	renderer:function(value, metadata, record){
            		if (value != null && value != '') {
            			var ids = value.toString().split(',');
            			var names ="",tooltip='';
            			for ( var i = 0; i < ids.length; i++) {
            				var rec = ds.contractorsStore.getById(parseInt(ids[i]));
            				if (rec){
            					names += rec.data.fullName+", ";	
            					tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
            				}
            			}
            			metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
            			return '('+ids.length+') '+ names.substring(0,names.length-2);
            		}
            		return 'N/A';
            	}
            },{
            	header : 'References',
            	dataIndex : 'reference',
            	renderer:function(value, metadata, record){
            		if (value != null && value != '') {
            			var ids = value.toString().split(',');
            			var names ="",tooltip='';
            			for ( var i = 0; i < ids.length; i++) {
            				var rec = ds.contactSearchStore.getById(parseInt(ids[i]));
            				if (rec){
            					names += rec.data.fullName+", ";	
            					tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
            				}
            			}
            			metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
            			return '('+ids.length+') '+ names.substring(0,names.length-2);
            		}
            		return 'N/A';
            	}
            },{
            	dataIndex: 'statusUpdated',
            	text: 'Status Updated Date',
            	width :  80,
            	xtype: 'datecolumn',
            	format:'m/d/Y',
            },{
            	text: 'Resume Help',
  				dataIndex: 'resumeHelp',
            },{
              	text: 'Interview Help',
              	dataIndex: 'interviewHelp',
            },{
              	text: 'Job Help',
              	dataIndex: 'jobHelp',
            },{
            	dataIndex: 'candidateExpectedRate',
            	text: 'Expected Rate',
            },{
            	dataIndex: 'cityAndState',
            	text: 'Homeloc',
            },{
            	dataIndex: 'relocate',
            	text: 'Will Relocate',
            	width :  80,
            	renderer:function(value, metadata, record){
            		if(value){
            			if (value == "YES") {
            				return '<span style="color:#87E320;">' + value + '</span>';
            			} 
            			if (value == "NO") {
            				return '<span style="color:red;">' + value + '</span>';
            			}
            			return value;
            		}
           			return null;
            	},
            },{
            	dataIndex: 'travel',
            	text: 'Will Travel',
            	width :  80,
            	renderer:function(value, metadata, record){
            		if(value){
            			if (value == "YES") {
            				return '<span style="color:#87E320;">' + value + '</span>';
            			} 
            			if (value == "NO") {
            				return '<span style="color:red;">' + value + '</span>';
            			}
            			return value;
            		}
            		return null;
            	}
            },{
            	dataIndex: 'availability',
            	text: 'Availability',
            	width :  80,
            	xtype: 'datecolumn',
            	format:'m/d/Y',
            }],
        });

        me.showPotentialCount = new Ext.form.field.Display({
        	fieldLabel: '',
        });

        me.potentialCandidates = Ext.create('Ext.grid.Panel', {
        	title : 'Suggested Candidates',
        	cls : 'suggested-Orange',
            frame:false,
            anchor:'98%',
            height:250,
            width:'100%',
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
    	        	xtype:'button',
    	        	icon : 'images/icon_copy.png',
    	        	text: 'Shortlist',
    	        	tooltip:'Mark candidate as shortlisted',
    	        	handler: function(){
    	        		me.shortlistCandidate();
    	        	}
    	        },'-',{
    	        	xtype:'button',
    	        	icon : 'images/icon_lookup_down.gif',
    	        	text: 'Submit',
    	        	tooltip:'Mark candidate as submitted',
    	        	handler: function(){
    	        		var record = me.potentialCandidates.getView().getSelectionModel().getSelection()[0];
    	        		if (record == null) {
    	        			Ext.Msg.alert('Error',"Please select a candidate.");
    	            		return false;
    	        		}
    	        		me.submitCandidate(record.data.id);
    	        	}
    	        },'-',{
            		xtype: 'button',
            		text: 'Reset',
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			me.loadPotentialCandidates(me.requirement);
            		} 
            	},'-',{
            		xtype: 'button',
            		text: 'Help',
            		iconCls : 'icon_info',
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
         		        var legendWin = Ext.create("Ext.window.Window", {
         					title: 'Match score',
         					modal: true,
         					height      : 600,
         					width       : 800,
         					bodyStyle   : 'padding: 1px;',
         					html: "&nbsp<table cellpadding='5' bgcolor='#575252'>" +
         							"<tr bgcolor='#dfe8f6'> <th >Job Opening</th> <th >Candidate</th> <th >Score</th> <th >Logic</th> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Skill Set</td> <td >Skill set</td> <td >2</td> <td >for each match</td> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Skill Set</td> <td >Target Skill set</td> <td >1</td> <td >for each match</td> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Module</td> <td >Skill set</td> <td >3</td> <td >for each match</td> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Role Set</td> <td >Role Set</td> <td >3</td> <td >for  1 or more matches</td> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Role Set</td> <td >Target Role</td> <td >2</td> <td >for  1 or more matches</td> </tr>" +
         							"<tr bgcolor='#dfe8f6'> <td >Mandatory Certification</td> <td >Certification</td> <td >5</td> <td >for each match</td> </tr> </table>" +
         							"<br>" +
         							"&nbspA score of <b>6</b> will be added for <b>P0</b> candidates in the above list, a score of <b>4 for P1</b>,  <b>3 for P2</b>, <b>2 for P3 & 1 for P4</b> candidates will be added" +
         							"<br><br>&nbsp--The above match score is calculated for the candidates whose <b>Marketing status</b> is 'Active' and 'Yet to start'" +
         							"<br><br>&nbsp--If the Job is a <b>Remote Role</b> then match score is calculated for all the candidates with any <b>Marketing status</b>" +
         							"<br><br>&nbsp--If the Job is <b>Relocation Needed</b> and <b>Travel needed</b>, candidate who are not <b>willing to Relocate/Travel</b> will be ignored" +
         							"<br><br><b>&nbsp---------Special criteria - These are displayed separately as a link on the top---------</b>" +
         							"<br><br>&nbsp--Candidates who are local to requirement (1.Active ,2.Yet to Start)" +
         							"<br><br>&nbsp--Candidates with Same Client & Same Module Submissions (1.Active ,2.Yet to Start)" +
         							"<br><br>&nbsp--Candidates with Same Client & Same Module Submissions (All Marketing Status)" +
         							"<br><br>&nbsp--Candidates having certifications that are required for the Job Opening (1.Active, 2.Yet to Start)" +
         							"<br><br>&nbsp--Candidate with Same Vendor & Same Module Submission ( All Marketing Status's )" +
         							"<br><br>&nbsp--Candidate with Same Client & Same Module Project ( All Marketing Status's )"
         				});

         				legendWin.show();
         		    } 
            	},'->',me.showPotentialCount]
            }],
            columns: [{
            		xtype: 'actioncolumn',
            		width :35,
            		items : [{
            			icon : 'images/icon_edit.gif',
            			tooltip : 'View Candidate',
            			padding: 50,
            			scope: this,
            			handler : function(grid, rowIndex, colIndex) {
       						me.viewCandidate(this.potentialCandidates.store.getAt(rowIndex).data.id);
            			}
            		}]
            },{
              		xtype: 'gridcolumn',
                    dataIndex: 'matchScore',
                    text: 'Match Score',
                    align : 'center',
                    width : 50,
                    renderer:function(value, metadata, record){
                    	metadata.tdAttr = 'data-qtip="' + record.data.matchScoreDetails + '"';
                    	return value;
                    }
              	},{
                    xtype: 'gridcolumn',
                    dataIndex: 'priority',
                    text: 'Priority',
                    align : 'center',
                    width : 40,
                    renderer:function(value, metadata, record){
                    	if (value == 0 || value == 1)
                    		return '<span style="color:blue;">' + value + '</span>';
                    	return value;
                    }
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'experience',
                    text: 'Experience',
                    align : 'center',
                    width : 40,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'communication',
                    text: 'Communication',
                    align : 'center',
                    width : 40,
                    renderer:function(value, metadata, record){
                    	metadata.tdAttr = 'data-qtip="' + value + '"';
                    	if (value == 'Excellent')
                    		return 9;
                    	else if (value == 'Good')
                    		return 8;
                    	else if (value == 'Average')
                    		return 5;
                    	else if (value == 'Not Relavant')
                    		return 0;
                    }
                },
                {text: 'Candidate',dataIndex: 'fullName',
                    renderer:function(value, metadata, record){
                    	if (record.data.matchIssue == true) {
                    		metadata.tdAttr = 'data-qtip="' + record.data.matchIssueNotes + '"';
                    		return '<span style="color:red;">' + value + '</span>';
						}
                    	metadata.tdAttr = 'data-qtip="' + value + '"';
                    	return value;
                    }
                },
                {text : 'Resume Help',width:80,dataIndex : 'resumeHelp',
                    renderer:function(value, metadata, record){
                    	return value;
                    }
                },
              	{text : 'Marketing Status',dataIndex: 'marketingStatus',renderer: candidateRender,width:55,},
              	{text : 'Alert',dataIndex: 'alert',width:70,renderer: candidateAlertRender,},
              	{text : 'Source',dataIndex: 'source',width:100,renderer: candidateRender,},
              	{text : 'Referral',dataIndex: 'referral',width:80,renderer: candidateRender,},
              	{text : 'Certifications',dataIndex: 'allCertifications',width:120,
              		renderer:function(value, metadata, record){	
                    	if(value != null){
                    	    metadata.tdAttr = 'data-qtip="' + value.replace(/ -- /g, '<br />') + '"';	
                    	}
                        return value ;
                    }	
              	},
              	{text : 'Skill Set',dataIndex: 'skillSet',width:120,renderer: candidateRender,},
              	{text : 'Email Id',dataIndex: 'emailId',width:60,renderer: candidateRender,},
              	{text : 'Contact No',dataIndex: 'contactNumber',width:80,renderer: candidateRender,},
              	{text : 'Availability',dataIndex : 'availability',width :  68,renderer: endDateRenderer,},
              	{text : 'P1 Comments',dataIndex: 'p1comments',width:90,renderer: candidateRender,},
              	{text : 'Expected Rate',dataIndex: 'expectedRate',width:75,renderer: candidateRender,},
              	{text : 'Rate From',dataIndex: 'rateFrom',width:75,renderer: numberRender,},
              	{text : 'Rate To',dataIndex: 'rateTo',width:75,renderer: numberRender,},
              	{text : 'Will Relocate',dataIndex : 'relocate',width :  55,renderer: activeRenderer,},
              	{text : 'Will Travel',dataIndex : 'travel',width :  55,renderer: activeRenderer,},
              	{text : 'Open To CTH',dataIndex : 'openToCTH',width :  75,renderer: candidateRender,},
              	{text : 'City & State',dataIndex : 'cityAndState',width :  90,renderer: candidateRender,},
              	{text : 'Location',dataIndex : 'location',width :  75,renderer: candidateRender,},
              	{text : 'Follow Up',dataIndex : 'followUp',width :  75,renderer: followUpRenderer,},
              	{text : 'Employment Type',dataIndex : 'employmentType',width :  75,renderer: candidateRender,},
              	{text : 'About Partner',dataIndex : 'aboutPartner',width :  75,renderer: candidateRender,},
              	{text : 'Personality',dataIndex : 'personality',width :  75,renderer: candidateRender,},
              	{text : 'Contact Address',dataIndex : 'contactAddress',width :  75,renderer: candidateRender,},
              	{text : 'Start Date',dataIndex : 'startDate',width :  75,renderer: candidateRender,},
              	{text : 'Education',dataIndex : 'education',width :  75,renderer: candidateRender,},
              	{text : 'Immigration Status',dataIndex : 'immigrationStatus',width :  75,renderer: candidateRender,},
              	{text : 'Immigration Verified',dataIndex : 'immigrationVerified',width :  75,renderer: candidateRender,},
              	{text : 'Type',dataIndex : 'type',width :  75,renderer: candidateRender,},
              	{text : 'Employer',dataIndex : 'employer',width :  75,renderer: candidateRender,},
              	{text : 'Highest Qualification Held',dataIndex : 'highestQualification',width :  75,renderer: candidateRender,},
              	{text : 'Roleset',dataIndex : 'currentRoles',width :  75,renderer: candidateRender,},
              	{text : 'Target Roles',dataIndex : 'targetRoles',width :  75,renderer: candidateRender,},
              	{text : 'Target Skill Set',dataIndex : 'targetSkillSet',width :  75,renderer: candidateRender,},
              	{text : 'Other SkillSet' , dataIndex: 'otherSkillSet',width:120,renderer: candidateRender,},
              	{text : 'Interview Help',dataIndex : 'interviewHelp',width :  75,renderer: candidateRender,},
              	{text : 'Job Help',dataIndex : 'jobHelp',width :  75,renderer: candidateRender,},
              	{text : 'Current Job Title',dataIndex : 'currentJobTitle',width :  75,renderer: candidateRender,},
                ],
        });

        me.shortlistedCandidates = Ext.create('Ext.grid.Panel', {
        	title: 'Shortlisted Candidates',
        	cls : 'shortlisted-yellow',
            frame:false,
            anchor:'98%',
            height:180,
            width:'100%',
            plugins: [{
            	ptype : 'cellediting',
            	clicksToEdit: 2
            }],
            listeners: {
                edit: function(editor, event){
                	var record = event.record;
            		if(event.originalValue != event.value){
            			me.modifiedShortlistIds.push(record.data.id);
            		}
            		if (event.column.dataIndex == "candidateId" && record.data.candidateId != null && record.data.candidateId != '' ) {
            			var candidate  = ds.candidateSearchStore.getById(record.data.candidateId);
            			if (candidate != null) {
            				record.data.candidate = candidate.data.fullName;
            				record.data.candidateExpectedRate = candidate.data.expectedRate;
            				record.data.cityAndState = candidate.data.cityAndState;
            				record.data.relocate = candidate.data.relocate;
            				record.data.travel = candidate.data.travel;
            				record.data.availability = candidate.data.availability;
            				me.shortlistedCandidates.getView().refresh();	
            			}
					}
                },
                beforeedit: function(editor, event) {
              		var record = event.record;
              		if (event.column.dataIndex == "candidateId" && record.data.candidateId != null && record.data.candidateId != '' ) {
              			var candidate  = ds.candidateSearchStore.getById(record.data.candidateId);
              			if (candidate == null)
							return false;
              		}
              		return true;
                }
            },
            dockedItems : [{
            	xtype: 'toolbar',
            	dock: 'top',
            	items: [{
            		xtype: 'button',
            		text: 'Add',
            		iconCls : 'btn-add',
            		scope: this,
            		handler: this.addShortlist
            	},'-',{
            		xtype: 'button',
            		text: 'Save',
            		iconCls : 'btn-save',
            		scope: this,
            		handler: this.saveShortlist
            	},'-',{
    	        	xtype:'button',
    	        	icon : 'images/icon_lookup_down.gif',
    	        	text: 'Submit',
    	        	tooltip:'Mark candidate as submitted',
    	        	handler: function(){
    	        		var record = me.shortlistedCandidates.getView().getSelectionModel().getSelection()[0];
    	        		if (record == null) {
    	        			Ext.Msg.alert('Error','Please select a candidate.');
    	            		return false;
    	        		}
    	        		me.submitCandidate(record.data.candidateId);
    	        	}
    	        },'-',{
		    		xtype:'button',
		    		iconCls : 'btn-add',
	                text: 'Add Guide',
	                tooltip:'Add Guide',
		    		handler: function(){
		    			me.addGuide('Shortlisted');
		    		}
		    	},'-',{
            		xtype: 'button',
            		text: 'Reset',
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            				me.modifiedShortlistIds = new Array();
        					me.loadShortlisted(me.requirement.data.id);
        				} 
            	}]
            }],

            columns: [{
            	xtype : 'actioncolumn',
            	hideable : false,
            	width : 30,
            	items : [ {
            		icon : 'images/icon_delete.gif',
            		tooltip : 'Remove',
            		padding : 10,
            		scope : this,
            		handler : function(grid, rowIndex, colIndex) {
            			this.removeShortlistConfirm(me.shortlistedCandidates.store.getAt(rowIndex));
            		} 
            	} ]
            },{
            	xtype : 'actioncolumn',
            	hideable : false,
            	width : 30,
            	items : [{
            		icon : 'images/icon_edit.gif',
            		tooltip : 'View Candidate',
            		padding: 50,
            		scope: this,
            		handler : function(grid, rowIndex, colIndex) {
            			if(me.shortlistedCandidates.store.getAt(rowIndex).data.candidateId != null && me.shortlistedCandidates.store.getAt(rowIndex).data.candidateId != 0)
            				me.viewCandidate(me.shortlistedCandidates.store.getAt(rowIndex).data.candidateId);
            		}
            	}]
            },{
            	header : 'Candidate',
            	dataIndex : 'candidateId',
            	width :  130,
            	editor: {
            		matchFieldWidth: false,
            		xtype: 'combobox',
            		queryMode: 'local',
            		triggerAction: 'all',
            		displayField: 'fullName',
  	                valueField: 'id',
  	                emptyText:'Select...',
  	                listClass: 'x-combo-list-small',
  	                store: ds.contractorsStore,
  	                anyMatch:true,
  	                width :  130,
            	},
            	renderer:function(value, metadata, record){
                  	if(record){
                  		return record.data.candidate;
                  	}else{
                  		return null;
                  	}
            	}
            },{
            	dataIndex: 'candidateExpectedRate',
            	text: 'Expected Rate',
            },{
            	header : 'Comments',
            	dataIndex : 'comments',
            	width : 120,
            	editor: {
            		xtype: 'textarea',
            		height : 40,
            		maxLength:250,
            		enforceMaxLength:true,
            	},
            },{
            	header : 'Guide',
            	dataIndex : 'guide',
            	renderer:function(value, metadata, record){
            		if (value != null && value != '') {
            			var ids = value.toString().split(',');
            			var names ="",tooltip='';
            			for ( var i = 0; i < ids.length; i++) {
            				var rec = ds.contractorsStore.getById(parseInt(ids[i]));
            				if (rec){
            					names += rec.data.fullName+", ";	
            					tooltip += (i+1)+'. '+rec.data.fullName+"<br>";
            				}
            			}
            			metadata.tdAttr = 'data-qtip="' + tooltip.substring(0,tooltip.length-2) + '"';
            			return '('+ids.length+') '+ names.substring(0,names.length-2);
            		}
            		return 'N/A';
            	}
            },{
            	text : 'Resume Help',
            	dataIndex : 'resumeHelp',
            },{
            	dataIndex: 'cityAndState',
            	text: 'Homeloc',
            },{
            	dataIndex: 'relocate',
            	text: 'Will Relocate',
            	width :  80,
            	renderer:function(value, metadata, record){
            		if(value){
            			if (value == "YES") {
            				return '<span style="color:#87E320;">' + value + '</span>';
            			} 
            			if (value == "NO") {
            				return '<span style="color:red;">' + value + '</span>';
            			}
            			return value;
            		}
            		return null;
            	},
            },{
            	dataIndex: 'travel',
            	text: 'Will Travel',
            	width :  80,
            	renderer:function(value, metadata, record){
            		if(value){
            			if (value == "YES") {
            				return '<span style="color:#87E320;">' + value + '</span>';
            			} 
            			if (value == "NO") {
            				return '<span style="color:red;">' + value + '</span>';
            			}
            			return value;
            		}
            		return null;
            	}
            },{
            	dataIndex: 'availability',
            	text: 'Availability',
            	width :  80,
            	xtype: 'datecolumn',
            	format:'m/d/Y',
            }],
        });
        
        me.criteriaPanel = Ext.create('Ext.form.Panel', {
        	frame:false,
        	xtype: 'container',
        	border : 0,
        	items:[{
        		xtype : 'fieldset',
				border : 0,
				layout : 'column',
				items : [{
	   	        	 xtype: 'container',
	   	        	 columnWidth:1,
	   	             items: [{
	 					xtype:'displayfield',
						fieldLabel: '',
						name: 'client',
						height : 18,
						listeners: {
							afterrender: function(component) {
								component.getEl().on('click', function() { 
									me.filterPotential('client');
								});  
							}
						}
					},{
	 					xtype:'displayfield',
						fieldLabel: '',
						name: 'clientAll',
						height : 18,
						listeners: {
							afterrender: function(component) {
								component.getEl().on('click', function() { 
									me.filterPotential('clientAll');
								});  
							}
						}
					},{
						xtype:'displayfield',
						fieldLabel: '',
						name: 'location',
						height : 18,
						listeners: {
							afterrender: function(component) {
								component.getEl().on('click', function() { 
									me.filterPotential('location');
								});  
							}
						}
					},{
						xtype:'displayfield',
						fieldLabel: '',
						name: 'certification',
						height : 18,
						listeners: {
							afterrender: function(component) {
								component.getEl().on('click', function() { 
									me.filterPotential('certification');
								});  
							}
						}
					},{
						xtype:'displayfield',
						fieldLabel: '',
						name: 'vendor',
						height : 18,
						listeners: {
							afterrender: function(component) {
								component.getEl().on('click', function() { 
									me.filterPotential('vendor');
								});  
							}
						}
					},{
						xtype:'displayfield',
						fieldLabel: '',
						name: 'project',
						height : 18,
						listeners: {
							afterrender: function(component) {
								component.getEl().on('click', function() { 
									me.filterPotential('project');
								});  
							}
						}
					},{
	   	            	 xtype:'displayfield',
	   	                 fieldLabel: '',
	   	                 name: 'jobOpeningDetails',
	   	             },{
	   	            	 xtype: 'fieldset',
	   	            	 title:'More',
	   	            	 collapsed : true,
	   	            	 collapsible: true,
	   	            	 columnWidth:.99,
	   	            	 items: [{
	   	            		 xtype: 'container',
	   	            		 columnWidth:1,
	   	            		 items: [{
	   		   	            	 xtype:'displayfield',
	   		   	                 fieldLabel: '',
	   		   	                 name: 'moreDetails',
	   		   	             },{
	   	            			 xtype:'displayfield',
	   	            			 fieldLabel: 'Roles and Responsibilities',
	   	            			 name: 'requirement',
	   	            		 }]
	   	            	 }]
	   	             }]
	   	         }]
        	}]
	     });

        me.items = [{
	   	    	 xtype:'fieldset',
	   	    	 border : 0,
	   	         collapsible: false,
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:1,
	   	             items: [me.criteriaPanel,
	   	                     {
	   	            	 border : false,
	   	            	 height: 5,
	   	             },
	   	             me.potentialCandidates,
	   	             {
	   	            	 border : false,
	   	            	 height: 5,
	   	             },
	   	             me.shortlistedCandidates,
	   	             {
	   	            	 border : false,
	   	            	 height: 5,
	   	             },
	   	             me.submittedCandidates]
	   	         }]
	   	     }
        ];

        me.callParent(arguments);
    },

    shortlistCandidate : function(dat) {
    	var selectedRecord = this.potentialCandidates.getView().getSelectionModel().getSelection()[0];
    	if (selectedRecord == null) {
    		Ext.Msg.alert('Error','Please select a candidate.');
    		return false;
		}
		var selectedRecord = this.potentialCandidates.getView().getSelectionModel().getSelection()[0];
		var requirementId = this.requirement.data.id;
		var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId,
   	 		candidateId : selectedRecord.data.id,
   	 		submissionStatus : 'Shortlisted',
   	 	});
   	 	this.shortlistedCandidates.store.insert(0, rec);
        this.shortlistedCandidates.getView().focusRow(0);
        
		for ( var i = 0; i < this.shortlistedCandidates.store.data.length; i++) {
			var candidate1 = this.shortlistedCandidates.store.getAt(i);
			for ( var j = 0; j < this.shortlistedCandidates.store.data.length; j++) {
				var candidate2 = this.shortlistedCandidates.store.getAt(j);
				if (candidate1 != candidate2 && candidate1.data.candidateId == candidate2.data.candidateId) {
					Ext.Msg.alert('Error',"Candidate already shortlisted");
					this.loadShortlisted(requirementId);
					return false;
				}
			}
		}
        this.saveShortlist();
	},
    
	submitCandidate : function(candidateId) {
		var requirementId = this.requirement.data.id;
		var vendorId = null;
		if (this.requirement.data.vendorId.split(',').length == 1) {
			vendorId = this.requirement.data.vendorId.split(',')[0];
		}
    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId,
   	 		vendorId : vendorId,
   	 		candidateId : candidateId,
   	 		submittedDate : new Date()
   	 	});
    	this.submittedCandidates.store.insert(0, rec);
        this.submittedCandidates.getView().focusRow(0);
        this.submittedCandidates.getView().refresh();
    	for ( var i = 0; i < this.submittedCandidates.store.data.length ; i++) {
			var record = this.submittedCandidates.store.getAt(i);
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				for ( var j = 0; j < this.submittedCandidates.store.getCount(); j++) {
					var oldRec = this.submittedCandidates.store.getAt(j);
					if (oldRec != record && oldRec.data.vendorId == record.data.vendorId && oldRec.data.candidateId == record.data.candidateId) {
						Ext.Msg.alert('Error','Candidate already submitted through given vendor.');
						this.loadSubRequirments(requirementId);
						return false;
					}
				}
			}
		}
    	this.saveCandidates();
	},
	
    addShortlist : function() {
    	var requirementId = this.requirement.data.id;
    	if (requirementId == null || requirementId == '') {
    		Ext.Msg.alert('Error','Please save Job opening');
    		return false;
		}
    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId,
   	 		submissionStatus : 'Shortlisted',
   	 	});
   	 	this.shortlistedCandidates.store.insert(0, rec);
        this.shortlistedCandidates.getView().focusRow(0);
	},
 
	saveShortlist : function() {
		for ( var i = 0; i < this.shortlistedCandidates.store.data.length; i++) {
			var candidate1 = this.shortlistedCandidates.store.getAt(i);
			for ( var j = 0; j < this.shortlistedCandidates.store.data.length; j++) {
				var candidate2 = this.shortlistedCandidates.store.getAt(j);
				if (candidate1 != candidate2 && candidate1.data.candidateId == candidate2.data.candidateId) {
					Ext.Msg.alert('Error','Candidate already shortlisted');
					return false;
				}
			}
		}
		
    	var records = new Array();
    	
    	for ( var i = 0; i < this.shortlistedCandidates.store.data.length ; i++) {
			var record = this.shortlistedCandidates.store.getAt(i);
			
			if (record.data.id == 0 || record.data.id == null || this.modifiedShortlistIds.indexOf(record.data.id) != -1) {
	    		if (record.data.id == 0 || record.data.id == null)
	    			delete record.data.id;
	    		if (record.data.candidateId == 0 || record.data.candidateId == null || record.data.candidateId == ''){
	    			Ext.Msg.alert('Error','Please select the candidate');
					return false;
				}
	    		delete record.data.vendorId;
    			records.push(record.data);
			}
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no changes to submit');
		}else{
	    	records = Ext.JSON.encode(records);
	    	app.requirementService.saveShortlisted(records,this.onSaveShortlisted,this);
		}
	},
	
	onSaveShortlisted : function(data) {
		if (data.success) {
			this.modifiedShortlistIds = new Array();
			this.loadShortlisted(this.requirement.data.id);
			if (Ext.getCmp('guidePopoutWindow') != null)
				Ext.getCmp('guidePopoutWindow').close();
		}else {
			Ext.Msg.alert('Error',data.errorMessage);
			this.loadShortlisted(this.requirement.data.id);
		}
		this.shortlistedCandidates.getSelectionModel().deselectAll();
	},
	

	removeShortlistConfirm : function(record) {
		this.removeRecord = record;
		if (record.data.id == 0 || record.data.id == null){
			this.shortlistedCandidates.store.remove(record);
		}else{
			Ext.Msg.confirm("Confirm.", "Do you want to remove selected Candidate from the Job Opening?", this.removeShortlist, this);
		}
	},

	removeShortlist : function(dat) {
		if (dat=='yes') {
			var record= this.removeRecord;
			var idVal  = record.data.id;
			app.requirementService.removeShortlist(idVal,this.onRemoveShortlist,this);
		}
	},
	
	onRemoveShortlist : function(data) {
		if (data.success) {
			this.modified = true;
			this.loadShortlisted(this.requirement.data.id);
		}else {
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
	},
	
	addCandidate : function() {
    	var requirementId = this.requirement.data.id;
    	if (requirementId == null || requirementId == '') {
    		Ext.Msg.alert('Error','Please save Job opening');
    		return false;
		}
    	var rec = Ext.create( 'tz.model.Sub_Requirement',{
   	 		id : null,
   	 		requirementId : requirementId,
   	 		submittedDate : new Date()
   	 	});
    	this.submittedCandidates.store.insert(0, rec);
        this.submittedCandidates.getView().focusRow(0);
	},
	
    saveCandidates : function() {
    	var records = new Array();
    	for ( var i = 0; i < this.submittedCandidates.store.data.length ; i++) {
			var record = this.submittedCandidates.store.getAt(i);
			
			if (record.data.id == 0 || record.data.id == null || this.modifiedCandidateIds.indexOf(record.data.id) != -1) {
				if (record.data.candidateId == 0 || record.data.candidateId == null || record.data.candidateId == ''){
					Ext.Msg.alert('Error','Please select a candidate');
					return false;
				}
				if (record.data.vendorId == 0 || record.data.vendorId == null || record.data.vendorId == ''){
					Ext.Msg.alert('Error','Please select the vendor for the candidate');
					return false;
				}
				for ( var j = 0; j < this.submittedCandidates.store.getCount(); j++) {
					var oldRec = this.submittedCandidates.store.getAt(j);
					if (oldRec != record && oldRec.data.vendorId == record.data.vendorId && oldRec.data.candidateId == record.data.candidateId) {
						Ext.Msg.alert('Error','Candidate already submitted through given vendor.');
						return false;
					}
				}
	    		if (record.data.id == 0 || record.data.id == null || record.data.id == '')
					delete record.data.id;
	    		
	    		if (record.data.submittedTime != null && record.data.submittedTime != ''){
	    			var time = record.data.submittedTime.split(':');
	    			record.data.submittedDate = new Date(record.data.submittedDate.setHours(record.data.submittedTime.split(':')[0]));
	    			record.data.submittedDate = new Date(record.data.submittedDate.setMinutes(record.data.submittedTime.split(':')[1]));
	    		}
	    		
				records.push(record.data);
			}
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no changes in candidates to submit');
		}else{
        	records = Ext.JSON.encode(records);
        	app.requirementService.saveSubRequirements(records,this.onSaveCandidates,this);
		}
	},
	
	onSaveCandidates : function(data) {
		if (data.success) {
			var jobId = this.requirement.data.id;
			this.loadSubRequirments(jobId);
			this.loadShortlisted(jobId);
			var verifyIds = new Array();
			for ( var i = 0; i < this.verifyCandidateIds.length; i++) {
				var candidate = ds.candidateSearchStore.getById(this.verifyCandidateIds[i]);
				if ((candidate.data.immigrationVerified != 'YES'  && candidate.data.immigrationVerified != 'Not Needed' )&& (candidate.data.priority == 0 || candidate.data.priority == 1 ))
					verifyIds.push(candidate.data.id);
			}
			if (verifyIds.length > 0) {
	        	var params = new Array();
	        	params.push(['candidateId','=', verifyIds.toString()]);
	        	params.push(['jobId','=', jobId]);
	        	var filter = getFilter(params);

				app.requirementService.immigrationVerifyEmails(Ext.JSON.encode(filter));	
			}
			this.verifyCandidateIds = new Array();
			this.modifiedCandidateIds = new Array();
			if (this.CAcceptedIds.length > 0) {
				app.interviewService.insertProjects(this.CAcceptedIds);
			}
			if (this.ISucceededIds.length > 0) {
				app.interviewService.interviewSucceedEmail(this.ISucceededIds);
			}
			
			this.ISucceededIds = new Array();
			if (Ext.getCmp('guidePopoutWindow') != null)
				Ext.getCmp('guidePopoutWindow').close();
			
			if (Ext.getCmp('referencePopoutWindow') != null)
				Ext.getCmp('referencePopoutWindow').close();
		}else {
			if (data.errorMessage == 'Candidate already submitted.Please refresh.'){
				Ext.Msg.alert('Error',data.errorMessage);
				this.loadSubRequirments(this.requirement.data.id);
			}else{
				Ext.Msg.alert('Error','Save Failed');	
			}
		}
		this.submittedCandidates.getSelectionModel().deselectAll();
	},

	removeCandidate : function(dat) {
		if (dat=='yes') {
			var record= this.removeRecord;
			app.requirementService.removeCandidate(record.data.id,this.onRemoveCandidate,this);
		}
	},

	removeCandidateConfirm : function(record) {
		this.removeRecord = record;
		if (record.data.id == 0 || record.data.id == null)
			this.submittedCandidates.store.remove(record);
		else
			Ext.Msg.confirm("Confirm.", "Do you want to remove selected candidate from the Job Opening?", this.removeCandidate, this);
	},
	
	onRemoveCandidate : function(data) {
		if (data.success) {
			this.modified = true;
			this.loadSubRequirments(this.requirement.data.id);
		}else {
			Ext.MessageBox.show({
				title:'Error',
	            msg: data.errorMessage,
	            buttons: Ext.MessageBox.OK,
	            icon: Ext.MessageBox.ERROR
	        });
		}
	},
	
    loadRecords : function(record) {
		this.criteriaPanel.form.findField('location').hide();
		this.criteriaPanel.form.findField('client').hide();
		this.criteriaPanel.form.findField('certification').hide();
		this.criteriaPanel.form.findField('clientAll').hide();
		this.criteriaPanel.form.findField('vendor').hide();
		this.criteriaPanel.form.findField('project').hide();

    	app.requirementService.getSuggestedCriteria(record.data.id,this.onGetSuggestedCriteria,this);
    	this.requirement = record;
    	this.modifiedCandidateIds = new Array();
    	this.modifiedShortlistIds = new Array();
    	ds.candidateSearchStore.clearFilter();
		
		this.loadSubRequirments(record.data.id);
		this.loadShortlisted(record.data.id);
		this.loadPotentialCandidates(record);
		
		if (record.data.clientId != null && record.data.clientId != '') {
			var client = ds.clientSearchStore.getById(Number(record.data.clientId)).data.name;
		}else
			var client = '';
		if (record.data.vendorId != null && record.data.vendorId != '') {
			var ids = record.data.vendorId.toString().split(',');
			var names ="";
			for ( var i = 0; i < ids.length; i++) {
				var rec = ds.vendorStore.getById(parseInt(ids[i]));
				if (rec)
					names += rec.data.name +", ";	
			}
			var vendor = names.substring(0,names.length-2);
		}else
			var vendor = '';
		
		var detailsTable = "<table cellpadding='5' bgcolor='#575252'>" +
				"<tr bgcolor='#ffffff'> <th >Posting Date</th> <th >Posting Title</th> <th >Hotness</th> <th >City & State</th> <th >Client</th> <th >Vendor</th> </tr>" +
				"<tr bgcolor='#ffffff'> <td >"+Ext.Date.format(record.data.postingDate,'m/d/Y')+"</td>" +
				"<td > <a onclick='javascript:app.homeMgmtPanel.editRequirement("+record.data.id+")' href=# title='View' ><img align='top' src='images/icon_edit.gif'></a>&nbsp&nbsp"+
				record.data.postingTitle+"</td>" +
				" <td >"+record.data.hotness+"</td><td >"+record.data.cityAndState+"</td><td >"+client+"</td><td >"+vendor+"</td></tr> </table>";
		
		this.criteriaPanel.form.findField('jobOpeningDetails').setValue(detailsTable);

		var resume = '';
		if(record.data.resume != null)
			resume = record.data.resume+'%';
		detailsTable = "<table cellpadding='5' bgcolor='#575252'>" +
				"<tr bgcolor='#ffffff'> <th >Module</th> <th >Alert</th> <th >Location</th> <th >Employer</th> <th >Resume</th> <th >Source</th> </tr>" +
				"<tr bgcolor='#ffffff' style='text-align:center;'> <td >"+record.data.module+"</td> <td >"+record.data.addInfo_Todos+"</td> <td > "+record.data.location+"</td> " +
				"<td >"+record.data.employer+"</td><td >"+resume+"</td><td >"+record.data.source+"</td></tr>" +
				"<tr bgcolor='#ffffff'> <th >Skill Set</th> <th >Role Set</th> <th >Mandatory Certifications</th> " +
				"<th >Communication Required</th> <th >Experience Required</th> <th >Ok to Train Candidate</th> </tr>" +
				"<tr bgcolor='#ffffff' style='text-align:center;'> <td >"+record.data.skillSet+"</td> <td >"+record.data.targetRoles+"</td> <td >"+record.data.certifications+"</td>" +
				"<td > "+record.data.communication+"</td> <td >"+record.data.experience+"</td><td >"+record.data.helpCandidate+"</td></tr>" +
				" </table>";

		this.criteriaPanel.form.findField('moreDetails').setValue(detailsTable);
		this.criteriaPanel.form.findField('requirement').setValue(record.data.requirement.replace(/(?:\r\n|\r|\n)/g, '<br />'));
	},
    
	onGetSuggestedCriteria : function(data) {
		if (data.success && data.returnVal.rows.length > 0) {
			for ( var i = 0; i < data.returnVal.rows.length; i++) {
				var rec = data.returnVal.rows[i];
				if (rec.type == 'location' ){
					if(rec.value > 0){
						this.criteriaPanel.form.findField('location').show();
						this.criteriaPanel.form.findField('location').setValue('<a >'+rec.value + ' Candidates who are local to requirement (1.Active ,2.Yet to Start)'+'</a><br><br>');
					}else
						this.criteriaPanel.form.findField('location').hide();
				}else if (rec.type == 'client'){
					if(rec.value > 0){
						this.criteriaPanel.form.findField('client').show();
						this.criteriaPanel.form.findField('client').setValue('<a >'+rec.value + ' Candidates with Same Client & Same Module Submissions (1.Active ,2.Yet to Start)'+'</a><br><br>');
					}else
						this.criteriaPanel.form.findField('client').hide();
				}else if (rec.type == 'certification'){
					if(rec.value > 0){
						this.criteriaPanel.form.findField('certification').show();
						this.criteriaPanel.form.findField('certification').setValue('<a >'+rec.value + ' Candidates having certifications that are required for the Job Opening (All Marketing Status)'+'</a><br><br>');
					}else
						this.criteriaPanel.form.findField('certification').hide();
				}else if (rec.type == 'clientAll'){
					if(rec.value > 0){
						this.criteriaPanel.form.findField('clientAll').show();
						this.criteriaPanel.form.findField('clientAll').setValue('<a >'+rec.value + ' Candidates with Same Client & Same Module Submissions (All Marketing Status)'+'</a><br><br>');
					}else
						this.criteriaPanel.form.findField('clientAll').hide();
				}else if (rec.type == 'vendor'){
					if(rec.value > 0){
						this.criteriaPanel.form.findField('vendor').show();
						this.criteriaPanel.form.findField('vendor').setValue('<a >'+rec.value + ' Candidates with Same Vendor & Same Module Submission ( All Marketing Status )'+'</a><br><br>');
					}else
						this.criteriaPanel.form.findField('vendor').hide();
				}else if (rec.type == 'project'){
					if(rec.value > 0){
						this.criteriaPanel.form.findField('project').show();
						this.criteriaPanel.form.findField('project').setValue('<a >'+rec.value + ' Candidates with Same Client & Same Module Project ( All Marketing Status )'+'</a><br><br>');
					}else
						this.criteriaPanel.form.findField('project').hide();
				}
			}
		}
	},
	
	loadSubRequirments : function(requirementId) {
    	if (requirementId != null && requirementId != ""){
        	var params = new Array();
        	params.push(['requirementId','=', requirementId]);
        	params.push(['table','=', "submissions"]);
        	var filter = getFilter(params);
        	this.submittedCandidates.store.loadByCriteria(filter);
    	}else{
    		this.submittedCandidates.store.removeAll();
    	}
	},
	
	loadPotentialCandidates : function(record) {
		var potentialCandidates = new Array();
		var reqSkillSet = record.data.skillSet.split(',');
		var reqModule = record.data.module.split(',');
		var reqTargetRoles = record.data.targetRoles.split(',');
		var reqCertifications = record.data.certifications.split(',');
		var remote = record.data.remote;
		var relocate = record.data.relocate;
		var travel = record.data.travel;
		var requirementId = record.data.id;
		this.potentialCandidates.store.removeAll();

		// if Remote is YES, Don't filter Active and In progress candidates
		// if job relocate or travel any one is true, then any one of them in candidate should be true
		// Both relocate and travel are false the no filter, match all candidates 
		
		if (remote == true) {
			if (relocate == true || travel == true) {
				// Remote - true , Don't filter Active and In progress & Remote or Travel true filter these in Candidates
				ds.candidateSearchStore.filterBy(function(rec) {
					return rec.get('relocate') == true || rec.get('travel') == true ;
				});
			}
		}else{
			if (relocate == true || travel == true) {
				//Remote - No filter Active and In progress & Remote or Travel true filter these in Candidates
				ds.candidateSearchStore.filterBy(function(rec) {
					return ((rec.get('marketingStatus') == '1. Active' || rec.get('marketingStatus') == '2. Yet to Start' )&&(rec.get('relocate') == true || rec.get('travel') == true)) ;
				});
			}else{
				//Remote - No filter Active and In progress & Remote are Travel false, filter these in Candidates
				ds.candidateSearchStore.filterBy(function(rec) {
					return rec.get('marketingStatus') == '1. Active' || rec.get('marketingStatus') == '2. Yet to Start' ;
				});
			}
		}
		
		for ( var i = 0; i < ds.candidateSearchStore.data.getCount(); i++) {
			var record = ds.candidateSearchStore.getAt(i).data;
			record = this.calculateMatching(record);
			var priority = 0;
			if (record.matchScore >= 1 ) {
				//If matchScore>=1, the add priority (for 0 - 6, 1 to 4 give 5-priority)
				if (record.priority != null ) {
					if (record.priority == 0 ){
						priority = 6;
					}else if (record.priority >= 1 && record.priority <= 4 ) {
						priority = 5-record.priority;
					}
				}
				record.matchScore = record.matchScore + priority;
				record.matchScoreDetails += '<br>Priority :' + priority; 
				potentialCandidates.push(record);
			}
		}
		this.potentialCandidates.store.add(potentialCandidates);
		this.potentialCandidates.store.sort('matchScore', 'DESC');
		this.potentialCandidates.getView().refresh();
		ds.candidateSearchStore.clearFilter();
		this.showPotentialCount.setValue(this.potentialCandidates.store.getCount()+' Suggested candidates');

		this.getPotentialCriteria();
		this.separateCriteria();
	},
	
	onGetPotentialCriteria : function(data) {
		if (data.success && data.returnVal.rows.length > 0) {
			for ( var int = 0; int < data.returnVal.rows.length; int++) {
				if (data.returnVal.rows[int].type == 'client' && data.returnVal.rows[int].value != null) {
					var canIds = data.returnVal.rows[int].value.split(',');
					var client = ds.clientSearchStore.getById(Number(data.returnVal.rows[int].clientId)).data.name;
					var module = data.returnVal.rows[int].module;
					for ( var i = 0; i < canIds.length; i++) {
						this.potentialCandidates.store.getById(Number(canIds[i])).data.matchIssue = true;
						this.potentialCandidates.store.getById(Number(canIds[i])).data.matchIssueNotes = '-Sub to '+client+' with a different module ('+module+') <br>';
					}
				}else if (data.returnVal.rows[int].type == 'vendor' && data.returnVal.rows[int].value != null) {
					var canIds = data.returnVal.rows[int].value.split(' :: ');
					for ( var i = 0; i < canIds.length; i++) {
						var vendor = ds.vendorStore.getById(Number(canIds[i].split(' -- ')[1])).data.name;
						var module = canIds[i].split(' -- ')[2];	
						this.potentialCandidates.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssue = true;
						if(this.potentialCandidates.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssueNotes == null)
							this.potentialCandidates.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssueNotes ='';
						this.potentialCandidates.store.getById(Number(canIds[i].split(' -- ')[0])).data.matchIssueNotes += '-Sub to '+vendor+' with a different module ('+module+') <br>';
					}
				}
			}
		}
		this.potentialCandidates.getView().refresh();
	},
	
	loadShortlisted : function(requirementId) {
    	if (requirementId != null && requirementId != ""){
        	var params = new Array();
        	params.push(['table','=', "Shortlisted"]);
        	params.push(['requirementId','=', requirementId]);
        	var filter = getFilter(params);
        	this.shortlistedCandidates.store.loadByCriteria(filter);
    	}else{
    		this.shortlistedCandidates.store.removeAll();
    	}
	},
	
	viewCandidate : function(candidateId) {
		app.homeMgmtPanel.viewCandidate(candidateId);
	},

	filterPotential : function(type) {
		var requirementId = this.requirement.data.id;
    	var params = new Array();
    	params.push(['requirementId','=', requirementId]);
    	params.push(['filterPotential','=', type]);
    	if (type == 'location' || type == 'client' )
    		params.push(['marketingStatus','=', "'1. Active','2. Yet to Start'"]);

    	var filter = getFilter(params);
		filter.pageSize=50;
		app.candidateService.getAllCandidates(Ext.JSON.encode(filter),this.onGetCandidates,this);
	},
	
	onGetCandidates : function(data) {
		this.potentialCandidates.store.removeAll();
		if (data.rows.length > 0) {
			this.potentialCandidates.store.add(data.rows);
		}
		var record = this.requirement; 
		var reqSkillSet = record.data.skillSet.split(',');
		var reqModule = record.data.module.split(',');
		var reqTargetRoles = record.data.targetRoles.split(',');
		var reqCertifications = record.data.certifications.split(',');
		
		for ( var i = 0; i < this.potentialCandidates.store.data.getCount(); i++) {
			var record = this.potentialCandidates.store.getAt(i).data;
			record = this.calculateMatching(record);
			var priority = 0;
			if (record.matchScore >= 1 ) {
				//If matchScore>=1, the add priority (for 0 - 6, 1 to 4 give 5-priority)
				if (record.priority != null ) {
					if (record.priority == 0 ){
						priority = 6;
					}else if (record.priority >= 1 && record.priority <= 4 ) {
						priority = 5-record.priority;
					}
				}
				record.matchScore = record.matchScore + priority;
				record.matchScoreDetails += '<br>Priority :' + priority; 
			}
		}
		this.potentialCandidates.store.sort('matchScore', 'DESC');
		this.potentialCandidates.getView().refresh();
		this.potentialCandidates.getView().refresh();
		this.showPotentialCount.setValue(this.potentialCandidates.store.getCount()+' Suggested candidates');

		if (this.potentialCandidates.store.getCount() > 0) {
	    	var params = new Array();
	    	var candidateIds = this.potentialCandidates.store.collect('id').toString();
	    	params.push(['candidateIds','=', candidateIds]);
	    	params.push(['requirementId','=', this.requirement.data.id]);
	    	var filter = getFilter(params);
			app.requirementService.getPotentialCriteria(Ext.JSON.encode(filter),this.onGetPotentialCriteria,this);
		}
		this.separateCriteria();
	},
	
	separateCriteria : function() {
		var reqModule = this.requirement.data.module.split(',');
		var moduleArray = new Array();
		for ( var i = 0; i < reqModule.length; i++) {
			moduleArray.push({"module":reqModule[i],"count":0,"candidates" : ''});
		}
		var reqSkillSet = this.requirement.data.skillSet.split(',');
		var skillSetArray = new Array();
		for ( var i = 0; i < reqSkillSet.length; i++) {
			skillSetArray.push({"skillSet":reqSkillSet[i],"count":0,"candidates" : ''});
		}
		
		for ( var i = 0; i < this.potentialCandidates.store.getCount(); i++) {
			var record = this.potentialCandidates.store.getAt(i).data;
			//skillset match with module
			if (record.skillSet != null && record.skillSet != '' && reqModule.length > 0) {
				canSkillSet = record.skillSet.split(','); // split with comma
				
				for ( var j = 0; j < canSkillSet.length; j++) {
					if(reqModule.indexOf(canSkillSet[j])!=-1){
						var index = reqModule.indexOf(canSkillSet[j]);
						moduleArray[index].count += 1;
						moduleArray[index].candidates = moduleArray[index].candidates + ',' + record.id;
					}//if
				}//for
			}//if
			
			//skillset match with skillset
			if (record.skillSet != null && record.skillSet != '' && reqSkillSet.length > 0) {
				canSkillSet = record.skillSet;
				canSkillSet = canSkillSet.split(','); // split with comma 
				for ( var j = 0; j < canSkillSet.length; j++) {
					if (reqSkillSet.indexOf(canSkillSet[j]) != -1) {
						var index = reqSkillSet.indexOf(canSkillSet[j]);
						skillSetArray[index].count += 1;
						skillSetArray[index].candidates = skillSetArray[index].candidates + ',' + record.id;
					}//if
				}//for
			}//if
		}//for
		
		if (Ext.getCmp('moduleContainer-'+this.requirement.id) != null)
			this.criteriaPanel.remove(Ext.getCmp('moduleContainer-'+this.requirement.id), true);
		if (Ext.getCmp('skillsetContainer-'+this.requirement.id) != null)
			this.criteriaPanel.remove(Ext.getCmp('skillsetContainer-'+this.requirement.id), true);
		
		var moduleContainer = new Ext.container.Container({
			id : 'moduleContainer-'+this.requirement.id,
			layout: {
		        type: 'table',
		    },
		    items: []
		});
		var items = new Array();
        var field = new Ext.form.field.Display({
        	fieldLabel: 'Module',
        	labelWidth : 50,
        });
        var panel = this;
        items.push(field);
		for ( var i = 0; i < moduleArray.length; i++) {
			var field = new Ext.form.field.Display({
				value : '<a>'+moduleArray[i].module + ' - ' + moduleArray[i].count+'</a>',
				itemId : moduleArray[i].candidates,
				padding : '0 10 0 0',
				listeners: {
					afterrender: function(component) {
						component.getEl().on('click', function() {
							var candidates = this.itemId;
							//remove comma at starting
							if (candidates.charAt(0) == ',')
								candidates = candidates.substr(1);
							panel.loadIntoPotentials(candidates);
						},this,panel);
					}
				}
			});
			items.push(field);
		}
		moduleContainer.add(items);
		
		var skillsetContainer = new Ext.container.Container({
			id : 'skillsetContainer-'+this.requirement.id,
			layout: {
		        type: 'table',
		    },
		    items: []
		});
		var items = new Array();
        var field = new Ext.form.field.Display({
        	fieldLabel: 'Skill Set',
        	labelWidth : 50,
        });
        items.push(field);

		for ( var i = 0; i < skillSetArray.length; i++) {
			var field = new Ext.form.field.Display({
				value : '<a>'+skillSetArray[i].skillSet + ' - ' + skillSetArray[i].count+'</a>',
				itemId : skillSetArray[i].candidates,
				padding : '0 10 0 0',
				listeners: {
					afterrender: function(component) {
						component.getEl().on('click', function() {
							var candidates = this.itemId;
							//remove comma at starting
							if (candidates.charAt(0) == ',')
								candidates = candidates.substr(1);
							panel.loadIntoPotentials(candidates);
						},this,panel);
					}
				}
			});
			items.push(field);
		}
		skillsetContainer.add(items)
		
		this.criteriaPanel.add(moduleContainer);
		this.criteriaPanel.add(skillsetContainer);
		this.criteriaPanel.doLayout();
	},
	
	loadIntoPotentials : function(candidates) {
		this.potentialCandidates.store.removeAll();
		if (candidates != '') {
			var potentialCandidates = new Array();
			var candidateIds = candidates.split(',');
			for ( var i = 0; i < candidateIds.length; i++) {
				var record = ds.candidateSearchStore.getById(Number(candidateIds[i])).data;
				record = this.calculateMatching(record);
				var priority = 0;
				if (record.matchScore >= 1 ) {
					//If matchScore>=1, the add priority (for 0 - 6, 1 to 4 give 5-priority)
					if (record.priority != null ) {
						if (record.priority == 0 ){
							priority = 6;
						}else if (record.priority >= 1 && record.priority <= 4 ) {
							priority = 5-record.priority;
						}
					}
					record.matchScore = record.matchScore + priority;
					record.matchScoreDetails += '<br>Priority :' + priority; 
				}
				potentialCandidates.push(record);
			}
			this.potentialCandidates.store.add(potentialCandidates);
		}
		this.potentialCandidates.store.sort('matchScore', 'DESC');
		this.potentialCandidates.getView().refresh();
		this.showPotentialCount.setValue(this.potentialCandidates.store.getCount()+' Suggested candidates');
		
		this.getPotentialCriteria();

	},

	getPotentialCriteria : function() {
		if (this.potentialCandidates.store.getCount() > 0) {
	    	var params = new Array();
	    	var candidateIds = this.potentialCandidates.store.collect('id').toString();
	    	params.push(['candidateIds','=', candidateIds]);
	    	params.push(['requirementId','=', this.requirement.data.id]);
	    	var filter = getFilter(params);
			app.requirementService.getPotentialCriteria(Ext.JSON.encode(filter),this.onGetPotentialCriteria,this);
		}
	},

	calculateMatching : function(record) {
		var reqSkillSet = this.requirement.data.skillSet.split(',');
		var reqModule = this.requirement.data.module.split(',');
		var reqTargetRoles = this.requirement.data.targetRoles.split(',');
		var reqCertifications = this.requirement.data.certifications.split(',');

		var skillset = 0, module =0, certification =0, targetRole =0, targetSkillset=0, currentRoles = 0 , priority = 0;
		var matchScore = 0;
		//skillset match with skillset, 2 - skillset
		if (record.skillSet != null && record.skillSet != '' && reqSkillSet != null && reqSkillSet != '') {
			canSkillSet = record.skillSet;
			canSkillSet = canSkillSet.split(','); // split with comma 
			for ( var j = 0; j < canSkillSet.length; j++) {
				if (reqSkillSet.indexOf(canSkillSet[j]) != -1) {
					skillset = skillset+2;
				}
			}
		}
		//skillset match with module,  3 - module
		if (record.skillSet != null && record.skillSet != '' && reqModule != null && reqModule != '') {
			canSkillSet = record.skillSet;
			canSkillSet = canSkillSet.split(','); // split with comma 
			for ( var j = 0; j < canSkillSet.length; j++) {
				if(reqModule.indexOf(canSkillSet[j])!=-1){
					module = module+3;
				}
			}
		}
		//target skillset match with target skillset, 1 - skillset
		if (record.targetSkillSet != null && record.targetSkillSet != '' && reqSkillSet != null && reqSkillSet != '') {
			canTargetSkillSet = record.targetSkillSet;
			canTargetSkillSet = canTargetSkillSet.split(','); // split with comma 
			for ( var j = 0; j < canTargetSkillSet.length; j++) {
				if (reqSkillSet.indexOf(canTargetSkillSet[j]) != -1) {
					targetSkillset = targetSkillset+1;
				}
			}
		}
		//Roleset(reqTargetRoles) to Target roles, 2 for any no of roles 
		if (record.targetRoles != null && record.targetRoles != '' && reqTargetRoles != null && reqTargetRoles != '') {
			canTargetRoles = record.targetRoles;
			canTargetRoles = canTargetRoles.split(','); // split with comma 
			for ( var j = 0; j < canTargetRoles.length; j++) {
				if (reqTargetRoles.indexOf(canTargetRoles[j]) != -1) {
					targetRole = 2;
				}
			}
		}
		//Role set(reqTargetRoles) to role set(currentroles), 3 for any no of roles 
		if (record.currentRoles != null && record.currentRoles != '' && reqTargetRoles != null && reqTargetRoles != '') {
			canCurrentRoles = record.currentRoles;
			canCurrentRoles = canCurrentRoles.split(','); // split with comma 
			for ( var j = 0; j < canCurrentRoles.length; j++) {
				if (reqTargetRoles.indexOf(canCurrentRoles[j]) != -1) {
					currentRoles = 3;
				}
			}
		}
		//Certifications match, 5 for each
		//allCertifications contains list of all certifications from grid seperated by ' -- '
		if (record.allCertifications != null && record.allCertifications != '' && reqCertifications != null && reqCertifications != '') {
			canCertifications = record.allCertifications;
			canCertifications = canCertifications.split(' -- '); // split with seperator 
			for ( var j = 0; j < canCertifications.length; j++) {
				if (reqCertifications.indexOf(canCertifications[j]) != -1) {
					certification = certification + 5;
				}
			}
		}
		
		matchScore = skillset + module + certification + targetRole + targetSkillset + currentRoles ;
		record.matchScore = matchScore;
		record.matchScoreDetails = 'Skill set : '+skillset +'<br>Role set : '+currentRoles+ '<br>Module : '+module+
									'<br>Target Skill set : '+ targetSkillset + '<br>Target Role : '+targetRole+'<br>Certification :' + certification ;
		return record;
	},

	addGuide : function(type) {
		if (type == "Shortlisted")
			var selectedRecord = this.shortlistedCandidates.getView().getSelectionModel().getSelection()[0];
		else
			var selectedRecord = this.submittedCandidates.getView().getSelectionModel().getSelection()[0];
    	if (selectedRecord == null) {
    		Ext.Msg.alert('Error','Please select a record.');
    		return false;
		}
    	var guide = selectedRecord.data.guide;
    	guide = guide.split(',').map(Number);
    	var guides = new Array();
    	for ( var i = 0; i < guide.length; i++) {
			var rec = ds.contractorsStore.getById(guide[i]);
			if (rec){
				guides.push(rec.data.fullName);
			}    		
    	}
    	guides = guides.toString();
    	
    	var panel = this;
		if (Ext.getCmp('guidePopoutWindow') == null) {
			var guideWindow = Ext.create('Ext.window.Window', {
			    title: 'Add guide',
			    id : 'guidePopoutWindow',
			    width: 380,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
				                xtype: 'combobox',
				                queryMode: 'local',
				                multiSelect: true,
				                forceSelection : true,
				                displayField: 'fullName',
				    		    valueField: 'id',
				                store: ds.contractorsStore,
			        			name: 'guide',
			        			fieldLabel: 'Guide',
			        			value : guide,
			        			allowBlank: false,
			        			width : 250,
								listeners:{
									select : function(combo,newValue,oldValue){
										Ext.getCmp('guidePopoutWindow').items.items[0].items.items[0].items.items[0].items.items[1].setValue(combo.rawValue);
									}
		   			            }	
			        		},{
			        			xtype: 'textarea',
			        			name: 'guides',  
			        			fieldLabel: 'Selected Guides',
			        			value : guides,
			        			width : 250,
			        		}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
		            handler: function(){
		            	var component = Ext.getCmp('guidePopoutWindow').items.items[0].items.items[0].items.items[0].items.items[0];
		            	if(! component.isValid()){
		            		Ext.Msg.alert('Error','Please fix the errors.');
		            		return false;
		            	}else{
		            		selectedRecord.data.guide = component.value.toString();
		            		
		            		if (type == 'Shortlisted'){
		            			panel.modifiedShortlistIds.push(selectedRecord.data.id);
		            			panel.saveShortlist();
		            		}else{
		            			panel.modifiedCandidateIds.push(selectedRecord.data.id);
		            			panel.saveCandidates();
		            		}
		            			
		            	}
		            }
		        }]
			});
		}
		Ext.getCmp('guidePopoutWindow').show();
	},
	
	addReference : function() {
		var selectedRecord = null;
    	selectedRecord = this.submittedCandidates.getView().getSelectionModel().getSelection()[0];
    	if (selectedRecord == null) {
    		Ext.Msg.alert('Error','Please select a record.');
    		return false;
		}
    	var reference = selectedRecord.data.reference;
    	reference = reference.split(',').map(Number);
    	var references = new Array();
    	for ( var i = 0; i < reference.length; i++) {
			var rec = ds.contactSearchStore.getById(reference[i]);
			if (rec){
				references.push(rec.data.fullName);
			}    		
    	}
    	references = references.toString();
    	
    	var panel = this;
		if (Ext.getCmp('referencePopoutWindow') == null) {
			var referenceWindow = Ext.create('Ext.window.Window', {
			    title: 'Add Reference',
			    id : 'referencePopoutWindow',
			    width: 380,
			    modal: true,
			    bodyPadding: 10,
			    layout: 'fit',
			    items: [{
			        xtype: 'form',
			        bodyPadding: 10,
			        border : 0,
			        frame : true,
			        items: [{
			        	xtype: 'fieldset',
			        	layout: 'anchor',
			        	border : 0,
			        	items: [{
			        		xtype: 'container',
			        		layout: 'anchor',          
			        		defaults: {anchor: '100%'},
			        		items: [{
				                xtype: 'combobox',
				                queryMode: 'local',
				                multiSelect: true,
				                forceSelection : true,
				                displayField: 'fullName',
				    		    valueField: 'id',
				                store: ds.contactSearchStore,
			        			name: 'reference',
			        			fieldLabel: 'Reference',
			        			value : reference,
			        			allowBlank: false,
			        			width : 250,
								listeners:{
									select : function(combo,newValue,oldValue){
										Ext.getCmp('referencePopoutWindow').items.items[0].items.items[0].items.items[0].items.items[1].setValue(combo.rawValue);
									}
		   			            }	
			        		},{
			        			xtype: 'textarea',
			        			name: 'references',  
			        			fieldLabel: 'Selected References',
			        			value : references,
			        			width : 250,
			        		}]
			        	}]
			        }]
			    }],
			    buttons: [{
			    	text: 'Save',
		            handler: function(){
		            	var component = Ext.getCmp('referencePopoutWindow').items.items[0].items.items[0].items.items[0].items.items[0];
		            	if(! component.isValid()){
		            		Ext.Msg.alert('Error','Please fix the errors.');
		            		return false;
		            	}else{
		            		selectedRecord.data.reference = component.value.toString();
		            		panel.modifiedCandidateIds.push(selectedRecord.data.id);
		            		panel.saveCandidates();
		            	}
		            }
		        }]
			});
		}
		Ext.getCmp('referencePopoutWindow').show();
	},


	
});
