Ext.define('tz.ui.RequirementMgmtPanel', {
    extend: 'tz.ui.BaseFormPanel',
    activeItem: 0,
    layout: {
        type: 'card',
    },
    title: '',
    padding: 10,
    border:false,
 
    initComponent: function() {
        var me = this;
        
		// Vendor Details Panel
    	me.requirementsGrid = new tz.ui.RequirementsGrid({manager:me});
    	me.requirementsSearchPanel = new tz.ui.RequirementsSearchPanel({manager:me});
    	me.requirementDetailPanel = new tz.ui.RequirementDetailPanel({manager:me});
    	me.interviewDetailPanel = new tz.ui.InterviewDetailPanel({manager:me});
    	me.requirementSharePanel = new tz.ui.RequirementSharePanel({manager:me});
    	
        me.items = [
            {
            	xtype: 'panel',
            	layout:'anchor',
            	autoScroll:true,    	                 
            	border:false,  
            	items: [me.requirementsSearchPanel,
            	        {
            				border : false,
		   	        		height: 10,
            	        },
            	        me.requirementsGrid]
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
	            items: [me.requirementDetailPanel]
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
	            items: [me.interviewDetailPanel]
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
            },{
            	xtype: 'panel',
                border:false,
                autoScroll:true,
                layout:'anchor',
	            items: [me.requirementSharePanel]
            }
        ];
        me.callParent(arguments);
    },

	initScreen: function(){
    	var browserHeight = app.mainViewport.height;
    	var searchPanelHt = Ext.getCmp('reqSearchPanel').getHeight();
		var headerHt = app.mainViewport.items.items[0].getHeight();
    	Ext.getCmp('reqGridPanel').setHeight(browserHeight - searchPanelHt - headerHt - 30);
    	this.requirementDetailPanel.openedFrom = 'Requirement';
    	
    	ds.contractorsStore.loadByCriteria();
    	var params = new Array();
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	if(app.candidateMgmtPanel != null || ds.candidateSearchStore.getCount() == 0)
    		ds.candidateSearchStore.loadByCriteria(filter);
    	ds.contactSearchStore.loadByCriteria(filter);
    	ds.client_vendorStore.loadByCriteria(filter);
    	
    	var params = new Array();
    	params.push(['type','=', 'Client']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.clientSearchStore.loadByCriteria(filter);

    	var params = new Array();
    	params.push(['type','=', 'Vendor']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.vendorStore.loadByCriteria(filter);

    	ds.userStore.loadByCriteria(filter);
    	var params = new Array();
    	params.push(['authority','=', 'RECRUITER']);
    	var filter = getFilter(params);
    	ds.recruiterStore.loadByCriteria(filter);

    	if (ds.requirementStore.getCount() ==0) 
    		this.requirementsSearchPanel.search();	
	},
	
	showRequirement : function(rowIndex) {
		this.getLayout().setActiveItem(1);
		if (typeof (rowIndex) != 'undefined' && rowIndex != null && rowIndex >= 0) {
			var record = ds.requirementStore.getAt(rowIndex);
		}
		this.requirementDetailPanel.loadForm(record);
		this.requirementDetailPanel.openedFrom = 'Requirement';
	},
	
	closeForm : function() {
		this.getLayout().setActiveItem(0);
		this.requirementsSearchPanel.search();
	},
	
	showInterview : function() {
		this.getLayout().setActiveItem(2);
	},
	
	showContactPanel : function() {
		this.getLayout().setActiveItem(3);
		this.contactDetailPanel.deleteButton.disable();
	},

	showSharePanel : function() {
		this.getLayout().setActiveItem(4);
		this.requirementSharePanel.form.reset();
	},

	showQuickAccess : function() {
    	ds.quickAccessStore.clearFilter();
		var employee = this.quickSearchPanel.quickAccessCombo.getValue();
		if (employee == null) {
			return false;
		}
		app.showSnapshot(employee);
	},

	loadStores : function() {
		ds.clientTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Client Type'])));
    	ds.employerStore.loadByCriteria(getFilter(new Array(['type','=', 'Employer'])));
		ds.researchStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Research Status'])));
		ds.hotnessStore.loadByCriteria(getFilter(new Array(['type','=', 'Hotness'])));
		ds.jobOpeningStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Job Opening Status'])));
		ds.submissionStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Submission Status'])));
    	ds.contactTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Contact Type'])));
    	ds.smeTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'SME Type'])));
    	ds.contactRoleStore.loadByCriteria(getFilter(new Array(['type','=', 'Contact Role'])));

    	//in candidates tab
		ds.marketingStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Marketing Status'])));
		ds.qualificationStore.loadByCriteria(getFilter(new Array(['type','=', 'Qualification'])));
    	ds.candidateTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Type'])));
    	ds.immigrationTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Immigration'])));
    	ds.employmentTypeStore.loadByCriteria(getFilter(new Array(['type','=', 'Employment Type'])));
    	ds.certificationStatusStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Status'])));
    	ds.certificationSponsoredStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Sponsored'])));
    	ds.candidateSourceStore.loadByCriteria(getFilter(new Array(['type','=', 'Candidate Source'])));
    	ds.communicationStore.loadByCriteria(getFilter(new Array(['type','=', 'Communication'])));
    	ds.personalityStore.loadByCriteria(getFilter(new Array(['type','=', 'Personality'])));
    	ds.experienceStore.loadByCriteria(getFilter(new Array(['type','=', 'Experience'])));
    	ds.targetRolesStore.loadByCriteria(getFilter(new Array(['type','=', 'Target Roles'])));
    	ds.certificationsStore.loadByCriteria(getFilter(new Array(['type','=', 'Certification Name'])));

    	var params = new Array();
    	params.push(['type','=', 'Skill Set/Target Skill Set']);
    	var filter = getFilter(params);
    	filter.pageSize=1000;
    	ds.skillSetStore.loadByCriteria(filter);

	}

});
