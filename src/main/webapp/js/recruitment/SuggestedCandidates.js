Ext.define('tz.ui.SuggestedCandidates', {
    extend: 'Ext.grid.Panel',
	title : 'Suggested Candidates',
	cls : 'suggested-Orange',
    frame:false,
    anchor:'98%',
    height:270,
    width:'100%',

    listeners: {
    	edit: function(editor, event){
        	var record = event.record;
    		if(event.originalValue != event.value && record.data.id != null){
    			this.modifiedIds.push(record.data.id);
    		}
    		if(editor.context.field == 'contactNumber'){
    			record.data.contactNumber = record.data.contactNumber.replace(/[^0-9]/g, "");
            	record.data.contactNumber = record.data.contactNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3');
            	this.getView().refresh();
            }
            var scrollPosition = this.getEl().down('.x-grid-view').getScroll();
        	this.getView().refresh();
        	//And then after the record is refreshed scroll to the same position
        	this.getEl().down('.x-grid-view').scrollTo('top', scrollPosition.top, false);
        },
    },
     
    initComponent: function() {
        var me = this;
        me.store = ds.potentialCandidatesStore;
        me.modifiedIds = new Array();
        me.limit = '10';
        
        me.columns = [{
    		xtype: 'actioncolumn',
    		width :35,
    		items : [{
    			icon : 'images/Popout.ico',
    			tooltip : 'Snapshot',
    			padding: 50,
    			scope: this,
    			handler : function(grid, rowIndex, colIndex) {
    				var candidateId = me.store.getAt(rowIndex).data.id;
    				app.homeMgmtPanel.homeDetailPanel.candidatesGrid.showQuickAccess(candidateId);
    			}
    		}]
      	},{
      		xtype: 'gridcolumn',
            dataIndex: 'matchScore',
            text: 'Match Score',
            align : 'center',
            width : 50,
            renderer:function(value, metadata, record){
            	metadata.tdAttr = 'data-qtip="' + record.data.matchScoreDetails + '"';
            	metadata.style = 'background-color:#e5e5e5;';
            	return value;
            }
      	},{
            xtype: 'gridcolumn',
            dataIndex: 'priority',
            text: 'Priority',
            align : 'center',
            width : 40,
            renderer:function(value, metadata, record){
            	if (value == 0 || value == 1)
            		return '<span style="color:blue;">' + value + '</span>';
            	return value;
            },
			editor: {
                xtype: 'numberfield',
                minValue: 0,
                maxValue: 100
            }
        },{
            xtype: 'gridcolumn',
            dataIndex: 'experience',
            text: 'Experience',
            align : 'center',
            width : 40,
			editor: {
                xtype: 'combobox',
                queryMode: 'local',
                typeAhead: true,
                triggerAction: 'all',
                selectOnTab: true,
                displayField: 'name',
    		    valueField: 'value',
                store: ds.experienceStore,
                forceSelection : true,
                lazyRender: true,
                listClass: 'x-combo-list-small'
            },
        },{
            xtype: 'gridcolumn',
            dataIndex: 'communication',
            text: 'Communication',
            align : 'center',
            width : 40,
            renderer:function(value, metadata, record){
            	metadata.tdAttr = 'data-qtip="' + value + '"';
            	metadata.style = 'background-color:#e5e5e5;';
            	if (value == 'Excellent')
            		return 9;
            	else if (value == 'Good')
            		return 8;
            	else if (value == 'Average')
            		return 5;
            	else if (value == 'Not Relavant')
            		return 0;
            }
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'technicalScore',
        	text: 'TS',
        	tooltip : 'Technical Score',
        	width : 40,
        	renderer : function(value, metadata, record) {
        		metadata.style = 'background-color:#e5e5e5;';
        		if (value != null) {
        			metadata.tdAttr = 'data-qtip="' + value + '"';
        			return value;
        		}
        		return 'N/A';
			},
        },
        {text: 'Candidate',dataIndex: 'fullName',
            renderer:function(value, metadata, record){
            	metadata.style = 'background-color:#e5e5e5;';
            	var tooltip ='';
            	if (record.data.matchIssue == true || record.data.matchIssueNotes != '')
            		tooltip += record.data.matchIssueNotes+"<br>";
            	if (record.data.immigrationStatus != null && record.data.immigrationStatus != '')
            		tooltip += '<b>Immigration Status : </b>'+record.data.immigrationStatus+"<br>";
            	if (record.data.p1comments != null && record.data.p1comments != '')
            		tooltip += '<b>P1 Comments : </b>'+record.data.p1comments.replace(/\n/g, '<br />')+"<br>";
            	if (record.data.p2comments != null && record.data.p2comments != '')
            		tooltip += '<b>P2 Comments : </b>'+record.data.p2comments.replace(/\n/g, '<br />')+"<br>";
            	
            	metadata.tdAttr = 'data-qtip="' + tooltip + '"';
            	if (record.data.matchIssue == true) 
            		return '<span style="color:red;">' + value + '</span>';
            	else if(record.data.matchScoreDetails.search('MVC') != -1)
        			return '<span style="color:green;">' + value + '</span>';
        		else if(record.data.matchScoreDetails.search('MV') != -1)
        			return '<span style="color:green;">' + value + '</span>';
        		else if (record.data.matchIssueNotes.search('Sub to same Module, with different vendor') != -1) 
            		return '<span style="color:blue;">' + value + '</span>';
            	 
            	return value;
            }
        },
        {text : 'Resume Help',width:80,dataIndex : 'resumeHelp',
            renderer:function(value, metadata, record){
            	metadata.tdAttr = 'data-qtip="' + value + '"';
            	if (value == '100-Yes')
            		metadata.style = 'background-color:#C3D5F6;';
            	else
            		metadata.style = 'background-color:#e5e5e5;'
            	return value;
            }
        },
      	{text : 'Marketing Status',dataIndex: 'marketingStatus',renderer: candidateRender,width:55,
            editor: {
                xtype: 'combobox',
                queryMode: 'local',
                typeAhead: true,
                triggerAction: 'all',
                selectOnTab: true,
                displayField: 'value',
    		    valueField: 'name',
                store: ds.marketingStatusStore,
                forceSelection : true,
                lazyRender: true,
                listClass: 'x-combo-list-small'
            },
      	},
      	{text : 'Alert',dataIndex: 'alert',width:70,renderer: candidateAlertRender,
            editor: {
                xtype: 'textfield',
                maxLength:45,
                enforceMaxLength:true,
                fieldStyle: "color: red;",
            },
      	},
      	{text : 'Certifications',dataIndex: 'allCertifications',width:120,
      		renderer:function(value, metadata, record){
      			var tooltip = "";
            	if(value != null && value != ''){
            		tooltip = "<b>Certifications :</b><br>"+value.replace(/ -- /g, '<br />')+'<br><br>';
            	}
            	if (record.data.skillSetIds != null  && record.data.skillSetIds != '') {
					var skillSet = record.data.skillSetIds.split(',');
					tooltip += "<b>Skill Set: </b><br>"
					for ( var i = 1; i <= skillSet.length; i++) {
						tooltip += ds.skillSetStore.getById(Number(skillSet[i-1])).data.value+',';
						if (i%3 == 0)
							tooltip += "<br>";
					}
					tooltip += "<br><br>";
				}
            	if (record.data.roleSetIds != null  && record.data.roleSetIds != '') {
					var roleSet = record.data.roleSetIds.split(',');
					tooltip += "<b>Roleset: </b><br>";
					for ( var i = 1; i <= roleSet.length; i++) {
						tooltip += ds.targetRolesStore.getById(Number(roleSet[i-1])).data.value+',';
						if (i%3 == 0)
							tooltip += "<br>";
					}
					tooltip += "<br><br>";
				}
            	if (record.data.targetSkillSetIds != null  && record.data.targetSkillSetIds != '') {
					var targetSkillSet = record.data.targetSkillSetIds.split(',');
					tooltip += "<b>Target Skill Set: </b><br>";
					for ( var i = 1; i <= targetSkillSet.length; i++) {
						tooltip += ds.skillSetStore.getById(Number(targetSkillSet[i-1])).data.value+',';
						if (i%3 == 0)
							tooltip += "<br>";
					}
					tooltip += "<br><br>";
				}
        		if (record.data.targetRoles != null  && record.data.targetRoles != '') {
					var targetRoles = record.data.targetRoles.split(',');
					tooltip += "<b>Target Roles: </b><br>";
					for ( var i = 1; i <= targetRoles.length; i++) {
						tooltip += targetRoles[i-1]+',';
						if (i%3 == 0)
							tooltip += "<br>";
					}
				}
        	    metadata.tdAttr = 'data-qtip="' + tooltip + '"';
        	    metadata.style = 'background-color:#e5e5e5;'
                return value.replace(/ -- /g, ', ') ;
            }	
      	},
      	{text : 'Skill Set',dataIndex: 'skillSet',width:120,renderer: grayColumnRender,hidden : true},
      	{text : 'Email Id',dataIndex: 'emailId',width:60,renderer: grayColumnRender,hidden : true},
      	{text : 'Expected Rate',dataIndex: 'expectedRate',width:75,renderer: candidateRender,
            editor: {
                xtype: 'textfield',
                maxLength:100,
                enforceMaxLength:true,
            }
      	},
      	{text : 'Availability',dataIndex : 'availability',width :  68,renderer: endDateRenderer,
			editor: {
                xtype: 'datefield',
                format: 'm/d/y'
            }
      	},
      	{
      		text: 'Source',dataIndex: 'source',width:80,
      		renderer:function(value, metadata, record){
      			metadata.tdAttr = 'data-qtip="' + "<b>Source : </b>" +value +"<br><b>Referral : </b>"+record.data.referral+ '"';
      			if (value == 'Referral' || value == 'Employee Referral')
      				return 'R-'+record.data.referral;
      			return value;
      		}
      	},
      	{text : 'Will Relocate',dataIndex : 'relocate',width :  55,renderer: activeRenderer,
			editor: {
                xtype: 'combobox',
                queryMode: 'local',
                typeAhead: true,
                triggerAction: 'all',
                selectOnTab: true,
				displayField: 'name',
				valueField: 'value',
				emptyText:'Select...',
				store : ds.boolStore,
                forceSelection : true,
                lazyRender: true,
                listClass: 'x-combo-list-small'
            }
      	},
      	{text : 'Will Travel',dataIndex : 'travel',width :  55,renderer: activeRenderer,
			editor: {
                xtype: 'combobox',
                queryMode: 'local',
                typeAhead: true,
                triggerAction: 'all',
                selectOnTab: true,
				displayField: 'name',
				valueField: 'value',
				emptyText:'Select...',
				store : ds.boolStore,
                forceSelection : true,
                lazyRender: true,
                listClass: 'x-combo-list-small'
            }
      	},
      	{text : 'Open To CTH',dataIndex : 'openToCTH',width :  75,renderer: candidateRender,
			editor: {
                xtype: 'combobox',
                queryMode: 'local',
                typeAhead: true,
                triggerAction: 'all',
                selectOnTab: true,
                displayField: 'name',
    		    valueField: 'value',
                store: ds.yesNoStore,
                forceSelection : true,
                lazyRender: true,
                listClass: 'x-combo-list-small'
            }
      	},
      	{text : 'City & State',dataIndex : 'cityAndState',width : 90,renderer: candidateRender,
            editor: {
                xtype: 'textfield',
                maxLength:90,
                enforceMaxLength:true,
            }
      	},
      	{text : 'Location',dataIndex : 'location',width :  75,renderer: candidateRender,
			editor: {
                xtype: 'combobox',
                queryMode: 'local',
                typeAhead: true,
                triggerAction: 'all',
                selectOnTab: true,
                displayField: 'name',
    		    valueField: 'value',
                store: ds.candidateLocationStore,
                forceSelection : true,
                lazyRender: true,
                listClass: 'x-combo-list-small'
            }
      	},
      	{text : 'Follow Up',dataIndex : 'followUp',width :  75,renderer: followUpRenderer,
			editor: {
                xtype: 'combobox',
                queryMode: 'local',
                typeAhead: true,
                triggerAction: 'all',
                selectOnTab: true,
                displayField: 'name',
    		    valueField: 'value',
                store: ds.yesNoStore,
                forceSelection : true,
                lazyRender: true,
                listClass: 'x-combo-list-small'
            }
      	},
      	{text : 'Immigration Status',dataIndex : 'immigrationStatus',width :  75,renderer: grayColumnRender,},
      	{text : 'Immigration Verified',dataIndex : 'immigrationVerified',width :  75,renderer: grayColumnRender,},
      	{text : 'Roleset',dataIndex : 'currentRoles',width :  75,renderer: grayColumnRender,hidden : true},
      	{text : 'Target Roles',dataIndex : 'targetRoles',width :  75,renderer: grayColumnRender,hidden : true},
      	{text : 'Target Skill Set',dataIndex : 'targetSkillSet',width :  75,renderer: grayColumnRender,hidden : true},
      	{text : 'Interview Help',dataIndex : 'interviewHelp',width :  75,renderer: grayColumnRender,},
      	{text : 'Job Help',dataIndex : 'jobHelp',width :  75,renderer: grayColumnRender,},
      	];
        
        me.viewConfig = {
        		stripeRows: false,
            	getRowClass: function (record, rowIndex, rowParams, store) {
					return 'row-font12';
            	}
        };

		me.genderCombo = Ext.create('Ext.form.ComboBox', {
			fieldLabel: "S",
            name: 'gender',
	        tabIndex:8,
		    queryMode: 'local',
		    displayField: 'value',
		    valueField: 'name',
            store: ds.allGenderStore,
            matchFieldWidth: false,
            labelWidth :10,
            width :80,
		});

        me.ottCheckbox = Ext.create('Ext.form.Checkbox', {
        	fieldLabel : 'OTT',
            name : 'ottCheckbox',
            inputValue: '0',
            padding : '0 0 0 0',
            checked :false,
            labelAlign :'rigth',
            labelWidth :30,
            itemId : 'ottCheckbox',
        });

        me.showPotentialCount = new Ext.form.field.Display({
        	align : 'right',
        	fieldLabel: '',
        });

		me.loadButton = new Ext.Button({
            text: 'Load top 10',
            icon : 'images/idea.png',
            tooltip : 'Load Suggested Candidates',
            scope : this,
            tabIndex:68,
            //hidden : true,
            handler: function(){
            	if (me.loadButton.text == 'Load top 10'){
            		me.loadButton.setText('Load all');
            		me.limit = 10;
            	}else{
            		me.loadButton.setText('Load top 10');
            		me.limit = null;
            	}
				me.manager.loadCandidatesStore();
    			me.manager.getSpecialCriteria(me.manager.requirementPanel.form.findField('id').getValue());
    		}
        });

        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [{
	        	xtype:'button',
                text: 'Save',
                tooltip: 'Save Candidates',
                iconCls : 'btn-save',
	        	tabIndex:65,
	        	handler: function(){
	        		me.saveCandidates();
	        	}
	        },'-',{
	        	xtype:'button',
	        	icon : 'images/icon_copy.png',
	        	text: 'Shortlist',
	        	tooltip:'Mark candidate as shortlisted',
	        	tabIndex:66,
	        	handler: function(){
	        		me.manager.shortlistCandidate();
	        	}
	        },'-',{
	        	xtype:'button',
	        	icon : 'images/icon_lookup_down.gif',
	        	text: 'Submit',
	        	tooltip:'Mark candidate as submitted',
	        	tabIndex:67,
	        	handler: function(){
	        		var record = me.manager.potentialCandidates.getView().getSelectionModel().getSelection()[0];
	        		if (record == null) {
	            		me.manager.updateError('Please select a candidate.');
	            		return false;
	        		}
	        		me.manager.submitCandidate(record.data.id);
	        	}
	        },'-',me.loadButton,'-',
	        {
                xtype: 'button',
                iconCls : 'btn-Search',
                text: 'Search',
                tooltip : 'Load All Suggested Candidates',
                //style:'background-color:#FED097',
                //overCls : 'my-over',
                //pressedCls : 'my-pressed',
                //hidden : true,
                tabIndex:47,
                scope : this,
                handler: function(){
                	me.manager.loadCandidatesStore();
                	me.manager.getSpecialCriteria(me.manager.requirementPanel.form.findField('id').getValue());
                }
            },{
        		xtype: 'button',
        		text: '',
        		tooltip :'Help',
        		iconCls : 'icon_info',
        		tabIndex:70,
        		scope: this,
        		handler : function(grid, rowIndex, colIndex) {
     		        var legendWin = Ext.create("Ext.window.Window", {
     					title: 'Match score',
     					modal: true,
     					autoScroll: true,
     					height      : 600,
     					width       : 850,
     					bodyStyle   : 'padding: 1px;',
     					html: "&nbsp<table cellpadding='5' bgcolor='#575252'>" +
     							"<tr bgcolor='#dfe8f6'> <th >Job Opening</th> <th >Candidate</th> <th >Score</th> <th >Logic</th> </tr>" +
     							"<tr bgcolor='#dfe8f6'> <td >Skill Set</td> <td >Skill set</td> <td >2</td> <td >for each match</td> </tr>" +
     							"<tr bgcolor='#dfe8f6'> <td >Skill Set</td> <td >Target Skill set</td> <td >1</td> <td >for each match</td> </tr>" +
     							"<tr bgcolor='#dfe8f6'> <td >Module</td> <td >Skill set</td> <td >3</td> <td >for each match</td> </tr>" +
     							"<tr bgcolor='#dfe8f6'> <td >Role Set</td> <td >Role Set</td> <td >3</td> <td >for  1 or more matches</td> </tr>" +
     							"<tr bgcolor='#dfe8f6'> <td >Role Set</td> <td >Target Role</td> <td >2</td> <td >for  1 or more matches</td> </tr>" +
     							"<tr bgcolor='#dfe8f6'> <td >Mandatory Certification</td> <td >Certification</td> <td >5</td> <td >for each match</td> </tr> " +
     							"</table>" +
     							"<br><b>&nbsp------Comparing Previous Submissions------</b>" +
     							"<br><br>&nbsp 10 for Same Module-Vendor-Client (MVC), candidate name will be shown in <span style='color:green;'>green color</span>" +
     							"<br><br>&nbsp 5 for Same Module-Vendor (MV), candidate name will be shown in <span style='color:green;'>green color</span>" +
     							"<br><br>&nbsp 2 for Same Module- Different Vendor, candidate name will be shown in <span style='color:blue;'>blue color</span>" +
     							"<br><br>&nbspA score of <b>6</b> will be added for <b>P0</b> candidates in the above list, a score of <b>4 for P1</b>,  <b>3 for P2</b>, <b>2 for P3 & 1 for P4</b> candidates will be added" +
     							"<br><br>&nbsp--The above match score is calculated for the candidates whose <b>Marketing status</b> is 'Active' and 'Yet to start'" +
     							"<br><br>&nbsp--If the Job is a <b>Remote Role</b> then match score is calculated for all the candidates with any <b>Marketing status</b>" +
     							"<br><br>&nbsp--If the Job is <b>Relocate needed</b> and <b>Travel needed</b>, candidate who are not <b>willing to Relocate/Travel</b> will be ignored" +
     							"<br><br><b>&nbsp---------Special criteria - These are displayed separately as a link on the top---------</b>" +
     							"<br><br>&nbsp--Candidates who are local to requirement (1.Active ,2.Yet to Start)" +
     							"<br><br>&nbsp--Candidates with Same Client & Same Module Submissions (1.Active ,2.Yet to Start)" +
     							"<br><br>&nbsp--Candidates with Same Client & Same Module Submissions (All Marketing Status)" +
     							"<br><br>&nbsp--Candidates having certifications that are required for the Job Opening (All Marketing Status's)" +
     							"<br><br>&nbsp--Candidate with Same Vendor & Same Module Submission ( All Marketing Status's )" +
     							"<br><br>&nbsp--Candidate with Same Client & Same Module Project ( All Marketing Status's )" +
     							"<br><br><font color='red'><b>&nbsp---------These are displayed in Red---------</b></font>" +
     							"<br><br>&nbsp--Candidate with Same Client & Different Module Submissions ( All Marketing Status's )" +
     							"<br><br>&nbsp--Candidate with Same Vendor & Different Module Submissions ( All Marketing Status's )" +
     							"<br><br>&nbsp--Candidate with Same Client & Same Vendor Submissions ( All Marketing Status's )" +
     							"<br><br>&nbsp--Candidates with Same Vendor & Withdrawn status ( All Marketing Status's )" +
     							"<br><br>&nbsp--Candidates with Same Client & Withdrawn status ( All Marketing Status's )"
     				});

     				legendWin.show();
     		    } 
        	},
        	'-',me.genderCombo,me.ottCheckbox,
        	'->',me.showPotentialCount,'->',
        	{
                xtype: 'button',
                //width : 30,
                icon : 'images/arrow_out.png',
                text: '',
                tooltip : 'Pop out Suggested Candidates',
                tabIndex:47,
                scope : this,
                handler: function(){
                	me.manager.showSuggestedPopout();
                	
                }
            }]
        }];
        
        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
      	});

        me.callParent(arguments);
        
    },

    plugins: [{
    	ptype : 'cellediting',
    	clicksToEdit: 2
    }],

    saveCandidates : function() {
    	var records = new Array();
    	for ( var i = 0; i < this.store.data.length ; i++) {
			var record = this.store.getAt(i);
    		if (record.data.id == 0 || record.data.id == null){
    			delete record.data.id;
    			records.push(record.data);
    		}
			delete record.data.confidentiality;
			delete record.data.employeeId;
			delete record.data.rateFrom;
			delete record.data.rateTo;
			delete record.data.version;
    		if (record.data.priority == null )
				delete record.data.priority;
    		if (record.data.relocate == null )
				delete record.data.relocate;
    		if (record.data.travel == null )
				delete record.data.travel;
    		if (record.data.goodCandidateForRemote == null )
				delete record.data.goodCandidateForRemote;

    		if (this.modifiedIds.indexOf(record.data.id) != -1)
    			records.push(record.data);	
		}
    	if (records.length == 0) {
    		Ext.Msg.alert('Error','There are no updated Candidates to save');
    		return;
		}else{
			for ( var i = 0; i < records.length; i++) {
				var record = records[i];
				if (record.emailId == null || record.emailId == "") {
	    			Ext.Msg.alert('Error','Candidate Email Id should not be blank');
	    			return false;
				}else if (record.marketingStatus == null || record.marketingStatus == "") {
	    			Ext.Msg.alert('Error','Candidate marketing status should not be blank');
	    			return false;
				}
			}
	    	records = Ext.JSON.encode(records);
	    	app.candidateService.saveCandidates(records,this.onSave,this);
		}
	},
	
	onSave : function(data) {
		if (data.success) {
			Ext.Msg.alert('Success','Saved Successfully');
		}else
			Ext.Msg.alert('Error','Saved Successfully');
		this.modifiedIds =  new Array();
		this.manager.loadCandidatesStore();
	},

});

function grayColumnRender(value, metadata, record, rowIndex, colIndex, store) {
	metadata.tdAttr = 'data-qtip="' + value + '"';
	metadata.style = 'background-color:#e5e5e5;';
	return value;
}