Ext.define('tz.ui.ReqSmeGridPanel', {
    extend: 'Ext.grid.Panel',
    title: "SME's",
    frame:false,
    anchor:'100%',
    height:200,
    width :'100%',
    
    initComponent: function() {
        var me = this;
        me.store = ds.reqSmeContactStore;

        me.columns = [{
        	xtype : 'actioncolumn',
        	width : 30,
        	items : [{
        		icon : 'images/icon_edit.gif',
        		tooltip : 'Edit/View Contact',
        		padding : 10,
        		scope : this,
        		handler : function(grid, rowIndex, colIndex) {
        			var record = ds.reqSmeContactStore.getAt(rowIndex);
        			me.manager.viewContact(record.data.id);
        		} 
        	}]
        },{
        	text : 'Unique Id',
        	dataIndex : 'id',
        	width :  50,
        	hidden : true,
        	renderer: contactRender,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'firstName',
        	text: 'Contact Name',
        	renderer:function(value, metadata, record){
        		metadata.tdAttr = 'data-qtip="'+record.data.firstName+' '+record.data.lastName+'"';
        		return record.data.firstName+' '+record.data.lastName ;
        	}
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'module',
        	text: 'Skill Set',
        	width:150,
        	renderer:function(value, metadata, record){
        		var tooltip = "";
        		if (value != null && value != '')
        			tooltip = "<b>Skill Set : </b><br>"+ value.replace(/,/g, '<br>') +'<br><br>';
        		
        		if (record.data.skillSetGroupIds != null  && record.data.skillSetGroupIds != '') {
					var skillSetGroupIds = record.data.skillSetGroupIds.split(',');
					tooltip = "<b>Skill Set Group : </b><br>";
					for ( var i = 0; i < skillSetGroupIds.length; i++) {
						tooltip += ds.skillsetGroupStore.getById(Number(skillSetGroupIds[i])).data.name +"<br>";
					}
				}
        		if (record.data.roleSetIds != null  && record.data.roleSetIds != '') {
        			var roleSet = record.data.roleSetIds.split(',');
        			tooltip = "<b>Roleset : </b><br>";
        			for ( var i = 1; i <= roleSet.length; i++) {
        				tooltip += ds.targetRolesStore.getById(Number(roleSet[i-1])).data.value +"<br>";
        			}
        		}
    			metadata.tdAttr = 'data-qtip="' + tooltip + '"';
        		return value;
        	}
        },{
        	header : 'Client/Vendor Name',
        	dataIndex : 'clientId',
        	width :  120,
        	renderer:function(value, metadata, record){
        		var rec = ds.client_vendorStore.getById(value);
        		if(rec){
        			metadata.tdAttr = 'data-qtip="'+rec.data.name+'"';
        			return rec.data.name ;
        		}else{
        			metadata.tdAttr = 'data-qtip="' + 'n/a' + '"';
        			return 'n/a';
        		}
        	}
        },{
        	header : 'City & State',
        	dataIndex : 'city',
        	width :  80,
        	renderer:function(value, metadata, record){
        		metadata.tdAttr = 'data-qtip="'+record.data.city+' '+record.data.state+'"';
        		return record.data.city+' '+record.data.state ;
        	}
        },{
        	xtype: 'gridcolumn',
        	text: 'Type',
        	dataIndex : 'type',
        	renderer: contactRender,
        	width :  90,
        	hidden : true,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'score',
        	text: 'Score',
        	width:50,
        	renderer: numberRender,
        	hidden : true,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'priority',
        	text: 'Priority',
        	width:50,
        	renderer: numberRender,
        	hidden : true,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'email',
        	text: 'Email',
        	width:120,
        	renderer: contactRender,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'gender',
        	text: 'Gender',
        	width:55,
        	renderer: contactRender,
        	hidden : true,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'module',
        	text: 'Skill Set',
        	width:100,
        	renderer: contactRender,
        	hidden : true,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'roleSetIds',
        	text: 'Role Set',
        	width:100,
        	hidden : true,
        	renderer:function(value, metadata, record){
        		var tooltip = "";
        		if (record.data.roleSetIds != null  && record.data.roleSetIds != '') {
        			var roleSet = record.data.roleSetIds.split(',');
        			for ( var i = 1; i <= roleSet.length; i++) {
        				tooltip += ds.targetRolesStore.getById(Number(roleSet[i-1])).data.value +"<br>";
        			}
        			metadata.tdAttr = 'data-qtip="' + tooltip + '"';
        		}else
        			return 'N/A' ;
        		return tooltip.replace(/<br>/g, ', ') ;
        	}
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'jobTitle',
        	text: 'Job Title',
        	width:120,
        	renderer: contactRender,
        	hidden : true,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'internetLink',
        	text: 'Internet Link',
        	renderer: contactRender,
        	hidden : true,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'cellPhone',
        	text: 'CellPhone',
        	renderer: contactRender,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'extension',
        	text: 'Extension',
        	hidden : true,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'comments',
        	text: 'Comments',
        	width:140,
        	renderer: contactRender,
        	hidden : true,
        },{
        	xtype: 'gridcolumn',
        	dataIndex: 'role',
        	text: 'Contact Type',
        	width:100,
        	renderer: contactRender,
        	hidden : true,
        }];

        me.resetButton = new Ext.Button({
            icon : 'images/idea.png',
            text: 'Reload',
            tooltip:"Reload SME's",
			scope: this,
	        handler: function() {
	        	this.search();
	        }
        });

        me.showCount = new Ext.form.field.Display({
        	fieldLabel: '',
            width :200,
        });

        me.dockedItems = [{
        	xtype: 'toolbar',
        	dock: 'top',
        	items: [ me.resetButton,'->',me.showCount]
        }];

        me.viewConfig = {
        		stripeRows: false,
        		style: {overflow:'auto', overflowX: 'hidden'},
        };

        me.on('scrollershow', function(scroller) {
      	  if (scroller && scroller.scrollEl) {
      	    scroller.clearManagedListeners(); 
      	    scroller.mon(scroller.scrollEl, 'scroll', scroller.onElScroll, scroller); 
      	  }
        });
        
        me.store.on('load', function(store, records, options) {
			msg = "Displaying "+store.getCount() +" Records";
			this.showCount.setValue(msg);
       	}, me);

        ds.client_vendorStore.on('load', function(store, records, options) {
        	me.getView().refresh();
       	}, me);

        me.callParent(arguments);

        // now you have access to the header - set an event on the header itself
        this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
        	var header = this.headerCt.getGridColumns();
            createHeaderMenu(menu,header);
        }, this);

    },
    
    search : function() {
    	this.store.removeAll();
    	var skillset = this.manager.requirementPanel.form.findField('skillSet').getValue();
    	if (skillset != null && skillset != '') {
    		var params = new Array();
        	params.push(['contactType','=', 'SME']);
        	params.push(['module','=', skillset]);
        	var filter = getFilter(params);
    		this.store.loadByCriteria(filter);
		}
	},
	
	viewContact : function(contactId) {
    	var params = new Array();
    	params.push(['id','=', contactId]);
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	app.contactService.getContacts(Ext.JSON.encode(filter),this.onGetContact,this);
	},
	
	onGetContact : function(data) {
		if (data.success && data.rows.length > 0) {
	    	app.setActiveTab(0,'li_contacts');
	    	app.contactMgmtPanel.getLayout().setActiveItem(1);
	    	app.contactMgmtPanel.contactDetailPanel.opendFrom='panel';
			var contact = Ext.create('tz.model.Contact', data.rows[0]);
			app.contactMgmtPanel.contactDetailPanel.loadForm(contact);
		}
	},
    
});
