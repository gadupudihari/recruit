Ext.define('tz.ui.RequirementsSearchPanel', {
    extend: 'Ext.form.Panel',    
    bodyPadding: 10,
    title: '',
    id : 'reqSearchPanel',
    frame:true,
    anchor:'100%',
    autoScroll:true,
    buttonAlign:'left',
//creating view in init function
    initComponent: function() {
        var me = this;

		me.candidateCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Candidate',
		    store: ds.contractorsStore,
		    queryMode: 'local',
		    anyMatch:true,
		    displayField: 'fullName',
		    valueField: 'id',
		    name : 'candidate',
		    tabIndex:6,
		    labelWidth :110,
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});

		me.vendorCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Vendor',
		    queryMode: 'local',
		    displayField: 'fullName',
		    valueField: 'id',
		    name : 'vendor',
            displayField: 'name',
            valueField: 'id',
            store: ds.vendorStore,
		    tabIndex:3,
		    labelWidth :45,
		    anyMatch:true,
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});

		me.clientCombo = Ext.create('Ext.form.ComboBox', {
		    fieldLabel: 'Client',
		    store: ds.clientSearchStore,
		    queryMode: 'local',
		    displayField: 'name',
		    valueField: 'id',
		    name : 'client',
		    tabIndex:4,
		    labelWidth :45,
		    anyMatch:true,
		    initQuery:  function(){
                this.doQuery('');
                this.setValue('');
            }
		});

		me.hotJobsCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Show by hotness',
		    store : ds.hotnessStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'hotJobs',
		    tabIndex:5,
		    labelWidth :110,
		});

		var defaultOpenings = ['01.In-progress','02.Resume Ready','03.Submitted','04.Hard to Fill'];
		
		me.jobOpeningStatusCombo = Ext.create('Ext.form.ComboBox', {
			multiSelect: true,
		    fieldLabel: 'Job Opening Status',
		    store: ds.jobOpeningStatusStore,
		    queryMode: 'local',
		    displayField: 'value',
            valueField: 'name',
		    name : 'jobOpeningStatus',
		    tabIndex:7,
		    labelWidth :110,
		    value : defaultOpenings,
		    listConfig: {
		        cls: 'grouped-list'
		      },
		      tpl: Ext.create('Ext.XTemplate',
		        '{[this.currentKey = null]}' +
		        '<tpl for=".">',
		          '<tpl if="this.shouldShowHeader(notes)">' +
		            '<div class="group-header">{[this.showHeader(values.notes)]}</div>' +
		          '</tpl>' +
		          '<div class="x-boundlist-item">{name}</div>',
		        '</tpl>',
		        {
		          shouldShowHeader: function(name){
		            return this.currentKey != name;
		          },
		          showHeader: function(name){
		            this.currentKey = name;
		            switch (name) {
		              case 'Active': return 'Active';
		              case 'Inactive': return 'Inactive';
		            }
		            return 'Other';
		          }
		        }
		      )
		});

        me.submissionCombo = Ext.create('Ext.form.ComboBox', {
            fieldLabel: 'Submitted Date',
            store: ds.monthStore,
            name : 'payroll',
            queryMode: 'local',
            displayField: 'label',
            valueField: 'value',
            labelAlign: 'right',
            tabIndex :9,
            labelWidth :120,
            width :240,
            listeners:{
		         select:function(combo, record,index){
	        		 var date = new Date();
	        		 me.form.findField('submittedDateTo').setValue(date);
		        	 if(combo.getValue() == 'One Week')
		        		 me.form.findField('submittedDateFrom').setValue(Ext.Date.add(date, Ext.Date.DAY, -6));
		        	 else if (combo.getValue() == 'Two Weeks')
		        		 me.form.findField('submittedDateFrom').setValue(Ext.Date.add(date, Ext.Date.DAY, -13));
		        	 else if (combo.getValue() == 'One Month') 
		        		 me.form.findField('submittedDateFrom').setValue(Ext.Date.add(date, Ext.Date.MONTH, -1));
		        	 else if (combo.getValue() == 'Three Months') 
		        		 me.form.findField('submittedDateFrom').setValue(Ext.Date.add(date, Ext.Date.MONTH, -3));
		         }
           }
        });

        me.postingCombo = Ext.create('Ext.form.ComboBox', {
            fieldLabel: 'Posting Date',
            store: ds.monthStore,
            name : 'payroll',
            queryMode: 'local',
            displayField: 'label',
            valueField: 'value',
            labelAlign: 'right',
            tabIndex :12,
            labelWidth :105,
            width :240,
            listeners:{
		         select:function(combo, record,index){
	        		 var date = new Date();
	        		 me.form.findField('postingDateTo').setValue(date);
		        	 if(combo.getValue() == 'One Week')
		        		 me.form.findField('postingDateFrom').setValue(Ext.Date.add(date, Ext.Date.DAY, -6));
		        	 else if (combo.getValue() == 'Two Weeks')
		        		 me.form.findField('postingDateFrom').setValue(Ext.Date.add(date, Ext.Date.DAY, -13));
		        	 else if (combo.getValue() == 'One Month') 
		        		 me.form.findField('postingDateFrom').setValue(Ext.Date.add(date, Ext.Date.MONTH, -1));
		        	 else if (combo.getValue() == 'Three Months') 
		        		 me.form.findField('postingDateFrom').setValue(Ext.Date.add(date, Ext.Date.MONTH, -3));
		         }
           }
        });

        me.statusUpdatedCombo = Ext.create('Ext.form.ComboBox', {
            fieldLabel: 'Status Updated',
            store: ds.monthStore,
            name : 'payroll',
            queryMode: 'local',
            displayField: 'label',
            valueField: 'value',
            labelAlign: 'right',
            tabIndex :17,
            labelWidth :120,
            width :240,
            listeners:{
		         select:function(combo, record,index){
	        		 var date = new Date();
	        		 me.form.findField('statusUpdatedTo').setValue(date);
		        	 if(combo.getValue() == 'One Week')
		        		 me.form.findField('statusUpdatedFrom').setValue(Ext.Date.add(date, Ext.Date.DAY, -6));
		        	 else if (combo.getValue() == 'Two Weeks')
		        		 me.form.findField('statusUpdatedFrom').setValue(Ext.Date.add(date, Ext.Date.DAY, -13));
		        	 else if (combo.getValue() == 'One Month') 
		        		 me.form.findField('statusUpdatedFrom').setValue(Ext.Date.add(date, Ext.Date.MONTH, -1));
		        	 else if (combo.getValue() == 'Three Months') 
		        		 me.form.findField('statusUpdatedFrom').setValue(Ext.Date.add(date, Ext.Date.MONTH, -3));
		         }
           }
        });

        me.items = [
            {
	   	    	 xtype:'container',
	   	         layout: 'column',
	   	         items :[{
	   	             xtype: 'container',
	   	             columnWidth:.19,
	   	             items: [{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Search',
	   	                 name: 'searchRequirements',
	   	                 labelWidth :115,
	   	                 tabIndex:1,
	   	                 labelWidth :45,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Module',
	   	                 name: 'module',
	   	                 labelWidth :115,
	   	                 tabIndex:2,
	   	                 labelWidth :45,
	   	             },
	   	             me.vendorCombo,me.clientCombo
	   	             ]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.21,
	   	             items: [me.hotJobsCombo,me.candidateCombo,me.jobOpeningStatusCombo,
	   	                     {
			                xtype: 'combobox',
			                queryMode: 'local',
			                anyMatch:true,
			                forceSelection : true,
			                displayField: 'fullName',
			    		    valueField: 'id',
			                store: ds.contactSearchStore,
		        			fieldLabel: 'Reference',
		        			name : 'reference',
		        			tabIndex : 8,
		        			labelWidth :110,
		        		}]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.2,
	   	             items: [me.submissionCombo,
	   	                     {
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Submitted Date From',
	   	                 name: 'submittedDateFrom',
	   	                 tabIndex:10,
	   	                 labelWidth :120,
	   	                 width :240,
	   	             },{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Submitted Date To',
	   	                 name: 'submittedDateTo',
	   	                 tabIndex:11,
	   	                 labelWidth :120,
	   	                 width :240,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'Unique Ids(Ex:1,2)',
	   	                 name: 'id',
	   	                 labelWidth :120,
	   	                 width :240,
	   	                 tabIndex:12
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.19,
	   	             items: [me.postingCombo,
	   	                     {
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Posting Date From',
	   	                 name: 'postingDateFrom',
	   	                 tabIndex:14,
	   	                 labelWidth :105,
	   	                 width :240,
	   	             },{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Posting Date To',
	   	                 name: 'postingDateTo',
	   	                 tabIndex:16,
	   	                 labelWidth :105,
	   	                 width :240,
	   	             },{
	   	                 xtype:'textfield',
	   	                 fieldLabel: 'City & State',
	   	                 name: 'cityAndState',
	   	                 labelWidth :105,
	   	                 width :240,
	   	                 tabIndex:16
	   	             }]
	   	         },{
	   	             xtype: 'container',
	   	             columnWidth:.19,
	   	             items: [me.statusUpdatedCombo,
	   	                     {
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Status Updated From',
	   	                 name: 'statusUpdatedFrom',
	   	                 tabIndex:18,
	   	                 labelWidth :120,
	   	                 width :240,
	   	             },{
	   	                 xtype:'datefield',
	   	                 fieldLabel: 'Status Updated To',
	   	                 name: 'statusUpdatedTo',
	   	                 tabIndex:19,
	   	                 labelWidth :120,
	   	                 width :240,
	   	             }]
	   	         }]
	   	     }
        ]; 
        me.buttons = [
            {
            	text : 'Search',
            	tooltip : 'Search for job openings',
            	scope: this,
    	        handler: this.search,
    	        tabIndex:20,
			},{
				text : 'Reset',
				tooltip : 'Reset and search for job openings',
				scope: this,
				handler: this.reset,
				tabIndex:21,
			},{
				text : 'Clear',
				tooltip : 'Clear all filters',
				scope: this,
				tabIndex:22,
				handler: function() {
					this.form.reset();
					me.jobOpeningStatusCombo.setValue(null);
				}
			}
		]
        
        me.listeners = {
            afterRender: function(thisForm, options){
                this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {                    
                    enter: this.search,
                    scope: this
                });
            }
        } 
        
        me.callParent(arguments);
    },

    search : function() {
    	// Set the params
    	var values = this.getValues();
    	var params = new Array();
    	params.push(['candidateId','=', this.candidateCombo.getValue()]);
    	params.push(['vendor','=', this.vendorCombo.getValue()]);
    	params.push(['client','=', this.clientCombo.getValue()]);
    	params.push(['search','=', values.searchRequirements]);
    	params.push(['reference','=', values.reference]);
    	params.push(['cityAndState','=', values.cityAndState]);
    	
    	var hotness = values.hotJobs.toString();
    	if (hotness != '')
    		hotness = "'"+hotness.replace(/,/g, "','")+"'";
    	params.push(['hotness','=', hotness]);
    	
    	var openings = values.jobOpeningStatus.toString();
    	if (openings != '')
    		openings = "'"+openings.replace(/,/g, "','")+"'";

    	params.push(['jobOpeningStatus','=', openings]);
    	if (values.submittedDateFrom != undefined && values.submittedDateFrom != '') {
    		var submittedDateFrom = new Date(values.submittedDateFrom);
        	params.push(['submittedDateFrom','=',submittedDateFrom]);
    	}
    	if (values.submittedDateTo != undefined && values.submittedDateTo != '') {
    		var submittedDateTo = new Date(values.submittedDateTo);
        	params.push(['submittedDateTo','=',submittedDateTo]);
    	}
    	if (values.postingDateFrom != undefined && values.postingDateFrom != '') {
    		var postingDateFrom = new Date(values.postingDateFrom);
        	params.push(['postingDateFrom','=',postingDateFrom]);
    	}
    	if (values.postingDateTo != undefined && values.postingDateTo != '') {
    		var postingDateTo = new Date(values.postingDateTo);
        	params.push(['postingDateTo','=',postingDateTo]);
    	}

    	if (values.statusUpdatedFrom != undefined && values.statusUpdatedFrom != '') {
    		var statusUpdatedFrom = new Date(values.statusUpdatedFrom);
        	params.push(['statusUpdatedFrom','=',statusUpdatedFrom]);
    	}
    	if (values.statusUpdatedTo != undefined && values.statusUpdatedTo != '') {
    		var statusUpdatedTo = new Date(values.statusUpdatedTo);
        	params.push(['statusUpdatedTo','=',statusUpdatedTo]);
    	}

    	params.push(['module','=', values.module]);
    	var id = values.id;
    	id = id.replace(',,',",");
    	while (id.length >0) {
        	if (id.substr(id.length-1,id.length) == ",")
        		id = id.substr(0,id.length-1);
			else
				break;
		}
    	if (id.search(',') != -1) 
    		id = "'"+id.replace(/,/g, "','")+"'";

    	params.push(['id','=', id]);
    	
    	// Get the filter and call the search
    	var filter = getFilter(params);
    	filter.pageSize=100;
    	ds.requirementStore.loadByCriteria(filter);
    	app.requirementMgmtPanel.requirementsGrid.modifiedIds =  new Array();
	},
	
	reset:function(){
		this.form.reset();
		app.requirementMgmtPanel.loadStores();
		this.search();
	},
	
});
