
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="icon" href="images/Aj-recruit.PNG" type="image/x-icon">

<title>AJ Recruit</title>
<link rel="stylesheet" type="text/css" href="styles/goog.css" />
<link rel="stylesheet" type="text/css"
	href="js/extjs/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="styles/navigation.css" />

<!-- <script type="application/javascript" src="http://jsonip.appspot.com/?callback=getip"></script> -->



<script>
		var isDevelopment = false;
		
		var userRole =  
			<sec:authorize access="hasRole('ADMIN')">'ADMIN';</sec:authorize>
			<sec:authorize access="hasRole('RECRUITER')">'RECRUITER';</sec:authorize>

		var homePage = 
			<sec:authorize access="hasRole('ADMIN')">'adminDashboard.xml';</sec:authorize>
			<sec:authorize access="hasRole('RECRUITER')">'adminDashboard.xml';</sec:authorize>
			
		var LoggedId = '<sec:authentication property="principal.username" />'  ;
			
</script>


<script type="text/javascript" src="js/extjs/bootstrap.js"></script>

<script type="text/javascript" src="js/util/combobox.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/BaseFormPanel.js"></script>
<script type="text/javascript" src="js/BaseStore.js"></script>

<script type="text/javascript" src="js/data/datastore.js"></script>
<script type="text/javascript" src="js/service/Service.js"></script>

<script type="text/javascript" src="js/Exporter-all.js"></script>
<script type="text/javascript" src="js/combos.js"></script>
<script type="text/javascript" src="js/UnderConstruction.js"></script>

<script type="text/javascript" src="js/employee/service/EmployeeService.js"></script>
<script type="text/javascript" src="js/employee/EmployeePasswordPanel.js"></script>
<script type="text/javascript" src="js/employee/UserGridPanel.js"></script>
<script type="text/javascript" src="js/employee/UserSearchPanel.js"></script>
<script type="text/javascript" src="js/employee/UserMgmtPanel.js"></script>
<script type="text/javascript" src="js/employee/UserDetailPanel.js"></script>

<script type="text/javascript" src="js/settings/service/SettingsService.js"></script>
<script type="text/javascript" src="js/settings/SettingsMgmtPanel.js"></script>
<script type="text/javascript" src="js/settings/SettingsDetailPanel.js"></script>
<script type="text/javascript" src="js/settings/SettingsGridPanel.js"></script>
<script type="text/javascript" src="js/settings/SettingsPanel.js"></script>
<script type="text/javascript" src="js/settings/AlertsDetailPanel.js"></script>
<script type="text/javascript" src="js/settings/ModuleDetailPanel.js"></script>

<script type="text/javascript" src="js/recruitment/service/RequirementService.js"></script>
<script type="text/javascript" src="js/recruitment/RequirementMgmtPanel.js"></script>
<script type="text/javascript" src="js/recruitment/RequirementsGrid.js"></script>
<script type="text/javascript" src="js/recruitment/RequirementsSearchPanel.js"></script>
<script type="text/javascript" src="js/recruitment/RequirementDetailPanel.js"></script>
<script type="text/javascript" src="js/recruitment/PotentialCandidatesPanel.js"></script>
<script type="text/javascript" src="js/recruitment/EmailWindowPanel.js"></script>
<script type="text/javascript" src="js/recruitment/RequirementSharePanel.js"></script>
<script type="text/javascript" src="js/recruitment/SuggestedCandidates.js"></script>
<script type="text/javascript" src="js/recruitment/ReqSmeGridPanel.js"></script>

<script type="text/javascript" src="js/candidate/service/CandidateService.js"></script>
<script type="text/javascript" src="js/candidate/CandidateMgmtPanel.js"></script>
<script type="text/javascript" src="js/candidate/CandidatesGridPanel.js"></script>
<script type="text/javascript" src="js/candidate/CandidatesSearchPanel.js"></script>
<script type="text/javascript" src="js/candidate/CandidatePanel.js"></script>
<script type="text/javascript" src="js/candidate/CertificationGridPanel.js"></script>
<script type="text/javascript" src="js/candidate/ProjectGridPanel.js"></script>

<script type="text/javascript" src="js/placement/service/PlacementService.js"></script>
<script type="text/javascript" src="js/placement/PlacementMgmtPanel.js"></script>
<script type="text/javascript" src="js/placement/PlacementsDetailPanel.js"></script>
<script type="text/javascript" src="js/placement/PlacementSearchPanel.js"></script>
<script type="text/javascript" src="js/placement/PlacementsGrid.js"></script>

<script type="text/javascript" src="js/client/service/ClientService.js"></script>
<script type="text/javascript" src="js/client/ClientGridPanel.js"></script>
<script type="text/javascript" src="js/client/ClientMgmtPanel.js"></script>
<script type="text/javascript" src="js/client/ClientSearchPanel.js"></script>
<script type="text/javascript" src="js/client/ClientPanel.js"></script>

<script type="text/javascript" src="js/contact/service/ContactService.js"></script>
<script type="text/javascript" src="js/contact/ContactMgmtPanel.js"></script>
<script type="text/javascript" src="js/contact/ContactGridPanel.js"></script>
<script type="text/javascript" src="js/contact/ContactSearchPanel.js"></script>
<script type="text/javascript" src="js/contact/ContactDetailPanel.js"></script>

<script type="text/javascript" src="js/interview/service/InterviewService.js"></script>
<script type="text/javascript" src="js/interview/InterviewGridPanel.js"></script>
<script type="text/javascript" src="js/interview/InterviewMgmtPanel.js"></script>
<script type="text/javascript" src="js/interview/InterviewSearchPanel.js"></script>
<script type="text/javascript" src="js/interview/InterviewDetailPanel.js"></script>

<script type="text/javascript" src="js/reports/service/ReportsService.js"></script>
<script type="text/javascript" src="js/reports/ReportsMgmtPanel.js"></script>
<script type="text/javascript" src="js/reports/ReportsDetailPanel.js"></script>


<script type="text/javascript" src="js/quickAccess/service/QuickAccessService.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickAccessPanel.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickAccessMgmtPanel.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickCandidatesGrid.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickVendorGrid.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickClientGrid.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickContactGrid.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickInterviewGrid.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickRequirementsGrid.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickSearchPanel.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickCertificationGrid.js"></script>
<script type="text/javascript" src="js/quickAccess/QuickProjectGridPanel.js"></script>

<script type="text/javascript" src="js/home/HomeMgmtPanel.js"></script>
<script type="text/javascript" src="js/home/HomeDetailPanel.js"></script>
<script type="text/javascript" src="js/home/HomeRequirementsGrid1.js"></script>
<script type="text/javascript" src="js/home/HomeRequirementsGrid2.js"></script>
<script type="text/javascript" src="js/home/HomeCandidatesGrid1.js"></script>
<script type="text/javascript" src="js/home/HomeCandidatesGrid2.js"></script>
<script type="text/javascript" src="js/home/HomeContactGridPanel.js"></script>
<script type="text/javascript" src="js/home/MessagesGrid.js"></script>
<script type="text/javascript" src="js/home/CandidatesEmailGrid.js"></script>
<script type="text/javascript" src="js/home/WorkListGrid.js"></script>
<script type="text/javascript" src="js/home/QuickAddPanel.js"></script>
<script type="text/javascript" src="js/home/HomeSmeGridPanel.js"></script>
<script type="text/javascript" src="js/home/HomeCandidatesSearchPanel.js"></script>

<!--   <script type="text/javascript" src="js/global.js"></script> -->

<script type="text/javascript" src="js/app.js"></script>
</head>
<body>

</body>
</html>
