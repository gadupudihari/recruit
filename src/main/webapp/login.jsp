<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt'%>

<html>
<head>
<title>Login | Recruitment</title>
<link rel="icon" href="images/Aj-recruit.PNG" type="image/x-icon">
<style>
body {
	font-size: 72.5%;
}

label,input {
	display: block;
}

input.text {
	margin-bottom: 12px;
	width: 65%;
	padding: .1em;
}

fieldset {
	padding: 0;
	border: 0;
	margin-top: 25px;
}

h1 {
	font-size: 1.2em;
	margin: .6em 0;
}

div#users-contain {
	width: 350px;
	margin: 20px 0;
}

div#users-contain table {
	margin: 1em 0;
	border-collapse: collapse;
	width: 100%;
}

div#users-contain table td,div#users-contain table th {
	border: 1px solid #eee;
	padding: .6em 10px;
	text-align: left;
}

.ui-dialog .ui-state-error {
	padding: .3em;
}

.validateTips {
	border: 1px solid transparent;
	padding: 0.3em;
}

.pushbutton {
	font-size: 13px;
	font-family: Verdana, sans-serif;
	color: #274302;
	background-color: white;
	border-top-style: solid;
	border-top-color: #274302;
	border-top-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #274302;
	border-bottom-width: 1px;
	border-left-style: solid;
	border-left-color: #274302;
	border-left-width: 1px;
	border-right-style: solid;
	border-right-color: #274302;
	border-right-width: 1px;
	cursor: hand;
}

#loading-mask {
	position: absolute;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	z-index: 21000; /* Normal loading masks are 20001 */
	background-color: white;
}

#loading {
	position: absolute;
	left: 50%;
	top: 50%;
	padding: 2px;
	z-index: 21001;
	height: auto;
	margin: -35px 0 0 -30px;
}

#loading .loading-indicator {
	background:
		url(http://extjs.com/deploy/dev/docs/resources/extanim32.gif)
		no-repeat;
	color: #555;
	font: bold 13px tahoma, arial, helvetica;
	padding: 8px 42px;
	margin: 0;
	text-align: center;
	height: auto;
}

.x-header {
	background: #4C8503;
	height : 67px;
}

</style>


<!-- ExtJS -->
<link type="text/css"
	href="styles/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jQuery/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="js/jQuery/jquery-ui-1.8.16.custom.min.js"></script>

<script>
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		/*
		 */
		 var h = $(window).height();
	     var w = $(window).width();
	        var name = navigator.appName;
	       // alert(name);
	        if(name.indexOf('Explorer')>10) {
	        	var bName="IE";
	        	h=450;
	        	w=380;
	        }else {
	        	var bName="NonIE";
	        	h=250;
	        	w=380;
			}	     
	        
		$("#dialog-modal").dialog({
			height : h,
			width : w,
			resizable: false,
			draggable: false,
			modal : false
			
		});
	});
</script>
</head>

<body style="background-color: rgba(21, 127, 204, 0.13)"
	onload="document.f.j_username.focus();">


        

	<div id="dialog-modal"  title="Recruitment Login" >
		<c:if test="${not empty param.login_error}">
			<font color="red"> Your login attempt was not successful, try
				again.<br /> Reason: <c:out
					value="${SPRING_SECURITY_LAST_EXCEPTION.message}" /> <br /> <br />
			</font>
		</c:if>

		<form name="f" action="<c:url value='j_spring_security_check'/>"
			method="POST">

			<fieldset>
				<p>
					<label for="j_username">Login Id </label> <input required="required" type='text'
						name='j_username' class="text ui-widget-content "
						value='<c:if test="${not empty param.login_error}"><c:out value="${SPRING_SECURITY_LAST_USERNAME}"/></c:if>' />
				</p>

				<p>
					<label for="j_password">Password </label> <input required="required" type='password'
						name='j_password' class="text ui-widget-content ">
				</p>
			</fieldset>


			<table>
				<!-- 
	        <tr><td><input type="checkbox" name="_spring_security_remember_me"></td><td>Don't ask for my password for two weeks</td></tr>
	         -->
				<tr>
					<td><input name="submit" class="pushbutton" type="submit"
						value="Login" /></td>
					<td><input name="reset" class="pushbutton" type="reset"></td>
					<td><span> <a href="<c:url value="emailPass.jsp" />">Forgot
								password?</a>
					</span></td>

				</tr>



			</table>
		</form>
		<br />
	</div>
	<!-- <script type="text/javascript" charset="utf-8">
	(function(){  
        Ext.get('loading').remove();  
        Ext.get('loading-mask').fadeOut({remove:true});  
      }).defer(250);
    });
    </script>
 -->



</body>
</html>

