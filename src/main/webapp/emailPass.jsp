<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page import="org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Reset password service</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%-- <link rel="shortcut icon" type="image/ico" href="<c:url value="/static/resources/img/favicon.ico" />"/> --%>
    <%-- <link rel="stylesheet" href="<c:url value="/static/resources/styles/style.css" />" type="text/css"/> --%>
    

    <link type="text/css" href="${pageContext.request.contextPath}/styles/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jQuery/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jQuery/jquery-ui-1.8.16.custom.min.js"></script>
    

    
    <script>
 	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		/*
		 */
		  var h = $(window).height();
	     var w = $(window).width();
	        var name = navigator.appName;
	       // alert(name);
	        if(name.indexOf('Explorer')>10) {
	        	var bName="IE";
	        	h=475;
	        	w=380;
	        }else {
	        	var bName="NonIE";
	        	h=275;
	        	w=380;
			}	     
	        
		$("#dialog-modal").dialog({
			height : h,
			width : w,
			resizable: false,
			draggable: false,
			modal : false
			
		});
	});  
</script>
    
    
</head>
<body style="background-color: #F3FCE7">
<div style="height: 48px;"></div>
<div id="loginWindow" >

<div id="dialog-modal" title="Recover Password">

    <form name="f" action="<c:url value="/remote/recoveryPassword/pass" />" method="post">
    
        <div class="row clear">
            <div class="value">Enter your EmailId or LoginId</div>
     
            <div style="font-size: 16px;">
                LoginId : <input type="text" name="email"/>
            </div>

          <div style="font-size: 16px;">
                  EmailId : <input type="text" name="email"/>
            </div>
     
            <div style="color: #FF0000;font-size: 15px;">${failMessage}</div>
            <div style="color: #438f23;font-size: 15px;">${successMessage}</div>
        </div>

        <br/>

        <div class="submit"><input type="submit"  name="Recover" value="Recover"/>
        <input type="reset"  name="Reset" value="Reset"/>
        </div>

        <div>
            <a href="<c:url value="/login.jsp" />">Log In</a>
        </div>
    </form>
</div></div>
</body>
</html>
